package commands.user;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * @author Ronn
 */
public class ServerTime implements Command {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("M/d/yy h:mm");

	/** текущая дата */
	private final Date date;

	public ServerTime() {
		this.date = new Date();
	}

	@Override
	public void execute(final PlayerShip ship, final String values) {

		final Date date = getDate();
		date.setTime(System.currentTimeMillis());

		synchronized(date) {
			ship.sendMessage(DATE_FORMAT.format(date), LocalObjects.get());
		}
	}

	@Override
	public int getAccessLevel() {
		return USER;
	}

	private Date getDate() {
		return date;
	}

	@Override
	public String getDescription() {
		return "@command:time@";
	}

	@Override
	public String getName() {
		return "time";
	}
}

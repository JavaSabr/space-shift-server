package commands.user;

import rlib.util.StringUtils;

import com.ss.server.LocalObjects;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.impl.Space;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerAction;

/**
 * Реализация команды приглашения в группу.
 * 
 * @author Ronn
 */
public class InviteToParty implements Command {

	private static final Space SPACE = Space.getInstance();

	@Override
	public void execute(final PlayerShip ship, final String values) {

		if(StringUtils.isEmpty(values)) {
			return;
		}

		if(StringUtils.equals(ship.getName(), values)) {
			return;
		}

		final SpaceLocation location = SPACE.getLocation(ship.getLocationId());
		final PlayerShip target = location.findObject(PlayerShip.class, ship, values);

		if(target == null) {
			return;
		}

		final PlayerManager manager = PlayerManager.getInstance();
		manager.requestAction(PlayerAction.INVITE_TO_PARTY, ship, target, LocalObjects.get());
	}

	@Override
	public int getAccessLevel() {
		return Command.USER;
	}

	@Override
	public String getDescription() {
		return "@command:invite@";
	}

	@Override
	public String getName() {
		return "invite";
	}
}

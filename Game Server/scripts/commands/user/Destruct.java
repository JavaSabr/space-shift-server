package commands.user;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Space;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Пользовательская команда для самоуничтожения.
 * 
 * @author Ronn
 */
public class Destruct implements Command {

	private static final Space SPACE = Space.getInstance();

	@Override
	public void execute(final PlayerShip ship, final String values) {

		final LocalObjects local = LocalObjects.get();

		if(!ship.isDestructed()) {

			ship.setCurrentStrength(0, local);
			ship.doDestruct(ship, local);

			final DestroyRatingEvent event = RatingEvents.getDestroyRatingEvent(local);
			event.setDamageType(null);
			event.setDestroyed(ship);
			event.setDestroyer(ship);

			final RatingSystem ratingSystem = ship.getRatingSystem();
			ratingSystem.notify(event);
		}
	}

	@Override
	public int getAccessLevel() {
		return USER;
	}

	@Override
	public String getDescription() {
		return "@command:destruct@";
	}

	@Override
	public String getName() {
		return "destruct";
	}
}

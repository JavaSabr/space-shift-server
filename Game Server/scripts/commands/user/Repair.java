package commands.user;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Space;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.module.external.impl.FractionRepairModule;

/**
 * Пользовательская команда для восстановления корабля на ближайшей точке.
 * 
 * @author Ronn
 */
public class Repair implements Command {

	private static final Space SPACE = Space.getInstance();

	private static final ThreadLocal<Array<SpaceStation>> LOCAL_STATION_CONTAINER = new ThreadLocal<Array<SpaceStation>>() {

		@Override
		protected Array<SpaceStation> initialValue() {
			return ArrayFactory.newArray(SpaceStation.class);
		}
	};

	@Override
	public void execute(final PlayerShip ship, final String values) {

		if(!ship.isDestructed()) {
			return;
		}

		final Array<SpaceStation> container = LOCAL_STATION_CONTAINER.get();
		container.clear();

		final LocalObjects local = LocalObjects.get();

		final SpaceLocation location = SPACE.getLocation(ship.getLocationId());
		location.addAvailableStations(container, ship, FractionRepairModule.class, local);

		if(container.isEmpty()) {
			ship.sendMessage("Нету подходящей станции для восстановления.", local);
			return;
		}

		SpaceStation near = null;

		float distance = Integer.MAX_VALUE;

		for(final SpaceStation station : container.array()) {

			if(station == null) {
				break;
			}

			final float distanceTo = station.distanceTo(ship);

			if(distanceTo < distance) {
				near = station;
				distance = distanceTo;
			}
		}

		if(near == null) {
			ship.sendMessage("Нету подходящей станции для восстановления.", local);
			return;
		}

		final FractionRepairModule module = near.findModule(FractionRepairModule.class);
		module.execute(near, ship, local);
	}

	@Override
	public int getAccessLevel() {
		return USER;
	}

	@Override
	public String getDescription() {
		return "@command:repair@";
	}

	@Override
	public String getName() {
		return "repair";
	}
}

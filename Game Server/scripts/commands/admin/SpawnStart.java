package commands.admin;

import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.table.SpawnTable;

/**
 * Команда для запуска спавнов.
 * 
 * @author Ronn
 */
public class SpawnStart implements Command {

	@Override
	public void execute(final PlayerShip ship, final String values) {
		final SpawnTable spawnTable = SpawnTable.getInstance();
		spawnTable.start();
	}

	@Override
	public int getAccessLevel() {
		return Command.ADMIN;
	}

	@Override
	public String getDescription() {
		return "@command:spawnStart@";
	}

	@Override
	public String getName() {
		return "spawn_start";
	}
}

package commands.admin;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.ItemUtils;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Команда для спавна предметов.
 * 
 * @author Ronn
 */
public class SpawnItem implements Command {

	@Override
	public void execute(final PlayerShip ship, final String values) {

		if(values == null) {
			return;
		}

		final String[] vals = values.split(" ");

		if(vals.length < 1) {
			return;
		}

		final int itemId = Integer.parseInt(vals[0]);
		final int itemCount = vals.length > 1 ? Integer.parseInt(vals[1]) : 1;

		final SpaceItem item = ItemUtils.createItem(itemId, itemCount, "command_spawn_item");

		if(item != null) {
			ItemUtils.spawnItemTo(item, ship, LocalObjects.get(), vals.length > 2 ? Integer.parseInt(vals[2]) : 0);
		}
	}

	@Override
	public int getAccessLevel() {
		return Command.ADMIN;
	}

	@Override
	public String getDescription() {
		return "@command:spawnItem@";
	}

	@Override
	public String getName() {
		return "spawn_item";
	}
}

package commands.admin;

import rlib.geom.Vector;

import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Команда для получения текущих координат.
 * 
 * @author Ronn
 */
public class ShipLoc implements Command {

	@Override
	public void execute(final PlayerShip ship, final String values) {

		final Vector location = ship.getLocation();

		final StringBuilder result = new StringBuilder("Current position: x = ");
		result.append(location.getX());
		result.append(" y = ").append(location.getY());
		result.append(" z = ").append(location.getZ());
		result.append(" location = ").append(ship.getLocationId());

		System.out.println(result);

		System.out.println("current rotation = " + ship.getRotation());
	}

	@Override
	public int getAccessLevel() {
		return Command.ADMIN;
	}

	@Override
	public String getDescription() {
		return "@command:shipLoc@";
	}

	@Override
	public String getName() {
		return "ship_loc";
	}
}

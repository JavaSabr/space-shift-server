package com.ss.server;

import java.util.concurrent.ScheduledExecutorService;

import rlib.database.ConnectFactory;
import rlib.idfactory.IdGenerator;
import rlib.idfactory.IdGeneratorFactory;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.server.database.DataBaseManager;
import com.ss.server.database.TableNames;
import com.ss.server.manager.ExecutorManager;

/**
 * Глобальная фабрика ид.
 * 
 * @author Ronn
 */
public final class IdFactory {

	public static IdFactory getInstance() {

		if(instance == null) {
			instance = new IdFactory();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(IdFactory.class);

	private static IdFactory instance;

	private static final String[][] SHIP_TABLE = { {
		TableNames.TABLE_SHIPS,
		TableNames.TABLE_SHIPS_C_OBJECT_ID,
	},
	};

	private static final String[][] ITEM_TABLE = { {
		TableNames.TABLE_ITEMS,
		TableNames.TABLE_ITEMS_C_OBJECT_ID,
	},
	};

	private static final String[][] MODULE_TABLE = { {
		TableNames.TABLE_MODULES,
		TableNames.TABLE_MODULES_C_OBJECT_ID,
	},
	};

	/** фабрика ид для кораблей */
	private final IdGenerator shipIds;
	/** фабрика ид для кораблей */
	private final IdGenerator moduleIds;
	/** фабрика ид для предметов */
	private final IdGenerator itemIds;
	/** фабрика ид для подтверждений аккаунтов */
	private final IdGenerator authIds;
	/** фабрика ид для космических станций */
	private final IdGenerator stationIds;
	/** фабрика ид для гравитационных объектов */
	private final IdGenerator gravityIds;
	/** фабрика ид для локационных объектов */
	private final IdGenerator locationIds;
	/** фабрика ид для NPS */
	private final IdGenerator npsIds;

	private IdFactory() {
		InitializeManager.valid(getClass());

		final DataBaseManager dbManager = DataBaseManager.getInstance();
		final ConnectFactory connectFactory = dbManager.getConnectFactory();

		final ExecutorManager executor = ExecutorManager.getInstance();
		final ScheduledExecutorService idExecutor = executor.getIdGeneratorExecutor();

		shipIds = IdGeneratorFactory.newBitSetIdGeneratoe(connectFactory, idExecutor, SHIP_TABLE);
		shipIds.prepare();

		moduleIds = IdGeneratorFactory.newBitSetIdGeneratoe(connectFactory, idExecutor, MODULE_TABLE);
		moduleIds.prepare();

		itemIds = IdGeneratorFactory.newBitSetIdGeneratoe(connectFactory, idExecutor, ITEM_TABLE);
		itemIds.prepare();

		authIds = IdGeneratorFactory.newSimpleIdGenerator(1, Integer.MAX_VALUE);
		stationIds = IdGeneratorFactory.newSimpleIdGenerator(1, Integer.MAX_VALUE);
		gravityIds = IdGeneratorFactory.newSimpleIdGenerator(1, Integer.MAX_VALUE);
		locationIds = IdGeneratorFactory.newSimpleIdGenerator(1, Integer.MAX_VALUE);
		npsIds = IdGeneratorFactory.newSimpleIdGenerator(1, Integer.MAX_VALUE);

		LOGGER.info("initialized.");
	}

	/**
	 * @return новый ид для ожидающего авторизации.
	 */
	public int getNextAuthId() {
		return authIds.getNextId();
	}

	/**
	 * @return новый ид для гравитационного объекта.
	 */
	public int getNextGravityId() {
		return gravityIds.getNextId();
	}

	/**
	 * @return новый ид для предмета.
	 */
	public int getNextItemId() {
		return itemIds.getNextId();
	}

	/**
	 * @return новый ид для локационного объекта.
	 */
	public int getNextLocationId() {
		return locationIds.getNextId();
	}

	/**
	 * @return новый ид для модуля.
	 */
	public int getNextModuleId() {
		return moduleIds.getNextId();
	}

	/**
	 * @return новый ид для NPS.
	 */
	public int getNextNpsId() {
		return npsIds.getNextId();
	}

	/**
	 * @return новый ид для корабля.
	 */
	public int getNextShipId() {
		return shipIds.getNextId();
	}

	/**
	 * @return новый ид для станции.
	 */
	public int getNextStationId() {
		return stationIds.getNextId();
	}
}
package com.ss.server.manager;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.ClassUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.model.func.Func;
import com.ss.server.model.func.factory.FuncFactory;

/**
 * Менеджер для работы с функциями.
 * 
 * @author Ronn
 */
public final class FuncManager {

	private static final Logger LOGGER = LoggerManager.getLogger(FuncManager.class);

	private static FuncManager instance;

	public static FuncManager getInstance() {

		if(instance == null) {
			instance = new FuncManager();
		}

		return instance;
	}

	/** таблица "название фунции - список фабрик функций */
	private final Table<String, Array<FuncFactory>> factories;

	private FuncManager() {
		InitializeManager.valid(getClass());
		factories = TableFactory.newObjectTable();

		prepare();

		LOGGER.info("loaded factories to " + factories.size() + " types funcs.");
	}

	/**
	 * Добавление новой фабрики функций.
	 * 
	 * @param type класс фабрики.
	 */
	private void addFuncFactory(final Class<? extends FuncFactory> type) {

		final FuncFactory factory = ClassUtils.newInstance(type);

		for(final String name : factory.getFuncNames()) {

			Array<FuncFactory> container = factories.get(name);

			if(container == null) {
				container = ArrayFactory.newArray(FuncFactory.class);
				factories.put(name, container);
			}

			container.add(factory);
		}
	}

	/**
	 * Парс функции из хмл.
	 * 
	 * @param node хмл узел.
	 * @param objects дополнительные объекты.
	 * @return итоговая функция.
	 */
	public Func parse(final Node node, final Object... objects) {

		final Array<FuncFactory> container = factories.get(node.getNodeName());

		if(container == null) {
			LOGGER.warning(new Exception("not found factory for type " + node.getNodeName()));
			return null;
		}

		for(final FuncFactory factory : container.array()) {

			if(factory == null) {
				break;
			}

			final Func func = factory.create(node, objects);

			if(func != null) {
				return func;
			}
		}

		return null;
	}

	/**
	 * Поиск и регистрация всех реалищаций функций.
	 */
	private void prepare() {

		final ClassManager manager = ClassManager.getInstance();
		final Array<Class<FuncFactory>> classes = ArrayFactory.newArray(Class.class);

		manager.findImplements(classes, FuncFactory.class);

		for(final Class<FuncFactory> factoryClass : classes) {
			addFuncFactory(factoryClass);
		}
	}
}

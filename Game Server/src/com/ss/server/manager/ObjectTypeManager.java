package com.ss.server.manager;

import rlib.util.StringUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.SpaceObjectType;

/**
 * Менеджер по типам игровых объектов.
 * 
 * @author Ronn
 */
public class ObjectTypeManager {

	private static final ObjectTypeManager INSTANCE = new ObjectTypeManager();

	public static ObjectTypeManager getInstance() {
		return INSTANCE;
	}

	/** список доступных типов объектов */
	private final Array<SpaceObjectType> objectTypes;

	public ObjectTypeManager() {
		this.objectTypes = ArrayFactory.newArray(SpaceObjectType.class);
	}

	/**
	 * Поиск типа указанного названия.
	 * 
	 * @param name название типа.
	 * @return тип по этому названию.
	 */
	public SpaceObjectType forName(final String name) {

		final Array<SpaceObjectType> objectTypes = getObjectTypes();

		for(final SpaceObjectType type : objectTypes.array()) {
			if(StringUtils.equals(type.getName(), name)) {
				return type;
			}
		}

		return null;
	}

	/**
	 * @return список доступных типов объектов.
	 */
	private Array<SpaceObjectType> getObjectTypes() {
		return objectTypes;
	}

	/**
	 * @param objectType регистрация типа объекта.
	 */
	public void register(final SpaceObjectType objectType) {
		final Array<SpaceObjectType> objectTypes = getObjectTypes();
		objectTypes.add(objectType);
		objectTypes.trimToSize();
	}
}

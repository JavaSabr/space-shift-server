package com.ss.server.manager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import rlib.concurrent.GroupThreadFactory;
import rlib.concurrent.executor.TaskExecutor;
import rlib.concurrent.task.SimpleTask;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.ServerThread;
import com.ss.server.executor.impl.AsyncPacketExecutor;
import com.ss.server.executor.impl.SyncPacketExecutor;

/**
 * Менеджер серверных потоков.
 * 
 * @author Ronn
 */
public final class ExecutorManager {

	/** логгер для менеджера */
	private static final Logger LOGGER = LoggerManager.getLogger(ExecutorManager.class);

	private static ExecutorManager instance;

	public static ExecutorManager getInstance() {

		if(instance == null) {
			instance = new ExecutorManager();
		}

		return instance;
	}

	/** исполнитель основных заданий */
	private final ScheduledExecutorService generalExecutor;
	/** исполнитель ид генераторов заданий */
	private final ScheduledExecutorService idGeneratorExecutor;
	/** исполнитель задач по AI */
	private final ScheduledExecutorService aiTaskExecutor;

	/** обработчик параллельных дейтсвий */
	private final ExecutorService executor;

	/** синхронизированный исполнитель пакетов */
	private final TaskExecutor<LocalObjects> syncPacketExecutor;
	/** асинхроный исполнитель клиентских пакетов */
	private final TaskExecutor<LocalObjects> asyncPacketExecutor;

	private ExecutorManager() {
		InitializeManager.valid(getClass());

		generalExecutor = Executors.newScheduledThreadPool(Config.THREAD_EXECUTOR_GENERAL_SIZE, new GroupThreadFactory("GeneralExecutor", ServerThread.class, Thread.NORM_PRIORITY));
		aiTaskExecutor = Executors.newScheduledThreadPool(2, new GroupThreadFactory("AiTaskExecutor", ServerThread.class, Thread.MIN_PRIORITY));
		idGeneratorExecutor = Executors.newSingleThreadScheduledExecutor();

		executor = Executors.newFixedThreadPool(2, new GroupThreadFactory("AsynThreadExecutor", ServerThread.class, Thread.MAX_PRIORITY));

		this.syncPacketExecutor = new SyncPacketExecutor();
		this.asyncPacketExecutor = AsyncPacketExecutor.create(2, 5);

		LOGGER.info("initialized.");
	}

	/**
	 * Выполнение задания в параллельном потоке.
	 * 
	 * @param runnable задание, которое надо выполнить.
	 */
	public void execute(final Runnable runnable) {
		executor.execute(runnable);
	}

	/**
	 * @return исполнитель для ид генераторов.
	 */
	public ScheduledExecutorService getIdGeneratorExecutor() {
		return idGeneratorExecutor;
	}

	/**
	 * Асинхронное выполнение задачи клиентского пакета.
	 * 
	 * @param task задача выполнения пакета.
	 */
	public void runAsyncPacket(final SimpleTask<LocalObjects> task) {
		asyncPacketExecutor.execute(task);
	}

	/**
	 * Синхронное выполнение задачи клиентского пакета.
	 * 
	 * @param task задача выполнения пакета.
	 */
	public void runSyncPacket(final SimpleTask<LocalObjects> task) {
		syncPacketExecutor.execute(task);
	}

	/**
	 * Создание периодическои исполняемой задачи по обработке задач AI.
	 * 
	 * @param runnable исполняемая задача.
	 * @param delay задержка перед первым запуском.
	 * @param interval интервал между выполнением задачи.
	 * @return ссылка на задачу.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleAiAtFixedRate(final T runnable, long delay, long interval) {

		try {

			if(interval < 0) {
				interval = 0;
			}

			if(delay < 0) {
				delay = 0;
			}

			return (ScheduledFuture<T>) aiTaskExecutor.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Создание отложенного общего таска.
	 * 
	 * @param runnable содержание таска.
	 * @param delay задержка перед выполнением.
	 * @return ссылка на таск.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneral(final T runnable, long delay) {
		try {

			if(delay < 0) {
				delay = 0;
			}

			return (ScheduledFuture<T>) generalExecutor.schedule(runnable, delay, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Создание периодического отложенного общего таска.
	 * 
	 * @param runnable содержание таска.
	 * @param delay задержка перед первым запуском.
	 * @param interval интервал между выполнением таска.
	 * @return ссылка на таск.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneralAtFixedRate(final T runnable, final long delay, long interval) {
		try {

			if(interval <= 0) {
				interval = 1;
			}

			return (ScheduledFuture<T>) generalExecutor.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}
}
package com.ss.server.manager;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.database.QuestDBManager;
import com.ss.server.document.DocumentQuest;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.action.QuestAction;
import com.ss.server.model.quest.action.QuestActionType;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventListener;
import com.ss.server.model.quest.event.QuestEventType;
import com.ss.server.model.quest.event.impl.AcceptedQuestEventListener;
import com.ss.server.model.quest.reward.QuestReward;
import com.ss.server.model.quest.reward.QuestRewardType;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Менеджер по работе квестов.
 * 
 * @author Ronn
 */
public final class QuestManager {

	private static final Logger LOGGER = LoggerManager.getLogger(QuestManager.class);

	public static final String ROOT_FOLDER_NAME = "quests";

	private static final QuestDBManager QUEST_DB_MANAGER = QuestDBManager.getInstance();

	private static QuestManager instance;

	public static QuestManager getInstance() {

		if(instance == null) {
			instance = new QuestManager();
		}

		return instance;
	}

	/** таблица квестов */
	private final Table<IntKey, Quest> table;

	private QuestManager() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		// регистрация действий от имени задания
		{
			final Array<Class<QuestAction>> container = ArrayFactory.newArray(Class.class);

			final ClassManager classManager = ClassManager.getInstance();
			classManager.findImplements(container, QuestAction.class);

			for(final Class<QuestAction> implementation : container) {
				QuestActionType.register(new QuestActionType(implementation.getSimpleName(), implementation));
			}
		}

		// регистрация типов наград заданий
		{
			final Array<Class<QuestReward>> container = ArrayFactory.newArray(Class.class);

			final ClassManager classManager = ClassManager.getInstance();
			classManager.findImplements(container, QuestReward.class);

			for(final Class<QuestReward> implementation : container) {
				QuestRewardType.register(new QuestRewardType(implementation.getSimpleName(), implementation));
			}
		}

		// регистрация реализаций слушателей событий заданий
		{
			final Array<Class<QuestEventListener>> container = ArrayFactory.newArray(Class.class);

			final ClassManager classManager = ClassManager.getInstance();
			classManager.findImplements(container, QuestEventListener.class);

			for(final Class<QuestEventListener> implementation : container) {
				QuestEventType.register(new QuestEventType(implementation.getSimpleName(), implementation));
			}
		}

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_DATA_PATH, ROOT_FOLDER_NAME), "xml");

		for(final File file : files) {

			final Array<Quest> quests = new DocumentQuest(file).parse();

			for(final Quest quest : quests) {

				if(table.containsKey(quest.getId())) {
					LOGGER.warning("found duplicate quest for id " + quest.getId());
				}

				table.put(quest.getId(), quest);
			}
		}

		LOGGER.info("loaded " + table.size() + " quests.");
	}

	/**
	 * Получение реализации квеста по ид.
	 * 
	 * @param id ид квеста.
	 * @return искомая реализация квеста.
	 */
	public Quest getQuest(final int id) {
		return table.get(id);
	}

	/**
	 * Запуск задания по указанному ид для указанного корабля игрока.
	 * 
	 * @param playerShip корабль игрока.
	 * @param local локальные объекты.
	 * @param questId ид запускаемого задания.
	 */
	public void startQuestTo(final PlayerShip playerShip, final LocalObjects local, final int questId) {

		final Quest quest = getQuest(questId);

		if(quest == null) {
			LOGGER.warning(new RuntimeException("not found quest " + questId));
			return;
		}

		final QuestEvent event = local.getNextQuestEvent();
		event.setEventType(AcceptedQuestEventListener.EVENT_TYPE);
		event.setPlayerShip(playerShip);
		event.setQuest(quest);

		quest.accept(event, local);
	}

	/**
	 * Обновление избранности у задания.
	 * 
	 * @param playerShip корабль игрока.
	 * @param objectId уникальный ид состояния.
	 * @param questId ид задания.
	 * @param favorite статус избранности.
	 */
	public void updateFavorite(final PlayerShip playerShip, final int objectId, final int questId, final boolean favorite) {

		final QuestList questList = playerShip.getQuestList();
		questList.asynLock();
		try {

			final QuestState state = questList.getQuestState(questId);

			if(state == null) {
				return;
			}

			state.setFavorite(favorite);

			QUEST_DB_MANAGER.updateQuest(playerShip, state);

		} finally {
			questList.asynUnlock();
		}
	}
}

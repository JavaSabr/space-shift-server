package com.ss.server.manager;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import rlib.concurrent.GroupThreadFactory;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.server.ServerThread;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.TimeActiveSkill;
import com.ss.server.task.impl.FinishActiveSkillTask;

/**
 * Реализация менеджера по обработке временно активных умений.
 * 
 * @author Ronn
 */
public class SkillActiveManager {

	protected static final Logger LOGGER = LoggerManager.getLogger(SkillActiveManager.class);

	private static SkillActiveManager instance;

	public static SkillActiveManager getInstance() {

		if(instance == null) {
			instance = new SkillActiveManager();
		}

		return instance;
	}

	/** исполнитель автовыключения умений */
	private final ScheduledExecutorService executor;

	private SkillActiveManager() {
		InitializeManager.valid(getClass());
		this.executor = Executors.newScheduledThreadPool(2, new GroupThreadFactory("ActiveSkillExecutor", ServerThread.class, Thread.NORM_PRIORITY));
	}

	/**
	 * Добавление умения в список автозавершаемых.
	 * 
	 * @param ship вызвавший корабль.
	 * @param skill завершаемое умение.
	 * @return ссылка на управление задачей.
	 */
	public ScheduledFuture<Void> execute(final SpaceShip ship, final TimeActiveSkill skill) {
		final ScheduledExecutorService executor = getExecutor();
		return executor.schedule(FinishActiveSkillTask.newTask(ship, skill), skill.getActiveTime(), TimeUnit.MILLISECONDS);
	}

	public ScheduledExecutorService getExecutor() {
		return executor;
	}
}

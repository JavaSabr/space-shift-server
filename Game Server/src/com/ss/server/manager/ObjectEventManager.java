package com.ss.server.manager;

import java.util.concurrent.atomic.AtomicReference;

import rlib.util.ArrayUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.listener.DestroyerListener;
import com.ss.server.model.listener.ItemTeleportedListener;
import com.ss.server.model.listener.PlayerEnteredListener;
import com.ss.server.model.listener.PlayerExitedListener;
import com.ss.server.model.listener.QuestAcceptedListener;
import com.ss.server.model.listener.QuestFinishedListener;
import com.ss.server.model.listener.SelectQuestButtonListener;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;

/**
 * Менеджер по события игровых объектов.
 * 
 * @author Ronn
 */
public class ObjectEventManager {

	private static final ObjectEventManager instance = new ObjectEventManager();

	public static ObjectEventManager getInstance() {
		return instance;
	}

	/** слушатели вхождения кораблей игроков в игру */
	private final Array<PlayerEnteredListener> playerEnteredListeners;
	/** слушатели выхода кораблей игроков из игры */
	private final Array<PlayerExitedListener> playerExitedListeners;

	/** слушатели взятий заданий */
	private volatile QuestAcceptedListener[] questAcceptedListeners;
	/** слушатели завершений заданий */
	private volatile QuestFinishedListener[] questFinishedListeners;
	/** слушатели нажатий на действия заданий */
	private volatile SelectQuestButtonListener[] selectQuestButtonListeners;
	/** слушатели телепортирования предметов */
	private volatile ItemTeleportedListener[] itemTeleportedListeners;
	/** слушатели уничтоженных объектов */
	private volatile DestroyerListener[] destroyerListeners;

	private ObjectEventManager() {
		this.playerEnteredListeners = ArrayFactory.newConcurrentAtomicArray(PlayerEnteredListener.class);
		this.playerExitedListeners = ArrayFactory.newConcurrentAtomicArray(PlayerExitedListener.class);
		this.questAcceptedListeners = new QuestAcceptedListener[0];
		this.selectQuestButtonListeners = new SelectQuestButtonListener[0];
		this.destroyerListeners = new DestroyerListener[0];
	}

	/**
	 * @param listener новый слушатель уничтожений объектов.
	 */
	public void addDestroyerListener(final DestroyerListener listener) {
		this.destroyerListeners = ArrayUtils.addToArray(destroyerListeners, listener, DestroyerListener.class);
	}

	/**
	 * @param listener новый слушатель телепортированных предметов.
	 */
	public void addItemTeleportedListener(final ItemTeleportedListener listener) {
		this.itemTeleportedListeners = ArrayUtils.addToArray(itemTeleportedListeners, listener, ItemTeleportedListener.class);
	}

	/**
	 * Добавление слушателя входа в мир кораблей игроков.
	 * 
	 * @param listener слушатель входа в мир кораблей игроков.
	 */
	public void addPlayerEnteredListener(final PlayerEnteredListener listener) {

		final Array<PlayerEnteredListener> listeners = getPlayerEnteredListeners();
		listeners.writeLock();
		try {
			listeners.add(listener);
		} finally {
			listeners.writeUnlock();
		}
	}

	/**
	 * Добавление слушателя выхода из мира кораблей игроков.
	 * 
	 * @param listener слушатель выхода из мира кораблей игроков.
	 */
	public void addPlayerExitedListener(final PlayerExitedListener listener) {

		final Array<PlayerExitedListener> listeners = getPlayerExitedListeners();
		listeners.writeLock();
		try {
			listeners.add(listener);
		} finally {
			listeners.writeUnlock();
		}
	}

	/**
	 * @param listener новый слушатель взятия заданий.
	 */
	public void addQuestAcceptedListeners(final QuestAcceptedListener listener) {
		this.questAcceptedListeners = ArrayUtils.addToArray(questAcceptedListeners, listener, QuestAcceptedListener.class);
	}

	/**
	 * @param listener новый слушатель завершения задания.
	 */
	public void addQuestFinishedListeners(final QuestFinishedListener listener) {
		this.questFinishedListeners = ArrayUtils.addToArray(questFinishedListeners, listener, QuestFinishedListener.class);
	}

	/**
	 * @param listener новый слушатель нажатий на действия заданий.
	 */
	public void addSelectQuestButtonListener(final SelectQuestButtonListener listener) {
		this.selectQuestButtonListeners = ArrayUtils.addToArray(selectQuestButtonListeners, listener, SelectQuestButtonListener.class);
	}

	/**
	 * @return слушатели уничтожения объектов.
	 */
	private DestroyerListener[] getDestroyerListeners() {
		return destroyerListeners;
	}

	/**
	 * @return слушатели телепортирования предметов.
	 */
	private ItemTeleportedListener[] getItemTeleportedListeners() {
		return itemTeleportedListeners;
	}

	/**
	 * @return слушатели вхождения кораблей игроков в игру.
	 */
	private Array<PlayerEnteredListener> getPlayerEnteredListeners() {
		return playerEnteredListeners;
	}

	/**
	 * @return слушатели выхода кораблей игроков из игры.
	 */
	private Array<PlayerExitedListener> getPlayerExitedListeners() {
		return playerExitedListeners;
	}

	/**
	 * @return слушатели взятия заданий.
	 */
	private QuestAcceptedListener[] getQuestAcceptedListeners() {
		return questAcceptedListeners;
	}

	/**
	 * @return слушатели завершений заданий.
	 */
	private QuestFinishedListener[] getQuestFinishedListeners() {
		return questFinishedListeners;
	}

	/**
	 * @return слушатели нажатий на действия заданий.
	 */
	private SelectQuestButtonListener[] getSelectQuestButtonListeners() {
		return selectQuestButtonListeners;
	}

	/**
	 * Уведомление о разрушении объекта указанным объектом.
	 * 
	 * @param object разрушенный объект.
	 * @param destroyer разрушитель.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyDestroyed(final SpaceObject object, final SpaceObject destroyer, final LocalObjects local) {

		if(object == null) {
			return;
		}

		final DestroyerListener[] listeners = getDestroyerListeners();

		if(listeners.length < 1) {
			return;
		}

		for(final DestroyerListener listener : listeners) {
			listener.notify(object, destroyer, local);
		}
	}

	/**
	 * Уведомление о том что корабль игрока зашел в мир.
	 * 
	 * @param playerShip корабль игрока.
	 */
	public void notifyEntered(final PlayerShip playerShip) {

		final Array<PlayerEnteredListener> listeners = getPlayerEnteredListeners();
		listeners.readLock();
		try {

			for(final PlayerEnteredListener listener : listeners.array()) {

				if(listener == null) {
					break;
				}

				listener.onEntered(playerShip);
			}

		} finally {
			listeners.readUnlock();
		}
	}

	/**
	 * Уведомление о том, что корабль игрока вышел из мира.
	 * 
	 * @param playerShip корабль игрока.
	 */
	public void notifyExited(final PlayerShip playerShip) {

		final Array<PlayerExitedListener> listeners = getPlayerExitedListeners();
		listeners.readLock();
		try {

			for(final PlayerExitedListener listener : listeners.array()) {

				if(listener == null) {
					break;
				}

				listener.onExited(playerShip);
			}

		} finally {
			listeners.readUnlock();
		}
	}

	/**
	 * Уведомление о добавлении предмета в хранилище указанного объекта.
	 * 
	 * @param object корабль, кому добавился предмет.
	 * @param item добавленный предмет.
	 */
	public void notifyItemAdded(final SpaceShip object, final SpaceItem item) {

	}

	/**
	 * Уведомление об телепортировании предмета в хранилище объекта.
	 * 
	 * @param object телепортировавший предмет.
	 * @param item телепортированный предмет.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyItemTeleported(final SpaceObject object, final SpaceItem item, final LocalObjects local) {

		final ItemTeleportedListener[] listeners = getItemTeleportedListeners();

		if(listeners == null || listeners.length < 1) {
			return;
		}

		for(final ItemTeleportedListener listener : listeners) {
			listener.notify(object, item, local);
		}
	}

	/**
	 * Уведомление о изминении предмета в хранилище указанного корабля.
	 * 
	 * @param object корабль, у которого изменился предмет.
	 * @param item предмет, который изменился.
	 */
	public void notifyItemUpdate(final SpaceShip object, final SpaceItem item) {

	}

	/**
	 * уведомление о том, что игрок взял новый квест.
	 * 
	 * @param ship корабль игрока.
	 * @param quest новый квест.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyQuestAccepted(final PlayerShip ship, final Quest quest, final LocalObjects local) {

		if(ship == null || quest == null) {
			return;
		}

		final QuestAcceptedListener[] listeners = getQuestAcceptedListeners();

		if(listeners.length < 1) {
			return;
		}

		for(final QuestAcceptedListener listener : listeners) {
			listener.notify(ship, quest, local);
		}
	}

	/**
	 * Уведомление о завершения задания.
	 * 
	 * @param ship корабль игрока.
	 * @param quest завершенное задание.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyQuestFinished(final PlayerShip ship, final Quest quest, final LocalObjects local) {

		if(ship == null || quest == null) {
			return;
		}

		final QuestFinishedListener[] listeners = getQuestFinishedListeners();

		if(listeners == null || listeners.length < 1) {
			return;
		}

		for(final QuestFinishedListener listener : listeners) {
			listener.notify(ship, quest, local);
		}
	}

	/**
	 * Уведомление о нажатии квестовой кнопки.
	 * 
	 * @param playerShip корабль игрока.
	 * @param questId ид задания.
	 * @param buttonId ид кнопки.
	 * @param local контейнер локальных объектов.
	 */
	public void notifySelectButton(final PlayerShip playerShip, final int questId, final int buttonId, final LocalObjects local) {

		if(playerShip == null) {
			return;
		}

		final QuestManager questManager = QuestManager.getInstance();
		final Quest quest = questManager.getQuest(questId);

		if(quest == null) {
			return;
		}

		final AtomicReference<SpaceStation> stationReference = playerShip.getStationReference();

		final QuestEvent event = local.getNextQuestEvent();
		event.setPlayerShip(playerShip);
		event.setQuest(quest);
		event.setStation(stationReference.get());

		final QuestButton button = quest.getButton(event, buttonId);

		if(button == null) {
			return;
		}

		final SelectQuestButtonListener[] listeners = getSelectQuestButtonListeners();

		if(listeners.length < 1) {
			return;
		}

		for(final SelectQuestButtonListener listener : listeners) {
			listener.notify(playerShip, button, local);
		}
	}

	/**
	 * Удаления слушателя входа в мир кораблей игроков.
	 * 
	 * @param listener слушатель входа в мир кораблей игроков.
	 */
	public void removePlayerEnteredListener(final PlayerEnteredListener listener) {

		final Array<PlayerEnteredListener> listeners = getPlayerEnteredListeners();
		listeners.writeLock();
		try {
			listeners.fastRemove(listener);
		} finally {
			listeners.writeUnlock();
		}
	}

	/**
	 * Удаление слушателя выхода из мира кораблей игроков.
	 * 
	 * @param listener слушатель выхода из мира кораблей игроков.
	 */
	public void removePlayerExitedListener(final PlayerExitedListener listener) {

		final Array<PlayerExitedListener> listeners = getPlayerExitedListeners();
		listeners.writeLock();
		try {
			listeners.fastRemove(listener);
		} finally {
			listeners.writeUnlock();
		}
	}
}

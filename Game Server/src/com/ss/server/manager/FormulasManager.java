package com.ss.server.manager;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.func.stat.StatFunc;
import com.ss.server.model.func.stat.StatType;
import com.ss.server.model.func.stat.impl.MathStatFunc;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.impact.ImpactType;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;

/**
 * Менеджер формул для различных рассчетов.
 * 
 * @author Ronn
 */
public class FormulasManager {

	private static final Logger LOGGER = LoggerManager.getLogger(FormulasManager.class);

	private static FormulasManager instance;

	public static FormulasManager getInstance() {

		if(instance == null) {
			instance = new FormulasManager();
		}

		return instance;
	}

	private final StatFunc ENGINE_ACCEL_FUNC = new MathStatFunc(StatType.FLY_ACCEL, null, env -> {

		final SpaceShip ship = env.owner;

		if(ship == null) {
			return;
		}

		env.value *= ship.getEngineEnergy();

	}, StatFunc.ORDER_60);

	private final StatFunc ENGINE_SPEED_FUNC = new MathStatFunc(StatType.MAX_CURRENT_FLY_SPEED, null, env -> {

		final SpaceShip ship = env.owner;

		if(ship == null) {
			return;
		}

		env.value *= ship.getEngineEnergy();

	}, StatFunc.ORDER_60);

	private final StatFunc ENGINE_CONSUME_FUNC = new MathStatFunc(StatType.ENGINE_CONSUME_ENERGY, null, env -> {

		final SpaceShip ship = env.owner;

		if(ship == null) {
			return;
		}

		env.value *= ship.getEngineEnergy();

	}, StatFunc.ORDER_60);

	public FormulasManager() {
		InitializeManager.valid(getClass());
		LOGGER.info("initialized.");
	}

	/**
	 * Добавление функций для космического корабля.
	 * 
	 * @param ship космический корабль.
	 */
	public void addFuncsTo(final SpaceShip ship) {
		ENGINE_ACCEL_FUNC.addMe(ship);
		ENGINE_CONSUME_FUNC.addMe(ship);
		ENGINE_SPEED_FUNC.addMe(ship);
	}

	/**
	 * Расчет нанесенного урона по цели.
	 * 
	 * @param local контейнр локальных объектов.
	 * @param damager атакующий корабль.
	 * @param skill умение которым был нанесен урон.
	 * @return результата атаки.
	 */
	public ImpactInfo calcDamage(final LocalObjects local, final SpaceShip damager, final Skill skill) {

		final ImpactInfo impactInfo = local.getNextImpactInfo();

		final float power = skill.getPower();

		impactInfo.setImpactType(ImpactType.DAMAGE);
		impactInfo.setValue((int) power);

		return impactInfo;
	}

	public ImpactInfo calcDamage(final SpaceShip damager, final Skill skill) {
		return calcDamage(LocalObjects.get(), damager, skill);
	}
}

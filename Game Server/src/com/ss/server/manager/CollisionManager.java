package com.ss.server.manager;

import java.util.concurrent.atomic.AtomicReference;

import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;

/**
 * Модель менеджера обработки и рассчета столкновений между космическими
 * объектами.
 * 
 * @author Ronn
 */
public final class CollisionManager {

	public static CollisionManager getInstance() {

		if(instance == null) {
			instance = new CollisionManager();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(CollisionManager.class);

	private static CollisionManager instance;

	private CollisionManager() {
		LOGGER.info("initializable.");
	}

	public boolean isCollision(final SpaceObject object, final SpaceObject target, final LocalObjects local, final VectorBuffer buffer) {

		if(object.isSpaceShip()) {

			if(target.isLocationObject()) {
				return isCollision(object.getSpaceShip(), target.getLocationObject(), local, buffer);
			} else if(target.isSpaceStation()) {
				return isCollision(object.getSpaceShip(), target.getSpaceStation(), local, buffer);
			}

		} else if(object.isSpaceStation()) {

			if(target.isSpaceShip()) {
				return isCollision(target.getSpaceShip(), object.getSpaceStation(), local, buffer);
			}
		}

		return false;
	}

	/**
	 * Проверка столкновения корабля с локационным объектом.
	 * 
	 * @param ship проверяемый корабль.
	 * @param object проверяемый объект.
	 * @param local контейнер локальных объектов.
	 * @return есть ли столкновение.
	 */
	public boolean isCollision(final SpaceShip ship, final LocationObject object, final LocalObjects local, final VectorBuffer buffer) {

		if(ship.isPlayerShip()) {
			return false;
		}

		final int distance = ship.getMaxSize() + object.getMaxSize();

		if(!ship.isInDistance(object, distance)) {
			return false;
		}

		final Bounding second = object.getBounding();
		return second.intersects(ship.getLocation(), ship.getDirection(), buffer);
	}

	/**
	 * Обработка пространственного взаимодействия между космическим кораблем и
	 * космической станцией.
	 * 
	 * @param ship космический корабль.
	 * @param station космическая станция.
	 * @param local контейнер локальных объектов.
	 * @return были ли вхождение корпуса корабля в корпус станции.
	 */
	public boolean isCollision(final SpaceShip ship, final SpaceStation station, final LocalObjects local, final VectorBuffer buffer) {

		if(ship.isPlayerShip()) {

			final PlayerShip playerShip = ship.getPlayerShip();

			final AtomicReference<SpaceStation> stationReference = playerShip.getStationReference();
			final SpaceStation currentStation = stationReference.get();

			final int warningRadius = station.getWarningRadius();
			final int teleportRadius = station.getTeleportRadius();

			// если игрок уже имеет возможность телепортироваться на станцию
			if(currentStation != null && currentStation == station) {

				// если он вышел из радиуса, вырубаем возможность
				// телепортироваться
				if(!playerShip.isInDistance(station, teleportRadius)) {
					playerShip.onExitStationRadius(station, local);
				}
				// если он уже начал подлетать слишком близко, надо предупредить
				// и забрать возможность телепортации
				else if(playerShip.isInDistance(station, warningRadius)) {
					station.showWarning(playerShip, local);
					playerShip.onExitStationRadius(station, local);
				}

			} else if(currentStation == null) {

				// если игрок не слишком близко и не слишком далеко, разрешаем
				// ему телепортироваться
				if(!playerShip.isInDistance(station, warningRadius) && playerShip.isInDistance(station, teleportRadius)) {
					playerShip.onEnterStationRadius(station, local);
				}
			}

			return false;
		}

		final Bounding first = station.getBounding();
		final Bounding second = ship.getBounding();

		return second.intersects(first, buffer);
	}

	/*
	 * 
	 * final Hangar[] hangars = station.getHangars();
	 * 
	 * if(hangars.length < 1) { return false; }
	 * 
	 * // если объект невидим if(!ship.isVisible()) {
	 * 
	 * for(int i = 0, length = hangars.length; i < length; i++) {
	 * 
	 * final Hangar hangar = hangars[i];
	 * 
	 * if(hangar.contains(ship)) { hangar.onExit(ship, local); return false; } }
	 * 
	 * return false; }
	 * 
	 * if(ship.isPlayerShip()) {
	 * 
	 * final Vector location = ship.getLocation(); int hangarCounts = 0;
	 * 
	 * for(int i = 0, length = hangars.length; i < length; i++) {
	 * 
	 * final Hangar hangar = hangars[i];
	 * 
	 * final boolean contains = hangar.contains(ship); final boolean inHangar =
	 * hangar.contains(location, local);
	 * 
	 * if(!contains && inHangar) { hangar.onEnter(ship, local); hangarCounts++;
	 * } else if(contains && !inHangar) { hangar.onExit(ship, local);
	 * hangarCounts--; } else if(contains) { hangarCounts++; } }
	 * 
	 * if(hangarCounts > 0) { return false; } }
	 * 
	 * final Bounding first = station.getBounding(); final Bounding second =
	 * ship.getBounding();
	 * 
	 * return second.intersects(first, buffer);
	 */
}

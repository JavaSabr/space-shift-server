package com.ss.server.manager;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.ArrayFactory;

import com.ss.server.Config;
import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.database.ItemDBManager;
import com.ss.server.database.ModuleDBManager;
import com.ss.server.database.PlayerDBManager;
import com.ss.server.database.QuestDBManager;
import com.ss.server.database.RatingDBManager;
import com.ss.server.document.DocumentFraction;
import com.ss.server.document.DocumentPlayerLevel;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.faction.Fraction;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerAction;
import com.ss.server.model.ship.player.action.PlayerActionHandler;
import com.ss.server.model.ship.player.action.PlayerActionHandlerFactory;
import com.ss.server.model.storage.impl.PlayerShipStorage;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseCreateShip;
import com.ss.server.network.game.packet.server.ResponseCreateShip.CreateShipResult;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Менеджер для работы с игроками.
 * 
 * @author Ronn
 */
public final class PlayerManager {

	private static final Logger LOGGER = LoggerManager.getLogger(PlayerManager.class);

	public static final String PVE_FRACTION_FOLDER = "pve-fraction";
	public static final String PVP_FRACTION_FOLDER = "pvp-fraction";

	/** лимит кораблей на 1 аккаунте */
	public static final int MAXIMUM_ACCOUNT_SIZE = 5;

	private static final PlayerDBManager PLAYER_DB_MANAGER = PlayerDBManager.getInstance();
	private static final ModuleDBManager MODULE_DB_MANAGER = ModuleDBManager.getInstance();
	private static final QuestDBManager QUEST_DB_MANAGER = QuestDBManager.getInstance();
	private static final RatingDBManager RATING_DB_MANAGER = RatingDBManager.getInstance();
	private static final ItemDBManager ITEM_DB_MANAGER = ItemDBManager.getInstance();

	private static final IdFactory ID_FACTORY = IdFactory.getInstance();

	private static final ArrayComparator<Fraction> FRACTION_COMPARATOR = (first, second) -> first.getId() - second.getId();

	private static PlayerManager instance;

	public static PlayerManager getInstance() {

		if(instance == null) {
			instance = new PlayerManager();
		}

		return instance;
	}

	/** список фракций */
	private final Fraction[] fractions;

	/** таблица опыта */
	private final long[] expTable;

	private PlayerManager() {
		InitializeManager.valid(getClass());

		this.expTable = fillExpTable();
		this.fractions = fillFractions();
	}

	/**
	 * Выполнение согласия на запрос игрока.
	 * 
	 * @param playerShip соглашающийся игрок.
	 * @param local контейнер локальных объектов.
	 */
	public void actionAccept(final PlayerShip playerShip, final LocalObjects local) {

		final AtomicReference<PlayerActionHandler> lastActionHandler = playerShip.getActionHandlerReference();
		final PlayerActionHandler handler = lastActionHandler.get();

		if(handler != null) {
			handler.handleAccept(local);
		}
	}

	/**
	 * Выполнение отмены запроса на действие.
	 * 
	 * @param playerShip отменяющий игрок.
	 * @param local контейнер локальных объектов.
	 */
	public void actionCancel(final PlayerShip playerShip, final LocalObjects local) {

		final AtomicReference<PlayerActionHandler> lastActionHandler = playerShip.getActionHandlerReference();
		final PlayerActionHandler handler = lastActionHandler.get();

		if(handler != null) {
			handler.handleCancel(local);
		}
	}

	/**
	 * Выполнение отказа от запроса игрока..
	 * 
	 * @param playerShip отказывающийся игрок.
	 * @param local контейнер локальных объектов.
	 */
	public void actionReject(final PlayerShip playerShip, final LocalObjects local) {

		final AtomicReference<PlayerActionHandler> lastActionHandler = playerShip.getActionHandlerReference();
		final PlayerActionHandler handler = lastActionHandler.get();

		if(handler != null) {
			handler.handleReject(local);
		}
	}

	/**
	 * Добавление опыта игроку.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param ship корабль игрока.
	 * @param exp опыт игрока.
	 */
	public void addExpTo(final LocalObjects local, final PlayerShip ship, final long exp) {

		final long[] expTable = getExpTable();

		if(ship.getLevel() >= getMaxLevel()) {
			return;
		}

		final long limit = expTable[expTable.length - 1];

		synchronized(ship) {

			final long current = ship.getExp();
			final long newExp = Math.min(current + exp, limit);

			ship.setExp(newExp);

			long toNextLevel = expTable[ship.getLevel() + 1];

			while(newExp > toNextLevel) {
				ship.increaseLevel(local);
				toNextLevel = expTable[ship.getLevel() + 1];
			}
		}
	}

	/**
	 * Создание нового корабля игрока.
	 * 
	 * @param client клиент игрока.
	 * @param accountName название аккаунта игрока.
	 * @param pilotName имя пилота корабля.
	 * @param fractionId ид фракции корабля.
	 */
	public synchronized void createPlayerShip(final GameClient client, final String accountName, final String pilotName, final int fractionId) {

		if(pilotName.length() < 3) {
			client.sendPacket(ResponseCreateShip.getInstance(CreateShipResult.INCORRECT_NAME));
			return;
		}

		final Fraction fraction = getFraction(fractionId);

		if(fraction == null) {
			client.sendPacket(ResponseCreateShip.getInstance(CreateShipResult.FRACTION_PROBLEMS), true);
			return;
		}

		final int currentSize = PLAYER_DB_MANAGER.getAccountSize(accountName);

		if(currentSize >= MAXIMUM_ACCOUNT_SIZE) {
			client.sendPacket(ResponseCreateShip.getInstance(CreateShipResult.LIMITED), true);
			return;
		}

		final PlayerShipTemplate template = fraction.getShipTemplate();

		if(template == null) {
			client.sendPacket(ResponseCreateShip.getInstance(CreateShipResult.EXCEPTION), true);
			return;
		}

		final PlayerShip playerShip = template.takeInstance(ID_FACTORY.getNextShipId()).getPlayerShip();
		playerShip.setName(pilotName);
		playerShip.setFraction(fraction);

		final LocalObjects local = LocalObjects.get();

		if(!PLAYER_DB_MANAGER.createPlayerShip(playerShip, accountName)) {
			client.sendPacket(ResponseCreateShip.getInstance(CreateShipResult.INCORRECT_NAME));
			return;
		}

		final RatingSystem ratingSystem = playerShip.getRatingSystem();
		ratingSystem.init();

		loadStorage(playerShip, local);

		fraction.addModulesTo(playerShip, local);
		fraction.moveSkillsToPanel(playerShip, local);
		fraction.addQuestsTo(playerShip, local);
		fraction.addItemsTo(playerShip, local);
		fraction.moveToStart(playerShip, local);

		playerShip.setCurrentStrength(Integer.MAX_VALUE, local);
		playerShip.setLevel(1);

		// установка начального распределения энергии
		playerShip.setEngineEnergy(1);
		playerShip.deleteMe(local);

		client.sendPacket(ResponseCreateShip.getInstance(CreateShipResult.SUCCESSFULL), true);
	}

	/**
	 * Удаление корабля у игрока.
	 * 
	 * @param accountName имя аккаунта.
	 * @param shipName название корабля.
	 */
	public synchronized boolean deletePlayerShip(final String accountName, final String shipName) {
		return PLAYER_DB_MANAGER.deletePlayerShip(accountName, shipName);
	}

	/**
	 * Заполнение таблицы опыта.
	 * 
	 * @return таблица опыта.
	 */
	private long[] fillExpTable() {

		int max = 1;

		final Map<Integer, Long> result = new DocumentPlayerLevel(new File(Config.FOLDER_DATA_PATH, "player_levels.xml")).parse();
		final Set<Integer> keys = result.keySet();

		for(final int key : keys) {
			max = Math.max(max, key);
		}

		final long[] expTable = new long[max + 1];

		for(final Integer key : keys) {
			expTable[key.intValue()] = result.get(key);
		}

		return expTable;
	}

	/**
	 * Подгрузка всех фракций.
	 */
	public Fraction[] fillFractions() {

		final Array<Fraction> result = ArrayFactory.newArray(Fraction.class);

		final String folderName = Config.SERVER_ONLY_PVP ? PVP_FRACTION_FOLDER : PVE_FRACTION_FOLDER;

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_DATA_PATH, folderName), "xml");

		for(final File file : files) {
			result.addAll(new DocumentFraction(file).parse());
		}

		for(final Iterator<Fraction> iterator = result.iterator(); iterator.hasNext();) {

			final Fraction fraction = iterator.next();

			if(!fraction.isReady()) {
				LOGGER.warning(new Exception("not correct loading fraction " + fraction));
				System.exit(1);
			}
		}

		result.sort(FRACTION_COMPARATOR);

		return result.toArray(new Fraction[result.size()]);
	}

	/**
	 * @return таблица опыта.
	 */
	public long[] getExpTable() {
		return expTable;
	}

	/**
	 * Получение фракции по ее ид.
	 * 
	 * @param id ид фракции.
	 * @return интерсуемая фракция.
	 */
	public Fraction getFraction(final int id) {

		for(final Fraction fraction : getFractions()) {
			if(fraction.getId() == id) {
				return fraction;
			}
		}

		return null;
	}

	/**
	 * @return список фракций.
	 */
	public Fraction[] getFractions() {
		return fractions;
	}

	/**
	 * @return максимальный уровень игрока.
	 */
	public int getMaxLevel() {
		final long[] expTable = getExpTable();
		return expTable.length - 1;
	}

	/**
	 * Прогрузка хранилища корабля игрока.
	 * 
	 * @param playerShip корабль игрока, чье хранилище надо подгрузить.
	 * @param local контейнер локальных объектов.
	 */
	private void loadStorage(final PlayerShip playerShip, final LocalObjects local) {

		PlayerShipStorage storage = PLAYER_DB_MANAGER.loagStorage(playerShip);

		if(storage == null) {
			storage = PlayerShipStorage.newInstance(playerShip);
			PLAYER_DB_MANAGER.addNewStorage(storage);
		}

		playerShip.setStorage(storage);

		final Array<SpaceItem> itemList = local.getNextItemList();

		ITEM_DB_MANAGER.loadStorageItems(playerShip.getObjectId(), itemList);

		for(final SpaceItem item : itemList.array()) {

			if(item == null) {
				break;
			}

			storage.setItem(item, item.getIndex());
		}
	}

	/**
	 * Обработка запроса на действие от игрока.
	 * 
	 * @param actionType тип действия.
	 * @param playerShip корабль игрока.
	 * @param object целевой объект.
	 * @param local контейнер локальных объектов.
	 */
	public void requestAction(final PlayerAction actionType, final PlayerShip playerShip, final SpaceObject object, final LocalObjects local) {

		if(!actionType.checkArguments(playerShip, object, local)) {
			return;
		}

		final PlayerActionHandler handler = PlayerActionHandlerFactory.takeHandler(actionType);
		handler.handleRequest(playerShip, object, local);
	}

	/**
	 * Загрузка корабля игрока по имени аккаунта и уникальному ид корабля.
	 * 
	 * @param accountName имя аккаунта.
	 * @param objectId уникальный ид корабля.
	 * @param local контейнер локальных объектов.
	 * @return восстановленный корабль.
	 */
	public PlayerShip restorePlayerShip(final String accountName, final int objectId, final LocalObjects local) {

		final PlayerShip playerShip = PLAYER_DB_MANAGER.restorePlayerShip(accountName, objectId, local);

		if(playerShip == null) {
			return null;
		}

		loadStorage(playerShip, local);

		PLAYER_DB_MANAGER.loadSkillStates(playerShip);
		MODULE_DB_MANAGER.loadModules(playerShip.getModuleSystem(), local, objectId);
		PLAYER_DB_MANAGER.loadInterface(objectId, playerShip.getInterfaceInfo());
		PLAYER_DB_MANAGER.loadSkillPanels(objectId, playerShip, playerShip.getSkillPanelInfo());
		PLAYER_DB_MANAGER.loadItemPanels(objectId, playerShip, playerShip.getItemPanelInfo());
		QUEST_DB_MANAGER.loadQuests(objectId, playerShip.getQuestList());

		RATING_DB_MANAGER.loadWeaponRatings(playerShip);
		RATING_DB_MANAGER.loadShieldRatings(playerShip);
		RATING_DB_MANAGER.loadDestroyRatings(playerShip);
		RATING_DB_MANAGER.loadLocalRatings(playerShip);

		return playerShip;
	}
}

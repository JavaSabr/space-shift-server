package com.ss.server.manager;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.ClassUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.model.ship.player.Command;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Менеджер обработки введенных команд в чат.
 * 
 * @author Ronn
 */
public class CommandManager {

	private static final String COMMAND_PREFIX = "/";

	private static final Logger LOGGER = LoggerManager.getLogger(CommandManager.class);

	private static CommandManager instance;

	public static CommandManager getInstance() {

		if(instance == null) {
			instance = new CommandManager();
		}

		return instance;
	}

	/** таблица обработчиков команд */
	private final Table<String, Command> commands;

	private CommandManager() {
		InitializeManager.valid(getClass());
		commands = TableFactory.newObjectTable();

		prepare();

		LOGGER.info("loaded " + commands.size() + " chat commands.");
	}

	/**
	 * Выполнение команды.
	 * 
	 * @param ship корабль игрока, ввевшего команду.
	 * @param message сообщение, которое он ввел.
	 */
	public void execute(final PlayerShip ship, final String message) {

		final String[] values = message.split(" ", 2);
		final Command command = commands.get(values[0]);

		// FIXME && command.getAccessLevel() <= ship.getAccessLevel()
		if(command != null) {
			command.execute(ship, values.length > 1 ? values[1] : null);
		}
	}

	/**
	 * Определние, является ли сообщение командой чата.
	 * 
	 * @param message сообщение от игрока.
	 * @return является ли это командой.
	 */
	public boolean isCommand(final String message) {
		return message.startsWith(COMMAND_PREFIX);
	}

	/**
	 * Поиск и регистрация всех реализованых команд.
	 */
	private void prepare() {

		final ClassManager manager = ClassManager.getInstance();
		final Array<Class<Command>> classes = ArrayFactory.newArray(Class.class);

		manager.findImplements(classes, Command.class);

		for(final Class<Command> cs : classes) {
			register(cs);
		}
	}

	/**
	 * Регистрация команды в менеджер.
	 * 
	 * @param cs класс команды.
	 */
	public void register(final Class<Command> cs) {

		Command command = ClassUtils.newInstance(cs);

		if(command == null) {
			LOGGER.warning("can;t create command for class " + cs);
			return;
		}

		if(commands.containsKey(command.getName())) {
			LOGGER.warning("found duplicate command " + command);
		}

		commands.put(COMMAND_PREFIX + command.getName(), command);
	}
}

package com.ss.server.manager;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.ShipAI;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.Skill;
import com.ss.server.network.game.packet.server.ResponseObjectFriendStatus;

/**
 * Менеджер по события игровых объектов.
 * 
 * @author Ronn
 */
public class ShipEventManager {

	private static final ShipEventManager instance = new ShipEventManager();

	public static ShipEventManager getInstance() {
		return instance;
	}

	private ShipEventManager() {
	}

	/**
	 * Уведомление о том, что корабль был добавлен/удален в агро лист НПС.
	 * 
	 * @param nps НПС, который добавил в агро лист корабль.
	 * @param ship корабль, который был добавлен в агро лист к НПС.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyAggro(final Nps nps, final SpaceShip ship, final LocalObjects local) {
		if(ship.isPlayerShip()) {
			ship.sendPacket(ResponseObjectFriendStatus.getInstance(ship.getPlayerShip(), nps, local), true);
		}
	}

	/**
	 * Уведомление об атаке кораблем корабля.
	 * 
	 * @param attacker атакующий.
	 * @param attacked атакуемый.
	 * @param skill атакующее умение.
	 * @param info информация об атаке.
	 */
	public void notifyAttack(final SpaceShip attacker, final SpaceShip attacked, final Skill skill, final ImpactInfo info, final LocalObjects local) {

		ShipAI ai = attacker.getAI();
		ai.notifyAttack(attacked, skill, info, local);

		ai = attacked.getAI();
		ai.notifyAttacked(attacker, skill, info, local);
	}

	/**
	 * Уведомление о изминении текущего значения энергии корабля.
	 * 
	 * @param ship корабль, у которого изменилось кол-во энергии.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyEnergyChanged(final SpaceShip ship, final LocalObjects local) {
	}

	/**
	 * Уведомление о смене распределения энергии на корабле.
	 * 
	 * @param engine старое значение распределение на энергию.
	 * @param ship корабль с измененным распределением энергии.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyEngineEnergyChanged(final float engine, final SpaceShip ship, final LocalObjects local) {
		ship.updateInfo(local);
	}
}

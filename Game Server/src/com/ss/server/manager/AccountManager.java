package com.ss.server.manager;

import java.util.function.Consumer;

import rlib.concurrent.util.ThreadUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.SafeTask;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.LocalObjects;
import com.ss.server.ServerThread;
import com.ss.server.model.impl.Account;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseAuthSuccesfull;
import com.ss.server.network.game.packet.server.ResponseServerTime;

/**
 * Менеджер для работы с аккаунтами пользователей.
 * 
 * @author Ronn
 */
public final class AccountManager implements SafeTask {

	private static final Logger LOGGER = LoggerManager.getLogger(AccountManager.class);

	private static AccountManager instance;

	public static AccountManager getInstance() {

		if(instance == null) {
			instance = new AccountManager();
		}

		return instance;
	}

	/** таблица ожидающих авторизации клиентов */
	private final Table<IntKey, GameClient> waitAuth;

	private AccountManager() {
		InitializeManager.valid(getClass());

		this.waitAuth = TableFactory.newConcurrentIntegerTable();

		final ServerThread thread = new ServerThread(this);
		thread.setName(getClass().getSimpleName() + "-checker");
		thread.setDaemon(true);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();
	}

	/**
	 * Добавление в ожидание на авторизацию клиента.
	 * 
	 * @param id ид ожидания.
	 * @param client ожидающий клиент.
	 */
	public void addWaitClient(final int id, final GameClient client) {
		waitAuth.put(id, client);
	}

	/**
	 * Выполнить завершение авторизации.
	 * 
	 * @param waitId ид авторизации.
	 * @param name имя аккаунта.
	 * @param password пароль аккаунта.
	 * @param correctly прошел ли аккаунт проверку.
	 */
	public void executeAuth(final int waitId, final String name, final String password, final boolean correctly) {

		final Table<IntKey, GameClient> waitAuth = getWaitAuth();
		waitAuth.writeLock();
		try {

			final GameClient client = waitAuth.get(waitId);

			if(client == null) {
				return;
			}

			if(!correctly) {
				client.close();
				return;
			}

			final Account account = Account.newInstance(name, password, client);

			client.setAccount(account);
			client.sendPacket(ResponseAuthSuccesfull.getInstance());
			client.setAuthed(true);

			waitAuth.remove(client.getWaitId());

			LOGGER.info("authed " + name);

		} finally {
			waitAuth.writeUnlock();
		}
	}

	/**
	 * @return таблица ожидающих авторизации аккаунтов.
	 */
	private Table<IntKey, GameClient> getWaitAuth() {
		return waitAuth;
	}

	/**
	 * Удаление ожидающего авторизации клиента.
	 * 
	 * @param client удаляемый клиент.
	 */
	public void removeWaitClient(final GameClient client) {
		waitAuth.remove(client.getWaitId());
	}

	@Override
	public void runImpl() {

		final LocalObjects local = LocalObjects.get();
		final Table<IntKey, GameClient> waitAuth = getWaitAuth();
		final Consumer<GameClient> consumer = client -> client.sendPacket(ResponseServerTime.getInstance(local), true);

		while(true) {

			ThreadUtils.sleep(60000);

			if(waitAuth.isEmpty()) {
				continue;
			}

			waitAuth.readLock();
			try {
				waitAuth.forEach(consumer);
			} finally {
				waitAuth.readUnlock();
			}
		}
	}
}

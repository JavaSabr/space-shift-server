package com.ss.server.manager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.ArrayFactory;
import rlib.util.random.Random;
import rlib.util.random.RandomFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.document.DocumentLocation;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.impl.Space;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.location.LocationInfo;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.table.LocationObjectTable;
import com.ss.server.template.LocationObjectTemplate;

/**
 * Менеджер работы локаций.
 * 
 * @author Ronn
 */
public final class LocationManager {

	private static final Logger LOGGER = LoggerManager.getLogger(LocationManager.class);

	public static final String PVE_LOCATIONS_FOLDER = "pve-locations";
	public static final String PVP_LOCATIONS_FOLDER = "pvp-locations";

	private static final int TARGET_LOCATION = 0;
	private static final int OBJECTS_LIMIT = 2000;
	private static final int MIN_X = -22000;
	private static final int MAX_X = 22000;
	private static final int MIN_Y = -3000;
	private static final int MAX_Y = 3000;
	private static final int MIN_Z = -22000;
	private static final int MAX_Z = 22000;

	private static final int[] OBJECT_IDS = ArrayFactory.toIntegerArray(1, 2, 3, 4, 5, 3, 4, 5, 3, 4, 5, 3, 4, 5, 3, 4, 5, 3, 4, 5);

	private static final ArrayComparator<SpaceLocation> COMPARATOR = (first, second) -> first.getId() - second.getId();

	private static LocationManager instance;

	public static LocationManager getInstance() {

		if(instance == null) {
			instance = new LocationManager();
		}

		return instance;
	}

	/** таблица локаций */
	private final Table<IntKey, LocationInfo> table;

	@SuppressWarnings("unused")
	private LocationManager() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final LocalObjects local = LocalObjects.get();

		final String folderName = Config.SERVER_ONLY_PVP ? PVP_LOCATIONS_FOLDER : PVE_LOCATIONS_FOLDER;

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_DATA_PATH, folderName), "xml");

		final Array<LocationInfo> check = ArrayFactory.newArray(LocationInfo.class);
		final Array<LocationInfo> result = ArrayFactory.newArray(LocationInfo.class);

		for(final File file : files) {
			result.addAll(new DocumentLocation(file).parse());
		}

		final Array<SpaceLocation> locations = ArrayFactory.newArray(SpaceLocation.class);

		for(final LocationInfo info : result) {

			if(check.contains(info)) {
				LOGGER.warning("found duplicate for id " + info.getId());
			}

			check.add(info);

			locations.add(new SpaceLocation(info.getName(), info.getId(), info.getSectorsX(), info.getSectorsY(), info.getSectorsZ()));
		}

		locations.sort(COMPARATOR);

		final Space space = Space.getInstance();

		for(int i = 0, length = locations.size(); i < length; i++) {

			final SpaceLocation location = locations.get(i);

			if(i != location.getId()) {
				throw new IllegalArgumentException("incorrect sequence of locations.");
			}

			space.addLocation(location);
		}

		final long currentTime = System.currentTimeMillis();

		for(final LocationInfo info : result) {

			final SpaceLocation location = space.getLocation(info.getId());

			final GravityObject[] gravity = info.getGravity();
			final LocationObject[] objects = info.getObjects();

			for(final GravityObject object : gravity) {

				location.addGravity(object);

				object.setLocationId(info.getId());
				object.spawnMe(local);
				object.updateGravity(currentTime, local);
			}

			for(final LocationObject object : objects) {

				object.setLocationId(location.getId());
				object.spawnMe(local);

				location.addLocationObject(object);
			}

			table.put(info.getId(), info);

			LOGGER.info("spawn " + gravity.length + " gravity objects and " + objects.length + " location objects for location " + info.getId());
		}

		LOGGER.info("loaded " + check.size() + " locations.");

		if(false) {
			generation(local);
		}
	}

	protected void generation(final LocalObjects local) {

		final IdFactory idFactory = IdFactory.getInstance();

		final Space space = Space.getInstance();
		final SpaceLocation location = space.getLocation(TARGET_LOCATION);
		final LocationObjectTable objectTable = LocationObjectTable.getInstance();

		if(location == null) {
			LOGGER.info("not found location from id " + TARGET_LOCATION);
		}

		final Array<SpaceObject> objects = ArrayFactory.newArray(SpaceObject.class);
		objects.addAll(location.getLocationObjects());
		objects.addAll(location.getGravity());

		final Array<LocationObject> result = ArrayFactory.newArray(LocationObject.class);

		final Random random = RandomFactory.newFastRandom();

		final Vector position = Vector.newInstance();
		final Vector offset = Vector.newInstance(6500, 0, 0);
		final Rotation rotation = Rotation.newInstance();

		for(int i = 0, length = OBJECTS_LIMIT; i < length; i++) {

			final int templateId = OBJECT_IDS[random.nextInt(OBJECT_IDS.length - 1)];

			final LocationObjectTemplate template = objectTable.getTemplate(templateId);

			if(template == null) {
				LOGGER.warning("not dound template from id " + templateId);
				continue;
			}

			final LocationObject object = template.takeInstance(idFactory.getNextLocationId());

			boolean ready = true;

			for(int d = 0, count = 10000; d < count; d++) {

				int diff = MAX_X - MIN_X;

				int x = random.nextInt(MIN_X + diff, MAX_X + diff) - diff;

				diff = MAX_Y - MIN_Y;

				int y = random.nextInt(MIN_Y, MAX_Y);

				diff = MAX_Z - MIN_Z;

				int z = random.nextInt(MIN_Z, MAX_Z);

				if(random.chance(50)) {
					x *= -1;
				}

				if(random.chance(50)) {
					y *= -1;
				}

				if(random.chance(50)) {
					z *= -1;
				}

				position.setXYZ(x, y, z);
				position.addLocal(offset);

				if(!location.valid(position)) {
					continue;
				}

				if(objects.isEmpty()) {
					ready = true;
				} else {

					for(final SpaceObject around : objects.array()) {

						if(around == null) {
							break;
						}

						if(!(around.isSpaceStation() || around.isLocationObject())) {
							continue;
						}

						final int objectMaxSize = object.getMaxSize();
						final int aroundMaxSize = around.getMaxSize();

						final float distanceTo = around.distanceTo(position);

						if(around.isSpaceStation() && distanceTo < (objectMaxSize + aroundMaxSize + 1500)) {
							ready = false;
							break;
						} else if(around.isLocationObject() && around.distanceTo(position) < (objectMaxSize + aroundMaxSize + 500)) {
							ready = false;
							break;
						}
					}
				}

				if(ready) {
					break;
				}
			}

			if(!ready) {
				continue;
			}

			rotation.random(random);

			object.setLocationId(location.getId());
			object.setLocation(position, Vector.ZERO, local);
			object.setRotation(rotation, local);
			object.spawnMe(local);

			objects.add(object);

			result.add(object);
		}

		LOGGER.info("generated " + result.size() + " objects.");

		if(result.isEmpty()) {
			return;
		}

		final StringBuilder xml = new StringBuilder("<template id=\"" + location.getId() + "\" name=\"" + location.getName() + "\">\n");
		xml.append("	<objects>\n");

		for(final LocationObject object : result) {

			Vector loc = object.getLocation();

			xml.append("		<object id=\"" + object.getTemplateId() + "\" position=\"" + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "\" mult=\"1\" random=\"true\" />\n");
		}

		xml.append("	</objects>\n");
		xml.append("</template>");

		try(BufferedWriter writer = Files.newBufferedWriter(Paths.get("./objects.xml"))) {
			writer.write(xml.toString());
		} catch(final IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param locationId ид локации.
	 * @return инфа об локации.
	 */
	public LocationInfo getLocationInfo(final int locationId) {
		return table.get(locationId);
	}
}

package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.shop.ItemPriceInfo;
import com.ss.server.model.shop.ItemPriceList;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Парсер модулей с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentItemShopPriceList extends AbstractFileDocument<Array<ItemPriceList>> {

	public static final String ATTR_SELL = "sell";
	public static final String ATTR_BUY = "buy";
	public static final String ATTR_ID = "id";

	public static final String NODE_PRICE = "price";
	public static final String NODE_ITEM = "item";
	public static final String NODE_ROOT = "list";

	public DocumentItemShopPriceList(final File file) {
		super(file);
	}

	@Override
	protected Array<ItemPriceList> create() {
		return ArrayFactory.newArray(ItemPriceList.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(NODE_ROOT.equals(child.getNodeName())) {
				parseList(child);
			}
		}
	}

	private void parseInfo(final ItemPriceList list, final Node node) {

		final ItemTable itemTable = ItemTable.getInstance();
		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_ITEM.equals(child.getNodeName())) {

				vars.parse(child);

				final int itemId = vars.getInteger(ATTR_ID);
				final int buy = vars.getInteger(ATTR_BUY, -1);
				final int sell = vars.getInteger(ATTR_SELL, -1);

				if(buy == -1 && sell == -1) {
					LOGGER.warning("incorrect sell or buy value for id " + itemId);
					continue;
				}

				final ItemTemplate template = itemTable.getTemplate(itemId);

				if(template == null) {
					LOGGER.warning("not found item template for " + itemId);
					continue;
				}

				list.addItem(new ItemPriceInfo(template, buy, sell));
			}
		}
	}

	private void parseList(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_PRICE.equals(child.getNodeName())) {

				vars.parse(child);

				final ItemPriceList priceList = new ItemPriceList(vars.getInteger(ATTR_ID));
				parseInfo(priceList, child);
				result.add(priceList);
			}
		}
	}
}

package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.template.LocationObjectTemplate;

/**
 * Парсер шаблонов локационных объктов с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentLocationObject extends AbstractFileDocument<Array<LocationObjectTemplate>> {

	private static final String NODE_TEMPLATE = "template";
	private static final String NODE_ROOT = "list";

	public DocumentLocationObject(final File file) {
		super(file);
	}

	@Override
	protected Array<LocationObjectTemplate> create() {
		return ArrayFactory.newArray(LocationObjectTemplate.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(NODE_ROOT.equals(child.getNodeName())) {
				parseTemplate(child);
			}
		}
	}

	private void parseTemplate(final Node node) {
		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_TEMPLATE.equals(child.getNodeName())) {
				result.add(new LocationObjectTemplate(VarTable.newInstance(child)));
			}
		}
	}
}

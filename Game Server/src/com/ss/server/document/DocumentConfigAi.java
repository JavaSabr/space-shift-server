package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.ai.nps.ConfigAi;

/**
 * Парсер конфига AI Nps с хмл.
 * 
 * @author Ronn
 */
public class DocumentConfigAi extends AbstractFileDocument<Array<ConfigAi>> {

	private static final String NODE_CONFIG = "config";
	private static final String NODE_LIST = "list";

	public DocumentConfigAi(final File file) {
		super(file);
	}

	@Override
	protected Array<ConfigAi> create() {
		return ArrayFactory.newArray(ConfigAi.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node node = document.getFirstChild(); node != null; node = node.getNextSibling()) {

			if(node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_LIST.equals(node.getNodeName())) {
				for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

					if(child.getNodeType() != Node.ELEMENT_NODE) {
						continue;
					}

					if(NODE_CONFIG.equals(child.getNodeName())) {
						result.add(new ConfigAi(child));
					}
				}
			}
		}
	}
}

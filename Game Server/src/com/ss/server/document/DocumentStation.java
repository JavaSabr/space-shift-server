package com.ss.server.document;

import java.io.File;
import java.lang.reflect.Constructor;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.ClassUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.manager.ClassManager;
import com.ss.server.model.station.module.StationModule;
import com.ss.server.template.HangarTemplate;
import com.ss.server.template.StationTemplate;

/**
 * Парсер станций с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentStation extends AbstractFileDocument<Array<StationTemplate>> {

	private static final String ATTR_NAME = "name";

	private static final String NODE_MODULE = "module";
	private static final String NODE_MODULES = "modules";
	private static final String NODE_HANGARS = "hangars";
	private static final String NODE_TEMPLATE = "template";
	private static final String NODE_ROOT = "list";

	public DocumentStation(final File file) {
		super(file);
	}

	@Override
	protected Array<StationTemplate> create() {
		return ArrayFactory.newArray(StationTemplate.class);
	}

	@Override
	protected void parse(final Document document) {

		final VarTable vars = VarTable.newInstance();

		for(Node root = document.getFirstChild(); root != null; root = root.getNextSibling()) {

			if(root.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!NODE_ROOT.equals(root.getNodeName())) {
				continue;
			}

			parse(vars, root);
		}
	}

	/**
	 * Парсинг списка шаблонов станций.
	 */
	protected void parse(final VarTable vars, Node root) {

		final ClassManager classManager = ClassManager.getInstance();

		for(Node node = root.getFirstChild(); node != null; node = node.getNextSibling()) {

			if(node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!NODE_TEMPLATE.equals(node.getNodeName())) {
				continue;
			}

			vars.parse(node);

			final StationTemplate template = new StationTemplate(vars);
			final Array<HangarTemplate> hangars = ArrayFactory.newArray(HangarTemplate.class);

			for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

				if(child.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				if(NODE_HANGARS.equals(child.getNodeName())) {
					parseHangars(hangars, child);
				} else if(NODE_MODULES.equals(child.getNodeName())) {
					parseModules(vars, classManager, template, child);
				}
			}

			template.setHangars(hangars.toArray(new HangarTemplate[hangars.size()]));

			result.add(template);
		}
	}

	/**
	 * Парсинг модулей станции.
	 */
	protected void parseModules(final VarTable vars, final ClassManager classManager, final StationTemplate template, Node parent) {

		for(Node node = parent.getFirstChild(); node != null; node = node.getNextSibling()) {

			if(node.getNodeType() != Node.ELEMENT_NODE || !NODE_MODULE.equals(node.getNodeName())) {
				continue;
			}

			vars.parse(node);

			final String moduleName = vars.getString(ATTR_NAME);
			final Class<StationModule> moduleClass = classManager.findImplements(moduleName, StationModule.class);

			if(moduleClass == null) {
				LOGGER.warning("not found station module class for name " + moduleName);
				continue;
			}

			final Constructor<StationModule> constructor = ClassUtils.getConstructor(moduleClass, StationTemplate.class, Node.class);

			if(constructor == null) {
				LOGGER.warning("not found constructor for module class " + moduleClass);
				continue;
			}

			final StationModule module = ClassUtils.newInstance(constructor, template, node);

			if(module == null) {
				LOGGER.warning("can't create module for class " + moduleClass);
				continue;
			}

			template.addModule(module);
		}
	}

	/**
	 * Парсинг ангаров станций.
	 */
	protected void parseHangars(final Array<HangarTemplate> hangars, Node child) {
		for(Node hangar = child.getFirstChild(); hangar != null; hangar = hangar.getNextSibling()) {

			if(hangar.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			hangars.add(new HangarTemplate(hangar));
		}
	}
}

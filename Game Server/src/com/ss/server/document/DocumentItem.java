package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.template.item.CommonItemTemplate;
import com.ss.server.template.item.ItemTemplate;

/**
 * Парсер шаблонов космических предметов с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentItem extends AbstractFileDocument<Array<ItemTemplate>> {

	private static final String NODE_COMMON = "common";
	private static final String NODE_ROOT = "list";

	public DocumentItem(final File file) {
		super(file);
	}

	@Override
	protected Array<ItemTemplate> create() {
		return ArrayFactory.newArray(ItemTemplate.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(NODE_ROOT.equals(child.getNodeName())) {
				parseTemplate(child);
			}
		}
	}

	private void parseCommon(final Node node) {
		final VarTable vars = VarTable.newInstance(node);
		result.add(new CommonItemTemplate(vars, node));
	}

	private void parseTemplate(final Node node) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			final String name = child.getNodeName();

			if(NODE_COMMON.equals(name)) {
				parseCommon(child);
			}
		}
	}
}

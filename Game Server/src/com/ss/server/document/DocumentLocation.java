package com.ss.server.document;

import java.awt.Color;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.location.BackgroundInfo;
import com.ss.server.model.location.LightInfo;
import com.ss.server.model.location.LocationInfo;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.table.GravityObjectTable;
import com.ss.server.table.LocationObjectTable;
import com.ss.server.table.StationTable;
import com.ss.server.template.GravityObjectTemplate;
import com.ss.server.template.LocationObjectTemplate;
import com.ss.server.template.StationTemplate;

/**
 * Парсер локаций с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentLocation extends AbstractFileDocument<Array<LocationInfo>> {

	private static final String LOCATION_OBJECT_NODE = "object";
	private static final String OBJECTS_NODE = "objects";
	private static final String LIGHT_NODE = "light";
	private static final String BACKGROUND_NODE = "background";
	private static final String LIGHTS_NODE = "lights";
	private static final String STATION_NODE = "station";
	private static final String GRAVITY_NODE = "gravity";
	private static final String TEMPLATE_NODE = "template";
	private static final String ROOT_NODE = "list";

	/** парсер даты */
	private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	/** множество уникальных ид гравитационных объектов */
	private static final Array<Integer> OBJECT_ID_GRAVITY_SET = ArrayFactory.newArraySet(Integer.class);
	/** множество уникальных ид станций */
	private static final Array<Integer> OBJECT_ID_STATION_SET = ArrayFactory.newArraySet(Integer.class);

	public DocumentLocation(final File file) {
		super(file);
	}

	@Override
	protected Array<LocationInfo> create() {
		return ArrayFactory.newArray(LocationInfo.class);
	}

	@Override
	protected void parse(final Document document) {

		final LocalObjects local = LocalObjects.get();
		final VarTable vars = VarTable.newInstance();

		for(Node node = document.getFirstChild(); node != null; node = node.getNextSibling()) {

			if(node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(ROOT_NODE.equals(node.getNodeName())) {
				for(Node temp = node.getFirstChild(); temp != null; temp = temp.getNextSibling()) {

					if(temp.getNodeType() != Node.ELEMENT_NODE) {
						continue;
					}

					if(TEMPLATE_NODE.equals(temp.getNodeName())) {

						vars.parse(temp);

						final Array<GravityObject> gravity = ArrayFactory.newArray(GravityObject.class);
						final Array<LocationObject> locationObjects = ArrayFactory.newArray(LocationObject.class);
						final Array<LightInfo> lights = ArrayFactory.newArray(LightInfo.class);

						BackgroundInfo backgroundInfo = null;
						Vector vectorStar = null;

						for(Node child = temp.getFirstChild(); child != null; child = child.getNextSibling()) {

							if(child.getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}

							if(GRAVITY_NODE.equals(child.getNodeName())) {

								final GravityObject instance = parseGravity(child);

								if(instance == null) {
									continue;
								}

								gravity.add(instance);
								parseGravity(instance, child);

							} else if(STATION_NODE.equals(child.getNodeName())) {

								final SpaceStation instance = parseStation(child);

								if(instance == null) {
									continue;
								}

								gravity.add(instance);
								parseStation(instance, child);

							} else if(LIGHTS_NODE.equals(child.getNodeName())) {
								vectorStar = parseLights(child, vars, lights);
							} else if(BACKGROUND_NODE.equals(child.getNodeName())) {
								backgroundInfo = new BackgroundInfo(VarTable.newInstance(child, "key", "type", "value"));
							} else if(OBJECTS_NODE.equals(child.getNodeName())) {
								parseLocationObjects(child, locationObjects, local);
							}
						}

						if(backgroundInfo == null) {
							throw new NullPointerException("not found background info for location " + vars.getString(LocationInfo.PROP_NAME) + " in file " + stream + ".");
						}

						if(vectorStar == null) {
							throw new NullPointerException("not found vector star for location " + vars.getString(LocationInfo.PROP_NAME) + " in file " + stream + ".");
						}

						vars.set(LocationInfo.PROP_GRAVITY, gravity.toArray(new GravityObject[gravity.size()]));
						vars.set(LocationInfo.PROP_LOCATION_OBJECTS, locationObjects.toArray(new LocationObject[locationObjects.size()]));
						vars.set(LocationInfo.PROP_LIGHTS, lights.toArray(new LightInfo[lights.size()]));
						vars.set(LocationInfo.PROP_BACKGROUND, backgroundInfo);
						vars.set(LocationInfo.PROP_VECTOR_STAR, vectorStar);

						result.add(new LocationInfo(vars));
					}
				}
			}
		}
	}

	private void parseGravity(final GravityObject gravity, final Node node) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(GRAVITY_NODE.equals(child.getNodeName())) {

				final GravityObject instance = parseGravity(child);

				if(instance == null) {
					continue;
				}

				gravity.addChild(instance);
				parseGravity(instance, child);

			} else if(STATION_NODE.equals(child.getNodeName())) {

				final SpaceStation instance = parseStation(child);

				if(instance == null) {
					continue;
				}

				gravity.addChild(instance);
				parseStation(instance, child);
			}
		}
	}

	private GravityObject parseGravity(final Node node) {

		final LocalObjects local = LocalObjects.get();
		final VarTable vars = VarTable.newInstance(node);
		final GravityObjectTable gravityTable = GravityObjectTable.getInstance();
		final GravityObjectTemplate template = gravityTable.getTemplate(vars.getInteger("id"));

		if(template == null) {
			return null;
		}

		final int objectId = vars.getInteger("objectId");

		if(OBJECT_ID_GRAVITY_SET.contains(objectId)) {
			LOGGER.warning("found duplicate objectId " + objectId + " for " + template + " in " + stream);
		}

		OBJECT_ID_GRAVITY_SET.add(objectId);

		final Vector location = Vector.newInstance(vars.getFloatArray("loc", ",", ArrayFactory.toFloatArray(0, 0, 0)));

		final float mult = vars.getFloat("mult", 0F);

		if(mult != 0F) {
			location.multLocal(mult);
		}

		final GravityObject instance = (GravityObject) template.takeInstance(objectId);
		instance.setLocation(location, Vector.ZERO, local);

		final Rotation rotation = instance.getOrbitalRotation();
		rotation.fromAngles(vars.getFloatArray("rotate", ",", ArrayFactory.toFloatArray(0, 0, 0)));

		if(vars.getBoolean("randomRotation", false)) {

			final Rotation random = Rotation.newInstance();
			random.random();

			instance.setRotation(random, local);
		}

		instance.setDistance(vars.getInteger("distance", 0));
		instance.setTurnAroundTime(vars.getLong("turnAroundTime", 0) * 1000);
		instance.setTurnTime(vars.getInteger("turnTime", 0));

		try {
			synchronized(DATE_PARSER) {
				instance.setStartTime(DATE_PARSER.parse(vars.getString("startTime", DATE_PARSER.format(new Date()))).getTime());
			}
		} catch(final ParseException e) {
			LOGGER.warning(this, e);
		}

		return instance;
	}

	private Vector parseLights(final Node node, final VarTable vars, final Array<LightInfo> container) {

		final VarTable attrs = VarTable.newInstance(node);

		float[] color = attrs.getFloatArray(LocationInfo.PROP_STAR_LIGHT_COLOR, ",", ArrayFactory.toFloatArray(1, 1, 1));

		vars.set(LocationInfo.PROP_STAR_LIGHT_COLOR, new Color(color[0], color[1], color[2], 1F));

		color = attrs.getFloatArray(LocationInfo.PROP_AMBIENT_COLOR, ",", ArrayFactory.toFloatArray(1, 1, 1));

		vars.set(LocationInfo.PROP_AMBIENT_COLOR, new Color(color[0], color[1], color[2], 1F));

		color = attrs.getFloatArray(LocationInfo.PROP_SHADOW_LIGHT_COLOR, ",", ArrayFactory.toFloatArray(1, 1, 1));

		vars.set(LocationInfo.PROP_SHADOW_LIGHT_COLOR, new Color(color[0], color[1], color[2], 1F));

		final float[] coords = attrs.getFloatArray(LocationInfo.PROP_VECTOR_STAR, ",", ArrayFactory.toFloatArray(1, 1, 1));

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && LIGHT_NODE.equals(child.getNodeName())) {
				container.add(new LightInfo(attrs.parse(child)));
			}
		}

		return Vector.newInstance(coords[0], coords[1], coords[2]);
	}

	/**
	 * Парс наличия локационных объектов.
	 */
	private void parseLocationObjects(final Node node, final Array<LocationObject> container, final LocalObjects local) {

		final LocationObjectTable objectTable = LocationObjectTable.getInstance();
		final IdFactory idFactory = IdFactory.getInstance();
		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !LOCATION_OBJECT_NODE.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final int templateId = vars.getInteger("id");

			final LocationObjectTemplate template = objectTable.getTemplate(templateId);

			if(template == null) {
				LOGGER.warning("not found location object template for template id " + templateId);
				continue;
			}

			final LocationObject object = template.takeInstance(idFactory.getNextLocationId());

			if(object == null) {
				LOGGER.warning("not created location object for " + template);
				continue;
			}

			final Vector location = Vector.newInstance(vars.getFloatArray("position", ",", ArrayFactory.toFloatArray(0, 0, 0)));

			final float mult = vars.getFloat("mult", 0F);

			if(mult != 0F) {
				location.multLocal(mult);
			}

			final Rotation rotation = Rotation.newInstance();
			rotation.fromAngles(vars.getFloatArray("rotation", ",", ArrayFactory.toFloatArray(0, 0, 0)));

			if(vars.getBoolean("random", false)) {
				rotation.random();
			}

			object.setLocation(location, Vector.ZERO, local);
			object.setRotation(rotation, local);

			container.add(object);
		}
	}

	private SpaceStation parseStation(final Node node) {

		final VarTable vars = VarTable.newInstance(node);
		final StationTable stationTable = StationTable.getInstance();
		final StationTemplate template = stationTable.getTemplate(vars.getInteger("id"));
		final LocalObjects local = LocalObjects.get();

		if(template == null) {
			return null;
		}

		final int objectId = vars.getInteger("objectId");

		if(OBJECT_ID_STATION_SET.contains(objectId)) {
			LOGGER.warning("found duplicate objectId " + objectId + " for " + template + " in " + stream);
		}

		OBJECT_ID_STATION_SET.add(objectId);

		final SpaceStation instance = (SpaceStation) template.takeInstance(objectId);
		instance.setLocation(Vector.newInstance(vars.getFloatArray("loc", ",", ArrayFactory.toFloatArray(0, 0, 0))), Vector.ZERO, local);

		final Rotation rotation = instance.getOrbitalRotation();
		rotation.fromAngles(vars.getFloatArray("rotate", ",", ArrayFactory.toFloatArray(0, 0, 0)));

		instance.setDistance(vars.getInteger("distance", 0));
		instance.setTurnAroundTime(vars.getLong("turnAroundTime", 0) * 1000);
		instance.setTurnTime(vars.getInteger("turnTime", 0));

		try {
			synchronized(DATE_PARSER) {
				instance.setStartTime(DATE_PARSER.parse(vars.getString("startTime", DATE_PARSER.format(new Date()))).getTime());
			}
		} catch(final ParseException e) {
			LOGGER.warning(this, e);
		}

		return instance;
	}

	private void parseStation(final SpaceStation station, final Node node) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(GRAVITY_NODE.equals(child.getNodeName())) {

				final GravityObject instance = parseGravity(child);
				station.addChild(instance);

				parseGravity(instance, child);

			} else if(STATION_NODE.equals(child.getNodeName())) {

				final GravityObject instance = parseGravity(child);
				station.addChild(instance);

				parseGravity(instance, child);
			}
		}
	}
}

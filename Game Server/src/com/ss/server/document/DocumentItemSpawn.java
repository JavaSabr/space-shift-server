package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.item.spawn.ItemSpawn;
import com.ss.server.model.item.spawn.SpawnType;
import com.ss.server.model.spawn.Spawn;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Парсер спавна предметов с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentItemSpawn extends AbstractFileDocument<Array<ItemSpawn>> {

	private static final String ITEM_TEMPLATE_ID = "itemId";
	private static final String TYPE = "type";

	private static final String NODE_SPAWN = "spawn";
	private static final String NODE_POSITION = "position";
	private static final String NODE_TIME = "time";
	private static final String NODE_LIST = "list";

	public DocumentItemSpawn(final File file) {
		super(file);
	}

	@Override
	protected Array<ItemSpawn> create() {
		return ArrayFactory.newArray(ItemSpawn.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_LIST.equals(child.getNodeName())) {
				parseSpawns(child);
			}
		}
	}

	/**
	 * Парс группы спавнов.
	 */
	private void parseSpawn(final Node node) {

		final VarTable baseVars = VarTable.newInstance(node);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_TIME.equals(child.getNodeName())) {
				parseTime(child, baseVars);
			}
		}
	}

	/**
	 * Парс набора спавнов в узле хмл.
	 */
	private void parseSpawns(final Node node) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_SPAWN.equals(child.getNodeName())) {
				continue;
			}

			parseSpawn(child);
		}
	}

	/**
	 * Парс времени и позиций спавна.
	 */
	private void parseTime(final Node node, final VarTable baseVars) {

		final ItemTable itemTable = ItemTable.getInstance();

		final VarTable timeVars = VarTable.newInstance(node);
		final VarTable positionVars = VarTable.newInstance();

		final ItemTemplate template = itemTable.getTemplate(baseVars.getInteger(ITEM_TEMPLATE_ID));

		if(template == null) {
			LOGGER.warning(getClass(), "not found template for id " + baseVars.getInteger(ITEM_TEMPLATE_ID));
			return;
		}

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!NODE_POSITION.equals(child.getNodeName())) {
				continue;
			}

			positionVars.parse(child);

			final Vector location = Vector.newInstance();
			location.setX(positionVars.getFloat("x", 0));
			location.setY(positionVars.getFloat("y", 0));
			location.setZ(positionVars.getFloat("z", 0));

			final Rotation rotation = Rotation.newInstance();
			rotation.setX(positionVars.getFloat("rX", 0));
			rotation.setY(positionVars.getFloat("rY", 0));
			rotation.setZ(positionVars.getFloat("rZ", 0));
			rotation.setW(positionVars.getFloat("rW", 1));

			final VarTable allVars = VarTable.newInstance();
			allVars.set(baseVars);
			allVars.set(timeVars);
			allVars.set(Spawn.LOCATION, location);
			allVars.set(Spawn.ROTATION, rotation);
			allVars.set(Spawn.TEMPLATE, template);

			final SpawnType spawnType = baseVars.getEnum(TYPE, SpawnType.class);
			result.add(spawnType.newSpawn(allVars));
		}
	}
}

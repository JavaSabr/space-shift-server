package com.ss.server.document;

import java.io.File;
import java.lang.reflect.Constructor;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.ClassUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.quest.Quest;

/**
 * Парсер квестов с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentQuest extends AbstractFileDocument<Array<Quest>> {

	private static final String QUEST_PACKAGE_NAME = Quest.class.getPackage().getName() + ".impl.";

	private static final String NODE_QUEST = "quest";
	private static final String NODE_ROOT = "list";

	private static final String ATTR_CLASS = "class";

	public DocumentQuest(final File file) {
		super(file);
	}

	@Override
	protected Array<Quest> create() {
		return ArrayFactory.newArray(Quest.class);
	}

	@Override
	protected void parse(final Document document) {

		final VarTable vars = VarTable.newInstance();

		for(Node list = document.getFirstChild(); list != null; list = list.getNextSibling()) {

			if(list.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_ROOT.equals(list.getNodeName())) {

				for(Node child = list.getFirstChild(); child != null; child = child.getNextSibling()) {

					if(child.getNodeType() != Node.ELEMENT_NODE) {
						continue;
					}

					if(NODE_QUEST.equals(child.getNodeName())) {

						vars.parse(child);

						final Constructor<?> constructor = ClassUtils.getConstructor(QUEST_PACKAGE_NAME + vars.getString(ATTR_CLASS), Node.class);
						result.add((Quest) ClassUtils.newInstance(constructor, child));
					}
				}
			}
		}
	}
}

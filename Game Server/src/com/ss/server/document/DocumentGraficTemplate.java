package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.GraphicEffectType;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;

/**
 * Парсер шаблонов графических эффектов с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentGraficTemplate extends AbstractFileDocument<Array<GraphicEffectTemplate>> {

	private static final String ATTR_TYPE = "type";

	private static final String NODE_TEMPLATE = "template";
	private static final String NODE_ROOT = "list";

	public DocumentGraficTemplate(final File file) {
		super(file);
	}

	@Override
	protected Array<GraphicEffectTemplate> create() {
		return ArrayFactory.newArray(GraphicEffectTemplate.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_ROOT.equals(child.getNodeName())) {
				parseTemplate(child);
			}
		}
	}

	private void parseTemplate(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_TEMPLATE.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final GraphicEffectType type = vars.getEnum(ATTR_TYPE, GraphicEffectType.class);

			result.add(type.newTemplate(child));
		}
	}
}

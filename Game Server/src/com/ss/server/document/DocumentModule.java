package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.template.ModuleTemplate;

/**
 * Парсер модулей с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentModule extends AbstractFileDocument<Array<ModuleTemplate>> {

	private static final String NODE_TEMPLATE = "template";
	private static final String NODE_ROOT = "list";

	public DocumentModule(final File file) {
		super(file);
	}

	@Override
	protected Array<ModuleTemplate> create() {
		return ArrayFactory.newArray(ModuleTemplate.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(NODE_ROOT.equals(child.getNodeName())) {
				parseTemplate(child);
			}
		}
	}

	private void parseTemplate(final Node node) {
		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_TEMPLATE.equals(child.getNodeName())) {
				result.add(new ModuleTemplate(VarTable.newInstance(child), child));
			}
		}
	}
}

package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.template.SkillTemplate;

/**
 * Парсер скилов с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentSkill extends AbstractFileDocument<Array<SkillTemplate>> {

	private static final String NODE_SKILL = "skill";
	private static final String NODE_ROOT = "list";

	public DocumentSkill(final File file) {
		super(file);
	}

	@Override
	protected Array<SkillTemplate> create() {
		return ArrayFactory.newArray(SkillTemplate.class);
	}

	@Override
	protected void parse(final Document doc) {
		for(Node child = doc.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_ROOT.equals(child.getNodeName())) {
				for(Node temp = child.getFirstChild(); temp != null; temp = temp.getNextSibling()) {

					if(temp.getNodeType() != Node.ELEMENT_NODE) {
						continue;
					}

					if(NODE_SKILL.equals(temp.getNodeName())) {
						result.add(new SkillTemplate(temp));
					}
				}
			}
		}
	}
}

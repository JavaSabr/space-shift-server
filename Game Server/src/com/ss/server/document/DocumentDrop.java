package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.model.drop.DropList;

/**
 * Парсер дропа для NPS с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentDrop extends AbstractFileDocument<Table<Integer, DropList>> {

	public static final String NODE_LIST = "list";

	public DocumentDrop(final File file) {
		super(file);
	}

	@Override
	protected Table<Integer, DropList> create() {
		return TableFactory.newObjectTable();
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_LIST.equals(child.getNodeName())) {

			}
		}
	}
}

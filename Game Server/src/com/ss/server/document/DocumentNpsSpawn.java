package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAiClass;
import com.ss.server.model.ship.nps.spawn.NpsSpawn;
import com.ss.server.model.ship.nps.spawn.SpawnType;
import com.ss.server.table.ConfigAiTable;
import com.ss.server.table.ModuleTable;
import com.ss.server.table.ShipTable;
import com.ss.server.template.ModuleTemplate;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Парсер спавна кораблей NPS с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentNpsSpawn extends AbstractFileDocument<Array<NpsSpawn>> {

	private static final String CLASS_AI_ATTRIBUTE = "class";
	private static final String CONFIG_AI_ATTRIBUTE = "config";

	private static final String TYPE = "type";
	private static final String MODULE_TEMPLATE_ID = "id";
	private static final String NPS_TEMPLATE_ID = "id";

	private static final String NODE_SPAWN = "spawn";
	private static final String NODE_POSITION = "position";
	private static final String NODE_TIME = "time";
	private static final String NODE_AI = "ai";
	private static final String NODE_MODULE = "module";
	private static final String NODE_SHIP = "ship";
	private static final String NODE_LIST = "list";

	public DocumentNpsSpawn(final File file) {
		super(file);
	}

	@Override
	protected Array<NpsSpawn> create() {
		return ArrayFactory.newArray(NpsSpawn.class);
	}

	@Override
	protected void parse(final Document doc) {
		for(Node child = doc.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_LIST.equals(child.getNodeName())) {
				parseSpawns(child);
			}
		}
	}

	private VarTable parseAi(final Node node) {

		final VarTable vars = VarTable.newInstance(node);
		final VarTable result = VarTable.newInstance();

		final ConfigAiTable table = ConfigAiTable.getInstance();

		final String configName = vars.getString(CONFIG_AI_ATTRIBUTE, ConfigAiTable.DEFAULT_CONFIG_NAME);

		ConfigAi configAi = table.getConfig(configName);

		if(configAi == null) {
			LOGGER.warning("not found config Ai for name " + configName);
			configAi = table.getConfig(ConfigAiTable.DEFAULT_CONFIG_NAME);
		}

		final NpsAiClass aiClass = vars.getEnum(CLASS_AI_ATTRIBUTE, NpsAiClass.class);

		result.set(NpsSpawn.CONFIG_AI, configAi);
		result.set(NpsSpawn.AI_CLASS, aiClass);

		return result;
	}

	/**
	 * Парс характеристик корабля в спавне.
	 */
	private VarTable parseShip(final Node node) {

		final ShipTable shipTable = ShipTable.getInstance();
		final ModuleTable moduleTable = ModuleTable.getInstance();

		final VarTable vars = VarTable.newInstance(node);

		int templateId = vars.getInteger(NPS_TEMPLATE_ID);

		final NpsTemplate template = shipTable.getTemplate(templateId);

		if(template == null) {
			throw new RuntimeException("not found nps template for " + templateId + " in " + stream);
		}

		final Array<ModuleTemplate> modules = ArrayFactory.newArray(ModuleTemplate.class);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_MODULE.equals(child.getNodeName())) {

				vars.parse(child);

				templateId = vars.getInteger(MODULE_TEMPLATE_ID);

				final ModuleTemplate moduleTemplate = moduleTable.getTemplate(templateId);

				if(moduleTemplate == null) {
					throw new RuntimeException("not found module template for " + templateId + " in " + stream);
				}

				modules.add(moduleTemplate);
			}
		}

		modules.trimToSize();

		vars.clear();
		vars.set(NpsSpawn.TEMPLATE, template);
		vars.set(NpsSpawn.MODULES, modules.array());

		return vars;
	}

	/**
	 * Парс группы спавнов.
	 */
	private void parseSpawn(final Node node) {

		final VarTable baseVars = VarTable.newInstance(node);
		VarTable shipVars = null;
		VarTable aiVars = null;

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_SHIP.equals(child.getNodeName())) {
				shipVars = parseShip(child);
			} else if(NODE_AI.equals(child.getNodeName())) {
				aiVars = parseAi(child);
			} else if(NODE_TIME.equals(child.getNodeName())) {
				parseTime(child, baseVars, shipVars, aiVars);
			}
		}
	}

	/**
	 * Парс набора спавнов в узле хмл.
	 */
	private void parseSpawns(final Node node) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_SPAWN.equals(child.getNodeName())) {
				continue;
			}

			parseSpawn(child);
		}
	}

	/**
	 * Парс времени и позиций спавна.
	 */
	private void parseTime(final Node node, final VarTable baseVars, final VarTable shipVars, final VarTable aiVars) {

		final VarTable timeVars = VarTable.newInstance(node);
		final VarTable positionVars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!NODE_POSITION.equals(child.getNodeName())) {
				continue;
			}

			positionVars.parse(child);

			final Vector location = Vector.newInstance();
			location.setX(positionVars.getFloat("x", 0));
			location.setY(positionVars.getFloat("y", 0));
			location.setZ(positionVars.getFloat("z", 0));

			final Rotation rotation = Rotation.newInstance();
			rotation.setX(positionVars.getFloat("rX", 0));
			rotation.setY(positionVars.getFloat("rY", 0));
			rotation.setZ(positionVars.getFloat("rZ", 0));
			rotation.setW(positionVars.getFloat("rW", 1));

			final VarTable allVars = VarTable.newInstance();
			allVars.set(baseVars);
			allVars.set(shipVars);
			allVars.set(aiVars);
			allVars.set(timeVars);
			allVars.set(NpsSpawn.LOCATION, location);
			allVars.set(NpsSpawn.ROTATION, rotation);

			final SpawnType spawnType = baseVars.getEnum(TYPE, SpawnType.class);
			result.add(spawnType.newSpawn(allVars));
		}
	}
}

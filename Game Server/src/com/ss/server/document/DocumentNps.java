package com.ss.server.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.manager.FuncManager;
import com.ss.server.model.func.Func;
import com.ss.server.model.module.ModuleType;
import com.ss.server.model.module.system.SlotInfo;
import com.ss.server.template.ship.NpsTemplate;
import com.ss.server.template.ship.ShipTemplate;

/**
 * Парсер кораблей NPS с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentNps extends AbstractFileDocument<Array<ShipTemplate>> {

	private static final String NODE_FUNCS = "funcs";
	private static final String NODE_MODULES = "modules";
	private static final String NODE_TEMPLATE = "template";

	private static final FuncManager FUNC_MANAGER = FuncManager.getInstance();

	public DocumentNps(final File file) {
		super(file);
	}

	@Override
	protected Array<ShipTemplate> create() {
		return ArrayFactory.newArray(ShipTemplate.class);
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && "list".equals(child.getNodeName())) {
				parseFuncs(child);
			}
		}
	}

	private void parseFuncs(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_TEMPLATE.equals(child.getNodeName())) {

				vars.parse(child);

				final Array<SlotInfo> slots = ArrayFactory.newArray(SlotInfo.class);
				final Array<Func> funcs = ArrayFactory.newArray(Func.class);

				for(Node setting = child.getFirstChild(); setting != null; setting = setting.getNextSibling()) {

					if(setting.getNodeType() != Node.ELEMENT_NODE) {
						continue;
					}

					if(NODE_MODULES.equals(setting.getNodeName())) {

						for(Node module = setting.getFirstChild(); module != null; module = module.getNextSibling()) {

							if(module.getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}

							final VarTable moduleAttrs = VarTable.newInstance(module);

							final ModuleType type = ModuleType.valueOf(moduleAttrs.getString("type"));

							slots.add(new SlotInfo(type, Vector.newInstance(moduleAttrs.getFloat("x"), moduleAttrs.getFloat("y"), moduleAttrs.getFloat("z"))));
						}

					} else if(NODE_FUNCS.equals(setting.getNodeName())) {

						for(Node func = setting.getFirstChild(); func != null; func = func.getNextSibling()) {

							if(func.getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}

							final Func newFunc = FUNC_MANAGER.parse(func);

							if(newFunc == null) {
								LOGGER.warning(new Exception("not parse func " + func));
								continue;
							}

							funcs.add(newFunc);
						}
					}
				}

				vars.set(ShipTemplate.FUNCS, funcs.toArray(new Func[funcs.size()]));
				vars.set(ShipTemplate.SLOTS, slots.toArray(new SlotInfo[slots.size()]));

				result.add(new NpsTemplate(vars));
			}
		}
	}
}

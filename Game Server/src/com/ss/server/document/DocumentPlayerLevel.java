package com.ss.server.document;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;

/**
 * Парсер таблицы требуемого опыта для поднятия уровня.
 * 
 * @author Ronn
 */
public final class DocumentPlayerLevel extends AbstractFileDocument<Map<Integer, Long>> {

	private static final String EXP_ATTRIBUTE = "exp";
	private static final String VALUE_ATTRIBUTE = "value";

	private static final String NODE_LEVEL = "level";
	private static final String NODE_ROOT = "table";

	public DocumentPlayerLevel(final File file) {
		super(file);
	}

	@Override
	protected Map<Integer, Long> create() {
		return new HashMap<>();
	}

	@Override
	protected void parse(final Document document) {
		for(Node child = document.getFirstChild(); child != null; child = child.getNextSibling()) {
			if(child.getNodeType() == Node.ELEMENT_NODE && NODE_ROOT.equals(child.getNodeName())) {
				parse(child, result);
			}
		}
	}

	private void parse(final Node node, final Map<Integer, Long> result) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_LEVEL.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final int level = vars.getInteger(VALUE_ATTRIBUTE);
			final long exp = vars.getLong(EXP_ATTRIBUTE);

			result.put(level, exp);
		}
	}
}

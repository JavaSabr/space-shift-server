package com.ss.server.document;

import java.io.File;
import java.lang.reflect.Constructor;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.ClassUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.faction.Fraction;

/**
 * Парсер фракций из XML.
 * 
 * @author Ronn
 */
public final class DocumentFraction extends AbstractFileDocument<Array<Fraction>> {

	public static final String FRACTION_PACKAGE = Fraction.class.getPackage().getName();

	public static final String NODE_FRACTION = "fraction";
	public static final String NODE_ROOT = "list";

	public static final String ATTR_CLASS = "class";

	public DocumentFraction(final File file) {
		super(file);
	}

	@Override
	protected Array<Fraction> create() {
		return ArrayFactory.newArray(Fraction.class);
	}

	@Override
	protected void parse(final Document document) {

		final VarTable vars = VarTable.newInstance();

		for(Node node = document.getFirstChild(); node != null; node = node.getNextSibling()) {

			if(node.getNodeType() != Node.ELEMENT_NODE || !NODE_ROOT.equals(node.getNodeName())) {
				continue;
			}

			for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

				if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_FRACTION.equals(child.getNodeName())) {
					continue;
				}

				vars.parse(child);

				final String className = FRACTION_PACKAGE + ".impl." + vars.getString(ATTR_CLASS);
				final Constructor<? extends Fraction> constructor = ClassUtils.getConstructor(className, VarTable.class, Node.class);

				result.add(ClassUtils.newInstance(constructor, vars, child));
			}
		}
	}
}

package com.ss.server.document;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;

import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.condition.ContainerCondition;
import com.ss.server.model.quest.condition.impl.ConditionLogicAnd;
import com.ss.server.model.quest.condition.impl.ConditionLogicNot;
import com.ss.server.model.quest.condition.impl.ConditionLogicOr;
import com.ss.server.model.quest.condition.impl.ConditionPlayerVar;
import com.ss.server.model.quest.condition.impl.ConditionQuestActive;
import com.ss.server.model.quest.condition.impl.ConditionQuestComplete;
import com.ss.server.model.quest.condition.impl.ConditionQuestState;
import com.ss.server.model.quest.condition.impl.ConditionStationId;

/**
 * Парсер квестовых условий с xml.
 * 
 * @author Ronn
 */
public final class DocumentQuestCondition {

	private static final Logger LOGGER = LoggerManager.getLogger(DocumentQuestCondition.class);

	public static final String CONDITION_NOT = "not";
	public static final String CONDITION_OR = "or";
	public static final String CONDITION_AND = "and";
	public static final String CONDITION_PLAYER = "player";

	public static final String CONDITION_QUEST = "quest";

	public static final String CONDITION_STATION = "station";
	public static final String CONDITION_STATION_ID = "id";

	public static final String CONDITION_PLAYER_VAR = "var";

	public static final String CONDITION_QUEST_COMPLETE = "complete";
	public static final String CONDITION_QUEST_ACTIVE = "active";
	public static final String CONDITION_QUEST_STATE = "state";

	private static DocumentQuestCondition instance;

	public static final DocumentQuestCondition getInstance() {

		if(instance == null) {
			instance = new DocumentQuestCondition();
		}

		return instance;
	}

	/**
	 * Объеденяет 2 условия в 1.
	 */
	public static Condition joinAnd(Condition first, final Condition second) {

		if(first == null) {
			first = new ConditionLogicAnd();
		}

		((ContainerCondition) first).add(second);

		return first;
	}

	private DocumentQuestCondition() {
		super();
	}

	/**
	 * @return отпаршенное условие.
	 */
	private Condition parse(final Node node, final Quest quest) {

		switch(node.getNodeName()) {
			case CONDITION_STATION: {
				return parseStationCondition(node, quest);
			}
			case CONDITION_QUEST: {
				return parseQuestCondition(node, quest);
			}
			case CONDITION_PLAYER: {
				return parsePlayerCondition(node, quest);
			}
			case CONDITION_AND: {
				return parseLogicAnd(node, quest);
			}
			case CONDITION_OR: {
				return parseLogicOr(node, quest);
			}
			case CONDITION_NOT: {
				return parseLogicNot(node, quest);
			}
		}

		return null;
	}

	/**
	 * Парсер условий в теге 'И'
	 */
	public void parseCondition(final ConditionLogicAnd container, final Node node, final Quest quest) {

		if(node == null) {
			return;
		}

		for(Node cond = node.getFirstChild(); cond != null; cond = cond.getNextSibling()) {

			if(cond.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			Condition condition = parse(cond, quest);

			if(condition != null) {
				container.add(condition);
			}

			condition = null;
		}
	}

	/**
	 * Парсер условий в теге 'ИЛИ'
	 */
	public void parseCondition(final ConditionLogicOr container, final Node node, final Quest quest) {

		if(node == null) {
			return;
		}

		for(Node cond = node.getFirstChild(); cond != null; cond = cond.getNextSibling()) {

			if(cond.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			Condition condition = parse(cond, quest);

			if(condition != null) {
				container.add(condition);
			}

			condition = null;
		}
	}

	/**
	 * Парс итогового условия.
	 */
	public Condition parseCondition(final Node node, final Quest quest) {

		if(node == null) {
			return null;
		}

		for(Node cond = node.getFirstChild(); cond != null; cond = cond.getNextSibling()) {

			if(cond.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			final Condition condition = parse(cond, quest);

			if(condition != null) {
				return condition;
			}
		}

		return null;
	}

	private Condition parseLogicAnd(final Node node, final Quest quest) {
		final ConditionLogicAnd condition = new ConditionLogicAnd();
		parseCondition(condition, node, quest);
		return condition;
	}

	private Condition parseLogicNot(final Node node, final Quest quest) {

		final Condition condition = parseCondition(node, quest);

		if(condition != null) {
			return new ConditionLogicNot(condition);
		}

		return null;
	}

	/**
	 * Парсим набор условий для 'ИЛИ'
	 */
	private Condition parseLogicOr(final Node node, final Quest quest) {
		final ConditionLogicOr condition = new ConditionLogicOr();
		parseCondition(condition, node, quest);
		return condition;
	}

	private Condition parsePlayerCondition(final Node node, final Quest quest) {

		Condition newCondition = null;

		final NamedNodeMap attrs = node.getAttributes();
		final VarTable vars = VarTable.newInstance(node);

		for(int i = 0, length = attrs.getLength(); i < length; i++) {

			final Node item = attrs.item(i);

			switch(item.getNodeName()) {
				case CONDITION_PLAYER_VAR: {

					final String name = vars.getString(CONDITION_PLAYER_VAR);
					final String comparator = vars.getString("compare");

					final int val = vars.getInteger("val");

					newCondition = new ConditionPlayerVar(name, comparator, val);
					break;
				}
			}
		}

		if(newCondition == null) {
			LOGGER.warning(this, "unrecognized <player> condition " + vars + " in quest " + quest.getName());
		}

		return newCondition;
	}

	private Condition parseQuestCondition(final Node node, final Quest quest) {

		Condition newCondition = null;

		final NamedNodeMap attrs = node.getAttributes();
		final VarTable vars = VarTable.newInstance(node);

		for(int i = 0, length = attrs.getLength(); i < length; i++) {

			final Node item = attrs.item(i);

			switch(item.getNodeName()) {
				case CONDITION_QUEST_STATE: {
					newCondition = new ConditionQuestState(vars.getInteger(CONDITION_QUEST_STATE));
					break;
				}
				case CONDITION_QUEST_COMPLETE: {
					newCondition = new ConditionQuestComplete(vars.getBoolean(CONDITION_QUEST_COMPLETE));
					break;
				}
				case CONDITION_QUEST_ACTIVE: {
					newCondition = new ConditionQuestActive(vars.getBoolean(CONDITION_QUEST_ACTIVE));
					break;
				}
			}
		}

		if(newCondition == null) {
			LOGGER.warning(this, "unrecognized <quest> condition " + vars + " in quest " + quest.getName());
		}

		return newCondition;
	}

	private Condition parseStationCondition(final Node node, final Quest quest) {

		Condition newCondition = null;

		final NamedNodeMap attrs = node.getAttributes();
		final VarTable vars = VarTable.newInstance(node);

		for(int i = 0, length = attrs.getLength(); i < length; i++) {

			final Node item = attrs.item(i);

			switch(item.getNodeName()) {
				case CONDITION_STATION_ID: {
					newCondition = new ConditionStationId(vars.getInteger(CONDITION_STATION_ID));
					break;
				}
			}
		}

		if(newCondition == null) {
			LOGGER.warning(this, "unrecognized <station> condition " + vars + " in quest " + quest.getName());
		}

		return newCondition;
	}
}

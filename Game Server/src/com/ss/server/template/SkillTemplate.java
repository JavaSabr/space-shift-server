package com.ss.server.template;

import java.util.Iterator;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayComparator;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.manager.FuncManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.damage.ShieldType;
import com.ss.server.model.func.Func;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.SkillGroup;
import com.ss.server.model.skills.SkillStatType;
import com.ss.server.model.skills.SkillType;

/**
 * Шаблон умения модуля.
 * 
 * @author Ronn
 */
public final class SkillTemplate {

	private static final Logger LOGGER = LoggerManager.getLogger(SkillTemplate.class);

	public static final String NODE_ACTIVE_FUNCS = "activeFuncs";
	public static final String NODE_DESCRIPTION = "description";

	public static final String ENERGY_WEAPON = "energyWeapon";
	public static final String STAT_FIELD_OFFSET = "__";
	public static final String DESCRIPTION_START = "\n%description_start%\n";
	public static final String STAT_START = "\n%stat_start%\n";
	public static final String TEXT = "text";
	public static final String STAT = "stat";
	public static final String STATS = "stats";
	public static final String VALUE = "value";
	public static final String SET = "set";
	public static final String NAME = "name";
	public static final String ID = "id";

	public static final String PROP_MAX_SPEED = "maxSpeed";
	public static final String PROP_MAX_DISTANCE = "maxDistance";
	public static final String PROP_SKILL_TYPE = "skillType";
	public static final String PROP_SKILL_GROUP = "skillGroup";
	public static final String PROP_ICON = "icon";
	public static final String PROP_RELOAD_ID = "reloadId";
	public static final String PROP_ACTIVE_TIME = "activeTime";
	public static final String PROP_ITEM_CONSUME_COUNT = "itemConsumeCount";
	public static final String PROP_ITEM_CONSUME_ID = "itemConsumeId";
	public static final String PROP_ROTATION_SPEED = "rotationSpeed";
	public static final String PROP_ACCEL = "accel";
	public static final String PROP_ENERGY_CONSUME = "energyConsume";
	public static final String PROP_POWER = "power";
	public static final String PROP_RELOAD_CONSUME = "reloadConsume";
	public static final String PROP_RELOAD_DELAY = "reloadDelay";
	public static final String PROP_MIN_RELOAD_DELAY = "minReloadDelay";
	public static final String PROP_MAX_RELOAD_DELAY = "maxReloadDelay";
	public static final String PROP_MAX_STRENGTH = "maxStrength";
	public static final String PROP_DAMAGE_TYPE = "damageType";
	public static final String PROP_DIRECTION_ATTACK = "directionAttack";
	public static final String PROP_SHIELD_TYPE = "shieldType";
	public static final String PROP_MAX_CHARGE = "maxCharge";
	public static final String PROP_FIRE_RATE = "fireRate";
	public static final String PROP_CHARGE_REGEN_INTERVAL = "chargeRegenInterval";
	public static final String PROP_CHARGE_REGEN = "chargeRegen";

	private static final FuncManager FUNC_MANAGER = FuncManager.getInstance();

	private static final ArrayComparator<String> STAT_KEY_COMPARATOR = (first, second) -> first.compareTo(second);

	/** пул экземпляров скилов */
	private final FoldablePool<Skill> pool;

	/** тип скила */
	private final SkillType skillType;
	/** группа скилов */
	private final SkillGroup skillGroup;
	/** тип урона умения */
	private final DamageType damageType;
	/** тип щита */
	private final ShieldType shieldType;

	/** ид темплейта */
	private final int id;

	/** набор активных функций */
	private final Func[] activeFuncs;

	/** название скила */
	private String name;
	/** описание скила */
	private String description;
	/** характеристики умения */
	private String descriptionStats;
	/** адресс иконки */
	private String icon;

	/** скорость разворота */
	private final float rotationSpeed;
	/** потребление времени перезарядки на время активности */
	private final float reloadConsume;

	/** максимальная скорость */
	private int maxSpeed;
	/** максимальная дистанция */
	private int maxDistance;
	/** время перезарядки */
	private int reuseDelay;
	/** минимальное время отката */
	private int minReloadDelay;
	/** максимальное время отката */
	private int maxReloadDelay;
	/** ид отката скила */
	private int reloadId;
	/** мощность умения */
	private int power;
	/** кол-во затрачиваемой энергии */
	private int energyConsume;
	/** ускорение */
	private int accel;
	/** ид потребляемого предмета */
	private int itemConsumeId;
	/** кол-во потребляемого предмета */
	private int itemConsumeCount;
	/** время активности умения */
	private int activeTime;
	/** максимальная прочность */
	private int maxStrength;
	/** максимальный заряд */
	private int maxCharge;
	/** интервал регенерации заряда */
	private int chargeRegenInterval;
	/** кол-во регенерирующего заряда за 1 интервал */
	private int chargeRegen;
	/** скорострельность */
	private int fireRate;

	/** энергетическое оружия */
	private boolean energyWeapon;
	/** направленная ли атака */
	private boolean directionAttack;

	public SkillTemplate(final Node node) {
		this.pool = PoolFactory.newAtomicFoldablePool(Skill.class);
		this.description = StringUtils.EMPTY;
		this.descriptionStats = StringUtils.EMPTY;

		final VarTable vars = VarTable.newInstance();

		// извлечение атрибутов
		{
			vars.parse(node);

			this.id = vars.getInteger(ID);
			this.name = vars.getString(NAME);
		}

		final Array<Func> activeFuncs = ArrayFactory.newArray(Func.class);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_DESCRIPTION.equals(child.getNodeName())) {
				parseDescription(child);
			} else if(NODE_ACTIVE_FUNCS.equals(child.getNodeName())) {
				parseFuncs(child, activeFuncs);
			}
		}

		this.activeFuncs = activeFuncs.toArray(new Func[activeFuncs.size()]);

		// извлечение параметров
		{
			vars.parse(node, SET, NAME, VALUE);

			this.rotationSpeed = vars.getFloat(PROP_ROTATION_SPEED, 0);
			this.reloadConsume = vars.getFloat(PROP_RELOAD_CONSUME, 1F);

			this.skillType = vars.getEnum(PROP_SKILL_TYPE, SkillType.class);
			this.skillGroup = vars.getEnum(PROP_SKILL_GROUP, SkillGroup.class);
			this.damageType = vars.getEnum(PROP_DAMAGE_TYPE, DamageType.class, DamageType.NONE);
			this.shieldType = vars.getEnum(PROP_SHIELD_TYPE, ShieldType.class, ShieldType.NONE);
			this.icon = vars.getString(PROP_ICON);
			this.maxDistance = vars.getInteger(PROP_MAX_DISTANCE, 0);
			this.maxSpeed = vars.getInteger(PROP_MAX_SPEED, 0);
			this.reuseDelay = vars.getInteger(PROP_RELOAD_DELAY, 0);
			this.reloadId = vars.getInteger(PROP_RELOAD_ID, id);
			this.power = vars.getInteger(PROP_POWER, 0);
			this.energyConsume = vars.getInteger(PROP_ENERGY_CONSUME, 0);
			this.accel = vars.getInteger(PROP_ACCEL, 0);
			this.itemConsumeId = vars.getInteger(PROP_ITEM_CONSUME_ID, 0);
			this.itemConsumeCount = vars.getInteger(PROP_ITEM_CONSUME_COUNT, 0);
			this.activeTime = vars.getInteger(PROP_ACTIVE_TIME, 0);
			this.minReloadDelay = vars.getInteger(PROP_MIN_RELOAD_DELAY, 0);
			this.maxReloadDelay = vars.getInteger(PROP_MAX_RELOAD_DELAY, 0);
			this.maxStrength = vars.getInteger(PROP_MAX_STRENGTH, 0);
			this.maxCharge = vars.getInteger(PROP_MAX_CHARGE, 0);
			this.chargeRegen = vars.getInteger(PROP_CHARGE_REGEN, 0);
			this.chargeRegenInterval = vars.getInteger(PROP_CHARGE_REGEN_INTERVAL, 0);
			this.fireRate = (int) (1000 / vars.getFloat(PROP_FIRE_RATE, 1));

			this.energyWeapon = vars.getBoolean(ENERGY_WEAPON, false);
			this.directionAttack = vars.getBoolean(PROP_DIRECTION_ATTACK, true);
		}
	}

	/**
	 * Добавить активные функции умения к указанному объекту.
	 * 
	 * @param object объект, которому надо добавить их.
	 */
	public final void addActiveFuncsTo(final SpaceObject object) {

		final Func[] funcs = getActiveFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(final Func func : funcs) {
			func.addMe(object);
		}
	}

	/**
	 * @return ускорение.
	 */
	public int getAccel() {
		return accel;
	}

	/**
	 * @return набор активных функций.
	 */
	public Func[] getActiveFuncs() {
		return activeFuncs;
	}

	/**
	 * @return время действия умения.
	 */
	public int getActiveTime() {
		return activeTime;
	}

	/**
	 * @return тип урона умения.
	 */
	public DamageType getDamageType() {
		return damageType;
	}

	/**
	 * @return описание скила.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return характеристики умения.
	 */
	public String getDescriptionStats() {
		return descriptionStats;
	}

	/**
	 * @return кол-во затрачиваемой энергии.
	 */
	public int getEnergyConsume() {
		return energyConsume;
	}

	/**
	 * @return адресс иконки скила.
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @return ид скила.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return кол-во потребляемого предмета.
	 */
	public int getItemConsumeCount() {
		return itemConsumeCount;
	}

	/**
	 * @return ид потребляемого предмета.
	 */
	public int getItemConsumeId() {
		return itemConsumeId;
	}

	/**
	 * @return максимальная дистанция скила.
	 */
	public int getMaxDistance() {
		return maxDistance;
	}

	/**
	 * @return максимальное время отката.
	 */
	public int getMaxReloadDelay() {
		return maxReloadDelay;
	}

	/**
	 * @return максимальная скорость скила.
	 */
	public int getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * @return максимальная прочность.
	 */
	public int getMaxStrength() {
		return maxStrength;
	}

	/**
	 * @return минимальное время отката.
	 */
	public int getMinReloadDelay() {
		return minReloadDelay;
	}

	/**
	 * @return название скила
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return мощность умения.
	 */
	public int getPower() {
		return power;
	}

	/**
	 * @return потребление времени перезарядки на время активности.
	 */
	public float getReloadConsume() {
		return reloadConsume;
	}

	/**
	 * @return время перезарядки.
	 */
	public int getReloadDelay() {
		return reuseDelay;
	}

	/**
	 * @return ид отката скила.
	 */
	public int getReloadId() {
		return reloadId;
	}

	/**
	 * @return скорость разворота.
	 */
	public float getRotationSpeed() {
		return rotationSpeed;
	}

	/**
	 * @return тип щита.
	 */
	public ShieldType getShieldType() {
		return shieldType;
	}

	/**
	 * @return группа скилов.
	 */
	public SkillGroup getSkillGroup() {
		return skillGroup;
	}

	/**
	 * @return тип скила.
	 */
	public SkillType getSkillType() {
		return skillType;
	}

	/**
	 * @return направленная ли атака.
	 */
	public boolean isDirectionAttack() {
		return directionAttack;
	}

	/**
	 * @return энергетическое оружия.
	 */
	public boolean isEnergyWeapon() {
		return energyWeapon;
	}

	/**
	 * @return новый экземпляр скила.
	 */
	public Skill newInstance() {
		return skillType.newInstance(this);
	}

	/**
	 * Парс данных для описания скила.
	 * 
	 * @param node корень описания в хмл.
	 */
	private void parseDescription(final Node node) {

		final VarTable vals = VarTable.newInstance(node);

		final Table<String, String> stats = TableFactory.newObjectTable();
		final Array<String> keys = ArrayFactory.newArray(String.class);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_DESCRIPTION.equals(child.getNodeName())) {
				vals.parse(child);
				this.description = vals.getString(TEXT);
			} else if(STATS.equals(child.getNodeName())) {

				vals.parse(child, STAT, NAME, VALUE);
				vals.getValues().keyArray(keys);

				for(final String key : keys) {
					final SkillStatType stat = SkillStatType.valueOf(key);
					stats.put(stat.getLangName(), stat.convert(vals.getString(key)));
				}

				keys.clear();
			}
		}

		if(!stats.isEmpty()) {

			final StringBuilder builder = new StringBuilder();
			stats.keyArray(keys).sort(STAT_KEY_COMPARATOR);

			for(final Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {

				final String key = iterator.next();

				builder.append(key).append(STAT_FIELD_OFFSET).append(stats.get(key));

				if(iterator.hasNext()) {
					builder.append("\n");
				}
			}

			this.descriptionStats = builder.toString();
		}
	}

	/**
	 * Парс функций из xml узла.
	 */
	private void parseFuncs(final Node node, final Array<Func> container) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			final Func newFunc = FUNC_MANAGER.parse(child);

			if(newFunc == null) {
				LOGGER.warning(new Exception("can't parse func " + child));
				continue;
			}

			container.add(newFunc);
		}
	}

	/**
	 * @param skill складываемый скил в пул.
	 */
	public void put(final Skill skill) {
		pool.put(skill);
	}

	/**
	 * Удалить активные функции умения у указанного объекта.
	 * 
	 * @param object объект, которому надо удалить их.
	 */
	public final void removeActiveFuncsTo(final SpaceObject object) {

		final Func[] funcs = getActiveFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(final Func func : funcs) {
			func.removeMe(object);
		}
	}

	/**
	 * @return максимальный заряд.
	 */
	public int getMaxCharge() {
		return maxCharge;
	}

	/**
	 * @return скорострельность.
	 */
	public int getFireRate() {
		return fireRate;
	}

	/**
	 * @return кол-во регенерирующего заряда за 1 интервал.
	 */
	public int getChargeRegen() {
		return chargeRegen;
	}

	/**
	 * @return интервал регенерации заряда.
	 */
	public int getChargeRegenInterval() {
		return chargeRegenInterval;
	}

	@Override
	public String toString() {
		return "SkillTemplate skillType = " + skillType + ",  id = " + id + ",  name = " + name + ",  description = " + description + ",  icon = " + icon;
	}
}

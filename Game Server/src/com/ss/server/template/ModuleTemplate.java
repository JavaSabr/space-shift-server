package com.ss.server.template;

import org.w3c.dom.Node;

import rlib.util.ArrayUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.manager.FuncManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.func.Func;
import com.ss.server.model.module.ModuleType;
import com.ss.server.model.module.info.BlasterRayInfo;
import com.ss.server.model.module.info.EngineFireInfo;
import com.ss.server.model.module.info.ForceShieldInfo;
import com.ss.server.model.module.info.RocketShotInfo;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseModuleTemplate;
import com.ss.server.table.SkillTable;
import com.ss.server.template.description.TemplateDescription;

/**
 * Темплейт модуля корабля.
 * 
 * @author Ronn
 */
public class ModuleTemplate extends ObjectTemplate {

	public static final String NODE_STATS = "stats";
	public static final String NODE_SKILLS = "skills";
	public static final String NODE_ACTIVE_FUNCS = "activeFuncs";
	public static final String NODE_FUNCS = "funcs";
	public static final String NODE_ROCKET_INFO = "rocket";
	public static final String NODE_FORCE_SHIELD_INFO = "force_shield";
	public static final String NODE_BLASTER_INFO = "blaster";
	public static final String NODE_DESCRIPTION = "description";

	public static final String NODE_ENGINES = "engines";
	public static final String NODE_ENGINE = "engine";

	public static final String PROP_ICON = "icon";
	public static final String PROP_VALUE = "value";
	public static final String PROP_NAME = "name";
	public static final String PROP_STAT = "stat";
	public static final String PROP_DESCRIPTION = "description";
	public static final String PROP_TEXT = "text";

	private static final FuncManager FUNC_MANAGER = FuncManager.getInstance();
	private static final SkillTable SKILL_TABLE = SkillTable.getInstance();

	/** описание шаблона */
	protected final TemplateDescription description;

	/** шаблон частиц двигателя */
	protected EngineFireInfo[] engineFireInfos;
	/** шаблон луча бластера */
	protected BlasterRayInfo[] blasterRayInfos;

	/** набор выдаваемых скилов модулем */
	protected SkillTemplate[] skills;

	/** функции от модуля */
	protected Func[] funcs;
	/** набор активных функций */
	protected Func[] activeFuncs;

	/** подходящие платформы */
	protected int[] platforms;

	/** информация о силовом щите модуля */
	protected ForceShieldInfo forceShieldInfo;
	/** описание ракеты модуля */
	protected RocketShotInfo rocketShotInfo;

	/** название модуля */
	protected String name;
	/** путь к иконке модуля */
	protected String icon;

	public ModuleTemplate(final VarTable vars, final Node node) {
		super(vars);

		this.description = new TemplateDescription();

		try {

			this.platforms = vars.getIntegerArray("platforms", ";");
			this.icon = vars.getString(PROP_ICON);
			this.name = vars.getString(PROP_NAME, "@module-name:unknown@");

			init(node);

		} catch(final Exception e) {
			LOGGER.warning(this, e);
		}
	}

	/**
	 * Добавить функции этого темплейта объекту.
	 * 
	 * @param object объект, которому надо добавить их.
	 */
	public final void addActiveFuncs(final SpaceObject object) {

		final Func[] funcs = getActiveFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(int i = 0, length = funcs.length; i < length; i++) {
			funcs[i].addMe(object);
		}
	}

	/**
	 * Добавить функции этого темплейта объекту.
	 * 
	 * @param object объект, которому надо добавить их.
	 */
	public final void addFuncs(final SpaceObject object) {

		final Func[] funcs = getFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(int i = 0, length = funcs.length; i < length; i++) {
			funcs[i].addMe(object);
		}
	}

	/**
	 * @return набор активных функций.
	 */
	public Func[] getActiveFuncs() {
		return activeFuncs;
	}

	/**
	 * @return набор описания лучшей бластеров.
	 */
	public BlasterRayInfo[] getBlasterRayInfos() {
		return blasterRayInfos;
	}

	/**
	 * @return описание шаблона.
	 */
	public TemplateDescription getDescription() {
		return description;
	}

	/**
	 * @return набор описаний струй двигателя модуля.
	 */
	public EngineFireInfo[] getEngineFireInfos() {
		return engineFireInfos;
	}

	/**
	 * @return информация о силовом поле модуля.
	 */
	public ForceShieldInfo getForceShieldInfo() {
		return forceShieldInfo;
	}

	/**
	 * @return функции модуля.
	 */
	public final Func[] getFuncs() {
		return funcs;
	}

	@Override
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return ResponseModuleTemplate.getInstance(this, local);
	}

	/**
	 * @return название модуля.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return путь к иконке модуля.
	 */
	public String getPropIcon() {
		return icon;
	}

	/**
	 * @return описание ракеты модуля.
	 */
	public RocketShotInfo getRocketShotInfo() {
		return rocketShotInfo;
	}

	/**
	 * @return массив прилагаемых скилов.
	 */
	public final SkillTemplate[] getSkills() {
		return skills;
	}

	@Override
	public ModuleType getType() {
		return (ModuleType) super.getType();
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return vars.getEnum("type", ModuleType.class);
	}

	/**
	 * Инициализация структуры шаблона.
	 */
	protected void init(final Node node) {

		final Array<EngineFireInfo> engineFireInfos = ArrayFactory.newArray(EngineFireInfo.class);
		final Array<BlasterRayInfo> blasterRayInfos = ArrayFactory.newArray(BlasterRayInfo.class);
		final Array<Func> funcs = ArrayFactory.newArray(Func.class);
		final Array<Func> activeFuncs = ArrayFactory.newArray(Func.class);
		final Array<SkillTemplate> skills = ArrayFactory.newArray(SkillTemplate.class);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			switch(child.getNodeName()) {
				case NODE_ENGINES: {
					parseEngines(engineFireInfos, child);
					break;
				}
				case NODE_BLASTER_INFO: {
					final VarTable params = VarTable.newInstance(child, "set", PROP_NAME, PROP_VALUE);
					blasterRayInfos.add(new BlasterRayInfo(params));
					break;
				}
				case NODE_FORCE_SHIELD_INFO: {
					final VarTable params = VarTable.newInstance(child, "set", PROP_NAME, PROP_VALUE);
					forceShieldInfo = new ForceShieldInfo(params);
					break;
				}
				case NODE_ROCKET_INFO: {
					rocketShotInfo = parseRocketShotInfo(child);
					break;
				}
				case NODE_FUNCS: {
					parseFuncs(child, funcs);
					break;
				}
				case NODE_ACTIVE_FUNCS: {
					parseFuncs(child, activeFuncs);
					break;
				}
				case NODE_SKILLS: {
					parseSkills(child, skills);
					break;
				}
				case NODE_DESCRIPTION: {
					TemplateDescription.parse(description, child);
					break;
				}
			}
		}

		engineFireInfos.trimToSize();
		blasterRayInfos.trimToSize();
		funcs.trimToSize();
		skills.trimToSize();
		activeFuncs.trimToSize();

		this.engineFireInfos = engineFireInfos.array();
		this.blasterRayInfos = blasterRayInfos.array();
		this.funcs = funcs.array();
		this.skills = skills.array();
		this.activeFuncs = activeFuncs.array();
	}

	/**
	 * Проверка на совместимость модуля с корпусом.
	 * 
	 * @param platformId ид корпуса.
	 * @return совместим ли модуль.
	 */
	public boolean isAvealablePlatform(final int platformId) {
		return ArrayUtils.contains(platforms, platformId);
	}

	/**
	 * Парсинг описаний двигателей модуля.
	 */
	private void parseEngines(final Array<EngineFireInfo> engineFireInfos, final Node node) {

		final VarTable params = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_ENGINE.equals(child.getNodeName())) {
				continue;
			}

			engineFireInfos.add(new EngineFireInfo(params.parse(child, "set", PROP_NAME, PROP_VALUE)));
		}
	}

	/**
	 * Парс функций из xml узла.
	 */
	private void parseFuncs(final Node node, final Array<Func> container) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			final Func newFunc = FUNC_MANAGER.parse(child);

			if(newFunc == null) {
				LOGGER.warning(new Exception("can't parse func " + child));
				continue;
			}

			container.add(newFunc);
		}
	}

	/**
	 * Парс описания ракеты из xml узла.
	 */
	private RocketShotInfo parseRocketShotInfo(final Node node) {

		final VarTable vars = VarTable.newInstance(node, "set", PROP_NAME, PROP_VALUE);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_ENGINE.equals(child.getNodeName())) {
				final VarTable params = VarTable.newInstance(child, "set", PROP_NAME, PROP_VALUE);
				vars.set(RocketShotInfo.PROP_ENGINE_INFO, new EngineFireInfo(params));
				break;
			}
		}

		return new RocketShotInfo(vars);
	}

	/**
	 * Парс скилов модуля из xml узла.
	 */
	private void parseSkills(final Node node, final Array<SkillTemplate> container) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			vars.parse(child);

			final SkillTemplate temp = SKILL_TABLE.getTemplate(vars.getInteger(PROP_ID));

			if(temp == null) {
				continue;
			}

			container.add(temp);
		}
	}

	/**
	 * Удалить функции этого темплейта у объекта.
	 * 
	 * @param object объект, которому надо удалить их.
	 */
	public final void removeActiveFuncs(final SpaceObject object) {

		final Func[] funcs = getActiveFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(int i = 0, length = funcs.length; i < length; i++) {
			funcs[i].removeMe(object);
		}
	}

	/**
	 * Удалить функции этого темплейта у объекта.
	 * 
	 * @param object объект, которому надо удалить их.
	 */
	public final void removeFuncs(final SpaceObject object) {

		final Func[] funcs = getFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(int i = 0, length = funcs.length; i < length; i++) {
			funcs[i].removeMe(object);
		}
	}

	/**
	 * @param funcs функции модуля.
	 */
	public final void setFuncs(final Func[] funcs) {
		this.funcs = funcs;
	}
}

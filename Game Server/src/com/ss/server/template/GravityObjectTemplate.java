package com.ss.server.template;

import rlib.geom.Vector;
import rlib.geom.bounding.Bounding;
import rlib.geom.bounding.BoundingFactory;
import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.gravity.GravityObjectType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseGravityObjectTemplate;

/**
 * Шаблон гравитационного объекта.
 * 
 * @author Ronn
 */
public class GravityObjectTemplate extends ObjectTemplate {

	public static final String BRIGHTNESS = "brightness";
	public static final String RADIUS = "radius";

	/** радиус объекта */
	private final int radius;

	/** яркость объекта */
	private final int brightness;

	public GravityObjectTemplate(final VarTable vars) {
		super(vars);

		this.radius = vars.getInteger(RADIUS, 0);
		this.sizeX = radius;
		this.sizeY = radius;
		this.sizeZ = radius;
		this.brightness = vars.getInteger(BRIGHTNESS, 0);
	}

	/**
	 * @return яркость объекта.
	 */
	public int getBrightness() {
		return brightness;
	}

	@Override
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return ResponseGravityObjectTemplate.getInstance(this);
	}

	/**
	 * @return радиус объекта.
	 */
	public final int getRadius() {
		return radius;
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return vars.getEnum(PROP_TYPE, GravityObjectType.class);
	}

	@Override
	public Bounding newBounding(final Vector center) {
		return BoundingFactory.newBoundingSphere(center, Vector.ZERO, getRadius());
	}
}

package com.ss.server.template;

import java.lang.reflect.Constructor;

import rlib.geom.Vector;
import rlib.geom.bounding.Bounding;
import rlib.geom.bounding.BoundingFactory;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ClassUtils;
import rlib.util.ObjectUtils;
import rlib.util.Reloadable;
import rlib.util.VarTable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.table.GraficEffectTable;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;

/**
 * Базовая модель шаблона всех космических объектов.
 * 
 * @author Ronn
 */
public abstract class ObjectTemplate implements Reloadable<ObjectTemplate> {

	protected static final Logger LOGGER = LoggerManager.getLogger(ObjectTemplate.class);

	public static final String PROP_ID = "id";
	public static final String PROP_TYPE = "type";
	public static final String PROP_KEY = "key";
	public static final String PROP_SIZE_Z = "sizeZ";
	public static final String PROP_SIZE_Y = "sizeY";
	public static final String PROP_SIZE_X = "sizeX";
	public static final String PROP_VISIBLE_RANGE = "visibleRange";
	public static final String PROP_DESTRUCT_EFFECT = "destructEffect";
	public static final String PROP_FINISH_TELEPORT_EFFECT = "finishTeleportEffect";
	public static final String PROP_START_TELEPORT_EFFECT = "startTeleportEffect";
	public static final String PROP_SCALE = "scale";

	/** пул инстансов этого темплейта */
	protected final FoldablePool<SpaceObject> instancePool;

	/** шаблон эффекта разрушения */
	protected final GraphicEffectTemplate destructEffect;
	/** эффект старта телепортации */
	protected final GraphicEffectTemplate startTeleportEffect;
	/** эффект финиша телепртации */
	protected final GraphicEffectTemplate finishTeleportEffect;

	/** тип объектов шаблона */
	private final SpaceObjectType type;

	/** ид шаблона */
	protected final int id;

	/** путь к контенту шаблона */
	protected String key;

	/** маштаб объекта */
	protected float scale;

	/** длинна модели */
	protected int sizeX;
	/** ширина модели */
	protected int sizeY;
	/** высота модели */
	protected int sizeZ;
	/** максимальный размер модели */
	protected int maxSize;
	/** минимальный размер модели */
	protected int minSize;

	/** дистанция видимости */
	protected int visibleRange;

	public ObjectTemplate(final VarTable vars) {
		this.id = vars.getInteger(PROP_ID);
		this.sizeX = vars.getInteger(PROP_SIZE_X, 0);
		this.sizeY = vars.getInteger(PROP_SIZE_Y, 0);
		this.sizeZ = vars.getInteger(PROP_SIZE_Z, 0);
		this.maxSize = Math.max(Math.max(sizeX, sizeY), sizeZ);
		this.minSize = Math.min(Math.min(sizeX, sizeY), sizeZ);
		this.visibleRange = vars.getInteger(PROP_VISIBLE_RANGE, Config.WORLD_DEFAULT_VISIBLE_RANGE);

		this.scale = vars.getFloat(PROP_SCALE, 1F);

		this.key = vars.getString(PROP_KEY, null);
		this.type = getType(vars);
		this.instancePool = PoolFactory.newAtomicFoldablePool(getInstanceClass());

		final GraficEffectTable effectTable = GraficEffectTable.getInstance();

		this.startTeleportEffect = effectTable.getTemplate(vars.getInteger(PROP_START_TELEPORT_EFFECT, 0));
		this.finishTeleportEffect = effectTable.getTemplate(vars.getInteger(PROP_FINISH_TELEPORT_EFFECT, 0));
		this.destructEffect = effectTable.getTemplate(vars.getInteger(PROP_DESTRUCT_EFFECT, 0));
	}

	/**
	 * @return шаблон эффекта разрушения.
	 */
	public GraphicEffectTemplate getDestructEffect() {
		return destructEffect;
	}

	/**
	 * @return эффект финиша телепртации.
	 */
	public GraphicEffectTemplate getFinishTeleportEffect() {
		return finishTeleportEffect;
	}

	/**
	 * @return ид шаблона.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Получение серверного пакета дл отправки описания этого шаблона.
	 * 
	 * @return серверный пакет с информацией о шаблоне.
	 */
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return null;
	}

	/**
	 * @return класс объектов этого шаблона.
	 */
	public Class<? extends SpaceObject> getInstanceClass() {
		return type.getInstanceClass();
	}

	/**
	 * @return путь к контенту модели.
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return максимальный размер модели.
	 */
	public int getMaxSize() {
		return maxSize;
	}

	/**
	 * @return минимальный размер модели.
	 */
	public int getMinSize() {
		return minSize;
	}

	/**
	 * @return маштаб объекта.
	 */
	public float getScale() {
		return scale;
	}

	/**
	 * @return размер модели по Х.
	 */
	public int getSizeX() {
		return sizeX;
	}

	/**
	 * @return размер модели по Y.
	 */
	public int getSizeY() {
		return sizeY;
	}

	/**
	 * @return размер модели по Z.
	 */
	public int getSizeZ() {
		return sizeZ;
	}

	/**
	 * @return эффект старта телепортации.
	 */
	public GraphicEffectTemplate getStartTeleportEffect() {
		return startTeleportEffect;
	}

	/**
	 * @return тип объектов шаблона.
	 */
	public SpaceObjectType getType() {
		return type;
	}

	/**
	 * Получение типа объектов шаблона.
	 */
	protected abstract SpaceObjectType getType(VarTable vars);

	/**
	 * @return дистанция видимости.
	 */
	public int getVisibleRange() {
		return visibleRange;
	}

	/**
	 * @return соответствующая форма.
	 */
	public Bounding newBounding(final Vector center) {
		return BoundingFactory.newBoundingEmpty();
	}

	/**
	 * Сохранение объекта в пул.
	 */
	public void putInstance(final SpaceObject object) {
		instancePool.put(object);
	}

	@Override
	public void reload(final ObjectTemplate update) {
		ObjectUtils.reload(this, update);
	}

	/**
	 * Получение нового объекта этого шаблона.
	 */
	@SuppressWarnings("unchecked")
	public <T extends SpaceObject> T takeInstance(final int objectId) {

		SpaceObject instance = instancePool.take();

		if(instance == null) {
			final Constructor<? extends SpaceObject> constructor = ClassUtils.getConstructor(getInstanceClass(), int.class, getClass());
			instance = ClassUtils.newInstance(constructor, objectId, this);
		}

		instance.setObjectId(objectId);

		return (T) instance;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " type = " + type + ",  id = " + id + ",  key = " + key + ",  sizeX = " + sizeX + ",  sizeY = " + sizeY + ",  sizeZ = " + sizeZ + ",  visibleRange = "
				+ visibleRange;
	}
}

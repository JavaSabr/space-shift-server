package com.ss.server.template.description;

import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.Table;

import com.ss.server.model.spec.ObjectSpecType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.table.SkillTable;
import com.ss.server.template.SkillTemplate;

/**
 * Реализация модели описанияш шаблона объекта.
 * 
 * @author Ronn
 */
public class TemplateDescription {

	public static final void parse(final TemplateDescription description, final Node node) {

		final VarTable vals = VarTable.newInstance(node);

		description.setText(vals.getString(PROP_TEXT, StringUtils.EMPTY));

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(StringUtils.equals(child.getNodeName(), NODE_STATS)) {

				final Array<String> keys = ArrayFactory.newArray(String.class);

				vals.parse(child, PROP_STAT, PROP_NAME, PROP_VALUE);

				final Table<String, Object> values = vals.getValues();
				values.keyArray(keys);

				for(final String key : keys) {
					final ObjectSpecType stat = ObjectSpecType.valueOf(key);
					description.addStat(new KeyValueDescription(stat.getLangName(), stat.convert(vals.getString(key))));
				}

				keys.clear();

			} else if(StringUtils.equals(child.getNodeName(), NODE_SPECS)) {

				final Array<String> keys = ArrayFactory.newArray(String.class);

				vals.parse(child, PROP_SPEC, PROP_NAME, PROP_VALUE);

				final Table<String, Object> values = vals.getValues();
				values.keyArray(keys);

				for(final String key : keys) {
					final ObjectSpecType stat = ObjectSpecType.valueOf(key);
					description.addSpec(new KeyValueDescription(stat.getLangName(), stat.convert(vals.getString(key))));
				}

				keys.clear();

			} else if(StringUtils.equals(child.getNodeName(), NODE_SKILLS)) {

				for(Node skillNode = child.getFirstChild(); skillNode != null; skillNode = skillNode.getNextSibling()) {

					if(skillNode.getNodeType() != Node.ELEMENT_NODE) {
						continue;
					}

					vals.parse(skillNode);

					final SkillTable skillTable = SkillTable.getInstance();
					final SkillTemplate template = skillTable.getTemplate(vals.getInteger(PROP_ID));

					if(template != null) {
						description.addSkill(template);
					}
				}
			}
		}
	}

	public static final String NODE_SPECS = "specs";
	public static final String NODE_STATS = "stats";

	public static final String NODE_SKILLS = "skills";
	public static final String PROP_ID = "id";
	public static final String PROP_TEXT = "text";
	public static final String PROP_VALUE = "value";
	public static final String PROP_NAME = "name";

	public static final String PROP_STAT = "stat";
	public static final String PROP_SPEC = "spec";

	/** спецификации входящие в описание */
	private final Array<KeyValueDescription> specs;
	/** характеристики входящие в описание */
	private final Array<KeyValueDescription> stats;
	/** умения входящие в описание */
	private final Array<SkillTemplate> skills;

	/** текст описания */
	private String text;

	public TemplateDescription() {
		this.text = StringUtils.EMPTY;
		this.specs = ArrayFactory.newArray(KeyValueDescription.class);
		this.stats = ArrayFactory.newArray(KeyValueDescription.class);
		this.skills = ArrayFactory.newArray(SkillTemplate.class);
	}

	/**
	 * @param template новое умение.
	 */
	public void addSkill(final SkillTemplate template) {
		skills.add(template);
	}

	/**
	 * @param description новая спецификация.
	 */
	public void addSpec(final KeyValueDescription description) {
		specs.add(description);
	}

	/**
	 * @param description новая характеристика.
	 */
	public void addStat(final KeyValueDescription description) {
		stats.add(description);
	}

	/**
	 * @return умения входящие в описание.
	 */
	public Array<SkillTemplate> getSkills() {
		return skills;
	}

	/**
	 * @return спецификации входящие в описание.
	 */
	public Array<KeyValueDescription> getSpecs() {
		return specs;
	}

	/**
	 * @return характеристики входящие в описание.
	 */
	public Array<KeyValueDescription> getStats() {
		return stats;
	}

	/**
	 * @return текст описания.
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text текст описания.
	 */
	public void setText(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [specs=" + specs + ", stats=" + stats + ", skills=" + skills + ", text=" + text + "]";
	}

	/**
	 * Запись описания в пакет.
	 * 
	 * @param packet записываемый пакет.
	 * @param buffer буффер данных.
	 */
	public void writeTo(final ServerPacket packet, final ByteBuffer buffer) {

		final Array<KeyValueDescription> specs = getSpecs();
		final Array<KeyValueDescription> stats = getStats();
		final Array<SkillTemplate> skills = getSkills();

		final String text = getText();

		packet.writeByte(buffer, text.length());
		packet.writeString(buffer, text);

		packet.writeByte(buffer, specs.size());

		if(!specs.isEmpty()) {

			for(final KeyValueDescription description : specs.array()) {

				if(description == null) {
					break;
				}

				final String name = description.getName();
				final String value = description.getValue();

				packet.writeByte(buffer, name.length());
				packet.writeString(buffer, name);

				packet.writeByte(buffer, value.length());
				packet.writeString(buffer, value);
			}
		}

		packet.writeByte(buffer, stats.size());

		if(!stats.isEmpty()) {

			for(final KeyValueDescription description : stats.array()) {

				if(description == null) {
					break;
				}

				final String name = description.getName();
				final String value = description.getValue();

				packet.writeByte(buffer, name.length());
				packet.writeString(buffer, name);

				packet.writeByte(buffer, value.length());
				packet.writeString(buffer, value);
			}
		}

		packet.writeByte(buffer, skills.size());

		if(!skills.isEmpty()) {

			for(final SkillTemplate template : skills.array()) {

				if(template == null) {
					break;
				}

				packet.writeInt(buffer, template.getId());
			}
		}
	}
}

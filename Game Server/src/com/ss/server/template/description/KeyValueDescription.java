package com.ss.server.template.description;

/**
 * Реализация модели описания название - значение.
 * 
 * @author Ronn
 */
public class KeyValueDescription {

	/** название элемента описания */
	private String name;
	/** значение элемента описания */
	private String value;

	public KeyValueDescription(String name, String value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * @return название элемента описания.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return значение элемента описания.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param name название элемента описания.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param value значение элемента описания.
	 */
	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + name + ", value=" + value + "]";
	}
}

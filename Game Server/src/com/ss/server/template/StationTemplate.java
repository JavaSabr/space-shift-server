package com.ss.server.template;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.bounding.Bounding;
import rlib.geom.bounding.BoundingFactory;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.station.SpaceStationType;
import com.ss.server.model.station.module.StationModule;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseStationTemplate;

/**
 * Модель шаблона станции.
 * 
 * @author Ronn
 */
public class StationTemplate extends GravityObjectTemplate {

	public static final String PROP_WARNING_RADIUS = "warningRadius";
	public static final String PROP_DESTROY_RADIUS = "destroyRadius";
	public static final String PROP_TELEPORT_RADIUS = "teleportRadius";
	public static final String PROP_NAME = "name";
	public static final String PROP_RESTORE_ROTATION = "restoreRotation";
	public static final String PROP_RESTORE_LOCATION = "restoreLocation";
	public static final String PROP_ROTATION = "rotation";

	/** список модулей станции */
	protected final Array<StationModule> modules;

	/** информация о ангараха */
	protected HangarTemplate[] hangars;

	/** список связанных квестов */
	protected Quest[] quests;

	/** разворот станции */
	protected Rotation rotation;

	/** название станции */
	protected String name;

	/** радиус телепорта на станцию */
	protected int teleportRadius;
	/** радиус уничтожения */
	protected int destroyRadius;
	/** предупредительный радиус */
	protected int warningRadius;

	public StationTemplate(final VarTable vars) {
		super(vars);
		this.modules = ArrayFactory.newArray(StationModule.class);

		this.name = vars.getString(PROP_NAME);

		this.sizeX = vars.getInteger(PROP_SIZE_X, 0);
		this.sizeY = vars.getInteger(PROP_SIZE_Y, 0);
		this.sizeZ = vars.getInteger(PROP_SIZE_Z, 0);
		this.teleportRadius = vars.getInteger(PROP_TELEPORT_RADIUS, 0);
		this.destroyRadius = vars.getInteger(PROP_DESTROY_RADIUS, 0);
		this.warningRadius = vars.getInteger(PROP_WARNING_RADIUS, 0);
		this.rotation = Rotation.newInstance();
		this.rotation.fromAngles(vars.getFloatArray(PROP_ROTATION, ",", ArrayFactory.toFloatArray(0, 0, 0)));
	}

	/**
	 * @return радиус уничтожения.
	 */
	public int getDestroyRadius() {
		return destroyRadius;
	}

	/**
	 * @return hangars инфа о ангарах станции.
	 */
	public final HangarTemplate[] getHangars() {
		return hangars;
	}

	@Override
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return ResponseStationTemplate.getInstance(this, local);
	}

	/**
	 * @return название станции.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return список связанных квестов.
	 */
	public final Quest[] getQuests() {
		return quests;
	}

	/**
	 * @return разворот станции.
	 */
	public Rotation getRotation() {
		return rotation;
	}

	/**
	 * @return радиус телепорта на станцию.
	 */
	public int getTeleportRadius() {
		return teleportRadius;
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return vars.getEnum("type", SpaceStationType.class);
	}

	/**
	 * @return предупредительный радиус.
	 */
	public int getWarningRadius() {
		return warningRadius;
	}

	@Override
	public Bounding newBounding(final Vector center) {
		return BoundingFactory.newBoundingBox(center, Vector.ZERO, sizeX, sizeY, sizeZ);
	}

	/**
	 * @param hangars инфа о ангарах станции.
	 */
	public final void setHangars(final HangarTemplate[] hangars) {
		this.hangars = hangars;
	}

	/**
	 * @param quests список связанных квестов.
	 */
	public final void setQuests(final Quest[] quests) {
		this.quests = quests;
	}

	/**
	 * @return список модулей станции.
	 */
	public Array<StationModule> getModules() {
		return modules;
	}

	/**
	 * Добавление нового модуля на станцию.
	 * 
	 * @param module новый модуль для станции.
	 */
	public void addModule(final StationModule module) {
		final Array<StationModule> modules = getModules();
		modules.add(module);
		modules.trimToSize();
	}
}

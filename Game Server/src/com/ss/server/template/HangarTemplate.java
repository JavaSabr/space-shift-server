package com.ss.server.template;

import java.lang.reflect.Constructor;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ClassUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.function.HangarFunction;
import com.ss.server.model.station.hangar.function.HangarFunctionFactory;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.model.station.hangar.window.HangarWindowFactory;

/**
 * Шаблон ангара космической станции.
 * 
 * @author Ronn
 */
public final class HangarTemplate {

	private static final Logger LOGGER = LoggerManager.getLogger(HangarTemplate.class);

	protected static final String HANGAR_PACKAGE_PATH = Hangar.class.getPackage().getName() + ".impl.";

	public static final String NODE_FUNCTION = "function";
	public static final String NODE_FUNCTIONS = "functions";
	public static final String NODE_WINDOW = "window";
	public static final String NODE_WINDOWS = "windows";

	public static final String PROP_ID = "id";
	public static final String PROP_NAME = "name";
	public static final String PROP_CLASS = "class";

	/** пул ангаров */
	private final FoldablePool<Hangar> pool = PoolFactory.newAtomicFoldablePool(Hangar.class);

	/** список доступных окон в ангаре */
	private final Array<HangarWindow> windows;
	/** список доступных функций у ангара */
	private final Array<HangarFunction> functions;

	/** параметры ангара */
	private final VarTable vars;

	/** конструктор ангара */
	private final Constructor<? extends Hangar> constructor;

	/** ид ангара */
	private final int id;

	public HangarTemplate(final Node node) {
		this.windows = ArrayFactory.newArray(HangarWindow.class);
		this.functions = ArrayFactory.newArray(HangarFunction.class);
		this.vars = VarTable.newInstance(node);
		this.id = vars.getInteger(PROP_ID);
		this.constructor = ClassUtils.getConstructor(HANGAR_PACKAGE_PATH + vars.getString(PROP_CLASS), HangarTemplate.class);
		parse(node);
	}

	/**
	 * @return конструктор ангара.
	 */
	public Constructor<? extends Hangar> getConstructor() {
		return constructor;
	}

	/**
	 * @return список доступных функций у ангара.
	 */
	public Array<HangarFunction> getFunctions() {
		return functions;
	}

	/**
	 * @return ид ангара.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * @return набор параметров ангара.
	 */
	public final VarTable getVars() {
		return vars;
	}

	/**
	 * @return список доступных окон в ангаре.
	 */
	public Array<HangarWindow> getWindows() {
		return windows;
	}

	/**
	 * Получение нового экземпляра ангара.
	 * 
	 * @param parent объект в котором находится ангар.
	 * @return новый экземпляр.
	 */
	public Hangar newInstance(final SpaceStation parent) {

		Hangar hangar = pool.take();

		if(hangar == null) {
			hangar = ClassUtils.newInstance(getConstructor(), this);
		}

		hangar.setParent(parent);
		return hangar;
	}

	/**
	 * Парсер фунционала и окон для этого ангара.
	 * 
	 * @param node узел с описанием ангара.
	 */
	private void parse(final Node node) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_WINDOWS.equals(child.getNodeName())) {
				parseWindows(child);
			} else if(NODE_FUNCTIONS.equals(child.getNodeName())) {
				parseFunctions(child);
			}
		}
	}

	/**
	 * Парс всех функций ангара.
	 * 
	 * @param node узел с описанием функций.
	 */
	private void parseFunctions(final Node node) {

		final Array<HangarFunction> functions = getFunctions();
		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!NODE_FUNCTION.equals(child.getNodeName())) {
				continue;
			}

			final HangarFunction function = HangarFunctionFactory.create(child, vars.parse(child));

			if(function == null) {
				LOGGER.warning("van't created function for " + vars);
				continue;
			}

			functions.add(function);
		}
	}

	/**
	 * Парс доступных окон для этого ангара.
	 * 
	 * @param node узел с описанием окон.
	 */
	private void parseWindows(final Node node) {

		final Array<HangarWindow> windows = getWindows();
		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!NODE_WINDOW.equals(child.getNodeName())) {
				continue;
			}

			final HangarWindow window = HangarWindowFactory.create(child, vars.parse(child));

			if(window == null) {
				LOGGER.warning("van't created window for " + vars);
				continue;
			}

			windows.add(window);
		}
	}

	/**
	 * @param hangar использованный ангар.
	 */
	public void put(final Hangar hangar) {
		pool.put(hangar);
	}
}

package com.ss.server.template.item;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.common.CommonItemType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseCommonItemTemplate;

/**
 * Шаблон различных космических предметов.
 * 
 * @author Ronn
 */
public class CommonItemTemplate extends ItemTemplate {

	public static final String ITEM_TYPE = "itemType";

	public CommonItemTemplate(final VarTable vars, final Node node) {
		super(vars, node);
	}

	@Override
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return ResponseCommonItemTemplate.getInstance(this, local);
	}

	@Override
	protected CommonItemType getType(final VarTable vars) {
		return vars.getEnum(PROP_TYPE, CommonItemType.class);
	}
}

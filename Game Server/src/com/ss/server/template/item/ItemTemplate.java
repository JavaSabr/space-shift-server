package com.ss.server.template.item;

import java.util.Comparator;
import java.util.Iterator;

import org.w3c.dom.Node;

import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.model.skills.SkillStatType;
import com.ss.server.template.ObjectTemplate;

/**
 * Шаблон космического предмета.
 * 
 * @author Ronn
 */
public abstract class ItemTemplate extends ObjectTemplate {

	public static final String ITEM_NAME = "%item_name%";
	public static final String DESCRIPTION = "description";
	public static final String TEXT = "text";
	public static final String STAT = "stat";
	public static final String STATS = "stats";
	public static final String VALUE = "value";
	public static final String STAT_START = "\n%stat_start%\n";
	public static final String STAT_FIELD_OFFSET = "___";
	public static final String DESCRIPTION_START = "\n%description_start%\n";

	public static final String NAME = "name";
	public static final String STACKEABLE = "stackable";
	public static final String ICON = "icon";

	private static final Comparator<String> STAT_KEY_COMPARATOR = (first, second) -> first.compareTo(second);

	/** название предмета */
	protected String name;
	/** ссылка на иконку предмета */
	protected String icon;
	/** описание прдмта */
	protected String description;
	/** характеристики прдмта */
	protected String descriptionStats;

	/** стакуемый ли предмет */
	protected boolean stackable;

	public ItemTemplate(final VarTable vars, final Node node) {
		super(vars);

		this.stackable = vars.getBoolean(STACKEABLE, false);
		this.icon = vars.getString(ICON);
		this.visibleRange = vars.getInteger(PROP_VISIBLE_RANGE, Config.WORLD_DEFAULT_VISIBLE_RANGE / 3);
		this.name = vars.getString(NAME);
		this.description = StringUtils.EMPTY;
		this.descriptionStats = StringUtils.EMPTY;

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(DESCRIPTION.equals(child.getNodeName())) {
				parseDescription(child);
			}
		}
	}

	/**
	 * @return описание предмета.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return характеристики предмта.
	 */
	public String getDescriptionStats() {
		return descriptionStats;
	}

	/**
	 * @return ссылка на иконку предмета.
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @return название предмета.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return стакуемый ли предмет.
	 */
	public boolean isStackable() {
		return stackable;
	}

	/**
	 * Парс данных для описания скила.
	 * 
	 * @param node корень описания в хмл.
	 */
	private void parseDescription(final Node node) {

		final VarTable vals = VarTable.newInstance(node);

		final Table<String, String> stats = TableFactory.newObjectTable();
		final Array<String> keys = ArrayFactory.newArray(String.class);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(DESCRIPTION.equals(child.getNodeName())) {
				vals.parse(child);
				this.description = vals.getString(TEXT);
			} else if(STATS.equals(child.getNodeName())) {

				vals.parse(child, STAT, NAME, VALUE);
				vals.getValues().keyArray(keys);

				for(final String key : keys) {
					final SkillStatType stat = SkillStatType.valueOf(key);
					stats.put(stat.getLangName(), stat.convert(vals.getString(key)));
				}

				keys.clear();
			}
		}

		if(!stats.isEmpty()) {

			final StringBuilder builder = new StringBuilder();
			stats.keyArray(keys).sort(STAT_KEY_COMPARATOR);

			for(final Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {

				final String key = iterator.next();

				builder.append(key).append(STAT_FIELD_OFFSET).append(stats.get(key));

				if(iterator.hasNext()) {
					builder.append("\n");
				}
			}

			this.descriptionStats = builder.toString();
		}
	}
}

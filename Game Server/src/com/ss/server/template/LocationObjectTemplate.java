package com.ss.server.template;

import rlib.geom.Vector;
import rlib.geom.bounding.Bounding;
import rlib.geom.bounding.BoundingFactory;
import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.location.object.LocationObjectType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseLocationObjectTemplate;

/**
 * Шаблон гравитационного объекта.
 * 
 * @author Ronn
 */
public class LocationObjectTemplate extends ObjectTemplate {

	public static final String PROP_SCALE = "scale";

	/** маштаб модели объекта */
	private final float scale;

	public LocationObjectTemplate(final VarTable vars) {
		super(vars);

		this.scale = vars.getFloat(PROP_SCALE, 1F);
	}

	@Override
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return ResponseLocationObjectTemplate.getInstance(this, local);
	}

	/**
	 * @return маштаб модели объекта.
	 */
	@Override
	public float getScale() {
		return scale;
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return vars.getEnum(PROP_TYPE, LocationObjectType.class);
	}

	@Override
	public Bounding newBounding(final Vector center) {
		return BoundingFactory.newBoundingBox(center, Vector.ZERO, sizeX, sizeY, sizeZ);
	}
}

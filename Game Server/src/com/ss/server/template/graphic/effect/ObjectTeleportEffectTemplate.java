package com.ss.server.template.graphic.effect;

import java.awt.Color;
import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.GraphicEffectType;
import com.ss.server.network.game.ServerPacket;

/**
 * Реализация шаблона эффекта телепортации объекта.
 * 
 * @author Ronn
 */
public class ObjectTeleportEffectTemplate extends GraphicEffectTemplate {

	public static final String NODE_ROUND_SPARK = "roundSpark";
	public static final String NODE_FLASH = "flash";

	public static final String PROP_TEXTURE = "texture";
	public static final String PROP_EFFECT_TIME = "time";
	public static final String PROP_END_COLOR = "endColor";
	public static final String PROP_START_COLOR = "startColor";
	public static final String PROP_MAX_LIFE = "maxLife";
	public static final String PROP_MIN_LIFE = "minLife";
	public static final String PROP_END_SIZE = "endSize";
	public static final String PROP_START_SIZE = "startSize";
	public static final String PROP_ROUND_SPARK_RADIUS = "roundSparkRadius";

	/** текстура вспышки */
	protected String flashTexture;
	/** текстура остаточных искр */
	protected String roundSparkTexture;

	/** цвет старта вспышки */
	protected Color flashStarColor;
	/** цвет финиша вспышки */
	protected Color flashEndColor;
	/** цвет старта окружающих частиц */
	protected Color roundSparkStarColor;
	/** цвет финиша окружающих частиц */
	protected Color roundSparkEndColor;

	/** размеры вспышки */
	protected float flashStartSize;
	protected float flashEndSize;
	/** вразмеры остаточных частиц */
	protected float roundSparkStartSize;
	protected float roundSparkEndSize;
	/** радиус источника остаточных частиц */
	protected float roundSparkRadius;

	/** время жизни вспышки */
	protected float flashMinLife;
	protected float flashMaxLife;
	/** время жизни остаточных частиц */
	protected float roundSparkMinLife;
	protected float roundSparkMaxLife;

	/** продолжительность эффекта */
	protected int timeEffect;

	/** есть ли вспышка */
	protected boolean hasFlash;
	/** есть ли остаточные искры */
	protected boolean hasRoundSpark;

	public ObjectTeleportEffectTemplate(final Node node) {
		super(node);

		final VarTable vars = VarTable.newInstance();
		vars.parse(node);

		this.timeEffect = vars.getInteger(PROP_EFFECT_TIME, 0);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			vars.parse(child, "set", "name", "value");

			if(NODE_FLASH.equals(child.getNodeName())) {

				this.hasFlash = true;
				this.flashTexture = vars.getString(PROP_TEXTURE, "effects/explosion/flash.png");
				this.flashStartSize = vars.getFloat(PROP_START_SIZE, 0.1F);
				this.flashEndSize = vars.getFloat(PROP_END_SIZE, 3.0F);
				this.flashMinLife = vars.getFloat(PROP_MIN_LIFE, 0.2F);
				this.flashMaxLife = vars.getFloat(PROP_MAX_LIFE, 0.2F);

				float[] array = vars.getFloatArray(PROP_START_COLOR, ",", ArrayFactory.toFloatArray(0.258F, 0.780F, 1));

				this.flashStarColor = new Color(array[0], array[1], array[2]);

				array = vars.getFloatArray(PROP_END_COLOR, ",", ArrayFactory.toFloatArray(0.258F, 0.780F, 1));

				this.flashEndColor = new Color(array[0], array[1], array[2]);

			} else if(NODE_ROUND_SPARK.equals(child.getNodeName())) {

				this.hasRoundSpark = true;
				this.roundSparkTexture = vars.getString(PROP_TEXTURE, "effects/explosion/roundspark.png");

				this.roundSparkEndSize = vars.getFloat(PROP_END_SIZE, 0);
				this.roundSparkStartSize = vars.getFloat(PROP_START_SIZE, 0);
				this.roundSparkMinLife = vars.getFloat(PROP_MIN_LIFE, 0);
				this.roundSparkMaxLife = vars.getFloat(PROP_MAX_LIFE, 0);
				this.roundSparkRadius = vars.getFloat(PROP_ROUND_SPARK_RADIUS, 1F);

				float[] array = vars.getFloatArray(PROP_START_COLOR, ",", ArrayFactory.toFloatArray(0.258F, 0.780F, 1));

				this.roundSparkStarColor = new Color(array[0], array[1], array[2]);

				array = vars.getFloatArray(PROP_END_COLOR, ",", ArrayFactory.toFloatArray(0.258F, 0.780F, 1));

				this.roundSparkEndColor = new Color(array[0], array[1], array[2]);
			}
		}
	}

	@Override
	public GraphicEffectType getEffectType() {
		return GraphicEffectType.OBJECT_TELEPORT;
	}

	/**
	 * @return цвет финиша вспышки.
	 */
	public Color getFlashEndColor() {
		return flashEndColor;
	}

	/**
	 * @return размер вспышки.
	 */
	public float getFlashEndSize() {
		return flashEndSize;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMaxLife() {
		return flashMaxLife;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMinLife() {
		return flashMinLife;
	}

	/**
	 * @return цвет старта вспышки.
	 */
	public Color getFlashStarColor() {
		return flashStarColor;
	}

	/**
	 * @return размеры вспышки.
	 */
	public float getFlashStartSize() {
		return flashStartSize;
	}

	/**
	 * @return текстура вспышки.
	 */
	public String getFlashTexture() {
		return flashTexture;
	}

	/**
	 * @return цвет финиша окружающих частиц.
	 */
	public Color getRoundSparkEndColor() {
		return roundSparkEndColor;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkEndSize() {
		return roundSparkEndSize;
	}

	/**
	 * @return время жизни остаточных частиц,
	 */
	public float getRoundSparkMaxLife() {
		return roundSparkMaxLife;
	}

	/**
	 * @return время жизни остаточных частиц.
	 */
	public float getRoundSparkMinLife() {
		return roundSparkMinLife;
	}

	/**
	 * @return радиус источника остаточных частиц.
	 */
	public float getRoundSparkRadius() {
		return roundSparkRadius;
	}

	/**
	 * @return цвет старта окружающих частиц.
	 */
	public Color getRoundSparkStarColor() {
		return roundSparkStarColor;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkStartSize() {
		return roundSparkStartSize;
	}

	/**
	 * @return текстура остаточных искр.
	 */
	public String getRoundSparkTexture() {
		return roundSparkTexture;
	}

	/**
	 * @return продолжительность эффекта.
	 */
	public int getTimeEffect() {
		return timeEffect;
	}

	/**
	 * @return есть ли вспышка.
	 */
	public boolean isHasFlash() {
		return hasFlash;
	}

	/**
	 * @return есть ли остаточные искры.
	 */
	public boolean isHasRoundSpark() {
		return hasRoundSpark;
	}

	@Override
	public void writeMe(final ByteBuffer buffer, final ServerPacket packet) {
		super.writeMe(buffer, packet);

		packet.writeInt(buffer, getTimeEffect());
		packet.writeByte(buffer, isHasFlash() ? 1 : 0);

		if(isHasFlash()) {

			final String texture = getFlashTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			Color color = getFlashStarColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			color = getFlashEndColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			final float flashMinLife = getFlashMinLife();
			final float flashMaxLife = getFlashMaxLife();

			packet.writeFloat(buffer, flashMinLife);
			packet.writeFloat(buffer, flashMaxLife);

			final float flashStartSize = getFlashStartSize();
			final float flashEndSize = getFlashEndSize();

			packet.writeFloat(buffer, flashStartSize);
			packet.writeFloat(buffer, flashEndSize);
		}

		packet.writeByte(buffer, isHasRoundSpark() ? 1 : 0);

		if(isHasRoundSpark()) {

			final String texture = getRoundSparkTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			Color color = getRoundSparkStarColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			color = getRoundSparkEndColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			packet.writeFloat(buffer, getRoundSparkStartSize());
			packet.writeFloat(buffer, getRoundSparkEndSize());
			packet.writeFloat(buffer, getRoundSparkRadius());

			packet.writeFloat(buffer, getRoundSparkMinLife());
			packet.writeFloat(buffer, getRoundSparkMaxLife());
		}
	}
}

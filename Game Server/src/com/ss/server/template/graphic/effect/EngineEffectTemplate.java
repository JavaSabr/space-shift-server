package com.ss.server.template.graphic.effect;

import java.awt.Color;
import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.GraphicEffectType;
import com.ss.server.network.game.ServerPacket;

/**
 * Реализация шаблона взрыва.
 * 
 * @author Ronn
 */
public class EngineEffectTemplate extends GraphicEffectTemplate {

	public static final String SCHOCK_WAVE_NODE = "schockWave";
	public static final String DEBRIS_NODE = "debris";
	public static final String SMOKE_TRAIL_NODE = "smokeTrail";
	public static final String ROUND_SPARK_NODE = "roundSpark";
	public static final String FLAME_NODE = "flame";
	public static final String FLASH_NODE = "flash";
	public static final String SPARK_NODE = "spark";

	private static final String TEXTURE = "texture";
	private static final String END_COLOR = "endColor";
	private static final String START_COLOR = "startColor";
	private static final String GRAVITY = "gravity";
	private static final String COLOR = "color";
	private static final String MAX_LIFE = "maxLife";
	private static final String MIN_LIFE = "minLife";
	private static final String END_SIZE = "endSize";
	private static final String START_SIZE = "startSize";

	/** текстура пламени */
	protected String flameTexture;
	/** текстура вспышки */
	protected String flashTexture;
	/** текстура остаточных искр */
	protected String roundSparkTexture;
	/** текстура разлетающихся искр */
	protected String sparkTexture;
	/** текстура дымовых частиц */
	protected String smokeTrailTexture;
	/** текстура осколков */
	protected String debrisTexture;
	/** текстура взрывной волны */
	protected String shockWaveTexture;

	/** цвет частиц */
	protected Color sparkColor;
	/** цвет старта пламени */
	protected Color flameStarColor;
	/** цвет финиша пламени */
	protected Color flameEndColor;
	/** цвет старта вспышки */
	protected Color flashStarColor;
	/** цвет финиша вспышки */
	protected Color flashEndColor;
	/** цвет старта волны */
	protected Color shockWaveStarColor;
	/** цвет финиша волны */
	protected Color shockWaveEndColor;

	/** направление частиц */
	protected Vector sparkGravity;

	/** размеры пламени */
	protected float flameStartSize;
	protected float flameEndSize;
	/** размеры частиц */
	protected float sparkStartSize;
	protected float sparkEndSize;
	/** размеры пламени */
	protected float shockWaveStartSize;
	protected float shockWaveEndSize;
	/** размеры вспышки */
	protected float flashStartSize;
	protected float flashEndSize;
	/** вразмеры остаточных частиц */
	protected float roundSparkStartSize;
	protected float roundSparkEndSize;

	/** время жизни пламени */
	protected float flameMinLife;
	protected float flameMaxLife;
	/** время жизни частиц */
	protected float sparkMinLife;
	protected float sparkMaxLife;
	/** время жизни вспышки */
	protected float flashMinLife;
	protected float flashMaxLife;
	/** время жизни волны */
	protected float shockWaveMinLife;
	protected float shockWaveMaxLife;
	/** время жизни остаточных частиц */
	protected float roundSparkMinLife;
	protected float roundSparkMaxLife;

	/** есть ли огонь */
	protected boolean hasFlame;
	/** есть ли вспышка */
	protected boolean hasFlash;
	/** есть ли разлетающиеся искры */
	protected boolean hasSpark;
	/** есть ли остаточные искры */
	protected boolean hasRoundSpark;
	/** есть ли разлетающийся дым */
	protected boolean hasSmokeTrail;
	/** есть ли разлетающиеся куски */
	protected boolean hasDebris;
	/** есть ли взрывная волна */
	protected boolean hasShockWave;

	public EngineEffectTemplate(final Node node) {
		super(node);

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			vars.parse(child, "set", "name", "value");

			if(SPARK_NODE.equals(child.getNodeName())) {

				this.hasSpark = true;
				this.sparkTexture = vars.getString(TEXTURE, "effects/explosion/spark.png");
				this.sparkStartSize = vars.getFloat(START_SIZE, 0.5F);
				this.sparkEndSize = vars.getFloat(END_SIZE, 0.5F);
				this.sparkMinLife = vars.getFloat(MIN_LIFE, 1.1F);
				this.sparkMaxLife = vars.getFloat(MAX_LIFE, 1.5F);

				float[] array = vars.getFloatArray(COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.sparkColor = new Color(array[0], array[1], array[2]);

				array = vars.getFloatArray(GRAVITY, ",", ArrayFactory.toFloatArray(0F, 0F, 0F));

				this.sparkGravity = Vector.newInstance(array);

			} else if(FLASH_NODE.equals(child.getNodeName())) {

				this.hasFlash = true;
				this.flashTexture = vars.getString(TEXTURE, "effects/explosion/flash.png");
				this.flashStartSize = vars.getFloat(START_SIZE, 0.1F);
				this.flashEndSize = vars.getFloat(END_SIZE, 3.0F);
				this.flashMinLife = vars.getFloat(MIN_LIFE, 0.2F);
				this.flashMaxLife = vars.getFloat(MAX_LIFE, 0.2F);

				float[] array = vars.getFloatArray(START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flashStarColor = new Color(array[0], array[1], array[2]);

				array = vars.getFloatArray(END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flashEndColor = new Color(array[0], array[1], array[2]);

			} else if(FLAME_NODE.equals(child.getNodeName())) {

				this.hasFlame = true;
				this.flameTexture = vars.getString(TEXTURE, "effects/explosion/flame.png");
				this.flameStartSize = vars.getFloat(START_SIZE, 0);
				this.flameEndSize = vars.getFloat(END_SIZE, 0);
				this.flameMinLife = vars.getFloat(MIN_LIFE, 0);
				this.flameMaxLife = vars.getFloat(MAX_LIFE, 0);

				float[] array = vars.getFloatArray(START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flameStarColor = new Color(array[0], array[1], array[2]);

				array = vars.getFloatArray(END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flameEndColor = new Color(array[0], array[1], array[2]);

			} else if(ROUND_SPARK_NODE.equals(child.getNodeName())) {

				this.hasRoundSpark = true;
				this.roundSparkTexture = vars.getString(TEXTURE, "effects/explosion/roundspark.png");

				this.roundSparkEndSize = vars.getFloat(END_SIZE, 0);
				this.roundSparkStartSize = vars.getFloat(START_SIZE, 0);
				this.roundSparkMinLife = vars.getFloat(MIN_LIFE, 0);
				this.roundSparkMaxLife = vars.getFloat(MAX_LIFE, 0);

			} else if(SMOKE_TRAIL_NODE.equals(child.getNodeName())) {

				this.hasSmokeTrail = true;
				this.smokeTrailTexture = vars.getString(TEXTURE, "effects/explosion/smoketrail.png");

			} else if(DEBRIS_NODE.equals(child.getNodeName())) {

				this.hasDebris = true;
				this.debrisTexture = vars.getString(TEXTURE, "effects/explosion/debris.png");

			} else if(SCHOCK_WAVE_NODE.equals(child.getNodeName())) {

				this.hasShockWave = true;
				this.shockWaveTexture = vars.getString(TEXTURE, "effects/explosion/shockwave.png");

				this.shockWaveEndSize = vars.getFloat(END_SIZE, 0);
				this.shockWaveStartSize = vars.getFloat(START_SIZE, 0);
				this.shockWaveMinLife = vars.getFloat(MIN_LIFE, 0);
				this.shockWaveMaxLife = vars.getFloat(MAX_LIFE, 0);

				float[] array = vars.getFloatArray(START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.shockWaveStarColor = new Color(array[0], array[1], array[2]);

				array = vars.getFloatArray(END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.shockWaveEndColor = new Color(array[0], array[1], array[2]);
			}
		}
	}

	/**
	 * @return текстура осколков.
	 */
	public String getDebrisTexture() {
		return debrisTexture;
	}

	@Override
	public GraphicEffectType getEffectType() {
		return GraphicEffectType.EXPLOSION;
	}

	/**
	 * @return цвет финиша пламени.
	 */
	public Color getFlameEndColor() {
		return flameEndColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getFlameEndSize() {
		return flameEndSize;
	}

	/**
	 * @return время жизни пламени.
	 */
	public float getFlameMaxLife() {
		return flameMaxLife;
	}

	/**
	 * @return время жизни пламени.
	 */
	public float getFlameMinLife() {
		return flameMinLife;
	}

	/**
	 * @return цвет старта пламени.
	 */
	public Color getFlameStarColor() {
		return flameStarColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getFlameStartSize() {
		return flameStartSize;
	}

	/**
	 * @return текстура пламени взрыва.
	 */
	public String getFlameTexture() {
		return flameTexture;
	}

	/**
	 * @return цвет финиша вспышки.
	 */
	public Color getFlashEndColor() {
		return flashEndColor;
	}

	/**
	 * @return размер вспышки.
	 */
	public float getFlashEndSize() {
		return flashEndSize;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMaxLife() {
		return flashMaxLife;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMinLife() {
		return flashMinLife;
	}

	/**
	 * @return цвет старта вспышки.
	 */
	public Color getFlashStarColor() {
		return flashStarColor;
	}

	/**
	 * @return размеры вспышки.
	 */
	public float getFlashStartSize() {
		return flashStartSize;
	}

	/**
	 * @return текстура вспышки.
	 */
	public String getFlashTexture() {
		return flashTexture;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkEndSize() {
		return roundSparkEndSize;
	}

	/**
	 * @return время жизни остаточных частиц,
	 */
	public float getRoundSparkMaxLife() {
		return roundSparkMaxLife;
	}

	/**
	 * @return время жизни остаточных частиц.
	 */
	public float getRoundSparkMinLife() {
		return roundSparkMinLife;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkStartSize() {
		return roundSparkStartSize;
	}

	/**
	 * @return текстура остаточных искр.
	 */
	public String getRoundSparkTexture() {
		return roundSparkTexture;
	}

	/**
	 * @return цвет финиша волны.
	 */
	public Color getShockWaveEndColor() {
		return shockWaveEndColor;
	}

	/**
	 * @return
	 */
	public float getShockWaveEndSize() {
		return shockWaveEndSize;
	}

	/**
	 * @return время жизни волны.
	 */
	public float getShockWaveMaxLife() {
		return shockWaveMaxLife;
	}

	/**
	 * @return время жизни волны.
	 */
	public float getShockWaveMinLife() {
		return shockWaveMinLife;
	}

	/**
	 * @return цвет старта волны.
	 */
	public Color getShockWaveStarColor() {
		return shockWaveStarColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getShockWaveStartSize() {
		return shockWaveStartSize;
	}

	/**
	 * @return текстура взрывной волны.
	 */
	public String getShockWaveTexture() {
		return shockWaveTexture;
	}

	/**
	 * @return текстура дымовых частиц.
	 */
	public String getSmokeTrailTexture() {
		return smokeTrailTexture;
	}

	/**
	 * @return цвет частиц.
	 */
	public Color getSparkColor() {
		return sparkColor;
	}

	/**
	 * @return конечный размер частиц.
	 */
	public float getSparkEndSize() {
		return sparkEndSize;
	}

	/**
	 * @return направление частиц.
	 */
	public Vector getSparkGravity() {
		return sparkGravity;
	}

	/**
	 * @return максимальное время жизни частиц.
	 */
	public float getSparkMaxLife() {
		return sparkMaxLife;
	}

	/**
	 * @return минимальное время жизни частицы.
	 */
	public float getSparkMinLife() {
		return sparkMinLife;
	}

	/**
	 * @return стартовый размер частиц.
	 */
	public float getSparkStartSize() {
		return sparkStartSize;
	}

	/**
	 * @return текстура разлетающихся искр.
	 */
	public String getSparkTexture() {
		return sparkTexture;
	}

	/**
	 * @return есть ли разлетающиеся куски.
	 */
	public boolean isHasDebris() {
		return hasDebris;
	}

	/**
	 * @return есть ли огонь.
	 */
	public boolean isHasFlame() {
		return hasFlame;
	}

	/**
	 * @return есть ли вспышка.
	 */
	public boolean isHasFlash() {
		return hasFlash;
	}

	/**
	 * @return есть ли остаточные искры.
	 */
	public boolean isHasRoundSpark() {
		return hasRoundSpark;
	}

	/**
	 * @return есть ли взрывная волна.
	 */
	public boolean isHasShockWave() {
		return hasShockWave;
	}

	/**
	 * @return есть ли разлетающийся дым.
	 */
	public boolean isHasSmokeTrail() {
		return hasSmokeTrail;
	}

	/**
	 * @return есть ли разлетающиеся искры.
	 */
	public boolean isHasSpark() {
		return hasSpark;
	}

	@Override
	public void writeMe(final ByteBuffer buffer, final ServerPacket packet) {
		super.writeMe(buffer, packet);

		packet.writeByte(buffer, isHasFlame() ? 1 : 0);

		if(isHasFlame()) {

			final String texture = getFlameTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			packet.writeFloat(buffer, getFlameStartSize());
			packet.writeFloat(buffer, getFlameEndSize());

			packet.writeFloat(buffer, getFlameMinLife());
			packet.writeFloat(buffer, getFlameMaxLife());

			Color color = getFlameStarColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			color = getFlameEndColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());
		}

		packet.writeByte(buffer, isHasFlash() ? 1 : 0);

		if(isHasFlash()) {

			final String texture = getFlashTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			Color color = getFlashStarColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			color = getFlashEndColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			final float flashMinLife = getFlashMinLife();
			final float flashMaxLife = getFlashMaxLife();

			packet.writeFloat(buffer, flashMinLife);
			packet.writeFloat(buffer, flashMaxLife);

			final float flashStartSize = getFlashStartSize();
			final float flashEndSize = getFlashEndSize();

			packet.writeFloat(buffer, flashStartSize);
			packet.writeFloat(buffer, flashEndSize);
		}

		packet.writeByte(buffer, isHasRoundSpark() ? 1 : 0);

		if(isHasRoundSpark()) {

			final String texture = getRoundSparkTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			packet.writeFloat(buffer, getRoundSparkStartSize());
			packet.writeFloat(buffer, getRoundSparkEndSize());

			packet.writeFloat(buffer, getRoundSparkMinLife());
			packet.writeFloat(buffer, getRoundSparkMaxLife());
		}

		packet.writeByte(buffer, isHasDebris() ? 1 : 0);

		if(isHasDebris()) {

			final String texture = getDebrisTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);
		}

		packet.writeByte(buffer, isHasShockWave() ? 1 : 0);

		if(isHasShockWave()) {

			final String texture = getShockWaveTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			packet.writeFloat(buffer, getShockWaveStartSize());
			packet.writeFloat(buffer, getShockWaveEndSize());
			packet.writeFloat(buffer, getShockWaveMinLife());
			packet.writeFloat(buffer, getShockWaveMaxLife());

			Color color = getShockWaveStarColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());

			color = getShockWaveEndColor();

			packet.writeInt(buffer, color.getRed());
			packet.writeInt(buffer, color.getGreen());
			packet.writeInt(buffer, color.getBlue());
		}

		packet.writeByte(buffer, isHasSmokeTrail() ? 1 : 0);

		if(isHasSmokeTrail()) {

			final String texture = getSmokeTrailTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);
		}

		packet.writeByte(buffer, isHasSpark() ? 1 : 0);

		if(isHasSpark()) {

			final String texture = getSparkTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			final Color sparkColor = getSparkColor();

			packet.writeInt(buffer, sparkColor.getRed());
			packet.writeInt(buffer, sparkColor.getGreen());
			packet.writeInt(buffer, sparkColor.getBlue());

			final float sparkStartSize = getSparkStartSize();
			final float sparkEndSize = getSparkEndSize();

			packet.writeFloat(buffer, sparkStartSize);
			packet.writeFloat(buffer, sparkEndSize);

			final Vector sparkGravity = getSparkGravity();

			packet.writeFloat(buffer, sparkGravity.getX());
			packet.writeFloat(buffer, sparkGravity.getY());
			packet.writeFloat(buffer, sparkGravity.getZ());

			final float sparkMaxLife = getSparkMaxLife();
			final float sparkMinLife = getSparkMinLife();

			packet.writeFloat(buffer, sparkMinLife);
			packet.writeFloat(buffer, sparkMaxLife);
		}
	}
}

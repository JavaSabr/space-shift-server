package com.ss.server.template.graphic.effect;

import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.model.GraphicEffectType;
import com.ss.server.network.game.ServerPacket;

/**
 * Базовый шаблон графического эффекта.
 * 
 * @author Ronn
 */
public abstract class GraphicEffectTemplate {

	/** ид эффекта */
	protected int id;

	public GraphicEffectTemplate(final Node node) {

		final VarTable vars = VarTable.newInstance(node);

		this.id = vars.getInteger("id");
	}

	/**
	 * @return тип эффекта.
	 */
	public GraphicEffectType getEffectType() {
		return null;
	}

	/**
	 * @return ид шаблона.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Запись шаблона в пакет.
	 * 
	 * @param buffer буффер пакета.
	 * @param packet записываемый пакет.
	 */
	public void writeMe(final ByteBuffer buffer, final ServerPacket packet) {
		packet.writeByte(buffer, getEffectType().ordinal());
		packet.writeInt(buffer, getId());
	}
}

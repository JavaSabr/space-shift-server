package com.ss.server.template.graphic.effect;

import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.graphics.color.ColorRGBA;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.GraphicEffectType;
import com.ss.server.network.game.ServerPacket;

/**
 * Реализация шаблона взрыва.
 * 
 * @author Ronn
 */
public class ExplosionEffectTemplate extends GraphicEffectTemplate {

	public static final String SCHOCK_WAVE_NODE = "schockWave";
	public static final String DEBRIS_NODE = "debris";
	public static final String SMOKE_TRAIL_NODE = "smokeTrail";
	public static final String ROUND_SPARK_NODE = "roundSpark";
	public static final String FLAME_NODE = "flame";
	public static final String FLASH_NODE = "flash";
	public static final String SPARK_NODE = "spark";

	private static final String PROP_TEXTURE = "texture";
	private static final String PROP_END_COLOR = "endColor";
	private static final String PROP_START_COLOR = "startColor";
	private static final String PROP_GRAVITY = "gravity";
	private static final String PROP_MAX_LIFE = "maxLife";
	private static final String PROP_MIN_LIFE = "minLife";
	private static final String PROP_END_SIZE = "endSize";
	private static final String PROP_START_SIZE = "startSize";

	/** текстура пламени */
	protected String flameTexture;
	/** текстура вспышки */
	protected String flashTexture;
	/** текстура остаточных искр */
	protected String roundSparkTexture;
	/** текстура разлетающихся искр */
	protected String sparkTexture;
	/** текстура дымовых частиц */
	protected String smokeTrailTexture;
	/** текстура осколков */
	protected String debrisTexture;
	/** текстура взрывной волны */
	protected String shockWaveTexture;

	/** цвет старта частиц */
	protected ColorRGBA sparkStartColor;
	/** цвет завершения частиц */
	protected ColorRGBA sparkEndColor;
	/** цвет старта остаточных частиц */
	protected ColorRGBA roundSparkStartColor;
	/** цвет завершения остаточных частиц */
	protected ColorRGBA roundSparkEndColor;
	/** цвет старта пламени */
	protected ColorRGBA flameStarColor;
	/** цвет финиша пламени */
	protected ColorRGBA flameEndColor;
	/** цвет старта вспышки */
	protected ColorRGBA flashStarColor;
	/** цвет финиша вспышки */
	protected ColorRGBA flashEndColor;
	/** цвет старта волны */
	protected ColorRGBA shockWaveStarColor;
	/** цвет финиша волны */
	protected ColorRGBA shockWaveEndColor;

	/** направление частиц */
	protected Vector sparkGravity;

	/** размеры пламени */
	protected float flameStartSize;
	protected float flameEndSize;
	/** размеры частиц */
	protected float sparkStartSize;
	protected float sparkEndSize;
	/** размеры пламени */
	protected float shockWaveStartSize;
	protected float shockWaveEndSize;
	/** размеры вспышки */
	protected float flashStartSize;
	protected float flashEndSize;
	/** вразмеры остаточных частиц */
	protected float roundSparkStartSize;
	protected float roundSparkEndSize;

	/** время жизни пламени */
	protected float flameMinLife;
	protected float flameMaxLife;
	/** время жизни частиц */
	protected float sparkMinLife;
	protected float sparkMaxLife;
	/** время жизни вспышки */
	protected float flashMinLife;
	protected float flashMaxLife;
	/** время жизни волны */
	protected float shockWaveMinLife;
	protected float shockWaveMaxLife;
	/** время жизни остаточных частиц */
	protected float roundSparkMinLife;
	protected float roundSparkMaxLife;

	/** есть ли огонь */
	protected boolean hasFlame;
	/** есть ли вспышка */
	protected boolean hasFlash;
	/** есть ли разлетающиеся искры */
	protected boolean hasSpark;
	/** есть ли остаточные искры */
	protected boolean hasRoundSpark;
	/** есть ли разлетающийся дым */
	protected boolean hasSmokeTrail;
	/** есть ли разлетающиеся куски */
	protected boolean hasDebris;
	/** есть ли взрывная волна */
	protected boolean hasShockWave;

	public ExplosionEffectTemplate(final Node node) {
		super(node);

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			vars.parse(child, "set", "name", "value");

			if(SPARK_NODE.equals(child.getNodeName())) {

				this.hasSpark = true;
				this.sparkTexture = vars.getString(PROP_TEXTURE, "effects/explosion/spark.png");
				this.sparkStartSize = vars.getFloat(PROP_START_SIZE, 0.5F);
				this.sparkEndSize = vars.getFloat(PROP_END_SIZE, 0.5F);
				this.sparkMinLife = vars.getFloat(PROP_MIN_LIFE, 1.1F);
				this.sparkMaxLife = vars.getFloat(PROP_MAX_LIFE, 1.5F);

				float[] array = vars.getFloatArray(PROP_START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.sparkStartColor = new ColorRGBA(array);

				array = vars.getFloatArray(PROP_END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.sparkEndColor = new ColorRGBA(array);

				array = vars.getFloatArray(PROP_GRAVITY, ",", ArrayFactory.toFloatArray(0F, 0F, 0F));

				this.sparkGravity = Vector.newInstance(array);

			} else if(FLASH_NODE.equals(child.getNodeName())) {

				this.hasFlash = true;
				this.flashTexture = vars.getString(PROP_TEXTURE, "effects/explosion/flash.png");
				this.flashStartSize = vars.getFloat(PROP_START_SIZE, 0.1F);
				this.flashEndSize = vars.getFloat(PROP_END_SIZE, 3.0F);
				this.flashMinLife = vars.getFloat(PROP_MIN_LIFE, 0.2F);
				this.flashMaxLife = vars.getFloat(PROP_MAX_LIFE, 0.2F);

				float[] array = vars.getFloatArray(PROP_START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flashStarColor = new ColorRGBA(array);

				array = vars.getFloatArray(PROP_END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flashEndColor = new ColorRGBA(array);

			} else if(FLAME_NODE.equals(child.getNodeName())) {

				this.hasFlame = true;
				this.flameTexture = vars.getString(PROP_TEXTURE, "effects/explosion/flame.png");
				this.flameStartSize = vars.getFloat(PROP_START_SIZE, 0);
				this.flameEndSize = vars.getFloat(PROP_END_SIZE, 0);
				this.flameMinLife = vars.getFloat(PROP_MIN_LIFE, 0);
				this.flameMaxLife = vars.getFloat(PROP_MAX_LIFE, 0);

				float[] array = vars.getFloatArray(PROP_START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flameStarColor = new ColorRGBA(array);

				array = vars.getFloatArray(PROP_END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.flameEndColor = new ColorRGBA(array);

			} else if(ROUND_SPARK_NODE.equals(child.getNodeName())) {

				this.hasRoundSpark = true;
				this.roundSparkTexture = vars.getString(PROP_TEXTURE, "effects/explosion/roundspark.png");

				float[] array = vars.getFloatArray(PROP_START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.roundSparkStartColor = new ColorRGBA(array[0], array[1], array[2]);

				array = vars.getFloatArray(PROP_END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.roundSparkEndColor = new ColorRGBA(array[0], array[1], array[2]);

				this.roundSparkEndSize = vars.getFloat(PROP_END_SIZE, 0);
				this.roundSparkStartSize = vars.getFloat(PROP_START_SIZE, 0);
				this.roundSparkMinLife = vars.getFloat(PROP_MIN_LIFE, 0);
				this.roundSparkMaxLife = vars.getFloat(PROP_MAX_LIFE, 0);

			} else if(SMOKE_TRAIL_NODE.equals(child.getNodeName())) {

				this.hasSmokeTrail = true;
				this.smokeTrailTexture = vars.getString(PROP_TEXTURE, "effects/explosion/smoketrail.png");

			} else if(DEBRIS_NODE.equals(child.getNodeName())) {

				this.hasDebris = true;
				this.debrisTexture = vars.getString(PROP_TEXTURE, "effects/explosion/debris.png");

			} else if(SCHOCK_WAVE_NODE.equals(child.getNodeName())) {

				this.hasShockWave = true;
				this.shockWaveTexture = vars.getString(PROP_TEXTURE, "effects/explosion/shockwave.png");

				this.shockWaveEndSize = vars.getFloat(PROP_END_SIZE, 0);
				this.shockWaveStartSize = vars.getFloat(PROP_START_SIZE, 0);
				this.shockWaveMinLife = vars.getFloat(PROP_MIN_LIFE, 0);
				this.shockWaveMaxLife = vars.getFloat(PROP_MAX_LIFE, 0);

				float[] array = vars.getFloatArray(PROP_START_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.shockWaveStarColor = new ColorRGBA(array);

				array = vars.getFloatArray(PROP_END_COLOR, ",", ArrayFactory.toFloatArray(1F, 0.8F, 0.36F));

				this.shockWaveEndColor = new ColorRGBA(array);
			}
		}
	}

	/**
	 * @return текстура осколков.
	 */
	public String getDebrisTexture() {
		return debrisTexture;
	}

	@Override
	public GraphicEffectType getEffectType() {
		return GraphicEffectType.EXPLOSION;
	}

	/**
	 * @return цвет финиша пламени.
	 */
	public ColorRGBA getFlameEndColor() {
		return flameEndColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getFlameEndSize() {
		return flameEndSize;
	}

	/**
	 * @return время жизни пламени.
	 */
	public float getFlameMaxLife() {
		return flameMaxLife;
	}

	/**
	 * @return время жизни пламени.
	 */
	public float getFlameMinLife() {
		return flameMinLife;
	}

	/**
	 * @return цвет старта пламени.
	 */
	public ColorRGBA getFlameStarColor() {
		return flameStarColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getFlameStartSize() {
		return flameStartSize;
	}

	/**
	 * @return текстура пламени взрыва.
	 */
	public String getFlameTexture() {
		return flameTexture;
	}

	/**
	 * @return цвет финиша вспышки.
	 */
	public ColorRGBA getFlashEndColor() {
		return flashEndColor;
	}

	/**
	 * @return размер вспышки.
	 */
	public float getFlashEndSize() {
		return flashEndSize;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMaxLife() {
		return flashMaxLife;
	}

	/**
	 * @return время жизни вспышки.
	 */
	public float getFlashMinLife() {
		return flashMinLife;
	}

	/**
	 * @return цвет старта вспышки.
	 */
	public ColorRGBA getFlashStarColor() {
		return flashStarColor;
	}

	/**
	 * @return размеры вспышки.
	 */
	public float getFlashStartSize() {
		return flashStartSize;
	}

	/**
	 * @return текстура вспышки.
	 */
	public String getFlashTexture() {
		return flashTexture;
	}

	/**
	 * @return цвет завершения остаточных частиц.
	 */
	public ColorRGBA getRoundSparkEndColor() {
		return roundSparkEndColor;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkEndSize() {
		return roundSparkEndSize;
	}

	/**
	 * @return время жизни остаточных частиц,
	 */
	public float getRoundSparkMaxLife() {
		return roundSparkMaxLife;
	}

	/**
	 * @return время жизни остаточных частиц.
	 */
	public float getRoundSparkMinLife() {
		return roundSparkMinLife;
	}

	/**
	 * @return цвет старта остаточных частиц.
	 */
	public ColorRGBA getRoundSparkStartColor() {
		return roundSparkStartColor;
	}

	/**
	 * @return вразмеры остаточных частиц.
	 */
	public float getRoundSparkStartSize() {
		return roundSparkStartSize;
	}

	/**
	 * @return текстура остаточных искр.
	 */
	public String getRoundSparkTexture() {
		return roundSparkTexture;
	}

	/**
	 * @return цвет финиша волны.
	 */
	public ColorRGBA getShockWaveEndColor() {
		return shockWaveEndColor;
	}

	/**
	 * @return
	 */
	public float getShockWaveEndSize() {
		return shockWaveEndSize;
	}

	/**
	 * @return время жизни волны.
	 */
	public float getShockWaveMaxLife() {
		return shockWaveMaxLife;
	}

	/**
	 * @return время жизни волны.
	 */
	public float getShockWaveMinLife() {
		return shockWaveMinLife;
	}

	/**
	 * @return цвет старта волны.
	 */
	public ColorRGBA getShockWaveStarColor() {
		return shockWaveStarColor;
	}

	/**
	 * @return размеры пламени.
	 */
	public float getShockWaveStartSize() {
		return shockWaveStartSize;
	}

	/**
	 * @return текстура взрывной волны.
	 */
	public String getShockWaveTexture() {
		return shockWaveTexture;
	}

	/**
	 * @return текстура дымовых частиц.
	 */
	public String getSmokeTrailTexture() {
		return smokeTrailTexture;
	}

	/**
	 * @return цвет завершения частиц.
	 */
	public ColorRGBA getSparkEndColor() {
		return sparkEndColor;
	}

	/**
	 * @return конечный размер частиц.
	 */
	public float getSparkEndSize() {
		return sparkEndSize;
	}

	/**
	 * @return направление частиц.
	 */
	public Vector getSparkGravity() {
		return sparkGravity;
	}

	/**
	 * @return максимальное время жизни частиц.
	 */
	public float getSparkMaxLife() {
		return sparkMaxLife;
	}

	/**
	 * @return минимальное время жизни частицы.
	 */
	public float getSparkMinLife() {
		return sparkMinLife;
	}

	/**
	 * @return цвет старта частиц.
	 */
	public ColorRGBA getSparkStartColor() {
		return sparkStartColor;
	}

	/**
	 * @return стартовый размер частиц.
	 */
	public float getSparkStartSize() {
		return sparkStartSize;
	}

	/**
	 * @return текстура разлетающихся искр.
	 */
	public String getSparkTexture() {
		return sparkTexture;
	}

	/**
	 * @return есть ли разлетающиеся куски.
	 */
	public boolean isHasDebris() {
		return hasDebris;
	}

	/**
	 * @return есть ли огонь.
	 */
	public boolean isHasFlame() {
		return hasFlame;
	}

	/**
	 * @return есть ли вспышка.
	 */
	public boolean isHasFlash() {
		return hasFlash;
	}

	/**
	 * @return есть ли остаточные искры.
	 */
	public boolean isHasRoundSpark() {
		return hasRoundSpark;
	}

	/**
	 * @return есть ли взрывная волна.
	 */
	public boolean isHasShockWave() {
		return hasShockWave;
	}

	/**
	 * @return есть ли разлетающийся дым.
	 */
	public boolean isHasSmokeTrail() {
		return hasSmokeTrail;
	}

	/**
	 * @return есть ли разлетающиеся искры.
	 */
	public boolean isHasSpark() {
		return hasSpark;
	}

	@Override
	public void writeMe(final ByteBuffer buffer, final ServerPacket packet) {
		super.writeMe(buffer, packet);

		packet.writeByte(buffer, isHasFlame() ? 1 : 0);

		if(isHasFlame()) {

			final String texture = getFlameTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			packet.writeFloat(buffer, getFlameStartSize());
			packet.writeFloat(buffer, getFlameEndSize());

			packet.writeFloat(buffer, getFlameMinLife());
			packet.writeFloat(buffer, getFlameMaxLife());

			ColorRGBA color = getFlameStarColor();

			packet.writeFloat(buffer, color.getRed());
			packet.writeFloat(buffer, color.getGreen());
			packet.writeFloat(buffer, color.getBlue());

			color = getFlameEndColor();

			packet.writeFloat(buffer, color.getRed());
			packet.writeFloat(buffer, color.getGreen());
			packet.writeFloat(buffer, color.getBlue());
		}

		packet.writeByte(buffer, isHasFlash() ? 1 : 0);

		if(isHasFlash()) {

			final String texture = getFlashTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			ColorRGBA color = getFlashStarColor();

			packet.writeFloat(buffer, color.getRed());
			packet.writeFloat(buffer, color.getGreen());
			packet.writeFloat(buffer, color.getBlue());

			color = getFlashEndColor();

			packet.writeFloat(buffer, color.getRed());
			packet.writeFloat(buffer, color.getGreen());
			packet.writeFloat(buffer, color.getBlue());

			final float flashMinLife = getFlashMinLife();
			final float flashMaxLife = getFlashMaxLife();

			packet.writeFloat(buffer, flashMinLife);
			packet.writeFloat(buffer, flashMaxLife);

			final float flashStartSize = getFlashStartSize();
			final float flashEndSize = getFlashEndSize();

			packet.writeFloat(buffer, flashStartSize);
			packet.writeFloat(buffer, flashEndSize);
		}

		packet.writeByte(buffer, isHasRoundSpark() ? 1 : 0);

		if(isHasRoundSpark()) {

			final String texture = getRoundSparkTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			final ColorRGBA roundSparkStartColor = getRoundSparkStartColor();

			packet.writeFloat(buffer, roundSparkStartColor.getRed());
			packet.writeFloat(buffer, roundSparkStartColor.getGreen());
			packet.writeFloat(buffer, roundSparkStartColor.getBlue());

			final ColorRGBA roundSparkEndColor = getRoundSparkEndColor();

			packet.writeFloat(buffer, roundSparkEndColor.getRed());
			packet.writeFloat(buffer, roundSparkEndColor.getGreen());
			packet.writeFloat(buffer, roundSparkEndColor.getBlue());

			packet.writeFloat(buffer, getRoundSparkStartSize());
			packet.writeFloat(buffer, getRoundSparkEndSize());

			packet.writeFloat(buffer, getRoundSparkMinLife());
			packet.writeFloat(buffer, getRoundSparkMaxLife());
		}

		packet.writeByte(buffer, isHasDebris() ? 1 : 0);

		if(isHasDebris()) {

			final String texture = getDebrisTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);
		}

		packet.writeByte(buffer, isHasShockWave() ? 1 : 0);

		if(isHasShockWave()) {

			final String texture = getShockWaveTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			packet.writeFloat(buffer, getShockWaveStartSize());
			packet.writeFloat(buffer, getShockWaveEndSize());
			packet.writeFloat(buffer, getShockWaveMinLife());
			packet.writeFloat(buffer, getShockWaveMaxLife());

			ColorRGBA color = getShockWaveStarColor();

			packet.writeFloat(buffer, color.getRed());
			packet.writeFloat(buffer, color.getGreen());
			packet.writeFloat(buffer, color.getBlue());

			color = getShockWaveEndColor();

			packet.writeFloat(buffer, color.getRed());
			packet.writeFloat(buffer, color.getGreen());
			packet.writeFloat(buffer, color.getBlue());
		}

		packet.writeByte(buffer, isHasSmokeTrail() ? 1 : 0);

		if(isHasSmokeTrail()) {

			final String texture = getSmokeTrailTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);
		}

		packet.writeByte(buffer, isHasSpark() ? 1 : 0);

		if(isHasSpark()) {

			final String texture = getSparkTexture();

			packet.writeByte(buffer, texture.length());
			packet.writeString(buffer, texture);

			final ColorRGBA sparkStartColor = getSparkStartColor();

			packet.writeFloat(buffer, sparkStartColor.getRed());
			packet.writeFloat(buffer, sparkStartColor.getGreen());
			packet.writeFloat(buffer, sparkStartColor.getBlue());

			final ColorRGBA sparkEndColor = getSparkEndColor();

			packet.writeFloat(buffer, sparkEndColor.getRed());
			packet.writeFloat(buffer, sparkEndColor.getGreen());
			packet.writeFloat(buffer, sparkEndColor.getBlue());

			final float sparkStartSize = getSparkStartSize();
			final float sparkEndSize = getSparkEndSize();

			packet.writeFloat(buffer, sparkStartSize);
			packet.writeFloat(buffer, sparkEndSize);

			final Vector sparkGravity = getSparkGravity();

			packet.writeFloat(buffer, sparkGravity.getX());
			packet.writeFloat(buffer, sparkGravity.getY());
			packet.writeFloat(buffer, sparkGravity.getZ());

			final float sparkMaxLife = getSparkMaxLife();
			final float sparkMinLife = getSparkMinLife();

			packet.writeFloat(buffer, sparkMinLife);
			packet.writeFloat(buffer, sparkMaxLife);
		}
	}
}

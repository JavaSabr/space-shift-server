package com.ss.server.template.ship;

import rlib.geom.Vector;
import rlib.geom.bounding.Bounding;
import rlib.geom.bounding.BoundingFactory;
import rlib.util.ClassUtils;
import rlib.util.VarTable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.func.Func;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.module.system.SlotInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.SpaceShipType;
import com.ss.server.template.ObjectTemplate;

/**
 * Шаблон космического корабля.
 * 
 * @author Ronn
 */
public abstract class ShipTemplate extends ObjectTemplate {

	public static final String FUNCS = "funcs";
	public static final String SLOTS = "slots";

	private static final SlotInfo[] EMPTY_SLOTS = new SlotInfo[0];
	private static final Func[] EMPTY_FUNCS = new Func[0];

	/** пул модульных систем */
	protected final FoldablePool<ModuleSystem> moduleSystemPool;

	/** набор слотов для модулей. */
	protected SlotInfo[] slots;

	/** набор функций шаблона */
	protected Func[] funcs;

	public ShipTemplate(final VarTable vars) {
		super(vars);

		this.slots = vars.get(SLOTS, SlotInfo[].class, EMPTY_SLOTS);
		this.funcs = vars.get(FUNCS, Func[].class, EMPTY_FUNCS);

		this.moduleSystemPool = PoolFactory.newAtomicFoldablePool(ModuleSystem.class);
	}

	/**
	 * Выдача функций кораблю.
	 */
	public final void addFuncsTo(final SpaceShip ship) {

		final Func[] funcs = getFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(final Func func : funcs) {
			func.addMe(ship);
		}
	}

	/**
	 * @return функции темплейта.
	 */
	protected final Func[] getFuncs() {
		return funcs;
	}

	/**
	 * @return класс используемоей системы модулей.
	 */
	public abstract Class<? extends ModuleSystem> getModuleSystemClass();

	/**
	 * @return набор слотов для модулей.
	 */
	public final SlotInfo[] getSlots() {
		return slots;
	}

	@Override
	public SpaceShipType getType() {
		return (SpaceShipType) super.getType();
	}

	@Override
	protected SpaceObjectType getType(final VarTable vars) {
		return vars.getEnum(PROP_TYPE, SpaceShipType.class);
	}

	@Override
	public Bounding newBounding(final Vector center) {
		return BoundingFactory.newBoundingBox(center, Vector.ZERO, sizeX, sizeY, sizeZ);
	}

	/**
	 * Сохранение использованной системы модулей.
	 */
	public void putModuleSystem(final ModuleSystem system) {
		moduleSystemPool.put(system);
	}

	/**
	 * Удаление функций кораблю.
	 */
	public final void removeFuncsTo(final SpaceShip ship) {

		final Func[] funcs = getFuncs();

		if(funcs == null || funcs.length < 1) {
			return;
		}

		for(final Func func : funcs) {
			func.removeMe(ship);
		}
	}

	/**
	 * @return новый экземпляр системы модулей.
	 */
	public ModuleSystem takeModuleSystem() {

		ModuleSystem system = moduleSystemPool.take();

		if(system == null) {
			system = ClassUtils.newInstance(getModuleSystemClass());
			system.init(slots);
		}

		return system;
	}

	@Override
	public String toString() {
		return super.toString() + " moduleSystemPool = " + moduleSystemPool;
	}
}

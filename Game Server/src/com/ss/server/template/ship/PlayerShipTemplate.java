package com.ss.server.template.ship;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.module.system.impl.PlayerModuleSystem;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponsePlayerShiptTemplate;

/**
 * Шаблон корабля игрока.
 * 
 * @author Ronn
 */
public final class PlayerShipTemplate extends ShipTemplate {

	public static final String CLASS = "class";

	public PlayerShipTemplate(final VarTable vars) {
		super(vars);
	}

	@Override
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return ResponsePlayerShiptTemplate.getInstance(this, local);
	}

	@Override
	public Class<? extends ModuleSystem> getModuleSystemClass() {
		return PlayerModuleSystem.class;
	}
}

package com.ss.server.template.ship;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.module.system.impl.NpsModuleSystem;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseNpsTemplate;

/**
 * Шаблон кораблей не игроков.
 * 
 * @author Ronn
 */
public class NpsTemplate extends ShipTemplate {

	public static final String EXP = "exp";
	public static final String LEVEL = "level";
	public static final String NAME = "name";

	/** название корабля */
	private final String name;

	/** уровень корабля */
	private final int level;

	/** кол-во опыта за уничтожение */
	private final int exp;

	public NpsTemplate(final VarTable vars) {
		super(vars);

		this.name = vars.getString(NAME);
		this.level = vars.getInteger(LEVEL);
		this.exp = vars.getInteger(EXP, 0);
	}

	/**
	 * @return кол-во опыта за уничтожение.
	 */
	public int getExp() {
		return exp;
	}

	@Override
	public ServerPacket getInfoPacket(final LocalObjects local) {
		return ResponseNpsTemplate.getInstance(this, local);
	}

	/**
	 * @return уровень корабля.
	 */
	public int getLevel() {
		return level;
	}

	@Override
	public Class<? extends ModuleSystem> getModuleSystemClass() {
		return NpsModuleSystem.class;
	}

	/**
	 * @return название корабля.
	 */
	public String getName() {
		return name;
	}
}

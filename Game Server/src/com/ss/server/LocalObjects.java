package com.ss.server;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.random.Random;
import rlib.util.random.RandomFactory;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.impl.Env;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.ship.ForceShield;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketFactory;
import com.ss.server.util.FastVectorBuffer;

/**
 * Контейнер локальных объектов для серверного потока.
 * 
 * @author Ronn
 */
public final class LocalObjects implements VectorBuffer {

	private static final int DEFAULT_BUFFER_SIZE = 80;

	public static final LocalObjects get() {
		return ((ServerThread) Thread.currentThread()).getLocal();
	}

	/** фабрика серверных пакетов */
	private final ServerPacketFactory packetFactory;
	/** контейнер локальных событий рейтинга */
	private final RatingEvents localRatingEvents;

	/** поток, за которым закреплен контейнер */
	private final Thread thread;
	/** локальный рандоминайзер */
	private final Random random;

	/** набор дополнительных векторов */
	private final Vector[] vectors;
	/** набор дополнительных разворотов */
	private final Rotation[] rotations;
	/** набор буфферных классов */
	private final Env[] envs;
	/** набор контейнеров инфы о воздействии */
	private final ImpactInfo[] impactInfos;
	/** набор промежуточных квестовых событий */
	private final QuestEvent[] questEvents;
	/** массив векторных буферов */
	private final VectorBuffer[] vectorBuffers;

	/** набор списков объектов */
	private final Array<SpaceObject>[] objectLists;
	/** набор списков предметов */
	private final Array<SpaceItem>[] itemLists;
	/** набор списков NPS'ов */
	private final Array<Nps>[] npsLists;
	/** набор списков игроков */
	private final Array<PlayerShip>[] playersLists;
	/** набор списков заданий */
	private final Array<Quest>[] questLists;
	/** набор списков кнопок заданий */
	private final Array<QuestButton>[] questButtonLists;
	/** набор списков силовых полей */
	private final Array<ForceShield>[] forceShieldLists;
	/** набор списков векторов */
	private final Array<Vector>[] vectorLists;

	/** лимит буфера */
	private final int limit;

	/** номер след. свободного буферного класса */
	private int envOrder;
	/** номер след. свободного контейнера ины о воздействии */
	private int impactInfoOrder;
	/** номер след. свободного квестового события */
	private int questEventOrder;
	/** номер след. вектора */
	private int vectorOrder;
	/** номер след. разворота */
	private int rotationOrder;
	/** индекс след. свободного списка объектов */
	private int objectListOrder;
	/** индекс след. свободного списка предметов */
	private int itemListOrder;
	/** индекс следующего свободного списка NPS */
	private int npsListOrder;
	/** индекс следующего свободного списка игроков */
	private int playerListOrder;
	/** индекс следующего свободного списка заданий */
	private int questListOrder;
	/** индекс следующего свободного списка кнопок заданий */
	private int questButtonListOrder;
	/** индекс следующего свободного списка векторов */
	private int vectorListOrder;
	/** индекс следующего векторного буфера */
	private int vectorBufferOrder;
	/** индекс следующего списка силовых щитов */
	private int forceShieldOrder;

	@SuppressWarnings("unchecked")
	public LocalObjects(final Thread thread) {
		this.thread = thread;
		this.limit = DEFAULT_BUFFER_SIZE - 2;
		this.random = RandomFactory.newRealRandom();
		this.packetFactory = new ServerPacketFactory();
		this.localRatingEvents = new RatingEvents();

		this.vectors = new Vector[DEFAULT_BUFFER_SIZE];
		this.rotations = new Rotation[DEFAULT_BUFFER_SIZE];
		this.envs = new Env[DEFAULT_BUFFER_SIZE];
		this.objectLists = new Array[DEFAULT_BUFFER_SIZE];
		this.itemLists = new Array[DEFAULT_BUFFER_SIZE];
		this.questEvents = new QuestEvent[DEFAULT_BUFFER_SIZE];
		this.impactInfos = new ImpactInfo[DEFAULT_BUFFER_SIZE];
		this.npsLists = new Array[DEFAULT_BUFFER_SIZE];
		this.playersLists = new Array[DEFAULT_BUFFER_SIZE];
		this.questLists = new Array[DEFAULT_BUFFER_SIZE];
		this.questButtonLists = new Array[DEFAULT_BUFFER_SIZE];
		this.forceShieldLists = new Array[DEFAULT_BUFFER_SIZE];
		this.vectorLists = new Array[DEFAULT_BUFFER_SIZE];
		this.vectorBuffers = new VectorBuffer[DEFAULT_BUFFER_SIZE];

		for(int i = 0, length = envs.length; i < length; i++) {
			vectors[i] = Vector.newInstance();
			rotations[i] = Rotation.newInstance();
			envs[i] = new Env();
			impactInfos[i] = new ImpactInfo();
			questEvents[i] = new QuestEvent();
			objectLists[i] = ArrayFactory.newArray(SpaceObject.class);
			itemLists[i] = ArrayFactory.newArray(SpaceItem.class);
			npsLists[i] = ArrayFactory.newArray(Nps.class);
			playersLists[i] = ArrayFactory.newArray(PlayerShip.class);
			questLists[i] = ArrayFactory.newArray(Quest.class);
			questButtonLists[i] = ArrayFactory.newArray(QuestButton.class);
			forceShieldLists[i] = ArrayFactory.newArray(ForceShield.class);
			vectorLists[i] = ArrayFactory.newArray(Vector.class);
			vectorBuffers[i] = new FastVectorBuffer();
		}
	}

	/**
	 * Создание нового серверного пакета соотвествующего класса указанному.
	 * 
	 * @param example пример создаваемого пакета.
	 * @return новый пакет соотвествующего класса.
	 */
	public <R extends ServerPacket> R create(final ServerPacket example) {
		return packetFactory.create(example);
	}

	/**
	 * @return контейнер локальных событий рейтинга.
	 */
	public RatingEvents getLocalRatingEvents() {
		return localRatingEvents;
	}

	/**
	 * @return след. свободный вспомогательный класс.
	 */
	public Env getNextEnv() {

		if(envOrder > limit) {
			envOrder = 0;
		}

		return envs[envOrder++].clear();
	}

	/**
	 * @return следующий список силовых полей.
	 */
	public Array<ForceShield> getNextForceShieldList() {

		if(forceShieldOrder > limit) {
			forceShieldOrder = 0;
		}

		return forceShieldLists[forceShieldOrder++].clear();
	}

	/**
	 * @return след. свободный контейнер.
	 */
	public ImpactInfo getNextImpactInfo() {

		if(impactInfoOrder > limit) {
			impactInfoOrder = 0;
		}

		return impactInfos[impactInfoOrder++].clear();
	}

	/**
	 * @return след. свободный список предметов.
	 */
	public Array<SpaceItem> getNextItemList() {

		if(itemListOrder > limit) {
			itemListOrder = 0;
		}

		return itemLists[itemListOrder++].clear();
	}

	/**
	 * @return след. свободный список NPS'ов.
	 */
	public Array<Nps> getNextNpsList() {

		if(npsListOrder > limit) {
			npsListOrder = 0;
		}

		return npsLists[npsListOrder++].clear();
	}

	/**
	 * @return след. свободный список объектов.
	 */
	public Array<SpaceObject> getNextObjectList() {

		if(objectListOrder > limit) {
			objectListOrder = 0;
		}

		return objectLists[objectListOrder++].clear();
	}

	/**
	 * @return след. свободный список игроков.
	 */
	public Array<PlayerShip> getNextPlayerList() {

		if(playerListOrder > limit) {
			playerListOrder = 0;
		}

		return playersLists[playerListOrder++].clear();
	}

	/**
	 * @return след. свободный список кнопок заданий.
	 */
	public Array<QuestButton> getNextQuestButtonList() {

		if(questButtonListOrder > limit) {
			questButtonListOrder = 0;
		}

		return questButtonLists[questButtonListOrder++].clear();
	}

	/**
	 * @return след. свободное квестовое событие.
	 */
	public QuestEvent getNextQuestEvent() {

		if(questEventOrder > limit) {
			questEventOrder = 0;
		}

		return questEvents[questEventOrder++].clear();
	}

	/**
	 * @return след. свободный список заданий.
	 */
	public Array<Quest> getNextQuestList() {

		if(questListOrder > limit) {
			questListOrder = 0;
		}

		return questLists[questListOrder++].clear();
	}

	/**
	 * @return след. свободный вспомогательный класс.
	 */
	public Rotation getNextRotation() {

		if(rotationOrder > limit) {
			rotationOrder = 0;
		}

		return rotations[rotationOrder++];
	}

	/**
	 * @return след. свободный вспомогательный класс.
	 */
	@Override
	public Vector getNextVector() {

		if(vectorOrder > limit) {
			vectorOrder = 0;
		}

		return vectors[vectorOrder++];
	}

	/**
	 * @return следующий свободный буффер.
	 */
	public VectorBuffer getNextVectorBuffer() {

		if(vectorBufferOrder > limit) {
			vectorBufferOrder = 0;
		}

		return vectorBuffers[vectorBufferOrder++];
	}

	/**
	 * @return след. свободный список векторов.
	 */
	public Array<Vector> getNextVectorList() {

		if(vectorListOrder > limit) {
			vectorListOrder = 0;
		}

		return vectorLists[vectorListOrder++].clear();
	}

	/**
	 * @return локальный рандоминайзер.
	 */
	public Random getRandom() {
		return random;
	}

	/**
	 * @return поток, за которым закреплен контейнер.
	 */
	public Thread getThread() {
		return thread;
	}
}

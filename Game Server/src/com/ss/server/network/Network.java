package com.ss.server.network;

import java.net.InetSocketAddress;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.network.NetworkFactory;
import rlib.network.client.ClientNetwork;
import rlib.network.server.ServerNetwork;
import rlib.util.SafeTask;

import com.ss.server.Config;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.network.game.ClientPacketType;
import com.ss.server.network.game.model.GameAcceptHandler;
import com.ss.server.network.game.model.GameNetworkConfig;
import com.ss.server.network.login.ServerPacketType;
import com.ss.server.network.login.model.LoginConnectHandler;
import com.ss.server.network.login.model.LoginServer;

/**
 * Сеть сервера.
 * 
 * @author Ronn
 */
public final class Network {

	public static Network getInstance() {

		if(instance == null) {
			instance = new Network();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(Network.class);

	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();

	/** адресс удаленного логин сервера */
	// private static final InetSocketAddress LOGIN_ADRESS = new
	// InetSocketAddress(Config.SERVER_LOGIN_HOST,
	// Config.SERVER_LOGIN_SERVER_PORT);
	/** адресс локального логин сервера */
	private static final InetSocketAddress LOCAL_ADRESS = new InetSocketAddress("localhost", Config.SERVER_LOGIN_PORT);

	/** экземпляр сети */
	private static Network instance;

	/** сеть для подключения к логин серверу */
	private final ClientNetwork loginNetwork;
	/** серверная сеть */
	private final ServerNetwork serverNetwork;

	/** логин сервер */
	private volatile LoginServer loginServer;

	public Network() {
		InitializeManager.valid(getClass());

		try {

			ServerPacketType.init();
			ClientPacketType.init();

			final SafeTask task = new SafeTask() {

				@Override
				public void runImpl() {

					LoginServer server = getLoginServer();

					if(server == null) {
						synchronized(this) {

							server = getLoginServer();

							if(server == null) {
								connectToLoginServer();
							}
						}
					}
				}
			};

			serverNetwork = NetworkFactory.newDefaultAsynchronousServerNetwork(GameNetworkConfig.getInstance(), GameAcceptHandler.getInstance());
			serverNetwork.bind(new InetSocketAddress(Config.SERVER_GAME_PORT));

			loginNetwork = NetworkFactory.newDefaultAsynchronousClientNetwork(GameNetworkConfig.getInstance(), LoginConnectHandler.getInstance());

			EXECUTOR_MANAGER.scheduleGeneralAtFixedRate(task, 500, 1000);

			LOGGER.info("initialized.");

		} catch(final Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Проба подключиться к логин серверу.
	 */
	public void connectToLoginServer() {
		loginNetwork.connect(LOCAL_ADRESS);
	}

	/**
	 * @return сеть логин сервера.
	 */
	public ClientNetwork getLoginNetwork() {
		return loginNetwork;
	}

	/**
	 * @return логин сервер.
	 */
	public final LoginServer getLoginServer() {
		return loginServer;
	}

	/**
	 * @return сеть гейм сервера.
	 */
	public ServerNetwork getServerNetwork() {
		return serverNetwork;
	}

	/**
	 * @param loginServer логин сервер.
	 */
	public final void setLoginServer(final LoginServer loginServer) {
		this.loginServer = loginServer;
	}
}

package com.ss.server.network.login.packet.client;

import java.nio.ByteBuffer;

import com.ss.server.Config;
import com.ss.server.network.login.ClientPacket;
import com.ss.server.network.login.ClientPacketType;

/**
 * Пакет с данными для регистрации игрового сервера на сервере авторизации.
 * 
 * @author Ronn
 */
public class ResponseRegister extends ClientPacket {

	public static ResponseRegister getInstance() {
		return (ResponseRegister) instance.newInstance();
	}

	private static final ResponseRegister instance = new ResponseRegister();

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.RESPONSE_REGISTER;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final String name = Config.SERVER_NAME;
		final String type = Config.SERVER_TYPE;
		final String host = Config.SERVER_GAME_HOST;

		final int port = Config.SERVER_GAME_PORT;
		final int online = 0;

		final float loaded = 0F;

		int length = name.length();

		writeByte(buffer, length);
		writeString(buffer, name);

		length = type.length();

		writeByte(buffer, length);
		writeString(buffer, type);

		length = host.length();

		writeByte(buffer, length);
		writeString(buffer, host);
		writeInt(buffer, port);
		writeFloat(buffer, loaded);
		writeInt(buffer, online);
	}
}

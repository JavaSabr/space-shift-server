package com.ss.server.network.login.packet.client;

import java.nio.ByteBuffer;

import com.ss.server.network.login.ClientPacket;
import com.ss.server.network.login.ClientPacketType;

/**
 * Серверный пакет с запросом проверки корректности аккаунта.
 * 
 * @author Ronn
 */
public class RequestCheckAccount extends ClientPacket {

	public static RequestCheckAccount getInstance(final String name, final String password, final int waitId) {

		final RequestCheckAccount packet = (RequestCheckAccount) instance.newInstance();

		packet.name = name;
		packet.password = password;
		packet.waitId = waitId;

		return packet;
	}

	private static final RequestCheckAccount instance = new RequestCheckAccount();

	/** имя аккаунта */
	private String name;
	/** пароль аккаунта */
	private String password;

	/** ид ожидания */
	private int waitId;

	@Override
	public void finalyze() {
		name = null;
		password = null;
	}

	@Override
	public ClientPacketType getPacketType() {
		return ClientPacketType.REQUEST_CHECK_ACCOUNT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, waitId);
		writeByte(buffer, name.length());
		writeString(buffer, name);
		writeByte(buffer, password.length());
		writeString(buffer, password);
	}
}

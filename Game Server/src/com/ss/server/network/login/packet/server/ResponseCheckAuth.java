package com.ss.server.network.login.packet.server;

import com.ss.server.LocalObjects;
import com.ss.server.manager.AccountManager;
import com.ss.server.network.login.ServerPacket;

/**
 * Результат проверки аккаунта подключившегося игрока.
 * 
 * @author Ronn
 */
public class ResponseCheckAuth extends ServerPacket {

	/** имя аккаунта */
	private String name;
	/** пароль аккаунта */
	private String password;

	/** ид ожидающего клиента */
	private int waitId;

	/** корректный ли аккаунт */
	private boolean correctly;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {
		final AccountManager accountManager = AccountManager.getInstance();
		accountManager.executeAuth(waitId, name, password, correctly);
	}

	@Override
	public void finalyze() {
		name = null;
		password = null;
	}

	@Override
	protected void readImpl() {
		waitId = readInt();
		name = readString(readByte());
		password = readString(readByte());
		correctly = readByte() == 1;
	}

	@Override
	public String toString() {
		return "ResponseCheckAuth [name=" + name + ", password=" + password + ", waitId=" + waitId + ", correctly=" + correctly + "]";
	}
}

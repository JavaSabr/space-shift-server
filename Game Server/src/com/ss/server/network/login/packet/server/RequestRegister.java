package com.ss.server.network.login.packet.server;

import com.ss.server.LocalObjects;
import com.ss.server.network.login.ServerPacket;
import com.ss.server.network.login.model.LoginServer;
import com.ss.server.network.login.packet.client.ResponseRegister;

/**
 * Запрос от логин сервера на получение инфы о сервере для его регистрации.
 * 
 * @author Ronn
 */
public class RequestRegister extends ServerPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {
		final LoginServer server = getOwner();
		server.sendPacket(ResponseRegister.getInstance(), true);
	}
}

package com.ss.server.network.login;

import java.util.HashSet;
import java.util.Set;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

/**
 * Перечисление типов отправляемых гейм сервером пакетов на логин сервер.
 * 
 * @author Ronn
 */
public enum ClientPacketType {
	/** отправка инфы о гейм сервере */
	RESPONSE_REGISTER(0x2000),
	/** запрос на проверку корректности аккаунта */
	REQUEST_CHECK_ACCOUNT(0x2001), ;

	/**
	 * Инициализация серверных пакетов.
	 */
	public static final void init() {

		final Set<Integer> set = new HashSet<Integer>();

		for(final ClientPacketType element : VALUES) {
			if(set.contains(element.opcode)) {
				log.warning("found duplicate opcode " + Integer.toHexString(element.opcode).toUpperCase());
			}

			set.add(element.opcode);
		}

		log.info("client packets prepared.");
	}

	private static final Logger log = LoggerManager.getLogger(ClientPacketType.class);
	/** массив всех типов пакетов */
	public static final ClientPacketType[] VALUES = values();

	/** кол-во всех типов пакетов */
	public static final int SIZE = VALUES.length;

	/** пул пакетов */
	private final FoldablePool<ClientPacket> pool;

	/** опкод пакета */
	private final int opcode;

	/**
	 * @param opcode опкод пакета.
	 */
	private ClientPacketType(final int opcode) {
		this.opcode = opcode;
		this.pool = PoolFactory.newAtomicFoldablePool(ClientPacket.class);
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * @return пул пакетов.
	 */
	public final FoldablePool<ClientPacket> getPool() {
		return pool;
	}
}

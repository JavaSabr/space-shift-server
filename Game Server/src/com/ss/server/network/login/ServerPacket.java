package com.ss.server.network.login;

import rlib.network.packet.impl.AbstractTaskReadeablePacket;
import rlib.util.ClassUtils;
import rlib.util.pools.FoldablePool;

import com.ss.server.LocalObjects;
import com.ss.server.network.login.model.LoginServer;

/**
 * Базовая реализация пакета LoginServer -> GameServer.
 * 
 * @author Ronn
 */
public abstract class ServerPacket extends AbstractTaskReadeablePacket<LoginServer, LocalObjects> {

	/** типа пакета */
	private ServerPacketType type;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {
	}

	@Override
	protected final FoldablePool<ServerPacket> getPool() {
		return type.getPool();
	}

	public final ServerPacket newInstance() {

		ServerPacket packet = getPool().take();

		if(packet == null) {
			packet = ClassUtils.newInstance(getClass());
			packet.type = type;
		}

		return packet;
	}

	@Override
	protected void readImpl() {
	}

	/**
	 * @param type тип пакета.
	 */
	public final void setPacketType(final ServerPacketType type) {
		this.type = type;
	}
}

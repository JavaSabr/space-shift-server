package com.ss.server.network.login;

import java.nio.ByteBuffer;

import rlib.network.packet.impl.AbstractReusableSendablePacket;
import rlib.util.ClassUtils;
import rlib.util.pools.FoldablePool;

import com.ss.server.network.login.model.LoginServer;

/**
 * Базовая реализация пакета GameServer -> LoginServer.
 * 
 * @author Ronn
 */
public abstract class ClientPacket extends AbstractReusableSendablePacket<LoginServer> {

	/** тип пакета */
	private final ClientPacketType type;

	public ClientPacket() {
		this.type = getPacketType();
	}

	@Override
	protected void completeImpl() {
		getPool().put(this);
	}

	/**
	 * @return тип пакета.
	 */
	public abstract ClientPacketType getPacketType();

	/**
	 * @return получение пула для соотвествующего пакета.
	 */
	protected FoldablePool<ClientPacket> getPool() {
		return type.getPool();
	}

	/**
	 * @return новый экземпляр пакета.
	 */
	public final ClientPacket newInstance() {

		ClientPacket packet = getPool().take();

		if(packet == null) {
			packet = ClassUtils.newInstance(getClass());
		}

		return packet;
	}

	@Override
	public final void writeHeader(final ByteBuffer buffer, final int length) {
		buffer.putShort(0, (short) length);
	}

	/**
	 * Запись опкода пакета.
	 */
	protected final void writeOpcode(final ByteBuffer buffer) {
		writeShort(buffer, type.getOpcode());
	}

	@Override
	public final void writePosition(final ByteBuffer buffer) {
		buffer.position(2);
	}
}

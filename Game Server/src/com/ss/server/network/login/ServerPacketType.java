package com.ss.server.network.login;

import java.util.HashSet;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.network.login.packet.server.RequestRegister;
import com.ss.server.network.login.packet.server.ResponseCheckAuth;

/**
 * Перечисление типов клиентских пакетов.
 * 
 * @author Ronn
 */
public enum ServerPacketType {
	REQUEST_REGISTER(0x2000, new RequestRegister()),
	RESPONSE_CHECK_AUTH(0x2001, new ResponseCheckAuth()), ;

	/**
	 * Возвращает новый экземпляр пакета в соответствии с опкодом
	 * 
	 * @param opcode опкод пакета.
	 * @return экземпляр нужного пакета.
	 */
	public static ServerPacket getPacketForOpcode(final int opcode) {
		final ServerPacket packet = packets[opcode];
		return packet == null ? null : packet.newInstance();
	}

	/**
	 * Инициализация клиентских пакетов.
	 */
	public static void init() {
		final HashSet<Integer> set = new HashSet<Integer>();

		for(final ServerPacketType packet : values()) {
			final int index = packet.getOpcode();

			if(set.contains(index)) {
				log.warning("found duplicate opcode " + index + " or " + Integer.toHexString(packet.getOpcode()) + "!");
			}

			set.add(index);
		}

		set.clear();

		packets = new ServerPacket[Short.MAX_VALUE * 2];

		for(final ServerPacketType packet : values()) {
			packets[packet.getOpcode()] = packet.packet;
		}

		log.info("server packets prepared.");
	}

	private static final Logger log = LoggerManager.getLogger(ServerPacketType.class);

	/** массив пакетов */
	private static ServerPacket[] packets;

	/** пул пакетов */
	private final FoldablePool<ServerPacket> pool;

	/** экземпляр пакета */
	private final ServerPacket packet;

	/** опкод пакета */
	private final int opcode;

	/**
	 * @param opcode опкод пакета.
	 * @param packet экземпляр пакета.
	 */
	private ServerPacketType(final int opcode, final ServerPacket packet) {
		this.opcode = opcode;
		this.packet = packet;
		this.packet.setPacketType(this);
		this.pool = PoolFactory.newAtomicFoldablePool(ServerPacket.class);
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * @return пул пакетов.
	 */
	public final FoldablePool<ServerPacket> getPool() {
		return pool;
	}
}
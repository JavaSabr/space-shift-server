package com.ss.server.network.login.model;

import rlib.network.client.server.impl.AbstractServer;

import com.ss.server.manager.ExecutorManager;
import com.ss.server.network.NetCrypt;
import com.ss.server.network.Network;
import com.ss.server.network.login.ClientPacket;
import com.ss.server.network.login.ServerPacket;

/**
 * Реализация серва авторизации.
 * 
 * @author Ronn
 */
public class LoginServer extends AbstractServer<LoginConnection, NetCrypt, ServerPacket, ClientPacket> {

	public LoginServer(final LoginConnection connection) {
		super(connection, new NetCrypt());
	}

	@Override
	public void close() {
		super.close();

		final Network network = Network.getInstance();
		network.setLoginServer(null);

		LOGGER.info(this, "close connect to Login Server.");
	}

	@Override
	protected void execute(final ServerPacket packet) {

		final ExecutorManager executor = ExecutorManager.getInstance();

		if(packet.isSynchronized()) {
			executor.runSyncPacket(packet);
		} else {
			executor.runAsyncPacket(packet);
		}
	}

	/**
	 * Отправка пакета.
	 * 
	 * @param packet отправляемый пакет.
	 * @param increaseSend нужно ли увеличивать счетчик отправок.
	 */
	public final void sendPacket(final ClientPacket packet, final boolean increaseSend) {

		if(increaseSend) {
			packet.increaseSends();
		}

		sendPacket(packet);
	}
}

package com.ss.server.network.login.model;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

import rlib.network.client.ClientNetwork;
import rlib.network.client.server.impl.AbstractServerConnection;
import rlib.util.Util;

import com.ss.server.Config;
import com.ss.server.network.login.ClientPacket;
import com.ss.server.network.login.ServerPacket;
import com.ss.server.network.login.ServerPacketType;

/**
 * Модель подключения к логин серверу.
 * 
 * @author Ronn
 */
public class LoginConnection extends AbstractServerConnection<LoginServer, ServerPacket, ClientPacket> {

	public LoginConnection(final ClientNetwork network, final AsynchronousSocketChannel channel, final Class<ClientPacket> sendableType) {
		super(network, channel, sendableType);
	}

	/**
	 * Получение пакета из буфера.
	 */
	private ServerPacket getPacket(final ByteBuffer buffer, final LoginServer server) {

		int opcode = -1;

		if(buffer.remaining() < 2) {
			return null;
		}

		opcode = buffer.getShort() & 0xFFFF;

		return ServerPacketType.getPacketForOpcode(opcode);
	}

	@Override
	protected ByteBuffer movePacketToBuffer(final ClientPacket packet, final ByteBuffer buffer) {

		final LoginServer server = getServer();

		buffer.clear();

		packet.writePosition(buffer);
		packet.write(buffer);

		buffer.flip();

		packet.writeHeader(buffer, buffer.limit());

		if(Config.DEVELOPER_DEBUG_LS_GS) {
			System.out.println("Login client packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):");
			System.out.println(Util.hexdump(buffer.array(), buffer.limit()));
		}

		server.encrypt(buffer, 2, buffer.limit() - 2);

		if(Config.DEVELOPER_DEBUG_LS_GS) {
			System.out.println("Login client crypt packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):");
			System.out.println(Util.hexdump(buffer.array(), buffer.limit()));
		}

		return buffer;
	}

	@Override
	protected void onWrited(final ClientPacket packet) {
		packet.complete();
	}

	@Override
	protected void readPacket(final ByteBuffer buffer) {

		final LoginServer server = getServer();

		if(Config.DEVELOPER_DEBUG_LS_GS) {
			System.out.println("Login server dump(size: " + buffer.limit() + "):\n" + Util.hexdump(buffer.array(), buffer.limit()));
		}

		int length = buffer.getShort() & 0xFFFF;

		server.decrypt(buffer, buffer.position(), length - 2);

		if(length >= buffer.remaining()) {
			server.readPacket(getPacket(buffer, server), buffer);
		} else {

			server.readPacket(getPacket(buffer, server), buffer);

			buffer.position(length);

			for(int i = 0; buffer.remaining() > 2 && i < 10; i++) {

				length += buffer.getShort() & 0xFFFF;

				server.decrypt(buffer, buffer.position(), length - 2);
				server.readPacket(getPacket(buffer, server), buffer);

				if(length < 4 || length > buffer.limit()) {
					break;
				}

				buffer.position(length);
			}
		}
	}
}

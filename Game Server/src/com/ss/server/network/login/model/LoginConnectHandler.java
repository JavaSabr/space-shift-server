package com.ss.server.network.login.model;

import java.io.IOException;
import java.net.ConnectException;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.AsynchronousSocketChannel;

import rlib.concurrent.util.ThreadUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.network.client.ConnectHandler;

import com.ss.server.network.Network;
import com.ss.server.network.login.ClientPacket;

/**
 * Обработчик подключения к логин серверу.
 * 
 * @author Ronn
 */
public class LoginConnectHandler implements ConnectHandler {

	public static LoginConnectHandler getInstance() {

		if(instance == null) {
			instance = new LoginConnectHandler();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(LoginConnectHandler.class);

	private static LoginConnectHandler instance;

	private LoginConnectHandler() {
		super();
	}

	@Override
	public void onConnect(final AsynchronousSocketChannel channel) {

		final Network network = Network.getInstance();

		final LoginConnection connection = new LoginConnection(network.getLoginNetwork(), channel, ClientPacket.class);
		final LoginServer server = new LoginServer(connection);

		network.setLoginServer(server);

		connection.setServer(server);
		connection.startRead();

		try {
			LOGGER.info("connect to Login Server " + channel.getRemoteAddress());
		} catch(final IOException e) {
			LOGGER.warning(e);
		}
	}

	@Override
	public void onFailed(final Throwable exc) {

		if(exc instanceof ConnectException || exc instanceof AsynchronousCloseException) {
			LOGGER.info("невозможно подключиться.");
		} else {
			LOGGER.warning(new Exception(exc));
		}

		ThreadUtils.sleep(5000);

		final Network network = Network.getInstance();
		network.connectToLoginServer();
	}
}

package com.ss.server.network;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.network.game.packet.server.ResponseChangeExp;
import com.ss.server.network.game.packet.server.ResponseChangeLevel;
import com.ss.server.network.game.packet.server.ResponseChangeStationRadius;
import com.ss.server.network.game.packet.server.ResponseObjectFinishTeleport;
import com.ss.server.network.game.packet.server.ResponseObjectStartTeleport;
import com.ss.server.network.game.packet.server.ResponsePlayerShipStatus;
import com.ss.server.network.game.packet.server.ResponseShipTeleportedToStation;
import com.ss.server.network.game.packet.server.ResponseUserInfo;

/**
 * Набор утильных методов по отправкам пакетов.
 * 
 * @author Ronn
 */
public final class PacketUtils {

	/**
	 * Отобразить всем то, что объект закончил телепортироваться.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param object телепортирующийся объект.
	 */
	public static void finishTeleport(final LocalObjects local, final SpaceObject object) {
		if(object.isNeedSendPacket()) {
			object.broadcastPacket(ResponseObjectFinishTeleport.getInstance(object, local));
		}
	}

	/**
	 * Уведомление о том, что корабль был телепортирован на станцию.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param playerShip корабль игрока.
	 */
	public static void shipTeleportedToStation(final LocalObjects local, final PlayerShip playerShip) {
		playerShip.sendPacket(ResponseShipTeleportedToStation.getInstance());
	}

	/**
	 * Отобразить всем то, что объект начал телепортироваться.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param object телепортирующийся объект.
	 */
	public static void startTeleport(final LocalObjects local, final SpaceObject object) {
		if(object.isNeedSendPacket()) {
			object.broadcastPacket(ResponseObjectStartTeleport.getInstance(object, local));
		}
	}

	/**
	 * Обновление опыта игрока.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param playerShip корабль игрока.
	 */
	public static void updateExp(final LocalObjects local, final PlayerShip playerShip) {
		playerShip.sendPacket(ResponseChangeExp.getInstance(playerShip.getExp(), local), true);
	}

	/**
	 * Обновление текущего статуса корабля игрока.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param playerShip корабль игрока.
	 */
	public static void updateLevel(final LocalObjects local, final PlayerShip playerShip) {
		playerShip.broadcastPacket(ResponseChangeLevel.getInstance(playerShip, playerShip.getLevel(), local));
	}

	/**
	 * Обновление индикатора нахождения в зоне действия телепорта дружественной
	 * станции.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param spaceStation станция, в радиусе которой находится/находился
	 * корабль.
	 * @param object вошедший/вышедший в радиус станции корабль.
	 * @param entered состояние индикатора.
	 */
	public static void updateStationInRange(final LocalObjects local, final SpaceStation spaceStation, final SpaceObject object, final boolean entered) {

		final SpaceShip playerShip = object.getPlayerShip();

		if(playerShip == null) {
			return;
		}

		playerShip.sendPacket(ResponseChangeStationRadius.getInstance(local, spaceStation, entered), true);
	}

	/**
	 * Обновление текущего статуса корабля игрока.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param playerShip корабль игрока.
	 */
	public static void updateStatus(final LocalObjects local, final PlayerShip playerShip) {
		playerShip.sendPacket(ResponsePlayerShipStatus.getInstance(playerShip, local), true);
	}

	/**
	 * Обновление информации о игроке.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param playerShip корабль игрока.
	 */
	public static void updateUserInfo(final LocalObjects local, final PlayerShip playerShip) {
		playerShip.sendPacket(ResponseUserInfo.getInstance(playerShip, local), true);
	}

	private PacketUtils() {
		throw new RuntimeException();
	}
}

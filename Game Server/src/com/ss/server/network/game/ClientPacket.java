package com.ss.server.network.game;

import rlib.network.packet.impl.AbstractTaskReadeablePacket;
import rlib.util.pools.FoldablePool;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.model.GameClient;

/**
 * Базовая реализация клиентского пакета.
 * 
 * @author Ronn
 */
public abstract class ClientPacket extends AbstractTaskReadeablePacket<GameClient, LocalObjects> {

	/** пул для хранения этого пакета после использования */
	private FoldablePool<ClientPacket> pool;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {
	}

	@Override
	protected final FoldablePool<ClientPacket> getPool() {
		return pool;
	}

	@Override
	protected void readImpl() {
	}

	/**
	 * @param pool пул для хранения этого пакета.
	 */
	public final void setPool(final FoldablePool<ClientPacket> pool) {
		this.pool = pool;
	}
}

package com.ss.server.network.game.model;

import rlib.network.NetworkConfig;

import com.ss.server.Config;
import com.ss.server.ServerThread;

/**
 * Конфиг сети сервера с игровыми клиентами.
 * 
 * @author Ronn
 */
public class GameNetworkConfig implements NetworkConfig {

	public static GameNetworkConfig getInstance() {

		if(instance == null) {
			instance = new GameNetworkConfig();
		}

		return instance;
	}

	private static GameNetworkConfig instance;

	@Override
	public String getGroupName() {
		return Config.NETWORK_GROUP_NAME;
	}

	@Override
	public int getGroupSize() {
		return Config.NETWORK_GROUP_SIZE;
	}

	@Override
	public int getReadBufferSize() {
		return Config.NETWORK_READ_BUFFER_SIZE;
	}

	@Override
	public Class<? extends Thread> getThreadClass() {
		return ServerThread.class;
	}

	@Override
	public int getThreadPriority() {
		return Config.NETWORK_THREAD_PRIORITY;
	}

	@Override
	public int getWriteBufferSize() {
		return Config.NETWORK_WRITE_BUFFER_SIZE;
	}

	@Override
	public boolean isVesibleReadException() {
		return false;
	}

	@Override
	public boolean isVesibleWriteException() {
		return false;
	}
}

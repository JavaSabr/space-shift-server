package com.ss.server.network.game.model;

import rlib.network.server.client.impl.AbstractClient;
import rlib.util.StringUtils;
import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.LocalObjects;
import com.ss.server.manager.AccountManager;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.model.impl.Account;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.NetCrypt;
import com.ss.server.network.PacketFactory;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.ClientPacketFactory;
import com.ss.server.network.game.ClientPacketType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.RequestAuth;

/**
 * Модель клиента игрока.
 * 
 * @author Ronn
 */
public class GameClient extends AbstractClient<Account, PlayerShip, GameConnection, NetCrypt, ClientPacket, ServerPacket> implements Foldable {

	public static final GameClient create(final GameConnection connection) {

		final GameClient client = POOL.take();

		if(client == null) {
			return new GameClient(connection, new NetCrypt());
		}

		client.switchTo(connection);

		return client;
	}

	private static final FoldablePool<GameClient> POOL = PoolFactory.newAtomicFoldablePool(GameClient.class);

	/** фабрика клиентских пакетов */
	private final PacketFactory<GameClient, ClientPacket, ClientPacketType> packetFactory;

	/** ид ожидания авторизации */
	private int waitId;

	/** авторизован ли клиент */
	private boolean authed;

	public GameClient(final GameConnection connection, final NetCrypt crypt) {
		super(connection, crypt);

		this.packetFactory = new ClientPacketFactory();
	}

	@Override
	public void close() {

		String name = StringUtils.EMPTY;

		final AccountManager accountManager = AccountManager.getInstance();

		if(!isAuthed()) {
			accountManager.removeWaitClient(this);
		} else {

			final PlayerShip owner = getOwner();

			if(owner != null) {
				owner.deleteMe(LocalObjects.get());
			}

			final Account account = getAccount();

			if(account != null) {
				name = account.getName();
				account.fold();
			}
		}

		LOGGER.info(this, "close client \"" + name + "\".");

		super.close();

		POOL.put(this);
	}

	@Override
	protected void execute(final ClientPacket packet) {

		final ExecutorManager executor = ExecutorManager.getInstance();

		if(packet.isSynchronized()) {
			executor.runSyncPacket(packet);
		} else {
			executor.runAsyncPacket(packet);
		}
	}

	@Override
	public void finalyze() {
		setOwner(null);
		setAccount(null);
	}

	@Override
	public String getHostAddress() {
		return "unknown";
	}

	/**
	 * @return фабрика клиентских пакетов.
	 */
	public PacketFactory<GameClient, ClientPacket, ClientPacketType> getPacketFactory() {
		return packetFactory;
	}

	/**
	 * @return ид ожидания авторизации.
	 */
	public final int getWaitId() {
		return waitId;
	}

	/**
	 * @return авторизован ли клиент/
	 */
	public final boolean isAuthed() {
		return authed;
	}

	@Override
	public void reinit() {
		setClosed(false);
	}

	/**
	 * Отправка пакета.
	 * 
	 * @param packet отправляемый пакет.
	 * @param increaseSend нужно ли увеличивать счетчик отправок.
	 */
	public final void sendPacket(final ServerPacket packet, final boolean increaseSend) {

		if(increaseSend) {
			packet.increaseSends();
		}

		sendPacket(packet);
	}

	/**
	 * @param authed авторизован ли клиент.
	 */
	public final void setAuthed(final boolean authed) {
		this.authed = authed;
	}

	/**
	 * @param waitId ид ожидания авторизации.
	 */
	public final void setWaitId(final int waitId) {
		this.waitId = waitId;
	}

	@Override
	public void successfulConnection() {
		sendPacket(RequestAuth.getInstance());
	}
}

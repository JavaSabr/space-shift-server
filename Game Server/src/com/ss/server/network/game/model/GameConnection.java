package com.ss.server.network.game.model;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

import rlib.network.server.ServerNetwork;
import rlib.network.server.client.impl.AbstractClientConnection;
import rlib.util.Util;

import com.ss.server.Config;
import com.ss.server.network.PacketFactory;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.ClientPacketType;
import com.ss.server.network.game.ServerPacket;

/**
 * Реализация асинхронного сетевого соединения между клиентом и сервером.
 * 
 * @author Ronn
 */
public class GameConnection extends AbstractClientConnection<GameClient, ClientPacket, ServerPacket> {

	public GameConnection(final ServerNetwork network, final AsynchronousSocketChannel channel, final Class<ServerPacket> sendableType) {
		super(network, channel, sendableType);
	}

	/**
	 * Получение пакета из буфера.
	 */
	private ClientPacket getPacket(final ByteBuffer buffer, final GameClient client) {

		if(buffer.remaining() < 2) {
			return null;
		}

		final ClientPacketType type = ClientPacketType.getPacketType(buffer.getShort() & 0xFFFF);

		if(type == null) {
			return null;
		}

		final PacketFactory<GameClient, ClientPacket, ClientPacketType> factory = client.getPacketFactory();
		return factory.create(type);
	}

	/**
	 * Получение размера следующего пакета в буфере.
	 * 
	 * @param buffer буфер с данными пакетов.
	 * @return размер пакета.
	 */
	private final int getPacketSize(final ByteBuffer buffer) {
		return buffer.getShort() & 0xFFFF;
	}

	@Override
	protected ByteBuffer movePacketToBuffer(final ServerPacket packet, final ByteBuffer buffer) {

		final GameClient client = getClient();

		buffer.clear();

		packet.writePosition(buffer);
		packet.write(buffer);

		buffer.flip();

		packet.writeHeader(buffer, buffer.limit());

		if(Config.DEVELOPER_DEBUG_SERVER_PACKETS) {
			System.out.println("Server packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):");
			System.out.println(Util.hexdump(buffer.array(), buffer.limit()));
		}

		client.encrypt(buffer, 2, buffer.limit() - 2);

		if(Config.DEVELOPER_DEBUG_SERVER_PACKETS) {
			System.out.println("Server crypt packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):");
			System.out.println(Util.hexdump(buffer.array(), buffer.limit()));
		}

		return buffer;
	}

	@Override
	protected void readPacket(final ByteBuffer buffer) {

		final GameClient client = getClient();

		if(Config.DEVELOPER_DEBUG_CLIENT_PACKETS) {
			System.out.println("Client crypt dump(size: " + buffer.limit() + "):\n" + Util.hexdump(buffer.array(), buffer.limit()));
		}

		for(int i = 0, limit = 0, size = 0; buffer.remaining() > 2 && i < 10; i++) {

			size = getPacketSize(buffer);
			limit += size;

			client.decrypt(buffer, buffer.position(), size - 2);
			client.readPacket(getPacket(buffer, client), buffer);

			if(limit > buffer.limit()) {
				break;
			}

			buffer.position(limit);
		}

		if(Config.DEVELOPER_DEBUG_CLIENT_PACKETS) {
			LOGGER.info("Client dump(size: " + buffer.limit() + "):\n" + Util.hexdump(buffer.array(), buffer.limit()));
		}
	}

	@Override
	public String toString() {
		return "GameConnection [network=" + network + ", channel=" + channel + ", closed=" + closed + ", lastActive=" + lastActive + "]";
	}
}

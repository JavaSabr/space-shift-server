package com.ss.server.network.game.model;

import java.nio.channels.AsynchronousSocketChannel;

import rlib.network.server.AcceptHandler;

import com.ss.server.LocalObjects;
import com.ss.server.network.Network;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseServerTime;

/**
 * Реализация обработчика новых подключений от клиентов.
 * 
 * @author Ronn
 */
public class GameAcceptHandler extends AcceptHandler {

	public static final GameAcceptHandler getInstance() {

		if(instance == null) {
			instance = new GameAcceptHandler();
		}

		return instance;
	}

	private static GameAcceptHandler instance;

	@Override
	protected void onAccept(final AsynchronousSocketChannel channel) {

		final GameConnection connect = new GameConnection(Network.getInstance().getServerNetwork(), channel, ServerPacket.class);
		final GameClient client = GameClient.create(connect);

		connect.setClient(client);
		client.successfulConnection();
		connect.startRead();
		client.sendPacket(ResponseServerTime.getInstance(LocalObjects.get()), true);
	}

	@Override
	protected void onFailed(final Throwable exc) {
		LOGGER.warning(this, exc);
	}
}

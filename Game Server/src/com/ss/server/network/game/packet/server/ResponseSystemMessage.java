package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.SystemMessageType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с системным сообщением.
 * 
 * @author Ronn
 */
public class ResponseSystemMessage extends ServerPacket {

	public static ResponseSystemMessage getInstance(final SystemMessageType messageType, final LocalObjects local) {

		final ResponseSystemMessage packet = local.create(instance);
		packet.messageType = messageType;

		return packet;
	}

	private static final ResponseSystemMessage instance = new ResponseSystemMessage();

	/** название переменных */
	private final Array<String> varNames;
	/** значение переменных */
	private final Array<String> varValues;

	/** тип системного сообщения */
	private SystemMessageType messageType;

	public ResponseSystemMessage() {
		this.varNames = ArrayFactory.newArray(String.class);
		this.varValues = ArrayFactory.newArray(String.class);
	}

	/**
	 * Добавление переменной.
	 * 
	 * @param name название переменной.
	 * @param value значение переменной.
	 */
	public void addVar(final String name, final String value) {
		varNames.add(name);
		varValues.add(value);
	}

	@Override
	public void finalyze() {
		varNames.clear();
		varValues.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SYSTEM_MESSAGE;
	}

	/**
	 * @return название переменных.
	 */
	public Array<String> getVarNames() {
		return varNames;
	}

	/**
	 * @return значение переменных.
	 */
	public Array<String> getVarValues() {
		return varValues;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, messageType.ordinal());

		final Array<String> varNames = getVarNames();
		final Array<String> varValues = getVarValues();

		writeInt(buffer, varNames.size());

		for(int i = 0, length = varNames.size(); i < length; i++) {

			final String name = varNames.get(i);

			writeInt(buffer, name.length());
			writeString(buffer, name);

			final String value = varValues.get(i);

			writeInt(buffer, value.length());
			writeString(buffer, value);
		}
	}
}

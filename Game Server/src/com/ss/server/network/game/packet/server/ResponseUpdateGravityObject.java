package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.gravity.GravityObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с обновлеием положения гравитационно объекта.
 * 
 * @author Ronn
 */
public class ResponseUpdateGravityObject extends ServerPacket {

	public static final ServerPacket getInstance(final GravityObject gravityObject, final double done) {

		final ResponseUpdateGravityObject packet = instance.newInstance();

		packet.done = done;
		packet.objectId = gravityObject.getObjectId();
		packet.classId = gravityObject.getClassId();

		return packet;
	}

	private static final ResponseUpdateGravityObject instance = new ResponseUpdateGravityObject();

	/** выполненый процент разворота */
	private double done;

	/** уникальный ид объекта */
	private int objectId;
	/** классовый ид объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_UPDATE_GRAVITY_OBJECT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeDouble(buffer, done);
	}
}

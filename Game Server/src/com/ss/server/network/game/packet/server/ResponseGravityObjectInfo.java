package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.model.gravity.GravityObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Информация о гравитационном объекте.
 * 
 * @author Ronn
 */
public class ResponseGravityObjectInfo extends ServerPacket {

	public static ResponseGravityObjectInfo getInstance(final GravityObject object) {

		final ResponseGravityObjectInfo packet = (ResponseGravityObjectInfo) instance.newInstance();

		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();
		packet.templateId = object.getTemplateId();

		packet.radius = object.getRadius();
		packet.distance = object.getDistance();
		packet.turnTime = object.getTurnTime();
		packet.turnAroundTime = object.getTurnAroundTime();

		final GravityObject parent = object.getParent();

		if(parent == null) {
			packet.parentId = 0;
			packet.parentClassId = 0;
		} else {
			packet.parentId = parent.getObjectId();
			packet.parentClassId = parent.getClassId();
		}

		packet.location.set(object.getLocation());
		packet.rotation.set(object.getOrbitalRotation());

		return packet;
	}

	private static final ResponseGravityObjectInfo instance = new ResponseGravityObjectInfo();

	/** позиция объекта */
	private final Vector location;
	/** разворот орбиты */
	private final Rotation rotation;

	/** уникальный ид станции */
	private int objectId;
	/** классовый ид станции */
	private int classId;
	/** ид владельца объекта */
	private int parentId;
	/** класс ид владельца */
	private int parentClassId;
	/** ид темплейта */
	private int templateId;
	/** радиус */
	private int radius;
	/** дистанция от центра */
	private int distance;
	/** время разворота вокруг своей оси */
	private int turnTime;

	/** время разворота вокруг орбиты */
	private long turnAroundTime;

	public ResponseGravityObjectInfo() {
		this.location = Vector.newInstance();
		this.rotation = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_GRAVITY_OBJECT_INFO;
	}

	@Override
	public String toString() {
		return "GravityObjectInfo location = " + location + ",  rotation = " + rotation + ",  objectId = " + objectId + ",  classId = " + classId + ",  parentId = " + parentId + ",  parentClassId = "
				+ parentClassId + ",  templateId = " + templateId + ",  radius = " + radius + ",  distance = " + distance + ",  turnTime = " + turnTime + ",  turnAroundTime = " + turnAroundTime;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, templateId);
		writeInt(buffer, parentId);
		writeInt(buffer, parentClassId);

		writeInt(buffer, radius);
		writeInt(buffer, distance);
		writeInt(buffer, turnTime);
		writeLong(buffer, turnAroundTime);

		writeVector(buffer, location);
		writeRotation(buffer, rotation);
	}
}

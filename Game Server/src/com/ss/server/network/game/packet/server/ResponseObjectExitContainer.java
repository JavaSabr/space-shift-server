package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о удалении объекта из контейнера.
 * 
 * @author Ronn
 */
public class ResponseObjectExitContainer extends ServerPacket {

	public static ResponseObjectExitContainer getInstance(final SpaceObject object, final SpaceObject container) {

		final ResponseObjectExitContainer packet = (ResponseObjectExitContainer) instance.newInstance();

		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();
		packet.targetObjectId = container.getObjectId();
		packet.targetClassId = container.getClassId();

		return packet;
	}

	private static final ResponseObjectExitContainer instance = new ResponseObjectExitContainer();

	/** уникальный ид объекта */
	private int objectId;
	/** классовый ид объекта */
	private int classId;

	/** уникальный ид целевого объекта */
	private int targetObjectId;
	/** классовый ид целевого объекта */
	private int targetClassId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_EXIT_CONTAINER;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, targetObjectId);
		writeInt(buffer, targetClassId);
	}
}

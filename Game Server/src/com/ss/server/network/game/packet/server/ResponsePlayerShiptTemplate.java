package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Серверный пакет с инфой о шаблоне корабля игрока.
 * 
 * @author Ronn
 */
public class ResponsePlayerShiptTemplate extends ResponseShipTemplate<PlayerShipTemplate> {

	public static final ServerPacket getInstance(final PlayerShipTemplate template, final LocalObjects local) {

		final ResponsePlayerShiptTemplate packet = local.create(instance);
		packet.setTemplate(template);

		return packet;
	}

	private static final ResponsePlayerShiptTemplate instance = new ResponsePlayerShiptTemplate();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_SHIP_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);
	}
}

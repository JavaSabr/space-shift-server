package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.SkillPanelInfo;
import com.ss.server.model.skills.Skill;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Список настроек положения скилов на панели.
 * 
 * @author Ronn
 */
public class ResponseSkillPanel extends ServerPacket {

	public static ResponseSkillPanel getInstance(final PlayerShip ship) {

		final ResponseSkillPanel packet = (ResponseSkillPanel) instance.newInstance();
		final ByteBuffer buffer = packet.getPrepare();

		// получаем настройки скилов на панели
		final Array<SkillPanelInfo> skillPanelInfo = ship.getSkillPanelInfo();

		skillPanelInfo.readLock();
		try {

			// получаем массив настроек
			final SkillPanelInfo[] array = skillPanelInfo.array();

			packet.writeByte(buffer, skillPanelInfo.size());

			// записываем кадый элемент
			for(int i = 0, length = skillPanelInfo.size(); i < length; i++) {

				final SkillPanelInfo info = array[i];

				final Skill skill = info.getSkill();

				packet.writeInt(buffer, skill.getModuleObjectId());
				packet.writeInt(buffer, skill.getId());
				packet.writeByte(buffer, info.getOrder());
			}

		} finally {
			skillPanelInfo.readUnlock();
		}

		return packet;
	}

	private static final ResponseSkillPanel instance = new ResponseSkillPanel();

	/** корабль игрока */
	private final ByteBuffer prepare;

	public ResponseSkillPanel() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SKILL_PANEL;
	}

	/**
	 * @return корабль игрока.
	 */
	protected ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.social.party.Party;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на исключение члена из группы.
 * 
 * @author Ronn
 */
public class RequestPartyExcludeMember extends ClientPacket {

	/** уникальный ид игрока */
	private int objectId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final Party party = playerShip.getParty();

		if(party != null) {
			party.excludePlayer(playerShip, objectId, local);
		}
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
	}
}

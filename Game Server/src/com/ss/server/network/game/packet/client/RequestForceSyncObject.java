package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.Space;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Запрос на принудительную синхронизацию с объектом.
 * 
 * @author Ronn
 */
public class RequestForceSyncObject extends ClientPacket {

	private static final Space SPACE = Space.getInstance();

	/** уникальнвй ид объекта */
	private int objectId;
	/** класс ид объекта */
	private int classId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final SpaceLocation location = SPACE.getLocation(playerShip.getLocationId());
		final SpaceObject object = location.findObject(SpaceObject.class, playerShip, objectId, classId);

		if(object == null) {
			return;
		}

		object.syncFor(playerShip, local);
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		classId = readInt();
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.impl.Account;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseEnterInfo;

/**
 * Запрос на выбор корабля для входа в мир.
 * 
 * @author Ronn
 */
public class RequestSelectShip extends ClientPacket {

	/** уникальный ид корабля */
	private int objectId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final Account account = client.getAccount();

		if(account == null) {
			return;
		}

		final PlayerManager playerManager = PlayerManager.getInstance();
		final PlayerShip playerShip = playerManager.restorePlayerShip(account.getName(), objectId, LocalObjects.get());

		if(playerShip == null) {
			return;
		}

		client.setOwner(playerShip);
		playerShip.setClient(client);

		client.sendPacket(ResponseEnterInfo.getInstance(playerShip), true);
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
	}
}

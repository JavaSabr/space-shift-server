package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о том, что игрок был выведен из игры.
 * 
 * @author Ronn
 */
public class ResponseExit extends ServerConstPacket {

	public static ResponseExit getInstance() {
		return instance;
	}

	private static final ResponseExit instance = new ResponseExit();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_EXIT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

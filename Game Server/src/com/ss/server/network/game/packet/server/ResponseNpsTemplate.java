package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Серверный пакет с инфой о шаблоне Nps.
 * 
 * @author Ronn
 */
public class ResponseNpsTemplate extends ResponseShipTemplate<NpsTemplate> {

	public static final ServerPacket getInstance(final NpsTemplate template, final LocalObjects local) {

		final ResponseNpsTemplate packet = local.create(instance);
		packet.setTemplate(template);

		return packet;
	}

	private static final ResponseNpsTemplate instance = new ResponseNpsTemplate();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_NPS_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);

		final NpsTemplate template = getTemplate();

		writeByte(buffer, template.getLevel());

		final String name = template.getName();

		writeByte(buffer, name.length());
		writeString(buffer, name);
	}
}

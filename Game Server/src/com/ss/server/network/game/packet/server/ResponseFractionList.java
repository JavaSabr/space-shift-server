package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.server.manager.PlayerManager;
import com.ss.server.model.faction.Fraction;
import com.ss.server.network.game.ServerPacketType;

/**
 * Ответ со списком фракций доступнывх на сервере.
 * 
 * @author Ronn
 */
public class ResponseFractionList extends ServerConstPacket {

	public static ResponseFractionList getInstance() {

		final ResponseFractionList packet = instance.newInstance();

		final PlayerManager manager = PlayerManager.getInstance();
		final Fraction[] fractions = manager.getFractions();

		final ByteBuffer buffer = packet.getPrepare();

		packet.writeByte(buffer, fractions.length);

		for(final Fraction fraction : fractions) {

			packet.writeInt(buffer, fraction.getId());

			final String name = fraction.getName();
			final String mainIcon = fraction.getMainIcon();
			final String history = fraction.getHistory();

			packet.writeByte(buffer, name.length());
			packet.writeString(buffer, name);
			packet.writeByte(buffer, mainIcon.length());
			packet.writeString(buffer, mainIcon);
			packet.writeByte(buffer, history.length());
			packet.writeString(buffer, history);
		}

		buffer.flip();

		return packet;
	}

	private static final ResponseFractionList instance = new ResponseFractionList();

	/** буффес с готовыми данными для записи */
	private final ByteBuffer prepare;

	public ResponseFractionList() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_FRACTION_LIST;
	}

	/**
	 * @return буффес с готовыми данными для записи.
	 */
	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

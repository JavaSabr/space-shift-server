package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Env;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о статусе корабля.
 * 
 * @author Ronn
 */
public class ResponseShipStatus extends ServerPacket {

	public static ResponseShipStatus getInstance(final SpaceShip ship, final LocalObjects local) {

		final Env env = local.getNextEnv();

		final int maxStrength = ship.getMaxStrength(env);
		final int maxShield = ship.getMaxShield();

		final ResponseShipStatus packet = local.create(instance);
		packet.objectId = ship.getObjectId();
		packet.classId = ship.getClassId();
		packet.strength = Math.min(ship.getCurrentStrength() * 1F / Math.max(maxStrength, 1F), 1F);
		packet.shield = maxShield < 1 ? 0 : Math.min(ship.getCurrentShield() * 1F / Math.max(maxShield, 1F), 1F);

		return packet;
	}

	private static final ResponseShipStatus instance = new ResponseShipStatus();

	/** уникальный ид корабля */
	private int objectId;
	/** класс ид корабля */
	private int classId;

	/** процент прочности */
	private float strength;
	/** процент состояния щитов корабля */
	private float shield;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHIP_STATUS;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeFloat(buffer, strength);
		writeFloat(buffer, shield);
	}

}

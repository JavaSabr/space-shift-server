package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об разворачивании корабля.
 * 
 * @author Ronn
 */
public class ResponseShipRotate extends ServerPacket {

	public static ResponseShipRotate getInstance(final SpaceShip ship, final Rotation start, final Rotation end, final float step, final float done, final boolean infinity, final boolean force,
			final LocalObjects local) {

		final ResponseShipRotate packet = local.create(instance);
		packet.objectId = ship.getObjectId();
		packet.classId = ship.getClassId();
		packet.step = step;
		packet.done = done;
		packet.start.set(start);
		packet.end.set(end);
		packet.infinity = infinity;
		packet.force = force;

		return packet;
	}

	private static final ResponseShipRotate instance = new ResponseShipRotate();

	/** стартовый разворот */
	private final Rotation start;
	/** конечный разворот */
	private final Rotation end;

	/** ид объекта */
	private int objectId;
	/** под ид объекта */
	private int classId;

	/** ускорение */
	private float step;
	/** выполненность разворота */
	private float done;

	/** бесконечный ли разворот */
	private boolean infinity;
	/** принудительная синхронизация */
	private boolean force;

	public ResponseShipRotate() {
		this.start = Rotation.newInstance();
		this.end = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHIP_ROTATION;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);

		writeFloat(buffer, step);
		writeFloat(buffer, done);

		writeRotation(buffer, start);
		writeRotation(buffer, end);
		writeByte(buffer, infinity ? 1 : 0);
		writeByte(buffer, force ? 1 : 0);
	}
}

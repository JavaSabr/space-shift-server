package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSlot;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Информация о корабле игрока.
 * 
 * @author Ronn
 */
public class ResponsePlayerShipInfo extends ServerPacket {

	public static ResponsePlayerShipInfo getInstance(final PlayerShip playerShip, final LocalObjects local) {

		final ResponsePlayerShipInfo packet = local.create(instance);

		final ByteBuffer buffer = packet.getPrepare();

		packet.writeInt(buffer, playerShip.getObjectId());
		packet.writeInt(buffer, playerShip.getClassId());
		packet.writeInt(buffer, playerShip.getTemplateId());

		final String name = playerShip.getName();

		packet.writeByte(buffer, name.length());
		packet.writeString(buffer, name);

		final ModuleSystem system = playerShip.getModuleSystem();

		system.lock();
		try {

			final int count = system.getModulesCount();

			packet.writeByte(buffer, count);

			final ModuleSlot[] slots = system.getSlots();

			for(int i = 0, length = slots.length; i < length; i++) {

				final ModuleSlot slot = slots[i];

				if(slot.isEmpty()) {
					continue;
				}

				final Module module = slot.getModule();

				packet.writeInt(buffer, module.getObjectId());
				packet.writeInt(buffer, module.getClassId());
				packet.writeInt(buffer, module.getTemplateId());
				packet.writeByte(buffer, module.getIndex());
			}

		} finally {
			system.unlock();
		}

		packet.writeVector(buffer, playerShip.getLocation());
		packet.writeRotation(buffer, playerShip.getRotation());

		return packet;
	}

	private static final ResponsePlayerShipInfo instance = new ResponsePlayerShipInfo();

	/** промежуточной буффер */
	private final ByteBuffer prepare;

	public ResponsePlayerShipInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_SHIP_INFO;
	}

	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();

		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

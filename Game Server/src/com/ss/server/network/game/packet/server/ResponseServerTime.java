package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с серверным временем.
 * 
 * @author Ronn
 */
public class ResponseServerTime extends ServerPacket {

	public static final ResponseServerTime getInstance(final LocalObjects local) {
		return local.create(instance);
	}

	private static final ResponseServerTime instance = new ResponseServerTime();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SERVER_TIME;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, System.currentTimeMillis());
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.LocationManager;
import com.ss.server.model.location.LocationInfo;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseLocationInfo;

/**
 * Клиентский пакет с запросом на получение инфы о локации.
 * 
 * @author Ronn
 */
public class RequestLocationInfo extends ClientPacket {

	private static final LocationManager LOCATION_MANAGER = LocationManager.getInstance();

	/** ид локации */
	private int locationId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final LocationInfo info = LOCATION_MANAGER.getLocationInfo(locationId);

		if(info != null) {
			client.sendPacket(ResponseLocationInfo.getInstance(info), true);
		}
	}

	@Override
	protected void readImpl() {
		locationId = readInt();
	}
}

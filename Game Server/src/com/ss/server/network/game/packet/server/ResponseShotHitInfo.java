package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.shot.Shot;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о попадании выстрела в объект.
 * 
 * @author Ronn
 */
public class ResponseShotHitInfo extends ServerPacket {

	public static ResponseShotHitInfo getInstance(final Shot shot, final SpaceObject object, final boolean onShield, final LocalObjects local) {

		final ResponseShotHitInfo packet = local.create(instance);
		packet.objectId = shot.getObjectId();
		packet.targretObjectId = object.getObjectId();
		packet.targetClassId = object.getClassId();
		packet.onShield = onShield;

		return packet;
	}

	private static final ResponseShotHitInfo instance = new ResponseShotHitInfo();

	/** уникальный ид выстрела */
	private int objectId;
	/** уникальный ид цели */
	private int targretObjectId;
	/** класс ид цели */
	private int targetClassId;

	/** было ли попадание в щит */
	private boolean onShield;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHOT_HIT_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, targretObjectId);
		writeInt(buffer, targetClassId);
		writeByte(buffer, onShield ? 1 : 0);
	}
}

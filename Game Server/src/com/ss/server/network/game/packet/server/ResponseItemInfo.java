package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Информация о космическом придмете.
 * 
 * @author Ronn
 */
public class ResponseItemInfo extends ServerPacket {

	public static ResponseItemInfo getInstance(final SpaceItem item, final LocalObjects local) {

		final ResponseItemInfo packet = local.create(instance);
		packet.objectId = item.getObjectId();
		packet.classId = item.getClassId();
		packet.templateId = item.getTemplateId();
		packet.itemCount = item.getItemCount();
		packet.location.set(item.getLocation());
		packet.rotation.set(item.getRotation());

		return packet;
	}

	private static final ResponseItemInfo instance = new ResponseItemInfo();

	/** положение в пространстве */
	private final Vector location;
	/** разворот */
	private final Rotation rotation;

	/** кол-во предметов */
	private long itemCount;

	/** уникальный ид предмета */
	private int objectId;
	/** ид класса предметов */
	private int classId;
	/** ид шаблона итема */
	private int templateId;

	public ResponseItemInfo() {
		this.location = Vector.newInstance();
		this.rotation = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_ITEM_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, templateId);
		writeLong(buffer, itemCount);
		writeVector(buffer, location);
		writeRotation(buffer, rotation);
	}
}

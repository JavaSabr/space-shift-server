package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.table.IntKey;
import rlib.util.table.Table;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Список скилов модулей у корабля.
 * 
 * @author Ronn
 */
public class ResponseSkillList extends ServerPacket {

	public static ResponseSkillList getInstance(final PlayerShip ship) {
		final ResponseSkillList packet = (ResponseSkillList) instance.newInstance();

		final ByteBuffer buffer = packet.prepare;

		// получаем таблицу скилов
		final Table<IntKey, Skill[]> skills = ship.getSkillTable();

		// записываем размер скил листа
		// packet.writeByte(buffer, skills.size());

		final int indexSize = buffer.position();

		buffer.position(buffer.position() + 1);

		int counter = 0;

		// записываем скилы
		for(final Skill[] array : skills) {
			if(array.length > 1) {
				for(int i = 0, length = array.length; i < length; i++) {

					final Skill skill = array[i];

					packet.writeInt(buffer, skill.getId());
					packet.writeInt(buffer, skill.getModuleObjectId());

					counter++;
				}
			} else {

				final Skill skill = array[0];

				packet.writeInt(buffer, skill.getId());
				packet.writeInt(buffer, skill.getModuleObjectId());

				counter++;
			}
		}

		final int indexLast = buffer.position();

		buffer.position(indexSize);
		packet.writeByte(buffer, counter);
		buffer.position(indexLast);

		return packet;
	}

	private static final ResponseSkillList instance = new ResponseSkillList();

	/** подготавливаемый буффер */
	private final ByteBuffer prepare;

	public ResponseSkillList() {
		super();
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SKILL_LIST;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		prepare.flip();
		buffer.put(prepare);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.SkillTemplate;

/**
 * Серврный пакет с инфой о шаблоне скила.
 * 
 * @author Ronn
 */
public class ResponseSkillTemplate extends ServerPacket {

	public static final ServerPacket getInstance(final SkillTemplate template) {

		final ResponseSkillTemplate packet = instance.newInstance();
		packet.template = template;

		return packet;
	}

	private static final ResponseSkillTemplate instance = new ResponseSkillTemplate();

	/** шаблон скила */
	private SkillTemplate template;

	@Override
	public void finalyze() {
		template = null;
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SKILL_TEMPLATE;
	}

	/**
	 * @return шаблон скила.
	 */
	public SkillTemplate getTemplate() {
		return template;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final SkillTemplate template = getTemplate();

		writeInt(buffer, template.getId());

		String string = template.getName();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		string = template.getDescription();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		string = template.getDescriptionStats();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		string = template.getIcon();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		writeInt(buffer, template.getSkillType().ordinal());
		writeInt(buffer, template.getReloadId());
	}
}

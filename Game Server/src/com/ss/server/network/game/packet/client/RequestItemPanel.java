package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseItemPanel;

/**
 * Пакет запроса на информацию о размещении предметов на панели быстрого
 * доступа.
 * 
 * @author Ronn
 */
public class RequestItemPanel extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip != null) {
			playerShip.sendPacket(ResponseItemPanel.getInstance(playerShip), true);
		}
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}
}

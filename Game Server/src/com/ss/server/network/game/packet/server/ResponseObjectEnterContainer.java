package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о добавлении объекта в контейнер.
 * 
 * @author Ronn
 */
public class ResponseObjectEnterContainer extends ServerPacket {

	public static ResponseObjectEnterContainer getInstance(final SpaceObject object, final SpaceObject container) {

		final ResponseObjectEnterContainer packet = (ResponseObjectEnterContainer) instance.newInstance();

		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();
		packet.targetObjectId = container.getObjectId();
		packet.targetClassId = container.getClassId();

		return packet;
	}

	private static final ResponseObjectEnterContainer instance = new ResponseObjectEnterContainer();

	/** уникальный ид объекта */
	private int objectId;
	/** классовый ид объекта */
	private int classId;

	/** уникальный ид целевого объекта */
	private int targetObjectId;
	/** классовый ид целевого объекта */
	private int targetClassId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_ENTER_CONTAINER;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, targetObjectId);
		writeInt(buffer, targetClassId);
	}
}

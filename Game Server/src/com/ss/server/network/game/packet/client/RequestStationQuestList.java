package com.ss.server.network.game.packet.client;

import java.util.concurrent.atomic.AtomicReference;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseStationQuestList;

/**
 * Клиентский пакет с запросом на получение инфы о доступных заданиях на
 * станции.
 * 
 * @author Ronn
 */
public class RequestStationQuestList extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip ship = client.getOwner();

		if(ship == null) {
			return;
		}

		final AtomicReference<SpaceStation> reference = ship.getStationReference();

		synchronized(reference) {

			final SpaceStation station = reference.get();

			if(station != null) {
				ship.sendPacket(ResponseStationQuestList.getInstance(station, ship, local), true);
			}
		}
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;

import com.ss.server.model.ship.player.ItemPanelInfo;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Список настроек положения предметов на панели.
 * 
 * @author Ronn
 */
public class ResponseItemPanel extends ServerPacket {

	public static ResponseItemPanel getInstance(final PlayerShip ship) {

		final ResponseItemPanel packet = (ResponseItemPanel) instance.newInstance();
		final ByteBuffer buffer = packet.getPrepare();

		final Array<ItemPanelInfo> itemPanelInfo = ship.getItemPanelInfo();

		itemPanelInfo.readLock();
		try {

			final ItemPanelInfo[] array = itemPanelInfo.array();

			packet.writeByte(buffer, itemPanelInfo.size());

			for(int i = 0, length = itemPanelInfo.size(); i < length; i++) {

				final ItemPanelInfo info = array[i];

				packet.writeInt(buffer, info.getObjectId());
				packet.writeInt(buffer, info.getClassId());
				packet.writeByte(buffer, info.getOrder());
			}

		} finally {
			itemPanelInfo.readUnlock();
		}

		buffer.flip();

		return packet;
	}

	private static final ResponseItemPanel instance = new ResponseItemPanel();

	/** корабль игрока */
	private final ByteBuffer prepare;

	public ResponseItemPanel() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_ITEM_PANEL;
	}

	/**
	 * @return корабль игрока.
	 */
	protected ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

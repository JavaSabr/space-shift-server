package com.ss.server.network.game.packet.client;

import java.util.function.Consumer;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.database.PlayerDBManager;
import com.ss.server.model.impl.Account;
import com.ss.server.model.ship.player.PlayerShipPreview;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseShipList;

/**
 * Запрос списка кораблей на аккаунте.
 * 
 * @author Ronn
 */
public class RequestShipList extends ClientPacket {

	private static final Consumer<PlayerShipPreview> FOLD_FUNC = preview -> preview.fold();

	private final Array<PlayerShipPreview> shipList;

	public RequestShipList() {
		this.shipList = ArrayFactory.newArray(PlayerShipPreview.class);
	}

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final Account account = client.getAccount();

		if(account == null) {
			return;
		}

		final Array<PlayerShipPreview> shipList = getShipList();

		final PlayerDBManager dbManager = PlayerDBManager.getInstance();
		dbManager.loadShipList(shipList, local, account.getName());

		client.sendPacket(ResponseShipList.getInstance(shipList), true);
	}

	@Override
	public void finalyze() {

		final Array<PlayerShipPreview> shipList = getShipList();

		shipList.forEach(FOLD_FUNC);
		shipList.clear();
	}

	protected final Array<PlayerShipPreview> getShipList() {
		return shipList;
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Запрос от клиента на нажатие кнопки задания.
 * 
 * @author Ronn
 */
public class RequestQuestButtonSelect extends ClientPacket {

	private static final ObjectEventManager EVENT_MANAGER = ObjectEventManager.getInstance();

	/** ид задания, которого была нажата кнопка */
	private int questId;
	/** ид нажатой кнопки */
	private int buttonId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		EVENT_MANAGER.notifySelectButton(playerShip, questId, buttonId, local);
	}

	@Override
	protected void readImpl() {
		questId = readInt();
		buttonId = readByte();
	}
}

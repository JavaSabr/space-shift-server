package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * пакет запроса шаблона придмета.
 * 
 * @author Ronn
 */
public class RequestItemTemplate extends ClientPacket {

	/** ид шаблона предмета */
	private int templateId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final ItemTable itemTable = ItemTable.getInstance();
		final ItemTemplate template = itemTable.getTemplate(templateId);

		if(template != null) {
			client.sendPacket(template.getInfoPacket(local), true);
		}
	}

	@Override
	protected void readImpl() {
		templateId = readInt();
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о роспуске группы.
 * 
 * @author Ronn
 */
public class ResponsePartyDisband extends ServerPacket {

	public static ResponsePartyDisband getInstance() {
		return instance.newInstance();
	}

	private static final ResponsePartyDisband instance = new ResponsePartyDisband();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PARTY_DISBAND;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

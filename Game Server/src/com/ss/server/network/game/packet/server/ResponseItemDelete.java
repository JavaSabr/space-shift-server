package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о удалении предмета из мира.
 * 
 * @author Ronn
 */
public class ResponseItemDelete extends ServerPacket {

	private static final ResponseItemDelete instance = new ResponseItemDelete();

	public static ResponseItemDelete getInstance(final SpaceItem item, final LocalObjects local) {

		final ResponseItemDelete packet = local.create(instance);
		packet.objectId = item.getObjectId();
		packet.classId = item.getClassId();

		return packet;
	}

	/** уникальный ид объекта */
	private volatile int objectId;
	/** классовый ид объекта */
	private volatile int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_ITEM_DELETE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

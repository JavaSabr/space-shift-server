package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Указание клиенту на возврат к рендеру объекта.
 * 
 * @author Ronn
 */
public class ResponseObjectShow extends ServerPacket {

	public static ResponseObjectShow getInstance(final SpaceObject ship) {

		final ResponseObjectShow packet = (ResponseObjectShow) instance.newInstance();

		packet.objectId = ship.getObjectId();
		packet.classId = ship.getClassId();

		return packet;
	}

	private static final ResponseObjectShow instance = new ResponseObjectShow();

	/** уникальный ид объекта */
	private int objectId;
	/** классовый ид объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_SHOW;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;

import com.ss.server.model.ship.player.InterfaceInfo;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об характеристиках полета корабля.
 * 
 * @author Ronn
 */
public class ResponseInterface extends ServerPacket {

	public static ResponseInterface getInstance(final PlayerShip ship) {

		final ResponseInterface packet = (ResponseInterface) instance.newInstance();

		final ByteBuffer buffer = packet.prepare;

		// получаем настройки интерфейса
		final Array<InterfaceInfo> interfaceInfo = ship.getInterfaceInfo();

		interfaceInfo.readLock();
		try {

			// получаем массив настроек
			final InterfaceInfo[] array = interfaceInfo.array();

			// вписываем кол-во настроек
			packet.writeByte(buffer, interfaceInfo.size());

			// записываем кадый элемент
			for(int i = 0, length = interfaceInfo.size(); i < length; i++) {
				final InterfaceInfo info = array[i];

				packet.writeByte(buffer, info.getType());
				packet.writeInt(buffer, info.getX());
				packet.writeInt(buffer, info.getY());
				packet.writeByte(buffer, info.isMinimized() ? 1 : 0);
			}

		} finally {
			interfaceInfo.readUnlock();
		}

		return packet;
	}

	private static final ResponseInterface instance = new ResponseInterface();

	/** корабль игрока */
	private final ByteBuffer prepare;

	public ResponseInterface() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_INTERFACE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		prepare.flip();
		buffer.put(prepare);
	}
}

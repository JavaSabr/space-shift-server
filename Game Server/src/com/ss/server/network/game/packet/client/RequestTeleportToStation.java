package com.ss.server.network.game.packet.client;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.ss.server.LocalObjects;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.ship.MessageUtils;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.teleport.impl.ShipToStationTeleportHandler;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на запуск телепортации на ближайшую космическую станцию.
 * 
 * @author Ronn
 */
public class RequestTeleportToStation extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final AtomicReference<SpaceStation> ref = playerShip.getStationReference();
		final AtomicBoolean onStation = playerShip.getOnStation();

		synchronized(ref) {

			if(ref.get() == null) {
				MessageUtils.sendMessage(local, playerShip, SystemMessageType.NO_SELECTED_TARGET);
				LOGGER.warning(this, "not found station");
				return;
			}

			synchronized(onStation) {

				if(onStation.get()) {
					playerShip.sendMessage("Вы уже находитесь на станции.", local);
					LOGGER.warning(this, "playerShip already on station.");
					return;
				}

				playerShip.startTeleport(ShipToStationTeleportHandler.getInstance(), local);
			}
		}
	}
}

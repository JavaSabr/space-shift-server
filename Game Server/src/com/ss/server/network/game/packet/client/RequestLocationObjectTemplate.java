package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.table.LocationObjectTable;
import com.ss.server.template.LocationObjectTemplate;

/**
 * Клиентский пакет с запросом на получение шаблона локационного объекта.
 * 
 * @author Ronn
 */
public class RequestLocationObjectTemplate extends ClientPacket {

	private static final LocationObjectTable OBJECT_TABLE = LocationObjectTable.getInstance();

	/** ид шаблона */
	private int templateId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final LocationObjectTemplate template = OBJECT_TABLE.getTemplate(templateId);

		if(template != null) {
			client.sendPacket(template.getInfoPacket(local), true);
		}
	}

	@Override
	protected void readImpl() {
		templateId = readInt();
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.QuestManager;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Запрос на внесения задания в избранное.
 * 
 * @author Ronn
 */
public class RequestQuestFavorite extends ClientPacket {

	/** уникальный ид состояния задания */
	private int objectId;
	/** ид задания */
	private int questId;

	/** состояния в избранном */
	private boolean favorite;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip ship = client.getOwner();

		if(ship == null) {
			return;
		}

		final QuestManager questManager = QuestManager.getInstance();
		questManager.updateFavorite(ship, objectId, questId, favorite);
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		questId = readInt();
		favorite = readByte() == 1;
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.template.ObjectTemplate;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;

/**
 * Серверный пакет, описывающий шаблон объекта.
 * 
 * @author Ronn
 */
public abstract class ResponseObjectTemplate<T extends ObjectTemplate> extends ServerPacket {

	/** записываемый шаблон */
	private T template;

	@Override
	public void finalyze() {
		setTemplate(null);
	}

	/**
	 * @return записываемый шаблон.
	 */
	public T getTemplate() {
		return template;
	}

	/**
	 * @param template записываемый шаблон.
	 */
	public void setTemplate(final T template) {
		this.template = template;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final T template = getTemplate();

		writeInt(buffer, template.getId());
		writeInt(buffer, template.getSizeX());
		writeInt(buffer, template.getSizeY());
		writeInt(buffer, template.getSizeZ());
		writeInt(buffer, template.getVisibleRange());

		writeFloat(buffer, template.getScale());

		final String key = template.getKey();

		writeInt(buffer, key.length());
		writeString(buffer, key);

		writeInt(buffer, template.getType().index());

		GraphicEffectTemplate effect = template.getDestructEffect();

		writeByte(buffer, effect == null ? 0 : 1);

		if(effect != null) {
			effect.writeMe(buffer, this);
		}

		effect = template.getStartTeleportEffect();

		writeByte(buffer, effect == null ? 0 : 1);

		if(effect != null) {
			effect.writeMe(buffer, this);
		}

		effect = template.getFinishTeleportEffect();

		writeByte(buffer, effect == null ? 0 : 1);

		if(effect != null) {
			effect.writeMe(buffer, this);
		}
	}
}

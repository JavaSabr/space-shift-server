package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Указание клиента на скрытия объекта из рендера.
 * 
 * @author Ronn
 */
public class ResponseObjectHide extends ServerPacket {

	public static ResponseObjectHide getInstance(final SpaceObject ship) {

		final ResponseObjectHide packet = (ResponseObjectHide) instance.newInstance();

		packet.objectId = ship.getObjectId();
		packet.classId = ship.getClassId();

		return packet;
	}

	private static final ResponseObjectHide instance = new ResponseObjectHide();

	/** уникальный ид объекта */
	private int objectId;
	/** классовый ид объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_HIDE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

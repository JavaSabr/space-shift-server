package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.template.graphic.effect.GraphicEffectTemplate;
import com.ss.server.template.item.ItemTemplate;

/**
 * Серверный пакет с инфой о шаблоне придмета.
 * 
 * @author Ronn
 */
public abstract class ResponseItemTemplate<T extends ItemTemplate> extends ResponseObjectTemplate<T> {

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);

		final T template = getTemplate();

		final String name = template.getName();

		writeByte(buffer, name.length());
		writeString(buffer, name);

		final String icon = template.getIcon();

		writeByte(buffer, icon.length());
		writeString(buffer, icon);

		final String description = template.getDescription();

		writeInt(buffer, description.length());
		writeString(buffer, description);

		final String stats = template.getDescriptionStats();

		writeInt(buffer, stats.length());
		writeString(buffer, stats);

		writeByte(buffer, template.isStackable() ? 1 : 0);

		GraphicEffectTemplate effect = template.getStartTeleportEffect();

		writeByte(buffer, effect == null ? 0 : 1);

		if(effect != null) {
			effect.writeMe(buffer, this);
		}

		effect = template.getFinishTeleportEffect();

		writeByte(buffer, effect == null ? 0 : 1);

		if(effect != null) {
			effect.writeMe(buffer, this);
		}
	}
}

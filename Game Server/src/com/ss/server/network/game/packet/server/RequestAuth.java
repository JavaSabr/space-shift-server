package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет запроса авторизации у игрока.
 * 
 * @author Ronn
 */
public class RequestAuth extends ServerConstPacket {

	public static RequestAuth getInstance() {
		return instance;
	}

	private static final RequestAuth instance = new RequestAuth();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.REQUEST_AUTH;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

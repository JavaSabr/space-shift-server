package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.MessageType;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об характеристиках полета корабля.
 * 
 * @author Ronn
 */
public class ResponseSayMessage extends ServerPacket {

	public static ResponseSayMessage getInstance(final MessageType type, final String sender, final String message, final LocalObjects local) {

		final ResponseSayMessage packet = local.create(instance);
		packet.type = type;
		packet.sender = sender;
		packet.message = message;

		return packet;
	}

	private static final ResponseSayMessage instance = new ResponseSayMessage();

	/** тип сообщения */
	private MessageType type;

	/** отправитель */
	private String sender;
	/** сообщение */
	private String message;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SAY_MESSAGE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, type.ordinal());
		writeByte(buffer, sender.length());
		writeString(buffer, sender);
		writeInt(buffer, message.length());
		writeString(buffer, message);
	}
}

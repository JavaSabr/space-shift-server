package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Vector;

import com.ss.server.model.module.ModuleType;
import com.ss.server.model.module.system.SlotInfo;
import com.ss.server.template.ship.ShipTemplate;

/**
 * Серверный пакет с инфой о шаблоне косического корабля.
 * 
 * @author Ronn
 */
public abstract class ResponseShipTemplate<T extends ShipTemplate> extends ResponseObjectTemplate<T> {

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);

		final T template = getTemplate();

		final SlotInfo[] slots = template.getSlots();

		writeInt(buffer, slots.length);

		if(slots.length > 0) {
			for(final SlotInfo info : slots) {

				final ModuleType type = info.getType();

				final Vector offset = info.getOffset();

				writeInt(buffer, type.ordinal());
				writeFloat(buffer, offset.getX());
				writeFloat(buffer, offset.getY());
				writeFloat(buffer, offset.getZ());
			}
		}
	}
}

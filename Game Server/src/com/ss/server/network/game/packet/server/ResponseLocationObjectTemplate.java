package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.LocationObjectTemplate;

/**
 * Серверный пакет с описанием локационного объекта.
 * 
 * @author Ronn
 */
public class ResponseLocationObjectTemplate extends ResponseObjectTemplate<LocationObjectTemplate> {

	public static ResponseLocationObjectTemplate getInstance(final LocationObjectTemplate template, final LocalObjects local) {

		final ResponseLocationObjectTemplate packet = local.create(instance);
		packet.setTemplate(template);

		return packet;
	}

	private static final ResponseLocationObjectTemplate instance = new ResponseLocationObjectTemplate();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_LOCATION_OBJECT_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);

		final LocationObjectTemplate template = getTemplate();
		writeFloat(buffer, template.getScale());
	}
}

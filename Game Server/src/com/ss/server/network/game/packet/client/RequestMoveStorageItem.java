package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.storage.StorageUtils;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Клиенсткий пакет с запросом перемещения предмета.
 * 
 * @author Ronn
 */
public class RequestMoveStorageItem extends ClientPacket {

	/** индекс ячейки источника */
	private int startIndex;
	/** индекс целевой ячейки */
	private int endIndex;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		StorageUtils.moveItem(playerShip, startIndex, endIndex, local);
	}

	@Override
	protected void readImpl() {
		startIndex = readInt();
		endIndex = readInt();
	}
}

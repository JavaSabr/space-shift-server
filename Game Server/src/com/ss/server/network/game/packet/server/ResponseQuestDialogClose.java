package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с запросом на закрытие квестовоо диалога.
 * 
 * @author Ronn
 */
public final class ResponseQuestDialogClose extends ServerConstPacket {

	public static ResponseQuestDialogClose getInstance() {
		return instance;
	}

	private static final ResponseQuestDialogClose instance = new ResponseQuestDialogClose();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_QUEST_DIALOG_CLOSE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

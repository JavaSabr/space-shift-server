package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseSkillTemplate;
import com.ss.server.table.SkillTable;
import com.ss.server.template.SkillTemplate;

/**
 * Клиенсткий пакет с запросом на получение шаблона скила.
 * 
 * @author Ronn
 */
public class RequestSkillTemplate extends ClientPacket {

	private static final SkillTable SKILL_TABLE = SkillTable.getInstance();

	/** ид темплейта */
	private int templateId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final SkillTemplate template = SKILL_TABLE.getTemplate(templateId);

		if(template != null) {
			client.sendPacket(ResponseSkillTemplate.getInstance(template), true);
		}
	}

	@Override
	protected void readImpl() {
		templateId = readInt();
	}
}

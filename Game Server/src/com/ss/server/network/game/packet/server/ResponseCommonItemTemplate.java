package com.ss.server.network.game.packet.server;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.item.CommonItemTemplate;

/**
 * @author Ronn
 */
public class ResponseCommonItemTemplate extends ResponseItemTemplate<CommonItemTemplate> {

	public static ResponseCommonItemTemplate getInstance(final CommonItemTemplate template, final LocalObjects local) {
		final ResponseCommonItemTemplate packet = local.create(instance);
		packet.setTemplate(template);
		return packet;
	}

	private static final ResponseCommonItemTemplate instance = new ResponseCommonItemTemplate();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_COMMON_ITEM_TEMPLATE;
	}
}

package com.ss.server.network.game.packet.client;

import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на на использование скила.
 * 
 * @author Ronn
 */
public class RequestUseSkill extends ClientPacket {

	/** позиция корабля */
	private final Vector location;

	/** уникальный ид модуля */
	private int objectId;
	/** ид скила */
	private int skillId;

	public RequestUseSkill() {
		this.location = Vector.newInstance();
	}

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null || playerShip.isDestructed()) {
			return;
		}

		final Skill skill = playerShip.getSkill(objectId, skillId);

		if(skill != null) {
			playerShip.getAI().startSkill(location, skill, local);
		}
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		skillId = readInt();
		location.setXYZ(readFloat(), readFloat(), readFloat());
	}
}

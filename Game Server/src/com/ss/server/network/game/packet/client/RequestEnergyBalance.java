package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ShipEventManager;
import com.ss.server.model.ship.ShipUtils;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Клиентский запрос на обновление распределения энергии.
 * 
 * @author Ronn
 */
public class RequestEnergyBalance extends ClientPacket {

	/** распределение на двигатель */
	private float engine;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null || playerShip.isDestructed()) {
			return;
		}

		final float currentEngine = playerShip.getEngineEnergy();

		playerShip.setEngineEnergy(engine);

		final ShipEventManager eventManager = ShipEventManager.getInstance();
		eventManager.notifyEngineEnergyChanged(currentEngine, playerShip, local);

		if(engine > 0 && !ShipUtils.isActiveEngines(playerShip)) {
			ShipUtils.activateEngines(playerShip, local);
		} else if(engine < 0.01F && ShipUtils.isActiveEngines(playerShip)) {
			ShipUtils.deactivateEngines(playerShip, local);
		}
	}

	@Override
	protected void readImpl() {
		engine = readFloat();
	}
}

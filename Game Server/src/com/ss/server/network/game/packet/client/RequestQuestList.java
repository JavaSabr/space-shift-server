package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseQuestListInfo;

/**
 * Клиентский пакет с запросом на получение инфы о квестовой книге.
 * 
 * @author Ronn
 */
public class RequestQuestList extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip ship = client.getOwner();

		if(ship != null) {
			ship.sendPacket(ResponseQuestListInfo.getInstance(ship, local), true);
		}
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Space;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.storage.StorageUtils;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Клиентский пакет с запросом погрузки предмета на склад.
 * 
 * @author Ronn
 */
public class RequestTeleportItem extends ClientPacket {

	private static final Space SPACE = Space.getInstance();

	/** уникальный ид предмета */
	private int objectId;
	/** ид класса */
	private int classId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final SpaceLocation location = SPACE.getLocation(playerShip);
		final SpaceItem item = location.findObject(SpaceItem.class, playerShip, objectId, classId);

		if(item == null) {
			return;
		}

		StorageUtils.startTeleportTo(playerShip, item, local);
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		classId = readInt();
	}
}

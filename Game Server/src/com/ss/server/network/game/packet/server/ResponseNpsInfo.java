package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSlot;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * пакет с информацией о корабле NPS.
 * 
 * @author Ronn
 */
public class ResponseNpsInfo extends ServerPacket {

	public static ResponseNpsInfo getInstance(final PlayerShip ship, final Nps nps, final LocalObjects local) {

		final ResponseNpsInfo packet = local.create(instance);

		final ByteBuffer buffer = packet.getPrepare();

		packet.writeInt(buffer, nps.getObjectId());
		packet.writeInt(buffer, nps.getClassId());
		packet.writeInt(buffer, nps.getTemplateId());
		packet.writeByte(buffer, nps.getFriendStatus(ship, local).ordinal());

		final ModuleSystem system = nps.getModuleSystem();

		system.lock();
		try {

			final int count = system.getModulesCount();

			packet.writeByte(buffer, count);

			final ModuleSlot[] slots = system.getSlots();

			for(int i = 0, length = slots.length; i < length; i++) {

				final ModuleSlot slot = slots[i];

				if(slot.isEmpty()) {
					continue;
				}

				final Module module = slot.getModule();

				packet.writeInt(buffer, module.getObjectId());
				packet.writeInt(buffer, module.getClassId());
				packet.writeInt(buffer, module.getTemplateId());
				packet.writeByte(buffer, module.getIndex());
			}

		} finally {
			system.unlock();
		}

		packet.writeVector(buffer, nps.getLocation());
		packet.writeRotation(buffer, nps.getRotation());

		buffer.flip();

		return packet;
	}

	private static final ResponseNpsInfo instance = new ResponseNpsInfo();

	/** промежуточной буффер */
	private final ByteBuffer prepare;

	public ResponseNpsInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_NPS_INFO;
	}

	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

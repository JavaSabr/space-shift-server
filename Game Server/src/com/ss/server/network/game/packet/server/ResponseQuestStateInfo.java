package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.QuestCounter;
import com.ss.server.model.quest.QuestPacketUtils;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.QuestStatus;
import com.ss.server.model.quest.QuestType;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.QuestReward;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с информацией о стадии квеста.
 * 
 * @author Ronn
 */
public class ResponseQuestStateInfo extends ServerPacket {

	public static ServerPacket getInstance(final QuestState state, final PlayerShip playerShip, final LocalObjects local) {

		final ResponseQuestStateInfo packet = local.create(instance.newInstance());

		final Quest quest = state.getQuest();

		final ByteBuffer buffer = packet.getPrepare();
		try {

			final Array<QuestReward> rewards = packet.getRewards();
			final Array<QuestCounter> counters = packet.getCounters();
			final Array<QuestButton> buttons = packet.getButtons();

			final QuestEvent event = packet.getEvent();
			event.setQuest(quest);
			event.setPlayerShip(playerShip);

			final QuestType questType = quest.getQuestType();
			final QuestStatus status = state.getStatus();

			packet.writeInt(buffer, quest.getId());
			packet.writeByte(buffer, questType.ordinal());
			packet.writeInt(buffer, state.getObjectId());
			packet.writeByte(buffer, status.ordinal());
			packet.writeByte(buffer, state.isFavorite() ? QuestPacketUtils.FAVORITE : QuestPacketUtils.NO_FAVORITE);

			String name = quest.getName();

			packet.writeByte(buffer, name.length());
			packet.writeString(buffer, name);

			String description = quest.getDescription();

			packet.writeByte(buffer, description.length());
			packet.writeString(buffer, description);

			description = quest.getDescription(state.getState());

			packet.writeByte(buffer, description.length());
			packet.writeString(buffer, description);

			if(status == QuestStatus.IN_PROGRESS) {
				quest.addRewardsTo(rewards, event, local);
				quest.addStateCountersTo(counters, event, state.getState());
				quest.addStateButtonsTo(buttons, event, state.getState());
				quest.addButtons(buttons, event);
			}

			QuestPacketUtils.writeRewards(packet, buffer, event, rewards);
			QuestPacketUtils.writeCounters(packet, buffer, counters, state);
			QuestPacketUtils.writeButtons(packet, buffer, buttons);

		} finally {
			buffer.flip();
		}

		return packet;
	}

	private static final ResponseQuestStateInfo instance = new ResponseQuestStateInfo();

	/** подготовленный буффер с данными */
	private final ByteBuffer prepare;

	/** действия заданий */
	private final Array<QuestButton> buttons;
	/** счетчики заданий */
	private final Array<QuestCounter> counters;
	/** награды за задание */
	private final Array<QuestReward> rewards;

	/** квестовое событие */
	private final QuestEvent event;

	public ResponseQuestStateInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
		this.buttons = ArrayFactory.newArray(QuestButton.class);
		this.counters = ArrayFactory.newArray(QuestCounter.class);
		this.rewards = ArrayFactory.newArray(QuestReward.class);
		this.event = new QuestEvent();
	}

	@Override
	public void finalyze() {
		prepare.clear();
		buttons.clear();
		event.clear();
		counters.clear();
		rewards.clear();
	}

	/**
	 * @return действия заданий.
	 */
	private Array<QuestButton> getButtons() {
		return buttons;
	}

	/**
	 * @return счетчики заданий.
	 */
	public Array<QuestCounter> getCounters() {
		return counters;
	}

	/**
	 * @return квестовое событие.
	 */
	private QuestEvent getEvent() {
		return event;
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_QUEST_STATE;
	}

	/**
	 * @return подготовленный буффер с данными
	 */
	public ByteBuffer getPrepare() {
		return prepare;
	}

	/**
	 * @return награды за задание.
	 */
	public Array<QuestReward> getRewards() {
		return rewards;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeBuffer(buffer, getPrepare());
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSlot;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Ответ сервера с информацией, которая необходима для входа в мир.
 * 
 * @author Ronn
 */
public class ResponseEnterInfo extends ServerPacket {

	private static final ResponseEnterInfo instance = new ResponseEnterInfo();

	public static ResponseEnterInfo getInstance(final PlayerShip playerShip) {

		final ResponseEnterInfo packet = (ResponseEnterInfo) instance.newInstance();

		final ByteBuffer buffer = packet.prepare;
		try {

			packet.writeInt(buffer, playerShip.getObjectId());
			packet.writeInt(buffer, playerShip.getClassId());

			final PlayerShipTemplate template = playerShip.getTemplate();

			packet.writeInt(buffer, template.getId());

			final String name = playerShip.getName();

			packet.writeByte(buffer, name.length());
			packet.writeString(buffer, name);

			final ModuleSystem system = playerShip.getModuleSystem();
			system.lock();
			try {

				final int count = system.getModulesCount();

				packet.writeByte(buffer, count);

				final ModuleSlot[] slots = system.getSlots();

				for(int i = 0, length = slots.length; i < length; i++) {

					final ModuleSlot slot = slots[i];

					if(slot.isEmpty()) {
						continue;
					}

					final Module module = slot.getModule();

					packet.writeInt(buffer, module.getObjectId());
					packet.writeInt(buffer, module.getClassId());
					packet.writeInt(buffer, module.getTemplateId());
					packet.writeByte(buffer, module.getIndex());
				}

			} finally {
				system.unlock();
			}

			packet.writeInt(buffer, playerShip.getLocationId());
			packet.writeVector(buffer, playerShip.getLocation());
			packet.writeRotation(buffer, playerShip.getRotation());

			int stationId = 0;

			final AtomicReference<SpaceStation> reference = playerShip.getStationReference();

			synchronized(reference) {

				final SpaceStation station = reference.get();

				if(station != null) {
					stationId = station.getObjectId();
				}
			}

			packet.writeInt(buffer, stationId);

			final AtomicBoolean onStation = playerShip.getOnStation();

			synchronized(onStation) {
				packet.writeByte(buffer, onStation.get() ? 1 : 0);
			}

		} finally {
			buffer.flip();
		}

		return packet;
	}

	/** промежуточной буффер */
	private final ByteBuffer prepare;

	public ResponseEnterInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_ENTER_INFO;
	}

	/**
	 * @return промежуточной буффер.
	 */
	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

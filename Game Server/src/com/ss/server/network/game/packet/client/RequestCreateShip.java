package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.impl.Account;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseCreateShip;
import com.ss.server.network.game.packet.server.ResponseCreateShip.CreateShipResult;

/**
 * Клиентский запрос на создание корабля.
 * 
 * @author Ronn
 */
public class RequestCreateShip extends ClientPacket {

	/** название корабля */
	private String name;

	/** ид фракции */
	private int fractionId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final Account account = client.getAccount();

		if(account == null) {
			client.sendPacket(ResponseCreateShip.getInstance(CreateShipResult.EXCEPTION));
			return;
		}

		final PlayerManager playerManager = PlayerManager.getInstance();
		playerManager.createPlayerShip(client, account.getName(), name, fractionId);
	}

	@Override
	public void finalyze() {
		name = null;
	}

	@Override
	protected void readImpl() {
		name = readString(readByte());
		fractionId = readByte();
	}
}

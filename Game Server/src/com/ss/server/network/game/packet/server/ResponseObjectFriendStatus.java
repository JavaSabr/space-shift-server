package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.FriendStatus;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет со статусом дружелбности объекта.
 * 
 * @author Ronn
 */
public class ResponseObjectFriendStatus extends ServerPacket {

	public static ResponseObjectFriendStatus getInstance(final PlayerShip ship, final SpaceObject object, final LocalObjects local) {

		final ResponseObjectFriendStatus packet = local.create(instance);
		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();
		packet.status = object.getFriendStatus(ship, local);

		return packet;
	}

	private static final ResponseObjectFriendStatus instance = new ResponseObjectFriendStatus();

	/** статус дружелюбности */
	private FriendStatus status;

	/** уникальный ид объекта */
	private int objectId;
	/** класс объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_FRIEND_STATUS;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeByte(buffer, status.ordinal());
	}
}

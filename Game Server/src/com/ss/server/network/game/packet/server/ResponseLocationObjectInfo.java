package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о локационном объекте.
 * 
 * @author Ronn
 */
public class ResponseLocationObjectInfo extends ServerPacket {

	public static ResponseLocationObjectInfo getInstance(final LocationObject object, final LocalObjects local) {

		final ResponseLocationObjectInfo packet = local.create(instance);
		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();
		packet.templateId = object.getTemplateId();
		packet.location.set(object.getLocation());
		packet.rotation.set(object.getRotation());

		return packet;
	}

	private static final ResponseLocationObjectInfo instance = new ResponseLocationObjectInfo();

	/** положение объекта */
	private final Vector location;
	/** разворот объекта */
	private final Rotation rotation;

	/** уникальный ид объекта */
	private int objectId;
	/** ид класса объекта */
	private int classId;
	/** ид шаблона объекта */
	private int templateId;

	public ResponseLocationObjectInfo() {
		this.location = Vector.newInstance();
		this.rotation = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_LOCATION_OBJECT_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, templateId);
		writeVector(buffer, location);
		writeRotation(buffer, rotation);
	}
}

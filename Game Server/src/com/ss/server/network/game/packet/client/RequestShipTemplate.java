package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.table.ShipTable;
import com.ss.server.template.ship.ShipTemplate;

/**
 * Клиентский пакет с запросом на полученин инфы об шаблоне корабля.
 * 
 * @author Ronn
 */
public class RequestShipTemplate extends ClientPacket {

	private static final ShipTable SHIP_TABLE = ShipTable.getInstance();

	/** ид шаблона корабля */
	private int templateId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final ShipTemplate template = SHIP_TABLE.getTemplate(templateId);

		if(template != null) {
			client.sendPacket(template.getInfoPacket(local), true);
		}
	}

	@Override
	protected void readImpl() {
		templateId = readInt();
	}
}

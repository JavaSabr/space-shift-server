package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.social.party.Party;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о составе группы.
 * 
 * @author Ronn
 */
public class ResponsePartyInfo extends ServerPacket {

	public static ResponsePartyInfo getInstance(final Party party, final LocalObjects local) {

		final ResponsePartyInfo packet = instance.newInstance();

		final ByteBuffer buffer = packet.getPrepare();

		final Array<PlayerShip> members = party.getMembers();
		final PlayerShip owner = party.getOwner();

		packet.writeInt(buffer, members.size());

		for(final PlayerShip member : members.array()) {

			if(member == null) {
				break;
			}

			final String name = member.getName();

			packet.writeInt(buffer, member.getObjectId());
			packet.writeInt(buffer, member.getClassId());
			packet.writeInt(buffer, name.length());
			packet.writeString(buffer, name);
			packet.writeByte(buffer, member == owner ? 1 : 0);
		}

		buffer.flip();

		return packet;
	}

	private static final ResponsePartyInfo instance = new ResponsePartyInfo();

	/** промежуточной буффер */
	private final ByteBuffer prepare;

	public ResponsePartyInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PARTY_INFO;
	}

	/**
	 * @return промежуточной буффер.
	 */
	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

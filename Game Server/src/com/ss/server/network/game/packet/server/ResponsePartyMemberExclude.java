package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о исключении члена группы.
 * 
 * @author Ronn
 */
public class ResponsePartyMemberExclude extends ServerPacket {

	public static ResponsePartyMemberExclude getInstance(final PlayerShip playerShip) {

		final ResponsePartyMemberExclude packet = (ResponsePartyMemberExclude) instance.newInstance();

		packet.objectId = playerShip.getObjectId();
		packet.classId = playerShip.getClassId();

		return packet;
	}

	private static final ResponsePartyMemberExclude instance = new ResponsePartyMemberExclude();

	/** уникальный ид члена группы */
	private int objectId;
	/** ид класса члена группы */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PARTY_MEMBER_EXCLUDE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

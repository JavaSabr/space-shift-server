package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Обновление статуса находения игрока в радиусе действия дружественной станции.
 * 
 * @author Ronn
 */
public class ResponseChangeStationRadius extends ServerPacket {

	public static ResponseChangeStationRadius getInstance(final LocalObjects local, final SpaceStation spaceStation, final boolean entered) {

		final ResponseChangeStationRadius packet = local.create(instance);
		packet.objectId = spaceStation.getObjectId();
		packet.classId = spaceStation.getClassId();
		packet.entered = entered ? 1 : 0;

		return packet;
	}

	private static final ResponseChangeStationRadius instance = new ResponseChangeStationRadius();

	/** уникальный ид станции */
	private int objectId;
	/** класс ид станции */
	private int classId;
	/** статус индикатора */
	private int entered;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_CHANGE_STATION_RADIUS;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, entered);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

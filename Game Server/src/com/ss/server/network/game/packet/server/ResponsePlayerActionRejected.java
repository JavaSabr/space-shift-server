package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об отмене запроса.
 * 
 * @author Ronn
 */
public class ResponsePlayerActionRejected extends ServerPacket {

	public static ResponsePlayerActionRejected getInstance() {
		return instance.newInstance();
	}

	private static final ResponsePlayerActionRejected instance = new ResponsePlayerActionRejected();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_ACTION_REJECTED;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Информация о космической станции.
 * 
 * @author Ronn
 */
public class ResponseSpaceStationInfo extends ServerPacket {

	public static ResponseSpaceStationInfo getInstance(final SpaceStation station) {

		final ResponseSpaceStationInfo packet = (ResponseSpaceStationInfo) instance.newInstance();

		packet.objectId = station.getObjectId();
		packet.classId = station.getClassId();
		packet.templateId = station.getTemplateId();

		packet.radius = station.getRadius();
		packet.distance = station.getDistance();
		packet.turnTime = station.getTurnTime();
		packet.turnAroundTime = station.getTurnAroundTime();

		final GravityObject parent = station.getParent();

		if(parent == null) {
			packet.parentId = 0;
			packet.parentClassId = 0;
		} else {
			packet.parentId = parent.getObjectId();
			packet.parentClassId = parent.getClassId();
		}

		packet.location.set(station.getLocation());
		packet.rotation.set(station.getRotation());
		packet.orbitalRotation.set(station.getOrbitalRotation());

		packet.name = station.getName();

		return packet;
	}

	private static final ResponseSpaceStationInfo instance = new ResponseSpaceStationInfo();

	/** позиция станции */
	private final Vector location;
	/** разворот станции */
	private final Rotation rotation;
	/** разворот орбиты станции */
	private final Rotation orbitalRotation;

	/** название станции */
	private String name;

	/** уникальный ид станции */
	private int objectId;
	/** классовый ид станции */
	private int classId;
	/** ид владельца объекта */
	private int parentId;
	/** класс ид владельца */
	private int parentClassId;
	/** ид темплейта */
	private int templateId;
	/** радиус */
	private int radius;
	/** дистанция от центра */
	private int distance;
	/** время разворота вокруг своей оси */
	private int turnTime;

	/** время разворота вокруг орбиты */
	private long turnAroundTime;

	public ResponseSpaceStationInfo() {
		this.location = Vector.newInstance();
		this.rotation = Rotation.newInstance();
		this.orbitalRotation = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_STATION_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, templateId);
		writeInt(buffer, parentId);
		writeInt(buffer, parentClassId);

		writeInt(buffer, radius);
		writeInt(buffer, distance);
		writeInt(buffer, turnTime);
		writeLong(buffer, turnAroundTime);

		writeVector(buffer, location);
		writeRotation(buffer, rotation);
		writeRotation(buffer, orbitalRotation);

		writeByte(buffer, name.length());
		writeString(buffer, name);
	}
}

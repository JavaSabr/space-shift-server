package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о удалении корабля из мира.
 * 
 * @author Ronn
 */
public class ResponseShipDelete extends ServerPacket {

	private static final ResponseShipDelete instance = new ResponseShipDelete();

	public static ResponseShipDelete getInstance(final SpaceShip ship, final LocalObjects local) {

		final ResponseShipDelete packet = local.create(instance);
		packet.objectId = ship.getObjectId();
		packet.classId = ship.getClassId();

		return packet;
	}

	/** уникальный ид объекта */
	private volatile int objectId;
	/** классовый ид объекта */
	private volatile int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHIP_DELETE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

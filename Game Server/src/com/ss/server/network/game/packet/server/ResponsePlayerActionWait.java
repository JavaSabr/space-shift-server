package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerAction;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о ожидании ответа на запрос о действии.
 * 
 * @author Ronn
 */
public class ResponsePlayerActionWait extends ServerPacket {

	public static ResponsePlayerActionWait getInstance(final PlayerShip playerShip, final PlayerAction actionType) {

		final ResponsePlayerActionWait packet = (ResponsePlayerActionWait) instance.newInstance();

		packet.actionType = actionType;
		packet.objectId = playerShip.getObjectId();
		packet.classId = playerShip.getClassId();

		return packet;
	}

	private static final ResponsePlayerActionWait instance = new ResponsePlayerActionWait();

	/** тип действия */
	private PlayerAction actionType;

	/** уникальный ид объекта */
	private int objectId;
	/** класс ид объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_ACTION_WAIT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, actionType.ordinal());
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

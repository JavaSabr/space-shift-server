package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Env;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет со статусом корабля игрока.
 * 
 * @author Ronn
 */
public class ResponsePlayerShipStatus extends ServerPacket {

	public static final ResponsePlayerShipStatus getInstance(final SpaceShip ship, final LocalObjects local) {

		final Env env = local.getNextEnv();

		final int currentEnergy = ship.getCurrentShield();
		final int currentStrength = ship.getCurrentStrength();

		final float energyPercent = currentEnergy * 1F / Math.max(ship.getMaxShield(), 1F);
		final float strengthPercent = currentStrength * 1F / Math.max(ship.getMaxStrength(env), 1F);

		final ResponsePlayerShipStatus packet = local.create(instance);
		packet.forceShieldStrength = currentEnergy;
		packet.currentStrength = currentStrength;
		packet.forceShieldStrengthPercent = energyPercent;
		packet.strengthPercent = strengthPercent;

		return packet;
	}

	private static final ResponsePlayerShipStatus instance = new ResponsePlayerShipStatus();

	/** процент прочности щитов */
	private float forceShieldStrengthPercent;
	/** процент прочности */
	private float strengthPercent;

	/** текущее состояние прочности щитов */
	private int forceShieldStrength;
	/** текущее состояни прочности */
	private int currentStrength;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_SHIP_STATUS;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, forceShieldStrength);
		writeInt(buffer, currentStrength);
		writeFloat(buffer, forceShieldStrengthPercent);
		writeFloat(buffer, strengthPercent);
	}
}

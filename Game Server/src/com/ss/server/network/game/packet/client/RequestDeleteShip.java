package com.ss.server.network.game.packet.client;

import rlib.util.StringUtils;

import com.ss.server.LocalObjects;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.impl.Account;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseDeleteShip;
import com.ss.server.network.game.packet.server.ResponseDeleteShip.DeleteShipResult;

/**
 * Пакет запроса на удаление корабля.
 * 
 * @author Ronn
 */
public class RequestDeleteShip extends ClientPacket {

	/** название корабля */
	private String name;
	/** пароль игрока */
	private String password;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final Account account = client.getAccount();

		if(account == null) {
			return;
		}

		if(!StringUtils.equals(password, account.getPassword())) {
			client.sendPacket(ResponseDeleteShip.getInstance(DeleteShipResult.INCORRECT_PASSWORD), true);
			return;
		}

		final PlayerManager playerManager = PlayerManager.getInstance();

		if(playerManager.deletePlayerShip(account.getName(), name)) {
			client.sendPacket(ResponseDeleteShip.getInstance(DeleteShipResult.SUCCESSFULL), true);
		} else {
			client.sendPacket(ResponseDeleteShip.getInstance(DeleteShipResult.INCORRECT_NAME), true);
		}
	}

	@Override
	public void finalyze() {
		name = null;
		password = null;
	}

	@Override
	protected void readImpl() {
		name = readString(readByte());
		password = readString(readByte());
	}
}

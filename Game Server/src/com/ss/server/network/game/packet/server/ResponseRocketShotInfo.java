package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.shot.impl.RocketShot;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о ракетном выстреле.
 * 
 * @author Ronn
 */
public class ResponseRocketShotInfo extends ServerPacket {

	public static ResponseRocketShotInfo getInstance(final SpaceShip ship, final Module module, final RocketShot shot, final LocalObjects local) {

		final SpaceObject target = shot.getTarget();

		final ResponseRocketShotInfo packet = local.create(instance);
		packet.shipId = ship.getObjectId();
		packet.shipClassId = ship.getClassId();
		packet.targetObjectId = target.getObjectId();
		packet.targetClassId = target.getClassId();
		packet.moduleId = module.getObjectId();
		packet.moduleClassId = module.getClassId();
		packet.objectId = shot.getObjectId();
		packet.maxDistance = shot.getMaxDistance();
		packet.index = shot.getIndex();
		packet.speed = shot.getSpeed();
		packet.maxSpeed = shot.getMaxSpeed();
		packet.accel = shot.getAccel();
		packet.rotationSpeed = shot.getRotationSpeed();
		packet.startLoc.set(shot.getStartLoc());
		packet.rotation.set(shot.getRotation());

		return packet;
	}

	private static final ResponseRocketShotInfo instance = new ResponseRocketShotInfo();

	/** координаты старта */
	private final Vector startLoc;
	/** разворот выстрела */
	private final Rotation rotation;

	/** скорость ракеты */
	private float speed;
	/** скорость разворота ракеты */
	private float rotationSpeed;

	/** ид стрелявшего корабля */
	private int shipId;
	/** класс ид стрелявшего корабля */
	private int shipClassId;
	/** уникальный ид цели */
	private int targetObjectId;
	/** класс ид цели */
	private int targetClassId;
	/** ид стрелявшего модуля */
	private int moduleId;
	/** класс ид стрелявшего модуля */
	private int moduleClassId;

	/** уникальный ид ракеты */
	private int objectId;
	/** максимальная дистанция ракеты */
	private int maxDistance;
	/** максимальная скорость ракеты */
	private int maxSpeed;
	/** ускорение ракеты */
	private int accel;
	/** индекс ствола модуля */
	private int index;

	public ResponseRocketShotInfo() {
		this.startLoc = Vector.newInstance();
		this.rotation = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_ROCKET_SHOT_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, shipId);
		writeInt(buffer, shipClassId);
		writeInt(buffer, targetObjectId);
		writeInt(buffer, targetClassId);
		writeInt(buffer, moduleId);
		writeInt(buffer, moduleClassId);
		writeInt(buffer, objectId);
		writeInt(buffer, maxDistance);
		writeInt(buffer, maxSpeed);
		writeInt(buffer, index);
		writeInt(buffer, accel);
		writeFloat(buffer, speed);
		writeFloat(buffer, rotationSpeed);
		writeRotation(buffer, rotation);
	}
}

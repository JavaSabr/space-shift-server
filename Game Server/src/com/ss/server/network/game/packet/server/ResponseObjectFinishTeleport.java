package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с уведомлением о завершении телепорта объекта.
 * 
 * @author Ronn
 */
public class ResponseObjectFinishTeleport extends ServerPacket {

	public static ResponseObjectFinishTeleport getInstance(final SpaceObject object, final LocalObjects local) {

		final ResponseObjectFinishTeleport packet = local.create(instance);
		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();

		return packet;
	}

	private static final ResponseObjectFinishTeleport instance = new ResponseObjectFinishTeleport();

	/** уникальный ид объекта */
	private int objectId;
	/** класс ид объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_FINISH_TELEPORT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.Space;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerAction;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запрос на выполнени какого-то действия от игрока.
 * 
 * @author Ronn
 */
public class RequestPlayerAction extends ClientPacket {

	private static final Space SPACE = Space.getInstance();

	/** тип действия */
	private PlayerAction actionType;

	/** уникальный ид объекта */
	private int objectId;
	/** ид класса объекта */
	private int classId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		if(actionType == null || !actionType.isImplemented()) {
			return;
		}

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final SpaceLocation location = SPACE.getLocation(playerShip.getLocationId());
		final SpaceObject object = location.findObject(SpaceObject.class, playerShip, objectId, classId);

		if(object == null) {
			return;
		}

		final PlayerManager manager = PlayerManager.getInstance();
		manager.requestAction(actionType, playerShip, object, local);
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		classId = readInt();
		actionType = PlayerAction.valueOf(readByte());
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;

import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Разворот дебага объекта.
 * 
 * @author Ronn
 */
public class ResponseDebugObjectRotation extends ServerPacket {

	public static final ResponseDebugObjectRotation getInstance(final SpaceObject object, final Rotation rotation) {

		final ResponseDebugObjectRotation packet = instance.newInstance();

		packet.objectId = object.getObjectId();
		packet.objectClassId = object.getClassId();
		packet.rotation.set(rotation);

		Thread.dumpStack();

		return packet;
	}

	private static final ResponseDebugObjectRotation instance = new ResponseDebugObjectRotation();

	/** разворот объекта */
	private final Rotation rotation;

	/** уникальный ид объекта */
	private int objectId;
	/** класс объекта */
	private int objectClassId;

	public ResponseDebugObjectRotation() {
		this.rotation = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_DEBUG_OBJECT_ROTATION;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, objectClassId);
		writeRotation(buffer, rotation);
	}
}

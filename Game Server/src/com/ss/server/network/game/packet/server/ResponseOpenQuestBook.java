package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с запросом на открытие квестовой книги.
 * 
 * @author Ronn
 */
public final class ResponseOpenQuestBook extends ServerConstPacket {

	public static ResponseOpenQuestBook getInstance() {
		return instance;
	}

	private static final ResponseOpenQuestBook instance = new ResponseOpenQuestBook();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OPEN_QUEST_BOOK;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

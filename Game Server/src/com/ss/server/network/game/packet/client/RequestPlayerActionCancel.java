package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет с уведомлением об отмене запроса на действие.
 * 
 * @author Ronn
 */
public class RequestPlayerActionCancel extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			LOGGER.warning(this, "not found player ship.");
			return;
		}

		final PlayerManager manager = PlayerManager.getInstance();
		manager.actionCancel(playerShip, local);
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Space;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseShipStatus;

/**
 * Запрос клиента на получение статуса корабля.
 * 
 * @author Ronn
 */
public class RequestShipStatus extends ClientPacket {

	private static final Space SPACE = Space.getInstance();

	/** уникальный ид корабля */
	private int objectId;
	/** класовый ид корабля */
	private int classId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final SpaceLocation location = SPACE.getLocation(playerShip);
		final SpaceShip ship = location.findObject(SpaceShip.class, playerShip, objectId, classId);

		if(ship == null) {
			return;
		}

		playerShip.sendPacket(ResponseShipStatus.getInstance(ship, local), true);
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		classId = readInt();
	}
}

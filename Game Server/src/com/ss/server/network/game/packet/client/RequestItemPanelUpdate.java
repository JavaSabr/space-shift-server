package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на обновление положения предмета на панели.
 * 
 * @author Ronn
 */
public class RequestItemPanelUpdate extends ClientPacket {

	/** уникальный ид предмета */
	private int objectId;
	/** новое положение */
	private int oldOrder;
	/** предыдущее положение */
	private int newOrder;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip != null) {
			playerShip.updateItemPanel(objectId, oldOrder, newOrder);
		}
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		readInt();
		oldOrder = readByte();
		newOrder = readByte();
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;

import com.ss.server.Config;
import com.ss.server.model.faction.Fraction;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSlot;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.ship.player.PlayerShipPreview;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Список кораблей на аккаунте.
 * 
 * @author Ronn
 */
public class ResponseShipList extends ServerPacket {

	public static ResponseShipList getInstance(final Array<PlayerShipPreview> shipList) {

		final ResponseShipList packet = (ResponseShipList) instance.newInstance();

		final ByteBuffer buffer = packet.prepare;

		packet.writeByte(buffer, shipList.size());

		for(final PlayerShipPreview preview : shipList) {

			packet.writeInt(buffer, preview.getObjectId());
			packet.writeInt(buffer, Config.SERVER_PLAYER_SHIP_CLASS_ID);

			final PlayerShipTemplate template = preview.getTemplate();

			packet.writeInt(buffer, template.getId());

			String name = preview.getName();

			packet.writeByte(buffer, name.length());
			packet.writeString(buffer, name);

			final Fraction fraction = preview.getFraction();

			name = fraction.getName();

			packet.writeByte(buffer, name.length());
			packet.writeString(buffer, name);

			packet.writeInt(buffer, preview.getCurrentStrength());
			packet.writeInt(buffer, preview.getMaxStrength());
			packet.writeInt(buffer, preview.getCurrentShield());
			packet.writeInt(buffer, preview.getMaxShield());

			final ModuleSystem system = preview.getModuleSystem();

			system.lock();
			try {

				final int count = system.getModulesCount();

				packet.writeByte(buffer, count);

				final ModuleSlot[] slots = system.getSlots();

				for(int i = 0, length = slots.length; i < length; i++) {

					final ModuleSlot slot = slots[i];

					if(slot.isEmpty()) {
						continue;
					}

					final Module module = slot.getModule();

					packet.writeInt(buffer, module.getObjectId());
					packet.writeInt(buffer, module.getClassId());
					packet.writeInt(buffer, module.getTemplateId());
					packet.writeByte(buffer, module.getIndex());
				}
			} finally {
				system.unlock();
			}
		}

		return packet;
	}

	private static final ResponseShipList instance = new ResponseShipList();

	private final ByteBuffer prepare;

	public ResponseShipList() {
		super();
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHIP_LIST;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		prepare.flip();
		buffer.put(prepare);
	}
}

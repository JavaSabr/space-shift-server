package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о результате запроса на удаление корабля.
 * 
 * @author Ronn
 */
public class ResponseDeleteShip extends ServerPacket {

	private static final ResponseDeleteShip instance = new ResponseDeleteShip();

	public static enum DeleteShipResult {
		SUCCESSFULL,
		INCORRECT_NAME,
		INCORRECT_PASSWORD,
	}

	public static ResponseDeleteShip getInstance(final DeleteShipResult result) {

		final ResponseDeleteShip packet = (ResponseDeleteShip) instance.newInstance();
		packet.result = result;

		return packet;
	}

	/** результат попытки удаления корабля */
	private volatile DeleteShipResult result;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_DELETE_SHIP;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, result.ordinal());
	}
}

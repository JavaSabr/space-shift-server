package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.quest.QuestState;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с информацией о состоянии счетчика задания.
 * 
 * @author Ronn
 */
public class ResponseQuestCounterInfo extends ServerPacket {

	public static ServerPacket getInstance(final QuestState state, final int counterId, final int value) {

		final ResponseQuestCounterInfo packet = instance.newInstance();

		packet.objectId = state.getObjectId();
		packet.questId = state.getQuestId();
		packet.counterId = counterId;
		packet.value = value;
		return packet;
	}

	private static final ResponseQuestCounterInfo instance = new ResponseQuestCounterInfo();

	/** уникальный ид задания */
	private int objectId;
	/** ид задания */
	private int questId;
	/** ид счетчика */
	private int counterId;
	/** новое значение счетчика */
	private int value;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_QUEST_COUNTER_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, questId);
		writeInt(buffer, counterId);
		writeInt(buffer, value);
	}
}

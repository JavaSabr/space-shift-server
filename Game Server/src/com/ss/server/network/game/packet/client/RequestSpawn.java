package com.ss.server.network.game.packet.client;

import java.util.concurrent.atomic.AtomicBoolean;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на создание корабля.
 * 
 * @author Ronn
 */
public class RequestSpawn extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final AtomicBoolean onStation = playerShip.getOnStation();

		synchronized(onStation) {
			onStation.set(false);
		}

		playerShip.spawnMe(local);
		playerShip.updateInfo(local);
	}
}

package com.ss.server.network.game.packet.server;

import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с уведомлением о телепортаровании корабля на космическую станцию.
 * 
 * @author Ronn
 */
public class ResponseShipTeleportedToStation extends ServerConstPacket {

	public static ResponseShipTeleportedToStation getInstance() {
		return instance;
	}

	private static final ResponseShipTeleportedToStation instance = new ResponseShipTeleportedToStation();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHIP_TELEPORTED_TO_STATION;
	}
}

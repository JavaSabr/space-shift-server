package com.ss.server.network.game.packet.client;

import java.util.function.Consumer;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.shop.TransactionItemInfo;
import com.ss.server.model.station.hangar.window.impl.ItemShopHangarWindow;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Клиентский пакет с запросом на транзакцию предметов.
 * 
 * @author Ronn
 */
public class RequestItemShopTransaction extends ClientPacket {

	/** пул с объектами информации */
	private final FoldablePool<TransactionItemInfo> pool = PoolFactory.newFoldablePool(TransactionItemInfo.class);

	/** функция возврата объектов в пул */
	private final Consumer<TransactionItemInfo> finalyzeFunc = info -> {
		pool.put(info);
	};

	/** покупаемые предметы */
	private final Array<TransactionItemInfo> buyItems;
	/** продаваемые предметы */
	private final Array<TransactionItemInfo> sellItems;

	public RequestItemShopTransaction() {
		this.buyItems = ArrayFactory.newArray(TransactionItemInfo.class);
		this.sellItems = ArrayFactory.newArray(TransactionItemInfo.class);
	}

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final ItemShopHangarWindow widow = playerShip.findHangarWindow(ItemShopHangarWindow.class);

		if(widow != null) {
			widow.processTransaction(playerShip, buyItems, sellItems, local);
		}
	}

	@Override
	public void finalyze() {

		buyItems.forEach(finalyzeFunc);
		buyItems.clear();

		sellItems.forEach(finalyzeFunc);
		sellItems.clear();
	}

	private TransactionItemInfo getNextInfo() {

		TransactionItemInfo info = pool.take();

		if(info == null) {
			info = new TransactionItemInfo();
		}

		return info;
	}

	@Override
	protected void readImpl() {

		int count = readInt();

		for(int i = 0; i < count; i++) {

			final TransactionItemInfo info = getNextInfo();
			info.setTemplateId(readInt());
			info.setCount(readInt());

			buyItems.add(info);
		}

		count = readInt();

		for(int i = 0; i < count; i++) {

			final TransactionItemInfo info = getNextInfo();

			info.setObjectId(readInt());
			readInt();

			info.setTemplateId(readInt());
			info.setCount(readInt());

			sellItems.add(info);
		}
	}
}

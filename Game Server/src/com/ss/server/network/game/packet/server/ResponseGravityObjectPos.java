package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Vector;

import com.ss.server.model.gravity.GravityObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Информация о гравитационном объекте.
 * 
 * @author Ronn
 */
public class ResponseGravityObjectPos extends ServerPacket {

	public static ResponseGravityObjectPos getInstance(final GravityObject object) {

		final ResponseGravityObjectPos packet = (ResponseGravityObjectPos) instance.newInstance();

		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();
		packet.location.set(object.getLocation());

		return packet;
	}

	private static final ResponseGravityObjectPos instance = new ResponseGravityObjectPos();

	/** позиция объекта */
	private final Vector location;

	/** уникальный ид станции */
	private int objectId;
	/** классовый ид станции */
	private int classId;

	public ResponseGravityObjectPos() {
		this.location = Vector.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_GRAVITY_OBJECT_POS;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		writeInt(buffer, objectId);
		writeInt(buffer, classId);

		writeVector(buffer, location);
	}
}

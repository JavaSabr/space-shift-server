package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.shot.impl.BlasterShot;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о выстреле.
 * 
 * @author Ronn
 */
public class ResponseBlasterShotInfo extends ServerPacket {

	public static ResponseBlasterShotInfo getInstance(final SpaceShip ship, final Module module, final BlasterShot shot, final LocalObjects local) {

		final ResponseBlasterShotInfo packet = local.create(instance);
		packet.shipId = ship.getObjectId();
		packet.shipClassId = ship.getClassId();
		packet.moduleId = module.getObjectId();
		packet.moduleClassId = module.getClassId();
		packet.objectId = shot.getObjectId();
		packet.maxDistance = shot.getMaxDistance();
		packet.index = shot.getIndex();
		packet.speed = shot.getSpeed();
		packet.targetLoc.set(shot.getTargetLoc());

		return packet;
	}

	private static final ResponseBlasterShotInfo instance = new ResponseBlasterShotInfo();

	/** целевая точка */
	private final Vector targetLoc;

	/** текущая скорость выстрела */
	private float speed;

	/** ид стрелявшего корабля */
	private int shipId;
	/** класс ид стрелявшего корабля */
	private int shipClassId;
	/** ид стрелявшего модуля */
	private int moduleId;
	/** класс ид стрелявшего модуля */
	private int moduleClassId;

	/** уникальный ид выстрела */
	private int objectId;
	/** максимальная дистанция выстрела */
	private int maxDistance;

	/** индекс ствола модуля */
	private int index;

	public ResponseBlasterShotInfo() {
		this.targetLoc = Vector.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_BLASTER_SHOT_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, shipId);
		writeInt(buffer, shipClassId);
		writeInt(buffer, moduleId);
		writeInt(buffer, moduleClassId);
		writeInt(buffer, objectId);
		writeInt(buffer, maxDistance);
		writeInt(buffer, index);
		writeFloat(buffer, speed);
		writeVector(buffer, targetLoc);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с обновлением ячеяки хранилища.
 * 
 * @author Ronn
 */
public class ResponseUpdateStorageCell extends ServerPacket {

	public static ResponseUpdateStorageCell getInstance(final int index, final LocalObjects local) {

		final ResponseUpdateStorageCell packet = local.create(instance);
		packet.objectId = 0;
		packet.classId = 0;
		packet.templateId = 0;
		packet.index = index;
		packet.itemCount = 0;

		return packet;
	}

	public static ResponseUpdateStorageCell getInstance(final SpaceItem item) {

		final ResponseUpdateStorageCell packet = instance.newInstance();
		packet.objectId = item.getObjectId();
		packet.classId = item.getClassId();
		packet.templateId = item.getTemplateId();
		packet.index = item.getIndex();
		packet.itemCount = item.getItemCount();

		return packet;
	}

	private static final ResponseUpdateStorageCell instance = new ResponseUpdateStorageCell();

	/** уникальный ид предмеиа */
	private int objectId;
	/** ид класса предмета */
	private int classId;
	/** индекс ячейки */
	private int index;
	/** ид шаблона предмета */
	private int templateId;

	/** кол-во предметов в ячейке */
	private long itemCount;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_UPDATE_STORAGE_CELL;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, templateId);
		writeInt(buffer, index);
		writeLong(buffer, itemCount);
	}
}

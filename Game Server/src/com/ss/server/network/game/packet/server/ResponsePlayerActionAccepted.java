package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об отмене запроса.
 * 
 * @author Ronn
 */
public class ResponsePlayerActionAccepted extends ServerPacket {

	public static ResponsePlayerActionAccepted getInstance() {
		return instance.newInstance();
	}

	private static final ResponsePlayerActionAccepted instance = new ResponsePlayerActionAccepted();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_ACTION_ACCEPTED;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

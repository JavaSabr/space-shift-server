package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с обновлением окна магазина станции.
 * 
 * @author Ronn
 */
public class ResponseUpdateItemShopHangarWindow extends ServerPacket {

	public static ResponseUpdateItemShopHangarWindow getInstance(final long credits, final LocalObjects local) {

		final ResponseUpdateItemShopHangarWindow packet = local.create(instance);
		packet.credits = credits;

		return packet;
	}

	private static final ResponseUpdateItemShopHangarWindow instance = new ResponseUpdateItemShopHangarWindow();

	/** текущее кол-во денег игрока */
	private long credits;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_UPDATE_ITEM_SHOP_HANGAR_WINDOW;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, credits);
	}
}

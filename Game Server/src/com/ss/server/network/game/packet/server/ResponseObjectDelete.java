package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о удалении объекта из мира.
 * 
 * @author Ronn
 */
public class ResponseObjectDelete extends ServerPacket {

	private static final ResponseObjectDelete instance = new ResponseObjectDelete();

	public static ResponseObjectDelete getInstance(final SpaceObject ship, final LocalObjects local) {

		final ResponseObjectDelete packet = local.create(instance);
		packet.objectId = ship.getObjectId();
		packet.classId = ship.getClassId();

		return packet;
	}

	/** уникальный ид объекта */
	private volatile int objectId;
	/** классовый ид объекта */
	private volatile int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_DELETE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с актуальным статусом распределения энергии.
 * 
 * @author Ronn
 */
public class ResponsePlayerShipEnergyBalance extends ServerPacket {

	public static ResponsePlayerShipEnergyBalance getInstance(final SpaceShip ship, final LocalObjects local) {

		final ResponsePlayerShipEnergyBalance packet = local.create(instance);
		packet.engine = ship.getEngineEnergy();

		return packet;
	}

	private static final ResponsePlayerShipEnergyBalance instance = new ResponsePlayerShipEnergyBalance();

	/** распределение на двигатель */
	private float engine;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_SHIP_ENERGY_BALANCE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeFloat(buffer, engine);
	}
}

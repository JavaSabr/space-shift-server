package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.table.StationTable;
import com.ss.server.template.StationTemplate;

/**
 * Клиентский пакет с запросом шаблона станции.
 * 
 * @author Rronn
 */
public class RequestStationTemplate extends ClientPacket {

	private static final StationTable STATION_TABLE = StationTable.getInstance();

	/** ид темплейта */
	private int templateId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final StationTemplate template = STATION_TABLE.getTemplate(templateId);

		if(template != null) {
			client.sendPacket(template.getInfoPacket(local), true);
		}
	}

	@Override
	protected void readImpl() {
		templateId = readInt();
	}
}

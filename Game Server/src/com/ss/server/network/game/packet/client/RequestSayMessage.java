package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.manager.CommandManager;
import com.ss.server.model.MessageType;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на создание корабля.
 * 
 * @author Ronn
 */
public class RequestSayMessage extends ClientPacket {

	/** тип сообщения */
	private MessageType type;

	/** содержание сообщения */
	private String message;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		final CommandManager manager = CommandManager.getInstance();

		if(manager.isCommand(message)) {
			manager.execute(playerShip, message);
			return;
		}

		playerShip.getAI().startSay(type, message, local);
	}

	@Override
	protected void readImpl() {
		type = MessageType.values()[readByte()];
		message = readString(readByte());
	}
}

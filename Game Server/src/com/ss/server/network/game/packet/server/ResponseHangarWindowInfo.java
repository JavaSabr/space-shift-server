package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией об окне ангара
 * 
 * @author Ronn
 */
public class ResponseHangarWindowInfo extends ServerPacket {

	public static ResponseHangarWindowInfo getInstance(final Hangar hangar, final PlayerShip playerShip, final LocalObjects local) {

		final ResponseHangarWindowInfo packet = instance.newInstance();

		final ByteBuffer buffer = packet.getPrepare();

		final Array<HangarWindow> windows = playerShip.getOpeHangarWindows();
		windows.readLock();
		try {

			packet.writeByte(buffer, windows.size());

			final SpaceStation station = hangar.getParent();

			for(final HangarWindow window : windows.array()) {

				if(window == null) {
					break;
				}

				window.writeTo(packet, buffer, hangar, station, playerShip, local);
			}

		} finally {
			windows.readUnlock();
		}

		buffer.flip();

		return packet;
	}

	private static final ResponseHangarWindowInfo instance = new ResponseHangarWindowInfo();

	/** промежуточной буффер */
	private final ByteBuffer prepare;

	public ResponseHangarWindowInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_HANGAR_WINDOW_INFO;
	}

	/**
	 * @return промежуточной буффер.
	 */
	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

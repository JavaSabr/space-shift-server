package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.server.LocalObjects;
import com.ss.server.model.func.stat.StatType;
import com.ss.server.model.impl.Env;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с дополнительной информацией об корабле игрока.
 * 
 * @author Ronn
 */
public class ResponseUserInfo extends ServerPacket {

	public static final ResponseUserInfo getInstance(final PlayerShip ship, final LocalObjects local) {

		final ResponseUserInfo packet = local.create(instance);

		final Env env = local.getNextEnv();

		final ByteBuffer prepare = packet.getPrepare();
		prepare.clear();
		try {

			final int currentShield = ship.getCurrentShield();
			final int currentStrength = ship.getCurrentStrength();
			final int maxShield = ship.getMaxShield();
			final int maxStrength = ship.getMaxStrength(env);

			int energyRegen = (int) ship.calcStat(StatType.REGEN_ENERGY, 0, env);
			energyRegen = Math.max(energyRegen - (int) ship.calcStat(StatType.ENGINE_CONSUME_ENERGY, 0, env), 0);

			final float shieldPercent = maxShield < 1 ? 0 : currentShield * 1F / Math.max(maxShield, 1F);
			final float strengthPercent = currentStrength * 1F / ship.getMaxStrength(env);

			final float rotateSpeed = ship.calcStat(StatType.ROTATE_SPEED, 0, env);

			packet.writeLong(prepare, ship.getExp());
			packet.writeFloat(prepare, rotateSpeed);
			packet.writeFloat(prepare, shieldPercent);
			packet.writeFloat(prepare, strengthPercent);

			packet.writeInt(prepare, ship.getLevel());
			packet.writeInt(prepare, currentShield);
			packet.writeInt(prepare, maxShield);
			packet.writeInt(prepare, energyRegen);
			packet.writeInt(prepare, currentStrength);
			packet.writeInt(prepare, maxStrength);

		} finally {
			prepare.flip();
		}

		return packet;
	}

	private static final ResponseUserInfo instance = new ResponseUserInfo();

	/** буфер с подготовленными данными */
	private final ByteBuffer prepare;

	public ResponseUserInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_USER_INFO;
	}

	/**
	 * @return буфер с подготовленными данными.
	 */
	public ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

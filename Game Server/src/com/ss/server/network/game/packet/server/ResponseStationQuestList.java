package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.QuestCounter;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestPacketUtils;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.QuestReward;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с информацией о доступных заданиях на космической станции.
 * 
 * @author Ronn
 */
public class ResponseStationQuestList extends ServerPacket {

	public static final ServerPacket getInstance(final SpaceStation station, final PlayerShip ship, final LocalObjects local) {

		final ResponseStationQuestList packet = local.create(instance);

		if(ship == null) {
			return packet;
		}

		final QuestEvent event = packet.getEvent();
		event.setPlayerShip(ship);
		event.setStation(station);

		final Array<Quest> quests = local.getNextQuestList();
		station.addAvailableQuests(quests, event, local);

		final QuestList questList = ship.getQuestList();

		// отсикаем дупликаты заданий доступных на станции и уже взятых
		if(!quests.isEmpty()) {
			questList.asynLock();
			try {

				final Quest[] array = quests.array();

				for(int i = 0, length = quests.size(); i < length; i++) {

					Quest quest = array[i];

					if(questList.getQuestState(quest) != null) {
						quests.fastRemove(i);
						i--;
						length--;
					}
				}

			} finally {
				questList.asynUnlock();
			}
		}

		final ByteBuffer buffer = packet.getPrepare();
		try {

			// запись заданий доступных для взятие на станции
			writeStationQuests(local, packet, buffer, event, quests);

			// запись активных текущих заданий
			writeActiveQuests(local, packet, buffer, event, questList);

		} finally {
			buffer.flip();
		}

		return packet;
	}

	/**
	 * Запись активных заданий игрока.
	 */
	protected static void writeActiveQuests(final LocalObjects local, final ResponseStationQuestList packet, final ByteBuffer buffer, final QuestEvent event, final QuestList questList) {

		final Array<QuestState> active = questList.getActive();

		questList.asynLock();
		try {

			packet.writeInt(buffer, active.size());

			if(!active.isEmpty()) {

				final Array<QuestButton> buttons = packet.getButtons();
				final Array<QuestCounter> counters = packet.getCounters();
				final Array<QuestReward> rewards = packet.getRewards();

				for(final QuestState state : active.array()) {

					if(state == null) {
						break;
					}

					final Quest quest = state.getQuest();
					event.setQuest(quest);

					quest.addRewardsTo(rewards.clear(), event, local);
					quest.addStateCountersTo(counters.clear(), event, state.getState());
					quest.addStateButtonsTo(buttons.clear(), event, state.getState());
					quest.addButtons(buttons, event);

					// записб тело задания
					QuestPacketUtils.writeQuest(packet, buffer, state, quest);
					// запись наград за задание
					QuestPacketUtils.writeRewards(packet, buffer, event, rewards);
					// запись текущих счетчиков
					QuestPacketUtils.writeCounters(packet, buffer, counters, state);
					// запись доступных действий
					QuestPacketUtils.writeButtons(packet, buffer, buttons);
				}
			}
		} finally {
			questList.asynUnlock();
		}
	}

	/**
	 * Запись доступных для взятия заданий на станции.
	 */
	protected static void writeStationQuests(final LocalObjects local, final ResponseStationQuestList packet, final ByteBuffer buffer, final QuestEvent event, final Array<Quest> quests) {

		packet.writeInt(buffer, quests.size());

		if(!quests.isEmpty()) {

			final Array<QuestButton> buttons = packet.getButtons();
			final Array<QuestReward> rewards = packet.getRewards();

			for(final Quest quest : quests.array()) {

				if(quest == null) {
					break;
				}

				event.setQuest(quest);

				quest.addRewardsTo(rewards.clear(), event, local);
				quest.addButtons(buttons.clear(), event);

				// запись тело задания
				QuestPacketUtils.writeQuest(packet, buffer, quest);
				// запись награды за задание
				QuestPacketUtils.writeRewards(packet, buffer, event, rewards);

				// запись кол-ва счетчиков
				packet.writeByte(buffer, QuestPacketUtils.NO_COUNTERS);

				if(buttons.size() != 1) {
					LOGGER.warning("incorrect button count for quest " + quest + " and event " + event);
				}

				// запись доступных действий
				QuestPacketUtils.writeButtons(packet, buffer, buttons);
			}
		}
	}

	private static final ResponseStationQuestList instance = new ResponseStationQuestList();

	/** подготовленный буффер с данными */
	private final ByteBuffer prepare;

	/** действия заданий */
	private final Array<QuestButton> buttons;
	/** награды за задание */
	private final Array<QuestReward> rewards;
	/** счетчики заданий */
	private final Array<QuestCounter> counters;

	/** квестовое событие */
	private final QuestEvent event;

	public ResponseStationQuestList() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
		this.buttons = ArrayFactory.newArray(QuestButton.class);
		this.rewards = ArrayFactory.newArray(QuestReward.class);
		this.counters = ArrayFactory.newArray(QuestCounter.class);
		this.event = new QuestEvent();
	}

	@Override
	public void finalyze() {
		prepare.clear();
		buttons.clear();
		event.clear();
		rewards.clear();
		counters.clear();
	}

	/**
	 * @return действия заданий.
	 */
	private Array<QuestButton> getButtons() {
		return buttons.clear();
	}

	/**
	 * @return счетчики заданий.
	 */
	private Array<QuestCounter> getCounters() {
		return counters.clear();
	}

	/**
	 * @return квестовое событие.
	 */
	private QuestEvent getEvent() {
		return event;
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_STATION_QUEST_LIST;
	}

	/**
	 * @return подготовленный буффер с данными
	 */
	public ByteBuffer getPrepare() {
		return prepare;
	}

	/**
	 * @return награды за задание.
	 */
	private Array<QuestReward> getRewards() {
		return rewards.clear();
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeBuffer(buffer, getPrepare());
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.client.command.ClientCommandHandler;
import com.ss.server.client.command.impl.CollissionDestroyCommandHandler;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Запрос на выполнение клиентской команды над кораблем игрока.
 * 
 * @author Ronn
 */
public class RequestPlayerShipCommand extends ClientPacket {

	public static enum PlayerShipCommand {
		COLLISSION_DESTROY(new CollissionDestroyCommandHandler()), ;

		private static final PlayerShipCommand[] COMMANDS = values();

		/** обработчик клиентской команды */
		private final ClientCommandHandler handler;

		private PlayerShipCommand(ClientCommandHandler handler) {
			this.handler = handler;
		}

		/**
		 * @return обработчик клиентской команды.
		 */
		private ClientCommandHandler getHandler() {
			return handler;
		}
	}

	/** команла, которую просит клиент исполнить */
	private PlayerShipCommand command;

	@Override
	protected void readImpl() {
		command = PlayerShipCommand.COMMANDS[readByte()];
	}

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final ClientCommandHandler handler = command.getHandler();

		if(handler == null) {
			LOGGER.warning(this, "not implemented client command " + command);
			return;
		}

		handler.execute(client, client.getOwner());
	}
}

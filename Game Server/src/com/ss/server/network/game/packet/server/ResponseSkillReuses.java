package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Список актуальных перезарядок умений корабля игрока.
 * 
 * @author Ronn
 */
public class ResponseSkillReuses extends ServerPacket {

	public static ResponseSkillReuses getInstance(final PlayerShip ship) {

		final ResponseSkillReuses packet = (ResponseSkillReuses) instance.newInstance();

		final ByteBuffer buffer = packet.prepare;

		// final SkillReuseList skillReuseList = ship.getSkillReuseList();
		// skillReuseList.lock();
		// try {
		//
		// final Array<ReuseDelayInfo> reuseInfos =
		// skillReuseList.getActualReuses();
		//
		// packet.writeInt(buffer, reuseInfos.size());
		//
		// for(final ReuseDelayInfo info : reuseInfos.array()) {
		//
		// if(info == null) {
		// break;
		// }
		//
		// final long startTime = info.getEndTime() - info.getReuseTime();
		//
		// packet.writeLong(buffer, startTime);
		// packet.writeInt(buffer, info.getModuleId());
		// packet.writeInt(buffer, info.getReuseId());
		// packet.writeInt(buffer, info.getReuseTime());
		// }
		//
		// } finally {
		// skillReuseList.unlock();
		// }

		buffer.flip();

		return packet;
	}

	private static final ResponseSkillReuses instance = new ResponseSkillReuses();

	/** подготавливаемый буффер */
	private final ByteBuffer prepare;

	public ResponseSkillReuses() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SKILL_REUSES;
	}

	/**
	 * @return подготавливаемый буффер.
	 */
	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

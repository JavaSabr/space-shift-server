package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacketType;

/**
 * Запрос на открытие панели скилов.
 * 
 * @author Ronn
 */
public class ResponseOpenSkillPanel extends ServerConstPacket {

	public static ServerConstPacket getInstance() {
		return instance.newInstance();
	}

	private static final ResponseOpenSkillPanel instance = new ResponseOpenSkillPanel();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OPEN_SKILL_PANEL;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

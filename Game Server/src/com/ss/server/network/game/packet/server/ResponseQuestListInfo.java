package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.QuestCounter;
import com.ss.server.model.quest.QuestDate;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.QuestStatus;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.QuestReward;
import com.ss.server.model.quest.reward.QuestRewardClass;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с инфой о состоянии журнала заданий.
 * 
 * @author Ronn
 */
public class ResponseQuestListInfo extends ServerPacket {

	public static final ServerPacket getInstance(final PlayerShip ship, final LocalObjects local) {

		final ResponseQuestListInfo packet = instance.newInstance();

		if(ship == null) {
			return packet;
		}

		final ByteBuffer buffer = packet.getPrepare();
		final QuestList questList = ship.getQuestList();

		final Table<IntKey, QuestDate> completed = questList.getCompleted();
		completed.readLock();
		try {

			final Array<QuestDate> container = completed.values(packet.getContainer());

			packet.writeInt(buffer, container.size());

			if(!container.isEmpty()) {

				for(final QuestDate date : container.array()) {

					if(date == null) {
						break;
					}

					final QuestStatus status = date.getStatus();
					final Quest quest = date.getQuest();
					final String name = quest.getName();

					packet.writeInt(buffer, quest.getId());
					packet.writeByte(buffer, name.length());
					packet.writeString(buffer, name);
					packet.writeByte(buffer, status.ordinal());
				}
			}

		} finally {
			completed.readUnlock();
		}

		final Array<QuestState> active = questList.getActive();
		active.readLock();
		try {

			packet.writeInt(buffer, active.size());

			if(!active.isEmpty()) {

				final Array<QuestButton> buttons = packet.getButtons();
				final Array<QuestCounter> counters = packet.getCounters();
				final Array<QuestReward> rewards = packet.getRewards();

				final QuestEvent event = packet.getEvent();
				event.setPlayerShip(ship);

				for(final QuestState state : active.array()) {

					if(state == null) {
						break;
					}

					final Quest quest = state.getQuest();
					event.setQuest(quest);

					String name = quest.getName();
					String description = quest.getDescription(state.getState());

					packet.writeInt(buffer, quest.getId());
					packet.writeInt(buffer, state.getObjectId());
					packet.writeByte(buffer, state.isFavorite() ? 1 : 0);
					packet.writeByte(buffer, name.length());
					packet.writeString(buffer, name);
					packet.writeByte(buffer, description.length());
					packet.writeString(buffer, description);

					quest.addRewardsTo(rewards.clear(), event, local);

					packet.writeByte(buffer, rewards.size());

					if(!rewards.isEmpty()) {
						for(final QuestReward reward : rewards.array()) {

							if(reward == null) {
								break;
							}

							final QuestRewardClass rewardClass = reward.getRewardClass();
							packet.writeByte(buffer, rewardClass.ordinal());
							rewardClass.writeTo(packet, buffer, reward, event);
						}
					}

					quest.addStateCountersTo(counters.clear(), event, state.getState());

					packet.writeByte(buffer, counters.size());

					if(!counters.isEmpty()) {
						for(final QuestCounter counter : counters.array()) {

							if(counter == null) {
								break;
							}

							description = counter.getDescription();

							packet.writeByte(buffer, counter.getId());
							packet.writeInt(buffer, counter.getValue(state));
							packet.writeInt(buffer, counter.getLimit());
							packet.writeByte(buffer, description.length());
							packet.writeString(buffer, description);
						}
					}

					quest.addStateButtonsTo(buttons.clear(), event, state.getState());

					packet.writeByte(buffer, buttons.size());

					if(!buttons.isEmpty()) {
						for(final QuestButton button : buttons.array()) {

							if(button == null) {
								break;
							}

							name = button.getName();

							packet.writeByte(buffer, button.getId());
							packet.writeByte(buffer, name.length());
							packet.writeString(buffer, name);
						}
					}
				}
			}
		} finally {
			active.readUnlock();
		}

		buffer.flip();

		return packet;
	}

	private static final ResponseQuestListInfo instance = new ResponseQuestListInfo();

	/** подготовленный буффер с данными */
	private final ByteBuffer prepare;

	/** контейнер выполненных квестов */
	private final Array<QuestDate> container;
	/** действия заданий */
	private final Array<QuestButton> buttons;
	/** счетчики заданий */
	private final Array<QuestCounter> counters;
	/** награды за задание */
	private final Array<QuestReward> rewards;

	/** квестовое событие */
	private final QuestEvent event;

	public ResponseQuestListInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
		this.container = ArrayFactory.newArray(QuestDate.class);
		this.buttons = ArrayFactory.newArray(QuestButton.class);
		this.counters = ArrayFactory.newArray(QuestCounter.class);
		this.rewards = ArrayFactory.newArray(QuestReward.class);
		this.event = new QuestEvent();
	}

	@Override
	public void finalyze() {
		prepare.clear();
		buttons.clear();
		event.clear();
		counters.clear();
		rewards.clear();
		container.clear();
	}

	/**
	 * @return действия заданий.
	 */
	private Array<QuestButton> getButtons() {
		return buttons;
	}

	/**
	 * @return контейнер выполненных квестов.
	 */
	private Array<QuestDate> getContainer() {
		return container;
	}

	/**
	 * @return счетчики заданий.
	 */
	private Array<QuestCounter> getCounters() {
		return counters;
	}

	/**
	 * @return квестовое событие.
	 */
	private QuestEvent getEvent() {
		return event;
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_QUEST_LIST_INFO;
	}

	/**
	 * @return подготовленный буффер с данными
	 */
	public ByteBuffer getPrepare() {
		return prepare;
	}

	/**
	 * @return награды за задание.
	 */
	private Array<QuestReward> getRewards() {
		return rewards;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseSkillReuses;

/**
 * Запрос на получения списка перезарядок текущих умений.
 * 
 * @author Ronn
 */
public class RequestSkillReuses extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			return;
		}

		playerShip.sendPacket(ResponseSkillReuses.getInstance(playerShip), true);
	}
}

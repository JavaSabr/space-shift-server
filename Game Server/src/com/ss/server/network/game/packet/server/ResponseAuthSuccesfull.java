package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.time.ZoneId;

import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с подтверждением входа на сервер игрока.
 * 
 * @author Ronn
 */
public class ResponseAuthSuccesfull extends ServerConstPacket {

	public static ResponseAuthSuccesfull getInstance() {
		return instance;
	}

	private static final ResponseAuthSuccesfull instance = new ResponseAuthSuccesfull();

	/** временная зона сервера */
	private final ZoneId zoneId;

	public ResponseAuthSuccesfull() {
		this.zoneId = ZoneId.systemDefault();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.AUTH_SUCCESSFUL;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final String id = zoneId.getId();

		writeInt(buffer, id.length());
		writeString(buffer, id);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о результате запроса создания корабля.
 * 
 * @author Ronn
 */
public class ResponseCreateShip extends ServerPacket {

	public static enum CreateShipResult {
		SUCCESSFULL,
		INCORRECT_NAME,
		LIMITED,
		FRACTION_PROBLEMS,
		EXCEPTION,
	}

	public static ResponseCreateShip getInstance(final CreateShipResult result) {

		final ResponseCreateShip packet = (ResponseCreateShip) instance.newInstance();
		packet.result = result;

		return packet;
	}

	private static final ResponseCreateShip instance = new ResponseCreateShip();

	/** результат попытки создания нового корабля */
	private CreateShipResult result;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_CREATE_SHIP;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, result.ordinal());
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseExit;

/**
 * Пакет с запросом о выходе из игры.
 * 
 * @author Ronn
 */
public class RequestExit extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			LOGGER.warning(this, "not found player ship.");
			return;
		}

		playerShip.deleteMe(local);

		client.setOwner(null);
		client.sendPacket(ResponseExit.getInstance());
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с уведомлением о том, что модуль был заюзан.
 * 
 * @author Ronn
 */
public class ResponseModuleUse extends ServerPacket {

	private static final ResponseModuleUse instance = new ResponseModuleUse();

	public static ResponseModuleUse getInstance(final SpaceShip ship, final Module module, final Skill skill, final LocalObjects local) {

		final ResponseModuleUse packet = local.create(instance);
		packet.shipId = ship.getObjectId();
		packet.shipClassId = ship.getClassId();
		packet.moduleId = module.getObjectId();
		packet.moduleClassId = module.getClassId();
		packet.skillId = skill.getId();

		return packet;
	}

	/** уникальный ид корабля */
	private volatile int shipId;
	/** ид класса корабля */
	private volatile int shipClassId;
	/** уникальный ид модуля */
	private volatile int moduleId;
	/** ид класса модуля */
	private volatile int moduleClassId;
	/** ид скила */
	private volatile int skillId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_MODULE_USE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, shipId);
		writeInt(buffer, shipClassId);
		writeInt(buffer, moduleId);
		writeInt(buffer, moduleClassId);
		writeInt(buffer, skillId);
	}
}

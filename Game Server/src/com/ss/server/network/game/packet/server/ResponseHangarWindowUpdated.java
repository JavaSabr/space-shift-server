package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией об завершении обновления окон ангара.
 * 
 * @author Ronn
 */
public class ResponseHangarWindowUpdated extends ServerPacket {

	public static ResponseHangarWindowUpdated getInstance() {
		return instance.newInstance();
	}

	private static final ResponseHangarWindowUpdated instance = new ResponseHangarWindowUpdated();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_HANGAR_WINDOW_UPDATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Vector;

import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Позиция дебага объекта.
 * 
 * @author Ronn
 */
public class ResponseDebugObjectPosition extends ServerPacket {

	public static final ResponseDebugObjectPosition getInstance(final SpaceObject object, final Vector position) {

		final ResponseDebugObjectPosition packet = instance.newInstance();

		packet.objectId = object.getObjectId();
		packet.objectClassId = object.getClassId();
		packet.position.set(position);

		return packet;
	}

	private static final ResponseDebugObjectPosition instance = new ResponseDebugObjectPosition();

	/** положение объекта */
	private final Vector position;

	/** уникальный ид объекта */
	private int objectId;
	/** класс объекта */
	private int objectClassId;

	public ResponseDebugObjectPosition() {
		this.position = Vector.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_DEBUG_OBJECT_POSITION;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, objectClassId);
		writeVector(buffer, position);
	}
}

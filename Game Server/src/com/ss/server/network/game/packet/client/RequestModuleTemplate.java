package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.table.ModuleTable;
import com.ss.server.template.ModuleTemplate;

/**
 * Клиентский пакет с запросом получения шаблона модуля.
 * 
 * @author Ronn
 */
public class RequestModuleTemplate extends ClientPacket {

	private static final ModuleTable MODULE_TABLE = ModuleTable.getInstance();

	/** ид шаблона */
	private int templateId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient owner = getOwner();

		if(owner == null) {
			return;
		}

		final ModuleTemplate template = MODULE_TABLE.getTemplate(templateId);

		if(template != null) {
			owner.sendPacket(template.getInfoPacket(local), true);
		}
	}

	@Override
	protected void readImpl() {
		templateId = readInt();
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с обновленным кол-вом опыта.
 * 
 * @author Ronn
 */
public class ResponseChangeExp extends ServerPacket {

	public static final ResponseChangeExp getInstance(final long exp, final LocalObjects local) {

		final ResponseChangeExp packet = local.create(instance);
		packet.exp = exp;

		return packet;
	}

	private static final ResponseChangeExp instance = new ResponseChangeExp();

	/** текущий опыт игрока */
	private long exp;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_CHANGE_EXP;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeLong(buffer, exp);
	}
}

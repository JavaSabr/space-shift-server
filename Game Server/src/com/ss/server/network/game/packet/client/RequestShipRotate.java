package com.ss.server.network.game.packet.client;

import rlib.geom.Rotation;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на переключение двигателя корабля.
 * 
 * @author Ronn
 */
public class RequestShipRotate extends ClientPacket {

	/** стартовый разворот */
	private final Rotation start;
	/** конечный разворот */
	private final Rotation end;

	/** модификатор скорости */
	private float modiff;

	public RequestShipRotate() {
		this.start = Rotation.newInstance();
		this.end = Rotation.newInstance();
	}

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip == null) {
			LOGGER.warning(this, new Exception("not found player ship"));
			return;
		}

		playerShip.getAI().startRotation(local, start, end, true, modiff);
	}

	@Override
	protected void readImpl() {
		start.setX(readFloat());
		start.setY(readFloat());
		start.setZ(readFloat());
		start.setW(readFloat());

		end.setX(readFloat());
		end.setY(readFloat());
		end.setZ(readFloat());
		end.setW(readFloat());

		readLong();

		modiff = readFloat();
	}
}

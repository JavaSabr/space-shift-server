package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с уведомлением об изменении уровня игрока.
 * 
 * @author Ronn
 */
public class ResponseChangeLevel extends ServerPacket {

	public static final ResponseChangeLevel getInstance(final SpaceObject object, final int level, final LocalObjects local) {

		final ResponseChangeLevel packet = local.create(instance);
		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();
		packet.level = level;

		return packet;
	}

	private static final ResponseChangeLevel instance = new ResponseChangeLevel();

	/** уникальный ид объекта */
	private int objectId;
	/** уникальный ид класса */
	private int classId;
	/** уровень объект */
	private int level;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_CHANGE_LEVEL;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, level);
	}
}

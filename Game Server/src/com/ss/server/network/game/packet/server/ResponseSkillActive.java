package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об изменении работы модуля.
 * 
 * @author Ronn
 */
public class ResponseSkillActive extends ServerPacket {

	public static ResponseSkillActive getInstance(final SpaceShip ship, final Module module, final int skillId, final int active, final LocalObjects local) {

		final ResponseSkillActive packet = local.create(instance);
		packet.shipObjectId = ship.getObjectId();
		packet.shipClassId = ship.getClassId();
		packet.moduleObjectId = module.getObjectId();
		packet.moduleClassId = module.getClassId();
		packet.skillId = skillId;
		packet.active = active;

		return packet;
	}

	public static final int ACTIVE = 1;

	public static final int DEACTIVE = 0;

	private static final ResponseSkillActive instance = new ResponseSkillActive();

	/** ид корабля */
	private int shipObjectId;
	/** класс ид корабля */
	private int shipClassId;
	/** ид модуля */
	private int moduleObjectId;
	/** класс ид модуля */
	private int moduleClassId;
	/** ид активируемого скила */
	private int skillId;
	/** активность модуля */
	private int active;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SKILL_ACTIVE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, shipObjectId);
		writeInt(buffer, shipClassId);
		writeInt(buffer, moduleObjectId);
		writeInt(buffer, moduleClassId);
		writeInt(buffer, moduleObjectId);
		writeInt(buffer, skillId);
		writeByte(buffer, active);
	}
}

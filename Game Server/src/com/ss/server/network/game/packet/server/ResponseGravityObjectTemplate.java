package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.GravityObjectTemplate;

/**
 * Серверный пакет с инфой о шаблоне грави объекта.
 * 
 * @author Ronn
 */
public class ResponseGravityObjectTemplate extends ResponseObjectTemplate<GravityObjectTemplate> {

	public static final ServerPacket getInstance(final GravityObjectTemplate template) {
		final ResponseGravityObjectTemplate packet = (ResponseGravityObjectTemplate) instance.newInstance();
		packet.setTemplate(template);
		return packet;
	}

	private static final ResponseGravityObjectTemplate instance = new ResponseGravityObjectTemplate();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_GRAVITY_OBJECT_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);
		final GravityObjectTemplate template = getTemplate();
		writeInt(buffer, template.getBrightness());
		writeInt(buffer, template.getRadius());
	}
}

package com.ss.server.network.game.packet.client;

import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.manager.AccountManager;
import com.ss.server.network.Network;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.RequestAuth;
import com.ss.server.network.login.model.LoginServer;
import com.ss.server.network.login.packet.client.RequestCheckAccount;

/**
 * Ответный пакет с информацией об аккаунте.
 * 
 * @author Ronn
 */
public class ResponseAuth extends ClientPacket {

	/** имя аккаунта */
	private String name;
	/** пароль к аккаунту */
	private String password;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final LoginServer loginServer = Network.getInstance().getLoginServer();

		if(loginServer == null) {
			client.sendPacket(RequestAuth.getInstance());
			return;
		}

		final IdFactory idFactory = IdFactory.getInstance();
		final AccountManager accountManager = AccountManager.getInstance();

		final int waitId = idFactory.getNextAuthId();
		client.setWaitId(waitId);

		accountManager.addWaitClient(waitId, client);
		loginServer.sendPacket(RequestCheckAccount.getInstance(name, password, waitId), true);
	}

	@Override
	public void finalyze() {
		name = null;
		password = null;
	}

	@Override
	protected void readImpl() {
		name = readString(readByte());
		password = readString(readByte());
	}
}

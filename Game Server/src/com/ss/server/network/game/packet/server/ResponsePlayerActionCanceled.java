package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об отмене запроса.
 * 
 * @author Ronn
 */
public class ResponsePlayerActionCanceled extends ServerPacket {

	public static ResponsePlayerActionCanceled getInstance() {
		return instance.newInstance();
	}

	private static final ResponsePlayerActionCanceled instance = new ResponsePlayerActionCanceled();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_ACTION_CANCELED;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

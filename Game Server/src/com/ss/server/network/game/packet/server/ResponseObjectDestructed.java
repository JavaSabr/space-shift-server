package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с уведомлением об уничтожении объекта.
 * 
 * @author Ronn
 */
public class ResponseObjectDestructed extends ServerPacket {

	public static ResponseObjectDestructed getInstance(final SpaceObject object, final LocalObjects local) {

		final ResponseObjectDestructed packet = local.create(instance);
		packet.objectId = object.getObjectId();
		packet.classId = object.getClassId();

		return packet;
	}

	private static final ResponseObjectDestructed instance = new ResponseObjectDestructed();

	/** уникальный ид объекта */
	private int objectId;
	/** ид класса объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_OBJECT_DESTRUCTED;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

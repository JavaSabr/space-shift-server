package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Обновление окна ангара.
 * 
 * @author Ronn
 */
public class ResponseUpdateHangarWindow extends ServerPacket {

	public static ResponseUpdateHangarWindow getInstance(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final HangarWindow window, final LocalObjects local) {

		final ResponseUpdateHangarWindow packet = instance.newInstance();

		final ByteBuffer buffer = packet.getPrepare();
		try {
			window.writeTo(packet, buffer, hangar, station, playerShip, local);
		} finally {
			buffer.flip();
		}

		return packet;
	}

	private static final ResponseUpdateHangarWindow instance = new ResponseUpdateHangarWindow();

	/** подготавливаемый буффер */
	private final ByteBuffer prepare;

	public ResponseUpdateHangarWindow() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_UPDATE_HANGAR_WINDOW;
	}

	/**
	 * @return подготавливаемый буффер.
	 */
	public ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

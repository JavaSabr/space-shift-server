package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.shot.Shot;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с указанием о завершении полета выстрела.
 * 
 * @author Ronn
 */
public class ResponseShotEnd extends ServerPacket {

	public static final ResponseShotEnd getInstance(final Shot shot, final boolean destruct, final LocalObjects local) {

		final ResponseShotEnd packet = local.create(instance);
		packet.objectId = shot.getObjectId();
		packet.destruct = destruct;

		return packet;
	}

	private static final ResponseShotEnd instance = new ResponseShotEnd();

	/** уникальный ид выстрела */
	private int objectId;

	/** самоуничтожение */
	private boolean destruct;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHOT_END;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeByte(buffer, destruct ? 1 : 0);
	}
}

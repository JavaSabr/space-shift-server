package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.table.GravityObjectTable;
import com.ss.server.template.GravityObjectTemplate;

/**
 * Клиентский пакет с запросом на получение шаблона грави объекта.
 * 
 * @author Ronn
 */
public class RequestGravityObjectTemplate extends ClientPacket {

	private static final GravityObjectTable GRAVITY_OBJECT_TABLE = GravityObjectTable.getInstance();

	/** ид шаблона */
	private int templateId;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final GravityObjectTemplate template = GRAVITY_OBJECT_TABLE.getTemplate(templateId);

		if(template != null) {
			client.sendPacket(template.getInfoPacket(local), true);
		}
	}

	@Override
	protected void readImpl() {
		templateId = readInt();
	}
}

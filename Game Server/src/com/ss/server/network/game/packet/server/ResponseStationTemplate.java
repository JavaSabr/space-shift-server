package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.StationTemplate;

/**
 * @author Ronn
 */
public class ResponseStationTemplate extends ResponseObjectTemplate<StationTemplate> {

	public static final ServerPacket getInstance(final StationTemplate template, final LocalObjects local) {

		final ResponseStationTemplate packet = local.create(instance);
		packet.setTemplate(template);

		return packet;
	}

	private static final ResponseStationTemplate instance = new ResponseStationTemplate();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SPACE_STATION_TEMPLATE;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);

		final StationTemplate template = getTemplate();
		final String name = template.getName();

		writeInt(buffer, name.length());
		writeString(buffer, name);
	}
}

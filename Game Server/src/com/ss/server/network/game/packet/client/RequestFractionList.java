package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Account;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseFractionList;

/**
 * Запрос списка кораблей на аккаунте.
 * 
 * @author Ronn
 */
public class RequestFractionList extends ClientPacket {

	private static final ResponseFractionList RESPONSE = ResponseFractionList.getInstance();

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final Account account = client.getAccount();

		if(account == null) {
			return;
		}

		client.sendPacket(RESPONSE);
	}
}

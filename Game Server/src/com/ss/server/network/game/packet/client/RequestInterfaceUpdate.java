package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на обновление положения элемента интерфейса.
 * 
 * @author Ronn
 */
public class RequestInterfaceUpdate extends ClientPacket {

	/** тип элемента */
	private int type;
	/** координата элемента */
	private int x;
	/** координата элемента */
	private int y;

	/** свернут ли элемент */
	private boolean minimized;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip != null) {
			playerShip.updateInterface(type, x, y, minimized);
		}
	}

	@Override
	protected void readImpl() {
		type = readByte();
		x = readInt();
		y = readInt();
		minimized = readByte() != 0;
	}
}

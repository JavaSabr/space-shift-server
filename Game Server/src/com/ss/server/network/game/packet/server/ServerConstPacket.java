package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.util.Util;

import com.ss.server.network.game.ServerPacket;

/**
 * Модель константного серверного пакета.
 * 
 * @author Ronn
 */
public abstract class ServerConstPacket extends ServerPacket {

	@Override
	public final void complete() {
	}

	@Override
	public final void decreaseSends(final int count) {
	}

	@Override
	public final void increaseSends(final int count) {
	}

	@Override
	public void write(final ByteBuffer buffer) {
		try {
			writeImpl(buffer);
		} catch(final Exception e) {
			LOGGER.warning(this, e);
			LOGGER.warning(this, "Buffer " + buffer + "\n" + Util.hexdump(buffer.array(), buffer.position()));
		}
	}
}

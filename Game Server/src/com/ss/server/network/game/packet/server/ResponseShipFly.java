package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление об характеристиках полета корабля.
 * 
 * @author Ronn
 */
public class ResponseShipFly extends ServerPacket {

	public static ResponseShipFly getInstance(final SpaceShip ship, final float accel, final float speed, final float maxSpeed, final float maxCurrentSpeed, final boolean active, final boolean force,
			final LocalObjects local) {

		final ResponseShipFly packet = local.create(instance);
		packet.objectId = ship.getObjectId();
		packet.classId = ship.getClassId();
		packet.accel = accel;
		packet.speed = speed;
		packet.maxSpeed = maxSpeed;
		packet.maxCurrentSpeed = maxCurrentSpeed;
		packet.active = active;
		packet.force = force;
		packet.location.set(ship.getLocation());

		return packet;
	}

	private static final ResponseShipFly instance = new ResponseShipFly();

	/** прошлая позиция корабля */
	private final Vector location;

	/** ид объекта */
	private int objectId;
	/** под ид объекта */
	private int classId;

	/** ускорение */
	private float accel;
	/** екущаяя скорость */
	private float speed;
	/** максимальная скорость */
	private float maxSpeed;
	/** максимально текущая скорость */
	private float maxCurrentSpeed;

	/** активен ли двигатель */
	private boolean active;
	/** принудительная ли синхронизация */
	private boolean force;

	public ResponseShipFly() {
		this.location = Vector.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SHIP_FLY;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeFloat(buffer, accel);
		writeFloat(buffer, speed);
		writeFloat(buffer, maxSpeed);
		writeFloat(buffer, maxCurrentSpeed);
		writeVector(buffer, location);
		writeByte(buffer, active ? 1 : 0);
		writeByte(buffer, force ? 1 : 0);
	}
}

package com.ss.server.network.game.packet.server;

import java.awt.Color;
import java.nio.ByteBuffer;

import com.ss.server.model.location.BackgroundInfo;
import com.ss.server.model.location.LightInfo;
import com.ss.server.model.location.LocationInfo;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Сервернвый пакет с инфой об локации.
 * 
 * @author Ronn
 */
public class ResponseLocationInfo extends ServerPacket {

	public static final ServerPacket getInstance(final LocationInfo info) {

		final ResponseLocationInfo packet = (ResponseLocationInfo) instance.newInstance();
		packet.info = info;

		return packet;
	}

	private static final ResponseLocationInfo instance = new ResponseLocationInfo();

	/** записываемая инфа о локации */
	private LocationInfo info;

	@Override
	public void finalyze() {
		info = null;
	}

	private final LocationInfo getInfo() {
		return info;
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_LOCATION_INFO;
	}

	private final void writeBackground(final ByteBuffer buffer, final BackgroundInfo info) {

		final String[] keys = info.getKeys();

		writeByte(buffer, keys.length);

		for(int i = 0, length = keys.length; i < length; i++) {

			final String key = keys[i];

			writeByte(buffer, i);
			writeInt(buffer, key.length());
			writeString(buffer, key);
		}
	}

	private void writeColor(final ByteBuffer buffer, final Color color) {
		writeFloat(buffer, color.getRed() / 255F);
		writeFloat(buffer, color.getGreen() / 255F);
		writeFloat(buffer, color.getBlue() / 255F);
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final LocationInfo info = getInfo();

		writeInt(buffer, info.getId());
		writeBackground(buffer, info.getBackgroundInfo());
		writeVector(buffer, info.getVectorStar());

		writeColor(buffer, info.getStarLightColor());
		writeColor(buffer, info.getShadowLightColor());
		writeColor(buffer, info.getAmbientColor());

		writeLights(buffer, info.getLights());
	}

	private final void writeLights(final ByteBuffer buffer, final LightInfo[] lights) {

		writeByte(buffer, lights.length);

		for(final LightInfo light : lights) {

			writeByte(buffer, light.getLightType().ordinal());
			writeInt(buffer, light.getRadius());
			writeColor(buffer, light.getColor());
			writeVector(buffer, light.getPosition());
			writeByte(buffer, light.isMain() ? 1 : 0);
		}
	}
}

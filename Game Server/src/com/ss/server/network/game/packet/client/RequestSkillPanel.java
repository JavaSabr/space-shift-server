package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseSkillPanel;

/**
 * Пакет запроса на обновление положения скила на панели.
 * 
 * @author Ronn
 */
public class RequestSkillPanel extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip != null) {
			playerShip.sendPacket(ResponseSkillPanel.getInstance(playerShip), true);
		}
	}
}

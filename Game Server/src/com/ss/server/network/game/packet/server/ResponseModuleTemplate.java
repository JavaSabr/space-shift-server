package com.ss.server.network.game.packet.server;

import java.awt.Color;
import java.nio.ByteBuffer;

import rlib.util.StringUtils;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.info.BlasterRayInfo;
import com.ss.server.model.module.info.EngineFireInfo;
import com.ss.server.model.module.info.ForceShieldInfo;
import com.ss.server.model.module.info.RocketShotInfo;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;
import com.ss.server.template.ModuleTemplate;
import com.ss.server.template.SkillTemplate;
import com.ss.server.template.description.TemplateDescription;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;

/**
 * Серверный пакет с инфой о шаблоне модуля.
 * 
 * @author Ronn
 */
public class ResponseModuleTemplate extends ResponseObjectTemplate<ModuleTemplate> {

	public static final ServerPacket getInstance(final ModuleTemplate template, final LocalObjects local) {

		final ResponseModuleTemplate packet = local.create(instance);
		packet.setTemplate(template);

		return packet;
	}

	private static final ResponseModuleTemplate instance = new ResponseModuleTemplate();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_MODULE_TEMPLAE;
	}

	/**
	 * Запись описания бластера в пакет.
	 */
	private void writeBlasterRayInfo(final ByteBuffer buffer, final BlasterRayInfo info) {

		String string = info.getModel();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		string = info.getMaterial();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		writeVector(buffer, info.getScale());
		writeVector(buffer, info.getOffset());

		final GraphicEffectTemplate explosion = info.getExplosion();
		explosion.writeMe(buffer, this);
	}

	/**
	 * Запись в пакет описание пламени двигателя.
	 */
	private void writeEngineInfo(final ByteBuffer buffer, final EngineFireInfo info) {

		String string = info.getFireModel();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		string = info.getFireMaterial();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		writeVector(buffer, info.getFireOffset());
		writeVector(buffer, info.getFireScale());

		writeRotation(buffer, info.getFireRotation());

		string = info.getTrailMaterial();

		writeInt(buffer, string.length());
		writeString(buffer, string);

		if(StringUtils.isEmpty(string)) {
			return;
		}

		final Color color = info.getTrailColor();

		writeInt(buffer, color.getRed());
		writeInt(buffer, color.getGreen());
		writeInt(buffer, color.getBlue());

		writeVector(buffer, info.getTrailOffset());
		writeFloat(buffer, info.getTrailStartSize());
		writeFloat(buffer, info.getTrailEndSize());
		writeFloat(buffer, info.getTrailSegmentLength());
		writeFloat(buffer, info.getTrailSergmentLife());
	}

	/**
	 * Запись информации о силовом щите в пакет.
	 */
	private void writeForceShieldInfo(final ByteBuffer buffer, final ForceShieldInfo info) {

		String value = info.getModelKey();

		writeByte(buffer, value.length());
		writeString(buffer, value);

		value = info.getHitTexture();

		writeByte(buffer, value.length());
		writeString(buffer, value);
		writeFloat(buffer, info.getHitEffectSize());

		Color color = info.getHitEffectColor();

		writeInt(buffer, color.getRed());
		writeInt(buffer, color.getGreen());
		writeInt(buffer, color.getBlue());

		color = info.getElectricColor();

		writeInt(buffer, color.getRed());
		writeInt(buffer, color.getGreen());
		writeInt(buffer, color.getBlue());

		writeFloat(buffer, info.getElectricFallOf());
		writeFloat(buffer, info.getElectricFog());
		writeFloat(buffer, info.getElectricLines());
		writeFloat(buffer, info.getElectricNoseAmount());
		writeFloat(buffer, info.getElectricSpeed());
		writeFloat(buffer, info.getElectricWidth());

		writeVector(buffer, info.getHitScale());
		writeVector(buffer, info.getElectricScale());
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		super.writeImpl(buffer);

		final ModuleTemplate template = getTemplate();

		final String icon = template.getPropIcon();

		writeByte(buffer, icon.length());
		writeString(buffer, icon);

		final String name = template.getName();

		writeByte(buffer, name.length());
		writeString(buffer, name);

		// запись скилов модуля
		{
			final SkillTemplate[] skills = template.getSkills();

			writeInt(buffer, skills.length);

			if(skills.length > 0) {
				for(final SkillTemplate skill : skills) {
					writeInt(buffer, skill.getId());
				}
			}
		}

		// запись струй пламени
		{
			final EngineFireInfo[] engineFireInfos = template.getEngineFireInfos();

			writeInt(buffer, engineFireInfos.length);

			if(engineFireInfos.length > 0) {
				for(final EngineFireInfo info : engineFireInfos) {
					writeEngineInfo(buffer, info);
				}
			}
		}

		// запись лучей бластеров
		{
			final BlasterRayInfo[] blasterRayInfos = template.getBlasterRayInfos();

			writeInt(buffer, blasterRayInfos.length);

			if(blasterRayInfos.length > 0) {
				for(final BlasterRayInfo info : blasterRayInfos) {
					writeBlasterRayInfo(buffer, info);
				}
			}
		}

		// запись описания силового щита
		{
			final ForceShieldInfo info = template.getForceShieldInfo();

			writeByte(buffer, info != null ? 1 : 0);

			if(info != null) {
				writeForceShieldInfo(buffer, info);
			}
		}

		// запись описания ракеты
		{
			final RocketShotInfo info = template.getRocketShotInfo();

			writeByte(buffer, info == null ? 0 : 1);

			if(info != null) {
				writeRocketShotInfo(buffer, info);
			}
		}

		final TemplateDescription description = template.getDescription();
		description.writeTo(this, buffer);
	}

	/**
	 * Запись описания ракеты в пакет.
	 */
	private void writeRocketShotInfo(final ByteBuffer buffer, final RocketShotInfo info) {

		final String string = info.getModel();

		writeByte(buffer, string.length());
		writeString(buffer, string);

		writeVector(buffer, info.getScale());
		writeVector(buffer, info.getOffset());

		final GraphicEffectTemplate explosion = info.getExplosion();
		explosion.writeMe(buffer, this);

		writeEngineInfo(buffer, info.getEngineFireInfo());
	}
}

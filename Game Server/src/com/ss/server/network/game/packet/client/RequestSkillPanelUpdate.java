package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;

/**
 * Пакет запроса на обновление положения скила на панели.
 * 
 * @author Ronn
 */
public class RequestSkillPanelUpdate extends ClientPacket {

	/** уникальный ид модуля */
	private int objectId;
	/** ид скила */
	private int skillId;
	/** новое положение */
	private int oldOrder;
	/** предыдущее положение */
	private int newOrder;

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip playerShip = client.getOwner();

		if(playerShip != null) {
			playerShip.updateSkillPanel(objectId, skillId, oldOrder, newOrder);
		}
	}

	@Override
	protected void readImpl() {
		objectId = readInt();
		skillId = readInt();
		oldOrder = readInt();
		newOrder = readInt();
	}
}

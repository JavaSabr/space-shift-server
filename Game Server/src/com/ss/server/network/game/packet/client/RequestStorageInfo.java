package com.ss.server.network.game.packet.client;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.storage.Storage;
import com.ss.server.network.game.ClientPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponseStorageInfo;

/**
 * Запрос на получение информации о содержании хранилища.
 * 
 * @author Ronn
 */
public class RequestStorageInfo extends ClientPacket {

	@Override
	protected void executeImpl(final LocalObjects local, final long currentTime) {

		final GameClient client = getOwner();

		if(client == null) {
			return;
		}

		final PlayerShip ship = client.getOwner();

		if(ship == null) {
			return;
		}

		final Storage storage = ship.getStorage();
		ship.sendPacket(ResponseStorageInfo.getInstance(storage), true);
	}

	@Override
	public boolean isSynchronized() {
		return true;
	}
}

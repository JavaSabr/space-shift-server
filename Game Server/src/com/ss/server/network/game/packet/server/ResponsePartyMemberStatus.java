package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Env;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о текущем статусе члена группы.
 * 
 * @author Ronn
 */
public class ResponsePartyMemberStatus extends ServerPacket {

	public static ResponsePartyMemberStatus getInstance(final PlayerShip playerShip, final LocalObjects local) {

		final Env env = local.getNextEnv();

		final ResponsePartyMemberStatus packet = local.create(instance);
		packet.objectId = playerShip.getObjectId();
		packet.classId = playerShip.getClassId();
		packet.currentStrength = playerShip.getCurrentStrength();
		packet.maxStrength = playerShip.getMaxStrength(env);
		packet.forceShieldStrength = playerShip.getCurrentShield();
		packet.maxForceShieldStregth = playerShip.getMaxShield();
		packet.level = playerShip.getLevel();

		return packet;
	}

	private static final ResponsePartyMemberStatus instance = new ResponsePartyMemberStatus();

	/** уникальный ид члена группы */
	private int objectId;
	/** ид класса члена группы */
	private int classId;

	/** текущее значение прочности */
	private int currentStrength;
	/** максимальное кол-во прочности */
	private int maxStrength;
	/** текущая общая прочность щита */
	private int forceShieldStrength;
	/** максимальная общая прочность щита */
	private int maxForceShieldStregth;

	/** текущий уровень игрока */
	private int level;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PARTY_MEMBER_STATUS;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
		writeInt(buffer, currentStrength);
		writeInt(buffer, maxStrength);
		writeInt(buffer, forceShieldStrength);
		writeInt(buffer, maxForceShieldStregth);
		writeInt(buffer, level);
	}
}

package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.skills.Skill;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с информацией о откате скила.
 * 
 * @author Ronn
 */
public class ResponseSkillReloadInfo extends ServerPacket {

	public static ResponseSkillReloadInfo getInstance(final Skill skill, final long startReuseTime, final int reuseTime, final LocalObjects local) {

		final ResponseSkillReloadInfo packet = local.create(instance);
		packet.objectId = skill.getModuleObjectId();
		packet.reuseId = skill.getReloadId();
		packet.startReuseTime = startReuseTime;
		packet.reuseTime = reuseTime;

		return packet;
	}

	private static final ResponseSkillReloadInfo instance = new ResponseSkillReloadInfo();

	/** дата старта отката */
	private long startReuseTime;
	/** уникальный ид скила */
	private int objectId;
	/** ид откат умения */
	private int reuseId;
	/** время отката */
	private int reuseTime;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SKILL_REUSE_INFO;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, objectId);
		writeInt(buffer, reuseId);
		writeInt(buffer, reuseTime);
		writeLong(buffer, startReuseTime);
	}
}

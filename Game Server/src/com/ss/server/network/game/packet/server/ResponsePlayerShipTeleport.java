package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Пакет с уведомлением о телепортации корабля игрока.
 * 
 * @author Ronn
 */
public class ResponsePlayerShipTeleport extends ServerPacket {

	public static ResponsePlayerShipTeleport getInstance(final PlayerShip playerShip) {

		final ResponsePlayerShipTeleport packet = instance.newInstance();
		packet.position.set(playerShip.getLocation());
		packet.rotation.set(playerShip.getRotation());
		packet.locationId = playerShip.getLocationId();

		return packet;
	}

	private static final ResponsePlayerShipTeleport instance = new ResponsePlayerShipTeleport();

	/** новая позиция корабля */
	private final Vector position;
	/** новый разворот корабля */
	private final Rotation rotation;

	/** ид локации */
	private int locationId;

	public ResponsePlayerShipTeleport() {
		this.position = Vector.newInstance();
		this.rotation = Rotation.newInstance();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_SHIP_TELEPORT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeFloat(buffer, position.getX());
		writeFloat(buffer, position.getY());
		writeFloat(buffer, position.getZ());
		writeFloat(buffer, rotation.getX());
		writeFloat(buffer, rotation.getY());
		writeFloat(buffer, rotation.getZ());
		writeFloat(buffer, rotation.getW());
		writeInt(buffer, locationId);
	}
}

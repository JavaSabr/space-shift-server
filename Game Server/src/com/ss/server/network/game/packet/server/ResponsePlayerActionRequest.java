package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerAction;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Уведомление о запросе от игрока на действие.
 * 
 * @author Ronn
 */
public class ResponsePlayerActionRequest extends ServerPacket {

	public static ResponsePlayerActionRequest getInstance(final PlayerShip playerShip, final PlayerAction actionType) {

		final ResponsePlayerActionRequest packet = (ResponsePlayerActionRequest) instance.newInstance();

		packet.actionType = actionType;
		packet.objectId = playerShip.getObjectId();
		packet.classId = playerShip.getClassId();

		return packet;
	}

	private static final ResponsePlayerActionRequest instance = new ResponsePlayerActionRequest();

	/** тип действия */
	private PlayerAction actionType;

	/** уникальный ид объекта */
	private int objectId;
	/** класс ид объекта */
	private int classId;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_PLAYER_ACTION_REQUEST;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, actionType.ordinal());
		writeInt(buffer, objectId);
		writeInt(buffer, classId);
	}
}

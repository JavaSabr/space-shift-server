package com.ss.server.network.game.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.storage.Cell;
import com.ss.server.model.storage.Storage;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.ServerPacketType;

/**
 * Серверный пакет с информацией о содержании хранилищи корабля игрока.
 * 
 * @author Ronn
 */
public class ResponseStorageInfo extends ServerPacket {

	public static final ResponseStorageInfo getInstance(final Storage storage) {

		final ResponseStorageInfo packet = instance.newInstance();

		final ByteBuffer buffer = packet.getPrepare();

		packet.writeInt(buffer, storage.getSize());

		storage.lock();
		try {

			for(final Cell cell : storage.getCells()) {

				final SpaceItem item = cell.getItem();

				packet.writeByte(buffer, item == null ? 0 : 1);

				if(item == null) {
					continue;
				}

				packet.writeInt(buffer, item.getObjectId());
				packet.writeInt(buffer, item.getClassId());
				packet.writeInt(buffer, item.getTemplateId());
				packet.writeLong(buffer, item.getItemCount());
			}

		} finally {
			storage.unlock();
		}

		buffer.flip();

		return packet;
	}

	public static final ResponseStorageInfo instance = new ResponseStorageInfo();

	/** промежуточный буфер пакета */
	private final ByteBuffer prepare;

	public ResponseStorageInfo() {
		this.prepare = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_STORAGE_INFO;
	}

	/**
	 * @return промежуточный буфер пакета.
	 */
	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

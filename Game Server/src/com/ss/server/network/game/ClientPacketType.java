package com.ss.server.network.game;

import java.util.HashSet;
import java.util.Set;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.network.game.packet.client.RequestCreateShip;
import com.ss.server.network.game.packet.client.RequestCurrentEnergyBalance;
import com.ss.server.network.game.packet.client.RequestDeleteShip;
import com.ss.server.network.game.packet.client.RequestEnergyBalance;
import com.ss.server.network.game.packet.client.RequestExit;
import com.ss.server.network.game.packet.client.RequestForceSyncObject;
import com.ss.server.network.game.packet.client.RequestFractionList;
import com.ss.server.network.game.packet.client.RequestGravityObjectTemplate;
import com.ss.server.network.game.packet.client.RequestInterface;
import com.ss.server.network.game.packet.client.RequestInterfaceUpdate;
import com.ss.server.network.game.packet.client.RequestItemPanel;
import com.ss.server.network.game.packet.client.RequestItemPanelUpdate;
import com.ss.server.network.game.packet.client.RequestItemShopTransaction;
import com.ss.server.network.game.packet.client.RequestItemTemplate;
import com.ss.server.network.game.packet.client.RequestLocationInfo;
import com.ss.server.network.game.packet.client.RequestLocationObjectTemplate;
import com.ss.server.network.game.packet.client.RequestModuleTemplate;
import com.ss.server.network.game.packet.client.RequestMoveStorageItem;
import com.ss.server.network.game.packet.client.RequestObjectFriendStatus;
import com.ss.server.network.game.packet.client.RequestPartyDisband;
import com.ss.server.network.game.packet.client.RequestPartyExcludeMember;
import com.ss.server.network.game.packet.client.RequestPartyLeave;
import com.ss.server.network.game.packet.client.RequestPartySetOwner;
import com.ss.server.network.game.packet.client.RequestPlayerAction;
import com.ss.server.network.game.packet.client.RequestPlayerActionAccept;
import com.ss.server.network.game.packet.client.RequestPlayerActionCancel;
import com.ss.server.network.game.packet.client.RequestPlayerActionReject;
import com.ss.server.network.game.packet.client.RequestPlayerShipCommand;
import com.ss.server.network.game.packet.client.RequestQuestButtonSelect;
import com.ss.server.network.game.packet.client.RequestQuestFavorite;
import com.ss.server.network.game.packet.client.RequestQuestList;
import com.ss.server.network.game.packet.client.RequestSayMessage;
import com.ss.server.network.game.packet.client.RequestSelectShip;
import com.ss.server.network.game.packet.client.RequestSelectTarget;
import com.ss.server.network.game.packet.client.RequestShipList;
import com.ss.server.network.game.packet.client.RequestShipRotate;
import com.ss.server.network.game.packet.client.RequestShipStatus;
import com.ss.server.network.game.packet.client.RequestShipTemplate;
import com.ss.server.network.game.packet.client.RequestSkillList;
import com.ss.server.network.game.packet.client.RequestSkillPanel;
import com.ss.server.network.game.packet.client.RequestSkillPanelUpdate;
import com.ss.server.network.game.packet.client.RequestSkillReuses;
import com.ss.server.network.game.packet.client.RequestSkillTemplate;
import com.ss.server.network.game.packet.client.RequestSpawn;
import com.ss.server.network.game.packet.client.RequestStationQuestList;
import com.ss.server.network.game.packet.client.RequestStationTemplate;
import com.ss.server.network.game.packet.client.RequestStorageInfo;
import com.ss.server.network.game.packet.client.RequestTeleportItem;
import com.ss.server.network.game.packet.client.RequestTeleportToStation;
import com.ss.server.network.game.packet.client.RequestUpdateHangarWindow;
import com.ss.server.network.game.packet.client.RequestUseSkill;
import com.ss.server.network.game.packet.client.ResponseAuth;

/**
 * Перечисление всех клиентских пакетов.
 * 
 * @author Ronn
 */
public enum ClientPacketType {
	RESPONSE_AUTH(0x3000, new ResponseAuth()),
	REQUEST_SHIP_LIST(0x3001, new RequestShipList()),
	REQUEST_CREATE_SHIP(0x3002, new RequestCreateShip()),
	REQUEST_DELETE_SHIP(0x3003, new RequestDeleteShip()),
	REQUEST_SELECT_SHIP(0x3004, new RequestSelectShip()),
	REQUEST_SPAWN(0x3005, new RequestSpawn()),
	REQUEST_SKILL_PANEL_UPDATE(0x3006, new RequestSkillPanelUpdate()),
	REQUEST_SHIP_ROTATE(0x3007, new RequestShipRotate()),
	REQUEST_SAY_MESSAGE(0x3008, new RequestSayMessage()),
	REQUEST_INTERFACE_UPDATE(0x3009, new RequestInterfaceUpdate()),
	REQUEST_INTERFACE(0x3010, new RequestInterface()),
	REQUEST_SKILL_LIST(0x3011, new RequestSkillList()),
	REQUEST_USE_SKILL(0x3012, new RequestUseSkill()),
	REQUEST_SKILL_PANEL(0x3013, new RequestSkillPanel()),
	REQUEST_OPEN_HANGAR_WINDOW(0x3014, new RequestTeleportToStation()),
	REQUEST_QUEST_BUTTON_SELECT(0x3015, new RequestQuestButtonSelect()),
	REQUEST_SHIP_TEMPLATE(0x3017, new RequestShipTemplate()),
	REQUEST_MODULE_TEMPLATE(0x3018, new RequestModuleTemplate()),
	REQUEST_GRAVITY_OBJECT_TEMPLATE(0x3019, new RequestGravityObjectTemplate()),
	REQUEST_LOCATION_INFO(0x3020, new RequestLocationInfo()),
	REQUEST_STATION_TEMPLATE(0x3021, new RequestStationTemplate()),
	REQUEST_SKILL_TEMPLATE(0x3022, new RequestSkillTemplate()),
	REQUEST_QUEST_LIST(0x3023, new RequestQuestList()),
	REQUEST_ITEM_TEMPLATE(0x3024, new RequestItemTemplate()),
	REQUEST_TELEPORT_ITEM(0x3025, new RequestTeleportItem()),
	REQUEST_STORAGE_INFO(0x3026, new RequestStorageInfo()),
	REQUEST_MOVE_STORAGE_ITEM(0x3027, new RequestMoveStorageItem()),
	REQUEST_FORCE_SYNC_OBJECT(0x3028, new RequestForceSyncObject()),
	REQUEST_SHIP_STATUS(0x3029, new RequestShipStatus()),
	REQUEST_ENERGY_BALANCE(0x3030, new RequestEnergyBalance()),
	REQUEST_CURRENT_ENERGY_BALANCE(0x3031, new RequestCurrentEnergyBalance()),
	REQUEST_OBJECT_FRIEND_STATUS(0x3032, new RequestObjectFriendStatus()),
	REQUEST_SELECT_TARGET(0x3033, new RequestSelectTarget()),
	REQUEST_LOCATION_OBJECT_TEMPLATE(0x3034, new RequestLocationObjectTemplate()),
	REQUEST_ITEM_PANEL(0x3035, new RequestItemPanel()),
	REQUEST_ITEM_PANEL_UPDATE(0x3036, new RequestItemPanelUpdate()),
	REQUEST_PLAYER_ACTION(0x3037, new RequestPlayerAction()),
	REQUEST_PLAYER_ACTION_CANCEL(0x3038, new RequestPlayerActionCancel()),
	REQUEST_PLAYER_ACTION_REJECT(0x3039, new RequestPlayerActionReject()),
	REQUEST_PLAYER_ACTION_ACCEPT(0x3040, new RequestPlayerActionAccept()),
	REQUEST_PARTY_DISBAND(0x3041, new RequestPartyDisband()),
	REQUEST_PARTY_LEAVE(0x3042, new RequestPartyLeave()),
	REQUEST_PARTY_EXCLUDE_MEMBER(0x3043, new RequestPartyExcludeMember()),
	REQUEST_PARTY_SET_OWNER(0x3044, new RequestPartySetOwner()),
	REQUEST_QUEST_FAVORITE(0x3045, new RequestQuestFavorite()),
	REQUEST_UPDATE_HANGAR_WINDOW(0x3046, new RequestUpdateHangarWindow()),
	REQUEST_ITEM_SHOP_TRANSATION(0x3047, new RequestItemShopTransaction()),
	REQUEST_FRACTION_LIST(0x3048, new RequestFractionList()),
	REQUEST_SKILL_REUSES(0x3049, new RequestSkillReuses()),
	REQUEST_EXIT(0x3050, new RequestExit()),
	REQUEST_TELEPORT_TO_STATION(0x3051, new RequestTeleportToStation()),
	REQUEST_STATION_QUEST_LIST(0x3052, new RequestStationQuestList()),
	REQUEST_PLAYER_SHIP_COMMAND(0x3053, new RequestPlayerShipCommand()), ;

	/**
	 * Возвращает тип пакета указанного опкода.
	 * 
	 * @param opcode опкод пакета.
	 * @return тип клиентского пакета.
	 */
	public static ClientPacketType getPacketType(final int opcode) {
		return types[opcode];
	}

	/**
	 * Инициализация клиентских пакетов.
	 */
	public static void init() {

		final Set<Integer> set = new HashSet<Integer>();

		for(final ClientPacketType packet : values()) {

			final int index = packet.getOpcode();

			if(set.contains(index)) {
				LOGGER.warning("found duplicate opcode " + index + " or " + Integer.toHexString(packet.getOpcode()) + "!");
			}

			set.add(index);
		}

		set.clear();

		types = new ClientPacketType[Short.MAX_VALUE * 2];

		for(final ClientPacketType type : values()) {
			types[type.getOpcode()] = type;
		}

		LOGGER.info("client packets prepared.");
	}

	private static final Logger LOGGER = LoggerManager.getLogger(ClientPacketType.class);

	/** массив типов пакетов */
	private static ClientPacketType[] types;

	/** экземпляр пакета */
	private final ClientPacket packet;

	/** опкод пакета */
	private final int opcode;

	private ClientPacketType(final int opcode, final ClientPacket packet) {
		this.opcode = opcode;
		this.packet = packet;
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * @return класс реализации пакета этого типа.
	 */
	public Class<? extends ClientPacket> getPacketClass() {
		return packet.getClass();
	}
}

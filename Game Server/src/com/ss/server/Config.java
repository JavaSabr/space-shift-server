package com.ss.server;

import java.io.File;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.FileUtils;
import rlib.util.PathBuilder;
import rlib.util.StringUtils;
import rlib.util.Util;
import rlib.util.VarTable;

import com.jolbox.bonecp.BoneCPConfig;
import com.ss.server.document.DocumentConfig;

/**
 * Набор параметров работы сервера.
 * 
 * @author Ronn
 */
public final class Config {

	/**
	 * Инициализация конфига
	 */
	public static void init(final String[] args) {
		initFolders();

		final File directory = new File(FOLDER_CONFIG_PATH);

		if(!directory.exists() || !directory.canRead()) {
			LOGGER.warning(directory + " not available, the configuration is not loaded!");
			System.exit(0);
		}

		final VarTable vars = VarTable.newInstance();
		final File[] files = FileUtils.getFiles(directory, ".xml");

		for(final File file : files) {
			vars.set(new DocumentConfig(file).parse());
		}

		ACCOUNT_MIN_ACCESS_LEVEL = vars.getInteger("ACCOUNT_MIN_ACCESS_LEVEL");

		SERVER_NAME = vars.getString("SERVER_NAME");
		SERVER_TYPE = vars.getString("SERVER_TYPE");
		SERVER_NAME_TEMPLATE = vars.getString("SERVER_NAME_TEMPLATE");
		SERVER_ACCEPT_FILTER = vars.getString("SERVER_ACCEPT_FILTER");
		SERVER_ONLINE_FILE = vars.getString("SERVER_ONLINE_FILE", StringUtils.EMPTY);
		SERVER_GAME_HOST = vars.getString("SERVER_GAME_HOST");
		SERVER_LOGIN_HOST = vars.getString("SERVER_LOGIN_HOST");

		SERVER_PLAYER_SHIP_CLASS_ID = vars.getInteger("SERVER_PLAYER_SHIP_CLASS_ID");
		SERVER_MODULE_CLASS_ID = vars.getInteger("SERVER_MODULE_CLASS_ID");
		SERVER_STATION_CLASS_ID = vars.getInteger("SERVER_STATION_CLASS_ID");
		SERVER_GRAVITY_OBJECT_CLASS_ID = vars.getInteger("SERVER_GRAVITY_OBJECT_CLASS_ID");
		SERVER_ITEM_CLASS_ID = vars.getInteger("SERVER_ITEM_CLASS_ID");
		SERVER_NPS_CLASS_ID = vars.getInteger("SERVER_NPS_CLASS_ID");
		SERVER_LOCATION_OBJECT_CLASS_ID = vars.getInteger("SERVER_LOCATION_OBJECT_CLASS_ID");
		SERVER_GAME_PORT = vars.getInteger("SERVER_GAME_PORT");
		SERVER_LOGIN_PORT = vars.getInteger("SERVER_LOGIN_PORT");

		SERVER_RATE_EXP = vars.getFloat("SERVER_RATE_EXP");
		SERVER_PARTY_RATE_EXP = vars.getFloat("SERVER_PARTY_RATE_EXP");
		SERVER_RATE_MONEY = vars.getFloat("SERVER_RATE_MONEY");
		SERVER_RATE_DROP_ITEM = vars.getFloat("SERVER_RATE_DROP_ITEM");

		SERVER_USE_FILE_CACHE = vars.getBoolean("SERVER_USE_FILE_CACHE");
		SERVER_ONLY_PVP = vars.getBoolean("SERVER_ONLY_PVP", false);

		WORLD_LIFE_TIME_DROP_ITEM = vars.getInteger("WORLD_LIFE_TIME_DROP_ITEM");
		WORLD_BLOCK_TIME_DROP_ITEM = vars.getInteger("WORLD_BLOCK_TIME_DROP_ITEM");
		WORLD_MIN_ACCESS_LEVEL = vars.getInteger("WORLD_MIN_ACCESS_LEVEL");
		WORLD_MAX_DIFF_LEVEL_ON_DROP = vars.getInteger("WORLD_MAX_DIFF_LEVEL_ON_DROP");
		WORLD_WIDTH_REGION = vars.getInteger("WORLD_WIDTH_REGION");
		WORLD_HEIGHT_REGION = vars.getInteger("WORLD_HEIGHT_REGION");
		WORLD_MAXIMUM_ONLINE = vars.getInteger("WORLD_MAXIMUM_ONLINE");
		WORLD_UPDATE_OBJECT_INTERVAL = vars.getInteger("WORLD_UPDATE_OBJECT_INTERVAL");
		WORLD_DEFAULT_VISIBLE_RANGE = vars.getInteger("WORLD_DEFAULT_VISIBLE_RANGE");

		WORLD_PLAYER_MAX_LEVEL = vars.getByte("WORLD_PLAYER_MAX_LEVEL");

		DATA_BASE_DRIVER = vars.getString("DATA_BASE_DRIVER");
		DATA_BASE_URL = vars.getString("DATA_BASE_URL");
		DATA_BASE_LOGIN = vars.getString("DATA_BASE_LOGIN");
		DATA_BASE_PASSWORD = vars.getString("DATA_BASE_PASSWORD");

		DATA_BASE_MAX_CONNECTIONS = vars.getInteger("DATA_BASE_MAX_CONNECTIONS");
		DATA_BASE_MAX_STATEMENTS = vars.getInteger("DATA_BASE_MAX_STATEMENTS");
		DATA_BASE_CLEANING_START = vars.getBoolean("DATA_BASE_CLEANING_START");
		DATA_BASE_USE_CACHE = vars.getBoolean("DATA_BASE_USE_CACHE");

		THREAD_EXECUTOR_GENERAL_SIZE = vars.getInteger("THREAD_EXECUTOR_GENERAL_SIZE");
		THREAD_EXECUTOR_SECTOR_SIZE = vars.getInteger("THREAD_EXECUTOR_SECTOR_SIZE");
		THREAD_EXECUTOR_LOCATION_SIZE = vars.getInteger("THREAD_EXECUTOR_LOCATION_SIZE");
		THREAD_EXECUTOR_PACKET_SIZE = vars.getInteger("THREAD_EXECUTOR_PACKET_SIZE");
		THREAD_EXECUTOR_UPDATE_OBJECT_SIZE = vars.getInteger("THREAD_EXECUTOR_UPDATE_OBJECT_SIZE");

		DEVELOPER_DEBUG_CLIENT_PACKETS = vars.getBoolean("DEVELOPER_DEBUG_CLIENT_PACKETS");
		DEVELOPER_DEBUG_SERVER_PACKETS = vars.getBoolean("DEVELOPER_DEBUG_SERVER_PACKETS");
		DEVELOPER_MAIN_DEBUG = vars.getBoolean("DEVELOPER_MAIN_DEBUG");
		DEVELOPER_DEBUG_LS_GS = vars.getBoolean("DEVELOPER_DEBUG_LS_GS");
		DEVELOPER_DEBUG_SYNCHRONIZE = vars.getBoolean("DEVELOPER_DEBUG_SYNCHRONIZE");

		SELECTOR_READ_BUFFER_SIZE = vars.getInteger("SELECTOR_READ_BUFFER_SIZE");
		SELECTOR_WRITE_BUFFER_SIZE = vars.getInteger("SELECTOR_WRITE_BUFFER_SIZE");
		SELECTOR_DIRECT_WRITE_BUFFER_SIZE = vars.getInteger("SELECTOR_DIRECT_WRITE_BUFFER_SIZE");
		SELECTOR_MAX_SEND_PER_PASS = vars.getInteger("SELECTOR_MAX_SEND_PER_PASS");
		SELECTOR_HELPER_BUFFER_COUNT = vars.getInteger("SELECTOR_HELPER_BUFFER_COUNT");
		SELECTOR_SLEEP_TIME = vars.getInteger("SELECTOR_SLEEP_TIME");
		SELECTOR_MAXIMUM_PACKET_CUT = vars.getInteger("SELECTOR_MAXIMUM_PACKET_CUT");

		NETWORK_GROUP_NAME = vars.getString("NETWORK_GROUP_NAME");

		NETWORK_MAXIMUM_PACKET_CUT = vars.getInteger("NETWORK_MAXIMUM_PACKET_CUT");
		NETWORK_GROUP_SIZE = vars.getInteger("NETWORK_GROUP_SIZE");
		NETWORK_THREAD_PRIORITY = vars.getInteger("NETWORK_THREAD_PRIORITY");
		NETWORK_READ_BUFFER_SIZE = vars.getInteger("NETWORK_READ_BUFFER_SIZE");
		NETWORK_WRITE_BUFFER_SIZE = vars.getInteger("NETWORK_WRITE_BUFFER_SIZE");

		DATA_BASE_CONFIG.setJdbcUrl(DATA_BASE_URL);
		DATA_BASE_CONFIG.setUsername(DATA_BASE_LOGIN);
		DATA_BASE_CONFIG.setPassword(DATA_BASE_PASSWORD);
		DATA_BASE_CONFIG.setAcquireRetryAttempts(0);
		DATA_BASE_CONFIG.setAcquireIncrement(5);
		DATA_BASE_CONFIG.setReleaseHelperThreads(0);
		DATA_BASE_CONFIG.setMinConnectionsPerPartition(2);
		DATA_BASE_CONFIG.setMaxConnectionsPerPartition(DATA_BASE_MAX_CONNECTIONS);
		DATA_BASE_CONFIG.setStatementsCacheSize(DATA_BASE_MAX_STATEMENTS);

		// создаем доп. проперти драйвера мускула
		final Properties properties = new Properties();

		Util.addUTFToSQLConnectionProperties(properties);

		// применяем проперти
		DATA_BASE_CONFIG.setDriverProperties(properties);

		Set<String> arguments = new HashSet<String>();

		for(String arg : args) {
			arguments.add(arg);
		}

		if(!SERVER_ONLY_PVP) {
			SERVER_ONLY_PVP = arguments.contains("-only-pvp");
		}
	}

	/**
	 * Инициализация путей к основным папкам сервера.
	 */
	private static void initFolders() {

		FOLDER_PROJECT_PATH = Util.getRootFolderFromClass(Config.class).getAbsolutePath();

		PathBuilder builder = new PathBuilder(FOLDER_PROJECT_PATH);
		builder.append(SCRIPTS_FOLDER_NAME);

		FOLDER_SCRIPTS_PATH = builder.getPath();

		builder = new PathBuilder(FOLDER_PROJECT_PATH);
		builder.append(DATA_FOLDER_NAME);

		FOLDER_DATA_PATH = builder.getPath();

		builder = new PathBuilder(FOLDER_PROJECT_PATH);
		builder.append(CONFIG_FOLDER_NAME);

		FOLDER_CONFIG_PATH = builder.getPath();
	}

	private static final Logger LOGGER = LoggerManager.getLogger(Config.class);

	public static final String CONFIG_FOLDER_NAME = "config";
	public static final String DATA_FOLDER_NAME = "data";

	public static final String SCRIPTS_FOLDER_NAME = "scripts";

	/** пути к основным папкам сервера */
	public static String FOLDER_PROJECT_PATH;
	public static String FOLDER_CONFIG_PATH;

	/**
	 * Настройки аккаунтов
	 */

	public static String FOLDER_SCRIPTS_PATH;

	/**
	 * Настройки сервера
	 */

	public static String FOLDER_DATA_PATH;

	/** минимальный уровень прав для входа на сервер */
	public static int ACCOUNT_MIN_ACCESS_LEVEL;

	/** название сервера */
	public static String SERVER_NAME;

	/** тип сервера */
	public static String SERVER_TYPE;
	/** допустимые ники на сервере */
	public static String SERVER_NAME_TEMPLATE;
	/** адресс гейм сервера */
	public static String SERVER_GAME_HOST;
	/** адресс логин сервера */
	public static String SERVER_LOGIN_HOST;
	/** тип фильтра соединений */
	public static String SERVER_ACCEPT_FILTER;
	/** адресс вывода в фаил онлайна сервера */
	public static String SERVER_ONLINE_FILE;
	/** масимальный доступный онлаин насервере */
	public static int SERVER_MAXIMUM_ONLAIN;

	/** класс ид корабля игрока */
	public static int SERVER_PLAYER_SHIP_CLASS_ID;
	/** класс ид модуля корабля */
	public static int SERVER_MODULE_CLASS_ID;
	/** класс ид космической станции */
	public static int SERVER_STATION_CLASS_ID;
	/** класс ид гравитационных объектов */
	public static int SERVER_GRAVITY_OBJECT_CLASS_ID;
	/** класс ид предметов */
	public static int SERVER_ITEM_CLASS_ID;
	/** класс ид NPS */
	public static int SERVER_NPS_CLASS_ID;
	/** класс ид для локационных объектов */
	public static int SERVER_LOCATION_OBJECT_CLASS_ID;
	/** порт сервера для бинда гейма */
	public static int SERVER_GAME_PORT;
	/** порт логин сервера */
	public static int SERVER_LOGIN_PORT;
	/** множитель получаемого опыта */
	public static float SERVER_RATE_EXP;

	/** множитель получаемого опыта в группе */
	public static float SERVER_PARTY_RATE_EXP;
	/** множитель дропнутых денег */
	public static float SERVER_RATE_MONEY;
	/** режим сервера чисто для боев */
	public static boolean SERVER_ONLY_PVP;

	/** множитель выпадающего дропа */
	public static float SERVER_RATE_DROP_ITEM;

	/**
	 * Настройки игрового мира
	 */

	/** использовать ли кэшеирование читаемых файлов сервером */
	public static boolean SERVER_USE_FILE_CACHE;

	/** время жизни выпавшего итема */
	public static int WORLD_LIFE_TIME_DROP_ITEM;

	/** время блокировки выпавшего итема на поднятие чужаком */
	public static int WORLD_BLOCK_TIME_DROP_ITEM;
	/** минимальный уровень доступа для входа в мир */
	public static int WORLD_MIN_ACCESS_LEVEL;
	/** максимальная разница уровня игрока-моб для выпадения дропа */
	public static int WORLD_MAX_DIFF_LEVEL_ON_DROP;
	/** ширина региона в мире */
	public static int WORLD_WIDTH_REGION;
	/** высота региона в мире */
	public static int WORLD_HEIGHT_REGION;
	/** максимальный онлайн */
	public static int WORLD_MAXIMUM_ONLINE;
	/** частота обновления объектов в мире */
	public static int WORLD_UPDATE_OBJECT_INTERVAL;
	/** стандартная дистанция видимости обычных объектов */
	public static int WORLD_DEFAULT_VISIBLE_RANGE;

	/**
	 * Настройки базы данных
	 */

	/** максимально доступный уровень игрока */
	public static byte WORLD_PLAYER_MAX_LEVEL;

	/** экземпляр конфига для БД */
	public static final BoneCPConfig DATA_BASE_CONFIG = new BoneCPConfig();

	/** класс драйвера базы данных */
	public static String DATA_BASE_DRIVER;

	/** адресс базы данных */
	public static String DATA_BASE_URL;
	/** логин для доступа к базе данных */
	public static String DATA_BASE_LOGIN;
	/** пароль для доступа к базе данных */
	public static String DATA_BASE_PASSWORD;
	/** максимальное кол-во коннектов к базе в пуле */
	public static int DATA_BASE_MAX_CONNECTIONS;

	/** максимальное кол-во создаваемых statements */
	public static int DATA_BASE_MAX_STATEMENTS;
	/** чистить ли БД при старте сервера */
	public static boolean DATA_BASE_CLEANING_START;

	/**
	 * Настройки потоков
	 */

	/** использование дополнительного кэша коннектов */
	public static boolean DATA_BASE_USE_CACHE;
	/** кол-во потоков для исполнения основных задач */
	public static int THREAD_EXECUTOR_GENERAL_SIZE;

	/** кол-во потоков для исполнения секторных задач */
	public static int THREAD_EXECUTOR_SECTOR_SIZE;
	/** кол-во потоков для исполнения локационных задач */
	public static int THREAD_EXECUTOR_LOCATION_SIZE;
	/** кол-во потоков для исполнения пакетных задач */
	public static int THREAD_EXECUTOR_PACKET_SIZE;

	/**
	 * Настройки для разработки
	 */

	/** кол-во потоков для исполнения обновления объектов */
	public static int THREAD_EXECUTOR_UPDATE_OBJECT_SIZE;
	/** вывод дебаг инфы о пересылке клиент пакетов */
	public static boolean DEVELOPER_DEBUG_CLIENT_PACKETS;

	/** вывод дебаг инфы о пересылке серверных пакетов */
	public static boolean DEVELOPER_DEBUG_SERVER_PACKETS;
	/** вывод общей дебаг инфы */
	public static boolean DEVELOPER_MAIN_DEBUG;
	/** вывод дебага связанного с логин сервером */
	public static boolean DEVELOPER_DEBUG_LS_GS;

	/**
	 * Настройки селектора
	 */

	/** отправлять ли пакеты с дебагом синхронизации */
	public static boolean DEVELOPER_DEBUG_SYNCHRONIZE;
	/** размер читаемого буфера */
	public static int SELECTOR_READ_BUFFER_SIZE;

	/** размер записываемого буфера */
	public static int SELECTOR_WRITE_BUFFER_SIZE;
	/** размер прямого записываемого буффера */
	public static int SELECTOR_DIRECT_WRITE_BUFFER_SIZE;
	/** максимальное кол-во отправляемых пакетов за проход */
	public static int SELECTOR_MAX_SEND_PER_PASS;
	/** максимальное кол-во буфферов в пуле */
	public static int SELECTOR_HELPER_BUFFER_COUNT;
	/** интервал в цикле селектора */
	public static int SELECTOR_SLEEP_TIME;

	/**
	 * Настройки сети
	 */

	/** максимальное число разрезаемых пакетов */
	public static int SELECTOR_MAXIMUM_PACKET_CUT;
	/** название группы сетевых потоков */
	public static String NETWORK_GROUP_NAME;

	/** максимальное число разрезаемых пакетов */
	public static int NETWORK_MAXIMUM_PACKET_CUT;

	/** размер группы сетевых потоков */
	public static int NETWORK_GROUP_SIZE;
	/** приоритет сетевых потоков */
	public static int NETWORK_THREAD_PRIORITY;
	/** размер буфера для чтения пакетов */
	public static int NETWORK_READ_BUFFER_SIZE;
	/** размер буфера для записи пакетов */
	public static int NETWORK_WRITE_BUFFER_SIZE;

	private Config() {
		throw new RuntimeException();
	}
}

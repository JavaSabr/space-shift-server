package com.ss.server.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import rlib.database.ConnectFactories;
import rlib.database.ConnectFactory;
import rlib.database.DBUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;

import com.ss.server.Config;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.storage.ItemLocation;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Менеджер по работе с предметами в БД.
 * 
 * @author Ronn
 */
public class ItemDBManager {

	private static final Logger LOGGER = LoggerManager.getLogger(ItemDBManager.class);

	/** добавление записи о предмете в БД */
	private static final String INSERT_ITEM = "INSERT INTO `items` (`object_id`, `location`, `template_id`, `item_count`, `create_date`, `creator`) VALUES(?,?,?,?,?,?)";
	/** обновление записи о предмете в БД */
	private static final String UPDATE_ITEM = "UPDATE `items` SET `owner_id` = ?, `location` = ?, `item_count` = ?, `index` = ? WHERE `object_id` = ? LIMIT 1";
	/** обновление записи о положении предмета в БД */
	private static final String UPDATE_ITEM_LOCATION = "UPDATE `items` SET `owner_id` = ?, `location` = ?, `index` = ? WHERE `object_id` = ? LIMIT 1";

	/** запрос на загрузку предметов в хранилище корабля игрока */
	private static final String SELECT_ITEMS_IN_STORAGE = "SELECT `object_id`, `template_id`, `item_count`, `index`, `create_date`, `creator` FROM `items` WHERE `owner_id` = ? AND `location` = ?";

	private static ItemDBManager instance;

	public static ItemDBManager getInstance() {

		if(instance == null) {
			instance = new ItemDBManager();
		}

		return instance;
	}

	/** фабрика подключений к БД */
	private final ConnectFactory connectFactory;

	private ItemDBManager() {
		InitializeManager.valid(getClass());
		this.connectFactory = ConnectFactories.newBoneCPConnectFactory(Config.DATA_BASE_CONFIG, Config.DATA_BASE_DRIVER);
	}

	/**
	 * Создать запись в БД о новом предмете.
	 * 
	 * @param item новый предмет.
	 */
	public void addNewItem(final SpaceItem item) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_ITEM);
			statement.setInt(1, item.getObjectId());
			statement.setInt(2, item.getItemLocation().ordinal());
			statement.setInt(3, item.getTemplateId());
			statement.setLong(4, item.getItemCount());
			statement.setLong(5, item.getCreateDate());
			statement.setString(6, item.getCreator());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Загрузка всех предметов находящихся в хранилище корабля игрока.
	 * 
	 * @param objectId уникальный ид корабля.
	 * @param container контейнер для предметов.
	 */
	public void loadStorageItems(final int objectId, final Array<SpaceItem> container) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {
			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_ITEMS_IN_STORAGE);
			statement.setInt(1, objectId);
			statement.setInt(2, ItemLocation.IN_SHIP_STORAGE.ordinal());

			rset = statement.executeQuery();

			final ItemTable itemTable = ItemTable.getInstance();

			while(rset.next()) {

				final int templateId = rset.getInt(2);

				final ItemTemplate template = itemTable.getTemplate(templateId);

				if(template == null) {
					LOGGER.warning("not found item for template id " + templateId);
					continue;
				}

				final SpaceItem item = template.takeInstance(rset.getInt(1));
				item.setIndex(rset.getInt(4));
				item.setItemCount(rset.getLong(3));
				item.setCreateDate(rset.getLong(5));
				item.setCreator(rset.getString(6));

				container.add(item);
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление в БД записи о предмете.
	 * 
	 * @param item обновляемый предмет.
	 */
	public void updateItem(final SpaceItem item) {

		PreparedStatement statement = null;
		Connection con = null;

		try {
			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_ITEM);
			statement.setInt(1, item.getOwnerId());
			statement.setInt(2, item.getItemLocation().ordinal());
			statement.setLong(3, item.getItemCount());
			statement.setInt(4, item.getIndex());
			statement.setInt(5, item.getObjectId());
			statement.execute();
		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Обновление в БД записи о положении предмета.
	 * 
	 * @param item обновляемый предмет.
	 */
	public void updateItemLocation(final SpaceItem item) {

		PreparedStatement statement = null;
		Connection con = null;

		try {
			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_ITEM_LOCATION);
			statement.setInt(1, item.getOwnerId());
			statement.setInt(2, item.getItemLocation().ordinal());
			statement.setInt(3, item.getIndex());
			statement.setInt(4, item.getObjectId());
			statement.execute();
		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}
}

package com.ss.server.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import rlib.database.ConnectFactories;
import rlib.database.ConnectFactory;
import rlib.database.DBUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.ref.Reference;
import rlib.util.table.Table;

import com.ss.server.Config;
import com.ss.server.manager.QuestManager;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestDate;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.QuestStatus;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Менеджер по работе с квестовыми данными в БД.
 * 
 * @author Ronn
 */
public class QuestDBManager implements TableNames {

	private static final Logger LOGGER = LoggerManager.getLogger(QuestDBManager.class);

	/** добавление записи о квесте в БД */
	private static final String INSERT_QUEST = "INSERT INTO `" + TABLE_QUESTS + "` (`" + TABLE_QUESTS_C_OBJECT_ID + "`, `" + TABLE_QUESTS_C_QUEST_ID + "`, `" + TABLE_QUESTS_C_STATE + "`, `"
			+ TABLE_QUESTS_C_FINISH_DATE + "`) VALUES(?,?,?,?)";

	/** обновление записи о квесте в БД */
	private static final String UPDATE_QUEST = "UPDATE `" + TABLE_QUESTS + "` SET `" + TABLE_QUESTS_C_STATE + "` = ?, `" + TABLE_QUESTS_C_FAVORITE + "` = ? WHERE `" + TABLE_QUESTS_C_OBJECT_ID
			+ "` = ? AND `" + TABLE_QUESTS_C_QUEST_ID + "` = ? LIMIT 1";

	/** обновление записи о задании на завершнное */
	private static final String FINISH_QUEST = "UPDATE `" + TABLE_QUESTS + "` SET `" + TABLE_QUESTS_C_STATE + "` = ?, `" + TABLE_QUESTS_C_FAVORITE + "` = ?, `" + TABLE_QUESTS_C_FINISH_DATE
			+ "` = ?, `" + TABLE_QUESTS_C_FINISH_STATUS + "` = ? WHERE `" + TABLE_QUESTS_C_OBJECT_ID + "` = ? AND `" + TABLE_QUESTS_C_QUEST_ID + "` = ? LIMIT 1";

	/** запрос на получение всех записей об квестах игрока */
	private static final String SELECT_QUESTS = "SELECT `" + TABLE_QUESTS_C_QUEST_ID + "`, `" + TABLE_QUESTS_C_STATE + "`, `" + TABLE_QUESTS_C_FINISH_DATE + "`, `" + TABLE_QUESTS_C_FINISH_STATUS
			+ "`, `" + TABLE_QUESTS_C_FAVORITE + "` FROM `" + TABLE_QUESTS + "` WHERE `" + TABLE_QUESTS_C_OBJECT_ID + "` = ?";

	/** добавление записи о квесте в БД */
	private static final String INSERT_QUEST_VAR = "INSERT INTO `" + TABLE_QUEST_VARS + "` (`" + TABLE_QUEST_VARS_C_OBJECT_ID + "`, `" + TABLE_QUEST_VARS_C_QUEST_ID + "`, `"
			+ TABLE_QUEST_VARS_C_VAR_NAME + "`, `" + TABLE_QUEST_VARS_C_VAR_VALUE + "`) VALUES(?,?,?,?)";

	/** запрос на получение всех записей об квестовых переменных */
	private static final String SELECT_QUEST_VAR = "SELECT `" + TABLE_QUEST_VARS_C_VAR_NAME + "`, `" + TABLE_QUEST_VARS_C_VAR_VALUE + "` FROM `" + TABLE_QUEST_VARS + "` WHERE `"
			+ TABLE_QUEST_VARS_C_OBJECT_ID + "` = ? AND `" + TABLE_QUEST_VARS_C_QUEST_ID + "` = ?";

	/** добавление записи о квесте в БД */
	private static final String UPDATE_QUEST_VAR = "UPDATE `" + TABLE_QUEST_VARS + "` SET `" + TABLE_QUEST_VARS_C_VAR_VALUE + "` = ? WHERE `" + TABLE_QUEST_VARS_C_OBJECT_ID + "` = ? AND `"
			+ TABLE_QUEST_VARS_C_QUEST_ID + "` = ? AND `" + TABLE_QUEST_VARS_C_VAR_NAME + "` = ? LIMIT 1";

	/** добавление записи о квесте в БД */
	private static final String CLEAR_QUEST_VAR = "DELETE FROM `" + TABLE_QUEST_VARS + "` WHERE `" + TABLE_QUEST_VARS_C_OBJECT_ID + "` = ? AND `" + TABLE_QUEST_VARS_C_QUEST_ID + "` = ?";

	private static QuestDBManager instance;

	public static QuestDBManager getInstance() {

		if(instance == null) {
			instance = new QuestDBManager();
		}

		return instance;
	}

	/** фабрика подключений к БД */
	private final ConnectFactory connectFactory;

	private QuestDBManager() {
		InitializeManager.valid(getClass());
		this.connectFactory = ConnectFactories.newBoneCPConnectFactory(Config.DATA_BASE_CONFIG, Config.DATA_BASE_DRIVER);
	}

	/**
	 * Создать запись в БД о взятом квесте игроком.
	 * 
	 * @param ship корабль игрока.
	 * @param state сохраняемый квест.
	 */
	public void addNewQuest(final PlayerShip ship, final QuestState state) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_QUEST);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, state.getQuestId());
			statement.setInt(3, state.getState());
			statement.setInt(4, 0);
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создать запись в БД переменной квеста.
	 * 
	 * @param ship корабль игрока.
	 * @param quest какого квеста переменная.
	 * @param name название переменной.
	 * @param value значение переменной.
	 */
	public void addNewQuestVar(final PlayerShip ship, final Quest quest, final String name, final int value) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_QUEST_VAR);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, quest.getId());
			statement.setString(3, name);
			statement.setInt(4, value);
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Запись в БД зафинишированного задания.
	 * 
	 * @param ship корабль, который выполнил задание.
	 * @param date дата выполненого задания.
	 */
	public void finishQuest(final PlayerShip ship, final QuestDate date) {

		final QuestStatus status = date.getStatus();
		final Quest quest = date.getQuest();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(FINISH_QUEST);
			statement.setInt(1, 0);
			statement.setInt(2, 0);
			statement.setLong(3, date.getDate());
			statement.setInt(4, status.ordinal());
			statement.setInt(5, ship.getObjectId());
			statement.setInt(6, quest.getId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * @return фабрика подключений к БД.
	 */
	public ConnectFactory getConnectFactory() {
		return connectFactory;
	}

	/**
	 * Загрузка квестов корабля.
	 */
	public void loadQuests(final int objectId, final QuestList questList) {

		final QuestManager questManager = QuestManager.getInstance();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_QUESTS);
			statement.setInt(1, objectId);

			rset = statement.executeQuery();

			questList.synLock();
			try {

				while(rset.next()) {

					final Quest quest = questManager.getQuest(rset.getInt(1));

					if(quest == null) {
						continue;
					}

					final long finishDate = rset.getLong(3);
					final QuestStatus finishStatus = QuestStatus.valueOf(rset.getInt(4));

					if(finishDate != 0) {
						questList.addFinishQuest(quest, finishStatus, finishDate);
					} else {
						questList.addActiveQuest(quest, rset.getInt(2), rset.getInt(5) == 1);
					}
				}

			} finally {
				questList.synUnlock();
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Загрузка квестов корабля.
	 */
	public void loadQuestVars(final PlayerShip ship, final Quest quest, final QuestState state, final Table<String, Reference> variables) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_QUEST_VAR);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, quest.getId());

			rset = statement.executeQuery();

			synchronized(variables) {
				while(rset.next()) {

					final String name = rset.getString(1);
					final int value = rset.getInt(2);

					if(quest == null) {
						continue;
					}

					variables.put(name, state.newVariable(value));
				}
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление в БД записи о состоянии квеста.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об положении скила.
	 */
	public void updateQuest(final PlayerShip ship, final QuestState state) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_QUEST);
			statement.setInt(1, state.getState());
			statement.setInt(2, state.isFavorite() ? 1 : 0);
			statement.setInt(3, ship.getObjectId());
			statement.setInt(4, state.getQuestId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Обновление в БД записи квестовой переменной.
	 * 
	 * @param ship корабль игрока.
	 * @param quest какого квеста переменная.
	 * @param name название переменной.
	 * @param value новое значение.
	 */
	public void updateQuestVar(final PlayerShip ship, final Quest quest, final String name, final int value) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_QUEST_VAR);
			statement.setInt(1, value);
			statement.setInt(2, ship.getObjectId());
			statement.setInt(3, quest.getId());
			statement.setString(4, name);
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}
}

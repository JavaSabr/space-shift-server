package com.ss.server.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import rlib.database.ConnectFactories;
import rlib.database.ConnectFactory;
import rlib.database.DBUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.server.Config;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.rating.event.local.module.shield.ShieldModuleRatingEvent;
import com.ss.server.model.rating.event.local.module.weapon.WeaponModuleRatingEvent;
import com.ss.server.model.rating.local.LocalRatingElement;
import com.ss.server.model.rating.local.LocalRatingElementType;
import com.ss.server.model.rating.local.LocalRatingElementVarType;
import com.ss.server.model.rating.local.destroy.DestroyRatingElement;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;
import com.ss.server.model.rating.local.kill.KillRatingElement;
import com.ss.server.model.rating.local.kill.KillRatingElementType;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElement;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElementType;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElement;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElementType;
import com.ss.server.model.ship.SpaceShip;

/**
 * Менеджер по работе с рейтингом в БД.
 * 
 * @author Ronn
 */
public class RatingDBManager implements TableNames {

	private static final Logger LOGGER = LoggerManager.getLogger(RatingDBManager.class);

	/** запрос на загрузку рейтинга использования оружия */
	private static final String SELECT_WEAPON_RATING = "SELECT `" + TABLE_WEAPON_C_WEAPON_TYPE + "`, `" + TABLE_WEAPON_C_SHOT_COUNT + "`, `" + TABLE_WEAPON_C_HIT_COUNT + "`, `"
			+ TABLE_WEAPON_C_DEPLETION_COUNT + "` FROM `" + TABLE_WEAPON_RATING + "` WHERE `" + TABLE_WEAPON_C_OBJECT_ID + "` = ? LIMIT " + WeaponModuleRatingElementType.SIZE;

	/** запрос на обновление рейтинга использования оружия */
	private static final String UPDATE_WEAPON_RATING = "UPDATE `" + TABLE_WEAPON_RATING + "` SET `" + TABLE_WEAPON_C_SHOT_COUNT + "` = ?, `" + TABLE_WEAPON_C_HIT_COUNT + "` = ?, `"
			+ TABLE_WEAPON_C_DEPLETION_COUNT + "` = ? WHERE `" + TABLE_WEAPON_C_OBJECT_ID + "` = ? AND `" + TABLE_WEAPON_C_WEAPON_TYPE + "` = ? LIMIT 1";

	/** запрос на вставку записи о рейтинге оружия */
	private static final String INSERT_WEAPON_RATING = "INSERT INTO `" + TABLE_WEAPON_RATING + "` (" + TABLE_WEAPON_C_OBJECT_ID + ", " + TABLE_WEAPON_C_WEAPON_TYPE + ", " + TABLE_WEAPON_C_SHOT_COUNT
			+ ", " + TABLE_WEAPON_C_HIT_COUNT + ", " + TABLE_WEAPON_C_DEPLETION_COUNT + ") VALUES(?,?,?,?,?)";

	/** запрос на загрузку рейтинга использования щита */
	private static final String SELECT_SHIELD_RATING = "SELECT `" + TABLE_SHIELD_C_SHIELD_TYPE + "`, `" + TABLE_SHIELD_C_HIT_COUNT + "`, `" + TABLE_SHIELD_C_RELOAD_COUNT + "` FROM `"
			+ TABLE_SHIELD_RATING + "` WHERE `" + TABLE_SHIELD_C_OBJECT_ID + "` = ? LIMIT " + ShieldModuleRatingElementType.SIZE;

	/** запрос на обновление рейтинга использования щита */
	private static final String UPDATE_SHIELD_RATING = "UPDATE `" + TABLE_SHIELD_RATING + "` SET `" + TABLE_SHIELD_C_HIT_COUNT + "` = ?, `" + TABLE_SHIELD_C_RELOAD_COUNT + "` = ? WHERE `"
			+ TABLE_SHIELD_C_OBJECT_ID + "` = ? AND `" + TABLE_SHIELD_C_SHIELD_TYPE + "` = ? LIMIT 1";

	/** запрос на вставку записи о рейтинге щита */
	private static final String INSERT_SHIELD_RATING = "INSERT INTO `" + TABLE_SHIELD_RATING + "` (" + TABLE_SHIELD_C_OBJECT_ID + ", " + TABLE_SHIELD_C_SHIELD_TYPE + ", " + TABLE_SHIELD_C_HIT_COUNT
			+ ", " + TABLE_SHIELD_C_RELOAD_COUNT + ") VALUES(?,?,?,?)";

	/** запрос на загрузку локального рейтинга */
	private static final String SELECT_LOCAL_RATING = "SELECT `" + TABLE_LOCAL_C_ELEMENT_TYPE + "`, `" + TABLE_LOCAL_C_HIT_COUNT + "`, `" + TABLE_LOCAL_C_REPAIR_COUNT + "` FROM `"
			+ TABLE_LOCAL_RATING + "` WHERE `" + TABLE_LOCAL_C_OBJECT_ID + "` = ? LIMIT " + LocalRatingElementType.SIZE;

	/** запрос на обновление локального рейтинга */
	private static final String UPDATE_LOCAL_RATING = "UPDATE `" + TABLE_LOCAL_RATING + "` SET `" + TABLE_LOCAL_C_HIT_COUNT + "` = ?, `" + TABLE_LOCAL_C_REPAIR_COUNT + "` = ? WHERE `"
			+ TABLE_LOCAL_C_OBJECT_ID + "` = ? AND `" + TABLE_LOCAL_C_ELEMENT_TYPE + "` = ? LIMIT 1";

	/** запрос на вставку записи о локальном рейтинге */
	private static final String INSERT_LOCAL_RATING = "INSERT INTO `" + TABLE_LOCAL_RATING + "` (" + TABLE_LOCAL_C_OBJECT_ID + ", " + TABLE_LOCAL_C_ELEMENT_TYPE + ", " + TABLE_LOCAL_C_HIT_COUNT
			+ ", " + TABLE_LOCAL_C_REPAIR_COUNT + ") VALUES(?,?,?,?)";

	/** запрос на загрузку рейтинга уничтожений */
	private static final String SELECT_DESTROY_RATING = "SELECT `" + TABLE_DESTROY_C_DESTROY_TYPE + "`, `" + TABLE_DESTROY_C_DESTROY_COUNT + "` FROM `" + TABLE_DESTROY_RATING + "` WHERE `"
			+ TABLE_DESTROY_C_OBJECT_ID + "` = ? LIMIT " + DestroyRatingElementType.SIZE;

	/** запрос на обновление рейтинга уничтожений */
	private static final String UPDATE_DESTROY_RATING = "UPDATE `" + TABLE_DESTROY_RATING + "` SET `" + TABLE_DESTROY_C_DESTROY_COUNT + "` = ? WHERE `" + TABLE_DESTROY_C_OBJECT_ID + "` = ? AND `"
			+ TABLE_DESTROY_C_DESTROY_TYPE + "` = ? LIMIT 1";

	/** запрос на вставку записи о рейтинге уничтожений */
	private static final String INSERT_DESTROY_RATING = "INSERT INTO `" + TABLE_DESTROY_RATING + "` (" + TABLE_DESTROY_C_OBJECT_ID + ", " + TABLE_DESTROY_C_DESTROY_TYPE + ", "
			+ TABLE_DESTROY_C_DESTROY_COUNT + ") VALUES(?,?,?)";

	/** запрос на загрузку рейтинга убийств */
	private static final String SELECT_KILL_RATING = "SELECT `" + TABLE_KILL_C_KILL_TYPE + "`, `" + TABLE_KILL_C_ROCKET_LAUNCHER_COUNT + "`, `" + TABLE_KILL_C_LASER_COUNT + "`, `"
			+ TABLE_KILL_C_NEUTRON_COUNT + "`, `" + TABLE_KILL_C_PHOTON_COUNT + "`, `" + TABLE_KILL_C_PLASMA_COUNT + "`, `" + TABLE_KILL_C_PULSE_COUNT + "`, `" + TABLE_KILL_C_TACHYON_COUNT
			+ "` FROM `" + TABLE_KILL_RATING + "` WHERE `" + TABLE_KILL_C_OBJECT_ID + "` = ? LIMIT " + KillRatingElementType.SIZE;

	/** запрос на обновление рейтинга убийств */
	private static final String UPDATE_KILL_RATING = "UPDATE `" + TABLE_KILL_RATING + "` SET `" + TABLE_KILL_C_LASER_COUNT + "` = ?, `" + TABLE_KILL_C_NEUTRON_COUNT + "` = ?, `"
			+ TABLE_KILL_C_PHOTON_COUNT + "` = ?, `" + TABLE_KILL_C_PLASMA_COUNT + "` = ?, `" + TABLE_KILL_C_PULSE_COUNT + "` = ?, `" + TABLE_KILL_C_ROCKET_LAUNCHER_COUNT + "` = ?, `"
			+ TABLE_KILL_C_TACHYON_COUNT + "` = ? WHERE `" + TABLE_KILL_C_OBJECT_ID + "` = ? AND `" + TABLE_KILL_C_KILL_TYPE + "` = ? LIMIT 1";

	/** запрос на вставку записи о рейтинге убийств */
	private static final String INSERT_KILL_RATING = "INSERT INTO `" + TABLE_KILL_RATING + "` (" + TABLE_KILL_C_OBJECT_ID + ", " + TABLE_KILL_C_KILL_TYPE + ", " + TABLE_KILL_C_LASER_COUNT + ", "
			+ TABLE_KILL_C_NEUTRON_COUNT + ", " + TABLE_KILL_C_PHOTON_COUNT + ", " + TABLE_KILL_C_PLASMA_COUNT + ", " + TABLE_KILL_C_PULSE_COUNT + ", " + TABLE_KILL_C_ROCKET_LAUNCHER_COUNT + ", "
			+ TABLE_KILL_C_TACHYON_COUNT + ") VALUES(?,?,?,?,?,?,?,?,?)";

	private static RatingDBManager instance;

	public static RatingDBManager getInstance() {

		if(instance == null) {
			instance = new RatingDBManager();
		}

		return instance;
	}

	/** фабрика подключений к БД */
	private final ConnectFactory connectFactory;

	private RatingDBManager() {
		InitializeManager.valid(getClass());
		this.connectFactory = ConnectFactories.newBoneCPConnectFactory(Config.DATA_BASE_CONFIG, Config.DATA_BASE_DRIVER);
	}

	/**
	 * Загрузка рейтинга оружия корабля.
	 * 
	 * @param ship корабль, для которого надо загрузить рейтинг.
	 */
	public void loadWeaponRatings(final SpaceShip ship) {

		final RatingSystem ratingSystem = ship.getRatingSystem();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_WEAPON_RATING);
			statement.setInt(1, ship.getObjectId());

			rset = statement.executeQuery();

			while(rset.next()) {

				final WeaponModuleRatingElementType type = WeaponModuleRatingElementType.valueOf(rset.getInt(1));

				final WeaponModuleRatingElement<WeaponModuleRatingEvent> element = ratingSystem.findElement(type);
				element.setShotCount(rset.getLong(2));
				element.setHitCount(rset.getLong(3));
				element.setDeplectionCount(rset.getLong(4));
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление рейтинга использования оружия.
	 * 
	 * @param ship корабль, у которого изменен рейтинг.
	 * @param element измененный элемент рейтинга.
	 */
	public void updateWeaponRating(SpaceShip ship, WeaponModuleRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_WEAPON_RATING);
			statement.setLong(1, element.getShotCount());
			statement.setLong(2, element.getHitCount());
			statement.setLong(3, element.getDepletionCount());
			statement.setInt(4, ship.getObjectId());
			statement.setInt(5, elementType.getIndex());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о новом рейтинге оружия.
	 * 
	 * @param ship кораблт для которого создается.
	 * @param element элемент для которого создается запись рейтинга.
	 */
	public void addNewWeaponRating(SpaceShip ship, WeaponModuleRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_WEAPON_RATING);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, elementType.getIndex());
			statement.setLong(3, element.getShotCount());
			statement.setLong(4, element.getHitCount());
			statement.setLong(5, element.getDepletionCount());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Загрузка рейтинга щита корабля.
	 * 
	 * @param ship корабль, для которого надо загрузить рейтинг.
	 */
	public void loadShieldRatings(final SpaceShip ship) {

		final RatingSystem ratingSystem = ship.getRatingSystem();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_SHIELD_RATING);
			statement.setInt(1, ship.getObjectId());

			rset = statement.executeQuery();

			while(rset.next()) {

				final ShieldModuleRatingElementType type = ShieldModuleRatingElementType.valueOf(rset.getInt(1));

				final ShieldModuleRatingElement<ShieldModuleRatingEvent> element = ratingSystem.findElement(type);
				element.setHitCount(rset.getLong(2));
				element.setReloadCount(rset.getLong(3));
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление рейтинга использования щита.
	 * 
	 * @param ship корабль, у которого изменен рейтинг.
	 * @param element измененный элемент рейтинга.
	 */
	public void updateShieldRating(SpaceShip ship, ShieldModuleRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_SHIELD_RATING);
			statement.setLong(1, element.getHitCount());
			statement.setLong(2, element.getReloadCount());
			statement.setInt(3, ship.getObjectId());
			statement.setInt(4, elementType.getIndex());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о новом рейтинге щита.
	 * 
	 * @param ship кораблт для которого создается.
	 * @param element элемент для которого создается запись рейтинга.
	 */
	public void addNewShieldRating(SpaceShip ship, ShieldModuleRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_SHIELD_RATING);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, elementType.getIndex());
			statement.setLong(3, element.getHitCount());
			statement.setLong(4, element.getReloadCount());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Загрузка локального рейтинга корабля корабля.
	 * 
	 * @param ship корабль, для которого надо загрузить рейтинг.
	 */
	public void loadLocalRatings(final SpaceShip ship) {

		final RatingSystem ratingSystem = ship.getRatingSystem();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_LOCAL_RATING);
			statement.setInt(1, ship.getObjectId());

			rset = statement.executeQuery();

			while(rset.next()) {

				final LocalRatingElementType type = LocalRatingElementType.valueOf(rset.getInt(1));

				final LocalRatingElement<LocalRatingEvent> element = ratingSystem.findElement(type);
				element.setVar(LocalRatingElementVarType.HIT_COUNT, rset.getLong(2));
				element.setVar(LocalRatingElementVarType.REPAIR_COUNT, rset.getLong(3));
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление локального рейтинга корабля.
	 * 
	 * @param ship корабль, у которого изменен рейтинг.
	 * @param element измененный элемент рейтинга.
	 */
	public void updateLocalRating(SpaceShip ship, LocalRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_LOCAL_RATING);
			statement.setLong(1, element.getLongVar(LocalRatingElementVarType.HIT_COUNT));
			statement.setLong(2, element.getLongVar(LocalRatingElementVarType.REPAIR_COUNT));
			statement.setInt(3, ship.getObjectId());
			statement.setInt(4, elementType.getIndex());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о новом элементе локального рейтинга.
	 * 
	 * @param ship корабль для которого создается.
	 * @param element элемент для которого создается запись рейтинга.
	 */
	public void addNewLocalRating(SpaceShip ship, LocalRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_LOCAL_RATING);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, elementType.getIndex());
			statement.setLong(3, element.getLongVar(LocalRatingElementVarType.HIT_COUNT));
			statement.setLong(4, element.getLongVar(LocalRatingElementVarType.REPAIR_COUNT));
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Загрузка рейтинга уничтожений корабля.
	 * 
	 * @param ship корабль, для которого надо загрузить рейтинг.
	 */
	public void loadDestroyRatings(final SpaceShip ship) {

		final RatingSystem ratingSystem = ship.getRatingSystem();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_DESTROY_RATING);
			statement.setInt(1, ship.getObjectId());

			rset = statement.executeQuery();

			while(rset.next()) {

				final DestroyRatingElementType type = DestroyRatingElementType.valueOf(rset.getInt(1));

				final DestroyRatingElement<DestroyRatingEvent> element = ratingSystem.findElement(type);
				element.setDestroyCount(rset.getLong(2));
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление рейтинга уничтожений корабля.
	 * 
	 * @param ship корабль, у которого изменен рейтинг.
	 * @param element измененный элемент рейтинга.
	 */
	public void updateDestroyRating(SpaceShip ship, DestroyRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_DESTROY_RATING);
			statement.setLong(1, element.getDestroyCount());
			statement.setInt(2, ship.getObjectId());
			statement.setInt(3, elementType.getIndex());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о новом элементе рейтинга уничтожений.
	 * 
	 * @param ship корабль для которого создается.
	 * @param element элемент для которого создается запись рейтинга.
	 */
	public void addNewDestroyRating(SpaceShip ship, DestroyRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_DESTROY_RATING);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, elementType.getIndex());
			statement.setLong(3, element.getDestroyCount());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Загрузка рейтинга убийств кораблем.
	 * 
	 * @param ship корабль, для которого надо загрузить рейтинг.
	 */
	public void loadKillRatings(final SpaceShip ship) {

		final RatingSystem ratingSystem = ship.getRatingSystem();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_KILL_RATING);
			statement.setInt(1, ship.getObjectId());

			rset = statement.executeQuery();

			while(rset.next()) {

				final KillRatingElementType type = KillRatingElementType.valueOf(rset.getInt(1));

				final KillRatingElement<KillRatingEvent> element = ratingSystem.findElement(type);
				element.setRocketLauncherCount(rset.getLong(1));
				element.setLaserCount(rset.getInt(2));
				element.setNeutronCount(rset.getLong(3));
				element.setPhotonCount(rset.getLong(4));
				element.setPlasmaCount(rset.getLong(5));
				element.setPulseCount(rset.getLong(6));
				element.setTachyonCount(rset.getLong(7));
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление рейтинга убийств кораблем.
	 * 
	 * @param ship корабль, у которого изменен рейтинг.
	 * @param element измененный элемент рейтинга.
	 */
	public void updateKillRating(SpaceShip ship, KillRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_KILL_RATING);
			statement.setLong(1, element.getLaserCount());
			statement.setLong(2, element.getNeutronCount());
			statement.setLong(3, element.getPhotonCount());
			statement.setLong(4, element.getPlasmaCount());
			statement.setLong(5, element.getPulseCount());
			statement.setLong(6, element.getRocketLayncherCount());
			statement.setLong(7, element.getTachyonCount());
			statement.setInt(8, ship.getObjectId());
			statement.setInt(9, elementType.getIndex());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о новом элементе рейтинга убийств.
	 * 
	 * @param ship корабль для которого создается.
	 * @param element элемент для которого создается запись рейтинга.
	 */
	public void addNewKillRating(SpaceShip ship, KillRatingElement<?> element) {

		final RatingElementType elementType = element.getElementType();

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_KILL_RATING);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, elementType.getIndex());
			statement.setLong(3, element.getLaserCount());
			statement.setLong(4, element.getNeutronCount());
			statement.setLong(5, element.getPhotonCount());
			statement.setLong(6, element.getPlasmaCount());
			statement.setLong(7, element.getPulseCount());
			statement.setLong(8, element.getRocketLayncherCount());
			statement.setLong(9, element.getTachyonCount());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}
}

package com.ss.server.database;

/**
 * Набор названий таблиц в БД сервера.
 * 
 * @author Ronn
 */
public interface TableNames {

	public static final String TABLE_SHIPS = "player_ships";
	public static final String TABLE_SHIPS_C_OBJECT_ID = "object_id";
	public static final String TABLE_SHIPS_C_ACCOUNT_NAME = "account_name";
	public static final String TABLE_SHIPS_C_PILOT_NAME = "pilot_name";
	public static final String TABLE_SHIPS_C_TEMPLATE_ID = "template_id";
	public static final String TABLE_SHIPS_C_CREATE_TIME = "create_time";
	public static final String TABLE_SHIPS_C_LOCATION_ID = "location_id";
	public static final String TABLE_SHIPS_C_LOC_X = "loc_x";
	public static final String TABLE_SHIPS_C_LOC_Y = "loc_y";
	public static final String TABLE_SHIPS_C_LOC_Z = "loc_z";
	public static final String TABLE_SHIPS_C_ROTATION_X = "q_x";
	public static final String TABLE_SHIPS_C_ROTATION_Y = "q_y";
	public static final String TABLE_SHIPS_C_ROTATION_Z = "q_z";
	public static final String TABLE_SHIPS_C_ROTATION_W = "q_w";
	public static final String TABLE_SHIPS_C_LEVEL = "level";
	public static final String TABLE_SHIPS_C_ENERGY = "energy";
	public static final String TABLE_SHIPS_C_ENGINE_ENERGY = "engine_energy";
	public static final String TABLE_SHIPS_C_FRACTION_ID = "fraction_id";
	public static final String TABLE_SHIPS_C_EXP = "exp";
	public static final String TABLE_SHIPS_C_STATION_ID = "station_id";
	public static final String TABLE_SHIPS_C_ON_STATION = "on_station";
	public static final String TABLE_SHIPS_C_CURRENT_STRENGTH = "current_strength";
	public static final String TABLE_SHIPS_C_MAX_STRENGTH = "max_strength";
	public static final String TABLE_SHIPS_C_CURRENT_SHIELD = "current_shield";
	public static final String TABLE_SHIPS_C_MAX_SHIELD = "max_shield";

	public static final String TABLE_MODULES = "player_ship_modules";
	public static final String TABLE_MODULES_C_OBJECT_ID = "object_id";
	public static final String TABLE_MODULES_C_TEMPLATE_ID = "template_id";
	public static final String TABLE_MODULES_C_OWNER_ID = "owner_id";
	public static final String TABLE_MODULES_C_INDEX = "index";

	public static final String TABLE_ITEMS = "items";
	public static final String TABLE_ITEMS_C_OBJECT_ID = "object_id";
	public static final String TABLE_ITEMS_C_LOCATION = "location";

	public static final String TABLE_SKILL_PANEL = "player_ship_skill_panel";
	public static final String TABLE_SKILL_PANEL_C_OBJECT_ID = "object_id";
	public static final String TABLE_SKILL_PANEL_C_MODULE_ID = "module_id";
	public static final String TABLE_SKILL_PANEL_C_SKILL_ID = "skill_id";
	public static final String TABLE_SKILL_PANEL_C_ORDER = "order";

	public static final String TABLE_ITEM_PANEL = "player_ship_item_panel";
	public static final String TABLE_ITEM_PANEL_C_OBJECT_ID = "object_id";
	public static final String TABLE_ITEM_PANEL_C_ITEM_ID = "item_id";
	public static final String TABLE_ITEM_PANEL_C_ORDER = "order";

	public static final String TABLE_INTERFACE = "player_ship_interface";
	public static final String TABLE_INTERFACE_C_OBJECT_ID = "object_id";
	public static final String TABLE_INTERFACE_C_ELEMENT_TYPE = "element_type";
	public static final String TABLE_INTERFACE_C_X = "x";
	public static final String TABLE_INTERFACE_C_Y = "y";
	public static final String TABLE_INTERFACE_C_MINIMIZED = "minimized";

	public static final String TABLE_QUESTS = "player_ship_quests";
	public static final String TABLE_QUESTS_C_OBJECT_ID = "object_id";
	public static final String TABLE_QUESTS_C_QUEST_ID = "quest_id";
	public static final String TABLE_QUESTS_C_STATE = "state";
	public static final String TABLE_QUESTS_C_FINISH_DATE = "finish_date";
	public static final String TABLE_QUESTS_C_FINISH_STATUS = "finish_status";
	public static final String TABLE_QUESTS_C_FAVORITE = "favorite";

	public static final String TABLE_QUEST_VARS = "player_ship_quest_vars";
	public static final String TABLE_QUEST_VARS_C_OBJECT_ID = "object_id";
	public static final String TABLE_QUEST_VARS_C_QUEST_ID = "quest_id";
	public static final String TABLE_QUEST_VARS_C_VAR_NAME = "var_name";
	public static final String TABLE_QUEST_VARS_C_VAR_VALUE = "var_value";

	public static final String TABLE_SKILL_STATES = "skill_states";
	public static final String TABLE_SKILL_STATES_C_OBJECT_ID = "object_id";
	public static final String TABLE_SKILL_STATES_C_MODULE_ID = "module_id";
	public static final String TABLE_SKILL_STATES_C_SKILL_ID = "skill_id";
	public static final String TABLE_SKILL_STATES_C_CHARGE = "charge";
	public static final String TABLE_SKILL_STATES_C_MAX_CHARGE = "max_charge";
	public static final String TABLE_SKILL_STATES_C_RELOAD_TIME = "reload_time";
	public static final String TABLE_SKILL_STATES_C_RELOAD_FINISH_TIME = "reload_finish_time";

	public static final String TABLE_STORAGE = "player_ship_storage";
	public static final String TABLE_STORAGE_C_OWNER_ID = "owner_id";

	public static final String TABLE_WEAPON_RATING = "weapon_rating";
	public static final String TABLE_WEAPON_C_OBJECT_ID = "object_id";
	public static final String TABLE_WEAPON_C_WEAPON_TYPE = "weapon_type";
	public static final String TABLE_WEAPON_C_SHOT_COUNT = "shot_count";
	public static final String TABLE_WEAPON_C_HIT_COUNT = "hit_count";
	public static final String TABLE_WEAPON_C_DEPLETION_COUNT = "depletion_count";

	public static final String TABLE_SHIELD_RATING = "shield_rating";
	public static final String TABLE_SHIELD_C_OBJECT_ID = "object_id";
	public static final String TABLE_SHIELD_C_SHIELD_TYPE = "shield_type";
	public static final String TABLE_SHIELD_C_HIT_COUNT = "hit_count";
	public static final String TABLE_SHIELD_C_RELOAD_COUNT = "reload_count";

	public static final String TABLE_LOCAL_RATING = "local_rating";
	public static final String TABLE_LOCAL_C_OBJECT_ID = "object_id";
	public static final String TABLE_LOCAL_C_ELEMENT_TYPE = "element_type";
	public static final String TABLE_LOCAL_C_HIT_COUNT = "hit_count";
	public static final String TABLE_LOCAL_C_REPAIR_COUNT = "repair_count";

	public static final String TABLE_DESTROY_RATING = "destroy_rating";
	public static final String TABLE_DESTROY_C_OBJECT_ID = "object_id";
	public static final String TABLE_DESTROY_C_DESTROY_TYPE = "destroy_type";
	public static final String TABLE_DESTROY_C_DESTROY_COUNT = "destroy_count";

	public static final String TABLE_KILL_RATING = "kill_rating";
	public static final String TABLE_KILL_C_OBJECT_ID = "object_id";
	public static final String TABLE_KILL_C_KILL_TYPE = "kill_type";
	public static final String TABLE_KILL_C_ROCKET_LAUNCHER_COUNT = "rocket_launcher_count";
	public static final String TABLE_KILL_C_TACHYON_COUNT = "tachyon_count";
	public static final String TABLE_KILL_C_LASER_COUNT = "laser_count";
	public static final String TABLE_KILL_C_PULSE_COUNT = "pulse_count";
	public static final String TABLE_KILL_C_NEUTRON_COUNT = "neutron_count";
	public static final String TABLE_KILL_C_PHOTON_COUNT = "photon_count";
	public static final String TABLE_KILL_C_PLASMA_COUNT = "plasma_count";

	public static final String TABLE_FRIEND_PILOTS = "friend_pilots";
	public static final String TABLE_FRIEND_C_OBJECT_ID = "object_id";
	public static final String TABLE_FRIEND_C_FRIEND_ID = "friend_id";
}

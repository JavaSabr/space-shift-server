package com.ss.server.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import rlib.database.ConnectFactories;
import rlib.database.ConnectFactory;
import rlib.database.DBUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.table.ModuleTable;
import com.ss.server.template.ModuleTemplate;

/**
 * Менеджер по работе с БД с модулями кораблей.
 * 
 * @author Ronn
 */
public class ModuleDBManager implements TableNames {

	private static final Logger LOGGER = LoggerManager.getLogger(PlayerDBManager.class);

	/** запрос на создание нового модуля */
	private static final String INSERT_MODULE = "INSERT INTO `" + TABLE_MODULES + "` (" + TABLE_MODULES_C_OBJECT_ID + ", " + TABLE_MODULES_C_TEMPLATE_ID + ", " + TABLE_MODULES_C_OWNER_ID
			+ ") VALUES (?, ?, ?)";

	/** запрос на обновлениео модуля */
	private static final String UPDATE_MODULE = "UPDATE `" + TABLE_MODULES + "` SET `" + TABLE_MODULES_C_OWNER_ID + "` = ?, `" + TABLE_MODULES_C_INDEX + "` = ? WHERE `" + TABLE_MODULES_C_OBJECT_ID
			+ "` = ? LIMIT 1";

	/** запрос на получение всех модулей для корабля */
	private static final String RESTORE_MODULE = "SELECT `" + TABLE_MODULES_C_TEMPLATE_ID + "`, `" + TABLE_MODULES_C_OBJECT_ID + "`, `" + TABLE_MODULES_C_INDEX + "` FROM `" + TABLE_MODULES
			+ "` WHERE `" + TABLE_MODULES_C_OWNER_ID + "` = ?";

	private static ModuleDBManager instance;

	public static ModuleDBManager getInstance() {

		if(instance == null) {
			instance = new ModuleDBManager();
		}

		return instance;
	}

	/** фабрика подключений к БД */
	private final ConnectFactory connectFactory;

	private ModuleDBManager() {
		InitializeManager.valid(getClass());
		this.connectFactory = ConnectFactories.newBoneCPConnectFactory(Config.DATA_BASE_CONFIG, Config.DATA_BASE_DRIVER);
	}

	/**
	 * Создание в БД записи о новом модуле.
	 * 
	 * @param module модуль корабля.
	 */
	public boolean addNewModule(final Module module) {

		PreparedStatement statement = null;
		Connection con = null;

		try {
			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_MODULE);
			statement.setInt(1, module.getObjectId());
			statement.setInt(2, module.getTemplateId());
			statement.setInt(3, module.getOwnerId());
			statement.execute();

			return true;
		} catch(final SQLException e) {
			LOGGER.warning(e);
			return false;
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * @return фабрика подключений к БД.
	 */
	public ConnectFactory getConnectFactory() {
		return connectFactory;
	}

	/**
	 * Загрузка в систему модулей, пренадлежащих кораблю.
	 */
	public void loadModules(final ModuleSystem system, final LocalObjects local, final int objectId) {

		final ModuleTable moduleTable = ModuleTable.getInstance();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(RESTORE_MODULE);
			statement.setInt(1, objectId);

			rset = statement.executeQuery();

			while(rset.next()) {

				final ModuleTemplate template = moduleTable.getTemplate(rset.getInt(1));

				if(template == null) {
					continue;
				}

				final Module module = template.takeInstance(rset.getInt(2)).getModule();
				system.setModule(rset.getInt(3), module, local);
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Обновление в БД записи о модуле.
	 * 
	 * @param module модуль корабля.
	 */
	public boolean updateModule(final Module module) {

		PreparedStatement statement = null;
		Connection con = null;

		try {
			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_MODULE);
			statement.setInt(1, module.getOwnerId());
			statement.setInt(2, module.getIndex());
			statement.setInt(3, module.getObjectId());
			statement.execute();

			return true;
		} catch(final SQLException e) {
			LOGGER.warning(e);
			return false;
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}
}

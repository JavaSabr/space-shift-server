package com.ss.server.database;

import rlib.database.CleaningManager;
import rlib.database.ConnectFactories;
import rlib.database.ConnectFactory;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.Config;
import com.ss.server.model.storage.ItemLocation;

/**
 * Менеджер для работы с БД.
 * 
 * @author Ronn
 */
public class DataBaseManager implements TableNames {

	protected static final Logger LOGGER = LoggerManager.getLogger(DataBaseManager.class);

	private static DataBaseManager instance;

	public static DataBaseManager getInstance() {

		if(instance == null) {
			instance = new DataBaseManager();
		}

		return instance;
	}

	/** фабрика подключений к БД */
	private final ConnectFactory connectFactory;

	private DataBaseManager() {
		this.connectFactory = ConnectFactories.newBoneCPConnectFactory(Config.DATA_BASE_CONFIG, Config.DATA_BASE_DRIVER);
	}

	/**
	 * Запуск очистки БД.
	 */
	public void clean() {

		if(Config.DATA_BASE_CLEANING_START) {

			// добавляем запросы для очистки БД
			CleaningManager.addQuery("removed {count} player ships", "DELETE FROM `" + TABLE_SHIPS + "` WHERE `" + TABLE_SHIPS_C_ACCOUNT_NAME + "` = 'removed'");
			CleaningManager.addQuery("removed {count} ship modules", "DELETE FROM `" + TABLE_MODULES + "` WHERE `" + TABLE_MODULES_C_OWNER_ID + "` NOT IN (SELECT `" + TABLE_SHIPS_C_OBJECT_ID
					+ "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} items in space", "DELETE FROM `" + TABLE_ITEMS + "` WHERE `" + TABLE_ITEMS_C_LOCATION + "` = '" + ItemLocation.IN_SPACE.ordinal() + "'");
			CleaningManager.addQuery("removed {count} items in storage", "DELETE FROM `" + TABLE_ITEMS + "` WHERE `" + TABLE_ITEMS_C_LOCATION + "` = '" + ItemLocation.IN_SHIP_STORAGE.ordinal()
					+ "' AND `owner_id` NOT IN (SELECT `" + TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} skill panel", "DELETE FROM `" + TABLE_SKILL_PANEL + "` WHERE `" + TABLE_SKILL_PANEL_C_MODULE_ID + "` NOT IN (SELECT `"
					+ TABLE_MODULES_C_OBJECT_ID + "` FROM `" + TABLE_MODULES + "`)");

			CleaningManager.addQuery("removed {count} interface elements", "DELETE FROM `" + TABLE_INTERFACE + "` WHERE `" + TABLE_INTERFACE_C_OBJECT_ID + "` NOT IN (SELECT `"
					+ TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} weapon rating elements", "DELETE FROM `" + TABLE_WEAPON_RATING + "` WHERE `" + TABLE_WEAPON_C_OBJECT_ID + "` NOT IN (SELECT `"
					+ TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} shield rating elements", "DELETE FROM `" + TABLE_SHIELD_RATING + "` WHERE `" + TABLE_SHIELD_C_OBJECT_ID + "` NOT IN (SELECT `"
					+ TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} local rating elements", "DELETE FROM `" + TABLE_LOCAL_RATING + "` WHERE `" + TABLE_LOCAL_C_OBJECT_ID + "` NOT IN (SELECT `"
					+ TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} destroy rating elements", "DELETE FROM `" + TABLE_DESTROY_RATING + "` WHERE `" + TABLE_DESTROY_C_OBJECT_ID + "` NOT IN (SELECT `"
					+ TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} kill rating elements", "DELETE FROM `" + TABLE_KILL_RATING + "` WHERE `" + TABLE_KILL_C_OBJECT_ID + "` NOT IN (SELECT `"
					+ TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} friend pilots", "DELETE FROM `" + TABLE_FRIEND_PILOTS + "` WHERE `" + TABLE_FRIEND_C_FRIEND_ID + "` NOT IN (SELECT `" + TABLE_SHIPS_C_OBJECT_ID
					+ "` FROM `" + TABLE_SHIPS + "`) OR `" + TABLE_FRIEND_C_OBJECT_ID + "` NOT IN (SELECT `" + TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} skill states", "DELETE FROM `" + TABLE_SKILL_STATES + "` WHERE `" + TABLE_SKILL_PANEL_C_OBJECT_ID + "` NOT IN (SELECT `"
					+ TABLE_SHIPS_C_OBJECT_ID + "` FROM `" + TABLE_SHIPS + "`)");

			CleaningManager.addQuery("removed {count} storages", "DELETE FROM `" + TABLE_STORAGE + "` WHERE `" + TABLE_STORAGE_C_OWNER_ID + "` NOT IN (SELECT `" + TABLE_SHIPS_C_OBJECT_ID + "` FROM `"
					+ TABLE_SHIPS + "`)");

			CleaningManager.cleaning(getConnectFactory());
		}
	}

	/**
	 * @return фабрика подкючений.
	 */
	public ConnectFactory getConnectFactory() {
		return connectFactory;
	}
}

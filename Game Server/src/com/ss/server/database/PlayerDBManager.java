package com.ss.server.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import rlib.database.ConnectFactories;
import rlib.database.ConnectFactory;
import rlib.database.DBUtils;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.ObjectContainer;
import com.ss.server.model.faction.Fraction;
import com.ss.server.model.impl.Env;
import com.ss.server.model.impl.Space;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.ship.player.InterfaceInfo;
import com.ss.server.model.ship.player.ItemPanelInfo;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.PlayerShipPreview;
import com.ss.server.model.ship.player.SkillPanelInfo;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.state.SkillStateInfo;
import com.ss.server.model.skills.state.SkillStateList;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.storage.impl.PlayerShipStorage;
import com.ss.server.table.ShipTable;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Менеджер по работе с предметами в БД.
 * 
 * @author Ronn
 */
public class PlayerDBManager implements TableNames {

	private static final Logger LOGGER = LoggerManager.getLogger(PlayerDBManager.class);

	/** запрос на получение кол-ва кораблей на аккаунте */
	private static final String ACCOUNT_SIZE = "SELECT COUNT(" + TABLE_SHIPS_C_PILOT_NAME + ") FROM `" + TABLE_SHIPS + "` WHERE `" + TABLE_SHIPS_C_ACCOUNT_NAME + "`= ? LIMIT 4";

	/** запрос на создание нового корабля */
	private static final String CREATE_SHIP = "INSERT INTO `" + TABLE_SHIPS + "` (" + TABLE_SHIPS_C_OBJECT_ID + ", " + TABLE_SHIPS_C_ACCOUNT_NAME + ", " + TABLE_SHIPS_C_PILOT_NAME + ", "
			+ TABLE_SHIPS_C_TEMPLATE_ID + ", " + TABLE_SHIPS_C_CREATE_TIME + ", " + TABLE_SHIPS_C_LOC_X + ", " + TABLE_SHIPS_C_LOC_Y + ", " + TABLE_SHIPS_C_LOC_Z + ", "
			+ TABLE_SHIPS_C_CURRENT_STRENGTH + ", " + TABLE_SHIPS_C_FRACTION_ID + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/** запрос на создание нового корабля */
	private static final String UPDATE_SHIP = "UPDATE `" + TABLE_SHIPS + "` SET `" + TABLE_SHIPS_C_LOC_X + "` = ?, `" + TABLE_SHIPS_C_LOC_Y + "` = ?, `" + TABLE_SHIPS_C_LOC_Z + "` = ?, `"
			+ TABLE_SHIPS_C_ROTATION_X + "` = ?, `" + TABLE_SHIPS_C_ROTATION_Y + "` = ?, `" + TABLE_SHIPS_C_ROTATION_Z + "` = ?, `" + TABLE_SHIPS_C_ROTATION_W + "` = ?, `" + TABLE_SHIPS_C_LOCATION_ID
			+ "` = ?, `" + TABLE_SHIPS_C_ENGINE_ENERGY + "` = ?, `" + TABLE_SHIPS_C_LEVEL + "` = ?, `" + TABLE_SHIPS_C_EXP + "` = ?, `" + TABLE_SHIPS_C_ON_STATION + "` = ?, `"
			+ TABLE_SHIPS_C_STATION_ID + "` = ?, `" + TABLE_SHIPS_C_CURRENT_STRENGTH + "` = ?, `" + TABLE_SHIPS_C_MAX_STRENGTH + "` = ?, `" + TABLE_SHIPS_C_CURRENT_SHIELD + "` = ?, `"
			+ TABLE_SHIPS_C_MAX_SHIELD + "` = ? WHERE `" + TABLE_SHIPS_C_OBJECT_ID + "` = ? LIMIT 1";

	/** ззапрос на обновление позиции корабля */
	private static final String UPDATE_SHIP_COORDS = "UPDATE`player_ships` SET `loc_x` = ?, `loc_y` = ?, `loc_z` = ?, `container_object_id` = ?, `container_class_id` = ? WHERE `object_id` = ? LIMIT 1";
	/** запрос на удаление корабля */
	private static final String DELETE_SHIP = "UPDATE `player_ships` SET `account_name` = 'removed' WHERE `ship_name` = ? AND `account_name` = ? LIMIT 1";

	/** запрос на получение всех кораблей аккаунта */
	private static final String GET_SHIP_LIST = "SELECT * FROM `player_ships` WHERE `account_name` = ? LIMIT " + PlayerManager.MAXIMUM_ACCOUNT_SIZE;
	/** запрос на загрузку корабля */
	private static final String RESTORE_SHIP = "SELECT * FROM `player_ships` WHERE `object_id` = ? AND `account_name` = ? LIMIT 1";
	/** добавление записи о хранилище в БД */
	private static final String INSERT_STORAGE = "INSERT INTO `player_ship_storage` (`owner_id`, `size`) VALUES(?,?)";

	/** обновление записи о хранилище в БД */
	private static final String UPDATE_STORAGE = "UPDATE `player_ship_storage` SET `size` = ? WHERE `owner_id` = ? LIMIT 1";

	/** запрос на загрузку хранилища корабля */
	private static final String SELECT_STORAGE = "SELECT size FROM `player_ship_storage` WHERE `owner_id` = ? LIMIT 1";

	/** запрос на обновление позиции элемента интерфейса */
	private static final String UPDATE_INTERFACE = "UPDATE `" + TABLE_INTERFACE + "` SET `" + TABLE_INTERFACE_C_X + "` = ?, `" + TABLE_INTERFACE_C_Y + "` = ?, `" + TABLE_INTERFACE_C_MINIMIZED
			+ "` = ? WHERE `" + TABLE_INTERFACE_C_OBJECT_ID + "` = ? AND `" + TABLE_INTERFACE_C_ELEMENT_TYPE + "` = ? LIMIT 1";

	/** запрос на вставку записи о позиции элемента интерфейса */
	private static final String INSERT_INTERFACE = "INSERT INTO `" + TABLE_INTERFACE + "` (" + TABLE_INTERFACE_C_OBJECT_ID + ", " + TABLE_INTERFACE_C_ELEMENT_TYPE + ", " + TABLE_INTERFACE_C_X + ", "
			+ TABLE_INTERFACE_C_Y + ") VALUES(?,?,?,?)";

	/** запрос на получение всех записей об положений интерфейса */
	private static final String SELECT_INTERFACE = "SELECT `" + TABLE_INTERFACE_C_ELEMENT_TYPE + "`, `" + TABLE_INTERFACE_C_X + "`, `" + TABLE_INTERFACE_C_Y + "`, `" + TABLE_INTERFACE_C_MINIMIZED
			+ "` FROM `" + TABLE_INTERFACE + "` WHERE `" + TABLE_INTERFACE_C_OBJECT_ID + "` = ?";

	/** запрос на обновление позиции скила на панели */
	private static final String UPDATE_SKILL_PANEL = "UPDATE `" + TABLE_SKILL_PANEL + "` SET `" + TABLE_SKILL_PANEL_C_ORDER + "` = ? WHERE `" + TABLE_SKILL_PANEL_C_OBJECT_ID + "` = ? AND `"
			+ TABLE_SKILL_PANEL_C_MODULE_ID + "` = ? AND `" + TABLE_SKILL_PANEL_C_SKILL_ID + "` = ? LIMIT 1";

	/** запрос на вставку записи о позиции скила на панели */
	private static final String INSERT_SKILL_PANEL = "INSERT INTO `" + TABLE_SKILL_PANEL + "` (`" + TABLE_SKILL_PANEL_C_OBJECT_ID + "`, `" + TABLE_SKILL_PANEL_C_MODULE_ID + "`, `"
			+ TABLE_SKILL_PANEL_C_SKILL_ID + "`, `" + TABLE_SKILL_PANEL_C_ORDER + "`) VALUES(?,?,?,?)";

	/** запрос на получение всех записей об положений скилов на панели */
	private static final String SELECT_SKILL_PANEL = "SELECT `" + TABLE_SKILL_PANEL_C_MODULE_ID + "`, `" + TABLE_SKILL_PANEL_C_SKILL_ID + "`, `" + TABLE_SKILL_PANEL_C_ORDER + "` FROM `"
			+ TABLE_SKILL_PANEL + "` WHERE `" + TABLE_SKILL_PANEL_C_OBJECT_ID + "` = ?";

	/** запрос на удаление о записи положения скила на панели */
	private static final String REMOVE_SKILL_PANEL = "DELETE FROM `" + TABLE_SKILL_PANEL + "` WHERE `" + TABLE_SKILL_PANEL_C_OBJECT_ID + "` = ? AND `" + TABLE_SKILL_PANEL_C_MODULE_ID + "` = ? AND `"
			+ TABLE_SKILL_PANEL_C_SKILL_ID + "` = ? LIMIT 1";

	/** запрос на обновление позиции скила на панели */
	private static final String UPDATE_ITEM_PANEL = "UPDATE `" + TABLE_ITEM_PANEL + "` SET `" + TABLE_ITEM_PANEL_C_ORDER + "` = ? WHERE `" + TABLE_ITEM_PANEL_C_OBJECT_ID + "` = ? AND `"
			+ TABLE_ITEM_PANEL_C_ITEM_ID + "` = ? LIMIT 1";

	/** запрос на вставку записи о позиции скила на панели */
	private static final String INSERT_ITEM_PANEL = "INSERT INTO `" + TABLE_ITEM_PANEL + "` (`" + TABLE_ITEM_PANEL_C_OBJECT_ID + "`, `" + TABLE_ITEM_PANEL_C_ITEM_ID + "`, `"
			+ TABLE_ITEM_PANEL_C_ORDER + "`) VALUES(?,?,?)";

	/** запрос на получение всех записей об положений скилов на панели */
	private static final String SELECT_ITEM_PANEL = "SELECT `" + TABLE_ITEM_PANEL_C_ITEM_ID + "`, `" + TABLE_ITEM_PANEL_C_ORDER + "` FROM `" + TABLE_ITEM_PANEL + "` WHERE `"
			+ TABLE_ITEM_PANEL_C_OBJECT_ID + "` = ?";

	/** запрос на удаление о записи положения скила на панели */
	private static final String REMOVE_ITEM_PANEL = "DELETE FROM `" + TABLE_ITEM_PANEL + "` WHERE `" + TABLE_ITEM_PANEL_C_OBJECT_ID + "` = ? AND `" + TABLE_ITEM_PANEL_C_ITEM_ID + "` = ?  LIMIT 1";

	/** ===================== Skill States ============================= */
	/** запрос на загрузку состояний умений из БД */
	private static final String SELECT_SKILL_STATES = "SELECT `" + TABLE_SKILL_STATES_C_MODULE_ID + "`,`" + TABLE_SKILL_STATES_C_SKILL_ID + "`, `" + TABLE_SKILL_STATES_C_CHARGE + "` , `"
			+ TABLE_SKILL_STATES_C_MAX_CHARGE + "`, `" + TABLE_SKILL_STATES_C_RELOAD_TIME + "`, `" + TABLE_SKILL_STATES_C_RELOAD_FINISH_TIME + "` FROM `" + TABLE_SKILL_STATES + "` WHERE `"
			+ TABLE_SKILL_STATES_C_OBJECT_ID + "` = ?";

	/** обновление состояния умения в БД */
	private static final String UPDATE_SKILL_STATE = "UPDATE `" + TABLE_SKILL_STATES + "` SET `" + TABLE_SKILL_STATES_C_CHARGE + "` = ?, `" + TABLE_SKILL_STATES_C_MAX_CHARGE + "` = ?, `"
			+ TABLE_SKILL_STATES_C_RELOAD_TIME + "` = ?, `" + TABLE_SKILL_STATES_C_RELOAD_FINISH_TIME + "` = ? WHERE `" + TABLE_SKILL_STATES_C_OBJECT_ID + "` = ? AND `"
			+ TABLE_SKILL_STATES_C_SKILL_ID + "` = ? AND `" + TABLE_SKILL_STATES_C_MODULE_ID + "` = ? LIMIT 1";

	/** добавление записи о состоянии умения в БД */
	private static final String INSERT_SKILL_STATE = "INSERT INTO `" + TABLE_SKILL_STATES + "` (`" + TABLE_SKILL_STATES_C_OBJECT_ID + "`, `" + TABLE_SKILL_STATES_C_MODULE_ID + "`, `"
			+ TABLE_SKILL_STATES_C_SKILL_ID + "`, `" + TABLE_SKILL_STATES_C_CHARGE + "`, `" + TABLE_SKILL_STATES_C_MAX_CHARGE + "`, `" + TABLE_SKILL_STATES_C_RELOAD_TIME + "`, `"
			+ TABLE_SKILL_STATES_C_RELOAD_FINISH_TIME + "`) VALUES(?,?,?,?,?,?,?)";

	/** ===================== Friend Pilots ============================= */
	/** запрос на загрузку списка друзей игрока */
	private static final String SELECT_FRIENDS = "SELECT `" + TABLE_FRIEND_C_FRIEND_ID + "` FROM `" + TABLE_FRIEND_PILOTS + "` WHERE `" + TABLE_FRIEND_C_OBJECT_ID + "` = ?";
	/** запрос на удаление игрока из друзей */
	private static final String DELETE_FRIEND = "DELETE FROM `" + TABLE_FRIEND_PILOTS + "` WHERE `" + TABLE_FRIEND_C_OBJECT_ID + "` = ? AND `" + TABLE_FRIEND_C_FRIEND_ID + "` = ? LIMIT 1";
	/** запрос на добавление друга */
	private static final String INSERT_FRIEND = "INSERT INTO `" + TABLE_FRIEND_PILOTS + "` (`" + TABLE_FRIEND_C_OBJECT_ID + "`, `" + TABLE_FRIEND_C_FRIEND_ID + "`) VALUES(?,?)";

	private static PlayerDBManager instance;

	public static PlayerDBManager getInstance() {

		if(instance == null) {
			instance = new PlayerDBManager();
		}

		return instance;
	}

	/** фабрика подключений к БД */
	private final ConnectFactory connectFactory;

	private PlayerDBManager() {
		InitializeManager.valid(getClass());
		this.connectFactory = ConnectFactories.newBoneCPConnectFactory(Config.DATA_BASE_CONFIG, Config.DATA_BASE_DRIVER);
	}

	/**
	 * Создание в БД записи о дружественном пилоте.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param friendId уникальный ид дружественного пилота.
	 */
	public boolean addNewFriend(final int objectId, final int friendId) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_FRIEND);
			statement.setInt(1, objectId);
			statement.setInt(2, friendId);
			statement.execute();

			return true;

		} catch(final SQLException e) {
			LOGGER.warning(e);
			return false;
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о элементе интерфейса.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об элементе.
	 */
	public void addNewInterface(final int objectId, final InterfaceInfo info) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_INTERFACE);
			statement.setInt(1, objectId);
			statement.setInt(2, info.getType());
			statement.setInt(3, info.getX());
			statement.setInt(4, info.getY());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о положении предмета на панели.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об положении предмета.
	 */
	public void addNewItemPanel(final int objectId, final ItemPanelInfo info) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_ITEM_PANEL);
			statement.setInt(1, objectId);
			statement.setInt(2, info.getObjectId());
			statement.setInt(3, info.getOrder());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о положении скила на панели.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об положении скила.
	 */
	public void addNewSkillPanel(final int objectId, final SkillPanelInfo info) {

		final Skill skill = info.getSkill();

		if(skill == null) {
			return;
		}

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_SKILL_PANEL);
			statement.setInt(1, objectId);
			statement.setInt(2, skill.getModuleObjectId());
			statement.setInt(3, skill.getId());
			statement.setInt(4, info.getOrder());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создать запись в БД о состоянии умения модуля корабля.
	 * 
	 * @param ship корабль игрока.
	 * @param info информация о состоянии.
	 */
	public void addNewSkillState(final PlayerShip ship, final SkillStateInfo info) {

		if(info == null) {
			return;
		}

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_SKILL_STATE);
			statement.setInt(1, ship.getObjectId());
			statement.setInt(2, info.getModuleId());
			statement.setInt(3, info.getSkillId());
			statement.setInt(4, info.getCharge());
			statement.setInt(5, info.getMaxCharge());
			statement.setInt(6, info.getReloadTime());
			statement.setLong(7, info.getReloadFinishTime());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создать запись в БД о новом хранилище игрока.
	 * 
	 * @param storage новое хранилище игрока.
	 */
	public void addNewStorage(final PlayerShipStorage storage) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(INSERT_STORAGE);
			statement.setInt(1, storage.getOwnerId());
			statement.setInt(2, storage.getSize());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Создание в БД записи о новом корабле.
	 * 
	 * @param ship новый корабль.
	 * @param accountName имя аккаунта.
	 * @return успешно ли создана запись.
	 */
	public boolean createPlayerShip(final PlayerShip ship, final String accountName) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			final PlayerShipTemplate template = ship.getTemplate();
			final Vector location = ship.getLocation();
			final Fraction fraction = ship.getFraction();

			statement = con.prepareStatement(CREATE_SHIP);
			statement.setInt(1, ship.getObjectId());
			statement.setString(2, accountName);
			statement.setString(3, ship.getName());
			statement.setInt(4, template.getId());
			statement.setLong(5, System.currentTimeMillis());
			statement.setFloat(6, location.getX());
			statement.setFloat(7, location.getY());
			statement.setFloat(8, location.getZ());
			statement.setInt(9, ship.getCurrentStrength());
			statement.setInt(10, fraction.getId());
			statement.execute();

			return true;
		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}

		return false;
	}

	/**
	 * Запрос на удаление корабля на аккаунте.
	 * 
	 * @param accountName имя аккаунта, на котором удаляем корабль.
	 * @param shipName название корабля.
	 * @return был ли выполнен запрос.
	 */
	public boolean deletePlayerShip(final String accountName, final String shipName) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(DELETE_SHIP);
			statement.setString(1, shipName);
			statement.setString(2, accountName);

			return statement.executeUpdate() > 0;

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}

		return false;
	}

	/**
	 * Получение размера аккаунта.
	 * 
	 * @param accountName имя аккаунта.
	 * @return кол-во кораблей на аккаунте.
	 */
	public int getAccountSize(final String accountName) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			int number = 0;

			con = connectFactory.getConnection();

			statement = con.prepareStatement(ACCOUNT_SIZE);
			statement.setString(1, accountName);

			rset = statement.executeQuery();

			while(rset.next()) {
				number = rset.getInt(1);
			}

			return number;
		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}

		return 0;
	}

	/**
	 * Загрузка списка дружественных пилотов.
	 */
	public PlayerShipStorage loadFriends(final PlayerShip playerShip) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_FRIENDS);
			statement.setInt(1, playerShip.getObjectId());

			rset = statement.executeQuery();

			while(rset.next()) {
				// TODO
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}

		return null;
	}

	/**
	 * Загрузка настроек интерфейса.
	 */
	public void loadInterface(final int objectId, final Array<InterfaceInfo> container) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_INTERFACE);
			statement.setInt(1, objectId);

			container.writeLock();
			try {

				rset = statement.executeQuery();

				while(rset.next()) {
					container.add(InterfaceInfo.newInstance(rset.getInt(1), rset.getInt(2), rset.getInt(3), rset.getBoolean(4)));
				}

			} finally {
				container.writeUnlock();
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Загрузка настроек положения предметов на панели.
	 */
	public void loadItemPanels(final int objectId, final PlayerShip ship, final Array<ItemPanelInfo> container) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_ITEM_PANEL);
			statement.setInt(1, objectId);

			container.writeLock();
			try {

				rset = statement.executeQuery();

				while(rset.next()) {
					container.add(ItemPanelInfo.newInstance(rset.getInt(1), Config.SERVER_ITEM_CLASS_ID, rset.getInt(2)));
				}

			} finally {
				container.writeUnlock();
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Получение списка кораблей на аккаунте.
	 * 
	 * @param container контейнер превьюшек.
	 * @param local контейнер локальных объектов.
	 * @param accountName имя акаунта.
	 */
	public void loadShipList(final Array<PlayerShipPreview> container, final LocalObjects local, final String accountName) {

		final PlayerManager playerManager = PlayerManager.getInstance();
		final ModuleDBManager dbManager = ModuleDBManager.getInstance();
		final ShipTable shipTable = ShipTable.getInstance();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(GET_SHIP_LIST);
			statement.setString(1, accountName);

			rset = statement.executeQuery();

			while(rset.next()) {

				final PlayerShipTemplate template = shipTable.getTemplate(PlayerShipTemplate.class, rset.getInt(TABLE_SHIPS_C_TEMPLATE_ID));

				if(template == null) {
					continue;
				}

				final ModuleSystem moduleSystem = template.takeModuleSystem();

				// создаем превью
				final PlayerShipPreview preview = new PlayerShipPreview();

				preview.setFraction(playerManager.getFraction(rset.getInt(TABLE_SHIPS_C_FRACTION_ID)));
				preview.setObjectId(rset.getInt(TABLE_SHIPS_C_OBJECT_ID));
				preview.setName(rset.getString(TABLE_SHIPS_C_PILOT_NAME));
				preview.setCurrentStrength(rset.getInt(TABLE_SHIPS_C_CURRENT_STRENGTH));
				preview.setMaxStrength(rset.getInt(TABLE_SHIPS_C_MAX_STRENGTH));
				preview.setCurrentShield(rset.getInt(TABLE_SHIPS_C_CURRENT_SHIELD));
				preview.setMaxShield(rset.getInt(TABLE_SHIPS_C_MAX_SHIELD));
				preview.setTemplate(template);
				preview.setModuleSystem(moduleSystem);

				dbManager.loadModules(moduleSystem, local, preview.getObjectId());

				container.add(preview);
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Загрузка настроек положения скилов на панели.
	 */
	public void loadSkillPanels(final int objectId, final PlayerShip ship, final Array<SkillPanelInfo> container) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_SKILL_PANEL);
			statement.setInt(1, objectId);

			container.writeLock();
			try {

				rset = statement.executeQuery();

				while(rset.next()) {

					final Skill skill = ship.getSkill(rset.getInt(1), rset.getInt(2));

					if(skill == null) {
						continue;
					}

					container.add(SkillPanelInfo.newInstance(skill, rset.getInt(3)));
				}

			} finally {
				container.writeUnlock();
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Загрузка состояний всех умений корабля.
	 * 
	 * @param playerShip корабль игрока.
	 */
	public void loadSkillStates(final PlayerShip playerShip) {

		final SkillStateList skillStateList = playerShip.getSkillStateList();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_SKILL_STATES);
			statement.setInt(1, playerShip.getObjectId());

			rset = statement.executeQuery();

			while(rset.next()) {
				skillStateList.addState(rset.getInt(1), rset.getInt(2), rset.getInt(3), rset.getInt(4), rset.getInt(5), rset.getLong(6));
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}
	}

	/**
	 * Загрузка хранилища игрока.
	 */
	public PlayerShipStorage loagStorage(final PlayerShip playerShip) {

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(SELECT_STORAGE);
			statement.setInt(1, playerShip.getObjectId());

			rset = statement.executeQuery();

			if(rset.next()) {
				return PlayerShipStorage.newInstance(playerShip, rset.getInt(1));
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}

		return null;
	}

	/**
	 * Удаление из БД записи о дружественном пилоте.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param friendId уникальный ид дружественного пилота.
	 */
	public boolean removeFriend(final int objectId, final int friendId) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(DELETE_FRIEND);
			statement.setInt(1, objectId);
			statement.setInt(2, friendId);
			statement.execute();

			return true;

		} catch(final SQLException e) {
			LOGGER.warning(e);
			return false;
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Удаление из БД записи о положении предмета на панели.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об положении предмета.
	 */
	public void removeItemPanel(final int objectId, final ItemPanelInfo info) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(REMOVE_ITEM_PANEL);
			statement.setInt(1, objectId);
			statement.setInt(2, info.getObjectId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Удаление из БД записи о положении скила на панели.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об скиле.
	 */
	public void removeSkillPanel(final int objectId, final SkillPanelInfo info) {

		final Skill skill = info.getSkill();

		if(skill == null) {
			return;
		}

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(REMOVE_SKILL_PANEL);
			statement.setInt(1, objectId);
			statement.setInt(2, skill.getModuleObjectId());
			statement.setInt(3, skill.getId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Загрузка корабля из базы.
	 * 
	 * @param accountName имя аккаунта.
	 * @param objectId уникальный ид корабля.
	 * @param local контейнер локальных объектов.
	 */
	public PlayerShip restorePlayerShip(final String accountName, final int objectId, final LocalObjects local) {

		final PlayerManager playerManager = PlayerManager.getInstance();
		final ShipTable shipTable = ShipTable.getInstance();

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(RESTORE_SHIP);
			statement.setInt(1, objectId);
			statement.setString(2, accountName);

			rset = statement.executeQuery();

			if(!rset.next()) {
				return null;
			}

			final PlayerShipTemplate template = shipTable.getTemplate(PlayerShipTemplate.class, rset.getInt("template_id"));

			if(template == null) {
				return null;
			}

			final Fraction fraction = playerManager.getFraction(rset.getInt(TABLE_SHIPS_C_FRACTION_ID));

			if(fraction == null) {
				LOGGER.warning("not found fraction for id " + rset.getInt(TABLE_SHIPS_C_FRACTION_ID));
				return null;
			}

			final PlayerShip playerShip = (PlayerShip) template.takeInstance(objectId);
			playerShip.setFraction(fraction);

			final Vector location = playerShip.getLocation();

			location.setXYZ(rset.getFloat("loc_x"), rset.getFloat("loc_y"), rset.getFloat("loc_z"));

			final Rotation rotation = playerShip.getRotation();

			rotation.setXYZW(rset.getFloat("q_x"), rset.getFloat("q_y"), rset.getFloat("q_z"), rset.getFloat("q_w"));

			playerShip.setRotation(rotation, local);

			playerShip.setLocationId(rset.getInt("location_id"));
			playerShip.setOnlineTime(rset.getLong("online_time"));
			playerShip.setEnterTime(System.currentTimeMillis());
			playerShip.setName(rset.getString(TABLE_SHIPS_C_PILOT_NAME));
			playerShip.setEngineEnergy(rset.getFloat("engine_energy"));
			playerShip.setLevel(rset.getInt("level"));
			playerShip.setExp(rset.getLong("exp"));

			final AtomicBoolean onStation = playerShip.getOnStation();
			onStation.set(rset.getByte(TABLE_SHIPS_C_ON_STATION) == 1);

			// TODO убрать авторемонт, когда будет реализована эта логика в
			// рамках игры
			playerShip.setCurrentStrength(Integer.MAX_VALUE, LocalObjects.get());

			final int stationId = rset.getInt(TABLE_SHIPS_C_STATION_ID);

			if(stationId != 0) {

				final Space space = Space.getInstance();
				final SpaceStation station = space.getContainer(stationId, Config.SERVER_STATION_CLASS_ID);

				final AtomicReference<SpaceStation> reference = playerShip.getStationReference();
				reference.getAndSet(station);
			}

			return playerShip;
		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCSR(con, statement, rset);
		}

		return null;
	}

	/**
	 * Обновление положения корабля в пространстве в БД.
	 * 
	 * @param objectId уникальный ид корабля.
	 * @param x текущая координата.
	 * @param y текущая координата.
	 * @param z текущая координата.
	 * @param container контейнер объектов, если он есть.
	 */
	public void updateCoords(final int objectId, final float x, final float y, final float z, final ObjectContainer container) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_SHIP_COORDS);
			statement.setFloat(1, x);
			statement.setFloat(2, y);
			statement.setFloat(3, z);

			if(container == null) {
				statement.setInt(4, 0);
				statement.setInt(5, 0);
			} else {
				statement.setInt(4, container.getObjectId());
				statement.setInt(5, container.getClassId());
			}

			statement.setInt(6, objectId);
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * обновление в БД записи о элементе интерфейса.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об элементе.
	 */
	public void updateInterface(final int objectId, final InterfaceInfo info) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_INTERFACE);
			statement.setInt(1, info.getX());
			statement.setInt(2, info.getY());
			statement.setBoolean(3, info.isMinimized());
			statement.setInt(4, objectId);
			statement.setInt(5, info.getType());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Обновление в БД записи о положении предмета на панели.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об положении предмета.
	 */
	public void updateItemPanel(final int objectId, final ItemPanelInfo info) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_ITEM_PANEL);
			statement.setInt(1, info.getOrder());
			statement.setInt(2, objectId);
			statement.setInt(3, info.getObjectId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Обновление в БД записи о новом корабле.
	 * 
	 * @param ship корабль.
	 * @return успешно ли обновлен.
	 */
	public boolean updatePlayerShip(final PlayerShip ship) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			final Vector location = ship.getLocation();
			final Rotation rotation = ship.getRotation();

			int stationId = 0;

			final AtomicReference<SpaceStation> stationReference = ship.getStationReference();
			final AtomicBoolean onStation = ship.getOnStation();

			synchronized(stationReference) {

				final SpaceStation station = stationReference.get();

				if(station != null) {
					stationId = station.getObjectId();
				}
			}

			final LocalObjects local = LocalObjects.get();
			final Env env = local.getNextEnv();

			statement = con.prepareStatement(UPDATE_SHIP);

			statement.setFloat(1, location.getX());
			statement.setFloat(2, location.getY());
			statement.setFloat(3, location.getZ());
			statement.setFloat(4, rotation.getX());
			statement.setFloat(5, rotation.getY());
			statement.setFloat(6, rotation.getZ());
			statement.setFloat(7, rotation.getW());
			statement.setInt(8, ship.getLocationId());
			statement.setFloat(9, ship.getEngineEnergy());
			statement.setInt(10, ship.getLevel());
			statement.setLong(11, ship.getExp());
			statement.setByte(12, (byte) (onStation.get() ? 1 : 0));
			statement.setLong(13, stationId);
			statement.setInt(14, ship.getCurrentStrength());
			statement.setInt(15, ship.getMaxStrength(env));
			statement.setInt(16, ship.getCurrentShield());
			statement.setInt(17, ship.getMaxShield());
			statement.setInt(18, ship.getObjectId());
			statement.executeUpdate();

			return true;
		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}

		return false;
	}

	/**
	 * Обновление в БД записи о положении скила на панели.
	 * 
	 * @param objectId уникальный ид игрока.
	 * @param info инфа об положении скила.
	 */
	public void updateSkillPanel(final int objectId, final SkillPanelInfo info) {

		final Skill skill = info.getSkill();

		if(skill == null) {
			return;
		}

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_SKILL_PANEL);
			statement.setInt(1, info.getOrder());
			statement.setInt(2, objectId);
			statement.setInt(3, skill.getModuleObjectId());
			statement.setInt(4, skill.getId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Обновление состояния умения модуля корабля в БД.
	 * 
	 * @param ship корабль игрока.
	 * @param info информация об состоянии.
	 */
	public void updateSkillState(final PlayerShip ship, final SkillStateInfo info) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_SKILL_STATE);
			statement.setInt(1, info.getCharge());
			statement.setInt(2, info.getMaxCharge());
			statement.setInt(3, info.getReloadTime());
			statement.setLong(4, info.getReloadFinishTime());
			statement.setInt(5, ship.getObjectId());
			statement.setInt(6, info.getSkillId());
			statement.setInt(7, info.getModuleId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Обновление в БД записи о хранилище игрока.
	 * 
	 * @param storage обновляемое храниище.
	 */
	public void updateStorage(final PlayerShipStorage storage) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(UPDATE_STORAGE);
			statement.setInt(1, storage.getSize());
			statement.setInt(2, storage.getOwnerId());
			statement.execute();

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}
}

package com.ss.server;

import java.io.File;

import rlib.Monitoring;
import rlib.compiler.CompilerFactory;
import rlib.concurrent.util.ThreadUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerLevel;
import rlib.logging.LoggerManager;
import rlib.logging.impl.FolderFileListener;
import rlib.manager.InitializeManager;
import rlib.monitoring.MemoryMonitoring;
import rlib.monitoring.MonitoringManager;
import rlib.util.Util;

import com.ss.server.database.DataBaseManager;
import com.ss.server.database.ItemDBManager;
import com.ss.server.database.ModuleDBManager;
import com.ss.server.database.PlayerDBManager;
import com.ss.server.database.QuestDBManager;
import com.ss.server.database.RatingDBManager;
import com.ss.server.manager.AccountManager;
import com.ss.server.manager.ClassManager;
import com.ss.server.manager.CollisionManager;
import com.ss.server.manager.CommandManager;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.manager.FormulasManager;
import com.ss.server.manager.FuncManager;
import com.ss.server.manager.LocationManager;
import com.ss.server.manager.PlayerManager;
import com.ss.server.manager.QuestManager;
import com.ss.server.manager.SkillActiveManager;
import com.ss.server.model.impl.Space;
import com.ss.server.network.Network;
import com.ss.server.table.ConfigAiTable;
import com.ss.server.table.GraficEffectTable;
import com.ss.server.table.GravityObjectTable;
import com.ss.server.table.ItemShopPriceTable;
import com.ss.server.table.ItemTable;
import com.ss.server.table.LocationObjectTable;
import com.ss.server.table.ModuleTable;
import com.ss.server.table.ShipTable;
import com.ss.server.table.SkillTable;
import com.ss.server.table.SpawnTable;
import com.ss.server.table.StationTable;

/**
 * Модель игрового сервера.
 * 
 * @author Ronn
 */
public class GameServer extends ServerThread {

	private static void configureLogging() {

		LoggerLevel.DEBUG.setEnabled(false);
		LoggerLevel.ERROR.setEnabled(true);
		LoggerLevel.WARNING.setEnabled(true);
		LoggerLevel.INFO.setEnabled(true);

		LoggerManager.addListener(new FolderFileListener(new File(Config.FOLDER_PROJECT_PATH, "log")));
	}

	/**
	 * @return экземпляр гейм сервера.
	 */
	public static GameServer getInstance() {

		if(instance == null) {
			instance = new GameServer();
		}

		return instance;
	}

	public static void main(final String[] args) throws Exception {

		if(!CompilerFactory.isAvailableCompiler()) {
			throw new RuntimeException("not found java compiler.");
		}

		Monitoring.init();
		Config.init(args);

		configureLogging();

		Util.checkFreePort("*", Config.SERVER_GAME_PORT);

		final DataBaseManager manager = DataBaseManager.getInstance();
		manager.clean();

		getInstance();

		System.gc();
	}

	private static final Logger LOGGER = LoggerManager.getLogger(GameServer.class);

	/** экземпляр гейм сервера */
	private static GameServer instance;

	public GameServer() {
		setName("Initialize Thread");
		start();
	}

	@Override
	public void run() {
		try {

			InitializeManager.register(ExecutorManager.class);
			InitializeManager.register(FormulasManager.class);
			InitializeManager.register(DataBaseManager.class);
			InitializeManager.register(ModuleDBManager.class);
			InitializeManager.register(PlayerDBManager.class);
			InitializeManager.register(RatingDBManager.class);
			InitializeManager.register(QuestDBManager.class);
			InitializeManager.register(ItemDBManager.class);
			InitializeManager.register(IdFactory.class);
			InitializeManager.register(AccountManager.class);
			InitializeManager.register(ClassManager.class);
			InitializeManager.register(FuncManager.class);
			InitializeManager.register(GraficEffectTable.class);
			InitializeManager.register(ItemTable.class);
			InitializeManager.register(ItemShopPriceTable.class);
			InitializeManager.register(SkillTable.class);
			InitializeManager.register(SkillActiveManager.class);
			InitializeManager.register(GravityObjectTable.class);
			InitializeManager.register(LocationObjectTable.class);
			InitializeManager.register(StationTable.class);
			InitializeManager.register(LocationManager.class);
			InitializeManager.register(ModuleTable.class);
			InitializeManager.register(ShipTable.class);
			InitializeManager.register(QuestManager.class);
			InitializeManager.register(PlayerManager.class);
			InitializeManager.register(Space.class);
			InitializeManager.register(CommandManager.class);
			InitializeManager.register(ConfigAiTable.class);
			InitializeManager.register(CollisionManager.class);
			InitializeManager.register(SpawnTable.class);
			InitializeManager.register(Network.class);
			InitializeManager.initialize();

			LOGGER.info("started.");

			final Runnable target = () -> {

				final MonitoringManager manager = MonitoringManager.getInstance();
				final MemoryMonitoring memory = manager.getMemoryMonitoring();

				while(true) {
					LOGGER.info(memory, memory.getHeapUsedSize() / 1024 / 1024 + " MiB.");
					ThreadUtils.sleep(30000);
				}
			};

			final ServerThread thread = new ServerThread(target);
			thread.setDaemon(true);
			thread.setName("Monitoring Memory");
			thread.start();

		} catch(final Exception e) {
			LOGGER.warning(e);
			System.exit(0);
		}
	}
}

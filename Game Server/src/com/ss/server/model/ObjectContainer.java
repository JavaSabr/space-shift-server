package com.ss.server.model;

import rlib.geom.Vector;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;

/**
 * Интерфейс для реализаци контейнера объектов.
 * 
 * @author Ronn
 */
public interface ObjectContainer extends SpaceObject {

	/**
	 * @param object добавляемый объект.
	 */
	public void add(SpaceObject object);

	/**
	 * Находится ли объект в контейнере.
	 * 
	 * @param object проверяемый объект.
	 * @return содержится ли объект.
	 */
	public boolean contains(SpaceObject object);

	/**
	 * Переместить содержимые объекты.
	 * 
	 * @param changed вектор смещения.
	 * @param local контейнер локальных объектов.
	 */
	public void flyChild(Vector changed, LocalObjects local);

	/**
	 * @return содержимые объекты.
	 */
	public Array<SpaceObject> getObjects();

	/**
	 * @param object удаляемый объект.
	 */
	public void remove(SpaceObject object);
}

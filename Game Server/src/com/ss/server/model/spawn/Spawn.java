package com.ss.server.model.spawn;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.template.ObjectTemplate;

/**
 * Интерфейс для реализации спавна чего-либо.
 * 
 * @author Ronn
 */
public interface Spawn {

	public static final String RESPAWN_TIME = "respawnTime";
	public static final String ROTATION = "rotation";
	public static final String LOCATION = "location";
	public static final String LOCATION_ID = "locationId";
	public static final String TEMPLATE = "template";

	/**
	 * @return ид локации спавна.
	 */
	public int getLocationId();

	/**
	 * @param local контейнер локальных объектов.
	 * @return время респавна.
	 */
	public long getRespawnTime(LocalObjects local);

	/**
	 * @return отспавненный на данный момент объект.
	 */
	public SpaceObject getSpawned();

	/**
	 * @param local контейнер локальных объектов.
	 * @return позиция спавна.
	 */
	public Vector getSpawnLocation(LocalObjects local);

	/**
	 * @param local контейнер локальных объектов.
	 * @return разворот при спавне.
	 */
	public Rotation getSpawnRotation(LocalObjects local);

	/**
	 * @return шаблон спавнящегося объекта.
	 */
	public ObjectTemplate getTemplate();

	/**
	 * @return отспавнен ли на данный момент объект спавна.
	 */
	public boolean isSpawned();

	/**
	 * @return запущен ли спавн.
	 */
	public boolean isStarted();

	/**
	 * Обработка завершения объекта.
	 * 
	 * @param object завершенный объект.
	 */
	public void notifyFinish(SpaceObject object);

	/**
	 * Запуск спавна.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void start(LocalObjects local);

	/**
	 * Остановка спавна.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void stop(LocalObjects local);
}

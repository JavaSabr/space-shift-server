package com.ss.server.model.spawn.impl;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.SafeTask;
import rlib.util.VarTable;
import rlib.util.random.Random;
import rlib.util.random.RandomFactory;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.Space;
import com.ss.server.model.spawn.Spawn;
import com.ss.server.template.ObjectTemplate;

/**
 * Базовая реализация спавна объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractSpawn<T extends ObjectTemplate, O extends SpaceObject> implements Spawn {

	protected static final Logger LOGGER = LoggerManager.getLogger(Spawn.class);

	protected static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();
	protected static final Space SPACE = Space.getInstance();

	protected static final ThreadLocal<Random> LOCAL_RANDOM = new ThreadLocal<Random>() {

		@Override
		protected Random initialValue() {
			return RandomFactory.newRealRandom();
		};
	};

	protected final SafeTask respawnTask = () -> doSpawn(LocalObjects.get());

	/** позиция спавна */
	protected final Vector spawnLocation;
	/** разворот спавна */
	protected final Rotation spawnRotation;
	/** флаг активности спавна */
	protected final AtomicBoolean started;

	/** ссылка на задачу спавна */
	protected volatile ScheduledFuture<SafeTask> scheduleSpawn;

	/** шаблон спавнящегося объекта */
	protected T template;
	/** отспавненный объект */
	protected O spawned;

	/** время респавна */
	protected int respawnTime;
	/** ид локации, в которой спавнится */
	protected int locationId;

	@SuppressWarnings("unchecked")
	public AbstractSpawn(final VarTable vars) {
		this.started = new AtomicBoolean();
		this.template = (T) vars.get(TEMPLATE, ObjectTemplate.class);
		this.locationId = vars.getInteger(LOCATION_ID);
		this.spawnLocation = vars.get(LOCATION, Vector.class);
		this.spawnRotation = vars.get(ROTATION, Rotation.class);
		this.respawnTime = vars.getInteger(RESPAWN_TIME, 0);
	}

	/**
	 * Завершениие работы спавна.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	protected abstract void doFinish(LocalObjects local);

	/**
	 * Обработка процесса спавна объекта.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	protected abstract void doSpawn(LocalObjects local);

	@Override
	public int getLocationId() {
		return locationId;
	}

	/**
	 * @return задача по респавну объекта.
	 */
	protected SafeTask getRespawnTask() {
		return respawnTask;
	}

	/**
	 * @return время респавна.
	 */
	protected int getRespawnTime() {
		return respawnTime;
	}

	@Override
	public long getRespawnTime(final LocalObjects local) {
		return getRespawnTime();
	}

	@Override
	public O getSpawned() {
		return spawned;
	}

	/**
	 * @return позиция спавна.
	 */
	protected Vector getSpawnLocation() {
		return spawnLocation;
	}

	@Override
	public Vector getSpawnLocation(final LocalObjects local) {
		return getSpawnLocation();
	}

	/**
	 * @return разворот при спавне.
	 */
	protected Rotation getSpawnRotation() {
		return spawnRotation;
	}

	@Override
	public Rotation getSpawnRotation(final LocalObjects local) {
		return getSpawnRotation();
	}

	@Override
	public T getTemplate() {
		return template;
	}

	@Override
	public boolean isSpawned() {
		return getSpawned() != null;
	}

	@Override
	public boolean isStarted() {
		return started.get();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void notifyFinish(final SpaceObject object) {
		notifyFinishImpl((O) object);
	}

	/**
	 * Реализации обработки завершения отспавненности объекта.
	 * 
	 * @param object объект, который перестал быть отспавненным.
	 */
	protected abstract void notifyFinishImpl(O object);

	/**
	 * @param spawned отспавненный объект.
	 */
	protected void setSpawned(final O spawned) {
		this.spawned = spawned;
	}

	@Override
	public void start(final LocalObjects local) {
		started.getAndSet(true);
		doSpawn(local);
	}

	@Override
	public void stop(final LocalObjects local) {
		started.getAndSet(false);
		doFinish(local);
	}
}

package com.ss.server.model.ship;

/**
 * Перечисление режимов работы генератора двигателя.
 * 
 * @author Ronn
 */
public enum GeneratorMode {
	ENGINE,
	FORCE_SHIELD,
	WEAPON;

	public static final GeneratorMode[] VALUES = values();

	public static final int LENGTH = VALUES.length;
}

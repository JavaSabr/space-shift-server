package com.ss.server.model.ship;

import rlib.geom.Vector;
import rlib.util.array.Array;
import rlib.util.pools.Foldable;
import rlib.util.table.IntKey;
import rlib.util.table.Table;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.ai.ShipAI;
import com.ss.server.model.func.stat.StatFunc;
import com.ss.server.model.func.stat.StatType;
import com.ss.server.model.impl.Env;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.state.SkillStateList;
import com.ss.server.model.storage.Storage;

/**
 * Интерфейс для реализации темплейта корабля.
 * 
 * @author Ronn
 */
public interface SpaceShip extends SpaceObject, Foldable {

	/**
	 * Добавление активности режиму генератора.
	 */
	public default void addActiveGenerator(final GeneratorMode mode) {
	}

	/**
	 * Добавление силового поля к кораблю.
	 * 
	 * @param forceShield силовое поле корабля.
	 */
	public default void addForceShield(final ForceShield forceShield) {
	}

	/**
	 * Добавление хейтера в список хейтеров корабля.
	 * 
	 * @param nps новый хейтер.
	 */
	public default void addHate(final Nps nps) {
	}

	/**
	 * Добавление умений модуля кораблю.
	 * 
	 * @param module модуль, от которого добавляется умения.
	 * @param added набор новых умений.
	 * @param local контейнер локальных объектов.
	 */
	public default void addSkills(final Module module, final Skill[] added, final LocalObjects local) {
	}

	/**
	 * Добавить функцию статов.
	 * 
	 * @param func добавляемвя функция.
	 */
	public default void addStatFunc(final StatFunc func) {
	}

	/**
	 * Рассчет значения стата.
	 * 
	 * @param type рассчитываемый стат.
	 * @param init начальное значение.
	 * @param env контейнер аргументов.
	 * @return итоговое значение стата.
	 */
	public default float calcStat(final StatType type, final int init, final Env env) {
		return 0;
	}

	/**
	 * Рассчет значения стата.
	 * 
	 * @param type рассчитываемый стат.
	 * @param init начальное значение.
	 * @param target целевой корабль.
	 * @param env контейнер аргументов.
	 * @return итоговое значение стата.
	 */
	public default float calcStat(final StatType type, final int init, final SpaceShip target, final Env env) {
		return 0;
	}

	/**
	 * Очистка листа хейтеров с удалением их агресси.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public default void clearHateList(final LocalObjects local) {
	}

	/**
	 * Обработка регерации объекта.
	 * 
	 * @param local локальные объекты.
	 * @param currentTime текущее время.
	 */
	public default void doRegen(final LocalObjects local, final long currentTime) {
	}

	/**
	 * @param location позиция корабля.
	 * @param skill используемый скил.
	 * @param local контейнер локальных объектов.
	 */
	public default void doSkillUse(final Vector location, final Skill skill, final LocalObjects local) {
	}

	/**
	 * Финиширование работы всех умений.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public default void finishSkills(LocalObjects local) {
	}

	/**
	 * @return ускорение корабля.
	 */
	public default float getAccel(final Env env) {
		return 0;
	}

	@Override
	public ShipAI getAI();

	/**
	 * @return общая прочность щита.
	 */
	public default int getCurrentShield() {
		return 0;
	}

	/**
	 * @return текущая скорость корабля.
	 */
	public default float getCurrentSpeed() {
		return 0;
	}

	/**
	 * @return текущая прочность корабля.
	 */
	public default int getCurrentStrength() {
		return 0;
	}

	/**
	 * @return распределение на двигатели.
	 */
	public default float getEngineEnergy() {
		return 0;
	}

	/**
	 * @return активные силовые поля.
	 */
	public default Array<ForceShield> getForceShields() {
		return null;
	}

	/**
	 * @return список хейтеров этого корабля.
	 */
	public default Array<Nps> getHateList() {
		return null;
	}

	/**
	 * @return уровень корабля.
	 */
	public default int getLevel() {
		return 0;
	}

	/**
	 * @param local контейнер локальных объектов.
	 * @return список хейтеров этого корабля.
	 */
	public default Array<Nps> getLocalHateList(final LocalObjects local) {
		return null;
	}

	/**
	 * @return максимальная прочность щита.
	 */
	public default int getMaxShield() {
		return 0;
	}

	/**
	 * @return максимальная прочность корабля.
	 */
	public default int getMaxStrength(final Env env) {
		return 0;
	}

	/**
	 * @return система модулей.
	 */
	public default ModuleSystem getModuleSystem() {
		return null;
	}

	/**
	 * @return название корабля.
	 */
	public default String getName() {
		return null;
	}

	/**
	 * @return следующий ид для выстрела.
	 */
	public default int getNextShotId() {
		return 0;
	}

	/**
	 * @return система рейтинга корабля.
	 */
	public default RatingSystem getRatingSystem() {
		return null;
	}

	/**
	 * Получение умения корабля указанного модуля.
	 * 
	 * @param objectId уникальный ид модуля.
	 * @param skillId ид шаблона скила.
	 * @return сам скил.
	 */
	public default Skill getSkill(final int objectId, final int skillId) {
		return null;
	}

	/**
	 * @return список всех умений.
	 */
	public Array<Skill> getSkills();

	/**
	 * Получения списка умений конкретного модуля корабля.
	 * 
	 * @param objectId уникальный ид модуля.
	 * @return список его умений.
	 */
	public default Skill[] getSkills(final int objectId) {
		return null;
	}

	/**
	 * @return список состояний всех уменикорабля.
	 */
	public SkillStateList getSkillStateList();

	/**
	 * Получить таблицу умений [ид модуля - умения модуля].
	 * 
	 * @return таблица умений.
	 */
	public default Table<IntKey, Skill[]> getSkillTable() {
		return null;
	}

	@Override
	public default SpaceShip getSpaceShip() {
		return this;
	}

	/**
	 * @return хранилище корабля.
	 */
	public default Storage getStorage() {
		return null;
	}

	/**
	 * @return ид типа корабля.
	 */
	public default int getTypeId() {
		return 0;
	}

	/**
	 * Прервать полет.
	 */
	public default void interruptFly() {
	}

	/**
	 * Активный ли указанный режим генератора.
	 * 
	 * @param mode режим генератора.
	 * @return активный ли указанный режим генератора.
	 */
	public default boolean isActiveGenerator(final GeneratorMode mode) {
		return false;
	}

	/**
	 * @return летит куда-нибудь ли сейчас корабль.
	 */
	public default boolean isFlying() {
		return false;
	}

	@Override
	public default boolean isSpaceShip() {
		return true;
	}

	/**
	 * @return поварачивает ли сейчас корабль.
	 */
	public default boolean isTurning() {
		return false;
	}

	/**
	 * Удаление активности режима генератора.
	 */
	public default void removeActiveGenerator(final GeneratorMode mode) {
	}

	/**
	 * Удаление силового поля из корабля.
	 * 
	 * @param forceShield силовое поле корабля.
	 */
	public default void removeForceShield(final ForceShield forceShield) {
	}

	/**
	 * Удаление неактуального хейтера из списка хейтеров.
	 * 
	 * @param nps неактуальный хейтер.
	 */
	public default void removeHate(final Nps nps) {
	}

	/**
	 * Удаление умений модуля.
	 * 
	 * @param module модуль которому принадлежат умения.
	 * @param skills удаляемы скилы.
	 * @param local контейнер локальных объектов.
	 */
	public default void removeSkills(final Module module, final Skill[] skills, final LocalObjects local) {
	}

	/**
	 * Удалить функсию статов.
	 * 
	 * @param func удаляемая функция.
	 */
	public default void removeStatFunc(final StatFunc func) {
	}

	/**
	 * @param message отправить сообщение от имени корабля.
	 * @param local контейнер локальных объектов.
	 */
	public default void sayMessage(final String message, final LocalObjects local) {
	}

	/**
	 * @param message отправить сис. сообщение кораблю.
	 * @param local контейнер локальных объектов.
	 */
	public default void sendMessage(final String message, final LocalObjects local) {
	}

	/**
	 * Отправка системного сообщения кораблю.
	 * 
	 * @param messageType тип сообщения.
	 * @param local контейнер локальных объектов.
	 */
	public default void sendSystemMessage(final SystemMessageType messageType, final LocalObjects local) {
	}

	/**
	 * Обновление состояния корпуса корабля.
	 * 
	 * @param currentStrength текущее состояние корпуса корабля.
	 * @param local контейнер локальных объектов.
	 */
	public default void setCurrentStrength(final int currentStrength, final LocalObjects local) {
	}

	/**
	 * Обновление текущего состояния всех щитов корабля.
	 * 
	 * @param currentShield текущее общее состояние.
	 * @param local контейнер локальных объектов.
	 */
	public default void setCurrentShield(final int currentShield, final LocalObjects local) {

	}

	/**
	 * @param engineEnergy распределение на двигатели.
	 */
	public default void setEngineEnergy(final float engineEnergy) {
	}

	/**
	 * @param name название корабля.
	 */
	public default void setName(final String name) {
	}

	/**
	 * @param storage хранилище корабля.
	 */
	public default void setStorage(final Storage storage) {
	}

	/**
	 * Обновление состояния энергии для корабля.
	 */
	public default void updateForceShieldStrength(final LocalObjects local) {
	}

	/**
	 * Обновление состояния для корабля.
	 */
	public default void updateStatus(final LocalObjects local) {
	}

	/**
	 * Обновление состояния прочности для корабля.
	 */
	public default void updateStrength(final LocalObjects local) {
	}
}

package com.ss.server.model.ship;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impact.ImpactInfo;

/**
 * Интерфейс для реализации силового щита.
 * 
 * @author Ronn
 */
public interface ForceShield {

	/**
	 * Отмена работы силового щита.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void cancel(LocalObjects local);

	/**
	 * @return текущая прочность щита.
	 */
	public int getCurrentStrength();

	/**
	 * @return максимальная прочность щита.
	 */
	public int getMaxStrength();

	/**
	 * @return активен ли сейчас силовой щит.
	 */
	public boolean isActive();

	/**
	 * @return был ли разрушен щит.
	 */
	public boolean isDestroyed();

	/**
	 * Обработка процесса поглащения урона.
	 * 
	 * @param effector объект, нанасивший урон.
	 * @param impactInfo информация об уроне.
	 * @param local контейнер локальных объектов.
	 * @return был ли поглащен урон.
	 */
	public boolean processAbsorb(SpaceObject effector, ImpactInfo impactInfo, LocalObjects local);
}

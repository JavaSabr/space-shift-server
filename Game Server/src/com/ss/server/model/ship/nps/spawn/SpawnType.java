package com.ss.server.model.ship.nps.spawn;

import rlib.util.VarTable;

import com.ss.server.model.ship.nps.spawn.impl.AreaNpsSpawn;
import com.ss.server.model.ship.nps.spawn.impl.DefaultNpsSpawn;
import com.ss.server.model.ship.nps.spawn.impl.RelativeNpsSpawn;
import com.ss.server.model.ship.nps.spawn.impl.StationDefenderSpawn;

/**
 * Перечисление типов спавнов.
 * 
 * @author Ronn
 */
public enum SpawnType {

	DEFAULT {

		@Override
		public NpsSpawn newSpawn(final VarTable vars) {
			return new DefaultNpsSpawn(vars);
		}
	},

	RELATIVE {

		@Override
		public NpsSpawn newSpawn(final VarTable vars) {
			return new RelativeNpsSpawn(vars);
		}
	},

	AREA {

		@Override
		public NpsSpawn newSpawn(final VarTable vars) {
			return new AreaNpsSpawn(vars);
		}
	},

	STATION_DEFENDER {

		@Override
		public NpsSpawn newSpawn(final VarTable vars) {
			return new StationDefenderSpawn(vars);
		}
	},

	;

	public NpsSpawn newSpawn(final VarTable vars) {
		return null;
	}
}

package com.ss.server.model.ship.nps.spawn.impl;

import rlib.util.VarTable;

/**
 * Реализация спавна с абсолютной позицией.
 * 
 * @author Ronn
 */
public class DefaultNpsSpawn extends AbstractNpsSpawn {

	public DefaultNpsSpawn(final VarTable vars) {
		super(vars);
	}
}

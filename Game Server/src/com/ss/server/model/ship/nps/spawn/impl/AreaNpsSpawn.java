package com.ss.server.model.ship.nps.spawn.impl;

import static rlib.geom.DirectionType.DIRECTION;

import java.util.concurrent.atomic.AtomicReference;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;

/**
 * Реализация спавна в области.
 * 
 * @author Ronn
 */
public class AreaNpsSpawn extends AbstractNpsSpawn {

	public static final String PROP_MAX_RADIUS = "maxRadius";
	public static final String PROP_MIN_RADIS = "minRadis";
	public static final String PROP_MAX_RADIUS_Z = "maxRadiusZ";
	public static final String PROP_MAX_RADIUS_Y = "maxRadiusY";
	public static final String PROP_MAX_RADIUS_X = "maxRadiusX";
	public static final String PROP_MIN_RADIUS_Z = "minRadiusZ";
	public static final String PROP_MIN_RADIUS_Y = "minRadiusY";
	public static final String PROP_MIN_RADIUS_X = "minRadiusX";

	/** ссылка на итоговую позицию спавна */
	protected final AtomicReference<Vector> locationReference;

	/** минимальный радиус спавна от позиции */
	protected int minRadius;
	/** максимальный радиус спавна от позиции */
	protected int maxRadius;

	/** минимальный ридус по X */
	protected int minRadiusX;
	/** минимальный ридус по Y */
	protected int minRadiusY;
	/** минимальный ридус по Z */
	protected int minRadiusZ;
	/** максимальный радиус по X */
	protected int maxRadiusX;
	/** максимальный радиус по Y */
	protected int maxRadiusY;
	/** максимальный радиус по Z */
	protected int maxRadiusZ;

	public AreaNpsSpawn(final VarTable vars) {
		super(vars);
		this.locationReference = new AtomicReference<Vector>();
		this.minRadius = vars.getInteger(PROP_MIN_RADIS, 0);
		this.maxRadius = vars.getInteger(PROP_MAX_RADIUS, 0);
		this.minRadiusX = vars.getInteger(PROP_MIN_RADIUS_X, 0);
		this.minRadiusY = vars.getInteger(PROP_MIN_RADIUS_Y, 0);
		this.minRadiusZ = vars.getInteger(PROP_MIN_RADIUS_Z, 0);
		this.maxRadiusX = vars.getInteger(PROP_MAX_RADIUS_X, 0);
		this.maxRadiusY = vars.getInteger(PROP_MAX_RADIUS_Y, 0);
		this.maxRadiusZ = vars.getInteger(PROP_MAX_RADIUS_Z, 0);
	}

	/**
	 * @return максимальный радиус спавна от позиции объекта.
	 */
	public int getMaxRadius() {
		return maxRadius;
	}

	/**
	 * @return максимальный радиус по X.
	 */
	public int getMaxRadiusX() {
		return maxRadiusX;
	}

	/**
	 * @return максимальный радиус по Y.
	 */
	public int getMaxRadiusY() {
		return maxRadiusY;
	}

	/**
	 * @return максимальный радиус по Z.
	 */
	public int getMaxRadiusZ() {
		return maxRadiusZ;
	}

	/**
	 * @return минимальный радиус спавна от позиции объекта.
	 */
	public int getMinRadius() {
		return minRadius;
	}

	/**
	 * @return минимальный ридус по X.
	 */
	public int getMinRadiusX() {
		return minRadiusX;
	}

	/**
	 * @return минимальный ридус по Y.
	 */
	public int getMinRadiusY() {
		return minRadiusY;
	}

	/**
	 * @return минимальный ридус по Z.
	 */
	public int getMinRadiusZ() {
		return minRadiusZ;
	}

	/**
	 * @return ссылка на итоговую позицию спавна.
	 */
	public AtomicReference<Vector> getLocationReference() {
		return locationReference;
	}

	@Override
	public Vector getSpawnLocation(final LocalObjects local) {

		final AtomicReference<Vector> reference = getLocationReference();

		if(reference.get() != null) {
			return reference.get();
		}

		final Vector location = getSpawnLocation();
		final Vector result = Vector.newInstance();

		final int maxRadius = getMaxRadius();
		final int minRadius = getMinRadius();
		final int maxRadiusX = getMaxRadiusX();
		final int maxRadiusY = getMaxRadiusY();
		final int maxRadiusZ = getMaxRadiusZ();
		final int minRadiusX = getMinRadiusX();
		final int minRadiusY = getMinRadiusY();
		final int minRadiusZ = getMinRadiusZ();

		if(maxRadius != 0 || minRadius != 0) {

			final Array<SpaceObject> objects = local.getNextObjectList();

			final Vector direction = local.getNextVector();

			final Random random = LOCAL_RANDOM.get();

			for(int i = 0, length = 500; i < length; i++, objects.clear()) {

				final int distance = random.nextInt(minRadius, maxRadius);

				final Rotation rotation = local.getNextRotation();
				rotation.random(random);
				rotation.getVectorDirection(DIRECTION, direction);

				direction.multLocal(distance);

				result.set(location);
				result.addLocal(direction);

				SPACE.addAround(SpaceObject.class, result, objects, getLocationId());

				boolean collision = false;

				for(final SpaceObject object : objects.array()) {

					if(object == null) {
						break;
					}

					if(!(object.isLocationObject() || object.isGravityObject())) {
						continue;
					}

					if(object.isInDistance(result, object.getMaxSize())) {
						collision = true;
						break;
					}
				}

				if(!collision) {
					break;
				}
			}

		} else if(maxRadiusX != 0 || maxRadiusY != 0 || maxRadiusZ != 0 || minRadiusX != 0 || minRadiusY != 0 || minRadiusZ != 0) {

			final Array<SpaceObject> objects = local.getNextObjectList();

			final Vector direction = local.getNextVector();
			final Vector distance = local.getNextVector();

			final Random random = LOCAL_RANDOM.get();

			for(int i = 0, length = 500; i < length; i++, objects.clear()) {

				int diff = maxRadiusX - minRadiusX;

				distance.setX(random.nextInt(minRadiusX + diff, maxRadiusX + diff) - diff);

				diff = maxRadiusY - minRadiusY;

				distance.setY(random.nextInt(minRadiusY + diff, maxRadiusY + diff) - diff);

				diff = maxRadiusZ - minRadiusZ;

				distance.setZ(random.nextInt(minRadiusZ + diff, maxRadiusZ + diff) - diff);

				final Rotation rotation = local.getNextRotation();
				rotation.random(random);
				rotation.getVectorDirection(DIRECTION, direction);

				direction.multLocal(distance);

				result.set(location);
				result.addLocal(direction);

				SPACE.addAround(SpaceObject.class, result, objects, getLocationId());

				boolean collision = false;

				for(final SpaceObject object : objects.array()) {

					if(object == null) {
						break;
					}

					if(!(object.isLocationObject() || object.isGravityObject())) {
						continue;
					}

					if(object.isInDistance(result, object.getMaxSize())) {
						collision = true;
						break;
					}
				}

				if(!collision) {
					break;
				}
			}

		} else {
			result.set(location);
		}

		reference.set(result);

		return result;
	}
}

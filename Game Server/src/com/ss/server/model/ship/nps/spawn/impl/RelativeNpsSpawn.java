package com.ss.server.model.ship.nps.spawn.impl;

import rlib.geom.DirectionType;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.impl.Space;
import com.ss.server.model.ship.nps.Nps;

/**
 * Реализация относительного спавна.
 * 
 * @author Ronn
 */
public class RelativeNpsSpawn extends AbstractNpsSpawn {

	public static final String MAX_RADIUS = "maxRadius";
	public static final String MIN_RADIS = "minRadis";
	public static final String OBJECT_ID = "objectId";

	protected static final Space SPACE = Space.getInstance();

	/** объект, относительного которого происходит спавн */
	protected GravityObject spawnTarget;

	/** уникальный ид объекта, относительно которого идет спавн */
	protected int objectId;

	/** минимальный радиус спавна от позиции объекта */
	protected int minRadius;
	/** максимальный радиус спавна от позиции объекта */
	protected int maxRadius;

	public RelativeNpsSpawn(final VarTable vars) {
		super(vars);

		this.objectId = vars.getInteger(OBJECT_ID);
		this.minRadius = vars.getInteger(MIN_RADIS, 0);
		this.maxRadius = vars.getInteger(MAX_RADIUS, 0);
	}

	@Override
	protected void doSpawn(final LocalObjects local) {

		final GravityObject spawnTarget = getSpawnTarget();

		if(spawnTarget == null) {
			throw new RuntimeException("not found spawnTarget for objectId " + getObjectId());
		}

		super.doSpawn(local);
	}

	/**
	 * @return максимальный радиус спавна от позиции объекта.
	 */
	public int getMaxRadius() {
		return maxRadius;
	}

	/**
	 * @return минимальный радиус спавна от позиции объекта.
	 */
	public int getMinRadius() {
		return minRadius;
	}

	/**
	 * @return уникальный ид объекта, относительно которого идет спавн.
	 */
	public int getObjectId() {
		return objectId;
	}

	@Override
	public Vector getSpawnLocation(final LocalObjects local) {

		final GravityObject spawnTarget = getSpawnTarget();

		if(spawnTarget == null) {
			throw new RuntimeException("not found spawnTarget for objectId " + getObjectId());
		}

		final Random random = LOCAL_RANDOM.get();

		final Rotation rotation = local.getNextRotation();
		rotation.random();

		final Vector direction = rotation.getVectorDirection(DirectionType.DIRECTION, local.getNextVector());
		direction.multLocal(random.nextInt(getMinRadius(), getMaxRadius()));

		final Vector spawnLocation = local.getNextVector();
		spawnLocation.set(spawnTarget.getLocation()).addLocal(direction);

		return spawnLocation;
	}

	/**
	 * @return объект, относительного которого происходит спавн.
	 */
	public GravityObject getSpawnTarget() {

		if(spawnTarget == null) {
			spawnTarget = SPACE.getGravityObject(getObjectId());
		}

		return spawnTarget;
	}

	@Override
	protected Nps processSpawn(final LocalObjects local) {

		final Nps spawned = super.processSpawn(local);

		if(spawned != null) {

			final GravityObject spawnTarget = getSpawnTarget();
			spawned.setSpawnLoc(spawnTarget.getLocation());

			final NpsAI ai = spawned.getAI();
			ai.setProtectedObject(getSpawnTarget());
		}

		return spawned;
	}
}

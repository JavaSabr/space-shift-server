package com.ss.server.model.ship.nps.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.EnemyNps;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Базовая реализация НПС агрессивно настроенного против игроков.
 * 
 * @author Ronn
 */
public abstract class AbstractEnemyNps extends AbstractNps implements EnemyNps {

	public AbstractEnemyNps(final int objectId, final NpsTemplate template) {
		super(objectId, template);
	}

	@Override
	public boolean canAttack(final SpaceObject object) {
		return super.canAttack(object) && checkAgression(object);
	}

	@Override
	public boolean checkAgression(final SpaceObject object) {

		final SpaceShip ship = object.getSpaceShip();

		if(ship == null || ship.isDestructed()) {
			return false;
		}

		if(ship.isNps()) {
			final Nps nps = ship.getNps();
			return nps.isFriendlyNps();
		}

		return false;// ship.isPlayerShip();
	}
}

package com.ss.server.model.ship.nps;

/**
 * Интерфейс для пометки НПС о том, что он настроен за игроков.
 * 
 * @author Ronn
 */
public interface FriendlyNps extends Nps {

	@Override
	public default boolean isFriendlyNps() {
		return true;
	}
}

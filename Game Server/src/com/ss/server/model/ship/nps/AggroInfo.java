package com.ss.server.model.ship.nps;

import rlib.util.array.Array;
import rlib.util.pools.Foldable;

import com.ss.server.model.ship.SpaceShip;

/**
 * Информация о аггре на нейкий объект.
 * 
 * @author Ronn
 */
public class AggroInfo implements Foldable {

	/**
	 * Поиск агрессора среди списка информаций.
	 * 
	 * @param object искомый агрессор.
	 * @param aggroList список информаций о арессорах.
	 * @return информация о указанном агрессоре.
	 */
	public static final AggroInfo find(final SpaceShip object, final Array<AggroInfo> aggroList) {

		if(aggroList.isEmpty()) {
			return null;
		}

		for(final AggroInfo info : aggroList.array()) {

			if(info == null) {
				break;
			}

			if(info.getObject() == object) {
				return info;
			}
		}

		return null;
	}

	public static SpaceShip findMostDamager(final Array<AggroInfo> aggroList) {

		if(aggroList.isEmpty()) {
			return null;
		}

		SpaceShip mostDamager = null;
		long damage = 0;

		aggroList.readLock();
		try {

			for(final AggroInfo info : aggroList.array()) {

				if(info == null) {
					break;
				}

				if(info.getDamage() > damage) {
					damage = info.getDamage();
					mostDamager = info.getObject();
				}
			}

			return mostDamager;

		} finally {
			aggroList.readUnlock();
		}
	}

	public static SpaceShip findMostHated(final Array<AggroInfo> aggroList) {

		if(aggroList.isEmpty()) {
			return null;
		}

		SpaceShip mostHated = null;
		long hate = 0;

		aggroList.readLock();
		try {

			for(final AggroInfo info : aggroList.array()) {

				if(info == null) {
					break;
				}

				if(info.getHate() > hate) {
					hate = info.getHate();
					mostHated = info.getObject();
				}
			}

			return mostHated;

		} finally {
			aggroList.readUnlock();
		}
	}

	/** агрессор */
	private SpaceShip object;

	/** нанесенный урон агрессором */
	private long damage;
	/** уровень агрессии */
	private long hate;

	/**
	 * @param damage добавление урона.
	 */
	public void addDamage(final long damage) {
		this.damage += damage;
	}

	/**
	 * @param hate добавление агрессии.
	 */
	public void addHate(final long hate) {
		this.hate += hate;
	}

	@Override
	public void finalyze() {
		setObject(null);
		setDamage(0);
		setHate(0);
	}

	/**
	 * @return нанесенный урон агрессором.
	 */
	public long getDamage() {
		return damage;
	}

	/**
	 * @return уровень агрессии.
	 */
	public long getHate() {
		return hate;
	}

	/**
	 * @return агрессор.
	 */
	public SpaceShip getObject() {
		return object;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param damage нанесенный урон агрессором.
	 */
	public void setDamage(final long damage) {
		this.damage = damage;
	}

	/**
	 * @param hate уровень агрессии.
	 */
	public void setHate(final long hate) {
		this.hate = hate;
	}

	/**
	 * @param object агрессор.
	 */
	public void setObject(final SpaceShip object) {
		this.object = object;
	}
}

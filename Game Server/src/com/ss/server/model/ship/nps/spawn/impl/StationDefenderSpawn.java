package com.ss.server.model.ship.nps.spawn.impl;

import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.random.Random;
import rlib.util.random.RandomFactory;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.model.impl.Space;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.nps.StationDefenderNps;
import com.ss.server.model.station.SpaceStation;

/**
 * Реализация спавна защитника станции.
 * 
 * @author Ronn
 */
public class StationDefenderSpawn extends AbstractNpsSpawn {

	public static final String PROP_STATION_ID = "stationId";

	private static final Space SPACE = Space.getInstance();

	/** защищаемая станция */
	private final SpaceStation station;

	public StationDefenderSpawn(final VarTable vars) {
		super(vars);

		this.station = SPACE.getContainer(vars.getInteger(PROP_STATION_ID), Config.SERVER_STATION_CLASS_ID);

		if(station == null) {
			throw new RuntimeException("not found station from id " + vars.getInteger(PROP_STATION_ID));
		}
	}

	@Override
	@SuppressWarnings("unused")
	public Vector getSpawnLocation(final LocalObjects local) {

		final SpaceStation station = getStation();

		final Vector spawnLocation = local.getNextVector();
		spawnLocation.set(station.getLocation()).addLocal(getSpawnLocation());

		if(false) {

			System.out.println("-----------------------------------------------");
			final int count = 3 * 3 * 3;

			final int length = 1500;
			final int width = 1500;
			final int height = 1500;

			final Array<Vector> vectors = ArrayFactory.newArraySet(Vector.class);

			final Random random = RandomFactory.newFastRandom();

			for(int i = 0; i < count; i++) {

				final Vector vector = Vector.newInstance();

				do {

					int v = random.nextInt(3);

					if(v == 0) {
						vector.setX(0);
					} else if(v == 1) {
						vector.setX(width);
					} else if(v == 2) {
						vector.setX(-width);
					}

					v = random.nextInt(3);

					if(v == 0) {
						vector.setY(0);
					} else if(v == 1) {
						vector.setY(height);
					} else if(v == 2) {
						vector.setY(-height);
					}

					v = random.nextInt(3);

					if(v == 0) {
						vector.setZ(0);
					} else if(v == 1) {
						vector.setZ(length);
					} else if(v == 2) {
						vector.setZ(-length);
					}

				} while(vectors.contains(vector));

				System.out.println("<position x=\"" + vector.getX() + "\" y=\"" + vector.getY() + "\" z=\"" + vector.getZ() + "\" />");

				vectors.add(vector);
			}

		}

		return spawnLocation;
	}

	/**
	 * @return защищаемая станция.
	 */
	public SpaceStation getStation() {
		return station;
	}

	@Override
	protected Nps processSpawn(final LocalObjects local) {

		final Nps spawned = super.processSpawn(local);

		if(!spawned.isStationDefender()) {
			throw new RuntimeException("incorrect nps class");
		}

		final StationDefenderNps defenderNps = (StationDefenderNps) spawned;
		defenderNps.setProtectedStation(station);

		return spawned;
	}
}

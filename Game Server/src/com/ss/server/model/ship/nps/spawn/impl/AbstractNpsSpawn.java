package com.ss.server.model.ship.nps.spawn.impl;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.random.Random;

import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAiClass;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.nps.spawn.NpsSpawn;
import com.ss.server.model.spawn.impl.AbstractSpawn;
import com.ss.server.template.ModuleTemplate;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Базовая реализация спавна НПС.
 * 
 * @author Ronn
 */
public abstract class AbstractNpsSpawn extends AbstractSpawn<NpsTemplate, Nps> implements NpsSpawn {

	/** шаблоны модулей Nps */
	protected final ModuleTemplate[] modules;

	/** конфигурация Ai Nps */
	protected final ConfigAi configAi;
	/** класс для создания Ai Nps */
	protected final NpsAiClass aiClass;

	/** разрушенный NPS */
	protected volatile Nps destruct;

	/** рандоминайзер времени респавна */
	protected int randomRespawnTime;

	public AbstractNpsSpawn(final VarTable vars) {
		super(vars);
		this.modules = vars.get(MODULES, ModuleTemplate[].class);
		this.randomRespawnTime = vars.getInteger(RANDOM_RESPAWN_TIME, 0);
		this.configAi = vars.get(CONFIG_AI, ConfigAi.class);
		this.aiClass = vars.getEnum(AI_CLASS, NpsAiClass.class);
	}

	/**
	 * @return новый NPS для спавна.
	 */
	protected Nps createNps(final LocalObjects local) {

		final IdFactory idFactory = IdFactory.getInstance();

		final NpsTemplate template = getTemplate();

		final Nps nps = template.takeInstance(idFactory.getNextNpsId());
		final ModuleSystem system = nps.getModuleSystem();

		for(final ModuleTemplate moduleTemplate : getModules()) {

			final Module module = moduleTemplate.takeInstance(idFactory.getNextModuleId());

			if(!system.addModule(module, local)) {
				LOGGER.warning(getClass(), "can't add module " + moduleTemplate + " to " + nps.getTemplate());
			}
		}

		final NpsAiClass aiClass = getAiClass();
		nps.setAI(aiClass.newInstance(nps, getConfigAi()));

		return nps;
	}

	@Override
	protected void doFinish(final LocalObjects local) {

		final Nps spawned = getSpawned();

		if(spawned != null) {
			spawned.setCurrentStrength(0, local);
			spawned.doDestruct(spawned, local);
		}
	}

	/**
	 * Старт процесса респавна.
	 */
	protected void doRespawn() {

		if(!started.get()) {
			return;
		}

		if(scheduleSpawn != null) {
			scheduleSpawn.cancel(false);
		}

		scheduleSpawn = EXECUTOR_MANAGER.scheduleGeneral(getRespawnTask(), getRespawnTime(LocalObjects.get()));
	}

	@Override
	protected void doSpawn(final LocalObjects local) {

		if(!started.get()) {
			return;
		}

		synchronized(started) {

			if(!started.get()) {
				return;
			}

			processSpawn(local);
		}
	}

	@Override
	public NpsAiClass getAiClass() {
		return aiClass;
	}

	@Override
	public ConfigAi getConfigAi() {
		return configAi;
	}

	@Override
	public Nps getDestruct() {
		return destruct;
	}

	@Override
	public int getLocationId() {
		return locationId;
	}

	@Override
	public ModuleTemplate[] getModules() {
		return modules;
	}

	/**
	 * @return рандомная часть времени спавна.
	 */
	public int getRandomRespawnTime() {
		return randomRespawnTime;
	}

	@Override
	public long getRespawnTime(final LocalObjects local) {

		final int respawnTime = getRespawnTime();
		final int randomRespawnTime = getRandomRespawnTime();

		if(randomRespawnTime == 0) {
			return respawnTime;
		}

		final Random random = LOCAL_RANDOM.get();

		long time = respawnTime - randomRespawnTime;
		time = random.nextLong(time, respawnTime + randomRespawnTime);

		return time;
	}

	@Override
	protected void notifyFinishImpl(final Nps object) {
		setSpawned(null);
		setDestruct(object);
		doRespawn();
	}

	protected Nps processSpawn(final LocalObjects local) {

		final Nps spawned = getSpawned();

		if(spawned != null) {
			throw new RuntimeException("nps is spawned.");
		}

		Nps destruct = getDestruct();

		if(destruct == null) {
			destruct = createNps(local);
		}

		final Vector location = getSpawnLocation(local);
		final Rotation rotation = getSpawnRotation(local);

		destruct.setRotation(rotation, local);
		destruct.setLocationId(getLocationId());
		destruct.setSpawn(this);
		destruct.getSpawnLoc().set(location);
		destruct.setEngineEnergy(EnergyModule.ENERGY_POWER_100);
		destruct.setCurrentStrength(Integer.MAX_VALUE, local);
		destruct.spawnMe(local, location.getX(), location.getY(), location.getZ());

		setDestruct(null);
		setSpawned(destruct);

		return destruct;
	}

	/**
	 * @param destruct разрушенный NPS.
	 */
	protected void setDestruct(final Nps destruct) {
		this.destruct = destruct;
	}
}

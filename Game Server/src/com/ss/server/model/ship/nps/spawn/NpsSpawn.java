package com.ss.server.model.ship.nps.spawn;

import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAiClass;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.spawn.Spawn;
import com.ss.server.template.ModuleTemplate;

/**
 * Интерфейс для реализации спавна НПС.
 * 
 * @author Ronn
 */
public interface NpsSpawn extends Spawn {

	public static final String TEMPLATE = "template";
	public static final String MODULES = "modules";

	public static final String LOCATION_ID = "locationId";
	public static final String RANDOM_RESPAWN_TIME = "randomRespawnTime";
	public static final String RESPAWN_TIME = "respawnTime";
	public static final String ROTATION = "rotation";
	public static final String LOCATION = "location";

	public static final String AI_CLASS = "ai_class";
	public static final String CONFIG_AI = "config_ai";

	/**
	 * @return используемый класс Ai Nps.
	 */
	public NpsAiClass getAiClass();

	/**
	 * @return конфигурация Ai Nps.
	 */
	public ConfigAi getConfigAi();

	/**
	 * @return разрушенный NPS.
	 */
	public Nps getDestruct();

	/**
	 * @return набор шаблонов модулей.
	 */
	public ModuleTemplate[] getModules();
}

package com.ss.server.model.ship.nps.impl;

import com.ss.server.LocalObjects;
import com.ss.server.model.FriendStatus;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Реализация дружественного дрона.
 * 
 * @author Ronn
 */
public class FriendlyDron extends AbstractFriendlyNps {

	public FriendlyDron(final int objectId, final NpsTemplate template) {
		super(objectId, template);
	}

	@Override
	public FriendStatus getFriendStatus(final PlayerShip ship, final LocalObjects local) {
		return FriendStatus.PARTY;
	}
}

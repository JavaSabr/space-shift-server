package com.ss.server.model.ship.nps;

import rlib.geom.Vector;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.spawn.NpsSpawn;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Интерфейс для реализации кораблей не игроков(Non Player Ship).
 * 
 * @author Ronn
 */
public interface Nps extends SpaceShip {

	/**
	 * Добавление агрессии указанного объекта.
	 * 
	 * @param object агрессор.
	 * @param damage урон от агрессора.
	 * @param hate сила агрессии.
	 * @param local контейнер локальных объектов.
	 */
	public void addAggro(SpaceShip object, long damage, long hate, LocalObjects local);

	/**
	 * Проверка возможности арессии Nps на указанный объект.
	 * 
	 * @param object проверяемый объект.
	 * @return есть ли возможность проявить агрессию.
	 */
	public boolean checkAgression(SpaceObject object);

	/**
	 * Очистка списка агрессии.
	 */
	public void clearAggroList();

	/**
	 * @return спиоск информаций о агрессорах.
	 */
	public Array<AggroInfo> getAggroList();

	@Override
	public NpsAI getAI();

	/**
	 * @return главный демагер агрессор.
	 */
	public SpaceShip getMostDamager();

	/**
	 * @return главный агрессор.
	 */
	public SpaceShip getMostHated();

	/**
	 * @return спавн NPS.
	 */
	public NpsSpawn getSpawn();

	/**
	 * @return позиция спавна.
	 */
	public Vector getSpawnLoc();

	@Override
	public NpsTemplate getTemplate();

	/**
	 * Является ли этот НПС вражеским для игроков.
	 */
	public default boolean isEnemyNps() {
		return false;
	}

	/**
	 * Является ли этот НПС дружественным для игроков.
	 */
	public default boolean isFriendlyNps() {
		return false;
	}

	@Override
	public default boolean isNps() {
		return true;
	}

	/**
	 * @return является ли НПС защитником станции.
	 */
	public default boolean isStationDefender() {
		return false;
	}

	/**
	 * Удаление из списка агрессий объекта.
	 * 
	 * @param object удаляемый из списка агрессий объект.
	 * @param local контейнер локальных объектов.
	 */
	public void removeAggro(SpaceShip object, LocalObjects local);

	/**
	 * Установка Ai для Nps.
	 * 
	 * @param ai новый Ai.
	 */
	public void setAI(NpsAI ai);

	/**
	 * @param mostDamager главный демагер агрессор.
	 */
	public void setMostDamager(SpaceShip mostDamager);

	/**
	 * @param mostHated главный агрессор.
	 */
	public void setMostHated(SpaceShip mostHated);

	/**
	 * @param spawn спавн NPS.
	 */
	public void setSpawn(NpsSpawn spawn);

	/**
	 * Установка позиции которая является центром спавна НПС.
	 * 
	 * @param spawnLoc позиция которая является центром спавна НПС.
	 */
	public void setSpawnLoc(Vector spawnLoc);

	/**
	 * Отспавн NPS в указанной позиции с запоминанием ссылки на нее.
	 * 
	 * @param spawnLoc позиция для спавна.
	 * @param local контейнер локальных объектов.
	 */
	public void spawnMe(Vector spawnLoc, LocalObjects local);
}

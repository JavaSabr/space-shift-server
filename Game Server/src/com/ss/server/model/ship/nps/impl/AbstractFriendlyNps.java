package com.ss.server.model.ship.nps.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.nps.FriendlyNps;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Базовая реализация НПС дружественно настроенного против игроков.
 * 
 * @author Ronn
 */
public abstract class AbstractFriendlyNps extends AbstractNps implements FriendlyNps {

	public AbstractFriendlyNps(final int objectId, final NpsTemplate template) {
		super(objectId, template);
	}

	@Override
	public boolean canAttack(final SpaceObject object) {
		return super.canAttack(object) && checkAgression(object);
	}

	@Override
	public boolean checkAgression(final SpaceObject object) {

		if(!object.isNps()) {
			return false;
		}

		final Nps nps = object.getNps();

		if(nps.isDestructed()) {
			return false;
		}

		return nps.isEnemyNps();
	}
}

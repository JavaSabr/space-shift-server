package com.ss.server.model.ship.nps;

/**
 * Интерфейс для пометки НПС о том, что он настроен против игроков.
 * 
 * @author Ronn
 */
public interface EnemyNps extends Nps {

	@Override
	public default boolean isEnemyNps() {
		return true;
	}
}

package com.ss.server.model.ship.nps.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.nps.StationDefenderNps;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Базоваяреализация NPS защитника станции.
 * 
 * @author Ronn
 */
public class DefaultStationDefenderNps extends AbstractNps implements StationDefenderNps {

	/** защищемая станция */
	protected SpaceStation station;

	public DefaultStationDefenderNps(final int objectId, final NpsTemplate template) {
		super(objectId, template);
	}

	@Override
	public boolean canAttack(final SpaceObject object) {
		return super.canAttack(object) && checkAgression(object);
	}

	@Override
	public boolean checkAgression(final SpaceObject object) {

		if(!object.isSpaceShip()) {
			return false;
		}

		if(object.isPlayerShip()) {
			final PlayerShip playerShip = object.getPlayerShip();
			return !playerShip.isDestructed();
		}

		final Nps nps = object.getNps();

		if(nps.isDestructed()) {
			return false;
		}

		return !nps.isStationDefender();
	}

	@Override
	public SpaceStation getProtectedStation() {
		return station;
	}

	@Override
	public void setProtectedStation(final SpaceStation station) {
		this.station = station;
	}
}

package com.ss.server.model.ship.nps;

import com.ss.server.model.station.SpaceStation;

/**
 * Интерфейс для реализации NPS защитника космической станции.
 * 
 * @author Ronn
 */
public interface StationDefenderNps extends Nps {

	/**
	 * @return защищаемая станция.
	 */
	public SpaceStation getProtectedStation();

	@Override
	public default boolean isStationDefender() {
		return true;
	}

	/**
	 * @param station защищаемая станция.
	 */
	public void setProtectedStation(SpaceStation station);
}

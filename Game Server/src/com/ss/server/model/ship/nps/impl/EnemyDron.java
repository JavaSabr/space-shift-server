package com.ss.server.model.ship.nps.impl;

import com.ss.server.LocalObjects;
import com.ss.server.model.FriendStatus;
import com.ss.server.model.ship.nps.AggroInfo;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Реализация боевого дрона.
 * 
 * @author Ronn
 */
public class EnemyDron extends AbstractEnemyNps {

	public EnemyDron(final int objectId, final NpsTemplate template) {
		super(objectId, template);
	}

	@Override
	public FriendStatus getFriendStatus(final PlayerShip ship, final LocalObjects local) {

		final AggroInfo info = AggroInfo.find(ship, getAggroList());

		if(info == null) {
			return FriendStatus.NEUTRAL;
		}

		return FriendStatus.AGGRESSOR;
	}
}

package com.ss.server.model.ship.nps.impl;

import com.ss.server.template.ship.NpsTemplate;

/**
 * Базовый вариант рализации Nps.
 * 
 * @author Ronn
 */
public class DefaultNps extends AbstractNps {

	public DefaultNps(final int objectId, final NpsTemplate template) {
		super(objectId, template);
	}
}

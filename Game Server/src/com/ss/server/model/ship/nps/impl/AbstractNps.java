package com.ss.server.model.ship.nps.impl;

import java.util.function.Consumer;

import rlib.geom.Vector;
import rlib.idfactory.IdGenerator;
import rlib.idfactory.IdGeneratorFactory;
import rlib.util.SafeTask;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.executor.GameExecutor;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.manager.ShipEventManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.impl.SpaceSector;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.impl.NpsRatingSystem;
import com.ss.server.model.ship.AbstractSpaceShip;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.AggroInfo;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.nps.spawn.NpsSpawn;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.skills.state.SkillStateInfo;
import com.ss.server.model.skills.state.SkillStateList;
import com.ss.server.network.game.packet.server.ResponseNpsInfo;
import com.ss.server.task.impl.UpdateNpsAITask;
import com.ss.server.template.ship.NpsTemplate;

/**
 * Базовая реализация кораблей не игроков.
 * 
 * @author Ronn
 */
public abstract class AbstractNps extends AbstractSpaceShip<NpsTemplate> implements Nps {

	private static final IdGenerator SHOT_ID_GENERATOR = IdGeneratorFactory.newSimpleIdGenerator(300000, 600000);

	private static final ShipEventManager SHIP_EVENT_MANAGER = ShipEventManager.getInstance();

	protected final FoldablePool<AggroInfo> aggroInfoPool = PoolFactory.newAtomicFoldablePool(AggroInfo.class);

	protected final Consumer<AggroInfo> aggroInfoFoldFunc = info -> {

		final SpaceShip object = info.getObject();

		if(object != null) {
			object.removeHate(this);
		}

		aggroInfoPool.put(info);
	};

	protected final SafeTask finishDestroyTask = () -> finishDestruct(LocalObjects.get());

	/** список агрессоров */
	protected final Array<AggroInfo> aggroList;

	/** спавн NPS */
	protected NpsSpawn spawn;

	/** главный демагер агрессор */
	protected SpaceShip mostDamager;
	/** главный агрессор */
	protected SpaceShip mostHated;

	/** позиция спавна */
	protected Vector spawnLoc;

	/** название окрабля */
	protected String name;

	public AbstractNps(final int objectId, final NpsTemplate template) {
		super(objectId, template);

		this.aggroList = ArrayFactory.newConcurrentAtomicArray(AggroInfo.class);

		setName(template.getName());
	}

	@Override
	protected RatingSystem createRatingSystem() {
		return new NpsRatingSystem(this);
	}

	@Override
	public void addAggro(final SpaceShip object, final long damage, final long hate, final LocalObjects local) {

		final Array<AggroInfo> aggroList = getAggroList();

		try {

			aggroList.readLock();
			try {

				final AggroInfo info = AggroInfo.find(object, aggroList);

				if(info != null) {
					info.addDamage(damage);
					info.addHate(hate);
					return;
				}

			} finally {
				aggroList.readUnlock();
			}

			boolean added = false;

			aggroList.writeLock();
			try {

				AggroInfo info = AggroInfo.find(object, aggroList);

				if(info != null) {
					info.addDamage(damage);
					info.addHate(hate);
				} else if(!isDestructed()) {

					info = getNewAggroInfo();
					info.setObject(object);
					info.setDamage(damage);
					info.setHate(hate);

					added = true;

					aggroList.add(info);
				}

			} finally {
				aggroList.writeUnlock();
			}

			if(added) {
				object.addHate(this);
				SHIP_EVENT_MANAGER.notifyAggro(this, object, local);
			}

		} finally {
			setMostHated(null);
			setMostDamager(null);
		}
	}

	@Override
	public void addMe(final PlayerShip playerShip, final LocalObjects local) {

		playerShip.sendPacket(ResponseNpsInfo.getInstance(playerShip, this, local), true);

		super.addMe(playerShip, local);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("add me " + this + " to " + playerShip);
		}
	}

	@Override
	public void addToUpdate(final SpaceLocation location) {
		super.addToUpdate(location);

		final GameExecutor<UpdateNpsAITask> executor = location.getAiExecutor();
		executor.addTask(getAI().getUpdateTask());
	}

	@Override
	public boolean checkAgression(final SpaceObject object) {
		return false;
	}

	@Override
	public void clearAggroList() {

		final Array<AggroInfo> aggroList = getAggroList();
		aggroList.writeLock();
		try {
			aggroList.forEach(aggroInfoFoldFunc);
			aggroList.clear();
		} finally {
			aggroList.writeUnlock();
		}

		setMostHated(null);
		setMostDamager(null);
	}

	@Override
	public void decayMe(final LocalObjects local) {

		final NpsAI ai = getAI();

		if(ai != null) {
			ai.stopAITasks();
		}

		super.decayMe(local);
	}

	@Override
	public void doDestruct(final SpaceObject destroyer, final LocalObjects local) {
		super.doDestruct(destroyer, local);

		processReward(destroyer, local);

		clearAggroList();
		clearSkillStates();

		final ExecutorManager executorManager = ExecutorManager.getInstance();
		executorManager.scheduleGeneral(finishDestroyTask, 100);
	}

	@Override
	protected void clearSkillStates() {

		final SkillStateList skillStateList = getSkillStateList();

		final Array<SkillStateInfo> skillStates = skillStateList.getActualStates();

		for(SkillStateInfo skillState : skillStates.array()) {

			if(skillState == null) {
				break;
			}

			skillState.setReloadFinishTime(0);
			skillState.setReloadTime(0);
			skillState.setCharge(skillState.getMaxCharge());
		}
	}

	/**
	 * Завершение разрушения.
	 */
	protected void finishDestruct(final LocalObjects local) {
		decayMe(local);
		getSpawn().notifyFinish(this);
	}

	@Override
	public Array<AggroInfo> getAggroList() {
		return aggroList;
	}

	@Override
	public NpsAI getAI() {
		return (NpsAI) ai;
	}

	@Override
	public int getClassId() {
		return Config.SERVER_NPS_CLASS_ID;
	}

	@Override
	public int getLevel() {
		return template.getLevel();
	}

	@Override
	public SpaceShip getMostDamager() {

		if(mostDamager == null) {
			mostDamager = AggroInfo.findMostDamager(getAggroList());
		}

		return mostDamager;
	}

	@Override
	public SpaceShip getMostHated() {

		if(mostHated == null) {
			mostHated = AggroInfo.findMostHated(getAggroList());
		}

		return mostHated;
	}

	@Override
	public String getName() {
		return name;
	}

	/**
	 * @return новый контейнер для хранения агрессии.
	 */
	protected AggroInfo getNewAggroInfo() {

		AggroInfo info = aggroInfoPool.take();

		if(info == null) {

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "create aggro info");
			}

			info = new AggroInfo();
		}

		return info;
	}

	@Override
	public int getNextShotId() {
		return SHOT_ID_GENERATOR.getNextId();
	}

	@Override
	public Nps getNps() {
		return this;
	}

	@Override
	public NpsSpawn getSpawn() {
		return spawn;
	}

	@Override
	public Vector getSpawnLoc() {

		if(spawnLoc == null) {
			spawnLoc = Vector.newInstance();
		}

		return spawnLoc;
	}

	@Override
	public boolean isNeedSendPacket() {

		final SpaceSector sector = getCurrentSector();

		if(sector != null) {
			return sector.isActive();
		}

		return false;
	}

	/**
	 * Рассчет награды за уничтожение NPS.
	 * 
	 * @param destroyer уничтожитель.
	 * @param local контейнер локальных объектов.
	 */
	protected void processReward(final SpaceObject destroyer, final LocalObjects local) {

		if(!destroyer.isPlayerShip()) {
			return;
		}

		final SpaceShip damager = getMostDamager();

		if(damager == null || !damager.isPlayerShip()) {
			return;
		}

		final PlayerShip playerShip = damager.getPlayerShip();

		final int exp = template.getExp();

		if(exp > 0) {
			playerShip.addExp(exp, this, local);
		}
	}

	@Override
	public void removeAggro(final SpaceShip object, final LocalObjects local) {

		boolean removed = false;

		final Array<AggroInfo> aggroList = getAggroList();
		aggroList.writeLock();
		try {

			final AggroInfo info = AggroInfo.find(object, aggroList);

			if(info == null) {
				return;
			}

			removed = aggroList.fastRemove(info);

			aggroInfoPool.put(info);

			setMostHated(null);
			setMostDamager(null);

		} finally {
			aggroList.writeUnlock();
		}

		if(removed) {
			object.removeHate(this);
			SHIP_EVENT_MANAGER.notifyAggro(this, object, local);
		}
	}

	@Override
	public void removeFromExecute(final SpaceLocation location) {
		super.removeFromExecute(location);

		final GameExecutor<UpdateNpsAITask> executor = location.getAiExecutor();
		executor.removeTask(getAI().getUpdateTask());
	}

	@Override
	public void setAI(final NpsAI ai) {
		this.ai = ai;
	}

	@Override
	public void setMostDamager(final SpaceShip mostDamager) {
		this.mostDamager = mostDamager;
	}

	@Override
	public void setMostHated(final SpaceShip mostHated) {
		this.mostHated = mostHated;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public void setSpawn(final NpsSpawn spawn) {
		this.spawn = spawn;
	}

	@Override
	public void setSpawnLoc(final Vector spawnLoc) {
		this.spawnLoc = spawnLoc;
	}

	@Override
	public void spawnMe(final LocalObjects local) {
		super.spawnMe(local);

		final NpsAI ai = getAI();

		if(ai != null) {
			if(ai.isGlobal()) {
				ai.startAITasks();
			} else {
				// TODO сделать проверку активности сектора
			}
		}
	}

	@Override
	public void spawnMe(final Vector spawnLoc, final LocalObjects local) {
		spawnMe(local, spawnLoc.getX(), spawnLoc.getY(), spawnLoc.getZ());
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + name + "]";
	}
}

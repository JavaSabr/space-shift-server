package com.ss.server.model.ship.regen;

import com.ss.server.LocalObjects;

/**
 * Интерфейс для реализации регенерации чего-либо у корабля.
 * 
 * @author Ronn
 */
public interface Regen {

	/**
	 * @return может ли сейчас сработать эта регенерация.
	 */
	public boolean canRegen(LocalObjects local);

	/**
	 * Обработка регенерации.
	 */
	public void doRegen(LocalObjects local);
}

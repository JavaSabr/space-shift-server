package com.ss.server.model.ship.regen.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.regen.Regen;

/**
 * Базовая реализация регена.
 * 
 * @author Ronn
 */
public abstract class AbstractRegen<T extends SpaceShip> implements Regen {

	protected static final Logger LOGGER = LoggerManager.getLogger(Regen.class);

	/** корабль владелец регена */
	protected final T owner;

	public AbstractRegen(final T owner) {
		this.owner = owner;
	}

	@Override
	public boolean canRegen(final LocalObjects local) {
		return false;
	}

	@Override
	public void doRegen(final LocalObjects local) {
	}

	/**
	 * @return корабль владелец регена.
	 */
	protected T getOwner() {
		return owner;
	}
}

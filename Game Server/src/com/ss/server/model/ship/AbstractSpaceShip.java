package com.ss.server.model.ship;

import static java.lang.Math.max;
import static java.lang.Math.min;
import rlib.concurrent.atomic.AtomicInteger;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.LocalObjects;
import com.ss.server.executor.GameExecutor;
import com.ss.server.manager.FormulasManager;
import com.ss.server.model.MessageType;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.ShipAI;
import com.ss.server.model.func.stat.Calculator;
import com.ss.server.model.func.stat.StatFunc;
import com.ss.server.model.func.stat.StatType;
import com.ss.server.model.impact.ImpactType;
import com.ss.server.model.impact.handler.ImpactHandler;
import com.ss.server.model.impact.handler.impl.ShipDamageImpactHandler;
import com.ss.server.model.impl.AbstractSpaceObject;
import com.ss.server.model.impl.Env;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.regen.Regen;
import com.ss.server.model.skills.ActivableSkill;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.TimeActiveSkill;
import com.ss.server.model.skills.state.SkillStateList;
import com.ss.server.model.storage.Storage;
import com.ss.server.network.game.packet.server.ResponseSayMessage;
import com.ss.server.network.game.packet.server.ResponseShipDelete;
import com.ss.server.task.impl.UpdateSpaceObjectTask;
import com.ss.server.task.ship.impl.ShipFlyTask;
import com.ss.server.task.ship.impl.ShipRegenTask;
import com.ss.server.task.ship.impl.ShipRotationTask;
import com.ss.server.template.ship.ShipTemplate;

/**
 * Базовая модель космического корабля.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceShip<T extends ShipTemplate> extends AbstractSpaceObject<T> implements SpaceShip {

	/** таблица скилов по уникальному ид модуля */
	protected final Table<IntKey, Skill[]> skillTable;

	/** активные силовые поля */
	protected final Array<ForceShield> forceShields;
	/** список доступных регенераторов кораблю */
	protected final Array<Regen> regenerations;
	/** список кораблей, которые имеют агресию на этот корабль */
	protected final Array<Nps> hateList;
	/** список обновляемых умений */
	protected final Array<Skill> skills;

	/** массив калькуляторов статов */
	protected final Calculator[] calcs;
	/** кол-во активных генераторов */
	protected final AtomicInteger[] generators;

	/** список состояний всех уменикорабля */
	protected final SkillStateList skillStateList;

	/** система рейтинга */
	protected final RatingSystem ratingSystem;
	/** система модулей */
	protected final ModuleSystem moduleSystem;

	/** хранилище корабля */
	protected volatile Storage storage;

	/** задача обработки процесса полета корабля */
	protected final ShipFlyTask flyTask;
	/** задача обработки процесса разворота корабля */
	protected final ShipRotationTask rotateTask;
	/** задача обработки регенерации корабял */
	protected final ShipRegenTask regenTask;
	/** задача по обновлению корабля */
	protected final UpdateSpaceObjectTask updateTask;

	/** распределение на двигатели */
	protected float engineEnergy;

	/** текущая прочность */
	protected volatile int currentStrength;
	/** текущая общая прочность щитов */
	protected volatile int currentShield;
	/** максимальная прочность щитов */
	protected volatile int maxShield;

	public AbstractSpaceShip(final int objectId, final T template) {
		super(objectId, template);

		this.skillStateList = new SkillStateList();
		this.ratingSystem = createRatingSystem();
		this.skillTable = TableFactory.newConcurrentIntegerTable();
		this.regenerations = ArrayFactory.newArray(Regen.class);
		this.forceShields = ArrayFactory.newConcurrentAtomicArray(ForceShield.class);
		this.calcs = new Calculator[StatType.LENGTH];
		this.moduleSystem = template.takeModuleSystem();
		this.moduleSystem.setOwner(this);
		this.flyTask = new ShipFlyTask(this);
		this.rotateTask = new ShipRotationTask(this);
		this.updateTask = new UpdateSpaceObjectTask(this);
		this.regenTask = new ShipRegenTask(this);
		this.hateList = ArrayFactory.newConcurrentAtomicArray(SpaceShip.class);
		this.skills = ArrayFactory.newConcurrentAtomicArray(Skill.class);

		this.generators = new AtomicInteger[GeneratorMode.LENGTH];

		for(int i = 0, length = generators.length; i < length; i++) {
			generators[i] = new AtomicInteger(0);
		}

		template.addFuncsTo(this);

		final FormulasManager formulasManager = FormulasManager.getInstance();
		formulasManager.addFuncsTo(this);

		initRegenerations();
	}

	@Override
	public Array<Skill> getSkills() {
		return skills;
	}

	@Override
	public void addActiveGenerator(final GeneratorMode mode) {
		generators[mode.ordinal()].incrementAndGet();
	}

	@Override
	public void addForceShield(final ForceShield forceShield) {

		final Array<ForceShield> forceShields = getForceShields();
		forceShields.writeLock();
		try {
			forceShields.add(forceShield);
		} finally {
			forceShields.writeUnlock();
		}

		updateMaxShield();
	}

	@Override
	public void addHate(final Nps nps) {

		final Array<Nps> hateList = getHateList();
		hateList.writeLock();
		try {

			if(!hateList.contains(nps)) {
				hateList.add(nps);
			}

		} finally {
			hateList.writeUnlock();
		}
	}

	@Override
	public void addMe(final PlayerShip playerShip, final LocalObjects local) {

		final Array<Skill> skills = getSkills();

		for(Skill skill : skills.array()) {

			if(skill == null) {
				break;
			}

			if(!(skill instanceof ActivableSkill)) {
				continue;
			}

			final ActivableSkill activableSkill = (ActivableSkill) skill;
			activableSkill.showTo(playerShip, local);
		}

		super.addMe(playerShip, local);
	}

	@Override
	public void addSkills(final Module module, final Skill[] added, LocalObjects local) {

		if(added.length < 1) {
			return;
		}

		final Table<IntKey, Skill[]> skillTable = getSkillTable();
		skillTable.writeLock();
		try {

			if(skillTable.containsKey(module.getObjectId())) {
				throw new RuntimeException("found duplicate add skils");
			}

			skillTable.put(module.getObjectId(), added);

		} finally {
			skillTable.writeUnlock();
		}

		final Array<Skill> skills = getSkills();
		skills.writeLock();
		try {
			skills.addAll(added);
		} finally {
			skills.writeUnlock();
		}

		for(Skill skill : added) {
			skill.init(this, local);
		}
	}

	@Override
	public void addStatFunc(final StatFunc func) {

		final Calculator[] calcs = getCalcs();

		synchronized(calcs) {

			Calculator calc = calcs[func.getType().ordinal()];

			if(calc == null) {
				calc = Calculator.newInstance();
				calcs[func.getType().ordinal()] = calc;
			}

			calc.addStatFunc(func);
		}
	}

	@Override
	public void addToUpdate(final SpaceLocation location) {
		final GameExecutor<UpdateSpaceObjectTask> executor = location.getObjectExecutor();
		executor.addTask(updateTask);
	}

	@Override
	public float calcStat(final StatType type, final int init, Env env) {

		final Calculator calc = getCalcs()[type.ordinal()];

		if(calc == null) {
			return init;
		}

		env = checkEnv(env);
		env.value = init;
		env.owner = this;

		calc.calc(env);

		return env.value;
	}

	@Override
	public float calcStat(final StatType type, final int init, final SpaceShip target, Env env) {

		final Calculator calc = getCalcs()[type.ordinal()];

		if(calc == null) {
			return init;
		}

		env = checkEnv(env);
		env.target = target;
		env.value = init;

		calc.calc(env);

		return env.value;
	}

	@Override
	public boolean canCollision(final SpaceObject object) {
		return object.isLocationObject() || object.isGravityObject();
	}

	/**
	 * Проверка наличия контейнера для рассчета функции.
	 */
	protected final Env checkEnv(Env env) {

		if(env != null) {
			env.clear();
		} else {
			final LocalObjects local = LocalObjects.get();
			env = local.getNextEnv();
		}

		return env;
	}

	@Override
	public void clearHateList(final LocalObjects local) {

		final Array<Nps> hateList = getLocalHateList(local);

		if(hateList.isEmpty()) {
			return;
		}

		for(final Nps nps : hateList.array()) {

			if(nps == null) {
				break;
			}

			nps.removeAggro(this, local);
		}
	}

	/**
	 * Очистка состояний умений.
	 */
	protected void clearSkillStates() {
		final SkillStateList stateList = getSkillStateList();
		stateList.clear();
	}

	/**
	 * @return соответствующая система рейтинга.
	 */
	protected RatingSystem createRatingSystem() {
		return null;
	}

	@Override
	public void decayMe(final LocalObjects local) {

		// прерывание движения
		rotateTask.stop();
		flyTask.stop();

		super.decayMe(local);
	}

	@Override
	protected void deleteMeImpl(LocalObjects local) {

		// завершаем активные умения
		finishSkills(local);
		// очищаем агрессию НПС относительного этого корабля
		clearHateList(local);
		// очищаем состояния умений
		clearSkillStates();

		super.deleteMeImpl(local);
	}

	@Override
	public void doDestruct(final SpaceObject destroyer, final LocalObjects local) {
		super.doDestruct(destroyer, local);

		// вырубаем все активные умения
		finishSkills(local);
	}

	@Override
	public void doRegen(final LocalObjects local, final long currentTime) {

		final Array<Regen> regens = getRegens();

		if(regens.isEmpty()) {
			return;
		}

		for(final Regen regen : regens.array()) {

			if(regen == null) {
				break;
			}

			if(regen.canRegen(local)) {
				regen.doRegen(local);
			}
		}
	}

	@Override
	public void doSkillUse(final Vector location, final Skill skill, final LocalObjects local) {

		if(location.distance(getLocation()) < 60) {
			synchronized(flyTask) {
				setLocation(location, Vector.ZERO, local);
			}
		}

		final long currentTime = System.currentTimeMillis();
		final SpaceObject target = getAI().getTarget();

		if(!skill.checkCondition(target, local, currentTime)) {
			return;
		}

		skill.preConsume(target, local);
		skill.useSkill(target, local, currentTime);
		skill.postConsume(target, local);

		if(skill.isNeedReloading()) {
			skill.startReload(local);
		}
	}

	@Override
	public void doUpdate(final LocalObjects local, final long currentTime) {
		super.doUpdate(local, currentTime);

		flyTask.update(local, currentTime);
		rotateTask.update(local, currentTime);

		final Array<Skill> skills = getSkills();
		skills.readLock();
		try {

			for(Skill skill : skills.array()) {

				if(skill == null) {
					break;
				}

				skill.update(currentTime, local);
			}

		} finally {
			skills.readUnlock();
		}
	}

	@Override
	public void finalyze() {
		super.finalyze();

		finalyzeRatingSystem();
		finalyzeModuleSystem();

		skillTable.clear();
	}

	protected void finalyzeModuleSystem() {
		final ModuleSystem moduleSystem = getModuleSystem();
		moduleSystem.finalyze();
	}

	protected void finalyzeRatingSystem() {
		final RatingSystem ratingSystem = getRatingSystem();
		ratingSystem.finalyze();
	}

	@Override
	public void finishSkills(final LocalObjects local) {

		final Array<Skill> skills = getSkills();
		skills.readLock();
		try {

			for(Skill skill : skills.array()) {

				if(skill == null) {
					break;
				}

				if(skill instanceof TimeActiveSkill) {
					((TimeActiveSkill) skill).cancel(local);
				} else if(skill instanceof ActivableSkill) {
					((ActivableSkill) skill).cancel(local);
				} else if(skill instanceof ForceShield) {
					((ForceShield) skill).cancel(local);
				}
			}

		} finally {
			skills.readUnlock();
		}
	}

	@Override
	public float getAccel(final Env env) {
		return calcStat(StatType.FLY_ACCEL, 0, env);
	}

	@Override
	public ShipAI getAI() {
		return null;
	}

	/**
	 * @return набор калькуляторов статов.
	 */
	protected final Calculator[] getCalcs() {
		return calcs;
	}

	@Override
	public float getCurrentSpeed() {
		return flyTask.getLastSpeed();
	}

	@Override
	public int getCurrentStrength() {
		return currentStrength;
	}

	@Override
	public float getEngineEnergy() {
		return engineEnergy;
	}

	/**
	 * @return задача обработки процесса полета корабля.
	 */
	protected ShipFlyTask getFlyTask() {
		return flyTask;
	}

	@Override
	public Array<ForceShield> getForceShields() {
		return forceShields;
	}

	@Override
	public int getCurrentShield() {
		return currentShield;
	}

	@Override
	public Array<Nps> getHateList() {
		return hateList;
	}

	@Override
	public int getLevel() {
		return 0;
	}

	@Override
	public Array<Nps> getLocalHateList(final LocalObjects local) {

		final Array<Nps> hateList = getHateList();
		final Array<Nps> result = local.getNextNpsList();

		hateList.readLock();
		try {
			result.addAll(hateList);
		} finally {
			hateList.readUnlock();
		}

		return result;
	}

	@Override
	public int getMaxShield() {
		return maxShield;
	}

	@Override
	public int getMaxStrength(final Env env) {
		return (int) calcStat(StatType.MAX_STRENGTH, 0, env);
	}

	@Override
	public ModuleSystem getModuleSystem() {
		return moduleSystem;
	}

	@Override
	public RatingSystem getRatingSystem() {
		return ratingSystem;
	}

	/**
	 * @return список доступных регенераторов кораблю.
	 */
	public Array<Regen> getRegens() {
		return regenerations;
	}

	/**
	 * @return задача обработки процесса разворота корабля.
	 */
	protected ShipRotationTask getRotateTask() {
		return rotateTask;
	}

	@Override
	public Skill getSkill(final int objectId, final int skillId) {

		final Skill[] array = getSkillTable().get(objectId);

		if(array == null || array.length < 1) {
			return null;
		}

		for(final Skill skill : array) {
			if(skill.getId() == skillId) {
				return skill;
			}
		}

		return null;
	}

	@Override
	public final Table<IntKey, Skill[]> getSkillTable() {
		return skillTable;
	}

	@Override
	public Skill[] getSkills(final int objectId) {
		return skillTable.get(objectId);
	}

	@Override
	public SkillStateList getSkillStateList() {
		return skillStateList;
	}

	@Override
	public Storage getStorage() {
		return storage;
	}

	@Override
	public int getTypeId() {
		return 0;
	}

	@Override
	protected void initImpactHandlers(final ImpactHandler[] impactHandlers) {
		impactHandlers[ImpactType.DAMAGE.ordinal()] = ShipDamageImpactHandler.getInstance();
	}

	/**
	 * Инициализация базовых регенераторов.
	 */
	protected void initRegenerations() {
	}

	@Override
	public void interruptFly() {
		flyTask.interrupt();
	}

	@Override
	public boolean isActiveGenerator(final GeneratorMode mode) {
		return generators[mode.ordinal()].get() > 0;
	}

	@Override
	public boolean isDestructed() {
		return getCurrentStrength() < 1;
	}

	@Override
	public boolean isFlying() {
		return getEngineEnergy() > 0F && flyTask.getLastSpeed() > 1;
	}

	@Override
	public boolean isInvul() {
		return false;
	}

	@Override
	public boolean isTurning() {
		return rotateTask.getDone() < 1F;
	}

	@Override
	public void onCollision(final SpaceObject target, final LocalObjects local) {
		super.onCollision(target, local);

		if((target.isLocationObject() || target.isGravityObject()) && !isDestructed()) {
			setCurrentStrength(0, local);
			doDestruct(target, local);
			interruptFly();
			sync(local, System.currentTimeMillis());
		}
	}

	@Override
	public void reinit() {
		super.reinit();
	}

	@Override
	public void removeActiveGenerator(final GeneratorMode mode) {
		generators[mode.ordinal()].decrementAndGet();
	}

	@Override
	public void removeForceShield(final ForceShield forceShield) {

		final Array<ForceShield> forceShields = getForceShields();
		forceShields.writeLock();
		try {
			forceShields.fastRemove(forceShield);
		} finally {
			forceShields.writeUnlock();
		}

		updateMaxShield();
	}

	@Override
	public void removeFromExecute(final SpaceLocation location) {
		final GameExecutor<UpdateSpaceObjectTask> executor = location.getObjectExecutor();
		executor.removeTask(updateTask);
	}

	@Override
	public void removeHate(final Nps nps) {
		final Array<Nps> hateList = getHateList();
		hateList.fastRemove(nps);
	}

	@Override
	public void removeMe(final PlayerShip playerShip, final LocalObjects local) {
		playerShip.sendPacket(ResponseShipDelete.getInstance(this, local), true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("remove me " + this + " to " + playerShip);
		}
	}

	@Override
	public void removeSkills(Module module, Skill[] remove, LocalObjects local) {

		if(remove.length < 1) {
			return;
		}

		final Table<IntKey, Skill[]> skillTable = getSkillTable();
		skillTable.writeLock();
		try {
			skillTable.remove(module.getObjectId());
		} finally {
			skillTable.writeUnlock();
		}

		final Array<Skill> skills = getSkills();
		skills.writeLock();
		try {

			for(Skill skill : remove) {
				skills.fastRemove(skill);
			}

		} finally {
			skills.writeUnlock();
		}
	}

	@Override
	public void removeStatFunc(final StatFunc func) {

		final Calculator[] calcs = getCalcs();

		synchronized(calcs) {

			final Calculator calc = calcs[func.getType().ordinal()];

			if(calc == null) {
				LOGGER.warning(new Exception("not found calc for type " + func.getType()));
				return;
			}

			calc.removeStatFunc(func);
		}
	}

	@Override
	public void rotate(final LocalObjects local, final Rotation start, final Rotation end, final boolean infinity) {
		rotateTask.update(local, start, end, infinity);
	}

	@Override
	public void sayMessage(final String message, final LocalObjects local) {
		broadcastLocationPacket(ResponseSayMessage.getInstance(MessageType.MAIN, getName(), message, local));
	}

	@Override
	public void setCurrentStrength(final int strength, final LocalObjects local) {
		this.currentStrength = max(min(strength, getMaxStrength(local.getNextEnv())), 0);
	}

	@Override
	public void setEngineEnergy(final float engineEnergy) {
		this.engineEnergy = max(min(engineEnergy, 1), 0);
	}

	/**
	 * @param maxForceShieldStrength максимальная прочность щита.
	 */
	protected void setMaxShield(final int maxForceShieldStrength) {
		this.maxShield = maxForceShieldStrength;
	}

	@Override
	public void setCurrentShield(int currentShield, LocalObjects local) {
		this.currentShield = currentShield;
	}

	@Override
	public void setStorage(final Storage storage) {
		this.storage = storage;
	}

	@Override
	public void spawnMe(final LocalObjects local) {
		super.spawnMe(local);

		// реинициализация положения
		setLocation(location, Vector.ZERO, local);

		flyTask.reinit();
		rotateTask.reinit();
		regenTask.reinit();
	}

	@Override
	public void sync(final LocalObjects local, final long currentTime) {
		super.sync(local, currentTime);
		flyTask.sync(local, currentTime);
		rotateTask.sync(local);
	}

	@Override
	public void syncFor(final SpaceObject object, final LocalObjects local) {
		super.syncFor(object, local);
		rotateTask.syncFor(local, object);
		flyTask.syncFor(local, object);
	}

	@Override
	public void teleportTo(final Vector position, final Rotation rotation, final LocalObjects local, final int locationId) {

		flyTask.stop();
		rotateTask.stop();
		try {

			flyTask.lock();
			rotateTask.lock();
			try {
				super.teleportTo(position, rotation, local, locationId);
			} finally {
				rotateTask.unlock();
				flyTask.unlock();
			}

		} finally {
			flyTask.reinit();
			rotateTask.reinit();
		}
	}

	/**
	 * Обновление максимального общего кол-ва прочности щитов.
	 */
	protected void updateMaxShield() {

		int maxStrength = 0;

		final Array<ForceShield> forceShields = getForceShields();
		forceShields.readLock();
		try {

			for(final ForceShield shield : forceShields.array()) {

				if(shield == null) {
					break;
				}

				maxStrength += shield.getMaxStrength();
			}

		} finally {
			forceShields.readUnlock();
		}

		setMaxShield(maxStrength);
	}
}

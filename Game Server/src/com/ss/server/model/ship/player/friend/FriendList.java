package com.ss.server.model.ship.player.friend;

import java.util.concurrent.atomic.AtomicReference;

import rlib.concurrent.lock.AsynReadSynWriteLock;
import rlib.concurrent.lock.LockFactory;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.Pool;
import rlib.util.pools.PoolFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.database.PlayerDBManager;
import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.impl.Space;
import com.ss.server.model.listener.PlayerEnteredListener;
import com.ss.server.model.listener.PlayerExitedListener;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Реализация списка дружественных пилотов игрока.
 * 
 * @author Ronn
 */
public class FriendList implements AsynReadSynWriteLock, PlayerEnteredListener, PlayerExitedListener {

	private static final PlayerDBManager PLAYER_DB_MANAGER = PlayerDBManager.getInstance();

	private static final ThreadLocal<Array<Friend>> THREAD_LOCAL_FRIEND_CONTAINER = new ThreadLocal<Array<Friend>>() {

		@Override
		protected Array<Friend> initialValue() {
			return ArrayFactory.newArray(Friend.class);
		}
	};

	/** пул использованных объектов дружественных пилотов */
	private final Pool<Friend> friendPool = PoolFactory.newAtomicFoldablePool(Friend.class);

	/** таблица дружественных пилотов */
	private final Table<IntKey, Friend> friends;

	/** синхронизатор */
	private final AsynReadSynWriteLock lock;

	/** владелец списка друзей */
	private final PlayerShip owner;

	public FriendList(final PlayerShip owner) {
		this.friends = TableFactory.newIntegerTable();
		this.lock = LockFactory.newARSWLock();
		this.owner = owner;
	}

	/**
	 * Активация работы списка друзей.
	 */
	public void activate() {

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.addPlayerEnteredListener(this);
		eventManager.addPlayerExitedListener(this);

		final Table<IntKey, Friend> friends = getFriends();

		if(friends.isEmpty()) {
			return;
		}

		final Space space = Space.getInstance();

		synLock();
		try {

			final Array<Friend> container = friends.values(THREAD_LOCAL_FRIEND_CONTAINER.get());

			for(final Friend friend : container.array()) {

				if(friend == null) {
					break;
				}

				final PlayerShip plahyerShip = space.getPlayerShip(friend.getObjectId());

				if(plahyerShip == null) {
					continue;
				}

				final AtomicReference<PlayerShip> reference = friend.getPlayerShipReference();

				plahyerShip.deleteLock();
				try {

					if(plahyerShip.isDeleted()) {
						continue;
					}

					if(!reference.compareAndSet(null, plahyerShip)) {
						throw new RuntimeException("reference " + reference + " is not null.");
					}

				} finally {
					plahyerShip.deleteUnlock();
				}
			}

		} finally {
			synUnlock();
		}
	}

	/**
	 * Добавление в список загруженного из БД дружественного пилота, выполнять
	 * из блока synLock().
	 * 
	 * @param friend дружественный пилот.
	 */
	public void addFriend(final Friend friend) {

		final Table<IntKey, Friend> friends = getFriends();

		if(friends.containsKey(friend.getObjectId())) {
			throw new RuntimeException("found duplicate friend.");
		}

		friends.put(friend.getObjectId(), friend);
	}

	public void addFriend(final String friendName) {

	}

	/**
	 * Добавление нового дружественого пилота в список друзей.
	 * 
	 * @param playerShip корабль дружественного пилота.
	 */
	public void addFriend(PlayerShip playerShip) {

		final Table<IntKey, Friend> friends = getFriends();
		final PlayerShip owner = getOwner();

		synLock();
		try {

			if(friends.containsKey(playerShip.getObjectId())) {
				throw new RuntimeException("found duplicate friend.");
			}

			if(!PLAYER_DB_MANAGER.addNewFriend(owner.getObjectId(), playerShip.getObjectId())) {
				throw new RuntimeException("problems for add friend " + playerShip + " to " + owner);
			}

			final Friend friend = createFriend();
			friend.setName(playerShip.getName());
			friend.setObjectId(playerShip.getObjectId());

			final AtomicReference<PlayerShip> reference = friend.getPlayerShipReference();
			reference.set(playerShip);

			friends.put(playerShip.getObjectId(), friend);

		} finally {
			synUnlock();
		}

		// TODO обновить список друзей
	}

	/**
	 * Удаление дружественного пилота из списка.
	 * 
	 * @param objectId уникальный ид дружественного пилота.
	 */
	public void removeFriend(int objectId) {

		final Table<IntKey, Friend> friends = getFriends();
		final PlayerShip owner = getOwner();

		synLock();
		try {

			final Friend friend = friends.get(objectId);

			if(friend == null) {
				throw new RuntimeException("not found friend for id " + objectId);
			}

			if(!PLAYER_DB_MANAGER.removeFriend(owner.getObjectId(), objectId)) {
				throw new RuntimeException("problems for remove friend " + objectId + " to " + owner);
			}

			friends.remove(objectId);

			final Pool<Friend> friendPool = getFriendPool();
			friendPool.put(friend);

		} finally {
			synUnlock();
		}
	}

	/**
	 * Сложить список дружественных пилотов в указанный контейнер, выполнять в
	 * syn/asynLock() блоке.
	 * 
	 * @param container контейнер для дружественных пилотов.
	 */
	public void addFriendsTo(final Array<Friend> container) {
		final Table<IntKey, Friend> friends = getFriends();
		friends.values(container);
	}

	@Override
	public void asynLock() {
		lock.asynLock();
	}

	@Override
	public void asynUnlock() {
		lock.asynUnlock();
	}

	/**
	 * Создание нового обхекта дружественного пилота для этого списка друзей.
	 * 
	 * @return новый объект дружественно пилота.
	 */
	public Friend createFriend() {

		Friend friend = getFriendPool().take();

		if(friend == null) {
			friend = new Friend();
		}

		return friend;
	}

	/**
	 * Деактивация работы списка друзей.
	 */
	public void deactivate() {

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.removePlayerEnteredListener(this);
		eventManager.removePlayerExitedListener(this);

		final Table<IntKey, Friend> friends = getFriends();

		if(friends.isEmpty()) {
			return;
		}

		final Pool<Friend> pool = getFriendPool();

		synLock();
		try {

			final Array<Friend> container = friends.values(THREAD_LOCAL_FRIEND_CONTAINER.get());

			for(final Friend friend : container.array()) {

				if(friend == null) {
					break;
				}

				pool.put(friend);
			}

			friends.clear();

		} finally {
			synUnlock();
		}
	}

	/**
	 * @return пул использованных объектов дружественных пилотов.
	 */
	private Pool<Friend> getFriendPool() {
		return friendPool;
	}

	/**
	 * @return таблица дружественных пилотов.
	 */
	private Table<IntKey, Friend> getFriends() {
		return friends;
	}

	/**
	 * @return владелец списка друзей.
	 */
	public PlayerShip getOwner() {
		return owner;
	}

	@Override
	public void onEntered(final PlayerShip playerShip) {

		final Table<IntKey, Friend> friends = getFriends();

		if(friends.isEmpty()) {
			return;
		}

		asynLock();
		try {

			final Friend friend = friends.get(playerShip.getObjectId());

			if(friend == null) {
				return;
			}

			final AtomicReference<PlayerShip> reference = friend.getPlayerShipReference();

			if(!reference.compareAndSet(null, playerShip)) {
				throw new RuntimeException("reference " + reference + " is not null");
			}

			// TODO обновить статус онлайна для игрока

		} finally {
			asynUnlock();
		}
	}

	@Override
	public void onExited(final PlayerShip playerShip) {

		final Table<IntKey, Friend> friends = getFriends();

		if(friends.isEmpty()) {
			return;
		}

		asynLock();
		try {

			final Friend friend = friends.get(playerShip.getObjectId());

			if(friend == null) {
				return;
			}

			final AtomicReference<PlayerShip> reference = friend.getPlayerShipReference();

			if(!reference.compareAndSet(playerShip, null)) {
				throw new RuntimeException("reference " + reference + " is null");
			}

			// TODO обновить статус онлайна для игрока

		} finally {
			asynUnlock();
		}
	}

	@Override
	public void synLock() {
		lock.synLock();
	}

	@Override
	public void synUnlock() {
		lock.synUnlock();
	}
}

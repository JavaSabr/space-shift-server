package com.ss.server.model.ship.player.action;

import java.lang.reflect.Constructor;
import java.util.concurrent.atomic.AtomicReference;

import rlib.util.ClassUtils;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.impl.PartyInvitePlayerActionHandler;

/**
 * Перечисление типов действий игрока.
 * 
 * @author Ronn
 */
public enum PlayerAction {
	INVITE_TO_PARTY(PartyInvitePlayerActionHandler.class),
	INVITE_TO_FRACTION(null),
	INVITE_TO_FRIEND(null),
	TRANSFER_ITEMS(null);

	public static final PlayerAction valueOf(final int index) {
		return ALL[index];
	}

	public static final PlayerAction[] ALL = values();

	/** конструктор обработчика */
	private Constructor<? extends PlayerActionHandler> constructor;

	private PlayerAction(final Class<? extends PlayerActionHandler> type) {
		if(type == null) {
			this.constructor = null;
		} else {
			this.constructor = ClassUtils.getConstructor(type);
		}
	}

	/**
	 * Проверка аргументов перед создание обработчика.
	 * 
	 * @param playerShip запрашивающий игрок.
	 * @param object целевой объект.
	 * @param local контейнер локальных объектов.
	 * @return подходят ли аргументы.
	 */
	public boolean checkArguments(final PlayerShip playerShip, final SpaceObject object, final LocalObjects local) {

		AtomicReference<PlayerActionHandler> refrence = playerShip.getActionHandlerReference();

		if(refrence.get() != null) {
			playerShip.sendSystemMessage(SystemMessageType.YOU_HAVE_SENT_REQUEST, local);
			return false;
		}

		if(object.isPlayerShip()) {

			final PlayerShip target = object.getPlayerShip();
			refrence = target.getActionHandlerReference();

			if(refrence.get() != null) {
				playerShip.sendSystemMessage(SystemMessageType.TARGET_ALREADY_OCCUPIED_ANOTHER_REQUEST, local);
				return false;
			}
		}

		return true;
	}

	/**
	 * @return реализован ли тип действия.
	 */
	public boolean isImplemented() {
		return constructor != null;
	}

	/**
	 * @return создание нового обработчика.
	 */
	public PlayerActionHandler newInstance() {
		return ClassUtils.newInstance(constructor);
	}
}

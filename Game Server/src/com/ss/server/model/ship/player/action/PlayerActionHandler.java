package com.ss.server.model.ship.player.action;

import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Реализация обработчика действия игрока.
 * 
 * @author Ronn
 */
public interface PlayerActionHandler extends Foldable {

	/**
	 * @return тип действия.
	 */
	public PlayerAction getType();

	/**
	 * Обработка принятия запроса.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void handleAccept(LocalObjects local);

	/**
	 * Обработка отмена запроса.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void handleCancel(LocalObjects local);

	/**
	 * Обработка отвержения запроса.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void handleReject(LocalObjects local);

	/**
	 * Обработка запроса на действие.
	 * 
	 * @param playerShip корабль игрока.
	 * @param target целевой объект.
	 * @param local контейнер локальных объектов.
	 */
	public void handleRequest(PlayerShip playerShip, SpaceObject target, LocalObjects local);
}

package com.ss.server.model.ship.player.friend;

import java.util.concurrent.atomic.AtomicReference;

import rlib.util.pools.Foldable;

import com.ss.server.model.ship.player.PlayerShip;

/**
 * Реализация модели дружественного пилота.
 * 
 * @author Ronn
 */
public class Friend implements Foldable {

	/** ссылка на корабль дружественного пилота (когда он онлаин) */
	private final AtomicReference<PlayerShip> playerShipReference;

	/** имя пилота */
	private String name;

	/** уникальный ид корабял пилота */
	private int objectId;

	public Friend() {
		this.playerShipReference = new AtomicReference<PlayerShip>();
	}

	/**
	 * @return имя пилота.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return в игре ли сейчас пилот.
	 */
	public boolean isOnline() {
		return playerShipReference.get() != null;
	}

	/**
	 * @return уникальный ид корабял пилота.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return ссылка на корабль дружественного пилота (когда он онлаин).
	 */
	public AtomicReference<PlayerShip> getPlayerShipReference() {
		return playerShipReference;
	}

	/**
	 * @param name имя пилота.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param objectId уникальный ид корабял пилота.
	 */
	public void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	@Override
	public void finalyze() {
		objectId = 0;
		name = null;
		playerShipReference.set(null);
	}
}

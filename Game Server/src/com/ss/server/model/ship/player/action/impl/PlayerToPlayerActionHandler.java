package com.ss.server.model.ship.player.action.impl;

import java.util.concurrent.atomic.AtomicReference;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.SystemMessageVars;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerActionHandler;
import com.ss.server.model.ship.player.action.PlayerActionHandlerFactory;
import com.ss.server.network.game.packet.server.ResponsePlayerActionCanceled;
import com.ss.server.network.game.packet.server.ResponsePlayerActionRequest;
import com.ss.server.network.game.packet.server.ResponsePlayerActionWait;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;

/**
 * Базовая реализация действия между игроками.
 * 
 * @author Ronn
 */
public abstract class PlayerToPlayerActionHandler extends AbstractPlayerActionHandler<PlayerShip> {

	/**
	 * Проверка дополнительных условий по запросу на действие.
	 * 
	 * @param playerShip запрашивающий игрок.
	 * @param target запрашиваемый игрок.
	 * @param local контейнер локальных объектов.
	 * @return можно ли выполнить запрос.
	 */
	protected abstract boolean checkCondition(PlayerShip playerShip, PlayerShip target, LocalObjects local);

	@Override
	protected boolean checkTarget(final SpaceObject target) {
		return target.isPlayerShip();
	}

	@Override
	public synchronized void handleAccept(final LocalObjects local) {

		final PlayerShip playerShip = getPlayerShip();
		final PlayerShip target = getTarget();

		if(playerShip != null && target != null) {
			handleAcceptImpl(playerShip, target, local);
		}

		if(target != null) {
			target.setActionHandler(null);
		}

		super.handleAccept(local);
	}

	/**
	 * Реализия обработки принятия запроса.
	 * 
	 * @param playerShip корабль запрашивающего игрока.
	 * @param target корабль запрашиваемого корабля.
	 * @param local контейнер локальных объектов.
	 * @return успешно ли принят.
	 */
	protected abstract boolean handleAcceptImpl(PlayerShip playerShip, PlayerShip target, LocalObjects local);

	@Override
	public synchronized void handleCancel(final LocalObjects local) {

		final PlayerShip playerShip = getPlayerShip();
		final PlayerShip target = getTarget();

		if(target != null) {

			target.setActionHandler(null);
			target.sendPacket(ResponsePlayerActionCanceled.getInstance(), true);

			if(playerShip != null) {

				final ResponseSystemMessage message = ResponseSystemMessage.getInstance(SystemMessageType.CANCELED_REQUEST, local);
				message.addVar(SystemMessageVars.VAR_OBJECT_NAME, playerShip.getName());

				playerShip.sendPacket(message, true);
			}
		}

		super.handleCancel(local);
	}

	@Override
	public synchronized void handleReject(final LocalObjects local) {

		final PlayerShip playerShip = getPlayerShip();
		final PlayerShip target = getTarget();

		if(playerShip != null && target != null) {
			handleRejectImpl(playerShip, target, local);
		}

		if(target != null) {

			target.setActionHandler(null);

			if(playerShip != null) {

				final ResponseSystemMessage message = ResponseSystemMessage.getInstance(SystemMessageType.REFUSED_TO_REQUEST, local);
				message.addVar(SystemMessageVars.VAR_OBJECT_NAME, target.getName());

				playerShip.sendPacket(message, true);
			}
		}

		super.handleReject(local);
	}

	/**
	 * Реализия обработки отказа от запроса.
	 * 
	 * @param playerShip запрашивающий игрок.
	 * @param target запрашиваемый игрок.
	 * @param local контейнер локальных объектов.
	 * @return успешно ли отказан.
	 */
	protected abstract boolean handleRejectImpl(PlayerShip playerShip, PlayerShip target, LocalObjects local);

	@Override
	protected void handleRequestImpl(final PlayerShip playerShip, final PlayerShip target, final LocalObjects local) {

		final AtomicReference<PlayerActionHandler> playerRef = playerShip.getActionHandlerReference();
		final AtomicReference<PlayerActionHandler> targetRef = target.getActionHandlerReference();

		synchronized(playerRef) {
			synchronized(targetRef) {

				if(playerRef.get() != null || targetRef.get() != null) {
					PlayerActionHandlerFactory.saveHandler(this);
					return;
				}

				if(!checkCondition(playerShip, target, local)) {
					PlayerActionHandlerFactory.saveHandler(this);
					return;
				}

				setPlayerShip(playerShip);
				setTarget(target);

				playerShip.setActionHandler(this);
				target.setActionHandler(this);
			}
		}

		playerShip.sendPacket(ResponsePlayerActionWait.getInstance(target, getType()), true);
		target.sendPacket(ResponsePlayerActionRequest.getInstance(playerShip, getType()), true);
	}
}

package com.ss.server.model.ship.player;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

/**
 * Модель информации о положении предмета на панели.
 * 
 * @author Ronn
 */
public class ItemPanelInfo implements Foldable {

	public static ItemPanelInfo newInstance(final int objectId, final int classId, final int order) {

		ItemPanelInfo info = pool.take();

		if(info == null) {
			info = new ItemPanelInfo();
		}

		info.objectId = objectId;
		info.classId = classId;
		info.order = order;

		return info;
	}

	private static final FoldablePool<ItemPanelInfo> pool = PoolFactory.newAtomicFoldablePool(ItemPanelInfo.class);

	/** уникальный ид предмета */
	private int objectId;
	/** класс предмета */
	private int classId;
	/** позиция скила на панели */
	private int order;
	/** счетчик изменений */
	private int update;

	/**
	 * Очистка счетчика.
	 */
	public final void clear() {
		this.update = 0;
	}

	@Override
	public void finalyze() {
		objectId = 0;
		order = -1;
		update = 0;
	}

	/**
	 * Складировать в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return класс предмета.
	 */
	public int getClassId() {
		return classId;
	}

	/**
	 * @return уникальный ид предмета.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return order позиция на панели.
	 */
	public final int getOrder() {
		return order;
	}

	/**
	 * @return было ли изменение.
	 */
	public boolean isChanged() {
		return update > 0;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param classId класс предмета.
	 */
	public void setClassId(final int classId) {
		this.classId = classId;
	}

	/**
	 * @param objectId уникальный ид предмета.
	 */
	public void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	/**
	 * @param order позиция на панели.
	 */
	public final void setOrder(final int order) {
		this.order = order;
	}

	/**
	 * @param order обновление позиции.
	 */
	public final void update(final int order) {
		this.order = order;
		this.update++;
	}
}

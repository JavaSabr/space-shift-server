package com.ss.server.model.ship.player.action;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

/**
 * Фабрика обработчиков действий игроков.
 * 
 * @author Ronn
 */
@SuppressWarnings("unchecked")
public class PlayerActionHandlerFactory {

	/**
	 * Созранение обработчика.
	 * 
	 * @param handler использованный обработчик.
	 */
	public static final void saveHandler(final PlayerActionHandler handler) {
		final FoldablePool<PlayerActionHandler> pool = POOLS[handler.getType().ordinal()];
		pool.put(handler);
	}

	/**
	 * Получение обработчика действия игрока.
	 * 
	 * @param type тип обработчика.
	 * @return новый обработчик.
	 */
	public static final PlayerActionHandler takeHandler(final PlayerAction type) {

		final FoldablePool<PlayerActionHandler> pool = POOLS[type.ordinal()];
		PlayerActionHandler handler = pool.take();

		if(handler == null) {
			handler = type.newInstance();
		}

		return handler;
	}

	private static final FoldablePool<PlayerActionHandler>[] POOLS;

	static {

		final PlayerAction[] types = PlayerAction.ALL;

		POOLS = new FoldablePool[types.length];

		for(final PlayerAction type : types) {
			POOLS[type.ordinal()] = PoolFactory.newAtomicFoldablePool(PlayerActionHandler.class);
		}
	}
}

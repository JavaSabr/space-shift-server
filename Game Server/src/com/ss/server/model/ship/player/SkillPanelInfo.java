package com.ss.server.model.ship.player;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.model.skills.Skill;

/**
 * Модель информации о положении скила на панели.
 * 
 * @author Ronn
 */
public class SkillPanelInfo implements Foldable {

	public static SkillPanelInfo newInstance(final Skill skill, final int order) {

		SkillPanelInfo info = pool.take();

		if(info == null) {
			info = new SkillPanelInfo();
		}

		info.skill = skill;
		info.order = order;

		return info;
	}

	private static final FoldablePool<SkillPanelInfo> pool = PoolFactory.newAtomicFoldablePool(SkillPanelInfo.class);

	/** скил на панели */
	private Skill skill;

	/** позиция скила на панели */
	private int order;
	/** счетчик изменений */
	private int update;

	/**
	 * Очистка счетчика.
	 */
	public final void clear() {
		this.update = 0;
	}

	@Override
	public void finalyze() {
		skill = null;
		update = 0;
	}

	/**
	 * Складировать в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return order позиция на панели.
	 */
	public final int getOrder() {
		return order;
	}

	/**
	 * @return skill скил на панели.
	 */
	public final Skill getSkill() {
		return skill;
	}

	/**
	 * @return было ли изменение.
	 */
	public boolean isChanged() {
		return update > 0;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param order позиция на панели.
	 */
	public final void setOrder(final int order) {
		this.order = order;
	}

	/**
	 * @param skill скил на панели.
	 */
	public final void setSkill(final Skill skill) {
		this.skill = skill;
	}

	/**
	 * @param order обновление позиции.
	 */
	public final void update(final int order) {
		this.order = order;
		this.update++;
	}
}

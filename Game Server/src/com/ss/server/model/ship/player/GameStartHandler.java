package com.ss.server.model.ship.player;

import com.ss.server.LocalObjects;

/**
 * Перечисление обработчиков различных режимов старта игры.
 * 
 * @author Ronn
 */
public enum GameStartHandler {

	ADVANCED {

		@Override
		public void prepare(final PlayerShip playerShip, final LocalObjects local) {
		}
	},
	;

	public static final GameStartHandler valueOf(final int index) {
		return VALUES[index];
	}

	public static final GameStartHandler[] VALUES = values();

	public void prepare(final PlayerShip playerShip, final LocalObjects local) {
		throw new RuntimeException("not supported");
	}
}

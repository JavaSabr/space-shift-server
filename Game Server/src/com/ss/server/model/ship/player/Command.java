package com.ss.server.model.ship.player;

/**
 * Интерфейс для реализации команды чата игрока.
 * 
 * @author Ronn
 */
public interface Command {

	public static final int ADMIN = 0x100;
	public static final int CENSOR = 0x10;
	public static final int USER = 0;

	/**
	 * @param ship исполнение команды.
	 */
	public void execute(PlayerShip ship, String values);

	/**
	 * @return уровень досткпа.
	 */
	public int getAccessLevel();

	/**
	 * @return описание команды.
	 */
	public String getDescription();

	/**
	 * @return название команды.
	 */
	public String getName();
}

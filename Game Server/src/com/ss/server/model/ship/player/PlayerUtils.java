package com.ss.server.model.ship.player;

import com.ss.server.model.social.party.Party;

/**
 * Набор утильных методов по работе с игроками.
 * 
 * @author Ronn
 */
public class PlayerUtils {

	public static boolean isPartyFriend(final PlayerShip first, final PlayerShip second) {
		final Party firstParty = first.getParty();
		return firstParty != null && firstParty == second.getParty();
	}
}

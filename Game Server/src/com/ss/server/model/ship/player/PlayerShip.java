package com.ss.server.model.ship.player;

import static com.ss.server.model.SystemMessageType.YOU_HAVE_ARRIVED_AT_THE_STATION;
import static com.ss.server.model.SystemMessageType.YOU_LEFT_THE_STATION;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.idfactory.IdGenerator;
import rlib.idfactory.IdGeneratorFactory;
import rlib.util.StringUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.database.PlayerDBManager;
import com.ss.server.manager.PlayerManager;
import com.ss.server.model.FriendStatus;
import com.ss.server.model.MessageType;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.ai.impl.PlayerShipAI;
import com.ss.server.model.faction.Fraction;
import com.ss.server.model.impl.SpaceSector;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.impl.PlayerRatingSystem;
import com.ss.server.model.ship.AbstractSpaceShip;
import com.ss.server.model.ship.MessageUtils;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.player.action.PlayerActionHandler;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.state.SkillStateInfo;
import com.ss.server.model.skills.state.SkillStateList;
import com.ss.server.model.social.party.Party;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.network.PacketUtils;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.model.GameClient;
import com.ss.server.network.game.packet.server.ResponsePlayerShipInfo;
import com.ss.server.network.game.packet.server.ResponsePlayerShipTeleport;
import com.ss.server.network.game.packet.server.ResponseSayMessage;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;
import com.ss.server.network.game.packet.server.ServerConstPacket;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Реализация космического корабля игрока.
 * 
 * @author Ronn
 */
public final class PlayerShip extends AbstractSpaceShip<PlayerShipTemplate> {

	private static final PlayerDBManager PLAYER_DB_MANAGER = PlayerDBManager.getInstance();

	private static final IdGenerator SHOT_ID_GENERATOR = IdGeneratorFactory.newSimpleIdGenerator(0, 300000);

	/** функция для обновления состояний умений в БД */
	private final Consumer<SkillStateInfo> storeSkillStates = info -> {

		if(!info.isContainsInDb()) {
			PLAYER_DB_MANAGER.addNewSkillState(this, info);
		} else if(info.isNeedUpdate()) {
			PLAYER_DB_MANAGER.updateSkillState(this, info);
		}
	};

	/** настройки интерфейса игрока */
	private final Array<InterfaceInfo> interfaceInfo;
	/** расположение умений на панели игрока */
	private final Array<SkillPanelInfo> skillPanelInfo;
	/** расположение предметов на панели игрока */
	private final Array<ItemPanelInfo> itemPanelInfo;

	/** открытые окна ангара */
	private final Array<HangarWindow> opeHangarWindows;
	/** список кнопок при последней отправке */
	private final Array<QuestButton> lastButtons;

	/** список заданий */
	private final QuestList questList;

	/** кол-во опыта корабля */
	private final AtomicLong exp;
	/** находится ли сейчас корабль на космической станции */
	private final AtomicBoolean onStation;

	/** ссылка на последнее действие */
	private final AtomicReference<PlayerActionHandler> actionHandlerReference;
	/** ссылка на текущую группу игрока */
	private final AtomicReference<Party> partyReference;
	/** ссылка на доступную для телепортации косм. станцию */
	private final AtomicReference<SpaceStation> stationReference;

	/** фракция игрока */
	private Fraction fraction;

	/** название корабля */
	private String name;

	/** клиент игрока */
	private GameClient client;

	/** время входа в игру */
	private long enterTime;
	/** итоговое время онлайна */
	private long onlineTime;

	/** уровень корабля игрока */
	private int level;

	public PlayerShip(final int objectId, final PlayerShipTemplate template) {
		super(objectId, template);

		this.ai = new PlayerShipAI(this);
		this.exp = new AtomicLong();
		this.interfaceInfo = ArrayFactory.newConcurrentAtomicArray(InterfaceInfo.class);
		this.skillPanelInfo = ArrayFactory.newConcurrentAtomicArray(SkillPanelInfo.class);
		this.itemPanelInfo = ArrayFactory.newConcurrentAtomicArray(ItemPanelInfo.class);
		this.opeHangarWindows = ArrayFactory.newConcurrentAtomicArray(HangarWindow.class);
		this.lastButtons = ArrayFactory.newConcurrentAtomicArray(QuestButton.class);
		this.questList = QuestList.getInstance();
		this.questList.setOwner(this);
		this.actionHandlerReference = new AtomicReference<>();
		this.partyReference = new AtomicReference<>();
		this.stationReference = new AtomicReference<>();
		this.onStation = new AtomicBoolean();
	}

	/**
	 * Добавление указанное кол-во опыта кораблю игрока.
	 * 
	 * @param exp кол-во добавляемого опыта.
	 * @param local контейнер локальных переменных.
	 */
	public void addExp(final long exp, final LocalObjects local) {

		final PlayerManager playerManager = PlayerManager.getInstance();
		playerManager.addExpTo(local, this, exp);

		MessageUtils.sendMessage(local, this, SystemMessageType.ADD_EXP, String.valueOf(exp));
		PacketUtils.updateExp(local, this);
	}

	/**
	 * Добавление опыта с уничтоженного НПС.
	 * 
	 * @param exp добавляемое кол-во опыта.
	 * @param nps уничтоженный НПС.
	 * @param local контейнер локальных переменных.
	 */
	public void addExp(final long exp, final Nps nps, final LocalObjects local) {
		addExp(exp, local);
	}

	@Override
	public void addMe(final PlayerShip playerShip, final LocalObjects local) {
		playerShip.sendPacket(ResponsePlayerShipInfo.getInstance(this, local), true);
		super.addMe(playerShip, local);
	}

	/**
	 * Добавить объект игроку.
	 * 
	 * @param object добавляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void addVisibleObject(final SpaceObject object, final LocalObjects local) {
		object.addMe(this, local);
	}

	@Override
	public void broadcastGlobalPacket(final ServerPacket packet) {
		packet.increaseSends();
		super.broadcastGlobalPacket(packet);
		sendPacket(packet, false);
	}

	@Override
	public void broadcastLocationPacket(final ServerPacket packet) {
		packet.increaseSends();
		super.broadcastLocationPacket(packet);
		sendPacket(packet, false);
	}

	@Override
	public void broadcastPacket(final ServerPacket packet) {
		packet.increaseSends();
		super.broadcastPacket(packet);
		sendPacket(packet, false);
	}

	@Override
	public boolean canAttack(final SpaceObject object) {

		if(!object.isSpaceShip()) {
			return false;
		}

		if(object.isPlayerShip()) {

			final PlayerShip target = object.getPlayerShip();

			if(PlayerUtils.isPartyFriend(this, target)) {
				return false;
			}

			return true;
		}

		final Nps nps = object.getNps();

		if(nps != null) {
			return nps.isEnemyNps() || nps.isStationDefender();
		}

		return false;
	}

	@Override
	protected RatingSystem createRatingSystem() {
		return new PlayerRatingSystem(this);
	}

	@Override
	protected void deleteMeImpl(LocalObjects local) {

		PLAYER_DB_MANAGER.updatePlayerShip(this);

		// сохраняем и очищаем настройки интерфейса
		saveAndClearInterface();
		saveAndClearSkillPanel();
		saveAndClearItemPanel();

		// запоминаем и очищаем состояние заданий
		saveAndClearQuests();
		// запоминаем изменения в рейтинге
		saveAndClearRatingSystem();

		// прерываем если есть действие игрока
		interruptedPlayerAction(local);
		// удаляем игрока из групы, если он там есть
		removeParty(local);

		// забываем станцию, на которой нахдоится игрок
		final AtomicReference<SpaceStation> stationReference = getStationReference();
		stationReference.set(null);

		super.deleteMeImpl(local);
	}

	/**
	 * Удаление из группы игрока, если он в ней состоит.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	protected void removeParty(LocalObjects local) {

		final AtomicReference<Party> reference = getPartyReference();

		synchronized(reference) {

			final Party party = reference.get();

			if(party != null) {
				party.removePlayer(this, local);
			}
		}
	}

	/**
	 * Прерываем взаимодействия игрока с кем-то игрока.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	protected void interruptedPlayerAction(LocalObjects local) {

		final AtomicReference<PlayerActionHandler> reference = getActionHandlerReference();

		synchronized(reference) {

			final PlayerActionHandler handler = reference.get();

			if(handler != null) {
				handler.handleCancel(local);
			}

			reference.set(null);
		}
	}

	/**
	 * Сохранение изменений и очистка рейтинговой системы.
	 */
	protected void saveAndClearRatingSystem() {
		final RatingSystem ratingSystem = getRatingSystem();
		ratingSystem.saveChanges();
	}

	/**
	 * Сохранение изменений и очистка списка заданий игрока.
	 */
	protected void saveAndClearQuests() {
		final QuestList questList = getQuestList();
		questList.synLock();
		try {
			questList.saveVars();
			questList.clear();
		} finally {
			questList.synUnlock();
		}
	}

	@Override
	protected void clearSkillStates() {

		final SkillStateList stateList = getSkillStateList();
		final Array<SkillStateInfo> states = stateList.getActualStates();
		states.forEach(storeSkillStates);

		super.clearSkillStates();
	}

	@Override
	public void doDestruct(final SpaceObject destroyer, final LocalObjects local) {
		super.doDestruct(destroyer, local);
		updateInfo(local);
	}

	@Override
	public void doUpdate(final LocalObjects local, final long currentTime) {

		final int forceShieldStrength = getCurrentShield();

		super.doUpdate(local, currentTime);

		if(forceShieldStrength != getCurrentShield()) {
			updateForceShieldStrength(local);
		}

		// float[] angles = new float[3];

		// getRotation().toAngles(angles);

		// System.out.println("rotation: " + Arrays.toString(angles));
	}

	@Override
	public void finalyze() {
		name = null;
		location.setXYZ(0, 0, 0);
		rotation.setXYZW(0, 0, 0, 1);
		super.finalyze();
	}

	/**
	 * Финализацич информации расположения элементов интерфейса.
	 */
	private void saveAndClearInterface() {

		final Array<InterfaceInfo> interfaceInfo = getInterfaceInfo();
		interfaceInfo.writeLock();
		try {

			for(final InterfaceInfo info : interfaceInfo.array()) {

				if(info == null) {
					break;
				}

				if(info.isChanged()) {
					PLAYER_DB_MANAGER.updateInterface(getObjectId(), info);
				}

				info.fold();
			}

			interfaceInfo.clear();

		} finally {
			interfaceInfo.writeUnlock();
		}
	}

	/**
	 * Финализация информации расположения предметов на панели.
	 */
	private void saveAndClearItemPanel() {

		final Array<ItemPanelInfo> itemPanelInfo = getItemPanelInfo();
		itemPanelInfo.writeLock();
		try {

			for(final ItemPanelInfo info : itemPanelInfo.array()) {

				if(info == null) {
					break;
				}

				if(info.isChanged()) {
					PLAYER_DB_MANAGER.updateItemPanel(getObjectId(), info);
				}

				info.fold();
			}

			itemPanelInfo.clear();

		} finally {
			itemPanelInfo.writeUnlock();
		}
	}

	/**
	 * Финализация информации расположения умений на панели.
	 */
	private void saveAndClearSkillPanel() {

		final Array<SkillPanelInfo> skillPanelInfo = getSkillPanelInfo();
		skillPanelInfo.writeLock();
		try {

			for(final SkillPanelInfo info : skillPanelInfo.array()) {

				if(info == null) {
					break;
				}

				if(info.isChanged()) {
					PLAYER_DB_MANAGER.updateSkillPanel(getObjectId(), info);
				}

				info.fold();
			}

			skillPanelInfo.clear();

		} finally {
			skillPanelInfo.writeUnlock();
		}
	}

	/**
	 * Поиск нужного окна ангара.
	 * 
	 * @param type тип искомого окна ангара.
	 * @return искомое окно.
	 */
	public <T extends HangarWindow> T findHangarWindow(final Class<T> type) {

		final Array<HangarWindow> widows = getOpeHangarWindows();

		if(widows.isEmpty()) {
			return null;
		}

		widows.readLock();
		try {

			for(final HangarWindow window : widows.array()) {

				if(window == null) {
					break;
				}

				if(type.isInstance(window)) {
					return type.cast(window);
				}
			}

		} finally {
			widows.readUnlock();
		}

		return null;
	}

	/**
	 * Получение уровня доступа к серверным командам этого игрока.
	 * 
	 * @return уровень доступа игрока.
	 */
	public int getAccessLevel() {
		return Command.USER;
	}

	/**
	 * @return ссылка на последнее действие.
	 */
	public AtomicReference<PlayerActionHandler> getActionHandlerReference() {
		return actionHandlerReference;
	}

	/**
	 * @return список активных квестов.
	 */
	public Array<QuestState> getActiveQuests() {
		return getQuestList().getActive();
	}

	@Override
	public PlayerShipAI getAI() {
		return (PlayerShipAI) ai;
	}

	@Override
	public int getClassId() {
		return Config.SERVER_PLAYER_SHIP_CLASS_ID;
	}

	/**
	 * @return client клиент игрока.
	 */
	public final GameClient getClient() {
		return client;
	}

	/**
	 * @return время входа в игру.
	 */
	public final long getEnterTime() {
		return enterTime;
	}

	/**
	 * @return кол-во опыта корабля.
	 */
	public long getExp() {
		return exp.get();
	}

	/**
	 * @return фракция игрока.
	 */
	public Fraction getFraction() {
		return fraction;
	}

	/**
	 * @return ид фракции игрока.
	 */
	public int getFractionId() {
		return getFraction().getId();
	}

	@Override
	public FriendStatus getFriendStatus(final PlayerShip ship, final LocalObjects local) {

		if(PlayerUtils.isPartyFriend(this, ship)) {
			return FriendStatus.PARTY;
		}

		return super.getFriendStatus(ship, local);
	}

	/**
	 * @return список настроек элементов интерфейса.
	 */
	public final Array<InterfaceInfo> getInterfaceInfo() {
		return interfaceInfo;
	}

	/**
	 * @return расположение предметов на панели игрока.
	 */
	public Array<ItemPanelInfo> getItemPanelInfo() {
		return itemPanelInfo;
	}

	/**
	 * @return список последних отправленных кнопок.
	 */
	public Array<QuestButton> getLastButtons() {
		return lastButtons;
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getNextShotId() {
		return SHOT_ID_GENERATOR.getNextId();
	}

	/**
	 * @return время онлайна.
	 */
	public final long getOnlineTime() {
		return onlineTime;
	}

	/**
	 * @return открытые окна ангара.
	 */
	public Array<HangarWindow> getOpeHangarWindows() {
		return opeHangarWindows;
	}

	/**
	 * @return текущая группа игрока.
	 */
	public Party getParty() {
		return partyReference.get();
	}

	/**
	 * @return ссылка на текущую группу игрока.
	 */
	public AtomicReference<Party> getPartyReference() {
		return partyReference;
	}

	@Override
	public PlayerShip getPlayerShip() {
		return this;
	}

	/**
	 * получить нужную квестовую кнопку из последних отправленнных.
	 * 
	 * @param questId ид квеста.
	 * @param buttonId ид кнопки.
	 * @return искомая кнопка.
	 */
	public QuestButton getQuestButton(final int questId, final int buttonId) {

		final Array<QuestButton> buttons = getLastButtons();

		if(buttons.isEmpty()) {
			return null;
		}

		buttons.readLock();
		try {

			for(final QuestButton button : buttons.array()) {

				if(button == null) {
					break;
				}

				if(button.getId() == buttonId && button.getQuest().getId() == questId) {
					return button;
				}
			}

		} finally {
			buttons.readUnlock();
		}

		return null;
	}

	/**
	 * @return список квестов.
	 */
	public QuestList getQuestList() {
		return questList;
	}

	/**
	 * @return список настроек расположения скилов игрока.
	 */
	public final Array<SkillPanelInfo> getSkillPanelInfo() {
		return skillPanelInfo;
	}

	/**
	 * @return ссылка на доступную станцию для телепортарования.
	 */
	public AtomicReference<SpaceStation> getStationReference() {
		return stationReference;
	}

	@Override
	public int getTypeId() {
		return template.getType().ordinal();
	}

	/**
	 * Увеличение уровня корабля игрока.
	 */
	public void increaseLevel(final LocalObjects local) {

		final PlayerManager playerManager = PlayerManager.getInstance();

		final int level = getLevel();

		if(level >= playerManager.getMaxLevel()) {
			return;
		}

		setLevel(level + 1);

		PacketUtils.updateLevel(local, this);
	}

	@Override
	public void invalidCoords(final LocalObjects local) {
		super.invalidCoords(local);
		client.close();
	}

	@Override
	public boolean isInvul() {
		return false;
	}

	@Override
	public boolean isNeedSendPacket() {
		return true;
	}

	@Override
	public boolean isPlayerShip() {
		return true;
	}

	/**
	 * Обработка вхождения корабля в область телепортации станции.
	 * 
	 * @param station станция, в область телепорта которую зашел корабль.
	 * @param local контейнер локальных объектов.
	 */
	public void onEnterStationRadius(final SpaceStation station, final LocalObjects local) {

		final AtomicReference<SpaceStation> reference = getStationReference();

		synchronized(reference) {

			if(reference.get() != null && reference.get() != station) {
				LOGGER.error(this, "can't enter to the station \"" + station + "\"");
				return;
			}

			reference.set(station);

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(getName() + " enter to the " + station.getName());
			}

			PacketUtils.updateStationInRange(local, station, this, true);
			MessageUtils.sendMessage(local, this, YOU_HAVE_ARRIVED_AT_THE_STATION, station.getName());
		}
	}

	/**
	 * Обработка выхода корабля из области телепортации станции.
	 * 
	 * @param station станция, из области телепорта которой вышел корабль.
	 * @param local контейнер локальных объектов.
	 */
	public void onExitStationRadius(final SpaceStation station, final LocalObjects local) {

		final AtomicReference<SpaceStation> reference = getStationReference();

		synchronized(reference) {

			if(reference.get() != station) {
				LOGGER.error(this, "can't exit to the station \"" + station + "\"");
				return;
			}

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(getName() + " exit from the " + station.getName());
			}

			reference.set(null);

			PacketUtils.updateStationInRange(local, station, this, false);
			MessageUtils.sendMessage(local, this, YOU_LEFT_THE_STATION, station.getName());
		}
	}

	@Override
	public void reinit() {
		super.reinit();
	}

	/**
	 * Удаление объекта у игрока.
	 * 
	 * @param object удаляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void removeVisibleObject(final SpaceObject object, final LocalObjects local) {
		object.removeMe(this, local);
	}

	/**
	 * Запуск разворота корабля с учетом модификатора.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param start статртовый разворот.
	 * @param end конечный разворот.
	 * @param infinity бесконечный ли разворот.
	 * @param modiff модификатор скорости.
	 */
	public void rotate(final LocalObjects local, final Rotation start, final Rotation end, final boolean infinity, final float modiff) {
		rotateTask.update(local, start, end, infinity, modiff);
	}

	@Override
	public void sendMessage(final String message, final LocalObjects local) {
		sendPacket(ResponseSayMessage.getInstance(MessageType.SYSTEM, StringUtils.EMPTY, message, local), true);
	}

	@Override
	public void sendPacket(final ServerConstPacket packet) {
		sendPacket(packet, false);
	}

	@Override
	public void sendPacket(final ServerPacket packet, final boolean increaseSend) {

		final GameClient client = getClient();

		if(client == null) {
			return;
		}

		client.sendPacket(packet, increaseSend);
	}

	@Override
	public void sendSystemMessage(final SystemMessageType messageType, final LocalObjects local) {
		sendPacket(ResponseSystemMessage.getInstance(messageType, local), true);
	}

	/**
	 * @param actionHandler последнее действие.
	 */
	public void setActionHandler(final PlayerActionHandler actionHandler) {
		this.actionHandlerReference.set(actionHandler);
	}

	/**
	 * @param client клиент игрока.
	 */
	public final void setClient(final GameClient client) {
		this.client = client;
	}

	@Override
	public void setCurrentSector(final SpaceSector currentSector) {
		super.setCurrentSector(currentSector);
	}

	/**
	 * @param enterTime время входа в игру.
	 */
	public final void setEnterTime(final long enterTime) {
		this.enterTime = enterTime;
	}

	/**
	 * @param exp кол-во опыта корабля.
	 */
	public void setExp(final long exp) {
		this.exp.set(exp);
	}

	/**
	 * @param fraction фракция игрока.
	 */
	public void setFraction(final Fraction fraction) {
		this.fraction = fraction;
	}

	/**
	 * @param level уровень корабля игрока.
	 */
	public void setLevel(final int level) {
		this.level = level;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param onlineTime время онлайна.
	 */
	public final void setOnlineTime(final long onlineTime) {
		this.onlineTime = onlineTime;
	}

	/**
	 * @param party текущая группа игрокаю
	 */
	public void setParty(final Party party) {
		this.partyReference.set(party);
	}

	@Override
	public void spawnMe(final LocalObjects local) {
		super.spawnMe(local);

		final AtomicReference<SpaceStation> reference = getStationReference();

		synchronized(reference) {

			final SpaceStation station = reference.get();

			if(station != null) {
				onEnterStationRadius(station, local);
			}
		}
	}

	@Override
	public void teleportTo(final Vector position, final Rotation rotation, final LocalObjects local, final int locationId) {
		super.teleportTo(position, rotation, local, locationId);
		sendPacket(ResponsePlayerShipTeleport.getInstance(this), true);
	}

	@Override
	public String toString() {
		return "PlayerShip [name=" + name + "]";
	}

	@Override
	public void updateForceShieldStrength(final LocalObjects local) {
		super.updateForceShieldStrength(local);

		PacketUtils.updateStatus(local, this);

		final Party party = getParty();

		if(party != null) {
			party.updateMember(this, false, true, local);
		}
	}

	@Override
	public void updateInfo(final LocalObjects local) {

		PacketUtils.updateUserInfo(local, this);

		super.updateInfo(local);
	}

	/**
	 * Обновление позиции элемента интерфейса.
	 * 
	 * @param type тип элемента.
	 * @param x координата элемента.
	 * @param y координата элемента.
	 * @param minimized свернут ли элемент.
	 */
	public void updateInterface(final int type, final int x, final int y, final boolean minimized) {

		final Array<InterfaceInfo> interfaceInfo = getInterfaceInfo();

		// если настройка этого интерфейса уже есть
		interfaceInfo.readLock();
		try {

			final InterfaceInfo[] array = interfaceInfo.array();

			for(int i = 0, length = interfaceInfo.size(); i < length; i++) {

				final InterfaceInfo info = array[i];

				if(info == null || info.getType() != type) {
					continue;
				}

				info.update(x, y, minimized);
				return;
			}

		} finally {
			interfaceInfo.readUnlock();
		}

		// если это впервый раз обновляем
		interfaceInfo.writeLock();
		try {

			final InterfaceInfo info = InterfaceInfo.newInstance(type, x, y, minimized);
			interfaceInfo.add(info);

			PLAYER_DB_MANAGER.addNewInterface(getObjectId(), info);

		} finally {
			interfaceInfo.writeUnlock();
		}
	}

	/**
	 * Обновление положения предмета на панели игрока.
	 * 
	 * @param objectId уникальный ид предмета.
	 * @param order позиция скила.
	 */
	public final void updateItemPanel(final int objectId, final int oldOrder, final int newOrder) {

		final Array<ItemPanelInfo> itemPanelInfo = getItemPanelInfo();

		if(newOrder != -1 && newOrder == oldOrder) {
			return;
		}

		// если игрок убрал предмет с панели
		if(newOrder == -1) {

			itemPanelInfo.writeLock();
			try {

				for(final ItemPanelInfo info : itemPanelInfo.array()) {

					if(info == null) {
						break;
					}

					if(info.getObjectId() == objectId && info.getOrder() == oldOrder) {
						PLAYER_DB_MANAGER.removeItemPanel(getObjectId(), info);
						itemPanelInfo.fastRemove(info);
						info.fold();
						break;
					}
				}

			} finally {
				itemPanelInfo.writeUnlock();
			}
		}
		// если предмет переместился на панель либо на панели место сменил
		else {

			itemPanelInfo.readLock();
			try {

				for(final ItemPanelInfo info : itemPanelInfo.array()) {

					if(info == null) {
						break;
					}

					if(info.getObjectId() == objectId && info.getOrder() == oldOrder) {
						info.update(newOrder);
						return;
					}
				}

			} finally {
				itemPanelInfo.readUnlock();
			}

			// если сюда дошли, значит предмет по новому на панель перенесли
			itemPanelInfo.writeLock();
			try {

				final ItemPanelInfo info = ItemPanelInfo.newInstance(objectId, Config.SERVER_ITEM_CLASS_ID, newOrder);
				itemPanelInfo.add(info);

				PLAYER_DB_MANAGER.addNewItemPanel(getObjectId(), info);

			} finally {
				itemPanelInfo.writeUnlock();
			}
		}
	}

	/**
	 * Обновление положения скила на панели игрока.
	 * 
	 * @param objectId уникальный ид модуля.
	 * @param skillId уникальный ид скила.
	 * @param order позиция скила.
	 */
	public final void updateSkillPanel(final int objectId, final int skillId, final int oldOrder, final int newOrder) {

		final Array<SkillPanelInfo> skillPanelInfo = getSkillPanelInfo();

		// если игрок убрал скил с панели
		if(newOrder == -1) {

			skillPanelInfo.writeLock();
			try {

				for(final SkillPanelInfo info : skillPanelInfo.array()) {

					if(info == null) {
						break;
					}

					final Skill skill = info.getSkill();

					if(skill.getModuleObjectId() == objectId && skill.getId() == skillId && info.getOrder() == oldOrder) {
						PLAYER_DB_MANAGER.removeSkillPanel(getObjectId(), info);
						skillPanelInfo.fastRemove(info);
						info.fold();
						break;
					}
				}

			} finally {
				skillPanelInfo.writeUnlock();
			}
		}
		// если скил переместился на панель либо на панели место сменил
		else {

			skillPanelInfo.readLock();
			try {

				for(final SkillPanelInfo info : skillPanelInfo.array()) {

					if(info == null) {
						break;
					}

					final Skill skill = info.getSkill();

					if(skill.getModuleObjectId() == objectId && skill.getId() == skillId && info.getOrder() == oldOrder) {
						info.update(newOrder);
						return;
					}
				}

			} finally {
				skillPanelInfo.readUnlock();
			}

			// если сюда дошли, значит скил по новому на панель перенесли
			final Skill skill = getSkill(objectId, skillId);

			if(skill == null) {
				return;
			}

			skillPanelInfo.writeLock();
			try {

				final SkillPanelInfo info = SkillPanelInfo.newInstance(skill, newOrder);
				skillPanelInfo.add(info);

				PLAYER_DB_MANAGER.addNewSkillPanel(getObjectId(), info);

			} finally {
				skillPanelInfo.writeUnlock();
			}
		}
	}

	@Override
	public void updateStatus(final LocalObjects local) {
		super.updateStatus(local);

		PacketUtils.updateStatus(local, this);

		final Party party = getParty();

		if(party != null) {
			party.updateMember(this, false, true, local);
		}
	}

	@Override
	public void updateStrength(final LocalObjects local) {
		super.updateStrength(local);

		PacketUtils.updateStatus(local, this);

		final Party party = getParty();

		if(party != null) {
			party.updateMember(this, false, true, local);
		}
	}

	/**
	 * @return находится ли сейчас корабль на космической станции.
	 */
	public AtomicBoolean getOnStation() {
		return onStation;
	}

}

package com.ss.server.model.ship.player.action.impl;

import java.util.concurrent.atomic.AtomicReference;

import com.ss.server.LocalObjects;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.SystemMessageVars;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerAction;
import com.ss.server.model.social.party.Party;
import com.ss.server.model.social.party.PartyFactory;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;

/**
 * Действие по приглашению игрока в группу.
 * 
 * @author Ronn
 */
public class PartyInvitePlayerActionHandler extends PlayerToPlayerActionHandler {

	@Override
	protected boolean checkCondition(final PlayerShip playerShip, final PlayerShip target, final LocalObjects local) {

		final AtomicReference<Party> targetPartyRef = target.getPartyReference();

		if(targetPartyRef.get() != null) {
			playerShip.sendSystemMessage(SystemMessageType.PLAYER_ALREADY_IN_PARTY, local);
			return false;
		}

		final Party party = playerShip.getParty();

		if(party != null && party.isFull()) {
			playerShip.sendSystemMessage(SystemMessageType.PARTY_ALREADY_IS_FULL, local);
			return false;
		}

		return true;
	}

	@Override
	public PlayerAction getType() {
		return PlayerAction.INVITE_TO_PARTY;
	}

	@Override
	protected boolean handleAcceptImpl(final PlayerShip playerShip, final PlayerShip target, final LocalObjects local) {

		final AtomicReference<Party> playerPartyRef = playerShip.getPartyReference();
		final AtomicReference<Party> targetPartyRef = target.getPartyReference();

		synchronized(playerPartyRef) {
			synchronized(targetPartyRef) {

				if(!checkCondition(playerShip, target, local)) {
					return false;
				}

				Party party = playerPartyRef.get();

				if(party != null) {
					party.addPlayer(playerShip, target, local);
				} else {
					party = PartyFactory.takeParty();
					party.init(playerShip, target, local);
				}
			}
		}

		ResponseSystemMessage responseSystemMessage = ResponseSystemMessage.getInstance(SystemMessageType.PARTY_WAS_ADDED, local);
		responseSystemMessage.addVar(SystemMessageVars.VAR_OBJECT_NAME, target.getName());

		playerShip.sendPacket(responseSystemMessage, true);

		responseSystemMessage = ResponseSystemMessage.getInstance(SystemMessageType.YOU_HAVE_BEEN_ADDED_PARTY, local);
		responseSystemMessage.addVar(SystemMessageVars.VAR_OBJECT_NAME, playerShip.getName());

		target.sendPacket(responseSystemMessage, true);
		return true;
	}

	@Override
	protected boolean handleRejectImpl(final PlayerShip playerShip, final PlayerShip target, final LocalObjects local) {
		return false;
	}
}

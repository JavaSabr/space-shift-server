package com.ss.server.model.ship.player;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

/**
 * Модель хранение информации об интерфейсе игрока.
 * 
 * @author Ronn
 */
public class InterfaceInfo implements Foldable {

	public static InterfaceInfo newInstance(final int type, final int x, final int y, final boolean minimized) {

		InterfaceInfo info = pool.take();

		if(info == null) {
			info = new InterfaceInfo();
		}

		info.type = type;
		info.x = x;
		info.y = y;
		info.minimized = minimized;

		return info;
	}

	private static final FoldablePool<InterfaceInfo> pool = PoolFactory.newAtomicFoldablePool(InterfaceInfo.class);

	/** тип элемента */
	private int type;
	/** координата элемента */
	private int x;
	/** координата элемента */
	private int y;
	/** счетчик обновлений */
	private int update;

	/** свернут ли элемент */
	private boolean minimized;

	private InterfaceInfo() {
		super();
	}

	@Override
	public void finalyze() {
		update = 0;
	}

	/**
	 * Складировать в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return тип элемента.
	 */
	public final int getType() {
		return type;
	}

	/**
	 * @return координата элемента.
	 */
	public final int getX() {
		return x;
	}

	/**
	 * @return координата элемента.
	 */
	public final int getY() {
		return y;
	}

	/**
	 * @return были ли обновления.
	 */
	public boolean isChanged() {
		return update > 0;
	}

	/**
	 * @return свернут ли элемент.
	 */
	public boolean isMinimized() {
		return minimized;
	}

	@Override
	public void reinit() {
		minimized = false;
	}

	/**
	 * @param minimized свернут ли элемент.
	 */
	public void setMinimized(final boolean minimized) {
		this.minimized = minimized;
	}

	/**
	 * Обновить положение.
	 * 
	 * @param x новая координата.
	 * @param y новая координата.
	 * @param minimized свернут ли элемент.
	 */
	public void update(final int x, final int y, final boolean minimized) {
		this.x = x;
		this.y = y;
		this.minimized = minimized;
		this.update++;
	}
}

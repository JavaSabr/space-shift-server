package com.ss.server.model.ship.player;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.model.faction.Fraction;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Модель превью корабля для ангара.
 * 
 * @author Ronn
 */
public final class PlayerShipPreview implements Foldable {

	public static PlayerShipPreview newInstance() {

		PlayerShipPreview preview = pool.take();

		if(preview == null) {
			preview = new PlayerShipPreview();
		}

		return preview;
	}

	private static final FoldablePool<PlayerShipPreview> pool = PoolFactory.newAtomicFoldablePool(PlayerShipPreview.class);

	/** название корабля */
	private String name;
	/** фракция корабля */
	private Fraction fraction;
	/** темплейт корабля */
	private PlayerShipTemplate template;

	/** система модулей корабля */
	private ModuleSystem moduleSystem;

	/** уникальный ид объекта */
	private int objectId;

	/** текущее состояние корпуса */
	private int currentStrength;
	/** максимальная прочность корпуса */
	private int maxStrength;

	/** текщие состояние щитов */
	private int currentShield;
	/** максимальная прочность щита */
	private int maxShield;

	@Override
	public void finalyze() {
		this.name = null;
		this.template.putModuleSystem(moduleSystem);
		this.moduleSystem = null;
	}

	/**
	 * Складировать в пул.
	 */
	public void fold() {
		pool.put(this);
	}

	/**
	 * @return текщие состояние щитов.
	 */
	public int getCurrentShield() {
		return currentShield;
	}

	/**
	 * @return текущее состояние корпуса.
	 */
	public int getCurrentStrength() {
		return currentStrength;
	}

	/**
	 * @return фракция корабля.
	 */
	public Fraction getFraction() {
		return fraction;
	}

	/**
	 * @return максимальная прочность щита.
	 */
	public int getMaxShield() {
		return maxShield;
	}

	/**
	 * @return максимальная прочность корпуса.
	 */
	public int getMaxStrength() {
		return maxStrength;
	}

	/**
	 * @return система модулей.
	 */
	public final ModuleSystem getModuleSystem() {
		return moduleSystem;
	}

	/**
	 * @return имя корабля.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return уникальный ид модели.
	 */
	public final int getObjectId() {
		return objectId;
	}

	/**
	 * @return темплейт корабля.
	 */
	public final PlayerShipTemplate getTemplate() {
		return template;
	}

	@Override
	public void reinit() {
		this.objectId = 0;
	}

	/**
	 * @param currentShield текщие состояние щитов.
	 */
	public void setCurrentShield(final int currentShield) {
		this.currentShield = currentShield;
	}

	/**
	 * @param currentStrength текущее состояние корпуса.
	 */
	public void setCurrentStrength(final int currentStrength) {
		this.currentStrength = currentStrength;
	}

	/**
	 * @param fraction фракция корабля.
	 */
	public void setFraction(final Fraction fraction) {
		this.fraction = fraction;
	}

	/**
	 * @param maxShield максимальная прочность щита.
	 */
	public void setMaxShield(final int maxShield) {
		this.maxShield = maxShield;
	}

	/**
	 * @param maxStrength максимальная прочность корпуса.
	 */
	public void setMaxStrength(final int maxStrength) {
		this.maxStrength = maxStrength;
	}

	/**
	 * @param moduleSystem система модулей.
	 */
	public final void setModuleSystem(final ModuleSystem moduleSystem) {
		this.moduleSystem = moduleSystem;
	}

	/**
	 * @param name имя корабля.
	 */
	public final void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param objectId уникальный ид модели.
	 */
	public final void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	/**
	 * @param template темплейт корабля.
	 */
	public final void setTemplate(final PlayerShipTemplate template) {
		this.template = template;
	}
}

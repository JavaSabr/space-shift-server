package com.ss.server.model.ship.player.action.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.ship.player.action.PlayerActionHandler;
import com.ss.server.model.ship.player.action.PlayerActionHandlerFactory;
import com.ss.server.network.game.packet.server.ResponsePlayerActionAccepted;
import com.ss.server.network.game.packet.server.ResponsePlayerActionRejected;

/**
 * Базовая реализация обработчика действий игрока.
 * 
 * @author Ronn
 */
public abstract class AbstractPlayerActionHandler<T extends SpaceObject> implements PlayerActionHandler {

	private static final Logger LOGGER = LoggerManager.getLogger(PlayerActionHandler.class);

	/** корабль игрока */
	private PlayerShip playerShip;
	/** цель действия */
	private T target;

	/**
	 * Проверка целевого объекта на соотвествии его к обработчику.
	 * 
	 * @param target целевой объект.
	 * @return соотвествует ли он обработчику.
	 */
	protected abstract boolean checkTarget(SpaceObject target);

	@Override
	public void finalyze() {
		// TODO Auto-generated method stub

	}

	/**
	 * @return корабль игрока.
	 */
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * @return цель действия.
	 */
	public T getTarget() {
		return target;
	}

	@Override
	public void handleAccept(final LocalObjects local) {

		final PlayerShip playerShip = getPlayerShip();

		if(playerShip != null) {
			playerShip.setActionHandler(null);
			playerShip.sendPacket(ResponsePlayerActionAccepted.getInstance(), true);
		}

		PlayerActionHandlerFactory.saveHandler(this);
	}

	@Override
	public void handleCancel(final LocalObjects local) {

		final PlayerShip playerShip = getPlayerShip();

		if(playerShip != null) {
			playerShip.setActionHandler(null);
		}

		PlayerActionHandlerFactory.saveHandler(this);
	}

	@Override
	public void handleReject(final LocalObjects local) {

		final PlayerShip playerShip = getPlayerShip();

		if(playerShip != null) {
			playerShip.setActionHandler(null);
			playerShip.sendPacket(ResponsePlayerActionRejected.getInstance(), true);
		}

		PlayerActionHandlerFactory.saveHandler(this);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void handleRequest(final PlayerShip playerShip, final SpaceObject target, final LocalObjects local) {
		if(checkTarget(target)) {
			handleRequestImpl(playerShip, (T) target, local);
		} else {
			LOGGER.warning(this, new Exception("incorrect target object " + target));
		}
	}

	/**
	 * Обработка запроса на действия игрока.
	 * 
	 * @param playerShip корабль игрока.
	 * @param target целево объект.
	 * @param local контейнер локальных объектов.
	 */
	protected abstract void handleRequestImpl(PlayerShip playerShip, T target, LocalObjects local);

	@Override
	public void reinit() {
	}

	/**
	 * @param playerShip корабль игрока.
	 */
	public void setPlayerShip(final PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	/**
	 * @param target цель действия.
	 */
	public void setTarget(final T target) {
		this.target = target;
	}
}

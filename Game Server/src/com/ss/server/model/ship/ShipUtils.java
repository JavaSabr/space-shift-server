package com.ss.server.model.ship;

import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.ActivableSkill;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.SkillGroup;
import com.ss.server.model.skills.SkillType;
import com.ss.server.model.skills.impl.ModuleEngineSkill;

/**
 * Класс с утильными методами по работе с космическими корабля.
 * 
 * @author Ronn
 */
public final class ShipUtils {

	/**
	 * Активация двигателей корабля.
	 * 
	 * @param ship корабль, которому нужно деактивировать двигатели.
	 * @param local контейнер локальных объектов.
	 */
	public static void activateEngines(final SpaceShip ship, final LocalObjects local) {

		final Table<IntKey, Skill[]> skillTable = ship.getSkillTable();

		skillTable.readLock();
		try {

			for(final Skill[] skills : skillTable) {
				for(final Skill skill : skills) {

					if(skill.getSkillType() != SkillType.ENGINE) {
						continue;
					}

					final ModuleEngineSkill engine = (ModuleEngineSkill) skill;

					if(engine.isActive()) {
						continue;
					}

					ship.getAI().startSkill(ship.getLocation(), engine, local);
				}
			}

		} finally {
			skillTable.readUnlock();
		}
	}

	/**
	 * Есть ли у указанного корабля возможность активировать щит.
	 */
	public static boolean canActivateForceShield(final SpaceShip spaceShip) {

		final Nps nps = spaceShip.getNps();

		if(nps != null) {

			final NpsAI ai = nps.getAI();

			final Skill[] skills = ai.getSkills(SkillGroup.FORCE_SHIELD);

			if(skills == null || skills.length < 1) {
				return false;
			}

			for(final Skill skill : skills) {

				if(!(skill instanceof ActivableSkill)) {
					continue;
				}

				final ActivableSkill activableSkill = (ActivableSkill) skill;

				if(activableSkill.isActive() || activableSkill.isReloading()) {
					continue;
				}

				return true;
			}
		}

		return false;
	}

	/**
	 * Деактивация двигателей корабля.
	 * 
	 * @param ship корабль, которому нужно деактивировать двигатели.
	 * @param local контейнер локальных объектов.
	 */
	public static void deactivateEngines(final SpaceShip ship, final LocalObjects local) {

		final Table<IntKey, Skill[]> skillTable = ship.getSkillTable();

		skillTable.readLock();
		try {

			for(final Skill[] skills : skillTable) {
				for(final Skill skill : skills) {

					if(skill.getSkillType() != SkillType.ENGINE) {
						continue;
					}

					final ModuleEngineSkill engine = (ModuleEngineSkill) skill;

					if(!engine.isActive()) {
						continue;
					}

					ship.getAI().startSkill(ship.getLocation(), engine, local);
				}
			}

		} finally {
			skillTable.readUnlock();
		}
	}

	/**
	 * Есть ли у указанного корабля активные двигатели.
	 */
	public static boolean isActiveEngines(final SpaceShip spaceShip) {

		final Table<IntKey, Skill[]> skillTable = spaceShip.getSkillTable();

		skillTable.readLock();
		try {

			for(final Skill[] skills : skillTable) {
				for(final Skill skill : skills) {

					if(skill.getSkillType() != SkillType.ENGINE) {
						continue;
					}

					final ModuleEngineSkill engine = (ModuleEngineSkill) skill;

					if(engine.isActive()) {
						return true;
					}
				}
			}

		} finally {
			skillTable.readUnlock();
		}

		return false;
	}

	/**
	 * Есть ли у указанного корабля активные силовые щиты.
	 */
	public static boolean isActiveForceShield(final SpaceShip spaceShip) {
		final Array<ForceShield> forceShields = spaceShip.getForceShields();
		return !forceShields.isEmpty();
	}

	private ShipUtils() {
		throw new RuntimeException();
	}
}

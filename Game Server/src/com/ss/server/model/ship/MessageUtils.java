package com.ss.server.model.ship;

import com.ss.server.LocalObjects;
import com.ss.server.model.SystemMessageType;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;

/**
 * Набор утильных методов по отправке сообщений.
 * 
 * @author Ronn
 */
public class MessageUtils {

	/**
	 * Отаправка кораблю системного сообщения.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param ship корабль, которому надо отправить сообщение.
	 * @param messageType тип системного сообщения.
	 */
	public static void sendMessage(final LocalObjects local, final SpaceShip ship, final SystemMessageType messageType) {
		ship.sendPacket(ResponseSystemMessage.getInstance(messageType, local), true);
	}

	/**
	 * Отаправка кораблю системного сообщения.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param ship корабль, которому надо отправить сообщение.
	 * @param messageType тип системного сообщения.
	 * @param value значение для переменной сообщения.
	 */
	public static void sendMessage(final LocalObjects local, final SpaceShip ship, final SystemMessageType messageType, final String value) {

		final String[] varNames = messageType.getVars();

		final ResponseSystemMessage message = ResponseSystemMessage.getInstance(messageType, local);
		message.addVar(varNames[0], value);

		ship.sendPacket(message, true);
	}

	/**
	 * Отаправка кораблю системного сообщения.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param ship корабль, которому надо отправить сообщение.
	 * @param messageType тип системного сообщения.
	 * @param value1 значение для переменной сообщения.
	 * @param value2 значение для переменной сообщения.
	 */
	public static void sendMessage(final LocalObjects local, final SpaceShip ship, final SystemMessageType messageType, final String value1, final String value2) {

		final String[] varNames = messageType.getVars();

		final ResponseSystemMessage message = ResponseSystemMessage.getInstance(messageType, local);
		message.addVar(varNames[0], value1);
		message.addVar(varNames[1], value2);

		ship.sendPacket(message, true);
	}

	/**
	 * Отаправка кораблю системного сообщения.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param ship корабль, которому надо отправить сообщение.
	 * @param messageType тип системного сообщения.
	 * @param value1 значение для переменной сообщения.
	 * @param value2 значение для переменной сообщения.
	 * @param value3 значение для переменной сообщения.
	 */
	public static void sendMessage(final LocalObjects local, final SpaceShip ship, final SystemMessageType messageType, final String value1, final String value2, final String value3) {

		final String[] varNames = messageType.getVars();

		final ResponseSystemMessage message = ResponseSystemMessage.getInstance(messageType, local);
		message.addVar(varNames[0], value1);
		message.addVar(varNames[1], value2);
		message.addVar(varNames[2], value3);

		ship.sendPacket(message, true);
	}
}

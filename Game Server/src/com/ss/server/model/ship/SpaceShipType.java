package com.ss.server.model.ship;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.ship.nps.impl.DefaultNps;
import com.ss.server.model.ship.nps.impl.DefaultStationDefenderNps;
import com.ss.server.model.ship.nps.impl.EnemyDron;
import com.ss.server.model.ship.nps.impl.FriendlyDron;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Перечисление типов космических кораблей.
 * 
 * @author Ronn
 */
public enum SpaceShipType implements SpaceObjectType {
	DEFAULT_NPS(DefaultNps.class),
	PLAYER_SHIP(PlayerShip.class),
	ENEMY_DRON(EnemyDron.class),
	FRIENDLY_DRON(FriendlyDron.class),
	STATION_DEFENDER(DefaultStationDefenderNps.class), ;

	/** типобъектов */
	private Class<? extends SpaceShip> instanceClass;

	private SpaceShipType(final Class<? extends SpaceShip> instanceClass) {
		this.instanceClass = instanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public int index() {
		return ordinal();
	}
}

package com.ss.server.model.skills;

import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.shot.Shot;

/**
 * Интерфейс для реализации скилы стрельбы.
 * 
 * @author Ronn
 */
public interface ShotSkill extends Skill {

	/**
	 * Приминение выстрела на цель.э
	 * 
	 * @param shot попавший выстрел.
	 * @param shooter стреляющий корабль.
	 * @param target цель, в которую попал выстрел.
	 * @param local контейнер локальных объектов.
	 * @return остановлен ли выстрел.
	 */
	public boolean applySkill(Shot shot, SpaceShip shooter, SpaceObject target, LocalObjects local);

	/**
	 * Получение итоговой стартовой позиции выстрела этим умением с учетом
	 * расположения модуля у корабля.
	 * 
	 * @param position контейнер для хранения данных позиции.
	 * @return итоговая стартовая позиция.
	 */
	public Vector getStartPosition(Vector position);
}

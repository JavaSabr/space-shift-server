package com.ss.server.model.skills;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Перечисление статов параметров умений.
 * 
 * @author Ronn
 */
public enum SkillStatType {

	/** ----------- характеристики полета ----------------- */

	/** максимальная скорость перемещения корабля */
	MAX_FLY_SPEED("@skill-stat-type:max-fly-speed@"),
	/** ускорение корабля */
	FLY_ACCEL("@skill-stat-type:fly-accel@"),
	/** скорость разворота корабля */
	ROTATE_SPEED("@skill-stat-type:rotate-speed@"),

	/** характеристики поражения */
	DISTANCE("@skill-stat-type:distance@"),
	POWER("@skill-stat-type:power@"),

	/** характеристики темпа использования */
	/** время перезарядки умения */
	REUSE_DELAY("@skill-stat-type:reuse-delay@"),
	/** минимальное время перезарядки */
	MIN_REUSE_DELAY("@skill-stat-type:min-reuse-delay@"),
	/** максимальное время перезарядки */
	MAX_REUSE_DELAY("@skill-stat-type:max-reuse-delay@"),
	/** максимальное время действия */
	MAX_ACTIVE_TIME("@skill-stat-type:max-active-time@"),

	/** характеристики потребления */
	REUSE_CONSUME("@skill-stat-type:reuse-consume@"),
	ENERGY_CONSUME("@skill-stat-type:energy-consume@"),
	ITEM_CONSUME_NAME("@skill-stat-type:item-consume-name@") {

		@Override
		public String convert(final String value) {

			final ItemTable itemTable = ItemTable.getInstance();
			final ItemTemplate template = itemTable.getTemplate(Integer.parseInt(value));

			if(template == null) {
				LOGGER.warning("not found item consume for " + value);
				return value;
			}

			return template.getName();
		}
	},

	/** остальные */
	/** максимальная прочность */
	MAX_STRENGTH("@skill-stat-type:max-strength@"),
	/** скорость восстановления */
	REGENERATION("@skill-stat-type:regeneration@"),

	;

	private static final Logger LOGGER = LoggerManager.getLogger(SkillStatType.class);

	public static final int LENGTH = values().length;

	private String langName;

	private SkillStatType(final String langName) {
		this.langName = langName;
	}

	/**
	 * Конвектирование значения.
	 * 
	 * @param value текущее значение.
	 * @return нужное значение.
	 */
	public String convert(final String value) {
		return value;
	}

	/**
	 * @return название стата для локализации.
	 */
	public String getLangName() {
		return langName;
	}
}

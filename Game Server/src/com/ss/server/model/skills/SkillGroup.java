package com.ss.server.model.skills;

/**
 * Перечисление групп скилов.
 * 
 * @author Ronn
 */
public enum SkillGroup {

	ENGINE,
	BLASTER,
	BLASTER_SHIELD_PIERCER,
	BLASTER_BODY_PIERCER,
	ROCKET,
	ROCKET_SHIELD_PIERCER,
	ROCKET_BODY_PIERCER,
	ENGINE_GENERATOR,
	FORCE_SHIELD, ;

	public static final SkillGroup[] VALUES = values();

	public static final int SIZE = VALUES.length;
}

package com.ss.server.model.skills;

import com.ss.server.LocalObjects;

/**
 * Интерфейсд ля реализаций умений с временной активностью.
 * 
 * @author Ronn
 */
public interface TimeActiveSkill extends Skill {

	/**
	 * Отмена работы умения.
	 */
	public void cancel(LocalObjects local);

	/**
	 * @return активно ли сейчас умение.
	 */
	public boolean isActive();

	/**
	 * Обработка завершения по истечению времени.
	 */
	public void timeFinish(LocalObjects local);
}

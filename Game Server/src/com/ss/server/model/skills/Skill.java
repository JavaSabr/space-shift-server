package com.ss.server.model.skills;

import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.SkillTemplate;

/**
 * Интерфейс для реализации скила модуля.
 * 
 * @author Ronn
 */
public interface Skill extends Foldable {

	/**
	 * Приминение умения на объект
	 * 
	 * @param target цель, на который будет применено умение.
	 * @param local контейнер локальных объектов.
	 * @return информация о ризультате приминения.
	 */
	public default ApplyInfo applySkill(final SpaceObject target, final LocalObjects local) {
		throw new RuntimeException("not implemented.");
	}

	/**
	 * Проверка условий перед использованием скила.
	 * 
	 * @param target объект, который является целью умения.
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 * @return выполнены ли все необходимые условия.
	 */
	public boolean checkCondition(final SpaceObject target, LocalObjects local, long currentTime);

	/**
	 * Складировать в пул.
	 */
	public default void fold() {
		getTemplate().put(this);
	}

	/**
	 * @return ускорение.
	 */
	public default int getAccel() {
		return getTemplate().getAccel();
	}

	/**
	 * @return время действия умения.
	 */
	public default int getActiveTime() {
		return getTemplate().getActiveTime();
	}

	/**
	 * @return текущий заряд.
	 */
	public int getCharge();

	/**
	 * @return тип урона умения.
	 */
	public default DamageType getDamageType() {
		return getTemplate().getDamageType();
	}

	/**
	 * @return скорострельность.
	 */
	public default int getFireRate() {
		return getTemplate().getFireRate();
	}

	/**
	 * @return ид скила.
	 */
	public default int getId() {
		return getTemplate().getId();
	}

	/**
	 * @return кол-во потребляемого предмета.
	 */
	public default int getItemConsumeCount() {
		return getTemplate().getItemConsumeCount();
	}

	/**
	 * @return ид потребляемого предмета.
	 */
	public default int getItemConsumeId() {
		return getTemplate().getItemConsumeId();
	}

	/**
	 * @return максимальный заряд.
	 */
	public default int getMaxCharge() {
		return getTemplate().getMaxCharge();
	}

	/**
	 * @return максимальная дистанция скила.
	 */
	public default int getMaxDistance() {
		return getTemplate().getMaxDistance();
	}

	/**
	 * @return максимальная скорость скила.
	 */
	public default int getMaxSpeed() {
		return getTemplate().getMaxSpeed();
	}

	/**
	 * @return минимальное время отката.
	 */
	public default int getMinReuseDelay() {
		return getTemplate().getMinReloadDelay();
	}

	/**
	 * @return модуль, который реализовывает умение.
	 */
	public Module getModule();

	/**
	 * @return уникальный ид модуля.
	 */
	public int getModuleObjectId();

	/**
	 * @return мощность умения.
	 */
	public default int getPower() {
		return getTemplate().getPower();
	}

	/**
	 * @return время перезарядки умения.
	 */
	public default int getReloadDelay() {
		return getTemplate().getReloadDelay();
	}

	/**
	 * @return время завершения перезарядки.
	 */
	public long getReloadFinishTime();

	/**
	 * @return индетификатор перезагрузки.
	 */
	public default int getReloadId() {
		return getTemplate().getReloadId();
	}

	/**
	 * @return время перезарядки.
	 */
	public int getReloadTime();

	/**
	 * @return потребление времени перезарядки на время активности.
	 */
	public default float getReloadConsume() {
		return getTemplate().getReloadConsume();
	}

	/**
	 * @return скорость разворота.
	 */
	public default float getRotationSpeed() {
		return getTemplate().getRotationSpeed();
	}

	/**
	 * @return группа умения.
	 */
	public default SkillGroup getSkillGroup() {
		return getTemplate().getSkillGroup();
	}

	/**
	 * @return тип умения.
	 */
	public default SkillType getSkillType() {
		return getTemplate().getSkillType();
	}

	/**
	 * @return шаблон умения.
	 */
	public SkillTemplate getTemplate();

	/**
	 * Инициализация умения под указанный корабль.
	 * 
	 * @param ship корабль к которому было добавлено умение.
	 * @param local контейнер локальных объектов.
	 */
	public default void init(final SpaceShip ship, final LocalObjects local) {
	}

	/**
	 * @return является ли умение прямой атакой.
	 */
	public default boolean isDirectionAttack() {
		return getTemplate().isDirectionAttack();
	}

	/**
	 * @return надо ли перезагрузить умение.
	 */
	public default boolean isNeedReloading() {
		return true;
	}

	/**
	 * @return находится ли сейчас в процессе перезарядки.
	 */
	public boolean isReloading();

	/**
	 * Обработка потребления умения.
	 * 
	 * @param target объект на который будет использовано умение.
	 * @param local контейнер локальных объектов.
	 * @return успешно ли произошло потребление.
	 */
	public boolean postConsume(SpaceObject target, LocalObjects local);

	/**
	 * Обработка потребления умения.
	 * 
	 * @param target объект на который будет использовано умение.
	 * @param local контейнер локальных объектов.
	 * @return успешно ли произошло потребление.
	 */
	public boolean preConsume(SpaceObject target, LocalObjects local);

	/**
	 * @param module модуль, который реализовывает умение.
	 */
	public void setModule(Module module);

	/**
	 * Запуск перезарядки умения.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public default void startReload(final LocalObjects local) {
	}

	/**
	 * Обновление состояния умения.
	 * 
	 * @param currentTime текущее время.
	 * @param local контейнер локальных объектов.
	 */
	public default void update(final long currentTime, final LocalObjects local) {
	}

	/**
	 * @return кол-во регенерирующего заряда за 1 интервал.
	 */
	public default int getChargeRegen() {
		return getTemplate().getChargeRegen();
	}

	/**
	 * @return интервал регенерации заряда.
	 */
	public default int getChargeRegenInterval() {
		return getTemplate().getChargeRegenInterval();
	}

	/**
	 * Процесс использования умения.
	 * 
	 * @param target цель использования умения.
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 * @return было ли использование.
	 */
	public default boolean useSkill(final SpaceObject target, final LocalObjects local, final long currentTime) {
		return false;
	}
}

package com.ss.server.model.skills.state;

import java.util.function.Consumer;

import rlib.concurrent.lock.AsynReadSynWriteLock;
import rlib.concurrent.lock.LockFactory;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.model.module.Module;
import com.ss.server.model.skills.Skill;

/**
 * Реализации модели списка состояний умений корабля.
 * 
 * @author Ronn
 */
public final class SkillStateList implements AsynReadSynWriteLock {

	private static final ThreadLocal<CollectionStateFunction> LOCAL_COLLECTION_FUNCTION = new ThreadLocal<CollectionStateFunction>() {

		@Override
		protected CollectionStateFunction initialValue() {
			return new CollectionStateFunction();
		}
	};

	/** пулы переиспользования объектов */
	private final FoldablePool<Array<SkillStateInfo>> containerPool = PoolFactory.newAtomicFoldablePool(Array.class);
	private final FoldablePool<SkillStateInfo> stateInfoPool = PoolFactory.newAtomicFoldablePool(SkillStateInfo.class);

	/** функция очистки */
	private final Consumer<Array<SkillStateInfo>> clearFunction = states -> clear(states);

	/** таблица состояний умений */
	protected final Table<IntKey, Array<SkillStateInfo>> stateTable;

	/** синхронизатор */
	protected final AsynReadSynWriteLock lock;

	public SkillStateList() {
		this.stateTable = TableFactory.newIntegerTable();
		this.lock = LockFactory.newPrimitiveAtomicARSWLock();
	}

	/**
	 * Добавление состояния при загрузке из БД.
	 * 
	 * @param moduleId ид модуля.
	 * @param skillId ид шаблона умения.
	 * @param charge кол-во зарядов.
	 * @param maxCharge максмимального кол-во заряда.
	 * @param reloadTime время перезарядки.
	 * @param reloadFinishTime дата окончания перезарядки.
	 */
	public void addState(final int moduleId, final int skillId, final int charge, final int maxCharge, final int reloadTime, final long reloadFinishTime) {

		final SkillStateInfo info = getNextInfo();
		info.setContainsInDb(true);
		info.setModuleId(moduleId);
		info.setSkillId(skillId);
		info.setCharge(charge);
		info.setMaxCharge(maxCharge);
		info.setReloadTime(reloadTime);
		info.setReloadFinishTime(reloadFinishTime);
		info.increaseModCounter();

		final Table<IntKey, Array<SkillStateInfo>> stateTable = getStateTable();

		synLock();
		try {

			Array<SkillStateInfo> container = stateTable.get(moduleId);

			if(container == null) {
				container = getNextContainer();
				stateTable.put(moduleId, container);
			}

			container.add(info);

		} finally {
			synUnlock();
		}
	}

	@Override
	public void asynLock() {
		lock.asynLock();
	}

	@Override
	public void asynUnlock() {
		lock.asynUnlock();
	}

	/**
	 * Очистка таблицы состояний.
	 */
	public void clear() {
		final Table<IntKey, Array<SkillStateInfo>> stateTable = getStateTable();
		stateTable.forEach(getClearFunction());
		stateTable.clear();
	}

	/**
	 * Очистка списка состояний для модуля.
	 * 
	 * @param stateInfos список состояний.
	 */
	private void clear(final Array<SkillStateInfo> stateInfos) {

		if(!stateInfos.isEmpty()) {

			final FoldablePool<SkillStateInfo> stateInfoPool = getStateInfoPool();

			for(final SkillStateInfo info : stateInfos.array()) {

				if(info == null) {
					break;
				}

				stateInfoPool.put(info);
			}
		}

		final FoldablePool<Array<SkillStateInfo>> containerPool = getContainerPool();
		containerPool.put(stateInfos);
	}

	/**
	 * @return локальный список всех актуальных состояний.
	 */
	public Array<SkillStateInfo> getActualStates() {

		final CollectionStateFunction function = LOCAL_COLLECTION_FUNCTION.get();

		final Array<SkillStateInfo> collection = function.getCollection();
		collection.clear();

		asynLock();
		try {
			final Table<IntKey, Array<SkillStateInfo>> reuseTable = getStateTable();
			reuseTable.forEach(function);
		} finally {
			asynUnlock();
		}

		return collection;
	}

	/**
	 * @return функция очистки.
	 */
	private Consumer<Array<SkillStateInfo>> getClearFunction() {
		return clearFunction;
	}

	/**
	 * @return пул переиспользуемых контейнеров.
	 */
	private FoldablePool<Array<SkillStateInfo>> getContainerPool() {
		return containerPool;
	}

	/**
	 * @return получение нового контейнера.
	 */
	private Array<SkillStateInfo> getNextContainer() {

		Array<SkillStateInfo> container = containerPool.take();

		if(container == null) {
			container = ArrayFactory.newArray(SkillStateInfo.class);
		}

		return container;
	}

	/**
	 * @return получение новой перезарядки.
	 */
	private SkillStateInfo getNextInfo() {

		SkillStateInfo info = stateInfoPool.take();

		if(info == null) {
			info = new SkillStateInfo();
		}

		return info;
	}

	/**
	 * Получение либо при отсутствии создание состояния для умения.
	 * 
	 * @param skill умение для которого необходимо состояние.
	 * @return информация о состоянии.
	 */
	public SkillStateInfo getOrCreate(final Skill skill) {

		final Module module = skill.getModule();

		if(module == null) {
			throw new RuntimeException("not found module for skill " + skill);
		}

		final Table<IntKey, Array<SkillStateInfo>> stateTable = getStateTable();

		synLock();
		try {

			Array<SkillStateInfo> container = stateTable.get(skill.getModuleObjectId());

			if(container == null) {
				container = ArrayFactory.newArray(SkillStateInfo.class);
				stateTable.put(skill.getModuleObjectId(), container);
			}

			if(!container.isEmpty()) {
				for(final SkillStateInfo info : container.array()) {

					if(info == null) {
						break;
					}

					if(info.getSkillId() == skill.getId()) {
						return info;
					}
				}
			}

			final SkillStateInfo info = getNextInfo();
			info.setModuleId(skill.getModuleObjectId());
			info.setSkillId(skill.getId());
			info.setCharge(skill.getMaxCharge());
			info.setMaxCharge(skill.getMaxCharge());
			info.increaseModCounter();
			info.setContainsInDb(false);

			container.add(info);

			return info;

		} finally {
			synUnlock();
		}
	}

	/**
	 * @return пул переиспользуемых состояний.
	 */
	private FoldablePool<SkillStateInfo> getStateInfoPool() {
		return stateInfoPool;
	}

	/**
	 * @return таблица состояний умений.
	 */
	private Table<IntKey, Array<SkillStateInfo>> getStateTable() {
		return stateTable;
	}

	@Override
	public void synLock() {
		lock.synLock();
	}

	@Override
	public void synUnlock() {
		lock.synUnlock();
	}
}

package com.ss.server.model.skills.state;

import java.util.function.Consumer;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Функция сборки состояний умений.
 * 
 * @author Ronn
 */
public class CollectionStateFunction implements Consumer<Array<SkillStateInfo>> {

	/** контейнер с состояниями умений */
	private final Array<SkillStateInfo> collection;

	public CollectionStateFunction() {
		this.collection = ArrayFactory.newArray(SkillStateInfo.class);
	}

	@Override
	public void accept(final Array<SkillStateInfo> infos) {

		if(infos.isEmpty()) {
			return;
		}

		collection.addAll(infos);
	}

	/**
	 * @return контейнер с состояниями умений.
	 */
	public Array<SkillStateInfo> getCollection() {
		return collection;
	}
}

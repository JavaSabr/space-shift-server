package com.ss.server.model.skills.state;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import rlib.util.pools.Foldable;

/**
 * Контейнер информации о состоянии умения игрока.
 * 
 * @author Ronn
 */
public class SkillStateInfo implements Foldable {

	/** кол-во зарядов */
	private final AtomicInteger charge;
	/** максимальное кол-во заряда */
	private final AtomicInteger maxCharge;
	/** счетчик модификаций */
	private final AtomicInteger modCounter;
	/** время перезагрузки */
	private final AtomicInteger reloadTime;
	/** время окончания перезагрузки */
	private final AtomicLong reloadFinishTime;

	/** уникальный ид модуля */
	private volatile int moduleId;
	/** ид умения в рамках модуля */
	private volatile int skillId;

	/** содержится ли запись в БД */
	private volatile boolean containsInDb;

	public SkillStateInfo() {
		this.charge = new AtomicInteger();
		this.maxCharge = new AtomicInteger();
		this.modCounter = new AtomicInteger();
		this.reloadFinishTime = new AtomicLong();
		this.reloadTime = new AtomicInteger();
	}

	/**
	 * Очистка счетчика модификаций.
	 */
	public void clearModCounter() {
		modCounter.set(0);
	}

	@Override
	public void finalyze() {
		setModuleId(0);
		setCharge(0);
		setMaxCharge(0);
		setReloadTime(0);
		setReloadFinishTime(0);
		setContainsInDb(false);
		clearModCounter();
	}

	/**
	 * @return кол-во зарядов.
	 */
	public int getCharge() {
		return charge.get();
	}

	/**
	 * @return максимальное кол-во заряда.
	 */
	public int getMaxCharge() {
		return maxCharge.get();
	}

	/**
	 * @return уникальный ид модуля.
	 */
	public int getModuleId() {
		return moduleId;
	}

	/**
	 * @return время окончания перезагрузки.
	 */
	public long getReloadFinishTime() {
		return reloadFinishTime.get();
	}

	/**
	 * @return время перезагрузки.
	 */
	public int getReloadTime() {
		return reloadTime.get();
	}

	/**
	 * @return ид умения в рамках модуля.
	 */
	public int getSkillId() {
		return skillId;
	}

	/**
	 * Увеличение счетчика модификаций.
	 */
	public void increaseModCounter() {
		modCounter.incrementAndGet();
	}

	/**
	 * @return содержится ли запись в БД.
	 */
	public boolean isContainsInDb() {
		return containsInDb;
	}

	/**
	 * @return нужно ли обновлять в БД.
	 */
	public boolean isNeedUpdate() {
		return modCounter.get() > 1;
	}

	@Override
	public void reinit() {
		clearModCounter();
	}

	/**
	 * @param charge кол-во зарядов.
	 */
	public void setCharge(final int charge) {
		this.charge.getAndSet(charge);
	}

	/**
	 * @return результат уменьшения заряда на 1.
	 */
	public int decrementChargeAndGet() {
		return charge.decrementAndGet();
	}

	/**
	 * @param containsInDb содержится ли запись в БД.
	 */
	public void setContainsInDb(final boolean containsInDb) {
		this.containsInDb = containsInDb;
	}

	/**
	 * @param maxCharge максимальное кол-во заряда.
	 */
	public void setMaxCharge(final int maxCharge) {
		this.maxCharge.getAndSet(maxCharge);
	}

	/**
	 * @param moduleId уникальный ид модуля.
	 */
	public void setModuleId(final int moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @param reloadFinishTime время окончания перезагрузки.
	 */
	public void setReloadFinishTime(final long reloadFinishTime) {
		this.reloadFinishTime.set(reloadFinishTime);
	}

	/**
	 * @param reloadTime время перезагрузки.
	 */
	public void setReloadTime(final int reloadTime) {
		this.reloadTime.set(reloadTime);
	}

	/**
	 * @param skillId ид умения в рамках модуля.
	 */
	public void setSkillId(final int skillId) {
		this.skillId = skillId;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [charge=" + charge + ", maxCharge=" + maxCharge + ", modCounter=" + modCounter + ", reloadTime=" + reloadTime + ", reloadFinishTime=" + reloadFinishTime
				+ ", moduleId=" + moduleId + ", skillId=" + skillId + ", containsInDb=" + containsInDb + "]";
	}
}

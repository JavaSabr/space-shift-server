package com.ss.server.model.skills.impl;

import com.ss.server.template.SkillTemplate;

/**
 * Реализация умения управления двигателям корабля.
 * 
 * @author Ronn
 */
public class ModuleEngineSkill extends ModuleActivableSkill {

	public ModuleEngineSkill(final SkillTemplate template) {
		super(template);
	}
}

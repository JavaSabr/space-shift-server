package com.ss.server.model.skills.impl;

import static com.ss.server.model.SystemMessageType.NO_SELECTED_TARGET;
import static com.ss.server.model.SystemMessageType.SELECTED_TARGET_CANT_ATTACKED;
import static com.ss.server.model.SystemMessageType.SELECTED_TARGET_IS_DESTROYED;
import static com.ss.server.model.rating.event.RatingEvents.getShotWeaponRatingEvent;
import static com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElementType.ROCKET_LAUNCHER;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.LocalObjects;
import com.ss.server.manager.FormulasManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.ModuleType;
import com.ss.server.model.module.impl.RocketModule;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.shot.Shot;
import com.ss.server.model.shot.impl.RocketShot;
import com.ss.server.model.skills.ShotSkill;
import com.ss.server.network.game.packet.server.ResponseModuleUse;
import com.ss.server.network.game.packet.server.ResponseRocketShotInfo;
import com.ss.server.network.game.packet.server.ResponseShotHitInfo;
import com.ss.server.template.SkillTemplate;

/**
 * Реализация скила ракетного.
 * 
 * @author Ronn
 */
public class RocketSkill extends AbstractSkill implements ShotSkill {

	private final FoldablePool<Shot> pool = PoolFactory.newAtomicFoldablePool(Shot.class);

	/** стартовая позиция выстрела */
	private final Vector startLoc;

	public RocketSkill(final SkillTemplate template) {
		super(template);

		this.startLoc = Vector.newInstance();
	}

	@Override
	public boolean isNeedReloading() {
		return true;
	}

	@Override
	public boolean applySkill(final Shot shot, final SpaceShip shooter, final SpaceObject target, final LocalObjects local) {

		final FormulasManager formulas = FormulasManager.getInstance();

		final ImpactInfo impactInfo = formulas.calcDamage(local, shooter, this);
		impactInfo.setDamageType(getDamageType());

		target.applyImpact(shooter, this, impactInfo, local);

		if(shooter.isNeedSendPacket()) {
			shooter.broadcastPacket(ResponseShotHitInfo.getInstance(shot, target, impactInfo.isOnShield(), local));
		}

		final RatingSystem ratingSystem = shooter.getRatingSystem();
		ratingSystem.notify(RatingEvents.getHitWeaponRatingEvent(local, ROCKET_LAUNCHER));

		return false;
	}

	@Override
	public boolean checkCondition(final SpaceObject target, final LocalObjects local, final long currentTime) {

		final SpaceShip ship = getShip();

		if(target == null) {
			ship.sendSystemMessage(NO_SELECTED_TARGET, local);
			return false;
		}

		if(!ship.canAttack(target)) {
			ship.sendSystemMessage(SELECTED_TARGET_CANT_ATTACKED, local);
			return false;
		}

		if(target.isDestructed()) {
			ship.sendSystemMessage(SELECTED_TARGET_IS_DESTROYED, local);
			return false;
		}

		return super.checkCondition(ship, local, currentTime);
	}

	/**
	 * @return следующий выстрел для запуска.
	 */
	protected RocketShot getNextShot() {

		RocketShot shot = (RocketShot) pool.take();

		if(shot == null) {
			shot = new RocketShot();
		}

		return shot;
	}

	/**
	 * @return пул выстрелов.
	 */
	public FoldablePool<Shot> getPool() {
		return pool;
	}

	/**
	 * @return точка старта выстрела.
	 */
	public Vector getStartLoc() {
		return startLoc;
	}

	@Override
	public boolean useSkill(final SpaceObject target, final LocalObjects local, final long currentTime) {

		final Module module = getModule();

		if(module == null || module.getType() != ModuleType.MODULE_ROCKET) {
			return false;
		}

		if(target == null || !target.isSpaceShip()) {
			return false;
		}

		if(!super.useSkill(target, local, currentTime)) {
			return false;
		}

		final SpaceShip ship = getShip();
		final RocketModule rocketModule = (RocketModule) module;
		final Rotation rotation = ship.getRotation();

		final Vector startLoc = getStartLoc();

		startLoc.set(rocketModule.getOffset());
		startLoc.addLocal(module.getLocation());
		rotation.multLocal(startLoc);
		startLoc.addLocal(ship.getLocation());

		if(ship.isNeedSendPacket()) {
			ship.broadcastPacket(ResponseModuleUse.getInstance(ship, module, this, local));
		}

		final RocketShot shot = getNextShot();
		shot.setSpeed(Math.min(getMaxSpeed(), ship.getCurrentSpeed() + getAccel()));
		shot.setRotationSpeed(getRotationSpeed());
		shot.setObjectId(ship.getNextShotId());
		shot.setMaxDistance(getMaxDistance());
		shot.setRotation(ship.getRotation());
		shot.setMaxSpeed(getMaxSpeed());
		shot.setStartLoc(startLoc);
		shot.setAccel(getAccel());
		shot.setModule(module);
		shot.setTarget(target);
		shot.setShooter(ship);
		shot.setSkill(this);
		shot.bind(getPool());
		shot.setIndex(0);

		if(ship.isNeedSendPacket()) {
			ship.broadcastPacket(ResponseRocketShotInfo.getInstance(ship, module, shot, local));
		}

		shot.start(local);

		final RatingSystem ratingSystem = ship.getRatingSystem();
		ratingSystem.notify(getShotWeaponRatingEvent(local, ROCKET_LAUNCHER));
		return true;
	}

	@Override
	public Vector getStartPosition(Vector position) {

		final Module module = getModule();

		if(module == null || module.getType() != ModuleType.MODULE_ROCKET) {
			return position;
		}

		final RocketModule rocketModule = (RocketModule) module;
		final SpaceShip ship = getShip();
		final Rotation rotation = ship.getRotation();

		position.set(rocketModule.getOffset());
		position.addLocal(module.getLocation());
		rotation.multLocal(position);
		position.addLocal(ship.getLocation());

		return position;
	}
}

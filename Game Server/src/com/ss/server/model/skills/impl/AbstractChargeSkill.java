package com.ss.server.model.skills.impl;

import java.util.concurrent.atomic.AtomicLong;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.skills.state.SkillStateInfo;
import com.ss.server.template.SkillTemplate;

/**
 * Базовая реализация умения использующего заряд обоймы.
 * 
 * @author Ronn
 */
public abstract class AbstractChargeSkill extends AbstractSkill {

	/** время последней регенерации */
	private final AtomicLong lastRegen;

	public AbstractChargeSkill(final SkillTemplate template) {
		super(template);

		this.lastRegen = new AtomicLong();
	}

	@Override
	public boolean checkCondition(final SpaceObject target, final LocalObjects local, final long currentTime) {

		if(getCharge() < 1) {
			return false;
		}

		return super.checkCondition(target, local, currentTime);
	}

	@Override
	public boolean useSkill(final SpaceObject target, final LocalObjects local, final long currentTime) {

		final SkillStateInfo stateInfo = getStateInfo();

		if(stateInfo.decrementChargeAndGet() < 0) {
			return false;
		}

		stateInfo.increaseModCounter();

		return super.useSkill(target, local, currentTime);
	}

	@Override
	protected void notifyFinishReload() {

		final SkillStateInfo stateInfo = getStateInfo();
		stateInfo.setCharge(getMaxCharge());

		super.notifyFinishReload();
	}

	@Override
	public void update(long currentTime, LocalObjects local) {
		super.update(currentTime, local);

		if(isReloading() || getCharge() == getMaxCharge()) {
			lastRegen.set(currentTime);
			return;
		}

		final SkillTemplate template = getTemplate();

		final long diff = currentTime - lastRegen.get();

		if(diff < template.getChargeRegenInterval()) {
			return;
		}

		final int newStrength = getCharge() + template.getChargeRegen();
		final int result = Math.min(newStrength, template.getMaxCharge());

		if(getShip().isPlayerShip()) {
			System.out.println();
		}

		final SkillStateInfo stateInfo = getStateInfo();
		stateInfo.setCharge(result);
		stateInfo.increaseModCounter();

		lastRegen.set(currentTime);
	}
}

package com.ss.server.model.skills.impl;

import static com.ss.server.model.SystemMessageType.ACTIVATED_SKILL_NAME;
import static com.ss.server.model.SystemMessageType.DEACTIVATED_SKILL_NAME;
import static com.ss.server.model.SystemMessageType.FORCE_SHIELD_DESTROYER;
import static com.ss.server.model.SystemMessageVars.VAR_SKILL_NAME;
import static com.ss.server.model.rating.event.RatingEvents.getReloadShieldRatingEvent;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageCalculator;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.damage.ElementType;
import com.ss.server.model.damage.ShieldType;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElementType;
import com.ss.server.model.ship.ForceShield;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.state.SkillStateInfo;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;
import com.ss.server.template.SkillTemplate;

/**
 * Реализация силового щита.
 * 
 * @author Ronn
 */
public class ModuleForceShieldSkill extends ModuleActivableSkill implements ForceShield {

	/** время последнего обновления прочности щита */
	private final AtomicLong lastRegen;

	/** был ли разрушен щит */
	private final AtomicBoolean destroyed;
	/** регенерация после ли уничтожения */
	private final AtomicBoolean postDestroy;

	public ModuleForceShieldSkill(final SkillTemplate template) {
		super(template);

		this.lastRegen = new AtomicLong();
		this.destroyed = new AtomicBoolean();
		this.postDestroy = new AtomicBoolean();
	}

	@Override
	public int getCurrentStrength() {
		return getCharge();
	}

	@Override
	public int getMaxStrength() {
		return template.getMaxCharge();
	}

	@Override
	public int getReloadDelay() {
		return isDestroyed() ? template.getMaxReloadDelay() : template.getMinReloadDelay();
	}

	@Override
	public boolean isDestroyed() {
		return destroyed.get();
	}

	@Override
	public boolean isNeedReloading() {
		return false;
	}

	@Override
	public boolean processAbsorb(final SpaceObject effector, final ImpactInfo impactInfo, final LocalObjects local) {

		final SpaceShip effected = getShip();

		final SkillTemplate template = getTemplate();
		final DamageType damageType = impactInfo.getDamageType();
		final DamageCalculator calculator = damageType.getCalculator();
		final ShieldType shieldType = template.getShieldType();

		try {

			// TODO добавить определение стехии атаки
			final int damage = calculator.getForShield(impactInfo.getValue(), ElementType.NONE, shieldType);
			final int charge = getCharge();

			if(charge > damage) {

				effected.sendMessage("поглащено " + damage + " урона.", local);

				if(effector.isPlayerShip()) {
					effector.getPlayerShip().sendMessage("Цель поглатила " + damage + " урона.", local);
				}

				updateCharge(charge - damage);

				final RatingSystem ratingSystem = effected.getRatingSystem();
				ratingSystem.notify(RatingEvents.getHitShieldRatingEvent(local, ShieldModuleRatingElementType.valueOf(shieldType)));

				return true;
			}

			effected.sendSystemMessage(FORCE_SHIELD_DESTROYER, local);

			destroyed.set(true);

			cancel(local);

		} finally {
			updateShield(local);
		}

		return false;
	}

	@Override
	protected void processCancel(final LocalObjects local) {

		final SpaceShip ship = getShip();
		ship.removeForceShield(this);

		updateShield(local);

		super.processCancel(local);

		if(isDestroyed()) {
			updateCharge(0);
		}

		if(ship.isPlayerShip()) {

			final ResponseSystemMessage message = ResponseSystemMessage.getInstance(DEACTIVATED_SKILL_NAME, local);
			message.addVar(VAR_SKILL_NAME, getTemplate().getName());

			ship.sendPacket(message, true);
		}
	}

	@Override
	protected void processUse(final LocalObjects local) {

		final SpaceShip ship = getShip();
		ship.addForceShield(this);

		updateShield(local);

		super.processUse(local);

		postDestroy.set(destroyed.get());
		destroyed.set(false);

		if(ship.isPlayerShip()) {

			final ResponseSystemMessage message = ResponseSystemMessage.getInstance(ACTIVATED_SKILL_NAME, local);
			message.addVar(VAR_SKILL_NAME, getTemplate().getName());

			ship.sendPacket(message, true);
		}
	}

	@Override
	public void update(final long currentTime, final LocalObjects local) {
		super.update(currentTime, local);

		if(!isActive() || isReloading() || getCharge() == getMaxCharge()) {
			lastRegen.set(currentTime);
			return;
		}

		final SkillTemplate template = getTemplate();

		final long diff = currentTime - lastRegen.get();

		if(diff < template.getChargeRegenInterval()) {
			return;
		}

		final int maxCharge = template.getMaxCharge();
		final int newStrength = getCharge() + template.getChargeRegen();
		final int result = Math.min(newStrength, maxCharge);

		updateCharge(result);
		updateShield(local);

		if(result == maxCharge && postDestroy.get()) {

			final SpaceShip ship = getShip();

			final RatingSystem ratingSystem = ship.getRatingSystem();
			ratingSystem.notify(getReloadShieldRatingEvent(local, ShieldModuleRatingElementType.valueOf(template.getShieldType())));

			postDestroy.set(false);
		}

		lastRegen.set(currentTime);
	}

	/**
	 * @param charge тновый уровень заряда щита.
	 */
	public void updateCharge(final int charge) {

		if(getShip().isPlayerShip()) {
			System.out.println();
		}

		final SkillStateInfo stateInfo = getStateInfo();
		stateInfo.setCharge(charge);
		stateInfo.increaseModCounter();
	}

	/**
	 * Обновление итогового состояния щитов корабля.
	 */
	protected void updateShield(final LocalObjects local) {

		final SpaceShip ship = getShip();

		final Array<ForceShield> forceShields = ship.getForceShields();

		if(forceShields.isEmpty() && ship.getCurrentShield() != 0) {
			ship.setCurrentShield(0, local);
			return;
		}

		int strength = 0;

		forceShields.readLock();
		try {

			for(final ForceShield forceShield : forceShields.array()) {

				if(forceShield == null) {
					break;
				}

				strength += forceShield.getCurrentStrength();
			}

		} finally {
			forceShields.readUnlock();
		}

		if(ship.getCurrentStrength() != strength) {
			ship.setCurrentShield(strength, local);
		}
	}

	@Override
	public boolean useSkill(final SpaceObject target, final LocalObjects local, final long currentTime) {

		if(!super.useSkill(target, local, currentTime)) {
			return false;
		}

		return true;
	}
}

package com.ss.server.model.skills.impl;

import java.util.concurrent.ScheduledFuture;

import com.ss.server.LocalObjects;
import com.ss.server.manager.SkillActiveManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.SystemMessageVars;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.TimeActiveSkill;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;
import com.ss.server.template.SkillTemplate;

/**
 * Реализация уменя генератора двигателя.
 * 
 * @author Ronn
 */
public class ModuleEngineGenerator extends ModuleActivableSkill implements TimeActiveSkill {

	/** ссылка на задачу по прекращению работы этого умения */
	private ScheduledFuture<Void> schedule;

	/** время активации умения */
	private long startTime;

	public ModuleEngineGenerator(final SkillTemplate template) {
		super(template);
	}

	@Override
	public boolean checkCondition(final SpaceObject target, final LocalObjects local, final long currentTime) {

		if(isActive()) {
			return true;
		}

		if(!super.checkCondition(target, local, currentTime)) {
			return false;
		}

		final SpaceShip ship = getShip();
		final Skill[] skills = ship.getSkills(getModuleObjectId());

		if(skills == null || skills.length < 2) {
			return true;
		}

		for(final Skill skill : skills) {

			if(skill == this || skill.getSkillGroup() != getSkillGroup()) {
				continue;
			}

			if(skill instanceof TimeActiveSkill) {
				if(((TimeActiveSkill) skill).isActive()) {
					return false;
				}
			}
		}

		return true;
	}

	@Override
	public int getReloadDelay() {
		final long diff = System.currentTimeMillis() - getStartTime();
		return Math.max(getMinReuseDelay(), (int) (getReloadConsume() * diff));
	}

	/**
	 * @return ссылка на задачу по прекращению работы этого умения.
	 */
	public ScheduledFuture<Void> getSchedule() {
		return schedule;
	}

	/**
	 * @return время активации умения.
	 */
	public long getStartTime() {
		return startTime;
	}

	@Override
	public boolean isNeedReloading() {
		return false;
	}

	@Override
	protected void processCancel(final LocalObjects local) {
		super.processCancel(local);

		final ScheduledFuture<Void> schedule = getSchedule();

		if(schedule != null) {
			schedule.cancel(false);
		}

		setSchedule(null);

		final SpaceShip ship = getShip();

		if(ship.isPlayerShip()) {

			final ResponseSystemMessage message = ResponseSystemMessage.getInstance(SystemMessageType.DEACTIVATED_SKILL_NAME, local);
			message.addVar(SystemMessageVars.VAR_SKILL_NAME, getTemplate().getName());

			ship.sendPacket(message, true);
		}
	}

	/**
	 * Отмена остальных активных умений этого же модуля.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	private void processCancelOthers(final LocalObjects local) {

		final SpaceShip ship = getShip();
		final Skill[] skills = ship.getSkills(getModuleObjectId());

		if(skills == null || skills.length < 2) {
			return;
		}

		for(final Skill skill : skills) {

			if(skill == this || skill.getSkillGroup() != getSkillGroup()) {
				continue;
			}

			if(skill instanceof TimeActiveSkill) {

				final TimeActiveSkill timeActiveSkill = (TimeActiveSkill) skill;

				if(timeActiveSkill.isActive()) {
					timeActiveSkill.cancel(local);
				}
			}
		}
	}

	@Override
	protected void processUse(final LocalObjects local) {
		processCancelOthers(local);
		super.processUse(local);

		final SpaceShip ship = getShip();

		final SkillActiveManager manager = SkillActiveManager.getInstance();
		setSchedule(manager.execute(ship, this));

		if(ship.isPlayerShip()) {

			final ResponseSystemMessage message = ResponseSystemMessage.getInstance(SystemMessageType.ACTIVATED_SKILL_NAME, local);
			message.addVar(SystemMessageVars.VAR_SKILL_NAME, getTemplate().getName());

			ship.sendPacket(message, true);
		}

		setStartTime(System.currentTimeMillis());
	}

	/**
	 * @param schedule ссылка на задачу по прекращению работы этого умения.
	 */
	public void setSchedule(final ScheduledFuture<Void> schedule) {
		this.schedule = schedule;
	}

	/**
	 * @param startTime время активации умения.
	 */
	public void setStartTime(final long startTime) {
		this.startTime = startTime;
	}

	@Override
	public void timeFinish(final LocalObjects local) {
		setSchedule(null);
		cancel(local);
	}
}

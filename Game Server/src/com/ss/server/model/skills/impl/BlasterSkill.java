package com.ss.server.model.skills.impl;

import static com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElementType.PULSE;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.LocalObjects;
import com.ss.server.manager.FormulasManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.ModuleType;
import com.ss.server.model.module.impl.BlasterModule;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.shot.Shot;
import com.ss.server.model.shot.impl.BlasterShot;
import com.ss.server.model.skills.ShotSkill;
import com.ss.server.network.game.packet.server.ResponseBlasterShotInfo;
import com.ss.server.network.game.packet.server.ResponseModuleUse;
import com.ss.server.network.game.packet.server.ResponseShotHitInfo;
import com.ss.server.template.SkillTemplate;

/**
 * Реализация умения модуля бластера.
 * 
 * @author Ronn
 */
public class BlasterSkill extends AbstractChargeSkill implements ShotSkill {

	/** пул использованных выстрелов */
	private final FoldablePool<? super Shot> pool = PoolFactory.newAtomicFoldablePool(BlasterShot.class);

	/** стартовая позиция выстрела */
	private final Vector startLoc;
	/** точка назначения выстрела */
	private final Vector targetLoc;

	public BlasterSkill(final SkillTemplate template) {
		super(template);

		this.startLoc = Vector.newInstance();
		this.targetLoc = Vector.newInstance();
	}

	@Override
	public boolean applySkill(final Shot shot, final SpaceShip shooter, final SpaceObject target, final LocalObjects local) {

		final FormulasManager formulas = FormulasManager.getInstance();

		final ImpactInfo impactInfo = formulas.calcDamage(local, shooter, this);
		impactInfo.setDamageType(getDamageType());

		target.applyImpact(shooter, this, impactInfo, local);

		if(shooter.isNeedSendPacket()) {
			shooter.broadcastPacket(ResponseShotHitInfo.getInstance(shot, target, impactInfo.isOnShield(), local));
		}

		final RatingSystem ratingSystem = shooter.getRatingSystem();
		ratingSystem.notify(RatingEvents.getHitWeaponRatingEvent(local, PULSE));

		return false;
	}

	/**
	 * @return следующий выстрел для запуска.
	 */
	protected BlasterShot getNextShot() {

		BlasterShot shot = (BlasterShot) pool.take();

		if(shot == null) {
			shot = new BlasterShot();
		}

		return shot;
	}

	/**
	 * @return пул выстрелов.
	 */
	public FoldablePool<? super Shot> getPool() {
		return pool;
	}

	/**
	 * @return точка старта выстрела.
	 */
	public Vector getStartLoc() {
		return startLoc;
	}

	/**
	 * @return точка финиша выстрела.
	 */
	public Vector getTargetLoc() {
		return targetLoc;
	}

	@Override
	public boolean isNeedReloading() {
		return getCharge() < 1;
	}

	@Override
	public boolean useSkill(final SpaceObject target, final LocalObjects local, final long currentTime) {

		final Module module = getModule();

		if(module == null || module.getType() != ModuleType.MODULE_BLASTER) {
			return false;
		}

		if(!super.useSkill(target, local, currentTime)) {
			return false;
		}

		final SpaceShip ship = getShip();

		final FoldablePool<? super Shot> pool = getPool();

		final Rotation rotation = ship.getRotation();

		final Vector targetLoc = getTargetLoc();
		final Vector startLoc = getStartLoc();
		final Vector direction = ship.getDirection();

		if(ship.isNeedSendPacket()) {
			ship.broadcastPacket(ResponseModuleUse.getInstance(ship, module, this, local));
		}

		final BlasterModule blasterModule = (BlasterModule) module;

		for(int i = 0, length = blasterModule.getShotCount(); i < length; i++) {

			final BlasterShot shot = getNextShot();

			startLoc.set(blasterModule.getOffset(i));
			startLoc.addLocal(module.getLocation());
			rotation.multLocal(startLoc);
			startLoc.addLocal(ship.getLocation());

			targetLoc.set(direction);
			targetLoc.multLocal(getMaxDistance());
			targetLoc.addLocal(startLoc);

			shot.setObjectId(ship.getNextShotId());
			shot.setMaxDistance(getMaxDistance());
			shot.setSpeed(getMaxSpeed());
			shot.setTargetLoc(targetLoc);
			shot.setStartLoc(startLoc);
			shot.setRotation(rotation);
			shot.setModule(module);
			shot.setShooter(ship);
			shot.setSkill(this);
			shot.setIndex(i);
			shot.bind(pool);

			if(ship.isNeedSendPacket()) {
				ship.broadcastPacket(ResponseBlasterShotInfo.getInstance(ship, module, shot, local));
			}

			shot.start(local);
		}

		final RatingSystem ratingSystem = ship.getRatingSystem();
		ratingSystem.notify(RatingEvents.getShotWeaponRatingEvent(local, PULSE));

		if(getCharge() < 1) {
			ratingSystem.notify(RatingEvents.getDepletionWeaponRatingEvent(local, PULSE));
		}

		return true;
	}

	@Override
	public Vector getStartPosition(Vector position) {

		final Module module = getModule();

		if(module == null || module.getType() != ModuleType.MODULE_BLASTER) {
			return position;
		}

		final BlasterModule blasterModule = (BlasterModule) module;
		final SpaceShip ship = getShip();
		final Rotation rotation = ship.getRotation();

		position.set(blasterModule.getOffset(0));
		position.addLocal(module.getLocation());
		rotation.multLocal(position);
		position.addLocal(ship.getLocation());

		return position;
	}
}

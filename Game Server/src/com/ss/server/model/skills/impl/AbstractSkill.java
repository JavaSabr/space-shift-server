package com.ss.server.model.skills.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.SystemMessageVars;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.state.SkillStateInfo;
import com.ss.server.model.skills.state.SkillStateList;
import com.ss.server.model.storage.Storage;
import com.ss.server.network.game.packet.server.ResponseSkillReloadInfo;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;
import com.ss.server.table.ItemTable;
import com.ss.server.template.SkillTemplate;
import com.ss.server.template.item.ItemTemplate;

/**
 * Базовая реализация умения модуля.
 * 
 * @author Ronn
 */
public abstract class AbstractSkill implements Skill {

	protected static final Logger LOGGER = LoggerManager.getLogger(Skill.class);

	/** шаблон умения */
	protected final SkillTemplate template;

	/** время последнего использования */
	protected final AtomicLong lastUse;
	/** в процессе перезаврядки */
	protected final AtomicBoolean reloading;

	/** корабль на котором установлен модуль */
	protected final AtomicReference<SpaceShip> shipReference;
	/** модуль умения */
	protected final AtomicReference<Module> moduleReference;
	/** контейнер информации о состояни умения */
	protected final AtomicReference<SkillStateInfo> stateInfoReference;

	public AbstractSkill(final SkillTemplate template) {
		this.template = template;
		this.lastUse = new AtomicLong();
		this.reloading = new AtomicBoolean(false);
		this.shipReference = new AtomicReference<SpaceShip>();
		this.moduleReference = new AtomicReference<Module>();
		this.stateInfoReference = new AtomicReference<SkillStateInfo>();
	}

	@Override
	public boolean checkCondition(final SpaceObject target, final LocalObjects local, final long currentTime) {

		if(isReloading()) {
			return false;
		}

		final int fireRate = getFireRate();

		if(fireRate > 0 && currentTime - getLastUse() < fireRate) {
			return false;
		}

		final SpaceShip ship = getShip();

		if(getItemConsumeId() > 0) {

			final Storage storage = ship.getStorage();

			if(!storage.contains(getItemConsumeId(), getItemConsumeCount())) {

				if(ship.isPlayerShip()) {

					final ItemTable itemTable = ItemTable.getInstance();
					final ItemTemplate template = itemTable.getTemplate(getItemConsumeId());

					if(template != null) {

						final ResponseSystemMessage message = ResponseSystemMessage.getInstance(SystemMessageType.MISSING_ITEMS, local);
						message.addVar(SystemMessageVars.VAR_ITEM_NAME, template.getName());
						message.addVar(SystemMessageVars.VAR_ITEM_COUNT, String.valueOf(getItemConsumeCount()));

						ship.sendPacket(message, true);
					}
				}

				return false;
			}
		}

		return true;
	}

	@Override
	public void finalyze() {

		final AtomicReference<Module> moduleReference = getModuleReference();
		moduleReference.set(null);

		final AtomicReference<SpaceShip> shipReference = getShipReference();
		shipReference.set(null);

		final AtomicReference<SkillStateInfo> stateInfoReference = getStateInfoReference();
		stateInfoReference.set(null);
	}

	@Override
	public int getCharge() {
		final SkillStateInfo stateInfo = getStateInfo();
		return stateInfo.getCharge();
	}

	/**
	 * @return время последнего использования.
	 */
	protected long getLastUse() {
		return lastUse.get();
	}

	@Override
	public final Module getModule() {
		return moduleReference.get();
	}

	@Override
	public final int getModuleObjectId() {
		return getModule().getObjectId();
	}

	/**
	 * @return модуль умения.
	 */
	protected final AtomicReference<Module> getModuleReference() {
		return moduleReference;
	}

	@Override
	public long getReloadFinishTime() {
		final SkillStateInfo stateInfo = getStateInfo();
		return stateInfo.getReloadFinishTime();
	}

	@Override
	public int getReloadTime() {
		final SkillStateInfo stateInfo = getStateInfo();
		return stateInfo.getReloadTime();
	}

	/**
	 * @return корабль на котором установлен модуль.
	 */
	protected final SpaceShip getShip() {
		return shipReference.get();
	}

	/**
	 * @return корабль на котором установлен модуль.
	 */
	protected final AtomicReference<SpaceShip> getShipReference() {
		return shipReference;
	}

	/**
	 * @return контейнер информации о состояни умения.
	 */
	protected final SkillStateInfo getStateInfo() {
		return stateInfoReference.get();
	}

	/**
	 * @return контейнер информации о состояни умения.
	 */
	protected final AtomicReference<SkillStateInfo> getStateInfoReference() {
		return stateInfoReference;
	}

	@Override
	public final SkillTemplate getTemplate() {
		return template;
	}

	@Override
	public void init(final SpaceShip ship, final LocalObjects local) {

		if(ship.isPlayerShip()) {
			System.out.println();
		}

		final SkillStateList stateList = ship.getSkillStateList();
		final SkillStateInfo info = stateList.getOrCreate(this);

		final AtomicReference<SpaceShip> shipReference = getShipReference();
		shipReference.set(ship);

		final AtomicReference<SkillStateInfo> stateInfoReference = getStateInfoReference();
		stateInfoReference.set(info);

		if(info.getMaxCharge() != getMaxCharge()) {
			info.setMaxCharge(getMaxCharge());
			info.increaseModCounter();
		}

		final long reloadFinishTime = info.getReloadFinishTime();
		reloading.set(reloadFinishTime > 0 && reloadFinishTime > System.currentTimeMillis());
	}

	@Override
	public final boolean isReloading() {
		return reloading.get();
	}

	protected void notifyFinishReload() {

		reloading.set(false);

		final SkillStateInfo stateInfo = getStateInfo();
		stateInfo.setReloadFinishTime(0);
		stateInfo.setReloadTime(0);
		stateInfo.increaseModCounter();
	}

	@Override
	public boolean postConsume(final SpaceObject target, final LocalObjects local) {
		return true;
	}

	@Override
	public boolean preConsume(final SpaceObject target, final LocalObjects local) {

		final int itemConsumeId = getItemConsumeId();

		if(itemConsumeId > 0) {

			final SpaceShip ship = getShip();
			final Storage storage = ship.getStorage();
			storage.lock();
			try {

				if(!storage.consume(itemConsumeId, getItemConsumeCount())) {
					return false;
				}

			} finally {
				storage.unlock();
			}
		}

		return true;
	}

	@Override
	public void reinit() {
		reloading.set(false);
	}

	@Override
	public final void setModule(final Module module) {
		moduleReference.set(module);
	}

	@Override
	public void startReload(final LocalObjects local) {

		final int reloadDelay = getReloadDelay();

		if(reloadDelay < 1) {
			notifyFinishReload();
		} else {

			final SkillStateInfo stateInfo = getStateInfo();

			final SpaceShip ship = getShip();
			final long currentTime = System.currentTimeMillis();

			if(ship.isPlayerShip()) {
				ship.sendPacket(ResponseSkillReloadInfo.getInstance(this, currentTime, reloadDelay, local), true);
			}

			reloading.set(true);

			stateInfo.setReloadTime(reloadDelay);
			stateInfo.setReloadFinishTime(currentTime + reloadDelay);
			stateInfo.increaseModCounter();
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [module=" + getModule() + ", objectId=" + getModuleObjectId() + "]";
	}

	/**
	 * @param lastUse время последнего использования.
	 */
	protected final void updateLastUse(final long lastUse) {
		this.lastUse.getAndSet(lastUse);
	}

	@Override
	public boolean useSkill(final SpaceObject target, final LocalObjects local, final long currentTime) {
		updateLastUse(System.currentTimeMillis());
		return true;
	}

	@Override
	public void update(long currentTime, LocalObjects local) {

		if(isReloading() && currentTime > getReloadFinishTime()) {
			notifyFinishReload();
		}
	}
}

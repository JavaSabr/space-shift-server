package com.ss.server.model.skills.impl;

import static com.ss.server.network.game.packet.server.ResponseSkillActive.ACTIVE;
import static com.ss.server.network.game.packet.server.ResponseSkillActive.DEACTIVE;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.packet.server.ResponseSkillActive;
import com.ss.server.template.SkillTemplate;

/**
 * Реализация умения по активации и деактивации работы модуля.
 * 
 * @author Ronn
 */
public class ModuleActivableSkill extends AbstractActivableSkill {

	public ModuleActivableSkill(final SkillTemplate template) {
		super(template);
	}

	@Override
	public void cancel(final LocalObjects local) {

		if(!isActive()) {
			return;
		}

		processCancel(local);
	}

	@Override
	public boolean isNeedReloading() {
		return false;
	}

	/**
	 * Реализация процесса отмены действия умения.
	 */
	protected void processCancel(final LocalObjects local) {

		final Module module = getModule();

		if(module == null) {
			LOGGER.warning("not found module in " + getShip() + " for skill " + this);
			return;
		}

		final SpaceShip ship = getShip();

		final SkillTemplate template = getTemplate();
		template.removeActiveFuncsTo(ship);

		setActive(false);

		if(ship.isNeedSendPacket()) {
			ship.broadcastPacket(ResponseSkillActive.getInstance(ship, module, getId(), DEACTIVE, local));
		}

		ship.updateInfo(local);

		startReload(local);
	}

	/**
	 * Реализация процессе активации умения
	 */
	protected void processUse(final LocalObjects local) {

		final Module module = getModule();

		if(module == null) {
			LOGGER.warning("not found module in " + getShip() + " for skill " + this);
			return;
		}

		final SpaceShip ship = getShip();

		final SkillTemplate template = getTemplate();
		template.addActiveFuncsTo(ship);

		setActive(true);

		if(ship.isNeedSendPacket()) {
			ship.broadcastPacket(ResponseSkillActive.getInstance(ship, module, getId(), ACTIVE, local));
		}

		ship.updateInfo(local);
	}

	@Override
	public void showAll(final LocalObjects local) {

		final Module module = getModule();

		if(module == null) {
			LOGGER.warning("not found module in " + getShip() + " for skill " + this);
			return;
		}

		final SpaceShip ship = getShip();

		if(ship.isNeedSendPacket()) {
			ship.broadcastPacket(ResponseSkillActive.getInstance(ship, module, getId(), isActive() ? ACTIVE : DEACTIVE, local));
		}
	}

	@Override
	public void showTo(final SpaceObject object, final LocalObjects local) {

		final SpaceShip ship = getShip();
		final Module module = getModule();

		if(module == null) {
			LOGGER.warning("not found module in " + ship + " for skill " + this);
			return;
		}

		object.sendPacket(ResponseSkillActive.getInstance(ship, module, getId(), isActive() ? ResponseSkillActive.ACTIVE : ResponseSkillActive.DEACTIVE, local), true);
	}

	@Override
	public boolean useSkill(final SpaceObject target, final LocalObjects local, final long currentTime) {

		if(!super.useSkill(target, local, currentTime)) {
			return false;
		}

		if(isActive()) {
			processCancel(local);
		} else {
			processUse(local);
		}

		return true;
	}
}

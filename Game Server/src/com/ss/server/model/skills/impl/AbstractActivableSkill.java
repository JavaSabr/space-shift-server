package com.ss.server.model.skills.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import com.ss.server.model.skills.ActivableSkill;
import com.ss.server.template.SkillTemplate;

/**
 * Базовая реализация активируемых умений.
 * 
 * @author Ronn
 */
public abstract class AbstractActivableSkill extends AbstractSkill implements ActivableSkill {

	/** статус активности */
	private final AtomicBoolean activate;

	public AbstractActivableSkill(final SkillTemplate template) {
		super(template);

		this.activate = new AtomicBoolean();
	}

	@Override
	public boolean isActive() {
		return activate.get();
	}

	/**
	 * @param active статус активированности.
	 */
	public void setActive(final boolean active) {
		activate.set(active);
	}
}

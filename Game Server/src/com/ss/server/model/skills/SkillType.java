package com.ss.server.model.skills;

import java.lang.reflect.Constructor;

import rlib.util.ClassUtils;

import com.ss.server.model.skills.impl.BlasterSkill;
import com.ss.server.model.skills.impl.ModuleActivableSkill;
import com.ss.server.model.skills.impl.ModuleEngineGenerator;
import com.ss.server.model.skills.impl.ModuleEngineSkill;
import com.ss.server.model.skills.impl.ModuleForceShieldSkill;
import com.ss.server.model.skills.impl.RocketSkill;
import com.ss.server.template.SkillTemplate;

/**
 * Перечисление типов скилов.
 * 
 * @author Ronn
 */
public enum SkillType {
	MODULE_ACTIVATE(ModuleActivableSkill.class),
	ENGINE_GENERATOR(ModuleEngineGenerator.class),
	FORCE_SHIELD(ModuleForceShieldSkill.class),
	BLASTER(BlasterSkill.class),
	ENGINE(ModuleEngineSkill.class),
	ROCKET(RocketSkill.class), ;

	/** конструктор умения */
	private final Constructor<? extends Skill> constructor;

	private SkillType(final Class<? extends Skill> type) {
		this.constructor = ClassUtils.getConstructor(type, SkillTemplate.class);
	}

	/**
	 * Создание нового экземпляра скила.
	 * 
	 * @param template темплейт скила.
	 * @return новый экземпляр скила.
	 */
	public Skill newInstance(final SkillTemplate template) {
		return ClassUtils.newInstance(constructor, template);
	}
}

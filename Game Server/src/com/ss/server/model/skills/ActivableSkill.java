package com.ss.server.model.skills;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;

/**
 * Интерфейс для реализации активируемых умений.
 * 
 * @author Ronn
 */
public interface ActivableSkill extends Skill {

	/**
	 * Отменить работу умения.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void cancel(LocalObjects local);

	/**
	 * @return активно ли сейчас умение.
	 */
	public boolean isActive();

	/**
	 * Отобразить состояние модуля для всех окружающих.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void showAll(LocalObjects local);

	/**
	 * Отобразить для указанного объекта.
	 * 
	 * @param object объект, который еще не видел активации скила.
	 * @param local контейнер локальных объектов.
	 */
	public void showTo(SpaceObject object, LocalObjects local);
}

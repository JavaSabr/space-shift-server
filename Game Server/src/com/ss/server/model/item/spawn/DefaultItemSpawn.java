package com.ss.server.model.item.spawn;

import rlib.util.VarTable;

/**
 * Стандартная реализация спавна предмета.
 * 
 * @author Ronn
 */
public final class DefaultItemSpawn extends AbstractItemSpawn {

	public DefaultItemSpawn(final VarTable vars) {
		super(vars);
	}

}

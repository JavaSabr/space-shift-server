package com.ss.server.model.item.spawn;

import rlib.util.VarTable;

/**
 * Перечисление типов спавнов предметов.
 * 
 * @author Ronn
 */
public enum SpawnType {

	DEFAULT {

		@Override
		public ItemSpawn newSpawn(final VarTable vars) {
			return new DefaultItemSpawn(vars);
		}
	};

	/**
	 * Создание нового спавна.
	 * 
	 * @param vars таблица параметров.
	 * @return новый спавн предмета.
	 */
	public ItemSpawn newSpawn(final VarTable vars) {
		return null;
	}
}

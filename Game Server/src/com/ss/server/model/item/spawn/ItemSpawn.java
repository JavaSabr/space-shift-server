package com.ss.server.model.item.spawn;

import com.ss.server.model.spawn.Spawn;

/**
 * Интерфейс для реализации спавна предмета.
 * 
 * @author Ronn
 */
public interface ItemSpawn extends Spawn {

	public static final String PROP_ITEM_MIN = "itemMin";
	public static final String PROP_ITEM_MAX = "itemMax";
	public static final String PROP_RANDOM_RESPAWN_TIME = "randomRespawnTime";
	public static final String PROP_MAX_RADIUS = "maxRadius";
	public static final String PROP_MIN_RADIS = "minRadis";
	public static final String PROP_MAX_RADIUS_Z = "maxRadiusZ";
	public static final String PROP_MAX_RADIUS_Y = "maxRadiusY";
	public static final String PROP_MAX_RADIUS_X = "maxRadiusX";
	public static final String PROP_MIN_RADIUS_Z = "minRadiusZ";
	public static final String PROP_MIN_RADIUS_Y = "minRadiusY";
	public static final String PROP_MIN_RADIUS_X = "minRadiusX";

	/**
	 * @return максимальное кол-во спавнящихся предметов.
	 */
	public int getItemMax();

	/**
	 * @return минимальное кол-во спавнещихся предметов.
	 */
	public int getItemMin();
}

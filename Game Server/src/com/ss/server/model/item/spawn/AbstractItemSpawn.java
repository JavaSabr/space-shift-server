package com.ss.server.model.item.spawn;

import static rlib.geom.DirectionType.DIRECTION;

import java.util.concurrent.atomic.AtomicReference;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.random.Random;

import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.database.ItemDBManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.spawn.impl.AbstractSpawn;
import com.ss.server.template.item.ItemTemplate;

/**
 * Базовая реализация спавна предмета.
 * 
 * @author Ronn
 */
public abstract class AbstractItemSpawn extends AbstractSpawn<ItemTemplate, SpaceItem> implements ItemSpawn {

	protected final String creatorName = getClass().getSimpleName();

	/** ссылка на итоговую позицию спавна */
	protected final AtomicReference<Vector> locationReference;

	/** рандоминайзер времени респавна */
	protected int randomRespawnTime;
	/** минимальное кол-во спавнещихся предметов */
	protected int itemMin;
	/** максимальное кол-во спавнящихся предметов */
	protected int itemMax;

	/** минимальный радиус спавна от позиции */
	protected int minRadius;
	/** максимальный радиус спавна от позиции */
	protected int maxRadius;

	/** минимальный ридус по X */
	protected int minRadiusX;
	/** минимальный ридус по Y */
	protected int minRadiusY;
	/** минимальный ридус по Z */
	protected int minRadiusZ;
	/** максимальный радиус по X */
	protected int maxRadiusX;
	/** максимальный радиус по Y */
	protected int maxRadiusY;
	/** максимальный радиус по Z */
	protected int maxRadiusZ;

	public AbstractItemSpawn(final VarTable vars) {
		super(vars);
		this.locationReference = new AtomicReference<Vector>();
		this.randomRespawnTime = vars.getInteger(PROP_RANDOM_RESPAWN_TIME, 0);
		this.itemMax = vars.getInteger(PROP_ITEM_MAX, 1);
		this.itemMin = vars.getInteger(PROP_ITEM_MIN, 1);
		this.maxRadius = vars.getInteger(PROP_MAX_RADIUS, 0);
		this.minRadius = vars.getInteger(PROP_MIN_RADIS, 0);
		this.minRadiusX = vars.getInteger(PROP_MIN_RADIUS_X, 0);
		this.minRadiusY = vars.getInteger(PROP_MIN_RADIUS_Y, 0);
		this.minRadiusZ = vars.getInteger(PROP_MIN_RADIUS_Z, 0);
		this.maxRadiusX = vars.getInteger(PROP_MAX_RADIUS_X, 0);
		this.maxRadiusY = vars.getInteger(PROP_MAX_RADIUS_Y, 0);
		this.maxRadiusZ = vars.getInteger(PROP_MAX_RADIUS_Z, 0);
	}

	/**
	 * Создание нового предмета для спавна его в космосе.
	 * 
	 * @param local контейнер локальных объектов.
	 * @return новый предмет для спавна.
	 */
	protected SpaceItem createItem(final LocalObjects local) {

		final IdFactory idFactory = IdFactory.getInstance();
		final ItemTemplate template = getTemplate();

		int itemCount = 1;

		if(template.isStackable()) {

			final int itemMin = getItemMin();
			final int itemMax = getItemMax();

			if(itemMin != itemMax) {
				final Random random = LOCAL_RANDOM.get();
				itemCount = Math.max(random.nextInt(itemMin, itemMax), 1);
			}
		}

		final SpaceItem item = template.takeInstance(idFactory.getNextItemId());
		item.setItemCount(itemCount);
		item.setCreateDate(System.currentTimeMillis());
		item.setCreator(creatorName);

		final ItemDBManager dbManager = ItemDBManager.getInstance();
		dbManager.addNewItem(item);

		return item;
	}

	@Override
	protected void doFinish(final LocalObjects local) {

		final SpaceItem spawned = getSpawned();

		if(spawned != null) {
			spawned.deleteMe(local);
		}
	}

	/**
	 * Старт процесса респавна.
	 */
	protected void doRespawn() {

		if(!started.get()) {
			return;
		}

		if(scheduleSpawn != null) {
			scheduleSpawn.cancel(false);
		}

		scheduleSpawn = EXECUTOR_MANAGER.scheduleGeneral(getRespawnTask(), getRespawnTime(LocalObjects.get()));
	}

	@Override
	protected void doSpawn(final LocalObjects local) {

		final SpaceItem spawned = getSpawned();

		if(spawned != null) {
			throw new RuntimeException("item is spawned.");
		}

		final SpaceItem item = createItem(local);

		final Vector location = getSpawnLocation(local);
		final Rotation rotation = getSpawnRotation(local);

		item.setRotation(rotation, local);
		item.setLocationId(getLocationId());
		item.getSpawnLoc().set(location);
		item.setSpawn(this);
		item.spawnMe(local, location.getX(), location.getY(), location.getZ());

		setSpawned(item);
	}

	@Override
	public int getItemMax() {
		return itemMax;
	}

	@Override
	public int getItemMin() {
		return itemMin;
	}

	/**
	 * @return ссылка на итоговую позицию спавна.
	 */
	public AtomicReference<Vector> getLocationReference() {
		return locationReference;
	}

	/**
	 * @return максимальный радиус спавна от позиции.
	 */
	protected int getMaxRadius() {
		return maxRadius;
	}

	/**
	 * @return максимальный радиус по X.
	 */
	public int getMaxRadiusX() {
		return maxRadiusX;
	}

	/**
	 * @return максимальный радиус по Y.
	 */
	public int getMaxRadiusY() {
		return maxRadiusY;
	}

	/**
	 * @return максимальный радиус по Z.
	 */
	public int getMaxRadiusZ() {
		return maxRadiusZ;
	}

	/**
	 * @return минимальный радиус спавна от позиции.
	 */
	protected int getMinRadius() {
		return minRadius;
	}

	/**
	 * @return минимальный ридус по X.
	 */
	public int getMinRadiusX() {
		return minRadiusX;
	}

	/**
	 * @return минимальный ридус по Y.
	 */
	public int getMinRadiusY() {
		return minRadiusY;
	}

	/**
	 * @return минимальный ридус по Z.
	 */
	public int getMinRadiusZ() {
		return minRadiusZ;
	}

	/**
	 * @return рандомная часть времени спавна.
	 */
	protected int getRandomRespawnTime() {
		return randomRespawnTime;
	}

	@Override
	public long getRespawnTime(final LocalObjects local) {

		final int randomRespawnTime = getRandomRespawnTime();
		final int respawnTime = getRespawnTime();

		if(randomRespawnTime == 0) {
			return respawnTime;
		}

		final Random random = LOCAL_RANDOM.get();

		long time = respawnTime - randomRespawnTime;
		time = random.nextLong(time, respawnTime + randomRespawnTime);

		return time;
	}

	@Override
	public Vector getSpawnLocation(final LocalObjects local) {

		final AtomicReference<Vector> reference = getLocationReference();

		if(reference.get() != null) {
			return reference.get();
		}

		final Vector location = getSpawnLocation();
		final Vector result = Vector.newInstance();

		final int minRadius = getMinRadius();
		final int maxRadius = getMaxRadius();
		final int maxRadiusX = getMaxRadiusX();
		final int maxRadiusY = getMaxRadiusY();
		final int maxRadiusZ = getMaxRadiusZ();
		final int minRadiusX = getMinRadiusX();
		final int minRadiusY = getMinRadiusY();
		final int minRadiusZ = getMinRadiusZ();

		if(maxRadius > 0) {

			final Array<SpaceObject> objects = local.getNextObjectList();

			final Vector direction = local.getNextVector();

			final Random random = LOCAL_RANDOM.get();

			for(int i = 0, length = 500; i < length; i++, objects.clear()) {

				final int distance = random.nextInt(minRadius, maxRadius);

				final Rotation rotation = local.getNextRotation();
				rotation.random(random);
				rotation.getVectorDirection(DIRECTION, direction);

				direction.multLocal(distance);

				result.set(location);
				result.addLocal(direction);

				SPACE.addAround(SpaceObject.class, result, objects, getLocationId());

				boolean collision = false;

				for(final SpaceObject object : objects.array()) {

					if(object == null) {
						break;
					}

					if(!(object.isLocationObject() || object.isGravityObject())) {
						continue;
					}

					if(object.isInDistance(result, object.getMaxSize())) {
						collision = true;
						break;
					}
				}

				if(!collision) {
					break;
				}
			}

		} else if(maxRadiusX != 0 || maxRadiusY != 0 || maxRadiusZ != 0 || minRadiusX != 0 || minRadiusY != 0 || minRadiusZ != 0) {

			final Array<SpaceObject> objects = local.getNextObjectList();

			final Vector direction = local.getNextVector();
			final Vector distance = local.getNextVector();

			final Random random = LOCAL_RANDOM.get();

			for(int i = 0, length = 500; i < length; i++, objects.clear()) {

				int diff = maxRadiusX - minRadiusX;

				distance.setX(random.nextInt(minRadiusX + diff, maxRadiusX + diff) - diff);

				diff = maxRadiusY - minRadiusY;

				distance.setY(random.nextInt(minRadiusY + diff, maxRadiusY + diff) - diff);

				diff = maxRadiusZ - minRadiusZ;

				distance.setZ(random.nextInt(minRadiusZ + diff, maxRadiusZ + diff) - diff);

				final Rotation rotation = local.getNextRotation();
				rotation.random(random);
				rotation.getVectorDirection(DIRECTION, direction);

				direction.multLocal(distance);

				result.set(location);
				result.addLocal(direction);

				SPACE.addAround(SpaceObject.class, result, objects, getLocationId());

				boolean collision = false;

				for(final SpaceObject object : objects.array()) {

					if(object == null) {
						break;
					}

					if(!(object.isLocationObject() || object.isGravityObject())) {
						continue;
					}

					if(object.isInDistance(result, object.getMaxSize())) {
						collision = true;
						break;
					}
				}

				if(!collision) {
					break;
				}
			}

		} else {
			result.set(location);
		}

		reference.set(result);

		return result;
	}

	@Override
	public Rotation getSpawnRotation(final LocalObjects local) {
		final Rotation rotation = getSpawnRotation();
		rotation.random();
		return rotation;
	}

	@Override
	protected void notifyFinishImpl(final SpaceItem object) {
		setSpawned(null);
		doRespawn();
	}
}

package com.ss.server.model.item;

import rlib.geom.Vector;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.item.spawn.ItemSpawn;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.storage.ItemLocation;

/**
 * Интерфейс для реализации космического предмета.
 * 
 * @author Ronn
 */
public interface SpaceItem extends SpaceObject {

	public static final int ITEM_ID_MONEY = 1;
	public static final int ITEM_ID_CONTAINER = 2;
	public static final int ITEM_ID_CRYSTAL = 3;
	public static final int ITEM_ID_ORE = 4;

	/**
	 * @param itemCount добавляемое кол-во предметов.
	 */
	public void addItemCount(long itemCount);

	/**
	 * @return дата создания предмета.
	 */
	public long getCreateDate();

	/**
	 * @return создатель предмета.
	 */
	public String getCreator();

	/**
	 * @return индекс ячейки, в которой размещен предмет.
	 */
	public int getIndex();

	/**
	 * @return кол-во предметов.
	 */
	public long getItemCount();

	/**
	 * @return тип размещения предмета в космосе.
	 */
	public ItemLocation getItemLocation();

	/**
	 * @return объект, который ожидает телепорта предмета к себе.
	 */
	public SpaceShip getLocker();

	/**
	 * @return название предмета.
	 */
	public String getName();

	/**
	 * @return ид владеющео объекта.
	 */
	public int getOwnerId();

	/**
	 * @return спавн предмета.
	 */
	public ItemSpawn getSpawn();

	/**
	 * @return место спавна предмета.
	 */
	public Vector getSpawnLoc();

	/**
	 * Стакуемый ли предмет.
	 */
	public boolean isStackable();

	/**
	 * @param createDate дата создания предмета.
	 */
	public void setCreateDate(long createDate);

	/**
	 * @param creator создатель предмета.
	 */
	public void setCreator(String creator);

	/**
	 * @param index индекс ячейки, в которой размещен предмет.
	 */
	public void setIndex(int index);

	/**
	 * @param itemCount кол-во предметов.
	 */
	public void setItemCount(long itemCount);

	/**
	 * @param location тип размещения предмета в космосе.
	 */
	public void setItemLocation(ItemLocation location);

	/**
	 * @param locker объект, который ожидает телепорта предмета к себе.
	 */
	public void setLocker(SpaceShip locker);

	/**
	 * @param ownerId ид владеющео объекта.
	 */
	public void setOwnerId(int ownerId);

	/**
	 * @param spawn спавн предмета.
	 */
	public void setSpawn(ItemSpawn spawn);

	/**
	 * @param spawnLoc место спавна предмета.
	 */
	public void setSpawnLoc(Vector spawnLoc);
}

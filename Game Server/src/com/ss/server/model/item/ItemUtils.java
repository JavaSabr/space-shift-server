package com.ss.server.model.item;

import rlib.geom.Vector;

import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.database.ItemDBManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Набор утильных методов по работе с предметами.
 * 
 * @author Ronn
 */
public class ItemUtils {

	/**
	 * Создание нового придмета по его ид шаблона.
	 * 
	 * @param templateId ид шаблона.
	 * @return новый предмет.
	 */
	public static SpaceItem createItem(final int templateId) {
		return createItem(templateId, 1L, "<unknown>");
	}

	/**
	 * Создание нового придмета по его ид шаблона.
	 * 
	 * @param templateId ид шаблона.
	 * @param itemCount кол-во предметов в стопке.
	 * @param creator создатель предмета.
	 * @return новый предмет.
	 */
	public static SpaceItem createItem(final int templateId, final long itemCount, final String creator) {

		final ItemTable itemTable = ItemTable.getInstance();
		final ItemTemplate template = itemTable.getTemplate(templateId);

		if(template == null) {
			return null;
		}

		return createItem(itemCount, creator, template);
	}

	/**
	 * Создание нового придмета по его шаблону.
	 * 
	 * @param itemCount кол-во предметов.
	 * @param creator создатель предмета.
	 * @param template шаблон предмета.
	 * @return новый предмет.
	 */
	public static SpaceItem createItem(final long itemCount, final String creator, final ItemTemplate template) {

		final IdFactory idFactory = IdFactory.getInstance();

		final SpaceItem item = template.takeInstance(idFactory.getNextItemId());
		item.setItemCount(template.isStackable() ? itemCount : 1L);
		item.setCreateDate(System.currentTimeMillis());
		item.setCreator(creator);

		final ItemDBManager dbManager = ItemDBManager.getInstance();
		dbManager.addNewItem(item);

		return item;
	}

	/**
	 * Спавн предмета у указанного объекта.
	 * 
	 * @param item отспавневаемый предмет.
	 * @param object объект, у которого надо отспавнить его.
	 * @param local контейнер локальных объектов.
	 * @param offset отступ вперед/назад от позиции объекта.
	 */
	public static void spawnItemTo(final SpaceItem item, final SpaceObject object, final LocalObjects local, final int offset) {

		final Vector loc = object.getLocation();

		item.setLocationId(object.getLocationId());

		if(offset == 0) {
			item.spawnMe(local, loc.getX(), loc.getY(), loc.getZ());
		} else {

			final Vector current = item.getLocation();
			current.set(object.getDirection());
			current.multLocal(offset);
			current.addLocal(object.getLocation());

			item.spawnMe(local, current.getX(), current.getY(), current.getZ());
		}
	}

	private ItemUtils() {
		throw new RuntimeException();
	}
}

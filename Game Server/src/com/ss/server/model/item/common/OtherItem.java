package com.ss.server.model.item.common;

import com.ss.server.template.item.CommonItemTemplate;

/**
 * Реализация различных космических предметов.
 * 
 * @author Ronn
 */
public class OtherItem extends CommonItem {

	public OtherItem(final int objectId, final CommonItemTemplate template) {
		super(objectId, template);
	}
}

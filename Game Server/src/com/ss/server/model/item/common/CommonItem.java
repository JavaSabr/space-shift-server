package com.ss.server.model.item.common;

import com.ss.server.model.item.AbstractSpaceItem;
import com.ss.server.template.item.CommonItemTemplate;

/**
 * Модель различных итемов.
 * 
 * @author Ronn
 */
public abstract class CommonItem extends AbstractSpaceItem<CommonItemTemplate> {

	public CommonItem(final int objectId, final CommonItemTemplate template) {
		super(objectId, template);
	}
}

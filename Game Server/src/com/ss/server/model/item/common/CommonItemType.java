package com.ss.server.model.item.common;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.item.SpaceItem;

/**
 * перечисление типов различных предметов.
 * 
 * @author Ronn
 */
public enum CommonItemType implements SpaceObjectType {
	OTHER(OtherItem.class), ;

	/** класс экземпляров типа предметов */
	private Class<? extends SpaceItem> instanceClass;

	private CommonItemType(final Class<? extends SpaceItem> instanceClass) {
		this.instanceClass = instanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public int index() {
		return ordinal();
	}
}

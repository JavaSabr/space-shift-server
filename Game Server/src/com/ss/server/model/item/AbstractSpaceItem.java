package com.ss.server.model.item;

import java.util.concurrent.ScheduledFuture;

import rlib.geom.Vector;
import rlib.util.SafeTask;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.executor.GameExecutor;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.model.impl.AbstractSpaceObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.impl.SpaceSector;
import com.ss.server.model.item.spawn.ItemSpawn;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.storage.ItemLocation;
import com.ss.server.network.game.packet.server.ResponseItemDelete;
import com.ss.server.network.game.packet.server.ResponseItemInfo;
import com.ss.server.task.impl.UpdateSpaceObjectTask;
import com.ss.server.template.item.ItemTemplate;

/**
 * Базовая реализация космического контейнера.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceItem<T extends ItemTemplate> extends AbstractSpaceObject<T> implements SpaceItem {

	protected static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();

	/** задача по автоматическому диспавну */
	protected final SafeTask despawnTask = () -> deleteMe(LocalObjects.get());

	/** задача по обновлению корабля */
	protected final UpdateSpaceObjectTask updateTask;

	/** тип размещения предмета в мире */
	protected ItemLocation itemLocation;

	/** создатель предмета */
	protected String creator;

	/** ссылка на задачу по деспавну */
	protected volatile ScheduledFuture<SafeTask> despawnSchedule;
	/** ссылка на задачу по телепортации */
	protected volatile ScheduledFuture<SafeTask> teleportSchedule;
	/** объект, который ожидает телепорта предмета к себе */
	protected volatile SpaceShip locker;
	/** спввн предмета */
	protected volatile ItemSpawn spawn;
	/** место спавна предмета */
	protected volatile Vector spawnLoc;

	/** кол-во предметов */
	protected long itemCount;
	/** дата создания предмета */
	protected long createDate;

	/** ид владеющео объекта */
	protected int ownerId;

	/** индекс ячейки, в которой размещен предмет */
	protected int index;

	public AbstractSpaceItem(final int objectId, final T template) {
		super(objectId, template);

		this.itemLocation = ItemLocation.IN_SPACE;
		this.updateTask = new UpdateSpaceObjectTask(this);
	}

	@Override
	public void addItemCount(final long itemCount) {
		this.itemCount += itemCount;
	}

	@Override
	public void addMe(final PlayerShip playerShip, final LocalObjects local) {
		playerShip.sendPacket(ResponseItemInfo.getInstance(this, local), true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("add me " + this + " to " + playerShip);
		}
	}

	@Override
	public void addToUpdate(final SpaceLocation location) {
		final GameExecutor<UpdateSpaceObjectTask> executor = location.getObjectExecutor();
		executor.addTask(updateTask);
	}

	@Override
	protected void deleteMeImpl(LocalObjects local) {

		if(despawnSchedule != null) {
			despawnSchedule.cancel(false);
			despawnSchedule = null;
		}

		if(teleportSchedule != null) {
			teleportSchedule.cancel(false);
			teleportSchedule = null;
		}

		super.deleteMeImpl(local);
	}

	@Override
	public void finalyze() {
		super.finalyze();
		setLocker(null);
		setSpawn(null);
	}

	@Override
	public int getClassId() {
		return Config.SERVER_ITEM_CLASS_ID;
	}

	@Override
	public long getCreateDate() {
		return createDate;
	}

	@Override
	public String getCreator() {
		return creator;
	}

	/**
	 * @return задача по автоматическому диспавну.
	 */
	public SafeTask getDespawnTask() {
		return despawnTask;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public long getItemCount() {
		return itemCount;
	}

	@Override
	public ItemLocation getItemLocation() {
		return itemLocation;
	}

	@Override
	public SpaceShip getLocker() {
		return locker;
	}

	@Override
	public String getName() {
		return template.getName();
	}

	@Override
	public int getOwnerId() {
		return ownerId;
	}

	@Override
	public SpaceItem getSpaceItem() {
		return this;
	}

	@Override
	public ItemSpawn getSpawn() {
		return spawn;
	}

	@Override
	public Vector getSpawnLoc() {

		if(spawnLoc == null) {
			spawnLoc = Vector.newInstance();
		}

		return spawnLoc;
	}

	@Override
	public boolean isNeedSendPacket() {

		final SpaceSector sector = getCurrentSector();

		if(sector != null) {
			return sector.isActive();
		}

		return false;
	}

	@Override
	public boolean isSpaceItem() {
		return true;
	}

	@Override
	public boolean isStackable() {
		return template.isStackable();
	}

	@Override
	public void reinit() {
		super.reinit();
		setItemLocation(ItemLocation.IN_SPACE);
	}

	@Override
	public void removeMe(final PlayerShip playerShip, final LocalObjects local) {
		playerShip.sendPacket(ResponseItemDelete.getInstance(this, local), true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("remove me " + this + " to " + playerShip);
		}
	}

	@Override
	public void setCreateDate(final long createDate) {
		this.createDate = createDate;
	}

	@Override
	public void setCreator(final String creator) {
		this.creator = creator;
	}

	@Override
	public void setIndex(final int index) {
		this.index = index;
	}

	@Override
	public void setItemCount(final long itemCount) {
		this.itemCount = itemCount;
	}

	@Override
	public void setItemLocation(final ItemLocation location) {
		this.itemLocation = location;
	}

	@Override
	public void setLocker(final SpaceShip locker) {
		this.locker = locker;
	}

	@Override
	public void setOwnerId(final int ownerId) {
		this.ownerId = ownerId;
	}

	@Override
	public void setSpawn(final ItemSpawn spawn) {
		this.spawn = spawn;
	}

	@Override
	public void setSpawnLoc(final Vector spawnLoc) {
		this.spawnLoc = spawnLoc;
	}

	@Override
	public void spawnMe(final LocalObjects local) {
		super.spawnMe(local);

		if(despawnSchedule != null) {
			despawnSchedule.cancel(false);
		}

		if(getSpawn() == null) {
			this.despawnSchedule = EXECUTOR_MANAGER.scheduleGeneral(getDespawnTask(), Config.WORLD_LIFE_TIME_DROP_ITEM * 1000);
		}
	}

	@Override
	public String toString() {
		return "SpaceItem itemCount = " + itemCount + ",  objectId = " + objectId + ", templateId " + getTemplateId();
	}
}

package com.ss.server.model;

/**
 * Перечисление типов сообщений в чат.
 * 
 * @author Ronn
 */
public enum MessageType {
	MAIN,
	SYSTEM,
	PARTY,
	CLAN,
	COMMONWEALTH,
	GLOBAL,
}

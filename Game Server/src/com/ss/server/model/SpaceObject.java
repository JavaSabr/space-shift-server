package com.ss.server.model;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.util.array.Array;
import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.AI;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.impact.ImpactType;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.impl.SpaceSector;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.model.module.Module;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.teleport.TeleportHandler;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ServerConstPacket;
import com.ss.server.template.ObjectTemplate;

/**
 * Интерфейс для реализации космического объекта.
 * 
 * @author Ronn
 */
public interface SpaceObject extends Foldable {

	/**
	 * Отобразить этот объект для указанного игрока.
	 * 
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public default void addMe(final PlayerShip playerShip, final LocalObjects local) {
	}

	/**
	 * Получить доступные действия с заданиями связанные с этим объектом для
	 * указанного игрока.
	 * 
	 * @param container контейнер действий.
	 * @param playerShip корабль игрока.
	 */
	public default void addQuestButtons(final Array<QuestButton> container, final PlayerShip playerShip) {
	}

	/**
	 * Получить доступные действия с заданиями связанные с этим объектом для
	 * указанного игрока.
	 * 
	 * @param container контейнер действий.
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public default void addQuestButtons(final Array<QuestButton> container, final PlayerShip playerShip, final LocalObjects local) {
	}

	/**
	 * Обработка добавления на обнолвение к исполнителям новой локации.
	 * 
	 * @param location новая локация.
	 */
	public default void addToUpdate(final SpaceLocation location) {
	}

	/**
	 * Обработка воздействия на объект другим объектом.
	 * 
	 * @param effector воздействующий объект.
	 * @param skill скил, которым воздейсвуют.
	 * @param impactInfo информация о воздействии.
	 * @param local контейнер локальных объектов.
	 */
	public default void applyImpact(final SpaceObject effector, final Skill skill, final ImpactInfo impactInfo, final LocalObjects local) {
	}

	/**
	 * Отправка пакета объекту и всем в космосе.
	 * 
	 * @param packet отправляемый пакет.
	 */
	public default void broadcastGlobalPacket(final ServerPacket packet) {
	}

	/**
	 * Отправка пакета объекту и всем в локации.
	 * 
	 * @param packet отправляемый пакет.
	 */
	public default void broadcastLocationPacket(final ServerPacket packet) {
	}

	/**
	 * Отправка пакета объекту и окружающим его.
	 * 
	 * @param packet отправляемый пакет.
	 */
	public default void broadcastPacket(final ServerPacket packet) {
	}

	/**
	 * Может ли объект атаковать указанный объект.
	 * 
	 * @param object проверяемый объект.
	 * @return может ли объект атаковать указанный.
	 */
	public default boolean canAttack(final SpaceObject object) {
		return false;
	}

	/**
	 * Может ли быть коллизия с указанным объектом.
	 * 
	 * @param object проверяемый объект.
	 * @return может быть ли с этим объектом коллизия.
	 */
	public default boolean canCollision(final SpaceObject object) {
		return false;
	}

	/**
	 * Можно ли применить указанное воздействие к этому объекту.
	 * 
	 * @param impactType тип воздействия.
	 * @return можно ли применить такое воздействие.
	 */
	public default boolean canImpact(final ImpactType impactType) {
		return false;
	}

	/**
	 * Спрятать объект.
	 */
	public default void decayMe(final LocalObjects local) {
	}

	/**
	 * Блокировка удаления объекта.
	 */
	public void deleteLock();

	/**
	 * Удаление из мира.
	 */
	public default void deleteMe(final LocalObjects local) {
	}

	/**
	 * Разблокировка удаления объекта.
	 */
	public void deleteUnlock();

	/**
	 * @param location целевая точка.
	 * @return квадрат дистанции от объекта до указанной точки.
	 */
	public default float distanceSquaredTo(final Vector location) {
		return 0;
	}

	/**
	 * @param object сравниваемый объект.
	 * @return дистанция от текущего до указанного объекта.
	 */
	public default float distanceTo(final SpaceObject object) {
		return 0;
	}

	/**
	 * @param location целевая точка.
	 * @return дистанция от объекта до указанной точки..
	 */
	public default float distanceTo(final Vector location) {
		return 0;
	}

	/**
	 * Уничтожение объекта.
	 */
	public default void doDestruct(final SpaceObject destroyer, final LocalObjects local) {
	}

	/**
	 * Процесс обновления состояния объекта.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 */
	public default void doUpdate(final LocalObjects local, final long currentTime) {
	}

	/**
	 * @return AI объекта.
	 */
	public AI getAI();

	/**
	 * @return форма объекта.
	 */
	public Bounding getBounding();

	/**
	 * @return класс ид объекта.
	 */
	public default int getClassId() {
		return 0;
	}

	/**
	 * @return текущий сектор объекта.
	 */
	public default SpaceSector getCurrentSector() {
		return null;
	}

	/**
	 * @return направление объекта.
	 */
	public default Vector getDirection() {
		return null;
	}

	/**
	 * Определение дружественного статуса объекта к указанному кораблю игрока.
	 * 
	 * @param ship корабль игрока.
	 * @param local контейнер локальных объектов.
	 * @return статус объекта к кораблю.
	 */
	public default FriendStatus getFriendStatus(final PlayerShip ship, final LocalObjects local) {
		return null;
	}

	/**
	 * @return гравитационный оюъект.
	 */
	public default GravityObject getGravityObject() {
		return null;
	}

	/**
	 * @return положение объекта в космосе.
	 */
	public default Vector getLocation() {
		return null;
	}

	/**
	 * @return ид локации, в которой находится объект.
	 */
	public default int getLocationId() {
		return 0;
	}

	/**
	 * @return локационный объект.
	 */
	public default LocationObject getLocationObject() {
		return null;
	}

	/**
	 * @return максимальный размер объекта.
	 */
	public int getMaxSize();

	/**
	 * @return минимальный размер объекта.
	 */
	public default int getMinSize() {
		return 0;
	}

	/**
	 * @return модуль.
	 */
	public default Module getModule() {
		return null;
	}

	/**
	 * @return НПС.
	 */
	public default Nps getNps() {
		return null;
	}

	/**
	 * @return уникальный ид объекта.
	 */
	public default int getObjectId() {
		return 0;
	}

	/**
	 * @return корабль игрока.
	 */
	public default PlayerShip getPlayerShip() {
		return null;
	}

	/**
	 * @return разворот объекта в мире.
	 */
	public default Rotation getRotation() {
		return null;
	}

	/**
	 * @return размер модели по X.
	 */
	public default int getSizeX() {
		return 0;
	}

	/**
	 * @return размер модели по Y.
	 */
	public default int getSizeY() {
		return 0;
	}

	/**
	 * @return размер модели по Z.
	 */
	public default int getSizeZ() {
		return 0;
	}

	/**
	 * @return космический предмет.
	 */
	public default SpaceItem getSpaceItem() {
		return null;
	}

	/**
	 * @return корабль.
	 */
	public default SpaceShip getSpaceShip() {
		return null;
	}

	/**
	 * @return станция.
	 */
	public default SpaceStation getSpaceStation() {
		return null;
	}

	/**
	 * @return темплейт объекта.
	 */
	public default ObjectTemplate getTemplate() {
		return null;
	}

	/**
	 * @return ид темплейта объекта.
	 */
	public default int getTemplateId() {
		return 0;
	}

	/**
	 * Обработка некорректных координат.
	 */
	public default void invalidCoords(final LocalObjects local) {
	}

	/**
	 * Проверка на столкновение объектов.
	 * 
	 * @param target проверяемый объект.
	 * @param buffer векторный буффер.
	 * @return есть ли столкновение.
	 */
	public default boolean isCollision(final SpaceObject target, final VectorBuffer buffer) {
		return false;
	}

	/**
	 * @return был ли объект удален.
	 */
	public boolean isDeleted();

	/**
	 * @return уничтожен ли объект.
	 */
	public default boolean isDestructed() {
		return false;
	}

	/**
	 * @return является ли гравитационным объектом.
	 */
	public default boolean isGravityObject() {
		return false;
	}

	/**
	 * @param object проверяемый объект.
	 * @param distance допустимая дистанция.
	 * @return находятся ли в рамках дистанции указанный объект.
	 */
	public default boolean isInDistance(final SpaceObject object, final int distance) {

		if(object == null) {
			return false;
		}

		final Vector self = getLocation();
		final Vector target = object.getLocation();

		final int dx = (int) self.getX() - (int) target.getX();
		final int dy = (int) self.getY() - (int) target.getY();
		final int dz = (int) self.getZ() - (int) target.getZ();

		return dx * dx + dy * dy + dz * dz <= distance * distance;
	}

	/**
	 * @param position проверяемая позиция.
	 * @param distance допустимая дистанция.
	 * @return находятся ли в рамках дистанции указанная позиция.
	 */
	public default boolean isInDistance(final Vector position, final int distance) {

		if(position == null) {
			return false;
		}

		final Vector self = getLocation();

		final int dx = (int) self.getX() - (int) position.getX();
		final int dy = (int) self.getY() - (int) position.getY();
		final int dz = (int) self.getZ() - (int) position.getZ();

		return dx * dx + dy * dy + dz * dz <= distance * distance;
	}

	/**
	 * @return неуязвим ли объект.
	 */
	public default boolean isInvul() {
		return false;
	}

	/**
	 * @return является ли локационным объектом.
	 */
	public default boolean isLocationObject() {
		return false;
	}

	/**
	 * @return является ли объект модулем.
	 */
	public default boolean isModule() {
		return false;
	}

	/**
	 * @return нужно ли отправлять пакет этому объекту.
	 */
	public default boolean isNeedSendPacket() {
		return false;
	}

	/**
	 * @return является ли объект НПС.
	 */
	public default boolean isNps() {
		return false;
	}

	/**
	 * @return является ли объект кораблем игрока.
	 */
	public default boolean isPlayerShip() {
		return false;
	}

	/**
	 * @return является ли объект предметом.
	 */
	public default boolean isSpaceItem() {
		return false;
	}

	/**
	 * @return является ли объект кораблем.
	 */
	public default boolean isSpaceShip() {
		return false;
	}

	/**
	 * @return является ли объект станцией.
	 */
	public default boolean isSpaceStation() {
		return false;
	}

	/**
	 * @return видимый ли объект.
	 */
	public default boolean isVisible() {
		return false;
	}

	/**
	 * Обработка столкновения.
	 * 
	 * @param target объект, с котороым произошло столкновение.
	 * @param local контейнер локальных объектов
	 */
	public default void onCollision(final SpaceObject target, final LocalObjects local) {
	}

	/**
	 * Обработка удаления из исполнителей обновлений старой локации.
	 * 
	 * @param location старая локация.
	 */
	public default void removeFromExecute(final SpaceLocation location) {
	}

	/**
	 * Удалить объект у игрока.
	 * 
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public default void removeMe(final PlayerShip playerShip, final LocalObjects local) {
	}

	/**
	 * Заставить объект разворачиваться до указанного направления.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param start стартовый разворот.
	 * @param end конечное направление.
	 * @param infinity бесконечно ли поворачивать.
	 */
	public default void rotate(final LocalObjects local, final Rotation start, final Rotation end, final boolean infinity) {
	}

	/**
	 * Отправка константного пакета.
	 * 
	 * @param packet серверный пакет.
	 */
	public default void sendPacket(final ServerConstPacket packet) {
	}

	/**
	 * Отправка пакета объекту.
	 * 
	 * @param packet отправляемый пакет.
	 * @param increaseSend увеличивать ли счетчик отправок.
	 */
	public default void sendPacket(final ServerPacket packet, final boolean increaseSend) {
	}

	/**
	 * @param sector новый сектор объекта.
	 */
	public default void setCurrentSector(final SpaceSector sector) {
	}

	/**
	 * Обновление положения объекта в мире.
	 * 
	 * @param vector новоне положение.
	 * @param changed изменение относительно старого.
	 */
	public default void setLocation(final Vector vector, final Vector changed, final LocalObjects local) {
	}

	/**
	 * @param locationId ид локации, в которой находится объект.
	 */
	public default void setLocationId(final int locationId) {
	}

	/**
	 * @param objectId уникальный ид.
	 */
	public default void setObjectId(final int objectId) {
	}

	/**
	 * @param rotation новый поворот корабля.
	 * @param local контейнер локальных объектов.
	 */
	public default void setRotation(final Rotation rotation, final LocalObjects local) {
	}

	/**
	 * @param visible видимый ли объект.
	 */
	public default void setVisible(final boolean visible) {
	}

	/**
	 * Добалвение в мир.
	 */
	public default void spawnMe(final LocalObjects local) {
	}

	/**
	 * Спавн в указанной точке.
	 * 
	 * @param x координата позиции.
	 * @param y координата позиции.
	 * @param z координата позиции.
	 */
	public default void spawnMe(final LocalObjects local, final float x, final float y, final float z) {
	}

	/**
	 * Запуск телепортации по указанному обработчику.
	 * 
	 * @param handler обработчик телепортации.
	 * @param local контейнер локальных объектов.
	 */
	public void startTeleport(TeleportHandler handler, final LocalObjects local);

	/**
	 * Синхронизация объекта с видящими его клиентами.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 */
	public default void sync(final LocalObjects local, final long currentTime) {
	}

	/**
	 * Синхронизоваться с указанным объектом.
	 * 
	 * @param object объект, который запрашивает актуальные данные.
	 * @param local контейнер локальных данных.
	 */
	public default void syncFor(final SpaceObject object, final LocalObjects local) {
	}

	/**
	 * Телепортация объекта в указанное место.
	 * 
	 * @param position новая позиция объекта.
	 * @param rotation разворот объекта.
	 * @param local контейнер локальных объектов.
	 * @param locationId ид локации.
	 */
	public default void teleportTo(final Vector position, final Rotation rotation, final LocalObjects local, final int locationId) {
	}

	/**
	 * Обновление информации об объекте.
	 */
	public default void updateInfo(final LocalObjects local) {
	}
}

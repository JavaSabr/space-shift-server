package com.ss.server.model.ai.impl;

import rlib.geom.Rotation;

import com.ss.server.LocalObjects;
import com.ss.server.model.MessageType;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.social.party.Party;
import com.ss.server.network.game.packet.server.ResponseSayMessage;

/**
 * Интерфейс для реализации АИ игрока.
 * 
 * @author Ronn
 */
public final class PlayerShipAI extends AbstractShipAI<PlayerShip> {

	public PlayerShipAI(final PlayerShip actor) {
		super(actor);
	}

	public void startRotation(final LocalObjects local, final Rotation start, final Rotation end, final boolean infinity, final float modiff) {

		final PlayerShip actor = getActor();

		if(actor == null) {
			LOGGER.warning(this, new Exception("not found actor"));
			return;
		}

		actor.rotate(local, start, end, infinity, modiff);
	}

	@Override
	public void startSay(final MessageType type, final String message, final LocalObjects local) {
		final PlayerShip actor = getActor();

		if(actor == null) {
			LOGGER.warning(this, new Exception("not found actor"));
			return;
		}

		super.startSay(type, message, local);

		switch(type) {
			case PARTY: {

				final Party party = actor.getParty();

				if(party != null) {
					party.sendPacket(null, ResponseSayMessage.getInstance(type, actor.getName(), message, local));
				}

				break;
			}
			default: {
				break;
			}
		}
	}
}

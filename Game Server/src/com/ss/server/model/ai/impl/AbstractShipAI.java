package com.ss.server.model.ai.impl;

import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.MessageType;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.ShipAI;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.network.game.packet.server.ResponseSayMessage;

/**
 * Базовая модель АИ корабля.
 * 
 * @author Ronn
 */
public abstract class AbstractShipAI<T extends SpaceShip> extends AbstractAI<T> implements ShipAI {

	/** текущая цель корабля */
	private SpaceObject target;

	public AbstractShipAI(final T actor) {
		super(actor);
	}

	@Override
	public SpaceObject getTarget() {
		return target;
	}

	@Override
	public void notifyAttack(final SpaceShip attacked, final Skill skill, final ImpactInfo info, final LocalObjects local) {
	}

	@Override
	public void notifyAttacked(final SpaceShip attacker, final Skill skill, final ImpactInfo info, final LocalObjects local) {
	}

	@Override
	public void notifyFinishTurn() {
	}

	@Override
	public void setTarget(final SpaceObject target) {
		this.target = target;
	}

	@Override
	public void startSay(final MessageType type, final String message, final LocalObjects local) {

		final T actor = getActor();

		if(actor == null) {
			LOGGER.warning(this, new Exception("not found actor"));
			return;
		}

		switch(type) {
			case MAIN: {
				actor.broadcastLocationPacket(ResponseSayMessage.getInstance(type, actor.getName(), message, local));
				break;
			}
			case GLOBAL: {
				actor.broadcastGlobalPacket(ResponseSayMessage.getInstance(type, actor.getName(), message, local));
				break;
			}
			default: {
				break;
			}
		}
	}

	@Override
	public void startSkill(final Vector location, final Skill skill, final LocalObjects local) {

		final T actor = getActor();

		if(actor == null) {
			LOGGER.warning(this, new Exception("not found actor"));
			return;
		}

		actor.doSkillUse(location, skill, local);
	}
}

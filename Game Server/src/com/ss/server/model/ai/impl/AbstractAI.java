package com.ss.server.model.ai.impl;

import rlib.geom.Rotation;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.AI;

/**
 * Базовая модель АИ.
 * 
 * @author Ronn
 */
public abstract class AbstractAI<T extends SpaceObject> implements AI {

	protected static final Logger LOGGER = LoggerManager.getLogger(AI.class);

	/** владелец АИ */
	protected T actor;

	public AbstractAI(final T actor) {
		this.actor = actor;
	}

	/**
	 * @return владелец АИ.
	 */
	public final T getActor() {
		return actor;
	}

	/**
	 * @param actor владелец АИ.
	 */
	public final void setActor(final T actor) {
		this.actor = actor;
	}

	@Override
	public void startRotation(final LocalObjects local, final Rotation start, final Rotation end, final boolean infinity) {

		final T actor = getActor();

		if(actor == null) {
			LOGGER.warning(this, new Exception("not found actor"));
			return;
		}

		actor.rotate(local, start, end, infinity);
	}
}

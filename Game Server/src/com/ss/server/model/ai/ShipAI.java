package com.ss.server.model.ai;

import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.MessageType;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;

/**
 * Интерфейс для реализации АИ корабля.
 * 
 * @author Ronn
 */
public interface ShipAI extends AI {

	/**
	 * @return текущая цель корабля.
	 */
	public SpaceObject getTarget();

	/**
	 * Уведомление АИ об атаке его кораблем другого корабля.
	 * 
	 * @param attacked атакованный корабль.
	 * @param skill умение.
	 * @param info информация об атаке.
	 */
	public void notifyAttack(SpaceShip attacked, Skill skill, ImpactInfo info, LocalObjects local);

	/**
	 * Уведомление АИ об атаке его другим кораблем.
	 * 
	 * @param attacker атакующий корабль.
	 * @param skill атакубщее умение.
	 * @param info информация об атаке.
	 */
	public void notifyAttacked(SpaceShip attacker, Skill skill, ImpactInfo info, LocalObjects local);

	/**
	 * @param target текущая цель корабля.
	 */
	public void setTarget(SpaceObject target);

	/**
	 * Отправить сообщение.
	 * 
	 * @param type тип сообщения.
	 * @param message содержание сообщения.
	 * @param local контейнер локальных объектов.
	 */
	public void startSay(MessageType type, String message, LocalObjects local);

	/**
	 * @param location позиция корабля.
	 * @param skill используемый скил.
	 * @param local контейнер локальных объектов.
	 */
	public void startSkill(Vector location, Skill skill, LocalObjects local);
}

package com.ss.server.model.ai;

import rlib.geom.Rotation;

import com.ss.server.LocalObjects;

/**
 * Интерфейс для реализации АИ.
 * 
 * @author Ronn
 */
public interface AI {

	/**
	 * Уведомление о завершении разворота.
	 */
	public void notifyFinishTurn();

	/**
	 * Запуск разворота до указаного направления.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param start стартовое направление.
	 * @param end конечное направление.
	 * @param infinity бесконечное ли вращение.
	 */
	public void startRotation(LocalObjects local, Rotation start, Rotation end, boolean infinity);
}

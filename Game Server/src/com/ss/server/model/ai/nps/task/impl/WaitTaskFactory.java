package com.ss.server.model.ai.nps.task.impl;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAiType;
import com.ss.server.model.ai.nps.VarType;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.ship.nps.Nps;

/**
 * Фабрика задач при ожидании.
 * 
 * @author Ronn
 */
public class WaitTaskFactory extends AbstractTaskFactory {

	public static final String PROP_AVOID_DISTANCE = "avoidDistance";
	public static final String PROP_TURNING_INTERVAL = "turningInterval";

	/** интервал разворота */
	protected int turningInterval;
	/** дистанция облета позиции/объекта */
	protected int avoidDistance;

	public WaitTaskFactory(final Node node) {
		super(node);

		if(!isInitialized()) {
			return;
		}

		final VarTable vars = VarTable.newInstance(node, "set", "name", "val");

		this.turningInterval = vars.getInteger(PROP_TURNING_INTERVAL, 5000);
		this.avoidDistance = vars.getInteger(PROP_AVOID_DISTANCE, 100);
	}

	@Override
	public <A extends Nps> void addNewTask(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		if(!isInitialized() || actor.isDestructed()) {
			return;
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "create new task...");
		}

		// обработка колизий при полете
		if(processCollision(ai, actor, local, config, currentTime)) {
			return;
		}

		// задача для дебага процесса колизии
		// processDebugCollision(ai, actor, local);

		// проверка состояния силового щита, если надо
		processForceShield(ai, actor, local, config, currentTime, true);

		// проверка состояния двигателя если надо
		processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_100);

		// если есть защищаемый объект, производим его облет
		if(processAvoid(ai, actor, local, config, currentTime)) {
			return;
		}
	}

	/**
	 * @return дистанция облета позиции/объекта.
	 */
	public int getAvoidDistance() {
		return avoidDistance;
	}

	/**
	 * @return интервал разворота.
	 */
	public int getTurningInterval() {
		return turningInterval;
	}

	/**
	 * Процесс облета в режиме ожидания.
	 */
	protected <A extends Nps> boolean processAvoid(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		final Random random = ai.getRandom();
		final SpaceObject object = ai.getProtectedObject();

		final int turningInterval = getTurningInterval();
		final int avoidDistance = getAvoidDistance();

		if(avoidDistance > 0) {

			Vector avoidPosition = null;

			if(object != null) {
				avoidPosition = object.getLocation();
			} else {
				avoidPosition = actor.getSpawnLoc();
			}

			if(avoidPosition != null) {

				if(!actor.isFlying()) {
					ai.addTask(TaskAiType.ACTIVAE_ENGINE, false);
					ai.setVar(VarType.FINISH_TURNING, 0);
				}

				final long time = ai.getVar(VarType.FINISH_TURNING);

				if(currentTime - time > turningInterval) {
					ai.addTask(TaskAiType.TURN_TO_AVOID_POSITION, avoidPosition, avoidDistance, false);
					ai.setVar(VarType.FINISH_TURNING, currentTime + random.nextInt(0, turningInterval));
				}
			}

			return true;
		}

		return false;
	}
}

package com.ss.server.model.ai.nps.task.handler.impl;

import static rlib.geom.DirectionType.LEFT;
import static rlib.geom.DirectionType.UP;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ShipEventManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.ai.nps.TaskAiType;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.ship.nps.Nps;

/**
 * Обработчик задачи по облету объекта с которым должно случиться столкновение..
 * 
 * @author Ronn
 */
public class AvoidCollissionObjectTaskHandler extends AbstractTaskHandler {

	/**
	 * Интерфейс функции получения направления из разворота.
	 */
	private static interface GetDirectionFunction {

		/**
		 * Получение направления из разворота.
		 */
		public Vector get(Rotation rotation, Vector store);
	}

	private final static GetDirectionFunction[] GET_DIRECTION_FUNCTIONS = {
		(rotation, store) -> rotation.getVectorDirection(LEFT, store),
		(rotation, store) -> rotation.getVectorDirection(LEFT, store).negateLocal(),
		(rotation, store) -> rotation.getVectorDirection(UP, store),
		(rotation, store) -> rotation.getVectorDirection(UP, store).negateLocal(),
	};

	@Override
	public boolean handle(NpsAI ai, Nps actor, TaskAi task, long currentTime, LocalObjects local) {

		final Array<SpaceObject> objects = task.getObjects();

		if(objects.isEmpty()) {
			return true;
		}

		final SpaceObject near = task.getNear();

		if(near == null) {
			return true;
		}

		final int distance = task.getDistance();

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "distance " + distance);
		}

		final VectorBuffer buffer = local.getNextVectorBuffer();

		final Vector location = actor.getLocation();
		final Vector upToActor = local.getNextVector();

		// определение вектора "вверх" для более корректного определения
		// разворота с позиции корабля на позицию объекта
		final Rotation rotation = actor.getRotation();
		rotation.getVectorDirection(UP, upToActor);

		final Array<Vector> directions = findDirections(actor, local, near, buffer, location, upToActor);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "finded directions " + directions);
		}

		final Array<Vector> positions = findPositions(local, near, directions);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "finded positions " + positions);
		}

		// определение преспективнейшей точки облета с ее направлением от центра
		// объекта
		Vector targetPosition = null;
		Vector targetDirection = null;

		for(int i = 0, length = positions.size(); i < length; i++) {

			Vector position = positions.get(i);

			if(targetPosition == null) {
				targetPosition = position;
				targetDirection = directions.get(i);
			} else if(location.distanceSquared(targetPosition) > location.distanceSquared(position)) {
				targetPosition = position;
				targetDirection = directions.get(i);
			}
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "finded near position " + targetPosition + ", distance " + targetPosition.distance(location));
		}

		final Vector direction = local.getNextVector();
		final Vector resultPosition = findPosition(near, distance, buffer, location, targetPosition, targetDirection, direction);

		if(resultPosition == null) {
			return true;
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "finded result position " + resultPosition + ", distance " + resultPosition.distance(location));
		}

		// получаем вектор направления от корабля в точку облета
		direction.set(resultPosition);
		direction.subtractLocal(location);
		direction.normalizeLocal();

		// определяем целевой разворот корабля для процесса облета
		final Rotation targetRotation = local.getNextRotation();
		targetRotation.lookAt(direction, upToActor, buffer);

		// если у нас движки не на максимуме, включаем на максимум
		if(actor.getEngineEnergy() != EnergyModule.ENERGY_POWER_100) {

			final float currentEngine = actor.getEngineEnergy();
			actor.setEngineEnergy(EnergyModule.ENERGY_POWER_100);

			final ShipEventManager eventManager = ShipEventManager.getInstance();
			eventManager.notifyEngineEnergyChanged(currentEngine, actor, local);
		}

		// ставим задачу разворота по направлению облета
		ai.addTask(TaskAiType.ROTATION, targetRotation, true);

		return true;
	}

	/**
	 * Поиск ближайшей доступной точки, в которую надо начинать лететь для
	 * облета объекта.
	 */
	protected Vector findPosition(final SpaceObject near, final int distance, final VectorBuffer buffer, final Vector location, Vector targetPosition, Vector targetDirection, Vector direction) {

		final Bounding bounding = near.getBounding();

		// отдаляем точку от центра объекта в удобнейшеее направление до техпор
		// пока полет в нее не будет пересекаться с объектом
		for(int i = 2, length = 100; i < length; i++) {

			targetPosition.set(targetDirection);
			targetPosition.multLocal(near.getMaxSize() + distance * i);
			targetPosition.addLocal(near.getLocation());

			direction.set(targetPosition);
			direction.subtractLocal(location);
			direction.normalizeLocal();

			// если полет на эту точку не пересекается с объектом
			if(!bounding.intersects(location, direction, buffer)) {

				// увеличиваем на один щаг для подстраховки
				targetPosition.set(targetDirection);
				targetPosition.multLocal(near.getMaxSize() + distance * (i + 1));
				targetPosition.addLocal(near.getLocation());

				return targetPosition;
			}
		}

		return null;
	}

	/**
	 * Определения крайних точек объекта по возможным направлениям облета.
	 */
	protected Array<Vector> findPositions(LocalObjects local, final SpaceObject near, final Array<Vector> directions) {

		final Array<Vector> positions = local.getNextVectorList();

		for(Vector direction : directions.array()) {

			if(direction == null) {
				break;
			}

			Vector position = local.getNextVector();
			position.set(direction).multLocal(near.getMaxSize());
			position.addLocal(near.getLocation());

			positions.add(position);
		}

		return positions;
	}

	/**
	 * Формирование списка направлений от облетаемого объекта.
	 */
	protected Array<Vector> findDirections(Nps actor, LocalObjects local, final SpaceObject near, final VectorBuffer buffer, final Vector location, final Vector upToActor) {

		// вектор направления от позиции корабля на позицию объекта
		Vector directionToTarget = local.getNextVector();
		directionToTarget.set(location);
		directionToTarget.subtractLocal(near.getLocation());
		directionToTarget.normalizeLocal();

		// какой должен быть разворот корабля, что бы он смотрел прямо на объект
		final Rotation rotationToTarget = local.getNextRotation();
		rotationToTarget.lookAt(directionToTarget, upToActor, buffer);

		final Array<Vector> directions = local.getNextVectorList();

		// извлекаем доступные направления
		for(GetDirectionFunction function : GET_DIRECTION_FUNCTIONS) {
			directions.add(function.get(rotationToTarget, local.getNextVector()));
		}

		return directions;
	}
}

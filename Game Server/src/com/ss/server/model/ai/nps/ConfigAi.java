package com.ss.server.model.ai.nps;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;

import com.ss.server.model.ai.nps.task.TaskFactory;
import com.ss.server.model.ai.nps.think.ThinkAction;

/**
 * Конфигуратор работы Ai.
 * 
 * @author Ronn
 */
public class ConfigAi {

	private static final Logger LOGGER = LoggerManager.getLogger(ConfigAi.class);

	public static final String TASK_FACTORY_PACKAGE = TaskFactory.class.getPackage().getName();
	public static final String THINK_ACTION_PACKAGE = ThinkAction.class.getPackage().getName();

	private static final String RUNNABLE_ATTRIBUTE = "runnable";
	private static final String GLOBAL_ATTRIBUTE = "global";
	private static final String NPS_AI_STATE_ATTRIBUTE = "state";
	private static final String DEFAULT_INTERVAL_ATTRIBUTE = "default";

	private static final String ATTRIBUTE_VALUE = "val";

	private static final String NODE_THINK = "think";
	private static final String NODE_THINKS = "thinks";
	private static final String NODE_TASK = "task";
	private static final String NODE_TASKS = "tasks";
	private static final String NODE_INTERVAL = "interval";
	private static final String NODE_INTERVALS = "intervals";

	private static final int DEFAULT_AI_TASK_DELAY = 500;

	/** набор генераторов действий */
	private final ThinkAction[] thinks;
	/** набор фабрик задач */
	private final TaskFactory[] factory;

	/** название конфига */
	private final String name;

	/** интервалы работы АИ */
	private final int[] intervals;

	/** дистанция агрессии */
	private final int aggroRange;

	/** является ли АИ глобальным */
	private final boolean global;
	/** является ли АИ активным */
	private final boolean runnable;

	@SuppressWarnings("unchecked")
	public ConfigAi(final Node node) {

		final VarTable vars = VarTable.newInstance(node);

		this.name = vars.getString("name");

		final NpsAiState[] states = NpsAiState.values();

		this.thinks = new ThinkAction[states.length];
		this.factory = new TaskFactory[states.length];

		this.intervals = new int[states.length];

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_INTERVALS.equals(child.getNodeName())) {

				vars.parse(child);

				final int def = vars.getInteger(DEFAULT_INTERVAL_ATTRIBUTE, DEFAULT_AI_TASK_DELAY);

				for(Node interval = child.getFirstChild(); interval != null; interval = interval.getNextSibling()) {

					if(interval.getNodeType() != Node.ELEMENT_NODE || !NODE_INTERVAL.equals(interval.getNodeName())) {
						continue;
					}

					vars.parse(interval);

					intervals[vars.getEnum(NPS_AI_STATE_ATTRIBUTE, NpsAiState.class).ordinal()] = vars.getInteger(ATTRIBUTE_VALUE);
				}

				for(int i = 0, length = intervals.length; i < length; i++) {
					if(intervals[i] < 1) {
						intervals[i] = def;
					}
				}

			} else if(NODE_TASKS.equals(child.getNodeName())) {

				for(Node task = child.getFirstChild(); task != null; task = task.getNextSibling()) {

					if(task.getNodeType() != Node.ELEMENT_NODE || !NODE_TASK.equals(task.getNodeName())) {
						continue;
					}

					vars.parse(task);

					try {

						final Class<TaskFactory> type = (Class<TaskFactory>) Class.forName(TASK_FACTORY_PACKAGE + ".impl." + vars.getString("factory"));
						final Constructor<TaskFactory> constructor = type.getConstructor(Node.class);
						factory[vars.getEnum(NPS_AI_STATE_ATTRIBUTE, NpsAiState.class).ordinal()] = constructor.newInstance(task);

					} catch(ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
						LOGGER.warning("name " + name);
						LOGGER.warning(e);
					}
				}

			} else if(NODE_THINKS.equals(child.getNodeName())) {

				for(Node think = child.getFirstChild(); think != null; think = think.getNextSibling()) {

					if(think.getNodeType() != Node.ELEMENT_NODE || !NODE_THINK.equals(think.getNodeName())) {
						continue;
					}

					vars.parse(think);

					try {

						final Class<ThinkAction> type = (Class<ThinkAction>) Class.forName(THINK_ACTION_PACKAGE + ".impl." + vars.getString("action"));
						final Constructor<ThinkAction> constructor = type.getConstructor(Node.class);
						thinks[vars.getEnum(NPS_AI_STATE_ATTRIBUTE, NpsAiState.class).ordinal()] = constructor.newInstance(think);

					} catch(ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
						LOGGER.warning("name " + name);
						LOGGER.warning(e);
					}
				}
			}
		}

		vars.parse(node, "set", "name", ATTRIBUTE_VALUE);

		this.aggroRange = vars.getInteger("aggroRange", 0);
		this.global = vars.getBoolean(GLOBAL_ATTRIBUTE, false);
		this.runnable = vars.getBoolean(RUNNABLE_ATTRIBUTE, true);

		for(int i = 0, length = states.length; i < length; i++) {

			if(thinks[i] == null) {
				thinks[i] = states[i].getThink();
			}

			if(factory[i] == null) {
				factory[i] = states[i].getFactory();
			}
		}
	}

	/**
	 * @return дистанция агрессии.
	 */
	public int getAggroRange() {
		return aggroRange;
	}

	/**
	 * Получить фабрику для указанного состояния Ai Nps.
	 * 
	 * @param state текущее состояние Ai.
	 * @return фабрика задач для такого состояния.
	 */
	public TaskFactory getFactory(final NpsAiState state) {
		return factory[state.ordinal()];
	}

	/**
	 * Получение интервала исполнения заадч для указанного состояния.
	 * 
	 * @param state текущее состояние Ai.
	 * @return интервал для исполнения задач при таком состянии.
	 */
	public int getInterval(final NpsAiState state) {
		return intervals[state.ordinal()];
	}

	/**
	 * @return название конфига.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Получить генератор действий для указанного состояния Ai Nps.
	 * 
	 * @param state текущее состояние Ai.
	 * @return генератор действий дял указанного состояния Ai.
	 */
	public ThinkAction getThink(final NpsAiState state) {
		return thinks[state.ordinal()];
	}

	/**
	 * @return является ли Ai глобальным.
	 */
	public boolean isGlobal() {
		return global;
	}

	/**
	 * @return активный ли Ai.
	 */
	public boolean isRunnable() {
		return runnable;
	}

	@Override
	public String toString() {
		return "ConfigAi name = " + name + ",  global = " + global + ",  runnable = " + runnable;
	}
}

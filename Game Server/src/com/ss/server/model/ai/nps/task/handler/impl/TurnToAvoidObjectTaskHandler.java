package com.ss.server.model.ai.nps.task.handler.impl;

import rlib.geom.Vector;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.TaskAi;

/**
 * Обработчик задачи по развороту на указанную позицию.
 * 
 * @author Ronn
 */
public class TurnToAvoidObjectTaskHandler extends TurnToAvoidPositionTaskHandler {

	@Override
	protected Vector getAvoidPosition(final TaskAi task) {
		final SpaceObject target = task.getTarget();
		return target.getLocation();
	}
}

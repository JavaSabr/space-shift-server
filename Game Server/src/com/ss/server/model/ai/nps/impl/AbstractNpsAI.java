package com.ss.server.model.ai.nps.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import rlib.util.linkedlist.LinkedList;
import rlib.util.linkedlist.LinkedListFactory;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;
import rlib.util.random.Random;
import rlib.util.random.RandomFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.impl.AbstractShipAI;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.NpsAiState;
import com.ss.server.model.ai.nps.NpsAiUtils;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.ai.nps.TaskAiType;
import com.ss.server.model.ai.nps.VarType;
import com.ss.server.model.ai.nps.task.handler.TaskHandler;
import com.ss.server.model.ai.nps.think.ThinkAction;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.SkillGroup;
import com.ss.server.task.impl.UpdateNpsAITask;

/**
 * Базовая реализация AI Nps.
 * 
 * @author Ronn
 */
public class AbstractNpsAI<T extends Nps> extends AbstractShipAI<T> implements NpsAI {

	protected final Consumer<TaskAi> FOLD_TASK_FUNC = task -> {
		final FoldablePool<TaskAi> pool = getTaskPool();
		pool.put(task);
	};

	/** пул использованных задач */
	protected final FoldablePool<TaskAi> taskPool;
	/** очередь задач для исполнения */
	protected final LinkedList<TaskAi> taskQueue;
	/** задача по обновлению AI */
	protected final UpdateNpsAITask updateTask;
	/** флаг активности */
	protected final AtomicBoolean enable;

	/** скилы Nps для использования в Ai */
	protected final Skill[][] skills;

	/** таблица переменных дат */
	protected final long[] timeVars;

	/** конфиг Ai Nps */
	protected final ConfigAi configAi;
	/** рандоминайер Ai */
	protected final Random random;

	/** состояние AI */
	protected volatile NpsAiState currentState;

	/** цель корабля */
	protected volatile SpaceObject target;
	/** охраняемый объект */
	protected volatile SpaceObject protectedObject;

	public AbstractNpsAI(final T actor, final ConfigAi configAi) {
		super(actor);

		this.taskPool = PoolFactory.newAtomicFoldablePool(TaskAi.class);
		this.taskQueue = LinkedListFactory.newLinkedList(TaskAi.class);
		this.configAi = configAi;
		this.currentState = NpsAiState.WAIT;
		this.random = RandomFactory.newFastRandom();
		this.skills = new Skill[SkillGroup.SIZE][];
		this.timeVars = new long[VarType.SIZE];
		this.enable = new AtomicBoolean(false);
		this.updateTask = new UpdateNpsAITask(this);

		NpsAiUtils.prepareSkills(skills, actor);
	}

	@Override
	public void abortAttack() {
		// TODO Auto-generated method stub
	}

	@Override
	public void clearTaskQueue() {

		final LinkedList<TaskAi> taskQueue = getTaskQueue();

		synchronized(taskQueue) {
			taskQueue.accept(FOLD_TASK_FUNC);
			taskQueue.clear();
		}
	}

	@Override
	public TaskAi createTask() {

		TaskAi task = taskPool.take();

		if(task == null) {

			task = new TaskAi();

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "create AI task from " + getActor());
			}
		}

		return task;
	}

	@Override
	public boolean doTask(final Nps actor, final long currentTime, final LocalObjects local) {

		if(actor.isDestructed()) {
			return false;
		}

		final LinkedList<TaskAi> queue = getTaskQueue();

		TaskAi task = null;

		synchronized(queue) {
			task = queue.poll();
		}

		if(task == null) {
			return false;
		}

		final TaskAiType taskType = task.getType();

		if(taskType == null) {
			throw new RuntimeException("not found task type for " + taskType);
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "execute task " + task);
			LOGGER.debug(this, "queue task " + queue);
		}

		final TaskHandler handler = taskType.getHandler();

		if(handler == null) {
			LOGGER.warning(this, "not found handler to " + taskType);
		} else if(handler.handle(this, actor, task, currentTime, local)) {
			finishTask(task);
		} else if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "not finished task " + task);
		}

		return isWaitingTask();
	}

	@Override
	public void doUpdate(final long currentTime, final LocalObjects local) {

		final T actor = getActor();

		if(actor == null) {
			LOGGER.warning(this, new Exception("not found actor"));
			return;
		}

		final ConfigAi config = getConfigAi();
		config.getThink(getCurrentState()).think(this, actor, local, config, currentTime);
	}

	@Override
	public void finishTask(final TaskAi task) {
		taskPool.put(task);
	}

	@Override
	public ConfigAi getConfigAi() {
		return configAi;
	}

	@Override
	public NpsAiState getCurrentState() {
		return currentState;
	}

	@Override
	public SpaceObject getProtectedObject() {
		return protectedObject;
	}

	@Override
	public Random getRandom() {
		return random;
	}

	@Override
	public Skill getSkill(final SkillGroup group) {

		final Skill[] temp = skills[group.ordinal()];

		if(temp.length < 1) {
			return null;
		}

		return temp[getRandom().nextInt(0, temp.length - 1)];
	}

	@Override
	public Skill[] getSkills(final SkillGroup group) {
		return skills[group.ordinal()];
	}

	@Override
	public SpaceObject getTarget() {
		return target;
	}

	/**
	 * @return пул использованных задач.
	 */
	public FoldablePool<TaskAi> getTaskPool() {
		return taskPool;
	}

	@Override
	public LinkedList<TaskAi> getTaskQueue() {
		return taskQueue;
	}

	/**
	 * @return таблица переменных дат.
	 */
	protected long[] getTimeVars() {
		return timeVars;
	}

	@Override
	public UpdateNpsAITask getUpdateTask() {
		return updateTask;
	}

	@Override
	public long getVar(final VarType type) {
		return timeVars[type.ordinal()];
	}

	@Override
	public boolean isGlobal() {
		return getConfigAi().isGlobal();
	}

	@Override
	public boolean isWaitingTask() {
		return !taskQueue.isEmpty();
	}

	@Override
	public void notifyAttacked(final SpaceShip attacker, final Skill skill, final ImpactInfo info, final LocalObjects local) {
		super.notifyAttacked(attacker, skill, info, local);

		final T actor = getActor();

		if(actor == null || actor.isDestructed()) {
			return;
		}

		actor.addAggro(attacker, info.getValue(), info.getValue(), local);
	}

	@Override
	public void setCurrentState(final NpsAiState currentState) {
		this.currentState = currentState;
	}

	@Override
	public void setNewState(final NpsAiState state) {

		final NpsAiState currentState = getCurrentState();

		if(currentState == state) {
			return;
		}

		synchronized(this) {

			if(currentState == state) {
				return;
			}

			setCurrentState(state);

			final ConfigAi config = getConfigAi();

			final ThinkAction think = config.getThink(state);
			think.onStart(this, getActor(), LocalObjects.get(), config, System.currentTimeMillis());

			final UpdateNpsAITask updateTask = getUpdateTask();
			updateTask.setInterval(config.getInterval(state));
		}
	}

	@Override
	public void setProtectedObject(final SpaceObject protectedObject) {
		this.protectedObject = protectedObject;
	}

	@Override
	public void setTarget(final SpaceObject object) {
		this.target = object;
	}

	@Override
	public void setVar(final VarType type, final long time) {
		timeVars[type.ordinal()] = time;
	}

	@Override
	public void startAITasks() {

		final ConfigAi config = getConfigAi();

		if(!config.isRunnable()) {
			return;
		}

		if(!enable.compareAndSet(false, true)) {
			return;
		}

		final UpdateNpsAITask updateTask = getUpdateTask();
		updateTask.setInterval(config.getInterval(getCurrentState()));
	}

	@Override
	public synchronized void stopAITasks() {

		final ConfigAi config = getConfigAi();

		if(!config.isRunnable()) {
			return;
		}

		if(!enable.compareAndSet(true, false)) {
			return;
		}

		setCurrentState(NpsAiState.WAIT);

		final T actor = getActor();

		if(actor != null) {
			// TODO actor.clearAggroList();
		}

		clearTaskQueue();

		final long[] timeVars = getTimeVars();

		for(int i = 0, length = timeVars.length; i < length; i++) {
			timeVars[i] = 0;
		}
	}
}

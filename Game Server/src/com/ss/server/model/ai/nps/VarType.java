package com.ss.server.model.ai.nps;

/**
 * Перечисление типов переменных связанных с датой действий.
 * 
 * @author Ronn
 */
public enum VarType {

	LAST_ENGINE_ACTIVATE,
	FINISH_TURNING,
	TURING_INTERVAL,
	FINISH_PROCESS_COLLISION,
	TARGET_DISTANCE,
	LAST_CHECK_FORCE_SHIELD,
	FINISH_GUIDANCE_TIME,
	AVOID_DIRECTION_TYPE, ;

	public static final VarType[] VALUES = values();

	public static final int SIZE = VALUES.length;
}

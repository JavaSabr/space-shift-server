package com.ss.server.model.ai.nps.task.handler.impl;

import static com.ss.server.model.ai.nps.TaskAiType.ACTIVAE_ENGINE;
import static com.ss.server.model.ai.nps.TaskAiType.DEACTIVATE_ENGINE;
import static com.ss.server.model.ai.nps.TaskAiType.ROTATION;
import static com.ss.server.model.ai.nps.VarType.FINISH_TURNING;
import static com.ss.server.model.ai.nps.VarType.TURING_INTERVAL;
import static rlib.geom.DirectionType.UP;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.bounding.Bounding;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ShipEventManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.ShotSkill;
import com.ss.server.model.skills.Skill;

/**
 * Реализация обработчика использования скила.
 * 
 * @author Ronn
 */
public class SkillUseTaskHandler extends AbstractTaskHandler {

	@Override
	public boolean handle(final NpsAI ai, final Nps actor, final TaskAi task, final long currentTime, final LocalObjects local) {

		final SpaceObject target = task.getTarget();
		final Skill skill = task.getSkill();

		if(skill == null || target == null) {
			throw new RuntimeException("not found skill or target.");
		}

		// если это направленная атака
		if(skill.isDirectionAttack()) {

			final Vector startLoc = local.getNextVector();

			if(skill instanceof ShotSkill) {
				((ShotSkill) skill).getStartPosition(startLoc);
			} else {
				startLoc.set(actor.getLocation());
			}

			final Bounding bounding = target.getBounding();

			// если луч направления не пересекает цель
			if(!bounding.intersects(startLoc, actor.getDirection(), local.getNextVectorBuffer())) {

				final Random random = local.getRandom();

				// если шанс выпадает, то пытаемся более точно прицелиться
				if(random.chance(80)) {
					rotateToTarget(ai, actor, target, skill, startLoc, currentTime, local);
					return true;
				}

				// определяем точку завершения полета атаки
				final Vector endPosition = local.getNextVector();
				endPosition.set(actor.getDirection());
				endPosition.multLocal(actor.distanceTo(target));
				endPosition.addLocal(startLoc);

				// определяем размер погрешности
				int check = target.getMaxSize();

				// для цели корабля немного по другому считаем погрешность
				if(target.isSpaceShip()) {

					final SpaceShip ship = target.getSpaceShip();

					// если атака не мгновенная, рассчитываем смену позиции цели
					// за время атаки
					if(skill.getMaxSpeed() > 0) {
						check = (int) getDistanceOffset(actor, skill, ship);
					} else {
						check = (int) (ship.getMaxSize() + ship.getCurrentSpeed());
					}
				}

				// вычисляем квадрат погрешности
				check = check * check;

				// если точка завершения атаки не находится у цели даже с учетом
				// погрешности, прицеливаемся более точно
				if(target.distanceSquaredTo(endPosition) > check) {
					rotateToTarget(ai, actor, target, skill, startLoc, currentTime, local);
					return true;
				}
			}
		}

		final float currentDistance = actor.distanceTo(target);
		final float maxDistance = skill.getMaxDistance();

		if(currentDistance > maxDistance) {
			return true;
		}

		ai.startSkill(actor.getLocation(), skill, local);

		if(currentDistance > maxDistance / 2) {
			rotateToTarget(ai, actor, target, skill, actor.getLocation(), currentTime, local);
		}

		return true;
	}

	/**
	 * Вычисление необходимого разворота корабля для прицеливания по цели.
	 */
	private void rotateToTarget(final NpsAI ai, final Nps actor, final SpaceObject target, final Skill skill, final Vector startLoc, final long currentTime, final LocalObjects local) {

		final Rotation currentRotation = actor.getRotation();
		final Vector up = currentRotation.getVectorDirection(UP, local.getNextVector());

		final Vector direction = local.getNextVector();
		direction.set(target.getLocation());

		if(skill.isDirectionAttack() && target.isSpaceShip() && skill.getMaxSpeed() > 0) {

			final SpaceShip ship = target.getSpaceShip();
			final float distance = getDistanceOffset(actor, skill, ship);

			direction.set(ship.getDirection());
			direction.multLocal(distance);
			direction.addLocal(ship.getLocation());
		}

		direction.subtractLocal(startLoc);
		direction.normalizeLocal();

		final Rotation targetRotation = local.getNextRotation();
		targetRotation.lookAt(direction, up, local);

		final float currentEnergy = actor.getEngineEnergy();

		if(!actor.isFlying()) {

			if(currentEnergy == EnergyModule.ENERGY_POWER_0) {

				final ShipEventManager eventManager = ShipEventManager.getInstance();
				eventManager.notifyEngineEnergyChanged(actor.getEngineEnergy(), actor, local);

				actor.setEngineEnergy(EnergyModule.ENERGY_POWER_10);
			}

			ai.addTask(ACTIVAE_ENGINE, false);
		}

		final long turningInterval = ai.getVar(TURING_INTERVAL);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "add rotation task to " + currentRotation.dot(targetRotation) + ", " + targetRotation);
		}

		ai.addTask(ROTATION, targetRotation, false);
		ai.setVar(FINISH_TURNING, currentTime + turningInterval);

		if(!actor.isFlying()) {
			ai.addTask(DEACTIVATE_ENGINE, false);
		}
	}

	/**
	 * Рассчитывание дистанции смещения цели за время полета атакующего умения
	 * до цели.
	 */
	protected float getDistanceOffset(final Nps actor, final Skill skill, final SpaceShip ship) {

		// определяем время полета скила до текущего положения корабля
		float distance = actor.distanceTo(ship);
		float time = distance / skill.getMaxSpeed();

		// определяем время полета скила, до положение корабля, на котором
		// он будет во время подлета скила
		distance += ship.getCurrentSpeed() * time;
		time = distance / skill.getMaxSpeed();

		// определяем дистанцию смещения за время подлета
		distance = ship.getCurrentSpeed() * time;

		return distance;
	}
}

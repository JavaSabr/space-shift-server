package com.ss.server.model.ai.nps.task.handler.impl;

import rlib.geom.Rotation;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.ship.nps.Nps;

/**
 * Реализация исполнения задачи по развороту.
 * 
 * @author Ronn
 */
public class RotationTaskHandler extends AbstractTaskHandler {

	@Override
	public boolean handle(final NpsAI ai, final Nps actor, final TaskAi task, final long currentTime, final LocalObjects local) {

		final Rotation current = actor.getRotation();
		final Rotation target = task.getRotation();

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "handle rotation task to " + current.dot(target));
		}

		ai.startRotation(local, current, target, false);

		return true;
	}
}

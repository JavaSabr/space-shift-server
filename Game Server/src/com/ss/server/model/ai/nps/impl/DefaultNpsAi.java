package com.ss.server.model.ai.nps.impl;

import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ship.nps.Nps;

/**
 * Стандартная реализация Ai Nps.
 * 
 * @author Ronn
 */
public class DefaultNpsAi extends AbstractNpsAI<Nps> {

	public DefaultNpsAi(final Nps actor, final ConfigAi configAi) {
		super(actor, configAi);
	}

}

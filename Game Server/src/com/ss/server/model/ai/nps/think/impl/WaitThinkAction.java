package com.ss.server.model.ai.nps.think.impl;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.NpsAiState;
import com.ss.server.model.ai.nps.VarType;
import com.ss.server.model.ai.nps.task.TaskFactory;
import com.ss.server.model.impl.Space;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;

/**
 * Реализация генератора действий во время ожидания.
 * 
 * @author Ronn
 */
public class WaitThinkAction extends AbstractThinkAction {

	protected static final Space SPACE = Space.getInstance();

	public static final String PROP_MAXIMUM_FLY_RANGE = "maximumFlyRange";

	/** максимальная дистанция отлета от точки спавна */
	protected int maximumFlyRange;

	public WaitThinkAction(final Node node) {
		super(node);

		if(!isInitialized()) {
			return;
		}

		final VarTable vars = VarTable.newInstance(node);

		this.maximumFlyRange = vars.getInteger(PROP_MAXIMUM_FLY_RANGE, 5000);
	}

	/**
	 * Проверка нахождения корабля в зоне его допустимой деятельности в случае
	 * выхождения из которой, дается задача возврата домой.
	 * 
	 * @param ai ИИ НПС.
	 * @param actor обрабатываемый НПС.
	 * @return отправлен ли НПС домой.
	 */
	protected <A extends Nps> boolean checkDistance(final NpsAI ai, final A actor) {

		final Vector spawnLoc = actor.getSpawnLoc();

		if(!actor.isInDistance(spawnLoc, getPropMaximumFlyRange())) {
			ai.setNewState(NpsAiState.RETURN_TO_SPAWN);
			ai.setTarget(null);
			ai.clearTaskQueue();
			return true;
		}

		return false;
	}

	/**
	 * Процесс поиска целей для атаки.
	 * 
	 * @param ai ИИ, котороая обрабатывает действия.
	 * @param actor {@link Nps} который ищет цели.
	 * @param local контейнер локальных объектов.
	 */
	protected <A extends Nps> void findTargets(final NpsAI ai, final A actor, final LocalObjects local) {

		if(ai.getAggroRange() < 1) {
			return;
		}

		final Array<SpaceObject> container = local.getNextObjectList();
		final Random random = ai.getRandom();

		// TODO оптимизировать поиск потенциальных агрессоров
		SPACE.addAround(SpaceShip.class, actor, container, ai.getAggroRange());

		for(final SpaceObject object : container.array()) {

			if(object == null) {
				break;
			}

			if(actor.checkAgression(object)) {
				actor.addAggro(object.getSpaceShip(), 0, random.nextInt(1, 100), local);
			}
		}
	}

	/**
	 * @return максимальная дистанция отлета от точки спавна.
	 */
	public int getPropMaximumFlyRange() {
		return maximumFlyRange;
	}

	@Override
	public <A extends Nps> void onFinish(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		super.onFinish(ai, actor, local, config, currentTime);
		ai.setVar(VarType.FINISH_TURNING, 0);
	}

	@Override
	public <A extends Nps> void onStart(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		super.onStart(ai, actor, local, config, currentTime);
		ai.setVar(VarType.FINISH_TURNING, 0);
	}

	@Override
	public <A extends Nps> void think(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		super.think(ai, actor, local, config, currentTime);

		if(!isInitialized()) {
			return;
		}

		// проверка выхода за область полета
		if(checkDistance(ai, actor)) {
			return;
		}

		final SpaceShip mostHated = actor.getMostHated();

		if(mostHated != null && !mostHated.isDestructed()) {
			ai.setNewState(NpsAiState.IN_BATTLE);
			ai.clearTaskQueue();
			return;
		}

		// поиск потенциальных целей дял атаки
		findTargets(ai, actor, local);

		if(preCreateTask(ai, actor, local, config, currentTime)) {
			return;
		}

		final TaskFactory currentFactory = ai.getCurrentFactory();
		currentFactory.addNewTask(ai, actor, local, config, currentTime);

		if(postCreateTask(ai, actor, local, config, currentTime)) {
			return;
		}

		executeTasks(ai, actor, local, currentTime);
	}
}

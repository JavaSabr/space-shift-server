package com.ss.server.model.ai.nps;

import com.ss.server.model.ai.nps.task.TaskFactory;
import com.ss.server.model.ai.nps.task.impl.EmptyTaskFactory;
import com.ss.server.model.ai.nps.think.ThinkAction;
import com.ss.server.model.ai.nps.think.impl.EmptyThinkAction;

/**
 * Перечисление состояния AI.
 * 
 * @author Ronn
 */
public enum NpsAiState {

	WAIT(new EmptyThinkAction(null), new EmptyTaskFactory(null)),
	PATROL(new EmptyThinkAction(null), new EmptyTaskFactory(null)),
	IN_BATTLE(new EmptyThinkAction(null), new EmptyTaskFactory(null)),
	RETURN_TO_SPAWN(new EmptyThinkAction(null), new EmptyTaskFactory(null)), ;

	/** стандартный генератор действий для соответствующего состояния */
	private ThinkAction thinkAction;
	/** стандартная фабрика задач для соответствующего состояния */
	private TaskFactory taskFactory;

	private NpsAiState(final ThinkAction thinkAction, final TaskFactory taskFactory) {
		this.thinkAction = thinkAction;
		this.taskFactory = taskFactory;
	}

	/**
	 * @return фабрика задач.
	 */
	public TaskFactory getFactory() {
		return taskFactory;
	}

	/**
	 * @return генератор действий.
	 */
	public ThinkAction getThink() {
		return thinkAction;
	}
}

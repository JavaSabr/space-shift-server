package com.ss.server.model.ai.nps.task.handler.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.model.ai.nps.task.handler.TaskHandler;

/**
 * Базовая реализация обработчиказ задчи.
 * 
 * @author Ronn
 */
public abstract class AbstractTaskHandler implements TaskHandler {

	protected static final Logger LOGGER = LoggerManager.getLogger(TaskHandler.class);
}

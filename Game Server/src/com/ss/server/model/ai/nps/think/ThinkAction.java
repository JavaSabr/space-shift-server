package com.ss.server.model.ai.nps.think;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ship.nps.Nps;

/**
 * Интерфейс для реализации генератора действий AI Nps.
 * 
 * @author Ronn
 */
public interface ThinkAction {

	/**
	 * Подготовка состояния, вызывается перед переключением на него.
	 * 
	 * @param ai аи того, кто думает.
	 * @param actor тот кто думает.
	 * @param local локальные объекты.
	 * @param config конфигурация АИ.
	 * @param currentTime текущее время.
	 */
	public <A extends Nps> void onFinish(NpsAI ai, A actor, LocalObjects local, ConfigAi config, long currentTime);

	/**
	 * Подготовка состояния, вызывается перед переключением на него.
	 * 
	 * @param ai аи того, кто думает.
	 * @param actor тот кто думает.
	 * @param local локальные объекты.
	 * @param config конфигурация АИ.
	 * @param currentTime текущее время.
	 */
	public <A extends Nps> void onStart(NpsAI ai, A actor, LocalObjects local, ConfigAi config, long currentTime);

	/**
	 * Активация АИ.
	 * 
	 * @param ai аи того, кто думает.
	 * @param actor тот кто думает.
	 * @param local локальные объекты.
	 * @param config конфигурация АИ.
	 * @param currentTime текущее время.
	 */
	public <A extends Nps> void startAITask(NpsAI ai, A actor, LocalObjects local, ConfigAi config, long currentTime);

	/**
	 * Сгенерировать действие.
	 * 
	 * @param ai аи того, кто думает.
	 * @param actor тот кто думает.
	 * @param local локальные объекты.
	 * @param config конфигурация АИ.
	 * @param currentTime текущее время.
	 */
	public <A extends Nps> void think(NpsAI ai, A actor, LocalObjects local, ConfigAi config, long currentTime);
}

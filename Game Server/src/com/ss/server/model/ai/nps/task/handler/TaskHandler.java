package com.ss.server.model.ai.nps.task.handler;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.ship.nps.Nps;

/**
 * Интерфейс для обработчика действий задачи.
 * 
 * @author Ronn
 */
public interface TaskHandler {

	/**
	 * Обработка действий для выполнения задачи.
	 * 
	 * @param ai Ai Nps.
	 * @param actor сам Nps.
	 * @param task исполняемая заадча.
	 * @param currentTime текущее время.
	 * @param local контейнер локальных объектов.
	 * @return завершена ли задача.
	 */
	public boolean handle(NpsAI ai, Nps actor, TaskAi task, long currentTime, LocalObjects local);
}

package com.ss.server.model.ai.nps;

import java.lang.reflect.Constructor;

import rlib.util.ClassUtils;

import com.ss.server.model.ai.nps.impl.DefaultNpsAi;
import com.ss.server.model.ship.nps.Nps;

/**
 * Перечисление классов Ai Nps.
 * 
 * @author Ronn
 */
public enum NpsAiClass {

	DEFAULT(DefaultNpsAi.class, Nps.class), ;

	/** конструктор Ai */
	private Constructor<? extends NpsAI> constructor;

	private NpsAiClass(final Class<? extends NpsAI> aiClass, final Class<? extends Nps> npsClass) {
		this.constructor = ClassUtils.getConstructor(aiClass, npsClass, ConfigAi.class);
	}

	/**
	 * Создание нового экземпляра Ai Nps сооветствующего класса.
	 * 
	 * @param nps Nps, для которого создается Ai.
	 * @param configAi конфигурация Ai.
	 * @return новый экземпляр Ai.
	 */
	public <T extends NpsAI> T newInstance(final Nps nps, final ConfigAi configAi) {
		return ClassUtils.newInstance(constructor, nps, configAi);
	}
}

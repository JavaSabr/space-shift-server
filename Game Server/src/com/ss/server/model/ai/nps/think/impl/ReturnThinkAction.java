package com.ss.server.model.ai.nps.think.impl;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.NpsAiState;
import com.ss.server.model.ai.nps.VarType;
import com.ss.server.model.ai.nps.task.TaskFactory;
import com.ss.server.model.impl.Space;
import com.ss.server.model.ship.nps.Nps;

/**
 * Реализация генератора действий во время возврата на изначальную позицию.
 * 
 * @author Ronn
 */
public class ReturnThinkAction extends AbstractThinkAction {

	protected static final Space SPACE = Space.getInstance();

	public static final String PROP_RETURN_DISTANCE = "returnDistance";
	public static final String PROP_RESTORE_STRENGTH = "restoreStrength";

	/** дистанция, на которой считается что возврат произведен */
	protected int returnDistance;

	/** идет ли восстановлие прочности при возврате домой */
	protected boolean restoreStrength;

	public ReturnThinkAction(final Node node) {
		super(node);

		if(!isInitialized()) {
			return;
		}

		final VarTable vars = VarTable.newInstance(node);

		this.returnDistance = vars.getInteger(PROP_RETURN_DISTANCE, 200);
		this.restoreStrength = vars.getBoolean(PROP_RESTORE_STRENGTH, false);
	}

	/**
	 * @return дистанция, на которой считается что возврат произведен.
	 */
	public int getPropReturnDistance() {
		return returnDistance;
	}

	/**
	 * @return идет ли восстановлие прочности при возврате домой.
	 */
	public boolean isRestoreStrength() {
		return restoreStrength;
	}

	@Override
	public <A extends Nps> void onFinish(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		super.onFinish(ai, actor, local, config, currentTime);
		ai.setVar(VarType.FINISH_TURNING, 0);
	}

	@Override
	public <A extends Nps> void onStart(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		super.onStart(ai, actor, local, config, currentTime);
		ai.setVar(VarType.FINISH_TURNING, 0);
	}

	@Override
	public <A extends Nps> void think(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		super.think(ai, actor, local, config, currentTime);

		if(!isInitialized()) {
			return;
		}

		final Vector spawnLoc = actor.getSpawnLoc();

		if(actor.isInDistance(spawnLoc, getPropReturnDistance())) {
			ai.setNewState(NpsAiState.WAIT);
			ai.clearTaskQueue();
			return;
		}

		// если надо, производим автоматическое восстановление объекта
		if(isRestoreStrength()) {
			actor.setCurrentStrength(Integer.MAX_VALUE, local);
		}

		// не создаем новых задач пока выполняется разворот
		if(actor.isTurning()) {
			return;
		}

		if(preCreateTask(ai, actor, local, config, currentTime)) {
			return;
		}

		final TaskFactory currentFactory = ai.getCurrentFactory();
		currentFactory.addNewTask(ai, actor, local, config, currentTime);

		if(postCreateTask(ai, actor, local, config, currentTime)) {
			return;
		}

		executeTasks(ai, actor, local, currentTime);
	}
}

package com.ss.server.model.ai.nps.think.impl;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.think.ThinkAction;
import com.ss.server.model.ship.nps.Nps;

/**
 * Базовая реализация генератора действий.
 * 
 * @author Ronn
 */
public abstract class AbstractThinkAction implements ThinkAction {

	protected static final Logger LOGGER = LoggerManager.getLogger(ThinkAction.class);

	/** инициализирован ли */
	private final boolean initialized;

	public AbstractThinkAction(final Node node) {
		initialized = node != null;
	}

	protected boolean isInitialized() {
		return initialized;
	}

	@Override
	public <A extends Nps> void onFinish(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
	}

	@Override
	public <A extends Nps> void onStart(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
	}

	/**
	 * Действие после создания задач.
	 * 
	 * @param ai аи того, кто думает.
	 * @param actor тот кто думает.
	 * @param local локальные объекты.
	 * @param config конфигурация АИ.
	 * @param currentTime текущее время.
	 * @return завершена ли работа think.
	 */
	protected <A extends Nps> boolean postCreateTask(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		return false;
	}

	/**
	 * Действия перед созданием задач.
	 * 
	 * @param ai аи того, кто думает.
	 * @param actor тот кто думает.
	 * @param local локальные объекты.
	 * @param config конфигурация АИ.
	 * @param currentTime текущее время.
	 * @return завершена ли работа think.
	 */
	protected <A extends Nps> boolean preCreateTask(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		return false;
	}

	@Override
	public <A extends Nps> void startAITask(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
	}

	@Override
	public <A extends Nps> void think(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
	}

	protected <A extends Nps> void executeTasks(final NpsAI ai, final A actor, final LocalObjects local, final long currentTime) {
		for(int i = 0; i < 10 && ai.isWaitingTask(); i++) {
			ai.doTask(actor, currentTime, local);
		}
	}
}

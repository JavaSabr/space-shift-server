package com.ss.server.model.ai.nps.task.impl;

import static com.ss.server.model.ai.nps.TaskAiType.TURN_TO_POSITION;
import static com.ss.server.model.ai.nps.VarType.AVOID_DIRECTION_TYPE;
import static com.ss.server.model.ai.nps.VarType.FINISH_GUIDANCE_TIME;
import static com.ss.server.model.ai.nps.VarType.FINISH_TURNING;
import static rlib.geom.DirectionType.LEFT;
import static rlib.geom.DirectionType.UP;

import org.w3c.dom.Node;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.SkillGroup;

/**
 * Фабрика задач при боевом состоянии.
 * 
 * @author Ronn
 */
public class BattleTaskFactory extends AbstractTaskFactory {

	public static final String PROP_PROCESS_AVOID_TIME = "processAvoidTime";
	public static final String PROP_AVOID_DISTANCE = "avoidDistance";
	public static final String PROP_TURNING_INTERVAL = "turningInterval";
	public static final String PROP_MIN_BATTLE_DISTANCE = "minBattleDistance";
	public static final String PROP_MAX_BATTLE_DISTANCE = "maxBattleDistance";
	public static final String PROP_MIDDLE_BATTLE_DISTANCE = "middleBattleDistance";

	/**
	 * Интерфейс функции получения направления из разворота.
	 */
	private static interface GetDirectionFunction {

		/**
		 * Получение направления из разворота.
		 */
		public Vector get(Rotation rotation, Vector store);
	}

	private static final GetDirectionFunction[] GET_DIRECTION_FUNCTIONS = {
		(rotation, store) -> rotation.getVectorDirection(LEFT, store),
		(rotation, store) -> rotation.getVectorDirection(LEFT, store).negateLocal(),
		(rotation, store) -> rotation.getVectorDirection(UP, store),
		(rotation, store) -> rotation.getVectorDirection(UP, store).negateLocal(),
	};

	/** таблица шансов использования групп скилов */
	protected int[] groupChance;

	/** максимальная дистанция боя */
	protected int maxBattleDistance;
	/** минимальная дистанция боя */
	protected int minBattleDistance;
	/** средняя дистанция боя */
	protected int middleBattleDistance;
	/** интервал разворота */
	protected int turningInterval;
	/** дистанция на которую заходит корабль за бок цели */
	protected int avoidDistance;
	/** время, которое дается на процесс захода за бок */
	protected int processAvoidTime;

	public BattleTaskFactory(final Node node) {
		super(node);

		if(!isInitialized()) {
			return;
		}

		VarTable vars = VarTable.newInstance(node);

		final int def = vars.getInteger("groupChance", 0);

		final SkillGroup[] groups = SkillGroup.VALUES;

		vars = VarTable.newInstance(node, "set", "name", "val");

		this.groupChance = new int[groups.length];

		for(int i = 0, length = groupChance.length; i < length; i++) {
			groupChance[i] = vars.getInteger(groups[i].name(), def);
		}

		this.maxBattleDistance = vars.getInteger(PROP_MAX_BATTLE_DISTANCE, 900);
		this.minBattleDistance = vars.getInteger(PROP_MIN_BATTLE_DISTANCE, 100);
		this.middleBattleDistance = vars.getInteger(PROP_MIDDLE_BATTLE_DISTANCE, 600);
		this.turningInterval = vars.getInteger(PROP_TURNING_INTERVAL, 1000);
		this.avoidDistance = vars.getInteger(PROP_AVOID_DISTANCE, middleBattleDistance);
		this.processAvoidTime = vars.getInteger(PROP_PROCESS_AVOID_TIME, 5000);
	}

	@Override
	public <A extends Nps> void addNewTask(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		if(!isInitialized() || actor.isDestructed()) {
			return;
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "start create task...");
		}

		if(processCollision(ai, actor, local, config, currentTime)) {
			return;
		}

		final SpaceObject target = ai.getTarget();

		if(!target.isSpaceShip()) {
			return;
		}

		final SpaceShip ship = target.getSpaceShip();

		if(ship.isDestructed()) {
			return;
		}

		// проверка сосотояние силового щита если надо
		processForceShield(ai, actor, local, config, currentTime, false);

		final Random random = ai.getRandom();

		// процесс наведения на цель
		if(processGuidance(ai, actor, ship, random, local, config, currentTime)) {
			return;
		}

		// FIXME сейчас вроде как нет нормального разеделния
		// final int forceShieldStrength = ship.getCurrentShield();
		//
		// if(forceShieldStrength > 0 && processAttackShield(ai, actor, ship,
		// random)) {
		// return;
		// } else if(forceShieldStrength < 1 && processAttackBody(ai, actor,
		// ship, random)) {
		// return;
		// }

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "process attack to " + target + ", distance " + actor.distanceTo(target));
		}

		processAttack(ai, actor, ship, random);
	}

	/**
	 * Можно ли использовать скилы из указанной группы.
	 * 
	 * @param group группа скилов.
	 * @param random рандоминайзер AI.
	 * @return можно ли использовать.
	 */
	protected boolean canUse(final SkillGroup group, final Random random) {
		return random.chance(groupChance[group.ordinal()]);
	}

	/**
	 * @return максимальная дистанция боя.
	 */
	public int getMaxBattleDistance() {
		return maxBattleDistance;
	}

	/**
	 * @return минимальная дистанция боя.
	 */
	public int getMinBattleDistance() {
		return minBattleDistance;
	}

	/**
	 * @return средняя дистанция боя.
	 */
	public int getMiddleBattleDistance() {
		return middleBattleDistance;
	}

	/**
	 * @return интервал разворота.
	 */
	public int getTurningInterval() {
		return turningInterval;
	}

	/**
	 * @return дистанция на которую заходит корабль за бок цели.
	 */
	public int getAvoidDistance() {
		return avoidDistance;
	}

	/**
	 * @return время, которое дается на процесс захода за бок.
	 */
	public int getProcessAvoidTime() {
		return processAvoidTime;
	}

	/**
	 * Процесс обычной атаки.
	 * 
	 * @param ai ИИ корабля.
	 * @param actor сам корабль.
	 * @param ship целевой корабль.
	 * @param random рандоминайзер.
	 */
	protected <A extends Nps> void processAttack(final NpsAI ai, final A actor, final SpaceShip ship, final Random random) {

		SkillGroup skillGroup = null;

		if(canUse(SkillGroup.ROCKET, random)) {
			skillGroup = SkillGroup.ROCKET;
		} else if(canUse(SkillGroup.BLASTER, random)) {
			skillGroup = SkillGroup.BLASTER;
		}

		if(skillGroup != null) {

			final Skill[] skills = ai.getSkills(skillGroup);

			for(final Skill skill : skills) {
				if(!skill.isReloading()) {
					ai.addSkillUseTask(skill, ship, false);
				}
			}
		}
	}

	/**
	 * Процесс атаки по корпусу цели.
	 * 
	 * @param ai ИИ корабля.
	 * @param actor сам корабль.
	 * @param ship целевой корабль.
	 * @param random рандоминайзер.
	 * @return были ли созданы задачи по атаке.
	 */
	protected <A extends Nps> boolean processAttackBody(final NpsAI ai, final A actor, final SpaceShip ship, final Random random) {

		SkillGroup skillGroup = null;

		if(canUse(SkillGroup.ROCKET_BODY_PIERCER, random)) {
			skillGroup = SkillGroup.ROCKET_BODY_PIERCER;
		} else if(canUse(SkillGroup.BLASTER_BODY_PIERCER, random)) {
			skillGroup = SkillGroup.BLASTER_BODY_PIERCER;
		}

		int tasks = 0;

		if(skillGroup != null) {

			final Skill[] skills = ai.getSkills(skillGroup);

			for(final Skill skill : skills) {
				if(!skill.isReloading()) {
					ai.addSkillUseTask(skill, ship, false);
					tasks++;
				}
			}
		}

		return tasks > 0;
	}

	/**
	 * Процесс атаки по щитам цели.
	 * 
	 * @param ai ИИ корабля.
	 * @param actor сам корабль.
	 * @param ship целевой корабль.
	 * @param random рандоминайзер.
	 * @return были ли созданы задачи по атаке.
	 */
	protected <A extends Nps> boolean processAttackShield(final NpsAI ai, final A actor, final SpaceShip ship, final Random random) {

		SkillGroup skillGroup = null;

		if(canUse(SkillGroup.ROCKET_SHIELD_PIERCER, random)) {
			skillGroup = SkillGroup.ROCKET_SHIELD_PIERCER;
		} else if(canUse(SkillGroup.BLASTER_SHIELD_PIERCER, random)) {
			skillGroup = SkillGroup.BLASTER_SHIELD_PIERCER;
		}

		int tasks = 0;

		if(skillGroup != null) {

			final Skill[] skills = ai.getSkills(skillGroup);

			for(final Skill skill : skills) {
				if(!skill.isReloading()) {
					ai.addSkillUseTask(skill, ship, false);
					tasks++;
				}
			}
		}

		return tasks > 0;
	}

	/**
	 * Процесс захода на позицию стрельбы по цели.
	 */
	protected <A extends Nps> boolean processGuidance(final NpsAI ai, final A actor, final SpaceShip target, final Random random, final LocalObjects local, final ConfigAi config,
			final long currentTime) {

		// сумарзный рамер НПС и его цели
		final int size = actor.getMaxSize() + target.getMaxSize();

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "process guidance " + actor + ", target " + target + ", distance " + (actor.distanceTo(target) - size));
		}

		// время завершения "захода за бок цели"
		final long finishTime = ai.getVar(FINISH_GUIDANCE_TIME);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "finish time " + ((finishTime - currentTime) / 1000));
		}

		// если мы сейчас в процессе захода за бок цели
		if(finishTime > currentTime) {

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "process avoid...");
			}

			final int directionType = (int) ai.getVar(AVOID_DIRECTION_TYPE);

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "direction type " + directionType);
			}

			// двигатели должны быть на максимуме
			processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_100);

			// высчитываем новую точку захода за бок
			final Vector direction = GET_DIRECTION_FUNCTIONS[directionType].get(target.getRotation(), local.getNextVector());
			direction.multLocal(getAvoidDistance());
			direction.addLocal(target.getLocation());

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "avoid position " + direction + ", distance " + actor.distanceTo(direction));
			}

			// ставим задачу развернуться для полета на нее
			processTurnToPosition(ai, actor, direction, random, local, config, currentTime, false);
			return true;
		}

		// если мы уже вышли на минимальную дистанцию боя, нужно на полной
		// мощности заходить за бок цели
		if(actor.isInDistance(target, getMinBattleDistance() + size)) {

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "process start avoid...");
			}

			// определяем в какую сторону заходить будем
			final int directionType = random.nextInt(0, GET_DIRECTION_FUNCTIONS.length - 1);

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "random new direction type " + directionType);
			}

			// двигатели должны быть на максимуме
			processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_100);

			// высчитываем новую точку захода за бок
			final Vector direction = GET_DIRECTION_FUNCTIONS[directionType].get(target.getRotation(), local.getNextVector());
			direction.multLocal(getAvoidDistance());
			direction.addLocal(target.getLocation());

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "avoid position " + direction + ", distance " + actor.distanceTo(direction));
			}

			// ставим задачу развернуться для полета на нее
			processTurnToPosition(ai, actor, direction, random, local, config, currentTime, false);

			ai.setVar(AVOID_DIRECTION_TYPE, directionType);
			ai.setVar(FINISH_GUIDANCE_TIME, currentTime + getProcessAvoidTime());

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "avoid finish time " + currentTime + getProcessAvoidTime());
			}

			return true;
		}

		// если мы не в зоне атаки цели, то на полной мощности летим к ней
		if(!actor.isInDistance(target, getMaxBattleDistance() + size)) {

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "process fly to target 1");
			}

			processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_100);
			processTurnToPosition(ai, actor, target.getLocation(), random, local, config, currentTime, false);

			return true;
		}

		// если мы уже вышли на среднюю дистанцию боя, можно снизить скорость
		if(actor.isInDistance(target, getMiddleBattleDistance() + size)) {

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "process battle to target");
			}

			processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_30);
			return false;
		}

		processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_100);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "process fly to target 2");
		}

		return false;
	}

	/**
	 * Процесс разворота на указанную точку.
	 */
	protected <A extends Nps> void processTurnToPosition(final NpsAI ai, final A actor, final Vector position, final Random random, final LocalObjects local, final ConfigAi config,
			final long currentTime, final boolean force) {

		if(force || currentTime > ai.getVar(FINISH_TURNING)) {
			ai.setVar(FINISH_TURNING, currentTime + getTurningInterval());
			ai.addTask(TURN_TO_POSITION, position, false);
		}
	}
}

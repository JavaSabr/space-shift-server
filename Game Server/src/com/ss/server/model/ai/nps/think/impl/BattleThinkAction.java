package com.ss.server.model.ai.nps.think.impl;

import static com.ss.server.model.ai.nps.NpsAiState.WAIT;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.task.TaskFactory;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.spawn.Spawn;

/**
 * Реализация генератора действий в бою.
 * 
 * @author Ronn
 */
public class BattleThinkAction extends AbstractThinkAction {

	public static final String PROP_BATTLE_RANGE = "battleRange";

	/** боевой радиус */
	protected int battleRange;

	public BattleThinkAction(final Node node) {
		super(node);

		if(!isInitialized()) {
			return;
		}

		final VarTable vars = VarTable.newInstance(node);

		this.battleRange = vars.getInteger(PROP_BATTLE_RANGE, 5000);
	}

	/**
	 * Проверка нахождения {@link Nps} в допустимой зоне боевых действий
	 * относительно точки {@link Spawn}.
	 * 
	 * @param ai ИИ обрабатывающее действия.
	 * @param actor {@link Nps} который находится в бою.
	 * @return продолжить ли думать дальнейшие действия.
	 */
	protected <A extends Nps> boolean checkBattleRange(final NpsAI ai, final A actor) {

		final Vector spawnLoc = actor.getSpawnLoc();

		if(!actor.isInDistance(spawnLoc, getBattleRange())) {
			actor.clearAggroList();
			ai.setNewState(WAIT);
			ai.setTarget(null);
			ai.clearTaskQueue();
			return true;
		}

		return false;
	}

	/**
	 * Проверка, стоит ли дальше заниматься {@link Nps} указанной цулью.
	 * 
	 * @param actor {@link Nps} который ведет бой.
	 * @param mostHated текущая цель {@link Nps}.
	 * @return можно ли эту цель отменить.
	 */
	protected <A extends Nps> boolean checkCancelTarget(final A actor, final SpaceShip mostHated) {
		return mostHated.isDestructed();
	}

	/**
	 * @return боевой радиус.
	 */
	public int getBattleRange() {
		return battleRange;
	}

	@Override
	public <A extends Nps> void think(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		if(!isInitialized() || actor.isDestructed()) {
			return;
		}

		if(checkBattleRange(ai, actor)) {
			return;
		}

		SpaceShip mostHated = null;

		while((mostHated = actor.getMostHated()) != null) {

			if(checkCancelTarget(actor, mostHated)) {
				actor.removeAggro(mostHated, local);
				ai.clearTaskQueue();
				continue;
			}

			break;
		}

		if(mostHated == null) {
			ai.setNewState(WAIT);
			ai.clearTaskQueue();
			ai.setTarget(null);
			return;
		}

		ai.setTarget(mostHated);

		if(preCreateTask(ai, actor, local, config, currentTime)) {
			return;
		}

		final TaskFactory currentFactory = ai.getCurrentFactory();
		currentFactory.addNewTask(ai, actor, local, config, currentTime);

		if(postCreateTask(ai, actor, local, config, currentTime)) {
			return;
		}

		executeTasks(ai, actor, local, currentTime);
	}
}

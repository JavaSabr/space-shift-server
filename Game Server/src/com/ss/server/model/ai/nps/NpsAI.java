package com.ss.server.model.ai.nps;

import static com.ss.server.model.ai.nps.TaskAiType.SKILL_USE;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.linkedlist.LinkedList;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.ShipAI;
import com.ss.server.model.ai.nps.task.TaskFactory;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.SkillGroup;
import com.ss.server.task.impl.UpdateNpsAITask;

/**
 * Интерфейс для реализации АИ для НПС.
 * 
 * @author Ronn
 */
public interface NpsAI extends ShipAI {

	/**
	 * Отмена атаки цели.
	 */
	public void abortAttack();

	/**
	 * Добавить задание по использованию скила на указанную цель.
	 * 
	 * @param skill используемый скил.
	 * @param target цель скила.
	 * @param first вставить первым в очередь.
	 */
	public default void addSkillUseTask(final Skill skill, final SpaceObject target, final boolean first) {

		final TaskAi task = createTask();
		task.setType(SKILL_USE);
		task.setSkill(skill);
		task.setTarget(target);

		addTask(task, first);
	}

	/**
	 * Добавление задачи на обработку.
	 * 
	 * @param task новая задача.
	 * @param first вставить первым в очередь.
	 */
	public default void addTask(final TaskAi task, final boolean first) {

		final LinkedList<TaskAi> queue = getTaskQueue();

		synchronized(queue) {

			if(first) {
				queue.offerFirst(task);
			} else {
				queue.offerLast(task);
			}
		}
	}

	/**
	 * Добавление задачи указанного типа, для задач, которым не нужны
	 * дополнительные данные.
	 * 
	 * @param type тип задачи.
	 * @param first вставить первым в очередь.
	 */
	public default void addTask(final TaskAiType type, final boolean first) {

		final TaskAi task = createTask();
		task.setType(type);

		addTask(task, first);
	}

	/**
	 * Добавление задачи по указанному типу и цели.
	 * 
	 * @param type тип задачи.
	 * @param rotation разворот цели задачи.
	 * @param first вставить первым в очередь.
	 */
	public default void addTask(final TaskAiType type, final Rotation rotation, final boolean first) {

		final TaskAi task = createTask();
		task.setType(type);
		task.setRotation(rotation);

		addTask(task, first);
	}

	/**
	 * Добавление задачи по указанному типу и цели.
	 * 
	 * @param type тип задачи.
	 * @param target цель задачи.
	 * @param first вставить первым в очередь.
	 */
	public default void addTask(final TaskAiType type, final SpaceObject target, final boolean first) {

		final TaskAi task = createTask();
		task.setType(type);
		task.setTarget(target);

		addTask(task, first);
	}

	/**
	 * Добавление задачи по указанному типу и цели.
	 * 
	 * @param type тип задачи.
	 * @param target цель задачи.
	 * @param distance дистанция.
	 * @param first вставить первым в очередь.
	 */
	public default void addTask(final TaskAiType type, final SpaceObject target, final int distance, final boolean first) {

		final TaskAi task = createTask();
		task.setType(type);
		task.setDistance(distance);
		task.setTarget(target);

		addTask(task, first);
	}

	/**
	 * Добавление задачи по указанному типу и позиции.
	 * 
	 * @param type тип задачи.
	 * @param position позиция задачи.
	 * @param first вставить первым в очередь.
	 */
	public default void addTask(final TaskAiType type, final Vector position, final boolean first) {

		final TaskAi task = createTask();
		task.setType(type);
		task.setPosition(position);

		addTask(task, first);
	}

	/**
	 * Добавление задачи по указанному типу и цели.
	 * 
	 * @param type тип задачи.
	 * @param position позиция цели задачи.
	 * @param distance дистанция.
	 * @param first вставить первым в очередь.
	 */
	public default void addTask(final TaskAiType type, final Vector position, final int distance, final boolean first) {

		final TaskAi task = createTask();
		task.setType(type);
		task.setDistance(distance);
		task.setPosition(position);

		addTask(task, first);
	}

	/**
	 * Очистка очереди задач.
	 */
	public void clearTaskQueue();

	/**
	 * @return новая задача для выполнения.
	 */
	public TaskAi createTask();

	/**
	 * Выполнение задания.
	 * 
	 * @return запустить ли еще раз выполнение.
	 */
	public boolean doTask(Nps actor, long currentTime, LocalObjects local);

	/**
	 * Процесс обновление AI.
	 * 
	 * @param currentTime текущее время.
	 * @param local контейнер локальных объектов.
	 */
	public void doUpdate(long currentTime, LocalObjects local);

	/**
	 * Финиширование задачи АИ.
	 */
	public void finishTask(TaskAi task);

	/**
	 * @return дистанция грессии.
	 */
	public default int getAggroRange() {
		return getConfigAi().getAggroRange();
	}

	/**
	 * @return конфигурация Ai.
	 */
	public ConfigAi getConfigAi();

	/**
	 * @return текущая фабрика заданий.
	 */
	public default TaskFactory getCurrentFactory() {
		return getConfigAi().getFactory(getCurrentState());
	}

	/**
	 * @return текущее состояния AI.
	 */
	public NpsAiState getCurrentState();

	/**
	 * @return охраняемый объект.
	 */
	public SpaceObject getProtectedObject();

	/**
	 * @return рандоминайзер Ai.
	 */
	public Random getRandom();

	/**
	 * Получение случайного скила из указанной группы.
	 * 
	 * @param group группа скилов.
	 * @return случайный скил из указанной группы доступный Nps.
	 */
	public Skill getSkill(SkillGroup group);

	/**
	 * Получение всех скилов Nps указанной группы.
	 * 
	 * @param group группа скилов.
	 * @return доступные скилы указанной группы.
	 */
	public Skill[] getSkills(SkillGroup group);

	/**
	 * @return текущая цель для AI.
	 */
	@Override
	public SpaceObject getTarget();

	/**
	 * @return очередь задач.
	 */
	public LinkedList<TaskAi> getTaskQueue();

	/**
	 * @return задача по обновлению состояния AI.
	 */
	public UpdateNpsAITask getUpdateTask();

	/**
	 * @param type тип переменной.
	 * @return текущее значение.
	 */
	public long getVar(VarType type);

	/**
	 * @return активен ли сейчас AI.
	 */
	public default boolean isEnabled() {
		return true;
	}

	/**
	 * @return глобальный ли AI.
	 */
	public boolean isGlobal();

	/**
	 * @return есть ли ожидающие исполнения задания.
	 */
	public boolean isWaitingTask();

	/**
	 * @param state новое состояние AI.
	 */
	public void setCurrentState(NpsAiState state);

	/**
	 * @param enabled активен ли сейчас AI.
	 */
	public default void setEnabled(final boolean enabled) {
	}

	/**
	 * Установка нового состояния со сменой интервала исполнения задач.
	 * 
	 * @param state новое состояние Ai Nps.
	 */
	public void setNewState(NpsAiState state);

	/**
	 * @param protectedObject охраняемый объект.
	 */
	public void setProtectedObject(SpaceObject protectedObject);

	/**
	 * @param object новая цель для AI.
	 */
	@Override
	public void setTarget(SpaceObject object);

	/**
	 * @param type тип переменной.
	 * @param time новое значение.
	 */
	public void setVar(VarType type, long time);

	/**
	 * Запуск задач AI.
	 */
	public void startAITasks();

	/**
	 * Остановка задач AI.
	 */
	public void stopAITasks();
}

package com.ss.server.model.ai.nps.task.impl;

import static com.ss.server.model.ai.nps.TaskAiType.ACTIVATE_FORCE_SHIELD;
import static com.ss.server.model.ai.nps.TaskAiType.AVOID_COLLISION_OBJECT;
import static com.ss.server.model.ai.nps.TaskAiType.DEACTIVATE_FORCE_SHIELD;
import static com.ss.server.model.ai.nps.TaskAiType.TURN_TO_POSITION;
import static com.ss.server.model.ai.nps.VarType.FINISH_PROCESS_COLLISION;
import static com.ss.server.model.ai.nps.VarType.LAST_CHECK_FORCE_SHIELD;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ShipEventManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.ai.nps.TaskAiType;
import com.ss.server.model.ai.nps.task.TaskFactory;
import com.ss.server.model.impl.Space;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.ship.ShipUtils;
import com.ss.server.model.ship.nps.Nps;

/**
 * Базовая реализация фабрики задач.
 * 
 * @author Ronn
 */
public abstract class AbstractTaskFactory implements TaskFactory {

	protected static final Logger LOGGER = LoggerManager.getLogger(TaskFactory.class);

	public static final String PROP_CHECK_FORCE_SHIELD_INTERVAL = "checkForceShieldInterval";
	public static final String PROP_MAX_PROCESS_COLLISION_TIME = "maxProcessCollisionTime";
	public static final String PROP_MIN_PROCESS_COLLISSION_TIME = "minProcessCollissionTime";

	private static final Space SPACE = Space.getInstance();

	/** интервал проверки состояния силового поля */
	private int checkForceShieldInterval;
	/** минимальное время обработки столкновения */
	private int minProcessCollissionTime;
	/** максимальное время обработки столкновения */
	private int maxProcessCollisionTime;

	/** инитиализирован ли */
	private final boolean initialized;

	public AbstractTaskFactory(final Node node) {
		this.initialized = node != null;

		if(node == null) {
			return;
		}

		final VarTable vars = VarTable.newInstance(node, "set", "name", "val");

		this.checkForceShieldInterval = vars.getInteger(PROP_CHECK_FORCE_SHIELD_INTERVAL, 2000);
		this.minProcessCollissionTime = vars.getInteger(PROP_MIN_PROCESS_COLLISSION_TIME, 3000);
		this.maxProcessCollisionTime = vars.getInteger(PROP_MAX_PROCESS_COLLISION_TIME, 10000);
	}

	@Override
	public <A extends Nps> void addNewTask(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
	}

	/**
	 * @return интервал проверки состояния силового поля.
	 */
	protected int getCheckForceShieldInterval() {
		return checkForceShieldInterval;
	}

	/**
	 * @return максимальное время обработки столкновения.
	 */
	public int getMaxProcessCollisionTime() {
		return maxProcessCollisionTime;
	}

	/**
	 * @return минимальное время обработки столкновения.
	 */
	public int getMinProcessCollissionTime() {
		return minProcessCollissionTime;
	}

	/**
	 * @return инитиализирован ли.
	 */
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * Обработка возможности столкновения с объектами.
	 * 
	 * @return завершить выполнение фабрики.
	 */
	protected <A extends Nps> boolean processCollision(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		final float currentSpeed = actor.getCurrentSpeed();

		if(currentSpeed < 1) {
			return false;
		}

		if(ai.getVar(FINISH_PROCESS_COLLISION) > currentTime) {
			return true;
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "process collision...");
		}

		// дистанция в рамках которой идет поиск колизий
		// FIXME 200 вынести в параметр
		final float distance = 200 + actor.getCurrentSpeed() * 2;

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "find collision for distance " + distance);
		}

		// контейнер потенциальных объектов для столкновения
		final Array<SpaceObject> objects = local.getNextObjectList();

		// поиск потенциальных объектов столкновений
		SPACE.findCollision(actor, objects, distance);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "finded " + objects);
		}

		if(objects.isEmpty()) {
			return false;
		}

		final VectorBuffer buffer = local.getNextVectorBuffer();

		// контейнер для объектов, с которыми возможно столкновение
		final Array<SpaceObject> checked = local.getNextObjectList();

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "process check targets...");
		}

		final Vector location = actor.getLocation();
		final Vector direction = actor.getDirection();

		// ближайший объект с которым возможно столкновение
		SpaceObject near = null;
		float distanceToNear = 0;

		for(final SpaceObject object : objects.array()) {

			if(object == null) {
				break;
			}

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "check " + object);
			}

			final Bounding bounding = object.getBounding();

			if(bounding.intersects(location, direction, buffer)) {

				checked.add(object);

				if(LOGGER.isEnabledDebug()) {
					LOGGER.debug(this, "detect collision for target " + object);
				}

				final float distanceTo = object.distanceSquaredTo(location);

				if(near == null) {
					near = object;
					distanceToNear = distanceTo;
				} else if(distanceTo < distanceToNear) {
					near = object;
					distanceToNear = distanceTo;
				}
			}
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "checked " + checked);
		}

		if(checked.isEmpty()) {
			return false;
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "near object " + near);
		}

		final TaskAi task = ai.createTask();
		task.setType(AVOID_COLLISION_OBJECT);
		task.setDistance((int) distance);
		task.setNear(near);

		final Array<SpaceObject> taskObjects = task.getObjects();
		taskObjects.addAll(checked);

		long processTime = (long) Math.max(getMinProcessCollissionTime(), near.getMaxSize() / currentSpeed * 1000);
		processTime = Math.min(processTime, getMaxProcessCollisionTime());

		ai.setVar(FINISH_PROCESS_COLLISION, currentTime + processTime);
		ai.clearTaskQueue();
		ai.addTask(task, true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "add task " + task);
			LOGGER.debug(this, "process collision interval " + processTime);
		}

		return true;
	}

	/**
	 * Процесс активации двигателя.
	 */
	protected <A extends Nps> void processEngine(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime, final float power) {

		if(!actor.isFlying() && power != EnergyModule.ENERGY_POWER_0) {
			ai.addTask(TaskAiType.ACTIVAE_ENGINE, false);
		}

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "current engine power " + actor.getEngineEnergy());
		}

		if(actor.getEngineEnergy() != power) {

			final float currentEngine = actor.getEngineEnergy();
			actor.setEngineEnergy(power);

			final ShipEventManager eventManager = ShipEventManager.getInstance();
			eventManager.notifyEngineEnergyChanged(currentEngine, actor, local);
		}
	}

	/**
	 * Процесс включения/выключения силового поля.
	 */
	protected <A extends Nps> void processForceShield(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime, final boolean deactivated) {

		if(currentTime < ai.getVar(LAST_CHECK_FORCE_SHIELD)) {
			return;
		}

		if(deactivated && ShipUtils.isActiveForceShield(actor)) {
			ai.addTask(DEACTIVATE_FORCE_SHIELD, false);
		} else if(!deactivated && !ShipUtils.isActiveForceShield(actor) && ShipUtils.canActivateForceShield(actor)) {
			ai.addTask(ACTIVATE_FORCE_SHIELD, false);
		}

		ai.setVar(LAST_CHECK_FORCE_SHIELD, currentTime + getCheckForceShieldInterval());
	}

	protected <A extends Nps> void processDebugCollision(final NpsAI ai, final A actor, final LocalObjects local) {

		SpaceObject target = ai.getTarget();

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "process debug collision...");
		}

		if(target == null || !actor.canCollision(target)) {

			final Array<SpaceObject> objects = local.getNextObjectList();
			final Random random = local.getRandom();

			final Space space = Space.getInstance();
			space.addAround(LocationObject.class, actor, objects);

			for(SpaceObject object : objects) {

				if(object.distanceTo(actor) > 1000) {
					continue;
				}

				if(random.chance(20)) {
					target = object;
				}

				break;
			}

			ai.setTarget(target);
		}

		if(target != null) {

			if(LOGGER.isEnabledDebug()) {
				LOGGER.debug(this, "finded debug collision target " + target + ", distance " + target.distanceTo(actor));
			}

			ai.addTask(TURN_TO_POSITION, target.getLocation(), false);
		}
	}
}

package com.ss.server.model.ai.nps.task.impl;

import org.w3c.dom.Node;

import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;

/**
 * Фабрика задач при боевом состоянии защитника станции.
 * 
 * @author Ronn
 */
public class StationDefenderBattleTaskFactory extends BattleTaskFactory {

	public StationDefenderBattleTaskFactory(final Node node) {
		super(node);
	}

	@Override
	protected <A extends Nps> boolean processCollision(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {
		return false;
	}

	@Override
	protected <A extends Nps> boolean processGuidance(final NpsAI ai, final A actor, final SpaceShip target, final Random random, final LocalObjects local, final ConfigAi config,
			final long currentTime) {

		final int size = actor.getMaxSize() + target.getMaxSize();

		if(!actor.isInDistance(target, getMaxBattleDistance() + size)) {
			return true;
		} else if(actor.isInDistance(target, getMinBattleDistance() + size)) {
			processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_10);
			processTurnToPosition(ai, actor, target.getLocation(), random, local, config, currentTime, false);
			return false;
		} else {
			processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_10);
		}

		return false;
	}
}

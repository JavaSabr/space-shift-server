package com.ss.server.model.ai.nps;

import com.ss.server.model.ai.nps.task.handler.TaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.ActivateEngineTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.ActivateForceShieldTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.AvoidCollissionObjectTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.DeactivateEngineTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.DeactivateForceShieldTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.RotationTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.SkillUseTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.TurnToAvoidObjectTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.TurnToAvoidPositionTaskHandler;
import com.ss.server.model.ai.nps.task.handler.impl.TurnToPositionTaskHandler;

/**
 * Перечисление типов задач Ai.
 * 
 * @author Ronn
 */
public enum TaskAiType {

	ROTATION(new RotationTaskHandler()),
	SKILL_USE(new SkillUseTaskHandler()),
	TURN_TO_POSITION(new TurnToPositionTaskHandler()),

	/**
	 * разворот к объекту в таком виде, что бы ео облететь сбоку.
	 */
	TURN_TO_AVOID_OBJECT(new TurnToAvoidObjectTaskHandler()),

	/** задача по развороту дял облета указанной позиции */
	TURN_TO_AVOID_POSITION(new TurnToAvoidPositionTaskHandler()),

	/** активация всех двигателей */
	ACTIVAE_ENGINE(new ActivateEngineTaskHandler()),
	/** деактивация всех двигателей */
	DEACTIVATE_ENGINE(new DeactivateEngineTaskHandler()),
	/** активация силового поля */
	ACTIVATE_FORCE_SHIELD(new ActivateForceShieldTaskHandler()),
	/** деактивация силового поля */
	DEACTIVATE_FORCE_SHIELD(new DeactivateForceShieldTaskHandler()),
	/** задача по облету объекта столкновения */
	AVOID_COLLISION_OBJECT(new AvoidCollissionObjectTaskHandler()), ;

	/** обработчик задачи */
	private final TaskHandler handler;

	private TaskAiType() {
		this.handler = null;
	}

	private TaskAiType(final TaskHandler handler) {
		this.handler = handler;
	}

	/**
	 * @return обработчик задачи.
	 */
	public TaskHandler getHandler() {
		return handler;
	}
}

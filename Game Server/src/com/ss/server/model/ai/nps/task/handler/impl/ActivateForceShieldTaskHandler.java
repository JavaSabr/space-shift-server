package com.ss.server.model.ai.nps.task.handler.impl;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.ActivableSkill;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.SkillGroup;

/**
 * Обработка активации силовых полей корабля.
 * 
 * @author Ronn
 */
public class ActivateForceShieldTaskHandler extends AbstractTaskHandler {

	@Override
	public boolean handle(final NpsAI ai, final Nps actor, final TaskAi task, final long currentTime, final LocalObjects local) {

		final Skill[] skills = ai.getSkills(SkillGroup.FORCE_SHIELD);

		for(final Skill skill : skills) {

			if(!(skill instanceof ActivableSkill)) {
				continue;
			}

			final ActivableSkill activableSkill = (ActivableSkill) skill;

			if(activableSkill.isActive() || activableSkill.isReloading()) {
				continue;
			}

			ai.startSkill(actor.getLocation(), activableSkill, local);
		}

		return true;
	}
}

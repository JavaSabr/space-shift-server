package com.ss.server.model.ai.nps;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.Foldable;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.skills.Skill;

/**
 * Реализация задачи для выполнения AI.
 * 
 * @author Ronn
 */
public final class TaskAi implements Foldable {

	/** список объектов задачи */
	private final Array<SpaceObject> objects;

	/** позиция для задачи */
	private final Vector position;
	/** разворот для задачи */
	private final Rotation rotation;

	/** цель задачи */
	private SpaceObject target;
	/** ближайший объект */
	private SpaceObject near;
	/** умение для задания */
	private Skill skill;
	/** тип задачи */
	private TaskAiType type;

	/** дистанция */
	private int distance;

	public TaskAi() {
		this.position = Vector.newInstance();
		this.rotation = Rotation.newInstance();
		this.objects = ArrayFactory.newArray(SpaceObject.class);
	}

	@Override
	public void finalyze() {
		setTarget(null);
		setType(null);
		setSkill(null);
		setNear(null);

		final Array<SpaceObject> objects = getObjects();
		objects.clear();
	}

	public int getDistance() {
		return distance;
	}

	public Vector getPosition() {
		return position;
	}

	public Rotation getRotation() {
		return rotation;
	}

	public Skill getSkill() {
		return skill;
	}

	public SpaceObject getTarget() {
		return target;
	}

	public TaskAiType getType() {
		return type;
	}

	@Override
	public void reinit() {
	}

	public void setDistance(final int distance) {
		this.distance = distance;
	}

	public void setPosition(final Vector position) {
		this.position.set(position);
	}

	public void setRotation(final Rotation rotation) {
		this.rotation.set(rotation);
	}

	public void setSkill(final Skill skill) {
		this.skill = skill;
	}

	public void setTarget(final SpaceObject target) {
		this.target = target;
	}

	public void setType(final TaskAiType type) {
		this.type = type;
	}

	public Array<SpaceObject> getObjects() {
		return objects;
	}

	public void setNear(SpaceObject near) {
		this.near = near;
	}

	public SpaceObject getNear() {
		return near;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [objects=" + objects + ", position=" + position + ", rotation=" + rotation + ", target=" + target + ", near=" + near + ", skill=" + skill + ", type="
				+ type + ", distance=" + distance + "]";
	}
}

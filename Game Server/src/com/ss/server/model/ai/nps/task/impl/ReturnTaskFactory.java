package com.ss.server.model.ai.nps.task.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAiType;
import com.ss.server.model.ai.nps.VarType;
import com.ss.server.model.module.EnergyModule;
import com.ss.server.model.ship.nps.Nps;

/**
 * Фабрика задач по возвращеению домой.
 * 
 * @author Ronn
 */
public class ReturnTaskFactory extends AbstractTaskFactory {

	private static final String PROP_TURNING_INTERVAL = "turningInterval";

	/** интервал разворота */
	protected int turningInterval;

	public ReturnTaskFactory(final Node node) {
		super(node);

		if(!isInitialized()) {
			return;
		}

		final VarTable vars = VarTable.newInstance(node, "set", "name", "val");

		this.turningInterval = vars.getInteger(PROP_TURNING_INTERVAL, 5000);
	}

	@Override
	public <A extends Nps> void addNewTask(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		if(!isInitialized() || actor.isDestructed()) {
			return;
		}

		if(processCollision(ai, actor, local, config, currentTime)) {
			return;
		}

		if(processTurnToSpawn(ai, actor, local, config, currentTime)) {
			return;
		}
	}

	/**
	 * @return интервал разворота.
	 */
	public int getTurningInterval() {
		return turningInterval;
	}

	/**
	 * Процесс раворота на позицию спавна.
	 * 
	 * @return стоит ли прервать дальнейшую обработку.
	 */
	protected <A extends Nps> boolean processTurnToSpawn(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		processEngine(ai, actor, local, config, currentTime, EnergyModule.ENERGY_POWER_100);

		final int turningInterval = getTurningInterval();

		final long time = ai.getVar(VarType.FINISH_TURNING);

		if(currentTime - time > turningInterval) {

			final Random random = ai.getRandom();

			ai.addTask(TaskAiType.TURN_TO_POSITION, actor.getSpawnLoc(), false);
			ai.setVar(VarType.FINISH_TURNING, currentTime + random.nextInt(turningInterval, turningInterval * 2));
		}

		return true;
	}
}

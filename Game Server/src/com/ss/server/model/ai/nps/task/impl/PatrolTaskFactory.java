package com.ss.server.model.ai.nps.task.impl;

import org.w3c.dom.Node;

/**
 * Фабрика задач при патрулировании.
 * 
 * @author Ronn
 */
public class PatrolTaskFactory extends AbstractTaskFactory {

	public PatrolTaskFactory(final Node node) {
		super(node);
	}

}

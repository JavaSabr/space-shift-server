package com.ss.server.model.ai.nps.task.impl;

import org.w3c.dom.Node;

/**
 * Пустая реализация фабрики задач.
 * 
 * @author Ronn
 */
public class EmptyTaskFactory extends AbstractTaskFactory {

	public EmptyTaskFactory(final Node node) {
		super(node);
	}

}

package com.ss.server.model.ai.nps.task.handler.impl;

import rlib.geom.DirectionType;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.ship.nps.Nps;

/**
 * Обработчик задачи по развороту для облета указанной позиции.
 * 
 * @author Ronn
 */
public class TurnToAvoidPositionTaskHandler extends TurnToPositionTaskHandler {

	protected Vector getAvoidPosition(final TaskAi task) {
		return task.getPosition();
	}

	@Override
	public boolean handle(final NpsAI ai, final Nps actor, final TaskAi task, final long currentTime, final LocalObjects local) {

		final Vector position = getAvoidPosition(task);

		final int avoidDistance = task.getDistance();
		final int size = (int) (actor.getCurrentSpeed() * 5);

		final Rotation currentRotation = actor.getRotation();
		final Rotation targetRotation = local.getNextRotation();

		final Vector currentLocation = actor.getLocation();
		final Vector temp = local.getNextVector();
		final Vector check = local.getNextVector();

		// определние разворота на объект
		final Vector up = currentRotation.getVectorDirection(DirectionType.UP, local.getNextVector());
		final Vector direction = local.getNextVector();
		direction.set(position);
		direction.subtractLocal(currentLocation);

		targetRotation.lookAt(direction, up, local);

		// определение вариантов направлений полета
		final Vector top = targetRotation.getVectorDirection(DirectionType.UP, local.getNextVector());
		final Vector left = targetRotation.getVectorDirection(DirectionType.LEFT, local.getNextVector());

		final Vector bottom = local.getNextVector();
		bottom.set(top).negateLocal();

		final Vector right = local.getNextVector();
		right.set(left).negateLocal();

		// добавление смещения позиции цели
		top.multLocal(avoidDistance).addLocal(position);
		bottom.multLocal(avoidDistance).addLocal(position);
		left.multLocal(avoidDistance).addLocal(position);
		right.multLocal(avoidDistance).addLocal(position);

		// определяем кратчайшее направление
		Vector targetVector = null;

		final Array<SpaceObject> objects = local.getNextObjectList();

		SPACE.addAround(SpaceObject.class, actor, objects);

		float min = Float.MAX_VALUE;
		float distance = currentLocation.distanceSquared(top);

		if(distance < min && checkCollision(objects, actor, top, temp, check, size)) {
			targetVector = top;
			min = distance;
		}

		distance = currentLocation.distanceSquared(bottom);

		if(distance < min && checkCollision(objects, actor, bottom, temp, check, size)) {
			targetVector = bottom;
			min = distance;
		}

		distance = currentLocation.distanceSquared(left);

		if(distance < min && checkCollision(objects, actor, left, temp, check, size)) {
			targetVector = left;
			min = distance;
		}

		distance = currentLocation.distanceSquared(right);

		if(distance < min && checkCollision(objects, actor, right, temp, check, size)) {
			targetVector = right;
			min = distance;
		}

		if(targetVector == null) {
			return true;
		}

		// определние разворота на кратчайшее направление
		targetVector.subtractLocal(currentLocation);
		targetRotation.lookAt(targetVector, up, local);

		// запускаем разворот
		ai.startRotation(local, currentRotation, targetRotation, false);
		return true;
	}
}

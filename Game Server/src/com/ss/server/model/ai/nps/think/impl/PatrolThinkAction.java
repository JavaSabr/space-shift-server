package com.ss.server.model.ai.nps.think.impl;

import org.w3c.dom.Node;

/**
 * Реализация генератора действий в режиме патрулирования.
 * 
 * @author Ronn
 */
public class PatrolThinkAction extends AbstractThinkAction {

	public PatrolThinkAction(final Node node) {
		super(node);
	}
}

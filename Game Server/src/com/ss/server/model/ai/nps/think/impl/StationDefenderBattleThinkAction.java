package com.ss.server.model.ai.nps.think.impl;

import org.w3c.dom.Node;

import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.nps.StationDefenderNps;
import com.ss.server.model.station.SpaceStation;

/**
 * Htfлизация думателя в боевом состоянии для {@link StationDefenderNps}.
 * 
 * @author Ronn
 */
public class StationDefenderBattleThinkAction extends BattleThinkAction {

	public StationDefenderBattleThinkAction(final Node node) {
		super(node);
	}

	@Override
	protected <A extends Nps> boolean checkBattleRange(final NpsAI ai, final A actor) {
		return false;
	}

	@Override
	protected <A extends Nps> boolean checkCancelTarget(final A actor, final SpaceShip mostHated) {

		final StationDefenderNps defenderNps = (StationDefenderNps) actor;
		final SpaceStation station = defenderNps.getProtectedStation();

		return mostHated.isDestructed() || !station.isInDistance(mostHated, getBattleRange());
	}
}

package com.ss.server.model.ai.nps.think.impl;

import org.w3c.dom.Node;

import rlib.util.array.Array;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.nps.StationDefenderNps;
import com.ss.server.model.station.SpaceStation;

/**
 * Реализация думателя в режиме ожидания для защитника станции.
 * 
 * @author Ronn
 */
public class StationDefenderWaitThinkAction extends WaitThinkAction {

	public StationDefenderWaitThinkAction(final Node node) {
		super(node);
	}

	@Override
	protected <A extends Nps> boolean checkDistance(final NpsAI ai, final A actor) {
		return false;
	}

	@Override
	protected <A extends Nps> void findTargets(final NpsAI ai, final A actor, final LocalObjects local) {

		final StationDefenderNps defenderNps = (StationDefenderNps) actor;
		final SpaceStation station = defenderNps.getProtectedStation();

		final int safeRadius = station.getDestroyRadius();

		if(safeRadius < 1) {
			return;
		}

		final Array<SpaceObject> container = local.getNextObjectList();
		final Random random = ai.getRandom();

		SPACE.addAround(SpaceShip.class, station, container, safeRadius);

		for(final SpaceObject object : container.array()) {

			if(object == null) {
				break;
			}

			if(actor.checkAgression(object)) {

				if(LOGGER.isEnabledDebug()) {
					LOGGER.debug(this, "detect new target " + object + " from " + actor);
				}

				actor.addAggro(object.getSpaceShip(), 0, random.nextInt(1, 100), local);
			}
		}
	}

	@Override
	public <A extends Nps> void think(final NpsAI ai, final A actor, final LocalObjects local, final ConfigAi config, final long currentTime) {

		if(!actor.isStationDefender()) {
			throw new RuntimeException("incorrect NPS class " + actor);
		}

		super.think(ai, actor, local, config, currentTime);
	}
}

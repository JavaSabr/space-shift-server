package com.ss.server.model.ai.nps.task;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.ConfigAi;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ship.nps.Nps;

/**
 * Интерфейс для реализации фабрики заданий для AI Nps.
 * 
 * @author Ronn
 */
public interface TaskFactory {

	/**
	 * Создать новую задачу.
	 * 
	 * @param ai аи, которое создает задачу.
	 * @param actor, нпс, которому надо придумать что делать.
	 * @param local локальные объекты.
	 * @param config конфигурация АИ.
	 * @param currentTime текущее время.
	 */
	public <A extends Nps> void addNewTask(NpsAI ai, A actor, LocalObjects local, ConfigAi config, long currentTime);
}

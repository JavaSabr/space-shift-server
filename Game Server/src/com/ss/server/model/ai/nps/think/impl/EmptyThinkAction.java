package com.ss.server.model.ai.nps.think.impl;

import org.w3c.dom.Node;

/**
 * Реализация пустого думателя.
 * 
 * @author Ronn
 */
public class EmptyThinkAction extends AbstractThinkAction {

	public EmptyThinkAction(final Node node) {
		super(node);
	}
}

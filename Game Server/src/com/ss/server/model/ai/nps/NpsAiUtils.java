package com.ss.server.model.ai.nps;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.skills.SkillGroup;

/**
 * Класс с утильными методами для Ai Nps.
 * 
 * @author Ronn
 */
public class NpsAiUtils {

	/**
	 * Разбиение по группам всех скилов Nps для использования их в Ai.
	 * 
	 * @param skills контейнер групп.
	 * @param nps Nps, чьи скилы надо разбить на группы.
	 */
	@SuppressWarnings("unchecked")
	public static void prepareSkills(final Skill[][] skills, final Nps nps) {

		final Array<Skill>[] temps = new Array[SkillGroup.SIZE];

		for(int i = 0, length = temps.length; i < length; i++) {
			temps[i] = ArrayFactory.newArray(Skill.class);
		}

		for(final Skill[] pckg : nps.getSkillTable()) {
			for(final Skill skill : pckg) {
				final SkillGroup group = skill.getSkillGroup();
				temps[group.ordinal()].add(skill);
			}
		}

		for(int i = 0, length = skills.length; i < length; i++) {
			final Array<Skill> container = temps[i];
			skills[i] = container.toArray(new Skill[container.size()]);
		}
	}
}

package com.ss.server.model.ai.nps.task.handler.impl;

import static rlib.geom.DirectionType.UP;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.nps.NpsAI;
import com.ss.server.model.ai.nps.TaskAi;
import com.ss.server.model.impl.Space;
import com.ss.server.model.ship.nps.Nps;

/**
 * Обработчик задачи по развороту на указанную позицию.
 * 
 * @author Ronn
 */
public class TurnToPositionTaskHandler extends AbstractTaskHandler {

	protected static final Space SPACE = Space.getInstance();

	/**
	 * Проверка столкновения при выборе направления разворота.
	 * 
	 * @param objects список окружающих объектов.
	 * @param filtred фильтрованный список.
	 * @param actor разворачиваемый корабль.
	 * @param position позиция целевого направления.
	 * @param size размер корабля.
	 * @return можно ли в то направление лететь.
	 */
	protected final boolean checkCollision(final Array<SpaceObject> objects, final Nps actor, final Vector position, final Vector direction, final Vector check, final int size) {

		if(objects.isEmpty()) {
			return true;
		}

		return true;

		// FIXME обновить алгоритм
		// final Vector location = actor.getLocation();
		//
		// direction.set(position).subtractLocal(location);
		// direction.normalizeLocal();
		//
		// check.set(direction).multLocal(size);
		// check.addLocal(location);
		//
		// for(final SpaceObject object : objects.array()) {
		//
		// if(object == null) {
		// break;
		// }
		//
		// if(actor.canCollision(object) && object.isInDistance(check,
		// object.getMaxSize())) {
		// return false;
		// }
		// }
		//
		// return true;
	}

	@Override
	public boolean handle(final NpsAI ai, final Nps actor, final TaskAi task, final long currentTime, final LocalObjects local) {

		final Vector position = task.getPosition();
		final Vector direction = local.getNextVector();
		direction.set(position);
		direction.subtractLocal(actor.getLocation());
		direction.normalizeLocal();

		final Rotation currentRotation = actor.getRotation();
		final Vector up = currentRotation.getVectorDirection(UP, local.getNextVector());

		final Rotation targetRotation = local.getNextRotation();
		targetRotation.lookAt(direction, up, local);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug(this, "add rotation task to position: " + currentRotation.dot(targetRotation) + ", " + targetRotation);
		}

		ai.startRotation(local, currentRotation, targetRotation, false);
		return true;
	}
}

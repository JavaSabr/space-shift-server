package com.ss.server.model.quest.reward.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.SystemMessageVars;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.AddItemQuestReward;
import com.ss.server.model.quest.reward.QuestRewardClass;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.storage.Storage;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Реализация награды по выдаче стакуемых предметов.
 * 
 * @author Ronn
 */
public class AddStackableItemQuestReward extends AbstractQuestReqward implements AddItemQuestReward {

	public static final String PROP_ID = "id";
	public static final String PROP_COUNT = "count";

	/** ид выдаваемого предмета */
	private final int id;
	/** кол-во выдаваемого предмета */
	private final long count;

	public AddStackableItemQuestReward(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);

		this.count = vars.getLong(PROP_COUNT);
		this.id = vars.getInteger(PROP_ID);
	}

	@Override
	public void apply(final QuestEvent event, final LocalObjects local) {

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(this, new RuntimeException("not found player ship."));
			return;
		}

		final Storage storage = playerShip.getStorage();

		if(storage == null) {
			LOGGER.warning(this, new RuntimeException("not found storage for player ship " + playerShip));
			return;
		}

		final ItemTable itemTable = ItemTable.getInstance();
		final ItemTemplate template = itemTable.getTemplate(id);

		if(template == null) {
			return;
		}

		storage.lock();
		try {

			storage.addItem(id, count, String.valueOf(getQuest()));

			final ResponseSystemMessage responseSystemMessage = ResponseSystemMessage.getInstance(SystemMessageType.TELEPORTED_ITEM, local);
			responseSystemMessage.addVar(SystemMessageVars.VAR_ITEM_NAME, template.getName());
			responseSystemMessage.addVar(SystemMessageVars.VAR_ITEM_COUNT, String.valueOf(count));

			playerShip.sendPacket(responseSystemMessage, true);

		} finally {
			storage.unlock();
		}
	}

	@Override
	public long getItemCount(final QuestEvent event) {
		return count;
	}

	@Override
	public int getItemId(final QuestEvent event) {
		return id;
	}

	@Override
	public QuestRewardClass getRewardClass() {
		return QuestRewardClass.ITEM;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [id=" + id + ", count=" + count + "]";
	}
}

package com.ss.server.model.quest.reward;

import java.nio.ByteBuffer;

import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.network.game.ServerPacket;

/**
 * перечисление классов наград за задание.
 * 
 * @author Ronn
 */
public enum QuestRewardClass {

	EXPERIENCE {

		@Override
		public void writeTo(final ServerPacket packet, final ByteBuffer buffer, final QuestReward reward, final QuestEvent event) {
			final ExperienceQuestReward expReward = (ExperienceQuestReward) reward;
			packet.writeLong(buffer, expReward.getReward(event));
		}
	},
	ITEM {

		@Override
		public void writeTo(final ServerPacket packet, final ByteBuffer buffer, final QuestReward reward, final QuestEvent event) {
			final AddItemQuestReward itemReward = (AddItemQuestReward) reward;
			packet.writeInt(buffer, itemReward.getItemId(event));
			packet.writeLong(buffer, itemReward.getItemCount(event));
		}
	};

	public void writeTo(final ServerPacket packet, final ByteBuffer buffer, final QuestReward reward, final QuestEvent event) {
		throw new RuntimeException("not implemented.");
	}
}

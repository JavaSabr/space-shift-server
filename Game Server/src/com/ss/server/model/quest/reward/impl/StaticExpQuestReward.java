package com.ss.server.model.quest.reward.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.ExperienceQuestReward;
import com.ss.server.model.quest.reward.QuestRewardClass;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Реализация награды по выдаче опыта.
 * 
 * @author Ronn
 */
public class StaticExpQuestReward extends AbstractQuestReqward implements ExperienceQuestReward {

	public static final String PROP_EXP = "exp";

	/** выдаваемое кол-во опыта */
	private final long exp;

	public StaticExpQuestReward(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);

		this.exp = vars.getLong(PROP_EXP);
	}

	@Override
	public void apply(final QuestEvent event, final LocalObjects local) {

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(this, new RuntimeException("not found player ship."));
			return;
		}

		playerShip.addExp(exp, local);
	}

	@Override
	public long getReward(final QuestEvent event) {
		return exp;
	}

	@Override
	public QuestRewardClass getRewardClass() {
		return QuestRewardClass.EXPERIENCE;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [exp=" + exp + "]";
	}
}

package com.ss.server.model.quest.reward;

import com.ss.server.model.quest.event.QuestEvent;

/**
 * Интерфейс для реализации наград по выдачи опыта.
 * 
 * @author Ronn
 */
public interface ExperienceQuestReward extends QuestReward {

	/**
	 * Выдаваемое кол-во опыта для такого-то случая.
	 * 
	 * @param event событие задания.
	 * @return выдаваемое кол-во опыта.
	 */
	public long getReward(QuestEvent event);
}

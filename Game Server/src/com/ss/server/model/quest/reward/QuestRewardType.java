package com.ss.server.model.quest.reward;

import java.lang.reflect.Constructor;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ClassUtils;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;

/**
 * Перечисление типов наград.
 * 
 * @author Ronn
 */
public class QuestRewardType {

	/**
	 * @param name исходное назввание типа награды.
	 * @return форматированное название типа награды.
	 */
	protected static String formatName(final String name) {
		return name.replace(QuestReward.class.getSimpleName(), StringUtils.EMPTY);
	}

	public static final QuestRewardType getRewardType(final String name) {
		return TABLE.get(name);
	}

	public static final void register(final QuestRewardType rewardType) {

		if(TABLE.containsKey(rewardType.getName())) {
			LOGGER.warning(new RuntimeException("found duplicate rewardType for name " + rewardType.getName()));
		}

		TABLE.put(rewardType.getName(), rewardType);
	}

	private static final Logger LOGGER = LoggerManager.getLogger(QuestRewardType.class);

	private static final Table<String, QuestRewardType> TABLE = TableFactory.newObjectTable();

	/** название типа */
	private final String name;

	/** конструктор награды */
	private final Constructor<? extends QuestReward> constructor;

	public QuestRewardType(final String name, final Class<? extends QuestReward> type) {
		this.name = formatName(name);
		this.constructor = ClassUtils.getConstructor(type, Quest.class, Condition.class, VarTable.class);
	}

	/**
	 * @return название типа.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Создать новый инстанс награды.
	 * 
	 * @param quest задание, которому принадлежит награда.
	 * @param condition условие выполнения награды.
	 * @param vars таблица атрибутов.
	 * @return новый инстанс награды.
	 */
	public QuestReward newInstance(final Quest quest, final Condition condition, final VarTable vars) {
		return ClassUtils.newInstance(constructor, quest, condition, vars);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + name + ", constructor=" + constructor + "]";
	}
}

package com.ss.server.model.quest.reward;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Интерфей с для реализации награды за задание.
 * 
 * @author Ronn
 */
public interface QuestReward {

	/**
	 * Приминение награды.
	 * 
	 * @param event событие задания.
	 * @param local контейнер локальных объектов.
	 */
	public void apply(QuestEvent event, LocalObjects local);

	/**
	 * @return класс награды.
	 */
	public QuestRewardClass getRewardClass();

	/**
	 * Тест выполнения условий.
	 * 
	 * @param event событие задания.
	 * @param local контейнер локальных объектов.
	 * @return выполняются ли условия.
	 */
	public boolean test(QuestEvent event, LocalObjects local);
}

package com.ss.server.model.quest.reward.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.QuestReward;

/**
 * Базовая реализация награды за задание.
 * 
 * @author Ronn
 */
public abstract class AbstractQuestReqward implements QuestReward {

	protected static final Logger LOGGER = LoggerManager.getLogger(QuestReward.class);

	/** задание ,которому принадлежит награда */
	private final Quest quest;
	/** условие выдачи награды */
	private final Condition condition;

	public AbstractQuestReqward(final Quest quest, final Condition condition, final VarTable vars) {
		this.quest = quest;
		this.condition = condition;
	}

	/**
	 * @return условие выдачи награды.
	 */
	protected Condition getCondition() {
		return condition;
	}

	/**
	 * @return задание ,которому принадлежит награда.
	 */
	protected Quest getQuest() {
		return quest;
	}

	@Override
	public boolean test(final QuestEvent event, final LocalObjects local) {

		final Condition condition = getCondition();

		if(condition == null || condition.test(event)) {
			return true;
		}

		return false;
	}
}

package com.ss.server.model.quest.reward;

import com.ss.server.model.quest.event.QuestEvent;

/**
 * Интерфейс для реализации награды в виде предметов за задание.
 * 
 * @author Ronn
 */
public interface AddItemQuestReward extends QuestReward {

	/**
	 * @param event событие задания.
	 * @return кол-во выдываемого предмета.
	 */
	public long getItemCount(QuestEvent event);

	/**
	 * @param event событие задания.
	 * @return ид шаблона выдаваемого предмета.
	 */
	public int getItemId(QuestEvent event);
}

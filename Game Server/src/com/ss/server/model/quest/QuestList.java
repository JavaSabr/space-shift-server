package com.ss.server.model.quest;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import rlib.concurrent.lock.AsynReadSynWriteLock;
import rlib.concurrent.lock.LockFactory;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.database.QuestDBManager;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Реализация контейнера списка заданий игрока.
 * 
 * @author Ronn
 */
public final class QuestList implements Foldable, AsynReadSynWriteLock {

	public static QuestList getInstance() {

		QuestList list = POOL.take();

		if(list == null) {
			list = new QuestList();
		}

		return list;
	}

	private static final QuestDBManager QUEST_DB_MANAGER = QuestDBManager.getInstance();

	private static final FoldablePool<QuestList> POOL = PoolFactory.newAtomicFoldablePool(QuestList.class);

	private final Consumer<QuestDate> COMPLATE_DATE_FOLD = date -> {
		final FoldablePool<QuestDate> pool = getDatePool();
		pool.put(date);
	};

	private final Consumer<QuestState> QUEST_STATE_FOLD = questState -> {
		final FoldablePool<QuestState> pool = getStatePool();
		pool.put(questState);
	};

	/** функция сохранения переменных у заданий */
	private final Consumer<QuestState> QUEST_STATE_SAVE_VARS = questState -> questState.saveVars();

	/** пул штампов */
	private final FoldablePool<QuestDate> datePool;
	/** пул состояний квестов */
	private final FoldablePool<QuestState> statePool;

	/** таблица выполненых квестов */
	private final Table<IntKey, QuestDate> completed;
	/** список активных квестов */
	private final Array<QuestState> active;

	/** синхронизатор действий над списком */
	private final AsynReadSynWriteLock lock;

	/** фабрика уникальных ид для заданий игрока */
	private final AtomicInteger objectIdFactory;

	/** владелец списка квестов */
	private PlayerShip owner;

	public QuestList() {
		this.lock = LockFactory.newPrimitiveAtomicARSWLock();
		this.datePool = PoolFactory.newFoldablePool(QuestDate.class);
		this.statePool = PoolFactory.newFoldablePool(QuestState.class);
		this.completed = TableFactory.newIntegerTable();
		this.active = ArrayFactory.newArray(QuestState.class);
		this.objectIdFactory = new AtomicInteger(1);
	}

	/**
	 * Добавление активного квеста в список.
	 * 
	 * @param quest активный квест.
	 * @param state стадия выполнения.
	 * @param favorite находится ли в избранном.
	 */
	public void addActiveQuest(final Quest quest, final int state, final boolean favorite) {

		final PlayerShip owner = getOwner();

		QuestState questState = statePool.take();

		if(questState == null) {
			questState = new QuestState(objectIdFactory.incrementAndGet());
		}

		questState.setQuest(quest);
		questState.setState(state);
		questState.setOwner(owner);
		questState.setFavorite(favorite);
		questState.setStatus(QuestStatus.IN_PROGRESS);
		questState.loadVars();

		final Array<QuestState> active = getActive();
		active.add(questState);
	}

	/**
	 * Добавление всех выполненных квестов в указанный контейнер.
	 * 
	 * @param container контейнер выполненных квестов.
	 */
	public void addCompletedTo(final Array<QuestDate> container) {
		getCompleted().values(container);
	}

	/**
	 * Добавление выполненного квеста.
	 * 
	 * @param quest выполненный квест.
	 * @param status статус завершения задания.
	 * @param time дата выполнения.
	 */
	public QuestDate addFinishQuest(final Quest quest, final QuestStatus status, final long time) {

		QuestDate date = datePool.take();

		if(date == null) {
			date = new QuestDate();
		}

		date.setQuest(quest);
		date.setDate(time);
		date.setStatus(status);

		completed.put(quest.getId(), date);

		return date;
	}

	/**
	 * Добавление нового квеста в список активных.
	 * 
	 * @param quest новый квест.
	 * @return состояние квеста.
	 */
	public QuestState addNewQuest(final Quest quest) {

		final PlayerShip owner = getOwner();

		QuestState state = statePool.take();

		if(state == null) {
			state = new QuestState(objectIdFactory.incrementAndGet());
		}

		state.setQuest(quest);
		state.setOwner(owner);
		state.setFavorite(true);
		state.setStatus(QuestStatus.IN_PROGRESS);

		QUEST_DB_MANAGER.addNewQuest(owner, state);

		final Array<QuestState> active = getActive();
		active.add(state);

		return state;
	}

	@Override
	public void asynLock() {
		lock.asynLock();
	}

	@Override
	public void asynUnlock() {
		lock.asynUnlock();
	}

	/**
	 * Очика списка заданий.
	 */
	public void clear() {

		final Array<QuestState> active = getActive();

		if(!active.isEmpty()) {
			active.forEach(QUEST_STATE_FOLD);
			active.clear();
		}

		final Table<IntKey, QuestDate> completed = getCompleted();

		if(!completed.isEmpty()) {
			completed.forEach(COMPLATE_DATE_FOLD);
			completed.clear();
		}
	}

	@Override
	public void finalyze() {

		completed.forEach(COMPLATE_DATE_FOLD);
		completed.clear();

		active.forEach(QUEST_STATE_FOLD);
		active.clear();
	}

	/**
	 * Завершение задания.
	 * 
	 * @param state завершенное задание.
	 */
	public void finishQuest(final QuestState state) {

		final Quest quest = state.getQuest();

		final Array<QuestState> active = getActive();
		active.writeLock();
		try {

			final PlayerShip owner = getOwner();

			if(owner == null || owner != state.getOwner()) {
				return;
			}

			final QuestDate date = addFinishQuest(quest, state.getStatus(), System.currentTimeMillis());

			active.fastRemove(state);
			statePool.put(state);

			QUEST_DB_MANAGER.finishQuest(owner, date);

		} finally {
			active.writeUnlock();
		}
	}

	/**
	 * @return активные квесты.
	 */
	public Array<QuestState> getActive() {
		return active;
	}

	/**
	 * @return таблица выполненных квестов.
	 */
	public Table<IntKey, QuestDate> getCompleted() {
		return completed;
	}

	/**
	 * @return пул штампов.
	 */
	private FoldablePool<QuestDate> getDatePool() {
		return datePool;
	}

	/**
	 * @return владелец списка.
	 */
	public PlayerShip getOwner() {
		return owner;
	}

	/**
	 * Получение контейнера состояния выполненности квеста с указанным ид.
	 * 
	 * @return контейнер состояние квеста.
	 */
	public QuestState getQuestState(final int questId) {

		final Array<QuestState> active = getActive();

		for(final QuestState questState : active.array()) {

			if(questState == null) {
				break;
			}

			if(questState.getQuestId() == questId) {
				return questState;
			}
		}

		return null;
	}

	/**
	 * @return состояние квеста.
	 */
	public QuestState getQuestState(final Quest quest) {
		return getQuestState(quest.getId());
	}

	/**
	 * @return пул состояний квестов.
	 */
	private FoldablePool<QuestState> getStatePool() {
		return statePool;
	}

	/**
	 * @return есть ли активные квесты.
	 */
	public boolean hasActiveQuest() {
		return !active.isEmpty();
	}

	/**
	 * Проверка выполненности квеста.
	 * 
	 * @param quest проверяемый квест.
	 * @return выполнен ли квест.
	 */
	public boolean isComplete(final Quest quest) {
		return isCompleted(quest.getId());
	}

	/**
	 * Проверка выполненности квеста с указанным ид.
	 * 
	 * @param questId проверяемый ид квеста.
	 * @return выполнен ли квест.
	 */
	public boolean isCompleted(final int questId) {
		return completed.containsKey(questId);
	}

	@Override
	public void release() {
		POOL.put(this);
	}

	/**
	 * Сохранение переменных заданий.
	 */
	public void saveVars() {
		final Array<QuestState> active = getActive();
		active.forEach(QUEST_STATE_SAVE_VARS);
	}

	/**
	 * @param owner владелец списка.
	 */
	public void setOwner(final PlayerShip owner) {
		this.owner = owner;
	}

	@Override
	public void synLock() {
		lock.synLock();
	}

	@Override
	public void synUnlock() {
		lock.synUnlock();
	}
}

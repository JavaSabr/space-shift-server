package com.ss.server.model.quest;

import rlib.util.Reloadable;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.QuestReward;

/**
 * Интерфейс для реализации задания.
 * 
 * @author Ronn
 */
public interface Quest extends Reloadable<Quest> {

	/**
	 * @return тип задания.
	 */
	public QuestType getQuestType();

	/**
	 * Обаботка взятия задания.
	 * 
	 * @param event событие, приведшее к взятию задания.
	 * @param local контейнер локальных объектов.
	 */
	public void accept(QuestEvent event, LocalObjects local);

	/**
	 * Добавить доступные действия задания.
	 * 
	 * @param container контейнер действий.
	 * @param event событие задания.
	 */
	public void addButtons(Array<QuestButton> container, QuestEvent event);

	/**
	 * Получение набора наград для отображении в журнале заданий.
	 * 
	 * @param контейнер наград.
	 * @param event событие задания.
	 * @param local контейнер локальных объектов.
	 * @return набор наград.
	 */
	public Array<QuestReward> addRewardsTo(Array<QuestReward> container, QuestEvent event, LocalObjects local);

	/**
	 * Получение набора кнопок для отображении в журнале заданий.
	 * 
	 * @param контейнер кнопок.
	 * @param event квестовое событие.
	 * @param state стадия квеста.
	 * @return набор кнопок для текущей стадии.
	 */
	public Array<QuestButton> addStateButtonsTo(Array<QuestButton> container, QuestEvent event, int state);

	/**
	 * Получение набора счетчиков для отображении в журнале заданий.
	 * 
	 * @param контейнер счетчиков.
	 * @param event событие задания.
	 * @param state стадия задания.
	 * @return набор счетчиков для текущей стадии.
	 */
	public Array<QuestCounter> addStateCountersTo(Array<QuestCounter> container, QuestEvent event, int state);

	/**
	 * Обработка отмены задания.
	 * 
	 * @param event событие, приведшее к отмене задания.
	 * @param local контейнер локальных объектов.
	 */
	public void cancel(QuestEvent event, LocalObjects local);

	/**
	 * Обработка завершения задания.
	 * 
	 * @param event событие, приведшее к финишу задания.
	 * @param local контейнер локальных объектов.
	 */
	public void finish(QuestEvent event, LocalObjects local);

	/**
	 * Получение действия из стадии.
	 * 
	 * @param event событие задания.
	 * @param id ид действия.
	 * @return искомое действие.
	 */
	public QuestButton getButton(QuestEvent event, int id);

	/**
	 * Получение описания стадии задания.
	 * 
	 * @param state интересуемая нас стадия.
	 * @return описание этой стадии.
	 */
	public String getDescription(int state);

	/**
	 * Получение описания задания.
	 * 
	 * @return описание задания.
	 */
	public String getDescription();

	/**
	 * @return ид задания.
	 */
	public int getId();

	/**
	 * @return название задания.
	 */
	public String getName();

	/**
	 * @return можно ли отменить задание.
	 */
	public boolean isCanceable();

	/**
	 * Есть ли что-то для игрока в задании.
	 * 
	 * @param event событие задания.
	 * @return интересено ли задание.
	 */
	public boolean isInteresting(QuestEvent event);

	/**
	 * Уведомление задания о событии.
	 * 
	 * @param event событие задания.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyEvent(QuestEvent event, LocalObjects local);
}

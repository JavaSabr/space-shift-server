package com.ss.server.model.quest;

/**
 * Реализация счетчика задания.
 * 
 * @author Ronn
 */
public class QuestCounter {

	/** задание, к которому принадлежит счетчик */
	private final Quest quest;

	/** описание счетчика */
	private final String description;
	/** название переменной, в которой хранится значение счетчика */
	private final String var;

	/** ид счетчика */
	private final int id;
	/** необходимое кол-во */
	private final int limit;

	public QuestCounter(final Quest quest, final String description, final String var, final int id, final int limit) {
		this.quest = quest;
		this.description = description;
		this.var = var;
		this.id = id;
		this.limit = limit;
	}

	/**
	 * @return описание счетчика.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return ид счетчика.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return необходимое кол-во.
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @return задание, к которому принадлежит счетчик.
	 */
	public Quest getQuest() {
		return quest;
	}

	/**
	 * Получение текущего значения счетчика для указанного состояния задания.
	 * 
	 * @param state состояние задания.
	 * @return значение текущее счетчика.
	 */
	public int getValue(final QuestState state) {
		return state.getVar(getVar());
	}

	/**
	 * @return название переменной, в которой хранится значение счетчика.
	 */
	public String getVar() {
		return var;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [description=" + description + ", var=" + var + ", id=" + id + ", limit=" + limit + "]";
	}
}

package com.ss.server.model.quest;

import rlib.util.pools.Foldable;

/**
 * Контейнер штампа даты выполнения задания.
 * 
 * @author Ronn
 */
public class QuestDate implements Foldable {

	/** выполненное задание */
	private Quest quest;

	/** дата выполнения */
	private long date;

	/** статус завершения задания */
	private QuestStatus status;

	@Override
	public void finalyze() {
		quest = null;
	}

	/**
	 * @return date время выполнения квеста.
	 */
	public final long getDate() {
		return date;
	}

	/**
	 * @return quest выполненый квест.
	 */
	public final Quest getQuest() {
		return quest;
	}

	/**
	 * @return статус завершения задания.
	 */
	public QuestStatus getStatus() {
		return status;
	}

	@Override
	public void reinit() {
	}

	/**
	 * @param date время выполнения квеста.
	 */
	public final void setDate(final long date) {
		this.date = date;
	}

	/**
	 * @param quest выполненый квест.
	 */
	public final void setQuest(final Quest quest) {
		this.quest = quest;
	}

	/**
	 * @param status статус завершения задания.
	 */
	public void setStatus(final QuestStatus status) {
		this.status = status;
	}
}

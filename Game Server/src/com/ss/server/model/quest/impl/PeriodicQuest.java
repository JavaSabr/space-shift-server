package com.ss.server.model.quest.impl;

import org.w3c.dom.Node;

import com.ss.server.model.quest.QuestType;

/**
 * Реализация периодичесеого задания.
 * 
 * @author Ronn
 */
public class PeriodicQuest extends AbstractQuest {

	public PeriodicQuest(Node node) {
		super(node);
	}

	@Override
	public QuestType getQuestType() {
		return QuestType.PERIODIC;
	}
}

package com.ss.server.model.quest.impl;

import org.w3c.dom.Node;

import com.ss.server.model.quest.QuestType;

/**
 * Модель сюжетного квеста.
 * 
 * @author Ronn
 */
public class StoryQuest extends AbstractQuest {

	public StoryQuest(final Node node) {
		super(node);
		setCanceable(false);
	}

	@Override
	public QuestType getQuestType() {
		return QuestType.STORY;
	}
}

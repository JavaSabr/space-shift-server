package com.ss.server.model.quest.impl;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ArrayUtils;
import rlib.util.ObjectUtils;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.LocalObjects;
import com.ss.server.document.DocumentQuestCondition;
import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.QuestCounter;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.QuestStatus;
import com.ss.server.model.quest.action.QuestAction;
import com.ss.server.model.quest.action.QuestActionType;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventListener;
import com.ss.server.model.quest.event.QuestEventType;
import com.ss.server.model.quest.reward.QuestReward;
import com.ss.server.model.quest.reward.QuestRewardType;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.packet.server.ResponseQuestStateInfo;
import com.ss.server.table.StationTable;
import com.ss.server.template.StationTemplate;

/**
 * Базовая модель реализации задания.
 * 
 * @author Ronn
 */
public abstract class AbstractQuest implements Quest {

	public static final String PROP_VAR = "var";

	public static final QuestCounter[] EMPTY_COUNTERS = new QuestCounter[0];

	private static final Logger LOGGER = LoggerManager.getLogger(Quest.class);

	public static final String NODE_DESCRIPTIONS = "descriptions";
	public static final String NODE_EVENTS = "events";
	public static final String NODE_BUTTONS = "buttons";
	public static final String NODE_STATIONS = "stations";
	public static final String NODE_STATION = "station";
	public static final String NODE_COUNTER = "counter";
	public static final String NODE_STATE = "state";
	public static final String NODE_EVENT = "event";
	public static final String NODE_COUNTERS = "counters";
	public static final String NODE_ACTION = "action";
	public static final String NODE_REWARDS = "rewards";
	public static final String NODE_REWARD = "reward";

	public static final String PROP_CANCEABLE = "canceable";
	public static final String PROP_ID = "id";
	public static final String PROP_NAME = "name";
	public static final String PROP_VAL = "val";
	public static final String PROP_DESCRIPTION = "description";
	public static final String PROP_LIMIT = "limit";
	public static final String PROP_VALUE = "value";

	private static final DocumentQuestCondition QUEST_CONDITION = DocumentQuestCondition.getInstance();
	private static final ObjectEventManager EVENT_MANAGER = ObjectEventManager.getInstance();

	private static final QuestButton[] EMPTY_BUTTONS = new QuestButton[0];

	private static final String BUTTON = "button";

	/** таблица кнопок по ид */
	private final Table<IntKey, QuestButton> buttonTable;
	/** таблица счетчиков по ид */
	private final Table<IntKey, QuestCounter> counterTable;

	/** список действий с заданием в журнале */
	protected QuestButton[][] stateButtons;
	/** счетчики по стадиям задания */
	protected QuestCounter[][] stateCounters;

	/** набор описаний стадий задания */
	protected String[] descriptions;

	/** список слушателей событий */
	protected QuestEventListener[] listeners;

	/** список действий с заданием в диалоговом окне */
	protected QuestButton[] buttons;
	/** награда за задание */
	protected QuestReward[] rewards;

	/** название задания */
	protected String name;
	/** описание задания */
	protected String description;

	/** ид задания */
	protected int id;

	/** можно ли отменять задание */
	protected boolean canceable;

	public AbstractQuest(final Node node) {
		this.buttonTable = TableFactory.newIntegerTable();
		this.counterTable = TableFactory.newIntegerTable();

		try {

			final VarTable vars = VarTable.newInstance(node);

			this.name = vars.getString(PROP_NAME, "@quest-name:unknown@");
			this.description = vars.getString(PROP_DESCRIPTION, "@quest-description:unknown@");
			this.id = vars.getInteger(PROP_ID);
			this.canceable = vars.getBoolean(PROP_CANCEABLE, true);

			for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

				if(child.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				if(NODE_STATIONS.equals(child.getNodeName())) {
					parseStations(child);
				} else if(NODE_BUTTONS.equals(child.getNodeName())) {
					parseButtons(child);
				} else if(NODE_COUNTERS.equals(child.getNodeName())) {
					parseCounters(child);
				} else if(NODE_EVENTS.equals(child.getNodeName())) {
					parseEvents(child);
				} else if(NODE_DESCRIPTIONS.equals(child.getNodeName())) {
					parseDescriptions(child);
				} else if(NODE_REWARDS.equals(child.getNodeName())) {
					parseRewards(child);
				}
			}

		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	@Override
	public void accept(final QuestEvent event, final LocalObjects local) {

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			return;
		}

		QuestState state = null;

		final QuestList questList = playerShip.getQuestList();
		questList.synLock();
		try {

			if(questList.isComplete(this) || questList.getQuestState(this) != null) {
				return;
			}

			state = questList.addNewQuest(this);

		} finally {
			questList.synUnlock();
		}

		EVENT_MANAGER.notifyQuestAccepted(playerShip, this, local);

		if(playerShip.getClient() != null) {
			playerShip.sendPacket(ResponseQuestStateInfo.getInstance(state, playerShip, local), true);
		}
	}

	@Override
	public void addButtons(final Array<QuestButton> container, final QuestEvent event) {
		for(final QuestButton button : getButtons()) {
			if(button.test(event)) {
				container.add(button);
			}
		}
	}

	@Override
	public Array<QuestReward> addRewardsTo(final Array<QuestReward> container, final QuestEvent event, final LocalObjects local) {

		final QuestReward[] rewards = getRewards();

		if(rewards == null || rewards.length < 1) {
			return container;
		}

		for(final QuestReward reward : rewards) {
			if(reward.test(event, local)) {
				container.add(reward);
			}
		}

		return container;
	}

	@Override
	public Array<QuestButton> addStateButtonsTo(final Array<QuestButton> container, final QuestEvent event, final int state) {

		final QuestButton[] buttons = getStateButtons(state);

		if(buttons.length < 1) {
			return container;
		}

		for(final QuestButton button : buttons) {
			if(button.test(event)) {
				container.add(button);
			}
		}

		return container;
	}

	@Override
	public Array<QuestCounter> addStateCountersTo(final Array<QuestCounter> container, final QuestEvent event, final int state) {

		final QuestCounter[] counters = getStateCounters(state);

		if(counters.length < 1) {
			return container;
		}

		for(final QuestCounter button : counters) {
			container.add(button);
		}

		return container;
	}

	@Override
	public void cancel(final QuestEvent event, final LocalObjects local) {
		// TODO Auto-generated method stub
	}

	@Override
	public void finish(final QuestEvent event, final LocalObjects local) {

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			return;
		}

		final QuestList questList = playerShip.getQuestList();
		final QuestState questState = questList.getQuestState(this);
		questState.setState(0);

		if(questState.getStatus() == QuestStatus.IN_PROGRESS) {
			questState.setStatus(QuestStatus.COMPLETED);
		}

		EVENT_MANAGER.notifyQuestFinished(playerShip, this, local);

		if(playerShip.getClient() != null) {
			playerShip.sendPacket(ResponseQuestStateInfo.getInstance(questState, playerShip, local), true);
		}

		if(questState.getStatus() == QuestStatus.COMPLETED) {
			giveRewards(event, local);
		}

		questList.finishQuest(questState);
	}

	@Override
	public QuestButton getButton(final QuestEvent event, final int id) {
		final QuestButton button = buttonTable.get(id);
		return button != null && button.test(event) ? button : null;
	}

	/**
	 * @return список действий с заданием в диалоговом окне.
	 */
	protected final QuestButton[] getButtons() {
		return buttons;
	}

	@Override
	public String getDescription(final int state) {

		final String[] descriptions = getDescriptions();

		if(descriptions != null && state < descriptions.length) {
			final String result = descriptions[state];
			return StringUtils.isEmpty(result) ? StringUtils.EMPTY : result;
		}

		return "<no description for " + state + ">";
	}

	/**
	 * @return набор описаний стадий задания.
	 */
	private String[] getDescriptions() {
		return descriptions;
	}

	@Override
	public final int getId() {
		return id;
	}

	/**
	 * @return список слушателей квестовых событий.
	 */
	public QuestEventListener[] getListeners() {
		return listeners;
	}

	@Override
	public final String getName() {
		return name;
	}

	/**
	 * @return награда за задание.
	 */
	private QuestReward[] getRewards() {
		return rewards;
	}

	/**
	 * @param state стадия задания.
	 * @return список возможных действий.
	 */
	private QuestButton[] getStateButtons(final int state) {

		if(stateButtons == null || state >= stateButtons.length) {
			return EMPTY_BUTTONS;
		}

		final QuestButton[] buttons = stateButtons[state];
		return buttons == null ? EMPTY_BUTTONS : buttons;
	}

	/**
	 * @param state стадия задания.
	 * @return список счетчиков.
	 */
	private QuestCounter[] getStateCounters(final int state) {

		if(stateCounters == null || state >= stateCounters.length) {
			return EMPTY_COUNTERS;
		}

		final QuestCounter[] counters = stateCounters[state];
		return counters == null ? EMPTY_COUNTERS : counters;
	}

	/**
	 * Выдача награды игроку за задание.
	 * 
	 * @param event событие задания.
	 * @param local контейнер локальных объектов.
	 */
	private void giveRewards(final QuestEvent event, final LocalObjects local) {

		final QuestReward[] rewards = getRewards();

		if(rewards != null && rewards.length > 0) {
			for(final QuestReward reward : rewards) {
				if(reward.test(event, local)) {
					reward.apply(event, local);
				}
			}
		}
	}

	@Override
	public final boolean isCanceable() {
		return canceable;
	}

	@Override
	public boolean isInteresting(final QuestEvent event) {

		final QuestButton[] buttons = getButtons();

		if(buttons == null || buttons.length < 1) {
			return false;
		}

		for(final QuestButton button : buttons) {
			if(button.test(event)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void notifyEvent(final QuestEvent event, final LocalObjects local) {

		final QuestEventListener[] listeners = getListeners();

		if(listeners == null || listeners.length < 1) {
			return;
		}

		for(final QuestEventListener listener : listeners) {
			listener.notifyEvent(event, local);
		}
	}

	/**
	 * Парс действий заадния в ответ на события.
	 */
	private QuestAction[] parseActions(final Node node) {

		final VarTable vars = VarTable.newInstance();

		final Array<QuestAction> actions = ArrayFactory.newArray(QuestAction.class);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_ACTION.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final String actionTypeName = vars.getString(PROP_NAME);
			final QuestActionType actionType = QuestActionType.getActionType(actionTypeName);
			final Condition condition = QUEST_CONDITION.parseCondition(child, this);

			actions.add(actionType.newInstance(this, condition, vars));
		}

		return actions.toArray(new QuestAction[actions.size()]);
	}

	/**
	 * Парсим кнопки.
	 */
	private void parseButtons(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(BUTTON.equals(child.getNodeName())) {

				vars.parse(child);

				final String name = vars.getString(PROP_NAME);

				final int id = vars.getInteger(PROP_ID);

				final Condition cond = QUEST_CONDITION.parseCondition(child, this);

				final QuestButton button = new QuestButton(this, name, StringUtils.EMPTY, cond, id);

				if(buttonTable.put(id, button) != null) {
					LOGGER.warning("found duplicate button for id " + id + " in quest " + getName());
				}

				buttons = ArrayUtils.addToArray(buttons, button, QuestButton.class);

			} else if(NODE_STATE.equals(child.getNodeName())) {
				parseStateButtons(child);
			}
		}
	}

	/**
	 * Парсим счетчики.
	 */
	private void parseCounters(final Node node) {

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_STATE.equals(child.getNodeName())) {
				parseStateCounters(child);
			}
		}
	}

	/**
	 * Парс описаний стадий.
	 */
	private void parseDescriptions(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !"description".equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final int state = vars.getInteger(NODE_STATE);

			final String value = vars.getString(PROP_VALUE);

			if(descriptions == null) {
				descriptions = new String[state + 1];
			} else if(descriptions.length <= state) {
				descriptions = ArrayUtils.copyOf(descriptions, state - descriptions.length + 1);
			}

			descriptions[state] = value;
		}
	}

	/**
	 * Парс слушателей событий задания.
	 */
	private void parseEvents(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_EVENT.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final String eventTypeName = vars.getString(PROP_NAME);
			final QuestEventType eventType = QuestEventType.getEventType(eventTypeName);

			vars.set(QuestEventListener.ACTIONS, parseActions(child));

			listeners = ArrayUtils.addToArray(listeners, eventType.newInstance(this, vars, child), QuestEventListener.class);
		}
	}

	/**
	 * Парсим награду за задание.
	 */
	private void parseRewards(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_REWARD.equals(child.getNodeName())) {

				vars.parse(child);

				final String rewardTypeName = vars.getString(PROP_NAME);
				final QuestRewardType rewardType = QuestRewardType.getRewardType(rewardTypeName);
				final Condition cond = QUEST_CONDITION.parseCondition(child, this);

				final QuestReward reward = rewardType.newInstance(this, cond, vars);

				rewards = ArrayUtils.addToArray(rewards, reward, QuestReward.class);
			}
		}
	}

	/**
	 * Парсим кнопки.
	 */
	private void parseStateButtons(final Node node) {

		final VarTable vars = VarTable.newInstance(node);

		final int state = vars.getInteger(PROP_VAL);

		if(stateButtons == null) {
			stateButtons = new QuestButton[state + 1][];
		} else if(stateButtons.length <= state) {
			stateButtons = ArrayUtils.copyOf(stateButtons, state - stateButtons.length + 1);
		}

		QuestButton[] buttons = stateButtons[state];

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(BUTTON.equals(child.getNodeName())) {

				vars.parse(child);

				final int id = vars.getInteger(PROP_ID);

				final String name = vars.getString(PROP_NAME);
				final Condition cond = QUEST_CONDITION.parseCondition(child, this);

				final QuestButton button = new QuestButton(this, name, StringUtils.EMPTY, cond, id);

				if(buttonTable.put(id, button) != null) {
					LOGGER.warning("found duplicate button for id " + id + " in quest " + getName());
				}

				buttons = ArrayUtils.addToArray(buttons, button, QuestButton.class);
			}

			stateButtons[state] = buttons;
		}
	}

	/**
	 * Парсим счетчики.
	 */
	private void parseStateCounters(final Node node) {

		final VarTable vars = VarTable.newInstance(node);

		final int state = vars.getInteger(PROP_VAL);

		if(stateCounters == null) {
			stateCounters = new QuestCounter[state + 1][];
		} else if(stateCounters.length <= state) {
			stateCounters = ArrayUtils.copyOf(stateCounters, state - stateCounters.length + 1);
		}

		QuestCounter[] counters = stateCounters[state];

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_COUNTER.equals(child.getNodeName())) {

				vars.parse(child);

				final int id = vars.getInteger(PROP_ID);
				final int limit = vars.getInteger(PROP_LIMIT);

				final String descr = vars.getString(PROP_DESCRIPTION);
				final String var = vars.getString(PROP_VAR);

				final QuestCounter counter = new QuestCounter(this, descr, var, id, limit);

				if(counterTable.put(id, counter) != null) {
					LOGGER.warning("found duplicate counter for id " + id + " in quest " + getName());
				}

				counters = ArrayUtils.addToArray(counters, counter, QuestCounter.class);
			}

			stateCounters[state] = counters;
		}
	}

	/**
	 * Вносим квесты в списке станций.
	 */
	private void parseStations(final Node node) {

		final VarTable vars = VarTable.newInstance();
		final StationTable stationTable = StationTable.getInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_STATION.equals(child.getNodeName())) {

				vars.parse(child);

				final StationTemplate template = stationTable.getTemplate(vars.getInteger(PROP_ID));
				template.setQuests(ArrayUtils.addToArray(template.getQuests(), this, Quest.class));
			}
		}
	}

	@Override
	public final void reload(final Quest update) {
		ObjectUtils.reload(this, (AbstractQuest) update);
	}

	/**
	 * @param canceable отменяемый ли квест.
	 */
	protected final void setCanceable(final boolean canceable) {
		this.canceable = canceable;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + name + ", description=" + description + ", id=" + id + ", canceable=" + canceable + "]";
	}
}

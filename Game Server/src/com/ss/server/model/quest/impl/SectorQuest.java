package com.ss.server.model.quest.impl;

import org.w3c.dom.Node;

import com.ss.server.model.quest.QuestType;

/**
 * Модель секторального задания.
 * 
 * @author Ronn
 */
public class SectorQuest extends AbstractQuest {

	public SectorQuest(final Node node) {
		super(node);
	}

	@Override
	public QuestType getQuestType() {
		return QuestType.SECTOR;
	}
}

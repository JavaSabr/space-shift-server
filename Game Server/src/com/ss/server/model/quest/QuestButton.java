package com.ss.server.model.quest;

import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Реализация действия над заданием.
 * 
 * @author Ronn
 */
public final class QuestButton {

	/** задание, к которому принадлежит кнопка */
	private final Quest quest;

	/** название кнопки */
	private final String name;
	/** описание к кнопке */
	private final String description;

	/** условие кнопки */
	private final Condition condition;

	/** ид кнопки */
	private final int id;

	public QuestButton(final Quest quest, final String name, final String description, final Condition condition, final int id) {
		this.quest = quest;
		this.name = name;
		this.description = description;
		this.condition = condition;
		this.id = id;
	}

	/**
	 * @return описание к кнопки.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return ид кнопки.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return название кнопки.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return задание, к которому принадлежит кнопка.
	 */
	public Quest getQuest() {
		return quest;
	}

	/**
	 * @return выполнены ли условия.
	 */
	public boolean test(final QuestEvent event) {
		return condition == null || condition.test(event);
	}

	@Override
	public String toString() {
		return "QuestButton [name=" + name + ", descr=" + description + ", cond=" + condition + ", id=" + id + "]";
	}
}

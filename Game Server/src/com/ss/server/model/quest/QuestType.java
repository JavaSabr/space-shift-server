package com.ss.server.model.quest;

/**
 * Перечисление видов заданий.
 * 
 * @author Ronn
 */
public enum QuestType {
	STORY,
	SECTOR,
	PERIODIC, ;

	public static final QuestType[] VALUES = values();

	public static final int LENGHT = VALUES.length;
}

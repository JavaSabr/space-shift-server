package com.ss.server.model.quest.action.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.action.QuestAction;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Базовая модель квестового действия.
 * 
 * @author Ronn
 */
public abstract class AbstractQuestAction implements QuestAction {

	protected static final Logger LOGGER = LoggerManager.getLogger(QuestAction.class);

	/** квест действия */
	private final Quest quest;

	/** условие выполнения */
	private final Condition condition;

	public AbstractQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		this.quest = quest;
		this.condition = condition;
	}

	/**
	 * Применить действие в ответ на событие.
	 * 
	 * @param event квестовое событие.
	 * @param local контейнер локальных объектов.
	 */
	protected abstract void apply(QuestEvent event, LocalObjects local);

	@Override
	public Condition getCondition() {
		return condition;
	}

	@Override
	public Quest getQuest() {
		return quest;
	}

	@Override
	public void notifyEvent(final QuestEvent event, final LocalObjects local) {

		final Condition condition = getCondition();

		if(condition == null || condition.test(event)) {
			apply(event, local);
		}
	}
}

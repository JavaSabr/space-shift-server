package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.database.QuestDBManager;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.packet.server.ResponseQuestStateInfo;

/**
 * Действие по смене состояния задания и отправке инфы о текущем состоянии.
 * 
 * @author Ronn
 */
public class ChangeStateAndUpdateInfoQuestAction extends ChangeStateQuestAction {

	private static final QuestDBManager QUEST_DB_MANAGER = QuestDBManager.getInstance();

	public ChangeStateAndUpdateInfoQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {
		super.apply(event, local);

		final PlayerShip ship = event.getPlayerShip();

		if(ship == null) {
			LOGGER.warning(new RuntimeException("not found player ship for " + event));
			return;
		}

		final QuestList questList = ship.getQuestList();
		final QuestState state = questList.getQuestState(getQuest());

		if(state == null) {
			LOGGER.warning(new RuntimeException("not found quest state for " + getQuest()));
			return;
		}

		state.setState(getState());

		QUEST_DB_MANAGER.updateQuest(ship, state);

		if(ship.getClient() != null) {
			ship.sendPacket(ResponseQuestStateInfo.getInstance(state, ship, local), true);
		}
	}
}

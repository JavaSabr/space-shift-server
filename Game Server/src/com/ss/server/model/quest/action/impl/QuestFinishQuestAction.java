package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventType;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Действие по завершению квеста игроком.
 * 
 * @author Ronn
 */
public class QuestFinishQuestAction extends AbstractQuestAction {

	public QuestFinishQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);
	}

	@Override
	protected void apply(final QuestEvent currentEvent, final LocalObjects currentLocal) {

		final QuestEventType eventType = currentEvent.getEventType();
		final PlayerShip playerShip = currentEvent.getPlayerShip();
		final Quest quest = getQuest();

		final ExecutorManager executorManager = ExecutorManager.getInstance();
		executorManager.execute(() -> {

			final LocalObjects local = LocalObjects.get();

			final QuestEvent event = local.getNextQuestEvent();
			event.setPlayerShip(playerShip);
			event.setEventType(eventType);

			quest.finish(event, local);
		});
	}
}

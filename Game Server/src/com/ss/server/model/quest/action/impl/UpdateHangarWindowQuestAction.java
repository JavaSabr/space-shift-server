package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Действие по обновлению окна ангара с заданиями.
 * 
 * @author Ronn
 */
public class UpdateHangarWindowQuestAction extends AbstractQuestAction {

	public UpdateHangarWindowQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(new RuntimeException("not found player ship for " + event));
			return;
		}

		if(playerShip.getClient() == null) {
			return;
		}
	}
}

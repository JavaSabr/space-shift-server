package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.util.NumOperation;

/**
 * Действие по обновлению квестовой переменной.
 * 
 * @author Ronn
 */
public class UpdateVarQuestAction extends AbstractQuestAction {

	public static final String PROP_OPERATION = "operation";
	public static final String PROP_VALUE = "value";
	public static final String PROP_VAR = "var";

	/** название переменной */
	private final String var;

	/** тип операции */
	private final NumOperation operation;

	/** оперируемое значение */
	private final int value;

	public UpdateVarQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);

		this.var = vars.getString(PROP_VAR);

		switch(vars.getString(PROP_OPERATION)) {
			case "=": {
				operation = NumOperation.SET;
				break;
			}
			case "*": {
				operation = NumOperation.MUL;
				break;
			}
			case "/": {
				operation = NumOperation.DIV;
				break;
			}
			case "+": {
				operation = NumOperation.ADD;
				break;
			}
			case "-": {
				operation = NumOperation.SUB;
				break;
			}
			default: {
				operation = null;
			}
		}

		this.value = vars.getInteger(PROP_VALUE);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {

		final PlayerShip ship = event.getPlayerShip();

		if(ship == null) {
			LOGGER.warning(this, new RuntimeException("not found player ship for " + event));
			return;
		}

		final QuestList questList = ship.getQuestList();
		final QuestState state = questList.getQuestState(getQuest());

		if(state == null) {
			LOGGER.warning(this, new RuntimeException("not found quest state for quest " + getQuest()));
			return;
		}

		final NumOperation operation = getOperation();
		final String var = getVar();

		state.setVar(var, operation.apply(state.getVar(var), getValue()));
	}

	/**
	 * @return тип операции.
	 */
	public NumOperation getOperation() {
		return operation;
	}

	/**
	 * @return оперируемое значение.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @return название переменной.
	 */
	public String getVar() {
		return var;
	}
}

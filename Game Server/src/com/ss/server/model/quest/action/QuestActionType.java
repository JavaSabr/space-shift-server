package com.ss.server.model.quest.action;

import java.lang.reflect.Constructor;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ClassUtils;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;

/**
 * Перечисление типов действий для заданий.
 * 
 * @author Ronn
 */
public class QuestActionType {

	private static final Logger LOGGER = LoggerManager.getLogger(QuestActionType.class);

	private static final Table<String, QuestActionType> TABLE = TableFactory.newObjectTable();

	/**
	 * @param name исходное название типа действия.
	 * @return
	 */
	protected static String formatName(final String name) {
		return name.replace(QuestAction.class.getSimpleName(), StringUtils.EMPTY);
	}

	public static final QuestActionType getActionType(final String name) {
		return TABLE.get(name);
	}

	/**
	 * Регистрация нового типа действия от имени задания.
	 * 
	 * @param actionType тип действия.
	 */
	public static final void register(final QuestActionType actionType) {

		if(TABLE.containsKey(actionType.getName())) {
			LOGGER.warning(new RuntimeException("found duplicate action type for name " + actionType.getName()));
		}

		TABLE.put(actionType.getName(), actionType);
	}

	/** название типа действия */
	private final String name;

	/** конструктор действия */
	private final Constructor<? extends QuestAction> constructor;

	public QuestActionType(final String name, final Class<? extends QuestAction> actionClass) {
		this.name = formatName(name);
		this.constructor = ClassUtils.getConstructor(actionClass, Quest.class, Condition.class, VarTable.class);
	}

	/**
	 * @return название типа действия.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Создать новый инстанс действия.
	 * 
	 * @param quest квест, которому принадлежит действие.
	 * @param condition условие выполнения действия.
	 * @param vars таблица атрибутов.
	 * @return новый инстанс действия.
	 */
	public QuestAction newInstance(final Quest quest, final Condition condition, final VarTable vars) {
		return ClassUtils.newInstance(constructor, quest, condition, vars);
	}
}

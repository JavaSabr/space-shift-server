package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Действие по взятию квеста игроком.
 * 
 * @author Ronn
 */
public class QuestStartQuestAction extends AbstractQuestAction {

	public QuestStartQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {
		final Quest quest = getQuest();
		quest.accept(event, local);
	}
}

package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.packet.server.ResponseQuestCounterInfo;

/**
 * Действие по актулизации счетчика у игрока.
 * 
 * @author Ronn
 */
public class UpdateCounterQuestAction extends AbstractQuestAction {

	public static final String PROP_ID = "id";
	public static final String PROP_VAR = "var";

	/** название переменной */
	private final String var;

	/** ид счетчика */
	private final int id;

	public UpdateCounterQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);

		this.var = vars.getString(PROP_VAR);
		this.id = vars.getInteger(PROP_ID);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			LOGGER.warning(new RuntimeException("not found player ship for " + event));
			return;
		}

		if(playerShip.getClient() == null) {
			return;
		}

		final QuestList questList = playerShip.getQuestList();
		final QuestState state = questList.getQuestState(getQuest());

		if(state == null) {
			LOGGER.warning(new RuntimeException("not found quest state for " + getQuest()));
			return;
		}

		playerShip.sendPacket(ResponseQuestCounterInfo.getInstance(state, getId(), state.getVar(getVar())), true);
	}

	/**
	 * @return ид счетчика.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return название переменной.
	 */
	public String getVar() {
		return var;
	}
}

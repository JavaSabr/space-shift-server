package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.packet.server.ResponseOpenSkillPanel;

/**
 * Действие по открытию у игрока панели скилов.
 * 
 * @author Ronn
 */
public class OpenSkillQuestAction extends AbstractQuestAction {

	public OpenSkillQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {

		final PlayerShip ship = event.getPlayerShip();

		if(ship != null) {
			ship.sendPacket(ResponseOpenSkillPanel.getInstance());
		}
	}
}

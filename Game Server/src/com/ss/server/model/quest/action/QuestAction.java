package com.ss.server.model.quest.action;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Интерфейс для реализации действия от задания.
 * 
 * @author Ronn
 */
public interface QuestAction {

	/**
	 * @return условие действия.
	 */
	public Condition getCondition();

	/**
	 * @return задание действития.
	 */
	public Quest getQuest();

	/**
	 * Уведомление действие о событии.
	 * 
	 * @param event событие задания.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyEvent(QuestEvent event, LocalObjects local);
}

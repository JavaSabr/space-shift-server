package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.packet.server.ResponseQuestStateInfo;

/**
 * Действие по отправке инфы о текущем состоянии квеста.
 * 
 * @author Ronn
 */
public class UpdateStateInfoQuestAction extends AbstractQuestAction {

	public UpdateStateInfoQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {

		final PlayerShip ship = event.getPlayerShip();

		if(ship == null) {
			LOGGER.warning(new RuntimeException("not found player ship for " + event));
			return;
		}

		if(ship.getClient() == null) {
			return;
		}

		final QuestList questList = ship.getQuestList();
		final QuestState state = questList.getQuestState(getQuest());

		if(state != null) {
			ship.sendPacket(ResponseQuestStateInfo.getInstance(state, ship, local), true);
		}
	}
}

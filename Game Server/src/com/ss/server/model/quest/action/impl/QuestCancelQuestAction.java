package com.ss.server.model.quest.action.impl;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Действие по отмене квеста игроком.
 * 
 * @author Ronn
 */
public class QuestCancelQuestAction extends AbstractQuestAction {

	public QuestCancelQuestAction(final Quest quest, final Condition condition, final VarTable vars) {
		super(quest, condition, vars);
	}

	@Override
	protected void apply(final QuestEvent event, final LocalObjects local) {
		final Quest quest = getQuest();
		quest.cancel(event, local);
	}
}

package com.ss.server.model.quest;

import java.nio.ByteBuffer;

import rlib.util.array.Array;

import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.QuestReward;
import com.ss.server.model.quest.reward.QuestRewardClass;
import com.ss.server.network.game.ServerPacket;

/**
 * Реализация набора методов использующихся при записи заданий в пакеты.
 * 
 * @author Ronn
 */
public class QuestPacketUtils {

	/**
	 * Запись списка доступных кнопок.
	 */
	public static void writeButtons(final ServerPacket packet, final ByteBuffer buffer, final Array<QuestButton> buttons) {

		packet.writeByte(buffer, buttons.size());

		if(!buttons.isEmpty()) {
			for(final QuestButton button : buttons.array()) {

				if(button == null) {
					break;
				}

				final String name = button.getName();

				packet.writeByte(buffer, button.getId());
				packet.writeByte(buffer, name.length());
				packet.writeString(buffer, name);
			}
		}
	}

	/**
	 * Запись списка доступных счетчиков.
	 */
	public static void writeCounters(final ServerPacket packet, final ByteBuffer buffer, final Array<QuestCounter> counters, final QuestState state) {

		packet.writeByte(buffer, counters.size());

		if(!counters.isEmpty()) {
			for(final QuestCounter counter : counters.array()) {

				if(counter == null) {
					break;
				}

				final String description = counter.getDescription();

				packet.writeByte(buffer, counter.getId());
				packet.writeInt(buffer, counter.getValue(state));
				packet.writeInt(buffer, counter.getLimit());
				packet.writeByte(buffer, description.length());
				packet.writeString(buffer, description);
			}
		}
	}

	/**
	 * Запись тело еще не взятого задания.
	 */
	public static void writeQuest(final ServerPacket packet, final ByteBuffer buffer, final Quest quest) {

		final QuestType questType = quest.getQuestType();
		final QuestStatus status = QuestStatus.NO_ACCEPTED;

		packet.writeInt(buffer, quest.getId());
		packet.writeByte(buffer, questType.ordinal());

		// запись уникального ид задания
		packet.writeInt(buffer, NO_OBJECT_ID);
		packet.writeByte(buffer, status.ordinal());

		// запись флага нахожедния в избранном
		packet.writeByte(buffer, NO_FAVORITE);

		final String name = quest.getName();

		// запись назадвания задания
		packet.writeByte(buffer, name.length());
		packet.writeString(buffer, name);

		String description = quest.getDescription();

		// запись описания задания
		packet.writeByte(buffer, description.length());
		packet.writeString(buffer, description);

		description = quest.getDescription(1);

		// запись описания стадии задания
		packet.writeByte(buffer, description.length());
		packet.writeString(buffer, description);
	}

	/**
	 * Запись тела задания.
	 */
	public static void writeQuest(final ServerPacket packet, final ByteBuffer buffer, final QuestState state, final Quest quest) {

		final QuestType questType = quest.getQuestType();
		final QuestStatus status = state.getStatus();

		packet.writeInt(buffer, quest.getId());
		packet.writeByte(buffer, questType.ordinal());

		// запись уникального ид задания
		packet.writeInt(buffer, state.getObjectId());
		// запись статуса задания
		packet.writeByte(buffer, status.ordinal());
		// запись нахождения задания в избранном
		packet.writeByte(buffer, state.isFavorite() ? FAVORITE : NO_FAVORITE);

		final String name = quest.getName();

		packet.writeByte(buffer, name.length());
		packet.writeString(buffer, name);

		String description = quest.getDescription();

		// запись общего описания задания
		packet.writeByte(buffer, description.length());
		packet.writeString(buffer, description);

		description = quest.getDescription(state.getState());

		// запись описания стадии задания
		packet.writeByte(buffer, description.length());
		packet.writeString(buffer, description);
	}

	/**
	 * Запись списка доступной награды.
	 */
	public static void writeRewards(final ServerPacket packet, final ByteBuffer buffer, final QuestEvent event, final Array<QuestReward> rewards) {

		packet.writeByte(buffer, rewards.size());

		if(!rewards.isEmpty()) {
			for(final QuestReward reward : rewards.array()) {

				if(reward == null) {
					break;
				}

				final QuestRewardClass rewardClass = reward.getRewardClass();
				packet.writeByte(buffer, rewardClass.ordinal());
				rewardClass.writeTo(packet, buffer, reward, event);
			}
		}
	}

	public static final int NO_COUNTERS = 0;
	public static final int NO_OBJECT_ID = 0;
	public static final int NO_FAVORITE = 0;
	public static final int FAVORITE = 1;
}

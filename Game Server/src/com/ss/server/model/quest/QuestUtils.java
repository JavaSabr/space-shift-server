package com.ss.server.model.quest;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Набор утильных методов по квестам.
 * 
 * @author Ronn
 */
public class QuestUtils {

	/**
	 * @return есть ли активные квесты у указанного игрока.
	 */
	public static final boolean hasActiveQuest(final PlayerShip playerShip) {
		final QuestList questList = playerShip.getQuestList();
		questList.asynLock();
		try {
			return questList.hasActiveQuest();
		} finally {
			questList.asynUnlock();
		}
	}

	/**
	 * Уведомление всех квестов о событии над каким-то квестом.
	 * 
	 * @param states список активных квестов игрока.
	 * @param event событие над квестом.
	 * @param local контейнер локальных объектов.
	 */
	public static final void notifyQuestEvent(final Array<QuestState> states, final QuestEvent event, final LocalObjects local) {
		for(final QuestState state : states.array()) {

			if(state == null) {
				break;
			}

			final Quest quest = state.getQuest();
			event.setQuest(quest);
			quest.notifyEvent(event, local);
		}
	}

	/**
	 * Уведомление всех квестов о событии над каким-то квестом.
	 * 
	 * @param playerShip игрок, у которого произошло событие.
	 * @param event событие над квестом.
	 * @param local контейнер локальных объектов.
	 */
	public static final void notifyQuestEvent(final PlayerShip playerShip, final QuestEvent event, final LocalObjects local) {
		notifyQuestEvent(playerShip.getQuestList(), event, local);
	}

	/**
	 * Уведомление всех квестов о событии над каким-то квестом.
	 * 
	 * @param questList список квестов игрока.
	 * @param event событие над квестом.
	 * @param local контейнер локальных объектов.
	 */
	public static final void notifyQuestEvent(final QuestList questList, final QuestEvent event, final LocalObjects local) {
		questList.asynLock();
		try {
			notifyQuestEvent(questList.getActive(), event, local);
		} finally {
			questList.asynUnlock();
		}
	}

	/**
	 * Уведомление активных квестов о квестовом событии.
	 * 
	 * @param states состояния активных квестов.
	 * @param event квестовое событие.
	 * @param local контейнер локальных объектов.
	 */
	public static final void notifyQuests(final Array<QuestState> states, final QuestEvent event, final LocalObjects local) {
		for(final QuestState state : states.array()) {

			if(state == null) {
				break;
			}

			final Quest quest = state.getQuest();
			event.setQuest(quest);
			quest.notifyEvent(event, local);
		}
	}

	/**
	 * Уведомление активных квестов о квестовом событии.
	 * 
	 * @param playerShip корабль игрока..
	 * @param event квестовое событие.
	 * @param local контейнер локальных объектов.
	 */
	public static final void notifyQuests(final PlayerShip playerShip, final QuestEvent event, final LocalObjects local) {
		notifyQuests(playerShip.getQuestList(), event, local);
	}

	/**
	 * Уведомление активных квестов о квестовом событии.
	 * 
	 * @param questList список квестов игрока.
	 * @param event квестовое событие.
	 * @param local контейнер локальных объектов.
	 */
	public static final void notifyQuests(final QuestList questList, final QuestEvent event, final LocalObjects local) {
		questList.asynLock();
		try {
			notifyQuests(questList.getActive(), event, local);
		} finally {
			questList.asynUnlock();
		}
	}

	private QuestUtils() {
		throw new RuntimeException();
	}
}

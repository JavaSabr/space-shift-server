package com.ss.server.model.quest;

/**
 * Перечисление статусов задания.
 * 
 * @author Ronn
 */
public enum QuestStatus {
	IN_PROGRESS,
	FAILED,
	COMPLETED,
	NO_ACCEPTED;

	public static final QuestStatus valueOf(final int index) {
		return VALUES[index];
	}

	private static final QuestStatus[] VALUES = values();
}

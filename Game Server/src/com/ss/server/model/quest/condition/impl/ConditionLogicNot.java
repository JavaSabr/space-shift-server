package com.ss.server.model.quest.condition.impl;

import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.condition.ContainerCondition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Обертка для реализации отрицания условия.
 * 
 * @author Ronn
 */
public final class ConditionLogicNot extends AbstractCondition implements ContainerCondition {

	/** отрицаемое условие */
	private Condition condition;

	/**
	 * @param condition отрицаемое услоаие.
	 */
	public ConditionLogicNot(final Condition condition) {
		this.condition = condition;
	}

	@Override
	public void add(final Condition condition) {
		this.condition = condition;
	}

	@Override
	public boolean test(final QuestEvent event) {
		return !condition.test(event);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + condition + "]";
	}
}

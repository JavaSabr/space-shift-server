package com.ss.server.model.quest.condition.impl;

import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.QuestStatus;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Модель условия на проверку выполненности квеста.
 * 
 * @author Ronn
 */
public class ConditionQuestComplete extends AbstractCondition {

	/** флаг выполненности квеста */
	private final boolean complete;

	public ConditionQuestComplete(final boolean complete) {
		this.complete = complete;
	}

	/**
	 * @return нужен ли нам выполненный квест.
	 */
	public boolean isComplete() {
		return complete;
	}

	@Override
	public boolean test(final QuestEvent event) {

		final Quest quest = event.getQuest();

		if(quest == null) {
			return false;
		}

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			return false;
		}

		final QuestList questList = playerShip.getQuestList();

		if(questList == null) {
			return false == isComplete();
		}

		final QuestState questState = questList.getQuestState(quest);

		if(isComplete()) {
			return questState == null || questState.getStatus() == QuestStatus.COMPLETED || questList.isComplete(quest);
		} else {
			return !questList.isComplete(quest) && (questState == null || questState.getStatus() == QuestStatus.IN_PROGRESS);
		}
	}

	@Override
	public String toString() {
		return "ConditionQuestComplete [complete=" + complete + "]";
	}
}

package com.ss.server.model.quest.condition;

import com.ss.server.model.quest.event.QuestEvent;

/**
 * Интерфейс для реализации условий.
 * 
 * @author Ronn
 */
public interface Condition {

	/**
	 * Проверка на удовлетворения условия.
	 * 
	 * @param event квестовое событие.
	 * @return выполнены ли условия.
	 */
	public boolean test(QuestEvent event);
}

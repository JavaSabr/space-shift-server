package com.ss.server.model.quest.condition.impl;

import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.station.SpaceStation;

/**
 * Модель условия на проверку станции, в которой находится игрок.
 * 
 * @author Ronn
 */
public class ConditionStationId extends AbstractCondition {

	/** проверяемая станция */
	private final int id;

	public ConditionStationId(final int id) {
		this.id = id;
	}

	@Override
	public boolean test(final QuestEvent event) {

		final Quest quest = event.getQuest();

		if(quest == null) {
			return false;
		}

		final SpaceStation station = event.getStation();

		if(station == null) {
			return false;
		}

		return station.getTemplateId() == id;
	}

	@Override
	public String toString() {
		return "ConditionStationId [id=" + id + "]";
	}
}

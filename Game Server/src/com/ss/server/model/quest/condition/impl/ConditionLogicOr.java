package com.ss.server.model.quest.condition.impl;

import java.util.Arrays;

import rlib.util.ArrayUtils;

import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.condition.ContainerCondition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Объеденяющее условие для реализации 'ИЛИ'
 * 
 * @author Ronn
 */
public final class ConditionLogicOr extends AbstractCondition implements ContainerCondition {

	/** объедененныя условия */
	private Condition[] conditions;

	public ConditionLogicOr() {
		conditions = EMPTY;
	}

	@Override
	public void add(final Condition condition) {

		if(condition == null) {
			return;
		}

		conditions = ArrayUtils.addToArray(conditions, condition, Condition.class);
	}

	/**
	 * @return массив кондишенов.
	 */
	private final Condition[] getConditions() {
		return conditions;
	}

	@Override
	public boolean test(final QuestEvent event) {

		final Condition[] conditions = getConditions();

		if(conditions.length < 1) {
			return true;
		}

		for(final Condition condition : conditions) {
			if(condition.test(event)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" + Arrays.toString(conditions);
	}
}

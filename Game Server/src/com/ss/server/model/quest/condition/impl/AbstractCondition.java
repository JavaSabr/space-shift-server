package com.ss.server.model.quest.condition.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.model.quest.condition.Condition;

/**
 * Базовая модель условия.
 * 
 * @author Ronn
 */
public abstract class AbstractCondition implements Condition {

	protected static final Logger LOGGER = LoggerManager.getLogger(Condition.class);

	protected static final Condition[] EMPTY = new Condition[0];
}

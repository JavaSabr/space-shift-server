package com.ss.server.model.quest.condition.impl;

import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.util.NumComparator;

/**
 * Условие по значению перемнной квеста у игрока.
 * 
 * @author Ronn
 */
public class ConditionPlayerVar extends AbstractCondition {

	/** сравниватель значений */
	private final NumComparator comparator;

	/** название переменной */
	private final String name;

	/** значение переменной */
	private final int value;

	public ConditionPlayerVar(final String name, final String compareType, final int value) {

		switch(compareType) {
			case "==": {
				comparator = NumComparator.EQUALITY;
				break;
			}
			case "!=": {
				comparator = NumComparator.NOT_EQUALITY;
				break;
			}
			case ">": {
				comparator = NumComparator.MORE;
				break;
			}
			case "<": {
				comparator = NumComparator.LESS;
				break;
			}
			case ">=": {
				comparator = NumComparator.MORE_THAN_OR_EQUA;
				break;
			}
			case "<=": {
				comparator = NumComparator.LESS_THAN_OR_EQUA;
				break;
			}
			default: {
				comparator = NumComparator.EQUALITY;
			}
		}

		this.name = name;
		this.value = value;
	}

	@Override
	public boolean test(final QuestEvent event) {

		final PlayerShip ship = event.getPlayerShip();

		if(ship == null) {
			return false;
		}

		final QuestList questList = ship.getQuestList();
		final QuestState questState = questList.getQuestState(event.getQuest());

		if(questState == null) {
			return false;
		}

		return comparator.compare(questState.getVar(name), value);
	}

	@Override
	public String toString() {
		return "ConditionPlayerVar comparator = " + comparator + ",  name = " + name + ",  value = " + value;
	}
}

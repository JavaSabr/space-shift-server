package com.ss.server.model.quest.condition;

/**
 * Интерфейс для реализации контейнера условий.
 * 
 * @author Ronn
 */
public interface ContainerCondition extends Condition {

	/**
	 * @param condition дополнительное условие.
	 */
	public void add(Condition condition);
}

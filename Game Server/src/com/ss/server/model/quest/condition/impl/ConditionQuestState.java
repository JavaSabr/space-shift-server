package com.ss.server.model.quest.condition.impl;

import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Модель условия состояния квеста.
 * 
 * @author Ronn
 */
public class ConditionQuestState extends AbstractCondition {

	/** состояние квеста */
	private final int state;

	public ConditionQuestState(final int state) {
		this.state = state;
	}

	/**
	 * @return интересуемая стадия.
	 */
	public int getState() {
		return state;
	}

	@Override
	public boolean test(final QuestEvent event) {

		final Quest quest = event.getQuest();

		if(quest == null) {
			return false;
		}

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			return false;
		}

		final QuestList questList = playerShip.getQuestList();

		if(questList == null) {
			return false;
		}

		final QuestState questState = questList.getQuestState(quest);

		if(questState == null) {
			return getState() == 0;
		}

		return questState.getState() == getState();
	}

	@Override
	public String toString() {
		return "ConditionQuestState [state=" + state + "]";
	}
}

package com.ss.server.model.quest.condition.impl;

import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestList;
import com.ss.server.model.quest.QuestState;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Реализация условия активности задания у игрока.
 * 
 * @author Ronn
 */
public class ConditionQuestActive extends AbstractCondition {

	/** активность задания */
	private final boolean active;

	public ConditionQuestActive(final boolean active) {
		this.active = active;
	}

	/**
	 * @return активность задания.
	 */
	public boolean isActive() {
		return active;
	}

	@Override
	public boolean test(final QuestEvent event) {

		final Quest quest = event.getQuest();

		if(quest == null) {
			return false;
		}

		final PlayerShip playerShip = event.getPlayerShip();

		if(playerShip == null) {
			return false;
		}

		final QuestList questList = playerShip.getQuestList();

		if(questList == null) {
			return false;
		}

		QuestState questState = null;

		questList.asynLock();
		try {
			questState = questList.getQuestState(quest);
		} finally {
			questList.asynUnlock();
		}

		if(questState == null) {
			return !isActive();
		}

		return isActive();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [active=" + active + "]";
	}
}

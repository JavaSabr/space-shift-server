package com.ss.server.model.quest.condition.impl;

import java.util.Arrays;

import rlib.util.ArrayUtils;

import com.ss.server.model.quest.condition.Condition;
import com.ss.server.model.quest.condition.ContainerCondition;
import com.ss.server.model.quest.event.QuestEvent;

/**
 * Объеденяющее условия для реализации 'И'
 * 
 * @author Ronn
 */
public final class ConditionLogicAnd extends AbstractCondition implements ContainerCondition {

	/** объедененные условия */
	private Condition[] conditions;

	public ConditionLogicAnd() {
		conditions = EMPTY;
	}

	@Override
	public void add(final Condition condition) {

		if(condition == null) {
			return;
		}

		conditions = ArrayUtils.addToArray(conditions, condition, Condition.class);
	}

	/**
	 * @return массив условий.
	 */
	private final Condition[] getConditions() {
		return conditions;
	}

	@Override
	public boolean test(final QuestEvent event) {

		final Condition[] conditions = getConditions();

		if(conditions.length < 1) {
			return true;
		}

		for(final Condition condition : conditions) {
			if(!condition.test(event)) {
				return false;
			}
		}

		return true;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" + Arrays.toString(conditions);
	}
}

package com.ss.server.model.quest.event.impl;

import java.util.concurrent.atomic.AtomicReference;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventType;
import com.ss.server.model.station.SpaceStation;

/**
 * Слушатель нажатий кнопок в квестовых диалогах.
 * 
 * @author Ronn
 */
public class ButtonSelectedQuestEventListener extends AbstractQuestEventListener {

	public static final QuestEventType EVENT_TYPE = QuestEventType.getEventType(ButtonSelectedQuestEventListener.class);

	public static final String PROP_BUTTON_ID = "id";

	static {

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.addSelectQuestButtonListener((ship, button, local) -> {

			final AtomicReference<SpaceStation> stationReference = ship.getStationReference();
			final Quest quest = button.getQuest();

			final QuestEvent event = local.getNextQuestEvent();
			event.setEventType(EVENT_TYPE);
			event.setPlayerShip(ship);
			event.setButton(button);
			event.setQuest(quest);
			event.setStation(stationReference.get());

			quest.notifyEvent(event, local);
		});
	}

	/** ид кнопки */
	private final int id;

	public ButtonSelectedQuestEventListener(final Quest quest, final VarTable vars, final Node node) {
		super(quest, vars, node);

		this.id = vars.getInteger(PROP_BUTTON_ID);
	}

	@Override
	protected boolean condition(final QuestEvent event) {

		if(event.getQuest() != getQuest()) {
			return false;
		}

		final QuestButton button = event.getButton();
		return button != null && button.getId() == getId();
	}

	@Override
	public QuestEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return ид прослушиваемой кнопки.
	 */
	public int getId() {
		return id;
	}
}

package com.ss.server.model.quest.event.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.action.QuestAction;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventListener;

/**
 * Базовая модель слушателя событий задания.
 * 
 * @author Ronn
 */
public abstract class AbstractQuestEventListener implements QuestEventListener {

	/** список ответных действий на событие */
	private final QuestAction[] actions;

	/** квест, за которым закреплен слушатель */
	private final Quest quest;

	public AbstractQuestEventListener(final Quest quest, final VarTable vars, final Node node) {
		this.quest = quest;
		this.actions = vars.get(ACTIONS, QuestAction[].class);
	}

	/**
	 * Проверка выполнение условия события.
	 * 
	 * @param event произошедшее событие.
	 * @return подходит ли это событие к слушателю.
	 */
	protected abstract boolean condition(QuestEvent event);

	@Override
	public QuestAction[] getActions() {
		return actions;
	}

	@Override
	public Quest getQuest() {
		return quest;
	}

	@Override
	public void notifyEvent(final QuestEvent event, final LocalObjects local) {

		if(event.getEventType() != getEventType() || !condition(event)) {
			return;
		}

		final QuestAction[] actions = getActions();

		if(actions.length < 1) {
			return;
		}

		for(final QuestAction action : actions) {
			action.notifyEvent(event, local);
		}
	}
}

package com.ss.server.model.quest.event.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestUtils;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventType;

/**
 * Слушатель событий связанных со взятием квеста.
 * 
 * @author Ronn
 */
public class AcceptedQuestEventListener extends AbstractQuestEventListener {

	public static final QuestEventType EVENT_TYPE = QuestEventType.getEventType(AcceptedQuestEventListener.class);

	public static final String PROP_QUEST_ID = "id";

	static {

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.addQuestAcceptedListeners((ship, quest, local) -> {

			final QuestEvent event = local.getNextQuestEvent();
			event.setQuest(quest);
			event.setEventType(EVENT_TYPE);
			event.setPlayerShip(ship);

			QuestUtils.notifyQuestEvent(ship, event, local);
		});
	}

	/** ид интересуемого квеста */
	private final int id;

	public AcceptedQuestEventListener(final Quest quest, final VarTable vars, final Node node) {
		super(quest, vars, node);

		this.id = vars.getInteger(PROP_QUEST_ID, quest.getId());
	}

	@Override
	protected boolean condition(final QuestEvent event) {
		final Quest quest = event.getQuest();
		return quest.getId() == getId();
	}

	@Override
	public QuestEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return ид интересуемого квеста.
	 */
	public int getId() {
		return id;
	}
}

package com.ss.server.model.quest.event;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;

/**
 * Модель квестового события.
 * 
 * @author Ronn
 */
public class QuestEvent {

	private Quest quest;

	private QuestEventType eventType;

	private PlayerShip playerShip;

	private SpaceStation station;

	private SpaceObject object;

	private QuestButton button;

	public QuestEvent clear() {
		return this;
	}

	public QuestButton getButton() {
		return button;
	}

	public QuestEventType getEventType() {
		return eventType;
	}

	public SpaceObject getObject() {
		return object;
	}

	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	public Quest getQuest() {
		return quest;
	}

	public SpaceStation getStation() {
		return station;
	}

	public void setButton(final QuestButton button) {
		this.button = button;
	}

	public void setEventType(final QuestEventType eventType) {
		this.eventType = eventType;
	}

	public void setObject(final SpaceObject object) {
		this.object = object;
	}

	public void setPlayerShip(final PlayerShip playerShip) {
		this.playerShip = playerShip;
	}

	public void setQuest(final Quest quest) {
		this.quest = quest;
	}

	public void setStation(final SpaceStation station) {
		this.station = station;
	}
}

package com.ss.server.model.quest.event.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestUtils;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventType;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Слушатель уничтожений NPS.
 * 
 * @author Ronn
 */
public class NpsDestroyerQuestEventListener extends AbstractQuestEventListener {

	public static final QuestEventType EVENT_TYPE = QuestEventType.getEventType(NpsDestroyerQuestEventListener.class);

	public static final String PROP_TEMPLATE_ID = "id";

	static {

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.addDestroyerListener((object, destroyer, local) -> {

			if(!object.isNps() || destroyer == null || !destroyer.isPlayerShip()) {
				return;
			}

			final PlayerShip playerShip = destroyer.getPlayerShip();

			final QuestEvent event = local.getNextQuestEvent();
			event.setEventType(EVENT_TYPE);
			event.setPlayerShip(playerShip);
			event.setObject(object);

			QuestUtils.notifyQuestEvent(playerShip, event, local);
		});
	}

	/** ид кнопки */
	private final int id;

	public NpsDestroyerQuestEventListener(final Quest quest, final VarTable vars, final Node node) {
		super(quest, vars, node);

		this.id = vars.getInteger(PROP_TEMPLATE_ID);
	}

	@Override
	protected boolean condition(final QuestEvent event) {

		if(event.getQuest() != getQuest()) {
			return false;
		}

		final SpaceObject object = event.getObject();
		return object.getTemplateId() == getId();
	}

	@Override
	public QuestEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return ид уничтожаемых NPS.
	 */
	public int getId() {
		return id;
	}
}

package com.ss.server.model.quest.event;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.action.QuestAction;

/**
 * Интерфейс для реализации слушателя квестового события.
 * 
 * @author Ronn
 */
public interface QuestEventListener {

	public static final String ACTIONS = "actions";

	/**
	 * @return список ответных действий на событие.
	 */
	public QuestAction[] getActions();

	/**
	 * @return тип прослушиваемых событий.
	 */
	public QuestEventType getEventType();

	/**
	 * @return квест, за которым закреплен слушатель.
	 */
	public Quest getQuest();

	/**
	 * Уведомление о событии.
	 * 
	 * @param event квестовое событие.
	 * @param local контейнер локальных объектов.
	 */
	public void notifyEvent(QuestEvent event, LocalObjects local);
}

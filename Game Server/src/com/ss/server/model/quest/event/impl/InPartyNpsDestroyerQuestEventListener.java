package com.ss.server.model.quest.event.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;
import rlib.util.array.Array;

import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestUtils;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.event.QuestEventType;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.social.party.Party;

/**
 * Слушатель уничтожений NPS в группе.
 * 
 * @author Ronn
 */
public class InPartyNpsDestroyerQuestEventListener extends AbstractQuestEventListener {

	public static final String PROP_TEMPLATE_ID = "id";

	public static final int MAX_DISTANCE = 2000;

	public static final QuestEventType EVENT_TYPE = QuestEventType.getEventType(InPartyNpsDestroyerQuestEventListener.class);

	static {

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.addDestroyerListener((object, destroyer, local) -> {

			if(!object.isNps() || destroyer == null || !destroyer.isPlayerShip()) {
				return;
			}

			final PlayerShip playerShip = destroyer.getPlayerShip();
			final Party party = playerShip.getParty();

			if(party == null) {
				return;
			}

			final Array<PlayerShip> members = party.getMembers();
			members.readLock();
			try {

				final QuestEvent event = local.getNextQuestEvent();
				event.setEventType(EVENT_TYPE);
				event.setObject(object);
				event.setPlayerShip(playerShip);

				for(final PlayerShip member : members.array()) {

					if(member == null) {
						break;
					}

					if(!(member == playerShip || playerShip.isInDistance(member, MAX_DISTANCE))) {
						continue;
					}

					QuestUtils.notifyQuestEvent(member, event, local);
				}

			} finally {
				members.readUnlock();
			}
		});
	}

	/** ид кнопки */
	private final int id;

	public InPartyNpsDestroyerQuestEventListener(final Quest quest, final VarTable vars, final Node node) {
		super(quest, vars, node);

		this.id = vars.getInteger(PROP_TEMPLATE_ID);
	}

	@Override
	protected boolean condition(final QuestEvent event) {

		if(event.getQuest() != getQuest()) {
			return false;
		}

		final SpaceObject object = event.getObject();
		return object.getTemplateId() == getId();
	}

	@Override
	public QuestEventType getEventType() {
		return EVENT_TYPE;
	}

	/**
	 * @return ид уничтожаемых NPS.
	 */
	public int getId() {
		return id;
	}
}

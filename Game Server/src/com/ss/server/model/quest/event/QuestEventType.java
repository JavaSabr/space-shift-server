package com.ss.server.model.quest.event;

import java.lang.reflect.Constructor;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ClassUtils;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.model.quest.Quest;

/**
 * Перечисление типов квестовых событий.
 * 
 * @author Ronn
 */
public class QuestEventType {

	/**
	 * Форматирование названия типа событий на основе реализации слушателя.
	 * 
	 * @param listenerClass реализация слушателя.
	 * @return форматированное название типа событий.
	 */
	protected static String formatName(final Class<?> listenerClass) {
		return listenerClass.getSimpleName().replace(QuestEventListener.class.getSimpleName(), StringUtils.EMPTY);
	}

	/**
	 * @param name название типа события.
	 * @return форматированное название типа события.
	 */
	protected static String formatName(final String name) {
		return name.replace(QuestEventListener.class.getSimpleName(), StringUtils.EMPTY);
	}

	/**
	 * @param listenerClass класс реализующий прослушивания типа событий.
	 * @return тип событий.
	 */
	public static final QuestEventType getEventType(final Class<?> listenerClass) {
		return TABLE.get(formatName(listenerClass));
	}

	/**
	 * @param name название типа событий.
	 * @return тип событий.
	 */
	public static final QuestEventType getEventType(final String name) {
		return TABLE.get(name);
	}

	/**
	 * Регистрация типа события.
	 * 
	 * @param eventType
	 */
	public static final void register(final QuestEventType eventType) {

		if(TABLE.containsKey(eventType.getName())) {
			LOGGER.warning(new RuntimeException("found duplicate event type for name " + eventType.getName()));
		}

		TABLE.put(eventType.getName(), eventType);
	}

	private static final Logger LOGGER = LoggerManager.getLogger(QuestEventType.class);

	private static final Table<String, QuestEventType> TABLE = TableFactory.newObjectTable();

	/** xml название типа */
	private String name;

	/** конструктор слушателя */
	private Constructor<? extends QuestEventListener> constructor;

	public QuestEventType(final String name, final Class<? extends QuestEventListener> eventClass) {
		try {
			this.name = formatName(name);
			this.constructor = eventClass.getConstructor(Quest.class, VarTable.class, Node.class);
		} catch(NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return xml название типа.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Создание нового инстанса слушателя.
	 * 
	 * @param quest квест, в котором находится слушатель.
	 * @param vars таблица атрибутов.
	 * @param node хмл узел.
	 * @return новый инстанс.
	 */
	public QuestEventListener newInstance(final Quest quest, final VarTable vars, final Node node) {
		return ClassUtils.newInstance(constructor, quest, vars, node);
	}
}

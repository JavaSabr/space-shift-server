package com.ss.server.model.quest;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;
import rlib.util.ref.Reference;
import rlib.util.ref.ReferenceFactory;
import rlib.util.ref.ReferenceType;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.database.QuestDBManager;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Контейнер состояния исполнения квеста.
 * 
 * @author Ronn
 */
public final class QuestState implements Foldable {

	private static final QuestDBManager QUEST_DB_MANAGER = QuestDBManager.getInstance();

	private final FoldablePool<Reference> referencePool = PoolFactory.newAtomicFoldablePool(Reference.class);

	private final Consumer<Reference> foldWrapFunc = wrap -> referencePool.put(wrap);

	private final BiConsumer<String, Reference> saveVarsFunc = (key, value) -> QUEST_DB_MANAGER.updateQuestVar(getOwner(), getQuest(), key, value.getInt());

	/** таблица квестовых переменных */
	private final Table<String, Reference> variables;

	/** кол-во модификаций переменных */
	private final AtomicInteger changed;

	/** владелец состояния квеста */
	private PlayerShip owner;

	/** выполняемый квест */
	private Quest quest;

	/** статус завершения задания */
	private QuestStatus status;

	/** стадия квеста */
	private int state;
	/** уникальный ид задания */
	private int objectId;

	/** находится ли задание в избранном */
	private boolean favorite;

	public QuestState(final int objectId) {
		this.changed = new AtomicInteger(0);
		this.variables = TableFactory.newObjectTable();
		this.objectId = objectId;
	}

	@Override
	public void finalyze() {
		variables.forEach(foldWrapFunc);
		variables.clear();
		setQuest(null);
		setOwner(null);
		setObjectId(0);
	}

	/**
	 * Получение квестовой кнопки.
	 * 
	 * @param event квестовое событие.
	 * @param id ид кнопки.
	 * @return квестовая кнопка.
	 */
	public QuestButton getButton(final QuestEvent event, final int id) {

		final Quest quest = getQuest();
		event.setQuest(quest);

		return quest.getButton(event, id);
	}

	/**
	 * @return уникальный ид задания.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return владелец состояния квеста.
	 */
	public PlayerShip getOwner() {
		return owner;
	}

	/**
	 * @return выполняемый квест.
	 */
	public Quest getQuest() {
		return quest;
	}

	/**
	 * @return ид выполняемого квеста.
	 */
	public int getQuestId() {
		return quest.getId();
	}

	/**
	 * @return стадия выполнения квеста.
	 */
	public int getState() {
		return state;
	}

	/**
	 * @return статус задания.
	 */
	public QuestStatus getStatus() {
		return status;
	}

	/**
	 * Получение значения переменной.
	 * 
	 * @param name название переменной.
	 * @return ее значение.
	 */
	public synchronized int getVar(final String name) {

		final Reference reference = variables.get(name);

		if(reference == null) {
			return 0;
		}

		if(reference.getReferenceType() != ReferenceType.INTEGER) {
			throw new RuntimeException("incorrect reference variable " + reference);
		}

		return reference.getInt();
	}

	/**
	 * @return таблица квестовых переменных.
	 */
	public Table<String, Reference> getVariables() {
		return variables;
	}

	/**
	 * @return находится ли задание в избранном.
	 */
	public boolean isFavorite() {
		return favorite;
	}

	/**
	 * Подгрузка переменных из БД.
	 */
	public void loadVars() {
		QUEST_DB_MANAGER.loadQuestVars(getOwner(), getQuest(), this, getVariables());
	}

	/**
	 * @return новая обертка для переменной.
	 */
	public Reference newVariable(final int value) {

		Reference reference = referencePool.take();

		if(reference == null) {
			reference = ReferenceFactory.newIntegerReference(value, false);
		}

		reference.setInt(value);

		return reference;
	}

	@Override
	public void reinit() {
		changed.set(0);
		state = 0;
	}

	/**
	 * Сохранение переменных в БД.
	 */
	public void saveVars() {

		if(changed.getAndSet(0) < 1) {
			return;
		}

		variables.accept(saveVarsFunc);
	}

	/**
	 * @param favorite находится ли задание в избранном.
	 */
	public void setFavorite(final boolean favorite) {
		this.favorite = favorite;
	}

	/**
	 * @param objectId уникальный ид задания.
	 */
	public void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	/**
	 * @param owner владелец состояния квеста.
	 */
	public void setOwner(final PlayerShip owner) {
		this.owner = owner;
	}

	/**
	 * @param quest выполняемый квест.
	 */
	public void setQuest(final Quest quest) {
		this.quest = quest;
	}

	/**
	 * @param state новая стадия квеста.
	 */
	public void setState(final int state) {
		this.state = state;
	}

	/**
	 * @param status статус задания.
	 */
	public void setStatus(final QuestStatus status) {
		this.status = status;
	}

	/**
	 * Установка нового значения переменной.
	 * 
	 * @param name название переменной.
	 * @param val новое значение.
	 */
	public synchronized void setVar(final String name, final int val) {

		final Reference reference = variables.get(name);

		if(reference == null) {
			variables.put(name, newVariable(val));
			QUEST_DB_MANAGER.addNewQuestVar(getOwner(), getQuest(), name, val);
		} else {
			reference.setInt(val);
			changed.incrementAndGet();
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [variables=" + variables + ", changed=" + changed + ", quest=" + quest + ", status=" + status + ", state=" + state + ", objectId=" + objectId
				+ ", favorite=" + favorite + "]";
	}
}

package com.ss.server.model;

/**
 * Интерфейс для реализации типа космического объекта.
 * 
 * @author Ronn
 */
public interface SpaceObjectType {

	/**
	 * @return тип объекта.
	 */
	public Class<? extends SpaceObject> getInstanceClass();

	/**
	 * @return индекс типа.
	 */
	public int index();

	/**
	 * @return получение имени для локализации.
	 */
	public default String getLangName() {
		return toString();
	}

	/**
	 * @return название типа.
	 */
	public default String getName() {
		return toString();
	}
}

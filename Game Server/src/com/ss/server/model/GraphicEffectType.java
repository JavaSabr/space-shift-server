package com.ss.server.model;

import java.lang.reflect.Constructor;

import org.w3c.dom.Node;

import rlib.util.ClassUtils;

import com.ss.server.template.graphic.effect.ExplosionEffectTemplate;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;
import com.ss.server.template.graphic.effect.ObjectTeleportEffectTemplate;

/**
 * Перечисление типов графических эффектов.
 * 
 * @author Ronn
 */
public enum GraphicEffectType {

	EXPLOSION(ExplosionEffectTemplate.class),
	OBJECT_TELEPORT(ObjectTeleportEffectTemplate.class), ;

	/** конструктор шаблона */
	private final Constructor<? extends GraphicEffectTemplate> constructor;

	private GraphicEffectType(final Class<? extends GraphicEffectTemplate> cs) {
		this.constructor = ClassUtils.getConstructor(cs, Node.class);
	}

	/**
	 * Создание нового шаблона.
	 * 
	 * @param node XML узел с описанием шаблона.
	 * @return новый шаблон эффекта.
	 */
	public GraphicEffectTemplate newTemplate(final Node node) {
		return ClassUtils.newInstance(constructor, node);
	}
}

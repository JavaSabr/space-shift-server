package com.ss.server.model.listener;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Слушатель взятия заданий.
 * 
 * @author Ronn
 */
public interface QuestAcceptedListener {

	/**
	 * @param ship корабль игрока.
	 * @param quest взятое задание.
	 * @param local контейнер локальных объектов.
	 */
	public void notify(final PlayerShip ship, final Quest quest, final LocalObjects local);
}

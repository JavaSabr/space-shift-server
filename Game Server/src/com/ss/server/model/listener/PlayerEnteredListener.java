package com.ss.server.model.listener;

import com.ss.server.model.ship.player.PlayerShip;

/**
 * Интерфейс для слушания входа корабля игрока куда-нибудь.
 * 
 * @author Ronn
 */
public interface PlayerEnteredListener {

	/**
	 * Уведомление о том, что корабль игрока вошел.
	 * 
	 * @param playerShip корабль игрока.
	 */
	public void onEntered(PlayerShip playerShip);
}

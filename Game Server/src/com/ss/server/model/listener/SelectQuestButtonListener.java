package com.ss.server.model.listener;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Слушатель нажатий игроком на действие заданий.
 * 
 * @author Ronn
 */
public interface SelectQuestButtonListener {

	/**
	 * @param ship корабль игрока.
	 * @param button нажатое действие.
	 * @param local контейнер локальных объектов.
	 */
	public void notify(final PlayerShip ship, final QuestButton button, LocalObjects local);
}

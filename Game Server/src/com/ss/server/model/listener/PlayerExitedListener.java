package com.ss.server.model.listener;

import com.ss.server.model.ship.player.PlayerShip;

/**
 * Интерфейс для слушания выхода корабля игрока от куда-то.
 * 
 * @author Ronn
 */
public interface PlayerExitedListener {

	/**
	 * Уведомление о том, что корабль игрока вышел.
	 * 
	 * @param playerShip корабль игрока.
	 */
	public void onExited(PlayerShip playerShip);
}

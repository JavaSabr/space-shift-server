package com.ss.server.model.listener;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;

/**
 * Интерфейс для реализации слушателя уничтоженных объектов.
 * 
 * @author Ronn
 */
public interface DestroyerListener {

	/**
	 * Уведомлние об уничтожении объекта.
	 * 
	 * @param object уничтоженный объект.
	 * @param destroyer уничтожитель.
	 * @param local контейнер локальных объектов.
	 */
	public void notify(SpaceObject object, SpaceObject destroyer, LocalObjects local);
}

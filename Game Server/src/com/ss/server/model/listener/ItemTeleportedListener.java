package com.ss.server.model.listener;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.item.SpaceItem;

/**
 * Интерфейс для реализации слушателя телепортированных предметов.
 * 
 * @author Ronn
 */
public interface ItemTeleportedListener {

	/**
	 * Уведомлние об телепортировании предмета.
	 * 
	 * @param object телепортировавший объект
	 * @param item телепортированный предмет.
	 * @param local контейнер локальных объектов.
	 */
	public void notify(SpaceObject object, SpaceItem item, LocalObjects local);
}

package com.ss.server.model.listener;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Слушатель завершения задания.
 * 
 * @author Ronn
 */
public interface QuestFinishedListener {

	/**
	 * @param ship корабль игрока.
	 * @param quest завершенное задание.
	 * @param local контейнер локальных объектов.
	 */
	public void notify(final PlayerShip ship, final Quest quest, final LocalObjects local);
}

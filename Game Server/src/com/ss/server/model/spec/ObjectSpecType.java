package com.ss.server.model.spec;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.manager.ObjectTypeManager;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.damage.DamageType;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Перечисление спецификаций объектов.
 * 
 * @author Ronn
 */
public enum ObjectSpecType {

	/** ----------- характеристики полета ----------------- */

	OBJECT_TYPE("@object-spec:type@") {

		@Override
		public String convert(final String value) {

			final SpaceObjectType type = TYPE_MANAGER.forName(value);

			if(type != null) {
				return type.getLangName();
			}

			LOGGER.warning("not found object type for name " + value);

			return super.convert(value);
		}
	},

	OBJECT_LEVEL("@object-spec:level@"),
	OBJECT_ATTACK_DISTANCE("@object-spec:attackDistance@"),
	OBJECT_ATTACK_POWER("@object-spec:attackPower@"),
	OBJECT_ATTACK_FIRE_RATE("@object-spec:attackFireRate@"),
	OBJECT_DAMAGE_TYPE("@object-spec:damageType@") {

		@Override
		public String convert(String value) {
			final DamageType damageType = DamageType.valueOf(value);
			return damageType.getLangName();
		}
	},
	OBJECT_SHOT_FLY_SPEED("@object-spec:shotFlySpeed@"),
	OBJECT_SHOT_ACCEL("@object-spec:shotAccel@"),
	OBJECT_SHOT_ROTATE_SPEED("@object-spec:shotRotateSpeed@"),
	OBJECT_RELOAD_TIME("@object-spec:reloadTime@"),
	OBJECT_MAX_RELOAD_TIME("@object-spec:maxReloadTime@"),
	OBJECT_MIN_RELOAD_TIME("@object-spec:minReloadTime@"),
	OBJECT_CHARGE_SIZE("@object-spec:chargeSize@"),
	OBJECT_CHARGE_REGEN("@object-spec:chargeRegen@"),
	OBJECT_MAX_FLY_SPEED("@object-spec:maxFlySpeed@"),
	OBJECT_FLY_ACCEL("@object-spec:flyAccel@"),
	OBJECT_ROTATE_SPEED("@object-spec:rotateSpeed@"),
	OBJECT_USED_ITEMS("@object-spec:usedItem@") {

		@Override
		public String convert(String value) {

			final ItemTable itemTable = ItemTable.getInstance();
			final ItemTemplate template = itemTable.getTemplate(Integer.parseInt(value));

			if(template == null) {
				LOGGER.warning("not found item consume for " + value);
				return value;
			}

			return template.getName();
		}
	};

	private static final Logger LOGGER = LoggerManager.getLogger(ObjectSpecType.class);

	private static final ObjectTypeManager TYPE_MANAGER = ObjectTypeManager.getInstance();

	public static final int LENGTH = values().length;

	private String langName;

	private ObjectSpecType(final String langName) {
		this.langName = langName;
	}

	/**
	 * Конвектирование значения.
	 * 
	 * @param value текущее значение.
	 * @return нужное значение.
	 */
	public String convert(final String value) {
		return value;
	}

	/**
	 * @return название стата для локализации.
	 */
	public String getLangName() {
		return langName;
	}
}

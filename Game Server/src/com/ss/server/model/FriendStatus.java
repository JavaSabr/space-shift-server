package com.ss.server.model;

/**
 * Перечисление статуса дружелюбности объекта в игроку.
 * 
 * @author Ronn
 */
public enum FriendStatus {
	PARTY,
	CLAN,
	ALIANCE,
	NEUTRAL,
	AGGRESSOR;
}

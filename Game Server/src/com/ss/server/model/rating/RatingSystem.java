package com.ss.server.model.rating;

import rlib.util.pools.Foldable;

import com.ss.server.model.rating.event.RatingEvent;
import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;
import com.ss.server.model.ship.SpaceShip;

/**
 * Интерфейс для реализации системы рейтинга объекта.
 * 
 * @author Ronn
 */
public interface RatingSystem extends Foldable {

	/**
	 * Уведомление о событии, влияющего на рейтинг модулей.
	 * 
	 * @param event событие.
	 */
	public void notify(ModuleRatingEvent event);

	/**
	 * Уведомление о событии, влияющего на локальный рейтинг.
	 * 
	 * @param event событие.
	 */
	public void notify(LocalRatingEvent event);

	/**
	 * Поиск элемента рейтинга указаного типа.
	 * 
	 * @param type тип элемента рейтинга.
	 * @return элемент рейтинга.
	 */
	public <T extends RatingElement<? extends RatingEvent>> T findElement(RatingElementType type);

	/**
	 * @return список всех элементов системы рейтинга.
	 */
	public RatingElement<? super RatingEvent>[] getRatingElements();

	/**
	 * Процесс сохраненрия изменений.
	 */
	public default void saveChanges() {
	}

	/**
	 * Инициализация рейтинга при первом создании.
	 */
	public default void init() {
	}

	/**
	 * @return корабль, за которым закреплен рейтинг.
	 */
	public SpaceShip getSpaceShip();
}

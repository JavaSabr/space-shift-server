package com.ss.server.model.rating.local.module.shield.impl;

import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.module.shield.ShieldModuleRatingEvent;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElementType;

/**
 * Реализация элемента рейтинга использования черепашьего щита.
 * 
 * @author Ronn
 */
public class TurtleRatingElement extends AbstractShieldModuleRatingElement<ShieldModuleRatingEvent> {

	@Override
	public RatingElementType getElementType() {
		return ShieldModuleRatingElementType.TURTLE;
	}
}

package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.nps.impl.EnemyDron;

/**
 * Реализация элемента рейтинга уничтожение дронами.
 * 
 * @author Ronn
 */
public class DroneNpsDestroyRatingElement extends NpsDestroyRatingElement {

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.NPS_DRON;
	}

	@Override
	public void notify(DestroyRatingEvent event) {

		final SpaceObject destroyer = event.getDestroyer();

		if(destroyer == null || !destroyer.isNps()) {
			return;
		}

		final Nps nps = destroyer.getNps();

		if(nps.isEnemyNps() && nps instanceof EnemyDron) {
			destroyCount.incrementAndGet();
			modify();
		}
	}
}

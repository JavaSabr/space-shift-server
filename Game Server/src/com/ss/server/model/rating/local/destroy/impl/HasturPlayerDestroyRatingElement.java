package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.faction.Fraction;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Рейтинг элемента смертей от игроков фракции хастура.
 * 
 * @author Ronn
 */
public class HasturPlayerDestroyRatingElement extends PlayerDestroyRatingElement {

	public static final int HASTUR_ID = 2;

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.PLAYER_HASTUR;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
		super.notify(event);

		final SpaceObject destroyer = event.getDestroyer();

		if(destroyer == null || !destroyer.isPlayerShip() || event.getDestroyed() == event.getDestroyer()) {
			return;
		}

		final PlayerShip playerShip = destroyer.getPlayerShip();
		final Fraction fraction = playerShip.getFraction();

		if(fraction.getId() == HASTUR_ID) {
			destroyCount.incrementAndGet();
			modify();
		}
	}
}

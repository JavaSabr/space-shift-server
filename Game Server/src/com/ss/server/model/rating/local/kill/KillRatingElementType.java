package com.ss.server.model.rating.local.kill;

import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.local.kill.impl.DronNpsKillRatingElement;
import com.ss.server.model.rating.local.kill.impl.FederationPlayerKillRatingElement;
import com.ss.server.model.rating.local.kill.impl.FreedomPlayerKillRatingElement;
import com.ss.server.model.rating.local.kill.impl.HasturPlayerKillRatingElement;

/**
 * Перечисление типов элементов рейтинга убийства врага.
 * 
 * @author Ronn
 */
@SuppressWarnings("rawtypes")
public enum KillRatingElementType implements RatingElementType {
	PLAYER_HASTUR(HasturPlayerKillRatingElement.class),
	PLAYER_FEDERATION(FederationPlayerKillRatingElement.class),
	PLAYER_FREEDOM(FreedomPlayerKillRatingElement.class),
	DRON_NPS(DronNpsKillRatingElement.class), ;

	public static final KillRatingElementType[] TYPES = values();

	public static final int SIZE = TYPES.length;

	public static final KillRatingElementType valueOf(int index) {
		return TYPES[index];
	}

	/** класс реализации элемента рейтинга */
	private Class<? extends KillRatingElement> implementation;

	private KillRatingElementType(final Class<? extends KillRatingElement> implementation) {
		this.implementation = implementation;
	}

	@Override
	public Class<? extends RatingElement> getImplementation() {
		return implementation;
	}

	@Override
	public int getIndex() {
		return ordinal();
	}

	@Override
	public String getName() {
		return name();
	}
}

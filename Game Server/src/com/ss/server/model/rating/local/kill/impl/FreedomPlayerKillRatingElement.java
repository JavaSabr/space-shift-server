package com.ss.server.model.rating.local.kill.impl;

import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.rating.local.kill.KillRatingElementType;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Реализация элемента рейтинга по убийству игроков фридума.
 * 
 * @author Ronn
 */
public class FreedomPlayerKillRatingElement extends PlayerKillRatingElement {

	@Override
	public RatingElementType getElementType() {
		return KillRatingElementType.PLAYER_FREEDOM;
	}

	@Override
	public void notify(KillRatingEvent event) {

		final SpaceShip killer = event.getKiller();

		if(!killer.isPlayerShip()) {
			return;
		}

		final PlayerShip playerShip = killer.getPlayerShip();

		if(playerShip.getFractionId() != FREEDOM_ID) {
			return;
		}

		notifyImpl(event);
	}
}

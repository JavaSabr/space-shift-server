package com.ss.server.model.rating.local.module.weapon;

import com.ss.server.model.rating.event.local.module.weapon.WeaponModuleRatingEvent;
import com.ss.server.model.rating.local.module.ModuleRatingElement;

/**
 * Интерфейс для реализации элемента рейтинга модуля оружия.
 * 
 * @author Ronn
 */
public interface WeaponModuleRatingElement<E extends WeaponModuleRatingEvent> extends ModuleRatingElement<E> {

	/**
	 * @return процент меткости.
	 */
	public default float getAccuracy() {
		return (float) (getHitCount() / (double) getShotCount());
	}

	/**
	 * @return кол-во итощений оружия.
	 */
	public long getDepletionCount();

	/**
	 * @return кол-во произведенных попаданий в цель.
	 */
	public long getHitCount();

	/**
	 * @return кол-во произведенных выстрелов.
	 */
	public long getShotCount();

	@Override
	public default boolean isWeaponRatingElement() {
		return true;
	}

	/**
	 * @param deplectionCount кол-во истощений оружия.
	 */
	public void setDeplectionCount(long deplectionCount);

	/**
	 * @param hitCount кол-во произведенных попаданий.
	 */
	public void setHitCount(long hitCount);

	/**
	 * @param shotCount кол-во произведенных выстрелов.
	 */
	public void setShotCount(long shotCount);
}

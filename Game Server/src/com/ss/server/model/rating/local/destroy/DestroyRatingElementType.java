package com.ss.server.model.rating.local.destroy;

import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.local.destroy.impl.BossNpsDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.DroneNpsDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.FederationPlayerDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.FreedomPlayerDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.HasturPlayerDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.LaserWeaponDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.NeutronWeaponDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.NpsDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.PhotonWeaponDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.PlasmaWeaponDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.PlayerDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.PulseWeaponDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.RocketWeaponDestroyRatingElement;
import com.ss.server.model.rating.local.destroy.impl.TachyonWeaponDestroyRatingElement;

/**
 * Перечисление типов элементов рейтинга уничтожение корабля.
 * 
 * @author Ronn
 */
@SuppressWarnings("rawtypes")
public enum DestroyRatingElementType implements RatingElementType {
	PLAYER(PlayerDestroyRatingElement.class),
	PLAYER_HASTUR(HasturPlayerDestroyRatingElement.class),
	PLAYER_FEDERATION(FederationPlayerDestroyRatingElement.class),
	PLAYER_FREEDOM(FreedomPlayerDestroyRatingElement.class),

	WEAPON_LASER(LaserWeaponDestroyRatingElement.class),
	WEAPON_PULSE(PulseWeaponDestroyRatingElement.class),
	WEAPON_PHOTON(PhotonWeaponDestroyRatingElement.class),
	WEAPON_NEUTRON(NeutronWeaponDestroyRatingElement.class),
	WEAPON_TACHYON(TachyonWeaponDestroyRatingElement.class),
	WEAPON_PLASMA(PlasmaWeaponDestroyRatingElement.class),
	WEAPON_ROCKET(RocketWeaponDestroyRatingElement.class),

	NPS(NpsDestroyRatingElement.class),
	NPS_DRON(DroneNpsDestroyRatingElement.class),
	NPS_BOSS(BossNpsDestroyRatingElement.class), ;

	public static final RatingElementType[] TYPES = values();

	public static final int SIZE = TYPES.length;

	public static final DestroyRatingElementType valueOf(int index) {
		return (DestroyRatingElementType) TYPES[index];
	}

	/** класс реализации элемента рейтинга */
	private Class<? extends DestroyRatingElement> implementation;

	private DestroyRatingElementType(final Class<? extends DestroyRatingElement> implementation) {
		this.implementation = implementation;
	}

	@Override
	public Class<? extends RatingElement> getImplementation() {
		return implementation;
	}

	@Override
	public int getIndex() {
		return ordinal();
	}

	@Override
	public String getName() {
		return name();
	}
}

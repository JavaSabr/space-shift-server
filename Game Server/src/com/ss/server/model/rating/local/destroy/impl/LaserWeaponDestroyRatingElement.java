package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;

/**
 * РЕализация элемента рейтинга уничтожения от лазерного оружия.
 * 
 * @author Ronn
 */
public class LaserWeaponDestroyRatingElement extends WeaponDestroyRatingElement {

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.WEAPON_LASER;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
		super.notify(event);

		final DamageType damageType = event.getDamageType();

		if(damageType == DamageType.LASER) {
			destroyCount.incrementAndGet();
			modify();
		}
	}
}

package com.ss.server.model.rating.local.module.impl;

import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;
import com.ss.server.model.rating.local.impl.AbstractLocalRatingElement;
import com.ss.server.model.rating.local.module.ModuleRatingElement;

/**
 * Базовая реализация элемента рейтинга модуля.
 * 
 * @author Ronn
 */
public abstract class AbstractModuleRatingElement<E extends ModuleRatingEvent> extends AbstractLocalRatingElement<E> implements ModuleRatingElement<E> {

}

package com.ss.server.model.rating.local.module.weapon.impl;

import java.util.concurrent.atomic.AtomicLong;

import com.ss.server.model.rating.event.local.module.weapon.WeaponModuleRatingEvent;
import com.ss.server.model.rating.event.local.module.weapon.impl.HitWeaponRatingEvent;
import com.ss.server.model.rating.event.local.module.weapon.impl.ShotWeaponRatingEvent;
import com.ss.server.model.rating.local.module.impl.AbstractModuleRatingElement;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElement;

/**
 * Базовая реализация элемента рейтинга модуля оружия.
 * 
 * @author Ronn
 */
public abstract class AbstractWeaponModuleRatingElement<E extends WeaponModuleRatingEvent> extends AbstractModuleRatingElement<E> implements WeaponModuleRatingElement<E> {

	/** кол-во произведенных выстрелов */
	protected final AtomicLong shotCount;
	/** кол-во попаданий */
	protected final AtomicLong hitCount;
	/** кол-во истощений оружия */
	protected final AtomicLong depletionCount;

	public AbstractWeaponModuleRatingElement() {
		this.shotCount = new AtomicLong();
		this.hitCount = new AtomicLong();
		this.depletionCount = new AtomicLong();
	}

	@Override
	public void notify(E event) {
		super.notify(event);

		if(event.getClass() == ShotWeaponRatingEvent.class) {
			shotCount.incrementAndGet();
			modify();
		} else if(event.getClass() == HitWeaponRatingEvent.class) {
			hitCount.incrementAndGet();
			modify();
		}
	}

	@Override
	public void finalyze() {
		super.finalyze();

		shotCount.set(0);
		hitCount.set(0);
		depletionCount.set(0);
	}

	@Override
	public long getHitCount() {
		return hitCount.get();
	}

	@Override
	public long getShotCount() {
		return shotCount.get();
	}

	@Override
	public void setHitCount(long hitCount) {
		this.hitCount.set(hitCount);
	}

	@Override
	public void setShotCount(long shotCount) {
		this.shotCount.set(shotCount);
	}

	@Override
	public long getDepletionCount() {
		return depletionCount.get();
	}

	@Override
	public void setDeplectionCount(long deplectionCount) {
		this.depletionCount.set(deplectionCount);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [shotCount=" + shotCount + ", hitCount=" + hitCount + "]";
	}
}

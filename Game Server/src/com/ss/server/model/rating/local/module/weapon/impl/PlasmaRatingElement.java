package com.ss.server.model.rating.local.module.weapon.impl;

import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.module.weapon.WeaponModuleRatingEvent;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElementType;

/**
 * Реализация элемента рейтинга использования импульсного оружия.
 * 
 * @author Ronn
 */
public class PlasmaRatingElement extends AbstractWeaponModuleRatingElement<WeaponModuleRatingEvent> {

	@Override
	public RatingElementType getElementType() {
		return WeaponModuleRatingElementType.PLASMA;
	}

}

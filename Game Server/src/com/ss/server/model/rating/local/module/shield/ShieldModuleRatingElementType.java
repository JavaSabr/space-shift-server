package com.ss.server.model.rating.local.module.shield;

import com.ss.server.model.damage.ShieldType;
import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.local.module.shield.impl.RefrectiveRatingElement;
import com.ss.server.model.rating.local.module.shield.impl.StealthRatingElement;
import com.ss.server.model.rating.local.module.shield.impl.TardionnyRatingElement;
import com.ss.server.model.rating.local.module.shield.impl.TurtleRatingElement;

/**
 * Перечисление типов элементов рейтингов модулей щитов.
 * 
 * @author Ronn
 */
@SuppressWarnings("rawtypes")
public enum ShieldModuleRatingElementType implements RatingElementType {
	REFRECTIVE(RefrectiveRatingElement.class, ShieldType.REFRECTIVE),
	STEALTH(StealthRatingElement.class, ShieldType.STEALTH),
	TARDIONNY(TardionnyRatingElement.class, ShieldType.TARDIONNY),
	TURTLE(TurtleRatingElement.class, ShieldType.TURTLE);

	public static final ShieldModuleRatingElementType[] TYPES = values();

	public static final int SIZE = TYPES.length;

	public static final ShieldModuleRatingElementType valueOf(int index) {
		return TYPES[index];
	}

	public static final ShieldModuleRatingElementType valueOf(ShieldType shieldType) {

		for(ShieldModuleRatingElementType type : TYPES) {
			if(type.shieldType == shieldType) {
				return type;
			}
		}

		return null;
	}

	/** класс реализации элемента рейтинга */
	private Class<? extends ShieldModuleRatingElement> implementation;

	/** тип щита, к которому относится тип рейтинга */
	private final ShieldType shieldType;

	private ShieldModuleRatingElementType(final Class<? extends ShieldModuleRatingElement> implementation, ShieldType shieldType) {
		this.implementation = implementation;
		this.shieldType = shieldType;
	}

	@Override
	public Class<? extends RatingElement> getImplementation() {
		return implementation;
	}

	@Override
	public int getIndex() {
		return ordinal();
	}

	@Override
	public String getName() {
		return name();
	}
}

package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;

/**
 * Реализация элемента рейтинга уничтожения от плазматического оружия.
 * 
 * @author Ronn
 */
public class PlasmaWeaponDestroyRatingElement extends WeaponDestroyRatingElement {

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.WEAPON_PLASMA;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
		super.notify(event);

		final DamageType damageType = event.getDamageType();

		if(damageType == DamageType.PLASMA) {
			destroyCount.incrementAndGet();
			modify();
		}
	}
}

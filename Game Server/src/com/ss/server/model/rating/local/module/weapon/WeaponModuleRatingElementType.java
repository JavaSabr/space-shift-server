package com.ss.server.model.rating.local.module.weapon;

import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.local.module.weapon.impl.LaserRatingElement;
import com.ss.server.model.rating.local.module.weapon.impl.NeutronRatingElement;
import com.ss.server.model.rating.local.module.weapon.impl.PhotonRatingElement;
import com.ss.server.model.rating.local.module.weapon.impl.PlasmaRatingElement;
import com.ss.server.model.rating.local.module.weapon.impl.PulseRatingElement;
import com.ss.server.model.rating.local.module.weapon.impl.RocketLauncherRatingElement;
import com.ss.server.model.rating.local.module.weapon.impl.TachyonRatingElement;

/**
 * Перечисление типов элементов рейтингов модулей оружия.
 * 
 * @author Ronn
 */
@SuppressWarnings("rawtypes")
public enum WeaponModuleRatingElementType implements RatingElementType {
	ROCKET_LAUNCHER(RocketLauncherRatingElement.class),
	TACHYON(TachyonRatingElement.class),
	LASER(LaserRatingElement.class),
	PULSE(PulseRatingElement.class),
	NEUTRON(NeutronRatingElement.class),
	PHOTON(PhotonRatingElement.class),
	PLASMA(PlasmaRatingElement.class);

	public static final RatingElementType[] TYPES = values();

	public static final int SIZE = TYPES.length;

	public static final WeaponModuleRatingElementType valueOf(int index) {
		return (WeaponModuleRatingElementType) TYPES[index];
	}

	/** класс реализации элемента рейтинга */
	private Class<? extends WeaponModuleRatingElement> implementation;

	private WeaponModuleRatingElementType(final Class<? extends WeaponModuleRatingElement> implementation) {
		this.implementation = implementation;
	}

	@Override
	public Class<? extends RatingElement> getImplementation() {
		return implementation;
	}

	@Override
	public int getIndex() {
		return ordinal();
	}

	@Override
	public String getName() {
		return name();
	}
}

package com.ss.server.model.rating.local;

/**
 * Перечисление типов переменных элементов локального рейтинга.
 * 
 * @author Ronn
 */
public enum LocalRatingElementVarType {

	HIT_COUNT(long.class),
	REPAIR_COUNT(long.class);

	private LocalRatingElementVarType(Class<?> type) {
	}
}

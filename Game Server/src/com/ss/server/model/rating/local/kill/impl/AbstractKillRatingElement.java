package com.ss.server.model.rating.local.kill.impl;

import java.util.concurrent.atomic.AtomicLong;

import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.rating.local.impl.AbstractLocalRatingElement;
import com.ss.server.model.rating.local.kill.KillRatingElement;

/**
 * Базовая реализация элемента рейтинга уничтожения врагов.
 * 
 * @author Ronn
 */
public abstract class AbstractKillRatingElement extends AbstractLocalRatingElement<KillRatingEvent> implements KillRatingElement<KillRatingEvent> {

	/** счетчик убийств ракетами */
	protected final AtomicLong rocketLauncherCount;
	/** мсетчик убийств лазерами */
	protected final AtomicLong laserCount;
	/** счетчик убийств импульсами */
	protected final AtomicLong pulseCount;
	/** счетчик убийств плазмой */
	protected final AtomicLong plasmaCount;
	/** счетчик убийств фотонами */
	protected final AtomicLong photonCount;
	/** счетчик убийств нейтронами */
	protected final AtomicLong neutronCount;
	/** счетчикубийств тахионами */
	protected final AtomicLong tachyonCount;

	public AbstractKillRatingElement() {
		this.rocketLauncherCount = new AtomicLong();
		this.laserCount = new AtomicLong();
		this.pulseCount = new AtomicLong();
		this.plasmaCount = new AtomicLong();
		this.photonCount = new AtomicLong();
		this.neutronCount = new AtomicLong();
		this.tachyonCount = new AtomicLong();
	}

	@Override
	public void finalyze() {
		super.finalyze();

		laserCount.set(0);
		rocketLauncherCount.set(0);
		pulseCount.set(0);
		plasmaCount.set(0);
		photonCount.set(0);
		tachyonCount.set(0);
		neutronCount.set(0);
	}

	@Override
	public long getLaserCount() {
		return laserCount.get();
	}

	@Override
	public long getNeutronCount() {
		return neutronCount.get();
	}

	@Override
	public long getPhotonCount() {
		return photonCount.get();
	}

	@Override
	public long getPlasmaCount() {
		return plasmaCount.get();
	}

	@Override
	public long getPulseCount() {
		return pulseCount.get();
	}

	@Override
	public long getRocketLayncherCount() {
		return rocketLauncherCount.get();
	}

	@Override
	public long getTachyonCount() {
		return tachyonCount.get();
	}

	@Override
	public void setLaserCount(final long count) {
		this.laserCount.set(count);
	}

	@Override
	public void setNeutronCount(final long count) {
		this.neutronCount.set(count);
	}

	@Override
	public void setPhotonCount(final long count) {
		this.photonCount.set(count);
	}

	@Override
	public void setPlasmaCount(final long count) {
		this.plasmaCount.set(count);
	}

	@Override
	public void setPulseCount(final long count) {
		this.pulseCount.set(count);
	}

	@Override
	public void setRocketLauncherCount(final long count) {
		this.rocketLauncherCount.set(count);
	}

	@Override
	public void setTachyonCount(final long count) {
		this.tachyonCount.set(count);
	}

	protected void notifyImpl(KillRatingEvent event) {

		final DamageType damageType = event.getDamageType();

		if(damageType == DamageType.NONE) {
			return;
		} else if(damageType == DamageType.EXPLOSION) {
			rocketLauncherCount.incrementAndGet();
		} else if(damageType == DamageType.LASER) {
			laserCount.incrementAndGet();
		} else if(damageType == DamageType.NEUTRON) {
			neutronCount.incrementAndGet();
		} else if(damageType == DamageType.PHOTON) {
			photonCount.incrementAndGet();
		} else if(damageType == DamageType.PLASMA) {
			plasmaCount.incrementAndGet();
		} else if(damageType == DamageType.PULSE) {
			pulseCount.incrementAndGet();
		} else if(damageType == DamageType.TACHYON) {
			tachyonCount.incrementAndGet();
		}

		modify();
	}
}

package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;

/**
 * Реализация элемента рейтинга уничтожение дронами.
 * 
 * @author Ronn
 */
public class BossNpsDestroyRatingElement extends NpsDestroyRatingElement {

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.NPS_BOSS;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
	}
}

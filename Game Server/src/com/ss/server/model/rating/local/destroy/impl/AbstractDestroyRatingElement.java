package com.ss.server.model.rating.local.destroy.impl;

import java.util.concurrent.atomic.AtomicLong;

import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElement;
import com.ss.server.model.rating.local.impl.AbstractLocalRatingElement;

/**
 * Базовая реализация элемента рейтинга смерти.
 * 
 * @author Ronn
 */
public abstract class AbstractDestroyRatingElement<E extends DestroyRatingEvent> extends AbstractLocalRatingElement<E> implements DestroyRatingElement<E> {

	/** счетчик кол-ва уничтожений */
	protected final AtomicLong destroyCount;

	public AbstractDestroyRatingElement() {
		this.destroyCount = new AtomicLong();
	}

	@Override
	public long getDestroyCount() {
		return destroyCount.get();
	}

	@Override
	public void setDestroyCount(long destroyCount) {
		this.destroyCount.getAndSet(destroyCount);
	}

	@Override
	public void notify(E event) {
		super.notify(event);
	}

	@Override
	public void finalyze() {
		super.finalyze();

		destroyCount.getAndSet(0);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [destroyCount=" + destroyCount + "]";
	}
}

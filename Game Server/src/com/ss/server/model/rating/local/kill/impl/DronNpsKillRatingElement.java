package com.ss.server.model.rating.local.kill.impl;

import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.rating.local.kill.KillRatingElementType;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.impl.EnemyDron;

/**
 * Реализация элемента рейтинга по уничтожению дронов.
 * 
 * @author Ronn
 */
public class DronNpsKillRatingElement extends NpsKillRatingElement {

	@Override
	public RatingElementType getElementType() {
		return KillRatingElementType.DRON_NPS;
	}

	@Override
	public void notify(KillRatingEvent event) {

		final SpaceShip killer = event.getKiller();

		if(!killer.isNps()) {
			return;
		}

		if(!(killer instanceof EnemyDron)) {
			return;
		}

		notifyImpl(event);
	}
}

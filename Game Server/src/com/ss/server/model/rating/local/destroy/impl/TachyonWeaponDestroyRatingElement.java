package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;

/**
 * Реализация элемента рейтинга уничтожения от тахионного оружия.
 * 
 * @author Ronn
 */
public class TachyonWeaponDestroyRatingElement extends WeaponDestroyRatingElement {

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.WEAPON_TACHYON;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
		super.notify(event);

		final DamageType damageType = event.getDamageType();

		if(damageType == DamageType.TACHYON) {
			destroyCount.incrementAndGet();
			modify();
		}
	}
}

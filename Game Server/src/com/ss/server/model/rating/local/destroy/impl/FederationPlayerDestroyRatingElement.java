package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.faction.Fraction;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Рейтинг элемента смертей от игроков фракции федерации.
 * 
 * @author Ronn
 */
public class FederationPlayerDestroyRatingElement extends PlayerDestroyRatingElement {

	public static final int FEDERATION_ID = 1;

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.PLAYER_FEDERATION;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
		super.notify(event);

		final SpaceObject destroyer = event.getDestroyer();

		if(destroyer == null || !destroyer.isPlayerShip() || event.getDestroyed() == event.getDestroyer()) {
			return;
		}

		final PlayerShip playerShip = destroyer.getPlayerShip();
		final Fraction fraction = playerShip.getFraction();

		if(fraction.getId() == FEDERATION_ID) {
			destroyCount.incrementAndGet();
			modify();
		}
	}
}

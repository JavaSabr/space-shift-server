package com.ss.server.model.rating.local.impl;

import java.util.concurrent.atomic.AtomicLong;

import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.rating.event.local.impl.HitSpaceShipBodyRatingEvent;
import com.ss.server.model.rating.event.local.impl.RepairSpaceShipBodyRatingEvent;
import com.ss.server.model.rating.local.LocalRatingElementType;
import com.ss.server.model.rating.local.LocalRatingElementVarType;

/**
 * Реализация локального рейтинга по кол-ву попаданий в корпус корабля.
 * 
 * @author Ronn
 */
public class SpaceShipBodyRatingElement extends AbstractLocalRatingElement<LocalRatingEvent> {

	/** кол-во попаданий в корпус */
	protected final AtomicLong hitCount;
	/** кол-во полных восстановлений */
	protected final AtomicLong repairCoint;

	public SpaceShipBodyRatingElement() {
		this.hitCount = new AtomicLong();
		this.repairCoint = new AtomicLong();
	}

	@Override
	public void notify(LocalRatingEvent event) {
		super.notify(event);

		if(event.getClass() == HitSpaceShipBodyRatingEvent.class) {
			hitCount.incrementAndGet();
			modify();
		} else if(event.getClass() == RepairSpaceShipBodyRatingEvent.class) {
			repairCoint.incrementAndGet();
			modify();
		}
	}

	@Override
	public void setVar(LocalRatingElementVarType varType, long value) {
		super.setVar(varType, value);

		switch(varType) {
			case HIT_COUNT: {
				setHitCount(value);
				break;
			}
			case REPAIR_COUNT: {
				setRepairCount(value);
				break;
			}
			default: {
				break;
			}
		}
	}

	@Override
	public long getLongVar(LocalRatingElementVarType varType) {

		switch(varType) {
			case HIT_COUNT: {
				return getHitCount();
			}
			case REPAIR_COUNT: {
				return getRepairCoint();
			}
			default: {
				break;
			}
		}

		return super.getLongVar(varType);
	}

	@Override
	public RatingElementType getElementType() {
		return LocalRatingElementType.SPACE_SHIP_BODY;
	}

	/**
	 * @return кол-во попаданий в корпус.
	 */
	public long getHitCount() {
		return hitCount.get();
	}

	/**
	 * @return кол-во полных восстановлений.
	 */
	public long getRepairCoint() {
		return repairCoint.get();
	}

	/**
	 * @param hitCount кол-во попаданий в корпус.
	 */
	public void setHitCount(long hitCount) {
		this.hitCount.getAndSet(hitCount);
	}

	/**
	 * @param repairCount кол-во полных восстановлений.
	 */
	public void setRepairCount(long repairCount) {
		this.repairCoint.getAndSet(repairCount);
	}

	@Override
	public void finalyze() {
		super.finalyze();

		hitCount.getAndSet(0);
		repairCoint.getAndSet(0);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [hitCount=" + hitCount + ", repairCoint=" + repairCoint + "]";
	}
}

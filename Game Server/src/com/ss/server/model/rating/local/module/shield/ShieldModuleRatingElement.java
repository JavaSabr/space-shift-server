package com.ss.server.model.rating.local.module.shield;

import com.ss.server.model.rating.event.local.module.shield.ShieldModuleRatingEvent;
import com.ss.server.model.rating.local.module.ModuleRatingElement;

/**
 * Интерфейс для реализации элемента рейтинга щита.
 * 
 * @author Ronn
 */
public interface ShieldModuleRatingElement<E extends ShieldModuleRatingEvent> extends ModuleRatingElement<E> {

	/**
	 * @return кол-во попаданий в щит.
	 */
	public long getHitCount();

	/**
	 * @return кол-во перезарядок щита.
	 */
	public long getReloadCount();

	/**
	 * @param hitCount кол-во попаданий в щит.
	 */
	public void setHitCount(long hitCount);

	/**
	 * @param reloadCount кол-во перезарядок щита.
	 */
	public void setReloadCount(long reloadCount);

	@Override
	public default boolean isShieldRatingElement() {
		return true;
	}
}

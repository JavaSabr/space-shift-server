package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;

/**
 * Реализация элемента рейтинга смертей от НПС.
 * 
 * @author Ronn
 */
public class NpsDestroyRatingElement extends AbstractDestroyRatingElement<DestroyRatingEvent> {

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.NPS;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
		super.notify(event);

		final SpaceObject destroyer = event.getDestroyer();

		if(destroyer != null && destroyer.isNps()) {
			destroyCount.incrementAndGet();
			modify();
		}
	}
}

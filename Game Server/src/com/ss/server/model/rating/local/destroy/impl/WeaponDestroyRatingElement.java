package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;

/**
 * Рейтинг элемента уничтожения оружием.
 * 
 * @author Ronn
 */
public abstract class WeaponDestroyRatingElement extends AbstractDestroyRatingElement<DestroyRatingEvent> {

}

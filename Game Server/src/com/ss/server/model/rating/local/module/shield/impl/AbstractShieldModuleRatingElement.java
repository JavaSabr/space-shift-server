package com.ss.server.model.rating.local.module.shield.impl;

import java.util.concurrent.atomic.AtomicLong;

import com.ss.server.model.rating.event.local.module.shield.ShieldModuleRatingEvent;
import com.ss.server.model.rating.event.local.module.shield.impl.HitShieldRatingEvent;
import com.ss.server.model.rating.local.module.impl.AbstractModuleRatingElement;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElement;

/**
 * Базовая реализация элемента рейтинга щита.
 * 
 * @author Ronn
 */
public abstract class AbstractShieldModuleRatingElement<E extends ShieldModuleRatingEvent> extends AbstractModuleRatingElement<E> implements ShieldModuleRatingElement<E> {

	/** счетчик попаадний в щит */
	protected final AtomicLong hitCount;
	/** счетчик перезагрузок щита */
	protected final AtomicLong reloadCount;

	public AbstractShieldModuleRatingElement() {
		this.hitCount = new AtomicLong();
		this.reloadCount = new AtomicLong();
	}

	@Override
	public void notify(E event) {
		super.notify(event);

		if(event.getClass() == HitShieldRatingEvent.class) {
			hitCount.incrementAndGet();
			modify();
		}
	}

	@Override
	public void finalyze() {
		super.finalyze();

		hitCount.getAndSet(0);
		reloadCount.getAndSet(0);
	}

	@Override
	public long getHitCount() {
		return hitCount.get();
	}

	@Override
	public long getReloadCount() {
		return reloadCount.get();
	}

	@Override
	public void setHitCount(long hitCount) {
		this.hitCount.getAndSet(hitCount);
	}

	@Override
	public void setReloadCount(long reloadCount) {
		this.reloadCount.getAndSet(reloadCount);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [hitCount=" + hitCount + ", reloadCount=" + reloadCount + "]";
	}
}

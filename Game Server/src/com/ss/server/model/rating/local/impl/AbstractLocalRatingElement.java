package com.ss.server.model.rating.local.impl;

import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.rating.impl.AbstractRatingElement;
import com.ss.server.model.rating.local.LocalRatingElement;
import com.ss.server.model.rating.local.LocalRatingElementVarType;

/**
 * Базовая реализация элемента локального рейтинга.
 * 
 * @author Ronn
 */
public abstract class AbstractLocalRatingElement<E extends LocalRatingEvent> extends AbstractRatingElement<E> implements LocalRatingElement<E> {

	@Override
	public void setVar(LocalRatingElementVarType varType, long value) {
	}

	@Override
	public long getLongVar(LocalRatingElementVarType varType) {
		return 0;
	}
}

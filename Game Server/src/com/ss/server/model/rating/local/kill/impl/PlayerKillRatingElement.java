package com.ss.server.model.rating.local.kill.impl;


/**
 * Реализация элемента рейтинга по убийстве игроков.
 * 
 * @author Ronn
 */
public abstract class PlayerKillRatingElement extends AbstractKillRatingElement {

	public static final int FEDERATION_ID = 1;
	public static final int HASTUR_ID = 2;
	public static final int FREEDOM_ID = 3;
}

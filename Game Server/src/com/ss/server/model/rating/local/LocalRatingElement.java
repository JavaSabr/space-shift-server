package com.ss.server.model.rating.local;

import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.event.local.LocalRatingEvent;

/**
 * Интерфейс для реализации элемента локального рейтинга.
 * 
 * @author Ronn
 */
public interface LocalRatingElement<E extends LocalRatingEvent> extends RatingElement<E> {

	/**
	 * Установка переменной элементу рейтинга.
	 * 
	 * @param varType тип переменной.
	 * @param value значение переменной.
	 */
	public void setVar(LocalRatingElementVarType varType, long value);

	/**
	 * Получение значения переменнойэ лемента.
	 * 
	 * @param varType тип переменной.
	 * @return значение переменной.
	 */
	public long getLongVar(LocalRatingElementVarType varType);

	@Override
	public default boolean isLocalRatingElement() {
		return true;
	}

	/**
	 * @return класс необходимых событий.
	 */
	public default Class<? extends LocalRatingEvent> getEventClass() {
		return LocalRatingEvent.class;
	}
}

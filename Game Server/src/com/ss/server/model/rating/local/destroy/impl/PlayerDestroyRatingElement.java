package com.ss.server.model.rating.local.destroy.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;

/**
 * Элемент рейтинга смертей от игрока.
 * 
 * @author Ronn
 */
public class PlayerDestroyRatingElement extends AbstractDestroyRatingElement<DestroyRatingEvent> {

	@Override
	public RatingElementType getElementType() {
		return DestroyRatingElementType.PLAYER;
	}

	@Override
	public void notify(DestroyRatingEvent event) {
		super.notify(event);

		final SpaceObject destroyer = event.getDestroyer();

		if(destroyer == null || !destroyer.isPlayerShip() || event.getDestroyed() == event.getDestroyer()) {
			return;
		}

		destroyCount.incrementAndGet();
		modify();
	}
}

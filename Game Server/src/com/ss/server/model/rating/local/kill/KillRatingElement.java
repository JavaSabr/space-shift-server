package com.ss.server.model.rating.local.kill;

import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.rating.local.LocalRatingElement;

/**
 * Интерфейс для реализации элемента рейтинга уничтожения врага.
 * 
 * @author Ronn
 */
public interface KillRatingElement<T extends KillRatingEvent> extends LocalRatingElement<T> {

	@Override
	public default Class<? extends LocalRatingEvent> getEventClass() {
		return KillRatingEvent.class;
	}

	/**
	 * @return кол-во убийств лазерным оружием.
	 */
	public long getLaserCount();

	/**
	 * @return кол-во убийств нейтронным оружием.
	 */
	public long getNeutronCount();

	/**
	 * @return кол-во убийств фотонным оружием.
	 */
	public long getPhotonCount();

	/**
	 * @return кол-во убийств плазматическим оружием.
	 */
	public long getPlasmaCount();

	/**
	 * @return кол-во убийств импульсным оружием.
	 */
	public long getPulseCount();

	/**
	 * @return кол-во убийств ракетным оружием.
	 */
	public long getRocketLayncherCount();

	/**
	 * @return кол-во убийств тахионным оружием.
	 */
	public long getTachyonCount();

	@Override
	public default boolean isKillRatingElement() {
		return true;
	}

	/**
	 * @param count кол-во убийств лазерным оружием.
	 */
	public void setLaserCount(long count);

	/**
	 * @param count кол-во убийств нейтронным оружием.
	 */
	public void setNeutronCount(long count);

	/**
	 * @param count кол-во убийств фотонным оружием.
	 */
	public void setPhotonCount(long count);

	/**
	 * @param count кол-во убийств плазматическим оружием.
	 */
	public void setPlasmaCount(long count);

	/**
	 * @param count кол-во убийств импульсным оружием.
	 */
	public void setPulseCount(long count);

	/**
	 * @param count кол-во убийств ракетным оружием.
	 */
	public void setRocketLauncherCount(long count);

	/**
	 * @param count кол-во убийств тахионным оружием.
	 */
	public void setTachyonCount(long count);
}

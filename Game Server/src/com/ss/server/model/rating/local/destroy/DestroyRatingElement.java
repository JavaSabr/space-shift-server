package com.ss.server.model.rating.local.destroy;

import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.local.LocalRatingElement;

/**
 * Интерфейс для реализации элементов рейтинга смерти.
 * 
 * @author Ronn
 */
public interface DestroyRatingElement<E extends DestroyRatingEvent> extends LocalRatingElement<E> {

	/**
	 * @return кол-во уничтожений.
	 */
	public long getDestroyCount();

	/**
	 * @param destroyCount кол-во уничтожений.
	 */
	public void setDestroyCount(long destroyCount);

	@Override
	public default boolean isDestroyRatingElement() {
		return true;
	}

	@Override
	public default Class<? extends LocalRatingEvent> getEventClass() {
		return DestroyRatingEvent.class;
	}
}

package com.ss.server.model.rating.local.module;

import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;
import com.ss.server.model.rating.local.LocalRatingElement;

/**
 * Интерфейс для реализации рейтинга модуля.
 * 
 * @author Ronn
 */
public interface ModuleRatingElement<E extends ModuleRatingEvent> extends LocalRatingElement<E> {

}

package com.ss.server.model.rating.local;

import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.local.impl.SpaceShipBodyRatingElement;

/**
 * Перечисление элементов локального рейтинга.
 * 
 * @author Ronn
 */
@SuppressWarnings("rawtypes")
public enum LocalRatingElementType implements RatingElementType {

	SPACE_SHIP_BODY(SpaceShipBodyRatingElement.class), ;

	public static final LocalRatingElementType[] TYPES = values();

	public static final int SIZE = TYPES.length;

	public static final LocalRatingElementType valueOf(int index) {
		return TYPES[index];
	}

	/** класс реализации элемента рейтинга */
	private Class<? extends LocalRatingElement> implementation;

	private LocalRatingElementType(final Class<? extends LocalRatingElement> implementation) {
		this.implementation = implementation;
	}

	@Override
	public Class<? extends RatingElement> getImplementation() {
		return implementation;
	}

	@Override
	public int getIndex() {
		return ordinal();
	}

	@Override
	public String getName() {
		return name();
	}
}

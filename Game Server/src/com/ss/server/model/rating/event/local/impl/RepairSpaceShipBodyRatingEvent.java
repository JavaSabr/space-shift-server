package com.ss.server.model.rating.event.local.impl;

/**
 * Событие рейтинга полного восстановления корапуса корабля.
 * 
 * @author Ronn
 */
public class RepairSpaceShipBodyRatingEvent extends AbstractLocalRatingEvent {

}

package com.ss.server.model.rating.event.local.destroy;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.ship.SpaceShip;

/**
 * Интерфейс для реализации события рейтинга смерти.
 * 
 * @author ronn
 *
 */
public interface DestroyRatingEvent extends LocalRatingEvent {

	/**
	 * @return тип урона, которым был уничтожен.
	 */
	public DamageType getDamageType();

	/**
	 * @return тот кого уничтожили.
	 */
	public SpaceShip getDestroyed();

	/**
	 * @return тот кто уничтожил.
	 */
	public SpaceObject getDestroyer();

	/**
	 * @param damageType тип урона, которым был уничтожен.
	 */
	public void setDamageType(DamageType damageType);

	/**
	 * @param destroyed тот кого уничтожили.
	 */
	public void setDestroyed(SpaceShip destroyed);

	/**
	 * @param destroyer тот кто уничтожил.
	 */
	public void setDestroyer(SpaceObject destroyer);
}

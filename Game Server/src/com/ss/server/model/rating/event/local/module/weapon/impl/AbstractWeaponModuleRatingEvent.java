package com.ss.server.model.rating.event.local.module.weapon.impl;

import com.ss.server.model.rating.event.local.module.impl.AbstractModuleRatingEvent;
import com.ss.server.model.rating.event.local.module.weapon.WeaponModuleRatingEvent;

/**
 * Базовая реализация события рейтинга модуля оружия.
 * 
 * @author Ronn
 */
public class AbstractWeaponModuleRatingEvent extends AbstractModuleRatingEvent implements WeaponModuleRatingEvent {

}

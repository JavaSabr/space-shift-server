package com.ss.server.model.rating.event.local.destroy.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.event.local.impl.AbstractLocalRatingEvent;
import com.ss.server.model.ship.SpaceShip;

/**
 * Базовая реализация события рейтинга уничтожения.
 * 
 * @author Ronn
 */
public class DestroyRatingEventImpl extends AbstractLocalRatingEvent implements DestroyRatingEvent {

	/** тот кто уничтожил */
	protected SpaceObject destroyer;
	/** тот кого уничтожили */
	protected SpaceShip destroyed;
	/** тип урона, которым был уничтожен */
	protected DamageType damageType;

	@Override
	public DamageType getDamageType() {
		return damageType;
	}

	@Override
	public SpaceShip getDestroyed() {
		return destroyed;
	}

	@Override
	public SpaceObject getDestroyer() {
		return destroyer;
	}

	@Override
	public void setDamageType(DamageType damageType) {
		this.damageType = damageType;
	}

	@Override
	public void setDestroyed(SpaceShip destroyed) {
		this.destroyed = destroyed;
	}

	@Override
	public void setDestroyer(SpaceObject destroyer) {
		this.destroyer = destroyer;
	}
}

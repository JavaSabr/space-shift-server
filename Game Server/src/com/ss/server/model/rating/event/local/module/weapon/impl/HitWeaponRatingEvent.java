package com.ss.server.model.rating.event.local.module.weapon.impl;

/**
 * Реализация события рейтинга об попадании оружем в цель.
 * 
 * @author Ronn
 */
public class HitWeaponRatingEvent extends AbstractWeaponModuleRatingEvent {

}

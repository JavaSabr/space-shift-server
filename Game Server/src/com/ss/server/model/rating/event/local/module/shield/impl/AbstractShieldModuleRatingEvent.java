package com.ss.server.model.rating.event.local.module.shield.impl;

import com.ss.server.model.rating.event.local.module.impl.AbstractModuleRatingEvent;
import com.ss.server.model.rating.event.local.module.shield.ShieldModuleRatingEvent;

/**
 * Базовая реализация события модуля щита.
 * 
 * @author Ronn
 */
public abstract class AbstractShieldModuleRatingEvent extends AbstractModuleRatingEvent implements ShieldModuleRatingEvent {

}

package com.ss.server.model.rating.event.local.module.weapon;

import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;

/**
 * Интерфейс для реализации события рейтинга модуля оружия.
 * 
 * @author Ronn
 */
public interface WeaponModuleRatingEvent extends ModuleRatingEvent {

}

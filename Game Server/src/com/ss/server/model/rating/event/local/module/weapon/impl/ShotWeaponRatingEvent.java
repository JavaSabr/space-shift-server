package com.ss.server.model.rating.event.local.module.weapon.impl;

/**
 * Реализация события рейтинга о произведении выстрела оружием.
 * 
 * @author Ronn
 */
public class ShotWeaponRatingEvent extends AbstractWeaponModuleRatingEvent {

}

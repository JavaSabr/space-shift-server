package com.ss.server.model.rating.event;

import com.ss.server.model.rating.RatingElementType;

/**
 * Интерфейс для реализации события влияющего на рейтинг.
 * 
 * @author Ronn
 */
public interface RatingEvent {

	/**
	 * @return тип элемента, к которому относится событие.
	 */
	public RatingElementType getElementType();

	/**
	 * @param elementType тип элемента, к которому относится событие.
	 */
	public void setElementType(RatingElementType elementType);
}

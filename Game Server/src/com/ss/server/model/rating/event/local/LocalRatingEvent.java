package com.ss.server.model.rating.event.local;

import com.ss.server.model.rating.event.RatingEvent;

/**
 * Интерфейс для реализации события относящегося к локальному рейтингу.
 * 
 * @author Ronn
 */
public interface LocalRatingEvent extends RatingEvent {

}

package com.ss.server.model.rating.event.local.module.weapon.impl;

/**
 * Реализация события рейтинга о истощении оружием.
 * 
 * @author Ronn
 */
public class DepletionWeaponRatingEvent extends AbstractWeaponModuleRatingEvent {

}

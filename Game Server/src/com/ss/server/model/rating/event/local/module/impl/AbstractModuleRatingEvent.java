package com.ss.server.model.rating.event.local.module.impl;

import com.ss.server.model.rating.event.local.impl.AbstractLocalRatingEvent;
import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;

/**
 * Базовая реализация события рейтинга модуля.
 * 
 * @author Rronn
 */
public class AbstractModuleRatingEvent extends AbstractLocalRatingEvent implements ModuleRatingEvent {

}

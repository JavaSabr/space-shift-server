package com.ss.server.model.rating.event;

import com.ss.server.LocalObjects;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.event.local.destroy.impl.DestroyRatingEventImpl;
import com.ss.server.model.rating.event.local.impl.HitSpaceShipBodyRatingEvent;
import com.ss.server.model.rating.event.local.impl.RepairSpaceShipBodyRatingEvent;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.rating.event.local.kill.impl.ShipKillRatingEvent;
import com.ss.server.model.rating.event.local.module.shield.impl.HitShieldRatingEvent;
import com.ss.server.model.rating.event.local.module.shield.impl.ReloadShieldRatingEvent;
import com.ss.server.model.rating.event.local.module.weapon.impl.DepletionWeaponRatingEvent;
import com.ss.server.model.rating.event.local.module.weapon.impl.HitWeaponRatingEvent;
import com.ss.server.model.rating.event.local.module.weapon.impl.ShotWeaponRatingEvent;
import com.ss.server.model.rating.local.LocalRatingElementType;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElementType;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElementType;

/**
 * Контейнер локальных для потока событий рейтинга.
 * 
 * @author Ronn
 */
public class RatingEvents {

	public static HitWeaponRatingEvent getHitWeaponRatingEvent(final LocalObjects local, final WeaponModuleRatingElementType elementType) {

		final RatingEvents events = local.getLocalRatingEvents();

		final HitWeaponRatingEvent event = events.hitWeaponRatingEvent;
		event.setElementType(elementType);

		return event;
	}

	public static ShotWeaponRatingEvent getShotWeaponRatingEvent(final LocalObjects local, final WeaponModuleRatingElementType elementType) {

		final RatingEvents events = local.getLocalRatingEvents();

		final ShotWeaponRatingEvent event = events.shotWeaponRatingEvent;
		event.setElementType(elementType);

		return event;
	}

	public static DepletionWeaponRatingEvent getDepletionWeaponRatingEvent(final LocalObjects local, final WeaponModuleRatingElementType elementType) {

		final RatingEvents events = local.getLocalRatingEvents();

		final DepletionWeaponRatingEvent event = events.depletionWeaponRatingEvent;
		event.setElementType(elementType);

		return event;
	}

	public static HitShieldRatingEvent getHitShieldRatingEvent(final LocalObjects local, final ShieldModuleRatingElementType elementType) {

		final RatingEvents events = local.getLocalRatingEvents();

		final HitShieldRatingEvent event = events.hitShieldRatingEvent;
		event.setElementType(elementType);

		return event;
	}

	public static ReloadShieldRatingEvent getReloadShieldRatingEvent(final LocalObjects local, final ShieldModuleRatingElementType elementType) {

		final RatingEvents events = local.getLocalRatingEvents();

		final ReloadShieldRatingEvent event = events.reloadShieldRatingEvent;
		event.setElementType(elementType);

		return event;
	}

	public static HitSpaceShipBodyRatingEvent getHitSpaceShipBodyRatingEvent(final LocalObjects local, final LocalRatingElementType elementType) {

		final RatingEvents events = local.getLocalRatingEvents();

		final HitSpaceShipBodyRatingEvent event = events.hitSpaceShipBodyRatingEvent;
		event.setElementType(elementType);

		return event;
	}

	public static RepairSpaceShipBodyRatingEvent getRepairSpaceShipBodyRatingEvent(final LocalObjects local, final LocalRatingElementType elementType) {

		final RatingEvents events = local.getLocalRatingEvents();

		final RepairSpaceShipBodyRatingEvent event = events.repairSpaceShipBodyRatingEvent;
		event.setElementType(elementType);

		return event;
	}

	public static DestroyRatingEvent getDestroyRatingEvent(final LocalObjects local) {
		final RatingEvents events = local.getLocalRatingEvents();
		return events.destroyRatingEvent;
	}

	public static KillRatingEvent getKillRatingEvent(final LocalObjects local) {
		final RatingEvents events = local.getLocalRatingEvents();
		return events.shipKillRatingEvent;
	}

	/** событие выстрела оружием */
	private final ShotWeaponRatingEvent shotWeaponRatingEvent;
	/** событие попадания оружием */
	private final HitWeaponRatingEvent hitWeaponRatingEvent;
	/** событие о итстощении оружия */
	private final DepletionWeaponRatingEvent depletionWeaponRatingEvent;

	/** событие поподания в щит */
	private final HitShieldRatingEvent hitShieldRatingEvent;
	/** событие восстановления щита */
	private final ReloadShieldRatingEvent reloadShieldRatingEvent;

	/** событие попадания в корпус корабля */
	private final HitSpaceShipBodyRatingEvent hitSpaceShipBodyRatingEvent;
	/** события полного восстановления корабля */
	private final RepairSpaceShipBodyRatingEvent repairSpaceShipBodyRatingEvent;

	/** события рейтинга по уничтожениям */
	private final DestroyRatingEventImpl destroyRatingEvent;
	/** событий рейтинга по убийствам */
	private final ShipKillRatingEvent shipKillRatingEvent;

	public RatingEvents() {
		this.shotWeaponRatingEvent = new ShotWeaponRatingEvent();
		this.hitWeaponRatingEvent = new HitWeaponRatingEvent();
		this.depletionWeaponRatingEvent = new DepletionWeaponRatingEvent();

		this.hitShieldRatingEvent = new HitShieldRatingEvent();
		this.reloadShieldRatingEvent = new ReloadShieldRatingEvent();

		this.hitSpaceShipBodyRatingEvent = new HitSpaceShipBodyRatingEvent();
		this.repairSpaceShipBodyRatingEvent = new RepairSpaceShipBodyRatingEvent();

		this.destroyRatingEvent = new DestroyRatingEventImpl();
		this.shipKillRatingEvent = new ShipKillRatingEvent();
	}
}

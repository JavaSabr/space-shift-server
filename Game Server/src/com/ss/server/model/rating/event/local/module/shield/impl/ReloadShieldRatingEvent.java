package com.ss.server.model.rating.event.local.module.shield.impl;

/**
 * Событие рейтинга о восстановлении щита.
 * 
 * @author Ronn
 */
public class ReloadShieldRatingEvent extends AbstractShieldModuleRatingEvent {

}

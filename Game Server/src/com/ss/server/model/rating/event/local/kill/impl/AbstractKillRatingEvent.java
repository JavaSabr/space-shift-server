package com.ss.server.model.rating.event.local.kill.impl;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.event.local.impl.AbstractLocalRatingEvent;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.ship.SpaceShip;

/**
 * Базовая реализация события рейтинга убийств.
 * 
 * @author Ronn
 */
public abstract class AbstractKillRatingEvent extends AbstractLocalRatingEvent implements KillRatingEvent {

	/** убийца */
	protected SpaceShip killer;
	/** убитый */
	protected SpaceObject killed;
	/** тип урона */
	protected DamageType damageType;

	@Override
	public DamageType getDamageType() {
		return damageType;
	}

	@Override
	public SpaceObject getKilled() {
		return killed;
	}

	@Override
	public SpaceShip getKiller() {
		return killer;
	}

	@Override
	public void setDamageType(DamageType damageType) {
		this.damageType = damageType;
	}

	@Override
	public void setKilled(SpaceObject killed) {
		this.killed = killed;
	}

	@Override
	public void setKiller(SpaceShip killer) {
		this.killer = killer;
	}
}

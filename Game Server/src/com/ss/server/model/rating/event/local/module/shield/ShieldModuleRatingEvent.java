package com.ss.server.model.rating.event.local.module.shield;

import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;

/**
 * Интерфейс для реализации события рейтинга щита.
 * 
 * @author Ronn
 */
public interface ShieldModuleRatingEvent extends ModuleRatingEvent {

}

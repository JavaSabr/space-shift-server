package com.ss.server.model.rating.event.local.kill;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.ship.SpaceShip;

/**
 * События рейтинга о убийстве врага.
 * 
 * @author Ronn
 */
public interface KillRatingEvent extends LocalRatingEvent {

	/**
	 * @return тип урона.
	 */
	public DamageType getDamageType();

	/**
	 * @return убитый.
	 */
	public SpaceObject getKilled();

	/**
	 * @return убийца.
	 */
	public SpaceShip getKiller();

	/**
	 * @param damageType тип урона.
	 */
	public void setDamageType(DamageType damageType);

	/**
	 * @param killed убитый.
	 */
	public void setKilled(SpaceObject killed);

	/**
	 * @param killer убийца.
	 */
	public void setKiller(SpaceShip killer);
}

package com.ss.server.model.rating.event.local.module;

import com.ss.server.model.rating.event.local.LocalRatingEvent;

/**
 * Интерфейс для реализации события относящегося к рейтингу модулей.
 * 
 * @author Ronn
 */
public interface ModuleRatingEvent extends LocalRatingEvent {

}

package com.ss.server.model.rating.event.local.impl;

import com.ss.server.model.rating.event.impl.AbstractRatingEvent;
import com.ss.server.model.rating.event.local.LocalRatingEvent;

/**
 * Базовая реализация события локального рейтинга.
 * 
 * @author Ronn
 */
public class AbstractLocalRatingEvent extends AbstractRatingEvent implements LocalRatingEvent {

}

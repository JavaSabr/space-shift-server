package com.ss.server.model.rating.event.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.event.RatingEvent;

/**
 * Базовая реализация события рейтинга.
 * 
 * @author Ronn
 */
public class AbstractRatingEvent implements RatingEvent {

	protected static final Logger LOGGER = LoggerManager.getLogger(RatingEvent.class);

	/** тип элемента, к которому относится событие */
	private RatingElementType elementType;

	@Override
	public RatingElementType getElementType() {
		return elementType;
	}

	@Override
	public void setElementType(final RatingElementType elementType) {
		this.elementType = elementType;
	}
}

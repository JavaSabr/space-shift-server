package com.ss.server.model.rating;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.rating.local.LocalRatingElementType;
import com.ss.server.model.rating.local.destroy.DestroyRatingElementType;
import com.ss.server.model.rating.local.kill.KillRatingElementType;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElementType;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElementType;

/**
 * Перечисление наборов типов элементов рейтинга.
 * 
 * @author Ronn
 */
public enum RatingElementTypes {

	/** набор локальных рейтингов */
	LOCAL_RATING_ELEMENTS(LocalRatingElementType.TYPES, DestroyRatingElementType.TYPES, KillRatingElementType.TYPES),
	/** раздле локального рейтинга модулей */
	MODULE_RATING_ELEMETS(WeaponModuleRatingElementType.TYPES, ShieldModuleRatingElementType.TYPES);

	public static final RatingElementTypes[] VALUES = values();

	/** список типов соотвествующего раздела рейтинга */
	private final RatingElementType[] types;

	private RatingElementTypes(final RatingElementType[]... types) {

		final Array<RatingElementType> result = ArrayFactory.newArray(RatingElementType.class);

		for(final RatingElementType[] elementTypes : types) {
			result.addAll(elementTypes);
		}

		this.types = result.toArray(new RatingElementType[result.size()]);
	}

	/**
	 * @return набор типов элементов рейтинга.
	 */
	public RatingElementType[] getTypes() {
		return types;
	}
}

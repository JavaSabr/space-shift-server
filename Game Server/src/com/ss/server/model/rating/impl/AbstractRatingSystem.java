package com.ss.server.model.rating.impl;

import static com.ss.server.model.rating.RatingElementTypes.LOCAL_RATING_ELEMENTS;
import static com.ss.server.model.rating.RatingElementTypes.MODULE_RATING_ELEMETS;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.RatingElementType;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvent;
import com.ss.server.model.rating.event.local.LocalRatingEvent;
import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;
import com.ss.server.model.rating.local.LocalRatingElement;
import com.ss.server.model.rating.local.module.ModuleRatingElement;
import com.ss.server.model.ship.SpaceShip;

/**
 * Базовая реализация рейтинговой системы.
 * 
 * @author Ronn
 */
public abstract class AbstractRatingSystem implements RatingSystem {

	/** список элементов рейтинга модулей системы */
	private final ModuleRatingElement<? super ModuleRatingEvent>[] moduleRatingElements;
	/** список элементов локального рейтинга */
	private final LocalRatingElement<? super LocalRatingEvent>[] localRatingElements;

	/** список всех элементов рейтинга */
	private final RatingElement<? super RatingEvent>[] ratingElements;

	/** корабль, за которым закреплен рейтинг */
	private final SpaceShip ship;

	public AbstractRatingSystem(SpaceShip ship) {
		this.ship = ship;
		this.localRatingElements = createElements(LocalRatingElement.class, LOCAL_RATING_ELEMENTS.getTypes());
		this.moduleRatingElements = createElements(ModuleRatingElement.class, MODULE_RATING_ELEMETS.getTypes());
		this.ratingElements = createRatingElements();
	}

	@Override
	public SpaceShip getSpaceShip() {
		return ship;
	}

	@SuppressWarnings("unchecked")
	private <T> T createElements(final Class<?> baseClass, final RatingElementType[] types) {

		final Array<RatingElement<? super RatingEvent>> elements = ArrayFactory.newArray(baseClass);

		for(final RatingElementType type : types) {
			elements.add(type.create());
		}

		elements.trimToSize();
		return (T) elements.array();
	}

	@SuppressWarnings("unchecked")
	private RatingElement<? super RatingEvent>[] createRatingElements() {

		final Array<RatingElement<? extends RatingEvent>> container = ArrayFactory.newArray(RatingElement.class);

		for(final ModuleRatingElement<? super ModuleRatingEvent> element : moduleRatingElements) {
			container.add(element);
		}

		for(final LocalRatingElement<? super LocalRatingEvent> element : localRatingElements) {
			container.add(element);
		}

		return container.toArray(new RatingElement[container.size()]);
	}

	@Override
	public void finalyze() {

		final ModuleRatingElement<? super ModuleRatingEvent>[] moduleRatingElements = getModuleRatingElements();

		for(final ModuleRatingElement<? super ModuleRatingEvent> element : moduleRatingElements) {
			element.finalyze();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends RatingElement<? extends RatingEvent>> T findElement(final RatingElementType type) {

		for(final RatingElement<? super RatingEvent> element : getRatingElements()) {
			if(element.getElementType() == type) {
				return (T) element;
			}
		}

		return null;
	}

	/**
	 * @return список элементов рейтинга модулей системы.
	 */
	protected ModuleRatingElement<? super ModuleRatingEvent>[] getModuleRatingElements() {
		return moduleRatingElements;
	}

	/**
	 * @return список элементов локального рейтинга.
	 */
	protected LocalRatingElement<? super LocalRatingEvent>[] getLocalRatingElements() {
		return localRatingElements;
	}

	@Override
	public RatingElement<? super RatingEvent>[] getRatingElements() {
		return ratingElements;
	}

	@Override
	public void notify(final ModuleRatingEvent event) {

		for(final ModuleRatingElement<? super ModuleRatingEvent> element : getModuleRatingElements()) {

			if(element.getElementType() != event.getElementType()) {
				continue;
			}

			element.notify(event);
		}
	}

	@Override
	public void notify(LocalRatingEvent event) {

		final Class<? extends LocalRatingEvent> eventClass = event.getClass();

		for(final LocalRatingElement<? super LocalRatingEvent> element : getLocalRatingElements()) {

			final RatingElementType elementType = event.getElementType();

			if(elementType != null && element.getElementType() != elementType) {
				continue;
			}

			if(!element.getEventClass().isAssignableFrom(eventClass)) {
				continue;
			}

			element.notify(event);
		}
	}
}

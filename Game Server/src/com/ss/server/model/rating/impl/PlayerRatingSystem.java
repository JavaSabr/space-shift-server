package com.ss.server.model.rating.impl;

import com.ss.server.database.RatingDBManager;
import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.event.RatingEvent;
import com.ss.server.model.rating.local.LocalRatingElement;
import com.ss.server.model.rating.local.destroy.DestroyRatingElement;
import com.ss.server.model.rating.local.kill.KillRatingElement;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElement;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElement;
import com.ss.server.model.ship.SpaceShip;

/**
 * Реализация системы рейтинга для игрока.
 * 
 * @author Ronn
 */
public class PlayerRatingSystem extends AbstractRatingSystem {

	private static final RatingDBManager RATING_DB_MANAGER = RatingDBManager.getInstance();

	public PlayerRatingSystem(SpaceShip ship) {
		super(ship);
	}

	@Override
	public void saveChanges() {
		super.saveChanges();

		final SpaceShip ship = getSpaceShip();

		final RatingElement<? super RatingEvent>[] ratingElements = getRatingElements();

		for(RatingElement<? super RatingEvent> ratingElement : ratingElements) {

			if(!ratingElement.hasChanges()) {
				continue;
			}

			if(ratingElement.isWeaponRatingElement()) {
				RATING_DB_MANAGER.updateWeaponRating(ship, (WeaponModuleRatingElement<?>) ratingElement);
			} else if(ratingElement.isShieldRatingElement()) {
				RATING_DB_MANAGER.updateShieldRating(ship, (ShieldModuleRatingElement<?>) ratingElement);
			} else if(ratingElement.isDestroyRatingElement()) {
				RATING_DB_MANAGER.updateDestroyRating(ship, (DestroyRatingElement<?>) ratingElement);
			} else if(ratingElement.isKillRatingElement()) {
				RATING_DB_MANAGER.updateKillRating(ship, (KillRatingElement<?>) ratingElement);
			} else if(ratingElement.isLocalRatingElement()) {
				RATING_DB_MANAGER.updateLocalRating(ship, (LocalRatingElement<?>) ratingElement);
			}

			ratingElement.resetChanges();
		}
	}

	@Override
	public void init() {
		super.init();

		final SpaceShip ship = getSpaceShip();

		final RatingElement<? super RatingEvent>[] ratingElements = getRatingElements();

		for(RatingElement<? super RatingEvent> ratingElement : ratingElements) {

			if(ratingElement.isWeaponRatingElement()) {
				RATING_DB_MANAGER.addNewWeaponRating(ship, (WeaponModuleRatingElement<?>) ratingElement);
			} else if(ratingElement.isShieldRatingElement()) {
				RATING_DB_MANAGER.addNewShieldRating(ship, (ShieldModuleRatingElement<?>) ratingElement);
			} else if(ratingElement.isKillRatingElement()) {
				RATING_DB_MANAGER.addNewKillRating(ship, (KillRatingElement<?>) ratingElement);
			} else if(ratingElement.isDestroyRatingElement()) {
				RATING_DB_MANAGER.addNewDestroyRating(ship, (DestroyRatingElement<?>) ratingElement);
			} else if(ratingElement.isLocalRatingElement()) {
				RATING_DB_MANAGER.addNewLocalRating(ship, (LocalRatingElement<?>) ratingElement);
			}
		}
	}
}

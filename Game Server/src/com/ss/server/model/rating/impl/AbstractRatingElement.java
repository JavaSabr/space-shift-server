package com.ss.server.model.rating.impl;

import java.util.concurrent.atomic.AtomicLong;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.model.rating.RatingElement;
import com.ss.server.model.rating.event.RatingEvent;

/**
 * Базовая реализация элемента рейтинга.
 * 
 * @author Ronn
 */
public abstract class AbstractRatingElement<E extends RatingEvent> implements RatingElement<E> {

	protected static final Logger LOGGER = LoggerManager.getLogger(RatingElement.class);

	/** счетчик изменений рейтинга */
	protected final AtomicLong modifyCount;

	public AbstractRatingElement() {
		this.modifyCount = new AtomicLong();
	}

	/**
	 * Пометка об изменении рейтинга.
	 */
	protected void modify() {
		modifyCount.incrementAndGet();
	}

	@Override
	public void finalyze() {
		modifyCount.getAndSet(0);
	}

	@Override
	public boolean check(final E event) {
		return false;
	}

	@Override
	public void notify(final E event) {
	}

	@Override
	public boolean hasChanges() {
		return modifyCount.get() > 0;
	}

	@Override
	public void resetChanges() {
		modifyCount.getAndSet(0);
	}
}

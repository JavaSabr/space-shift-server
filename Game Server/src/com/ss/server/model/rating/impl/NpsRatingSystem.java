package com.ss.server.model.rating.impl;

import com.ss.server.model.rating.event.local.module.ModuleRatingEvent;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.nps.Nps;

/**
 * Реализация рейтинговой системы для {@link Nps}.
 * 
 * @author Ronn
 */
public class NpsRatingSystem extends AbstractRatingSystem {

	public NpsRatingSystem(SpaceShip ship) {
		super(ship);
	}

	@Override
	public void notify(ModuleRatingEvent event) {
	}
}

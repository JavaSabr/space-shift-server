package com.ss.server.model.rating;

import rlib.util.pools.Foldable;

import com.ss.server.model.rating.event.RatingEvent;
import com.ss.server.model.rating.local.LocalRatingElement;
import com.ss.server.model.rating.local.destroy.DestroyRatingElement;
import com.ss.server.model.rating.local.kill.KillRatingElement;
import com.ss.server.model.rating.local.module.shield.ShieldModuleRatingElement;
import com.ss.server.model.rating.local.module.weapon.WeaponModuleRatingElement;

/**
 * Интерфейс для реализации элемента рейтинга.
 * 
 * @author Ronn
 */
public interface RatingElement<E extends RatingEvent> extends Foldable {

	/**
	 * Проверка отношения события к этому элементу рейтинга.
	 * 
	 * @param event событие влияющиее на рейтинг.
	 * @return относится ли событие к этому элементу.
	 */
	public boolean check(E event);

	/**
	 * @return тип элемента.
	 */
	public RatingElementType getElementType();

	/**
	 * Уведомление об событии связанного с рейтингом.
	 * 
	 * @param event событие.
	 */
	public void notify(E event);

	/**
	 * @return был ли изменен рейтинг с момента инициализации.
	 */
	public boolean hasChanges();

	/**
	 * Сбрасывание счетчика изменений.
	 */
	public void resetChanges();

	/**
	 * @return является ли этот элемент {@link WeaponModuleRatingElement}.
	 */
	public default boolean isWeaponRatingElement() {
		return false;
	}

	/**
	 * @return является ли этот элемент {@link ShieldModuleRatingElement}.
	 */
	public default boolean isShieldRatingElement() {
		return false;
	}

	/**
	 * @return является ли этот элемент {@link LocalRatingElement}.
	 */
	public default boolean isLocalRatingElement() {
		return false;
	}

	/**
	 * @return является ли этот элемент {@link DestroyRatingElement}.
	 */
	public default boolean isDestroyRatingElement() {
		return false;
	}

	/**
	 * @return является ли этот элемент {@link KillRatingElement}.
	 */
	public default boolean isKillRatingElement() {
		return false;
	}
}

package com.ss.server.model.rating;

import rlib.util.ClassUtils;

import com.ss.server.model.rating.event.RatingEvent;

/**
 * Интерфейс для реализации типа элемента рейтинга.
 * 
 * @author Ronn
 */
@SuppressWarnings("rawtypes")
public interface RatingElementType {

	/**
	 * @return создание нового элемента.
	 */
	public default <T extends RatingElement<? super RatingEvent>> T create() {
		return ClassUtils.newInstance(getImplementation());
	}

	/**
	 * @return класс реализации.
	 */
	public Class<? extends RatingElement> getImplementation();

	/**
	 * @return индекс типа.
	 */
	public int getIndex();

	/**
	 * @return название типа.
	 */
	public String getName();
}

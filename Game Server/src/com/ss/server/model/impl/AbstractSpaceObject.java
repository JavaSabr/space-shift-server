package com.ss.server.model.impl;

import java.util.concurrent.locks.Lock;

import rlib.concurrent.lock.LockFactory;
import rlib.geom.DirectionType;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.array.Array;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.manager.ExecutorManager;
import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.FriendStatus;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ai.AI;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.impact.ImpactType;
import com.ss.server.model.impact.handler.ImpactHandler;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.teleport.TeleportHandler;
import com.ss.server.model.teleport.TeleportObjectTask;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseDebugObjectPosition;
import com.ss.server.network.game.packet.server.ResponseDebugObjectRotation;
import com.ss.server.network.game.packet.server.ResponseObjectDestructed;
import com.ss.server.template.ObjectTemplate;

/**
 * Базовая модель космического объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceObject<T extends ObjectTemplate> implements SpaceObject {

	protected static final Logger LOGGER = LoggerManager.getLogger(SpaceObject.class);
	protected static final Space SPACE = Space.getInstance();

	/** список обработчиков воздействий на объект */
	protected final ImpactHandler[] impactHandlers;

	/** задача по телепортированию объекта */
	protected final TeleportObjectTask teleportTask;

	/** положение объекта */
	protected final Vector location;
	/** направление вперед объекта */
	protected final Vector direction;

	/** направление объекта в пространстве */
	protected final Rotation rotation;
	/** форма объекта */
	protected final Bounding bounding;

	/** синхронизатор удаления объекта */
	protected final Lock deleteLock;

	/** темплейт объекта */
	protected volatile T template;

	/** текущий сектор, в котором находится объект */
	protected volatile SpaceSector currentSector;

	/** АИ объекта */
	protected volatile AI ai;

	/** уникальный ид объекта */
	protected volatile int objectId;
	/** ид локации */
	protected volatile int locationId;

	/** видимый ли объект */
	protected volatile boolean visible;
	/** был ли объект удален */
	protected volatile boolean deleted;

	public AbstractSpaceObject(final int objectId, final T template) {
		this.objectId = objectId;
		this.template = template;
		this.location = Vector.newInstance();
		this.rotation = Rotation.newInstance();
		this.direction = Vector.newInstance();
		this.bounding = template.newBounding(location);
		this.impactHandlers = new ImpactHandler[ImpactType.SIZE];
		this.teleportTask = new TeleportObjectTask(this);
		this.deleteLock = LockFactory.newReentrantAtomicLock();

		initImpactHandlers(getImpactHandlers());
	}

	@Override
	public void addMe(final PlayerShip playerShip, final LocalObjects local) {
	}

	@Override
	public void addQuestButtons(final Array<QuestButton> container, final PlayerShip playerShip) {
		addQuestButtons(container, playerShip, LocalObjects.get());
	}

	@Override
	public void addQuestButtons(final Array<QuestButton> container, final PlayerShip playerShip, final LocalObjects local) {
	}

	@Override
	public void applyImpact(final SpaceObject effector, final Skill skill, final ImpactInfo impactInfo, final LocalObjects local) {

		final ImpactType impactType = impactInfo.getImpactType();

		if(impactType == null) {
			LOGGER.warning(new Exception("not found impact type"));
			return;
		}

		final ImpactHandler handler = getImpactHandlers()[impactType.ordinal()];

		if(handler == null) {
			LOGGER.warning(new Exception("not found ImpactHandler for " + impactType));
			return;
		}

		handler.handle(effector, this, skill, impactInfo, local);
	}

	@Override
	public void broadcastGlobalPacket(final ServerPacket packet) {
		SPACE.broadcastGlobalPacket(packet, this);
	}

	@Override
	public void broadcastLocationPacket(final ServerPacket packet) {
		SPACE.broadcastLocationPacket(packet, this);
	}

	@Override
	public void broadcastPacket(final ServerPacket packet) {
		SPACE.broadcastPacket(packet, this);
	}

	@Override
	public boolean canAttack(final SpaceObject object) {
		return !object.isInvul();
	}

	@Override
	public boolean canImpact(final ImpactType impactType) {
		return getImpactHandlers()[impactType.ordinal()] != null;
	}

	@Override
	public void decayMe(final LocalObjects local) {
		setVisible(false);
		SPACE.removeVisibleObject(this, false, local);
	}

	@Override
	public void deleteLock() {
		deleteLock.lock();
	}

	@Override
	public void deleteMe(final LocalObjects local) {
		deleteLock();
		try {

			if(isDeleted()) {
				return;
			}

			deleteMeImpl(local);

		} finally {
			deleteUnlock();
		}
	}

	/**
	 * Реализация процесса удаления объекта из мира.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	protected void deleteMeImpl(final LocalObjects local) {

		// удаляем из видимого мира
		decayMe(local);

		// помечаем что объект был удален
		setDeleted(true);

		// сохраняем в шаблон для переиспользования
		final T template = getTemplate();
		template.putInstance(this);
	}

	@Override
	public void deleteUnlock() {
		deleteLock.unlock();
	}

	@Override
	public float distanceSquaredTo(final Vector location) {
		return getLocation().distanceSquared(location);
	}

	@Override
	public float distanceTo(final SpaceObject object) {
		return getLocation().distance(object.getLocation());
	}

	@Override
	public float distanceTo(final Vector location) {
		return getLocation().distance(location);
	}

	@Override
	public void doDestruct(final SpaceObject destroyer, final LocalObjects local) {

		if(isNeedSendPacket()) {
			broadcastPacket(ResponseObjectDestructed.getInstance(this, local));
		}

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.notifyDestroyed(this, destroyer, local);
	}

	@Override
	public void doUpdate(final LocalObjects local, final long currentTime) {
		teleportTask.update(local, currentTime);
	}

	@Override
	public AI getAI() {
		return ai;
	}

	@Override
	public Bounding getBounding() {
		return bounding;
	}

	@Override
	public SpaceSector getCurrentSector() {
		return currentSector;
	}

	@Override
	public Vector getDirection() {
		return direction;
	}

	@Override
	public FriendStatus getFriendStatus(final PlayerShip ship, final LocalObjects local) {
		return FriendStatus.NEUTRAL;
	}

	/**
	 * @return список обработчиков воздействия.
	 */
	protected ImpactHandler[] getImpactHandlers() {
		return impactHandlers;
	}

	@Override
	public Vector getLocation() {
		return location;
	}

	@Override
	public int getLocationId() {
		return locationId;
	}

	@Override
	public int getMaxSize() {
		return template.getMaxSize();
	}

	@Override
	public int getMinSize() {
		return template.getMinSize();
	}

	@Override
	public final int getObjectId() {
		return objectId;
	}

	@Override
	public Rotation getRotation() {
		return rotation;
	}

	@Override
	public final int getSizeX() {
		return template.getSizeX();
	}

	@Override
	public final int getSizeY() {
		return template.getSizeY();
	}

	@Override
	public final int getSizeZ() {
		return template.getSizeZ();
	}

	@Override
	public T getTemplate() {
		return template;
	}

	@Override
	public int getTemplateId() {
		return template.getId();
	}

	/**
	 * Инициализация обработчиков воздействия.
	 */
	protected void initImpactHandlers(final ImpactHandler[] impactHandlers) {
	}

	@Override
	public void invalidCoords(final LocalObjects local) {

		LOGGER.warning(this, "invlid coords from " + this);

		final ExecutorManager executorManager = ExecutorManager.getInstance();
		executorManager.execute(() -> {
			deleteMe(local);
		});
	}

	@Override
	public boolean isCollision(final SpaceObject target, final VectorBuffer buffer) {
		return bounding.intersects(target.getBounding(), buffer);
	}

	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setCurrentSector(final SpaceSector currentSector) {
		this.currentSector = currentSector;

		if(currentSector != null) {
			setLocationId(currentSector.getLocationId());
		}
	}

	/**
	 * @param deleted был ли объект удален.
	 */
	public void setDeleted(final boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public void setLocation(final Vector vector, final Vector changed, final LocalObjects local) {

		if(location != vector) {
			location.set(vector);
		}

		SPACE.addVisibleObject(this, false, local);
	}

	@Override
	public void setLocationId(final int locationId) {
		this.locationId = locationId;
	}

	@Override
	public void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	@Override
	public void setRotation(final Rotation newRotation, final LocalObjects local) {

		if(rotation != newRotation) {
			rotation.set(newRotation);
		}

		newRotation.getVectorDirection(DirectionType.DIRECTION, direction);

		bounding.update(newRotation, local);
	}

	@Override
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}

	@Override
	public void spawnMe(final LocalObjects local) {
		setVisible(true);
		setDeleted(false);
		setLocation(getLocation(), Vector.ZERO, local);
	}

	@Override
	public void spawnMe(final LocalObjects local, final float x, final float y, final float z) {
		location.setXYZ(x, y, z);
		spawnMe(local);
	}

	@Override
	public void startTeleport(final TeleportHandler handler, final LocalObjects local) {
		teleportTask.startTeleport(handler, local);
	}

	@Override
	public void syncFor(final SpaceObject object, final LocalObjects local) {

		if(Config.DEVELOPER_DEBUG_SYNCHRONIZE) {
			object.sendPacket(ResponseDebugObjectPosition.getInstance(this, location), true);
			object.sendPacket(ResponseDebugObjectRotation.getInstance(this, rotation), true);
		}
	}

	@Override
	public void teleportTo(final Vector position, final Rotation rotation, final LocalObjects local, final int locationId) {

		boolean onlyLocalObjects = getLocationId() == locationId;

		setVisible(false);
		LOGGER.info("===========================remove object in space=============================");
		SPACE.removeVisibleObject(this, onlyLocalObjects, local);

		setCurrentSector(null);
		getLocation().set(position);
		getRotation().set(rotation);
		setLocationId(locationId);

		setVisible(true);
		LOGGER.info("===========================add object to space=============================");
		SPACE.addVisibleObject(this, onlyLocalObjects, local);
	}

	@Override
	public void reinit() {
		setDeleted(false);
	}

	@Override
	public void finalyze() {
		setCurrentSector(null);
	}
}

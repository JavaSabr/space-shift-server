package com.ss.server.model.impl;

import com.ss.server.model.ship.SpaceShip;

/**
 * Контейнер параметров для различных методов.
 * 
 * @author Ronn
 */
public final class Env {

	/** корабль */
	public SpaceShip owner;
	/** целевой корабль */
	public SpaceShip target;

	/** значение */
	public float value;

	/**
	 * @return очищенный класс.
	 */
	public Env clear() {
		owner = null;
		target = null;
		return this;
	}

	public void setOwner(final SpaceShip owner) {
		this.owner = owner;
	}
}

package com.ss.server.model.impl;

import java.util.concurrent.ScheduledFuture;

import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ArrayUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.executor.GameExecutor;
import com.ss.server.executor.impl.UpdateNpsAIExecutor;
import com.ss.server.executor.impl.UpdateObjectExecutor;
import com.ss.server.executor.impl.UpdateShotExecutor;
import com.ss.server.manager.CollisionManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.module.external.ExternalStationModule;
import com.ss.server.task.impl.UpdateNpsAITask;
import com.ss.server.task.impl.UpdateShotTask;
import com.ss.server.task.impl.UpdateSpaceObjectTask;

/**
 * Реализация космической локации с 3х мерным массивом космических векторов.
 * 
 * @author Ronn
 */
public final class SpaceLocation {

	private static final Logger LOGGER = LoggerManager.getLogger(SpaceLocation.class);

	/** размеры одной локации */
	private static final int LOC_MIN_X = -100_000;
	private static final int LOC_MAX_X = 100_000;
	private static final int LOC_MIN_Y = -100_000;
	private static final int LOC_MAX_Y = 100_000;
	private static final int LOC_MIN_Z = -100_000;
	private static final int LOC_MAX_Z = 100_000;

	/** размер секторов */
	private static final int SECTOR_SIZE = 2_000;

	/** рассчет смещения */
	private static final int OFFSET_X = Math.abs(LOC_MIN_X / SECTOR_SIZE);
	private static final int OFFSET_Y = Math.abs(LOC_MIN_Y / SECTOR_SIZE);
	private static final int OFFSET_Z = Math.abs(LOC_MIN_Z / SECTOR_SIZE);

	/** кол-во секторов в локации */
	public static final int SECTORS_X = LOC_MAX_X / SECTOR_SIZE + OFFSET_X;
	public static final int SECTORS_Y = LOC_MAX_Y / SECTOR_SIZE + OFFSET_Y;
	public static final int SECTORS_Z = LOC_MAX_Z / SECTOR_SIZE + OFFSET_Z;

	/** массив всех секторов космоса */
	private final SpaceSector[][][] sectors;

	/** активные сектора */
	private final Array<SpaceSector> active;
	/** список гравитационных объектов в локации */
	private final Array<GravityObject> gravity;
	/** список локационных объектов в локации */
	private final Array<LocationObject> locationObjects;

	/** исполнитель выстрелов в рамках локации */
	private final GameExecutor<UpdateShotTask> shotExecutor;
	/** исполнитель обновлений объектов в локации */
	private final GameExecutor<UpdateSpaceObjectTask> objectExecutor;
	/** исполнитель обновления AI NPS */
	private final GameExecutor<UpdateNpsAITask> aiExecutor;

	/** название локации */
	private final String name;

	/** ид локации */
	private final int id;

	/** минимальные сектора валидные для локации */
	private final int sectorMinX;
	private final int sectorMinY;
	private final int sectorMinZ;

	/** максимальные сектора валидные для локации */
	private final int sectorMaxX;
	private final int sectorMaxY;
	private final int sectorMaxZ;

	/** ссылка на исполнение задания */
	private volatile ScheduledFuture<SpaceLocation> schedule;

	public SpaceLocation(final String name, final int id, final int sectorsX, final int sectorsY, final int sectorsZ) {
		this.shotExecutor = new UpdateShotExecutor(String.valueOf(id));
		this.objectExecutor = new UpdateObjectExecutor(String.valueOf(id));
		this.aiExecutor = new UpdateNpsAIExecutor(String.valueOf(id));
		this.name = name;
		this.id = id;
		this.gravity = ArrayFactory.newArray(GravityObject.class);
		this.locationObjects = ArrayFactory.newArray(LocationObject.class);
		this.active = ArrayFactory.newConcurrentAtomicArray(SpaceSector.class);
		this.sectors = new SpaceSector[SECTORS_X + 1][SECTORS_Y + 1][SECTORS_Z + 1];

		this.sectorMinX = SECTORS_X / 2 - sectorsX / 2;
		this.sectorMinY = SECTORS_Y / 2 - sectorsY / 2;
		this.sectorMinZ = SECTORS_Z / 2 - sectorsZ / 2;
		this.sectorMaxX = SECTORS_X / 2 + sectorsX / 2;
		this.sectorMaxY = SECTORS_Y / 2 + sectorsY / 2;
		this.sectorMaxZ = SECTORS_Z / 2 + sectorsZ / 2;
	}

	/**
	 * @return список локационных объектов в локации.
	 */
	public Array<LocationObject> getLocationObjects() {
		return locationObjects;
	}

	/**
	 * @param sector добавляемый активный сектор.
	 */
	public void addActiveSector(final SpaceSector sector) {

		final Array<SpaceSector> active = getActive();
		active.writeLock();
		try {

			active.add(sector);

			if(active.size() > 0 && schedule == null) {
				// TODO если понадобится, запустить задачу по обновлению грави
				// объектов
			}

		} finally {
			active.writeUnlock();
		}
	}

	/**
	 * Сбор станций в локации с доступным для ирока функционалом.
	 * 
	 * @param container контейнер станций.
	 * @param playerShip корабль игрока.
	 * @param functionClass класс функции.
	 * @param local контейнер локальных объектов.
	 */
	public void addAvailableStations(final Array<SpaceStation> container, final PlayerShip playerShip, final Class<? extends ExternalStationModule> moduleClass, final LocalObjects local) {

		final Array<GravityObject> gravity = getGravity();

		for(final GravityObject object : gravity.array()) {

			if(object == null) {
				break;
			}

			if(!object.isSpaceStation()) {
				continue;
			}

			final SpaceStation station = object.getSpaceStation();

			if(station.isAvailableModule(playerShip, moduleClass, local)) {
				container.add(station);
			}
		}
	}

	/**
	 * @param object гравитационный объект.
	 */
	public void addGravity(final GravityObject object) {

		final Array<GravityObject> gravity = getGravity();
		gravity.add(object);

		final Array<GravityObject> childs = object.getChilds();

		for(final GravityObject child : childs) {
			addGravity(child);
		}
	}

	/**
	 * @param object локационный объект.
	 */
	public void addLocationObject(final LocationObject object) {
		final Array<LocationObject> objects = getLocationObjects();
		objects.add(object);
	}

	/**
	 * Добавление гравитационных объектов игроку.
	 * 
	 * @param ship корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void addGravityTo(final PlayerShip ship, final LocalObjects local) {

		final Array<GravityObject> gravity = getGravity();

		for(final GravityObject object : gravity.array()) {

			if(object == null) {
				break;
			}

			object.addMe(ship, this, local);
		}
	}

	/**
	 * Добавление локационных объектов игроку.
	 * 
	 * @param ship корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void addLocationObjectsTo(final PlayerShip ship, final LocalObjects local) {

		final Array<LocationObject> objects = getLocationObjects();

		for(final LocationObject object : objects.array()) {

			if(object == null) {
				break;
			}

			object.addMe(ship, this, local);
		}
	}

	/**
	 * Добавить видимый объект в космическое пространство.
	 * 
	 * @param object добавляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void addVisibleObject(final SpaceObject object, final LocalObjects local) {

		if(object == null || !object.isVisible()) {
			return;
		}

		if(!valid(object.getLocation())) {
			object.invalidCoords(local);
			LOGGER.warning(new Exception("incorrect coords for object " + object + ", location " + object.getLocation()));
			return;
		}

		updateSector(object, local);
	}

	/**
	 * Поиск объекта указанного типа с указанным уникальным ид.
	 * 
	 * @param type тип интересуемого корабля.
	 * @param object объект, от которого идет поиск.
	 * @param objectId уникальный ид объекта.
	 * @param classId ид класса объекта.
	 * @return искомый объект.
	 */
	public <T extends SpaceObject> T findObject(final Class<T> type, final SpaceObject object, final int objectId, final int classId) {

		final SpaceSector currentSector = object.getCurrentSector();

		if(currentSector == null) {
			return null;
		}

		for(final SpaceSector neighbor : currentSector.getNeighbors()) {

			final T target = neighbor.findObject(type, object, objectId, classId);

			if(target != null) {
				return target;
			}
		}

		return null;
	}

	/**
	 * Поиск объекта указанного типа с указанным именем.
	 * 
	 * @param type тип интересуемого корабля.
	 * @param object объект, от которого идет поиск.
	 * @param name имя интересуемого объекта.
	 * @return искомый объект.
	 */
	public <T extends SpaceShip> T findObject(final Class<T> type, final SpaceObject object, final String name) {

		final SpaceSector currentSector = object.getCurrentSector();

		if(currentSector == null) {
			return null;
		}

		for(final SpaceSector neighbor : currentSector.getNeighbors()) {

			final T target = neighbor.findObject(type, object, name);

			if(target != null) {
				return target;
			}
		}

		return null;
	}

	/**
	 * @return список активных секторов.
	 */
	public Array<SpaceSector> getActive() {
		return active;
	}

	/**
	 * @return исполнитель обновления AI NPS.
	 */
	public GameExecutor<UpdateNpsAITask> getAiExecutor() {
		return aiExecutor;
	}

	/**
	 * @return список гравитационных объектов в локации.
	 */
	public Array<GravityObject> getGravity() {
		return gravity;
	}

	/**
	 * @return ид локации.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return название локации.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Получить список соседних секторов вокруг указанного сектора.
	 * 
	 * @param locationId ид локации.
	 * @param x индекс сектора.
	 * @param y индекс сектора.
	 * @param z индекс сектора.
	 * @return список соседних секторов.
	 */
	public SpaceSector[] getNeighbors(final int x, final int y, final int z) {

		final Array<SpaceSector> result = ArrayFactory.newArray(SpaceSector.class);

		final SpaceSector[][][] sectors = getSectors();

		for(int a = -1; a <= 1; a++) {
			for(int b = -1; b <= 1; b++) {
				for(int c = -1; c <= 1; c++) {

					final int tileX = x + a;
					final int tileY = y + b;
					final int tileZ = z + c;

					if(validSector(tileX, tileY, tileZ)) {

						SpaceSector sector = sectors[tileX][tileY][tileZ];

						if(sector == null) {
							synchronized(this) {

								sector = sectors[tileX][tileY][tileZ];

								if(sector == null) {
									sector = new SpaceSector(this, tileX, tileY, tileZ);
									sectors[tileX][tileY][tileZ] = sector;
								}
							}
						}

						result.add(sector);
					}
				}
			}
		}

		return result.toArray(new SpaceSector[result.size()]);
	}

	/**
	 * @return исполнитель обновлений объектов в локации.
	 */
	public GameExecutor<UpdateSpaceObjectTask> getObjectExecutor() {
		return objectExecutor;
	}

	/**
	 * Получение сектора, в котором находится точка.
	 * 
	 * @param x координата.
	 * @param y координата.
	 * @param z координата.
	 * @return искомый сектор.
	 */
	public SpaceSector findSector(final float x, final float y, final float z) {

		final int newX = (int) x / SECTOR_SIZE + OFFSET_X;
		final int newY = (int) y / SECTOR_SIZE + OFFSET_Y;
		final int newZ = (int) z / SECTOR_SIZE + OFFSET_Z;

		return getSector(newX, newY, newZ);
	}

	/**
	 * Получение сектора по индексам.
	 * 
	 * @param x индекс.
	 * @param y индекс.
	 * @param z индекс.
	 * @return искомый сектор.
	 */
	public SpaceSector getSector(final int x, final int y, final int z) {

		final SpaceSector[][][] sectors = getSectors();

		SpaceSector sector = null;

		if(validSector(x, y, z)) {

			sector = sectors[x][y][z];

			if(sector == null) {
				synchronized(this) {

					sector = sectors[x][y][z];

					if(sector == null) {
						sector = new SpaceSector(this, x, y, z);
						sectors[x][y][z] = sector;
					}
				}
			}
		}

		return sector;
	}

	/**
	 * Получение сектора, в котром находися указанный объект.
	 * 
	 * @param object объект.
	 * @return искомый сектор.
	 */
	public SpaceSector findSector(final SpaceObject object) {
		final Vector vector = object.getLocation();
		return findSector(vector.getX(), vector.getY(), vector.getZ());
	}

	/**
	 * @return массив секторов в локации.
	 */
	public SpaceSector[][][] getSectors() {
		return sectors;
	}

	/**
	 * @return исполнитель выстрелов в рамках локации.
	 */
	public GameExecutor<UpdateShotTask> getShotExecutor() {
		return shotExecutor;
	}

	/**
	 * @param sector удаляемый активный сектор.
	 */
	public void removeActiveSector(final SpaceSector sector) {

		final Array<SpaceSector> active = getActive();
		active.writeLock();
		try {

			active.fastRemove(sector);

			if(active.isEmpty() && schedule != null) {
				schedule.cancel(false);
				schedule = null;
			}

		} finally {
			active.writeUnlock();
		}
	}

	/**
	 * Удаение гравитационных объектов игроку.
	 * 
	 * @param ship корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void removeGravityTo(final PlayerShip ship, final LocalObjects local) {

		if(ship.getClient() == null) {
			return;
		}

		final Array<GravityObject> gravity = getGravity();

		for(final GravityObject object : gravity.array()) {

			if(object == null) {
				break;
			}

			object.removeMe(ship, this, local);
		}
	}

	/**
	 * Удаение локационных объектов у игрока.
	 * 
	 * @param ship корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void removeLocationObjectsTo(final PlayerShip ship, final LocalObjects local) {

		if(ship.getClient() == null) {
			return;
		}

		final Array<LocationObject> objects = getLocationObjects();

		for(final LocationObject object : objects.array()) {

			if(object == null) {
				break;
			}

			object.removeMe(ship, this, local);
		}
	}

	// TODO если в будущем понадобится, процесс обновление грави объектов
	// @Override
	// public void runImpl() {
	//
	// final LocalObjects local = LocalObjects.get();
	// final long current = System.currentTimeMillis();
	//
	// final Array<GravityObject> gravity = getGravity();
	//
	// gravity.readLock();
	// try {
	//
	// for(final GravityObject object : gravity.array()) {
	//
	// if(object == null) {
	// break;
	// }
	//
	// object.updateGravity(current, local);
	// }
	//
	// } finally {
	// gravity.readUnlock();
	// }
	// }

	/**
	 * Удалить видимый объект из космического пространства.
	 * 
	 * @param object удаляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void removeVisibleObject(final SpaceObject object, final LocalObjects local) {

		if(object == null || object.isVisible()) {
			return;
		}

		final SpaceSector currentSector = object.getCurrentSector();

		if(currentSector != null) {

			currentSector.removeObject(object);

			for(final SpaceSector neighbor : currentSector.getNeighbors()) {
				neighbor.removeFromPlayers(object, local);
			}

			object.setCurrentSector(null);
			object.removeFromExecute(this);
		}
	}

	/**
	 * Обновить столкновение с гравитационными объекктами.
	 * 
	 * @param object обновляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void updateCollision(final SpaceObject object, final LocalObjects local) {
		updateCollision(object, local);
	}

	/**
	 * Обновить столкновение с гравитационными объекктами.
	 * 
	 * @param object обновляемый объект.
	 * @param local контейнер локальных объектов.
	 * @param manager менеджер по коллизиям.
	 */
	public void updateCollision(final SpaceObject object, final LocalObjects local, final CollisionManager manager) {

		final SpaceSector sector = object.getCurrentSector();

		if(sector == null) {
			return;
		}

		final VectorBuffer buffer = local.getNextVectorBuffer();

		for(final SpaceSector neighbor : sector.getNeighbors()) {
			neighbor.updateCollision(object, local, buffer, manager);
		}
	}

	/**
	 * Обновить текущий сектор объекта.
	 * 
	 * @param object обновляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void updateSector(final SpaceObject object, final LocalObjects local) {

		final SpaceSector sector = findSector(object);
		final SpaceSector currentSector = object.getCurrentSector();

		if(sector == null || currentSector != null && currentSector == sector) {
			return;
		}

		if(!validSector(sector.getX(), sector.getY(), sector.getZ())) {
			object.invalidCoords(local);
			LOGGER.warning(new Exception("incorrect sector for object " + object));
			return;
		}

		sector.addObject(object);
		object.setCurrentSector(sector);

		if(currentSector == null) {

			for(final SpaceSector neighbor : sector.getNeighbors()) {
				neighbor.addToPlayers(object, local);
			}

			object.setLocationId(sector.getLocationId());
			object.addToUpdate(this);

		} else {

			final SpaceSector[] oldNeighbors = currentSector.getNeighbors();
			final SpaceSector[] newNeighbors = sector.getNeighbors();

			for(final SpaceSector neighbor : oldNeighbors) {
				if(!ArrayUtils.contains(newNeighbors, neighbor)) {
					neighbor.removeFromPlayers(object, local);
				}
			}

			for(final SpaceSector neighbor : newNeighbors) {
				if(!ArrayUtils.contains(oldNeighbors, neighbor)) {
					neighbor.addToPlayers(object, local);
				}
			}

			currentSector.removeObject(object);
		}
	}

	/**
	 * Проверка корректности координат.
	 * 
	 * @param x текущая координата.
	 * @param y текущая координата.
	 * @param z текущая координата.
	 * @return корректны ли координаты.
	 */
	public boolean valid(final float x, final float y, final float z) {
		return x > LOC_MIN_X && x < LOC_MAX_X && y > LOC_MIN_Y && y < LOC_MAX_Y && z > LOC_MIN_Z && z < LOC_MAX_Z;
	}

	/**
	 * Проверка корректности координат.
	 * 
	 * @param location позиция объекта.
	 * @return корректны ли координаты.
	 */
	public boolean valid(final Vector location) {
		return valid(location.getX(), location.getY(), location.getZ());
	}

	/**
	 * Проверка на корректность индексов сектора.
	 * 
	 * @param x индекс сектора.
	 * @param y индекс сектора.
	 * @param z индекс сектора.
	 * @return корректные ли индексы сектора.
	 */
	public boolean validSector(final int x, final int y, final int z) {
		return x >= sectorMinX && x <= sectorMaxX && y >= sectorMinY && y <= sectorMaxY && z >= sectorMinZ && z <= sectorMaxZ;
	}
}

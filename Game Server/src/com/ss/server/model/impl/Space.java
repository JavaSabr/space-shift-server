package com.ss.server.model.impl;

import rlib.geom.Vector;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.ObjectContainer;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;

/**
 * Модель космоса.
 * 
 * @author Ronn
 */
public final class Space {

	private static final Space instance = new Space();

	/**
	 * @return инстанс космоса.
	 */
	public static Space getInstance() {
		return instance;
	}

	/** таблица существующих гравитационных объектов */
	private final Table<IntKey, GravityObject> gravityTable;
	/** таблица онлаин игроков */
	private final Table<IntKey, PlayerShip> playerTable;

	/** список локаций */
	private final Array<SpaceLocation> locations;
	/** список активных секторов */
	private final Array<SpaceSector> sectors;
	/** все контейнеры в космосе */
	private final Array<ObjectContainer> containers;

	private Space() {
		this.locations = ArrayFactory.newArray(SpaceLocation.class);
		this.sectors = ArrayFactory.newConcurrentAtomicArray(SpaceSector.class);
		this.playerTable = TableFactory.newAtomicConcurrentIntegerTable();
		this.containers = ArrayFactory.newArray(ObjectContainer.class);
		this.gravityTable = TableFactory.newAtomicConcurrentIntegerTable();
	}

	/**
	 * Добавление к активным секторам нового сектора.
	 * 
	 * @param sector новый активный сектор.
	 */
	public void addActiveSector(final SpaceSector sector) {
		sectors.add(sector);
	}

	/**
	 * Добавить окружающих объектов.
	 * 
	 * @param type тип искомых объектов.
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 */
	public <T extends SpaceObject, V extends T> void addAround(final Class<V> type, final SpaceObject object, final Array<T> container) {

		final SpaceSector sector = object.getCurrentSector();

		if(sector == null) {
			return;
		}

		for(final SpaceSector neighbor : sector.getNeighbors()) {
			neighbor.addAround(type, object, container);
		}
	}

	/**
	 * Добавить окружающих объектов.
	 * 
	 * @param type тип искомых объектов.
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 * @param maxDistance максимальная дистанция.
	 */
	public <T extends SpaceObject, V extends T> void addAround(final Class<V> type, final SpaceObject object, final Array<T> container, final int maxDistance) {

		final SpaceSector sector = object.getCurrentSector();

		if(sector == null) {
			return;
		}

		for(final SpaceSector neighbor : sector.getNeighbors()) {
			neighbor.addAround(type, object, container, maxDistance);
		}
	}

	/**
	 * Добавить окружающих объектов.
	 * 
	 * @param type тип искомых объектов.
	 * @param position позиция вокруг которого ищем.
	 * @param container контейнер для объектов.
	 * @param locationId ид локации.
	 */
	public <T extends SpaceObject, V extends T> void addAround(final Class<V> type, final Vector position, final Array<T> container, final int locationId) {

		final SpaceLocation location = getLocation(locationId);

		if(location == null) {
			return;
		}

		final SpaceSector sector = location.findSector(position.getX(), position.getY(), position.getZ());

		if(sector == null) {
			return;
		}

		for(final SpaceSector neighbor : sector.getNeighbors()) {
			neighbor.addAround(type, position, container);
		}
	}

	/**
	 * Добавить окружающих объектов.
	 * 
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 */
	public void addAround(final SpaceObject object, final Array<SpaceObject> container) {

		final SpaceSector sector = object.getCurrentSector();

		if(sector == null) {
			return;
		}

		for(final SpaceSector neighbor : sector.getNeighbors()) {
			neighbor.addAround(sector, object, container);
		}
	}

	/**
	 * Поиск объектов с которыми может быть столкновения в рамках указанной
	 * дистанции.
	 * 
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 * @param distance интересуемая дистанция.
	 */
	public void findCollision(final SpaceObject object, final Array<SpaceObject> container, float distance) {

		final SpaceSector sector = object.getCurrentSector();

		if(sector == null) {
			return;
		}

		for(final SpaceSector neighbor : sector.getNeighbors()) {
			neighbor.findCollision(object, container, distance);
		}
	}

	/**
	 * @param container контейнер объектов.
	 */
	public void addContainer(final ObjectContainer container) {
		containers.add(container);
	}

	/**
	 * @param object новый гравитационных объект.
	 */
	public void addGravityObject(final GravityObject object) {
		gravityTable.put(object.getObjectId(), object);
	}

	/**
	 * @param location новая локация.
	 */
	public void addLocation(final SpaceLocation location) {
		locations.add(location);
	}

	/**
	 * Добавление в реейст активных кораблей игроков космоса.
	 * 
	 * @param playerShip корабль игрока.
	 */
	private void addNewPlayerShip(final PlayerShip playerShip) {

		final Table<IntKey, PlayerShip> playerTable = getPlayerTable();
		playerTable.writeLock();
		try {

			if(playerTable.containsKey(playerShip.getObjectId())) {
				throw new RuntimeException("found duplicate player ship for object id " + playerShip.getObjectId());
			}

			playerTable.put(playerShip.getObjectId(), playerShip);

		} finally {
			playerTable.writeUnlock();
		}
	}

	/**
	 * Добавить видимый объект в космическое пространство.
	 * 
	 * @param object добавляемый объект.
	 * @param onlyLocalObjects добавление только локальных объектов.
	 * @param local контейнер локальных объектов.
	 */
	public void addVisibleObject(final SpaceObject object, boolean onlyLocalObjects, final LocalObjects local) {

		if(object == null || !object.isVisible()) {
			return;
		}

		final SpaceLocation location = getLocation(object);

		if(location == null) {
			return;
		}

		final SpaceSector current = object.getCurrentSector();

		if(object.isPlayerShip()) {

			final PlayerShip playerShip = object.getPlayerShip();

			if(!onlyLocalObjects && (current == null || current.getLocation() != location)) {
				location.addGravityTo(playerShip, local);
				location.addLocationObjectsTo(playerShip, local);
			}

			if(current == null) {
				addNewPlayerShip(playerShip);
			}
		}

		location.addVisibleObject(object, local);
	}

	/**
	 * Отправка пакета всем игрокам в космосе.
	 * 
	 * @param packet отправляемый пакет.
	 * @param sender отправитель.
	 */
	public void broadcastGlobalPacket(final ServerPacket packet, final SpaceObject sender) {

		final Array<SpaceSector> sectors = getActiveSectors();

		if(sectors.isEmpty()) {
			return;
		}

		sectors.readLock();
		try {

			packet.increaseSends();
			try {

				for(final SpaceSector sector : sectors.array()) {

					if(sector == null) {
						break;
					}

					sector.broadcastPacket(packet, sender);
				}

			} finally {
				packet.complete();
			}

		} finally {
			sectors.readUnlock();
		}

	}

	/**
	 * Отправка пакета всем игрокам в локации от отправителя.
	 * 
	 * @param packet отправляемый пакет.
	 * @param sender отправитель.
	 */
	public void broadcastLocationPacket(final ServerPacket packet, final SpaceObject sender) {

		final SpaceSector sector = sender.getCurrentSector();

		if(sector == null) {
			return;
		}

		final Array<SpaceSector> sectors = getActiveSectors();

		if(sectors.isEmpty()) {
			return;
		}

		sectors.readLock();
		try {

			packet.increaseSends();
			try {

				for(final SpaceSector target : sectors.array()) {

					if(target == null) {
						break;
					}

					if(target.getLocationId() != sender.getLocationId()) {
						continue;
					}

					target.broadcastPacket(packet, sender);
				}

			} finally {
				packet.complete();
			}

		} finally {
			sectors.readUnlock();
		}
	}

	/**
	 * Отправка пакета всем игрокам в области от отправителя.
	 * 
	 * @param packet отправляемый пакет.
	 * @param sender отправитель.
	 */
	public void broadcastPacket(final ServerPacket packet, final SpaceObject sender) {

		final SpaceSector sector = sender.getCurrentSector();

		if(sector == null) {
			return;
		}

		packet.increaseSends();
		try {

			for(final SpaceSector neighbor : sector.getNeighbors()) {
				neighbor.broadcastPacket(packet, sender);
			}

		} finally {
			packet.complete();
		}
	}

	/**
	 * @return список активных секторов.
	 */
	public Array<SpaceSector> getActiveSectors() {
		return sectors;
	}

	/**
	 * Получение контейнера.
	 * 
	 * @param objectId уникальный ид контейнера..
	 * @param classId класс ид контейнера.
	 * @return искомый контейнер.
	 */
	@SuppressWarnings("unchecked")
	public <T extends ObjectContainer> T getContainer(final int objectId, final int classId) {

		final Array<ObjectContainer> containers = getContainers();

		if(containers.isEmpty()) {
			return null;
		}

		for(final ObjectContainer container : containers.array()) {

			if(container == null) {
				break;
			}

			if(container.getObjectId() == objectId && container.getClassId() == classId) {
				return (T) container;
			}
		}

		return null;
	}

	/**
	 * @return список всех контейнеров.
	 */
	public Array<ObjectContainer> getContainers() {
		return containers;
	}

	/**
	 * Получение существующего гравитационного объекта по его уникальному ид.
	 * 
	 * @param objectId уникальный ид гравитационного объекта.
	 * @return найденный объект.
	 */
	public GravityObject getGravityObject(final int objectId) {
		return gravityTable.get(objectId);
	}

	/**
	 * @return локация.
	 */
	public SpaceLocation getLocation(final int locationId) {
		return locations.get(locationId);
	}

	/**
	 * @return локация в которой находится объект.
	 */
	public SpaceLocation getLocation(final SpaceObject object) {
		return locations.get(object.getLocationId());
	}

	/**
	 * Получение корабля игрока в космосе по его уникальному ид.
	 * 
	 * @param objectId уникальный ид корабля.
	 * @return ссылка на корабль игрока.
	 */
	public PlayerShip getPlayerShip(final int objectId) {
		final Table<IntKey, PlayerShip> playerTable = getPlayerTable();
		playerTable.readLock();
		try {
			return playerTable.get(objectId);
		} finally {
			playerTable.readUnlock();
		}
	}

	/**
	 * @return таблица онлаин игроков.
	 */
	private Table<IntKey, PlayerShip> getPlayerTable() {
		return playerTable;
	}

	/**
	 * Удаление из активных старый сектор.
	 * 
	 * @param sector старый сектор.
	 */
	public void removeActiveSector(final SpaceSector sector) {
		sectors.fastRemove(sector);
	}

	/**
	 * Удаление из реестра активных кораблей игроков космоса.
	 * 
	 * @param playerShip корабль игрока.
	 */
	private void removeOldPlayerShip(final PlayerShip playerShip) {

		final Table<IntKey, PlayerShip> playerTable = getPlayerTable();
		playerTable.writeLock();
		try {

			if(!playerTable.containsKey(playerShip.getObjectId())) {
				throw new RuntimeException("not found player ship for object id " + playerShip.getObjectId());
			}

			playerTable.remove(playerShip.getObjectId());

		} finally {
			playerTable.writeUnlock();
		}
	}

	/**
	 * Удалить видимый объект из космического пространства.
	 * 
	 * @param object удаляемый объект.
	 * @param onlyLocalObjects удаление только локальных объектов.
	 * @param local контейнер локальных объектов.
	 */
	public void removeVisibleObject(final SpaceObject object, boolean onlyLocalObjects, final LocalObjects local) {

		if(object == null || object.isVisible()) {
			return;
		}

		final SpaceLocation location = getLocation(object);

		if(location == null) {
			return;
		}

		final SpaceSector sector = object.getCurrentSector();

		if(object.isPlayerShip()) {

			final PlayerShip playerShip = object.getPlayerShip();

			if(!onlyLocalObjects) {
				location.removeGravityTo(playerShip, local);
				location.removeLocationObjectsTo(playerShip, local);
			}

			if(sector != null) {
				removeOldPlayerShip(playerShip);
			}
		}

		location.removeVisibleObject(object, local);
	}
}

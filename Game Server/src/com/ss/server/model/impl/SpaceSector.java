package com.ss.server.model.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import rlib.concurrent.atomic.AtomicInteger;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.util.StringUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.manager.CollisionManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;

/**
 * Реализация космического сектора локации.
 * 
 * @author Ronn
 */
public final class SpaceSector {

	private static final Space SPACE = Space.getInstance();

	/** список объектов в секторе */
	private final Array<SpaceObject> objects;
	/** ид локации, к которому принадлежит он */
	private final SpaceLocation location;

	/** счетчик игроков в секторе */
	private final AtomicInteger playerCounter;
	/** статус активности сектора */
	private final AtomicBoolean active;

	/** координаты сектора в космосе */
	private final int x;
	private final int y;
	private final int z;

	/** соседние сектора */
	private SpaceSector[] neighbors;

	public SpaceSector(final SpaceLocation location, final int x, final int y, final int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.location = location;
		this.objects = ArrayFactory.newConcurrentAtomicArray(SpaceObject.class);
		this.playerCounter = new AtomicInteger(0);
		this.active = new AtomicBoolean(false);
	}

	/**
	 * Поиск и добавление в указанный контейнер всех окружающих объектов
	 * исключая указанный.
	 * 
	 * @param type тип искомых объектов.
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 */
	public <T extends SpaceObject, V extends T> void addAround(final Class<V> type, final SpaceObject object, final Array<T> container) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			container.checkSize(container.size() + objects.size());

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				if(target == object || !type.isInstance(target)) {
					continue;
				}

				container.unsafeAdd(type.cast(target));
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Добавить окружающих объектов.
	 * 
	 * @param type тип искомых объектов.
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 * @param maxDistance максимальная дистанция.
	 */
	public <T extends SpaceObject, V extends T> void addAround(final Class<V> type, final SpaceObject object, final Array<T> container, final int maxDistance) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			container.checkSize(container.size() + objects.size());

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				if(target == object || !type.isInstance(target) || !object.isInDistance(target, maxDistance)) {
					continue;
				}

				container.unsafeAdd(type.cast(target));
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Добавить окружающих объектов.
	 * 
	 * @param type тип искомых объектов.
	 * @param position позиция, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 */
	public <T extends SpaceObject, V extends T> void addAround(final Class<V> type, final Vector position, final Array<T> container) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				if(!type.isInstance(target)) {
					continue;
				}

				container.add(type.cast(target));
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Поиск и добавление в указанный контейнер всех окружающих объектов
	 * исключая указанный.
	 * 
	 * @param sector сектор этого объекта.
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 */
	public void addAround(final SpaceSector sector, final SpaceObject object, final Array<SpaceObject> container) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			container.addAll(objects);

			if(sector == this) {
				container.fastRemove(object);
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Поиск объектов с которыми может быть коллизия.
	 * 
	 * @param object объект, вокруг которого ищем.
	 * @param container контейнер для объектов.
	 * @param distance дистанция в рамках которой идет поиск.
	 */
	public void findCollision(final SpaceObject object, final Array<SpaceObject> container, float distance) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			for(SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				if(object == target) {
					continue;
				}

				if(!object.canCollision(target) || !object.isInDistance(target, (int) (target.getMaxSize() + distance))) {
					continue;
				}

				container.add(target);
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Добавление нового объекта в сектор.
	 * 
	 * @param object новый объект.
	 */
	public void addObject(final SpaceObject object) {

		if(object == null) {
			return;
		}

		final Array<SpaceObject> objects = getObjects();
		objects.writeLock();
		try {
			objects.add(object);
		} finally {
			objects.writeUnlock();
		}

		if(!object.isPlayerShip()) {
			return;
		}

		final AtomicInteger playerCounter = getPlayerCounter();

		final int oldPlayers = playerCounter.getAndIncrement();
		final int newPlayers = playerCounter.get();

		// если в секторе небыло игроков и он появился
		if(oldPlayers == 0 && newPlayers > 0) {
			updateStatus();
		}
	}

	/**
	 * Добавить объект для игроков.
	 * 
	 * @param object добавляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void addToPlayers(final SpaceObject object, final LocalObjects local) {

		final Array<SpaceObject> objects = getObjects();

		if(object.isGravityObject() || objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			final PlayerShip playerShip = object.getPlayerShip();

			if(playerShip != null) {

				for(final SpaceObject target : objects.array()) {

					if(target == null) {
						break;
					}

					if(target == object || target.isGravityObject()) {
						continue;
					}

					playerShip.addVisibleObject(target, local);
				}
			}

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				final PlayerShip player = target.getPlayerShip();

				if(player != null && player != object) {
					player.addVisibleObject(object, local);
				}
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Отправка пакета окружающим кораблям в секторе.
	 * 
	 * @param packet отправляемый пакет.
	 * @param sender отправитель.
	 */
	public void broadcastPacket(final ServerPacket packet, final SpaceObject sender) {

		if(playerCounter.get() < 1) {
			return;
		}

		final Array<SpaceObject> objects = getObjects();
		objects.readLock();
		try {

			for(final SpaceObject object : objects.array()) {

				if(object == null) {
					break;
				}

				final PlayerShip playerShip = object.getPlayerShip();

				if(playerShip != null && playerShip != sender) {
					playerShip.sendPacket(packet, true);
				}
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Поиск объекта указанного типа с указанным уникальным ид.
	 * 
	 * @param type тип интересуемого корабля.
	 * @param object объект, от которого идет поиск.
	 * @param objectId уникальный ид объекта.
	 * @param classId ид класса объекта.
	 * @return искомый объект.
	 */
	public <T extends SpaceObject> T findObject(final Class<T> type, final SpaceObject object, final int objectId, final int classId) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return null;
		}

		objects.readLock();
		try {

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				if(target == object || target.getObjectId() != objectId || target.getClassId() != classId) {
					continue;
				}

				if(type.isInstance(target)) {
					return type.cast(target);
				}
			}

		} finally {
			objects.readUnlock();
		}

		return null;
	}

	/**
	 * Поиск объекта указанного типа с указанным именем.
	 * 
	 * @param type тип интересуемого корабля.
	 * @param object объект, от которого идет поиск.
	 * @param name имя интересуемого объекта.
	 * @return искомый объект.
	 */
	public <T extends SpaceShip> T findObject(final Class<T> type, final SpaceObject object, final String name) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return null;
		}

		objects.readLock();
		try {

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				if(target == object || !target.isSpaceShip()) {
					continue;
				}

				final SpaceShip spaceShip = target.getSpaceShip();

				if(!StringUtils.equalsIgnoreCase(spaceShip.getName(), name)) {
					continue;
				}

				if(type.isInstance(target)) {
					return type.cast(target);
				}
			}

		} finally {
			objects.readUnlock();
		}

		return null;
	}

	/**
	 * @return локация, в которой находится сектор.
	 */
	public SpaceLocation getLocation() {
		return location;
	}

	/**
	 * @return ид локации.
	 */
	public int getLocationId() {
		return location.getId();
	}

	/**
	 * @return массив соседних регионов.
	 */
	public SpaceSector[] getNeighbors() {

		if(neighbors == null) {
			neighbors = location.getNeighbors(x, y, z);
		}

		return neighbors;
	}

	/**
	 * @return objects массив объектов сектора.
	 */
	public final Array<SpaceObject> getObjects() {
		return objects;
	}

	/**
	 * @return счетчик игроков в секторе.
	 */
	public AtomicInteger getPlayerCounter() {
		return playerCounter;
	}

	/**
	 * @return координаты сектора в космосе.
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return координаты сектора в космосе.
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return координаты сектора в космосе.
	 */
	public int getZ() {
		return z;
	}

	public boolean hasPlayers() {
		return playerCounter.get() > 0;
	}

	/**
	 * @return активен ли сектор.
	 */
	public boolean isActive() {
		return active.get();
	}

	/**
	 * Удаляем объект для игроков.
	 * 
	 * @param object удаляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void removeFromPlayers(final SpaceObject object, final LocalObjects local) {

		final Array<SpaceObject> objects = getObjects();

		if(object.isGravityObject() || objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			final PlayerShip playerShip = object.getPlayerShip();

			if(playerShip != null) {
				for(final SpaceObject target : objects.array()) {

					if(target == null) {
						break;
					}

					if(target == object || object.isGravityObject()) {
						continue;
					}

					playerShip.removeVisibleObject(target, local);
				}
			}

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				final PlayerShip player = target.getPlayerShip();

				if(player != null && target != object) {
					player.removeVisibleObject(object, local);
				}
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Удаление объекта из сектора.
	 * 
	 * @param object удаляемый объект.
	 */
	public void removeObject(final SpaceObject object) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		boolean removed = false;

		objects.writeLock();
		try {
			removed = objects.fastRemove(object);
		} finally {
			objects.writeUnlock();
		}

		if(!removed) {
			return;
		}

		if(!object.isPlayerShip()) {
			return;
		}

		final AtomicInteger playerCounter = getPlayerCounter();

		final int oldplayers = playerCounter.getAndDecrement();
		final int newPlayers = playerCounter.get();

		if(oldplayers > 0 && newPlayers == 0) {
			updateStatus();
		}
	}

	/**
	 * Установка активности секторв.
	 */
	private synchronized void setActive(final boolean active) {

		if(isActive() == active) {
			return;
		}

		this.active.getAndSet(active);

		if(active) {
			location.addActiveSector(this);
			SPACE.addActiveSector(this);
		} else {
			location.removeActiveSector(this);
			SPACE.removeActiveSector(this);
		}
	}

	@Override
	public String toString() {
		return "SpaceSector [playerCounter=" + playerCounter + ", active=" + active + ", x=" + x + ", y=" + y + ", z=" + z + "]";
	}

	/**
	 * Обновление состояние активности региона.
	 */
	private void updateActive() {

		final SpaceSector[] around = getNeighbors();

		boolean current = false;

		for(final SpaceSector sector : around) {
			if(sector.hasPlayers()) {
				current = true;
				break;
			}
		}

		if(current != active.get()) {
			setActive(current);
		}
	}

	/**
	 * Обновить столкновение объектов.
	 * 
	 * @param object обновляемый объект.
	 * @param buffer буффер векторов для вычислений.
	 * @param manager менеджер по коллизиям.
	 */
	public void updateCollision(final SpaceObject object, final LocalObjects local, final VectorBuffer buffer, final CollisionManager manager) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			for(final SpaceObject target : objects.array()) {

				if(target == null) {
					break;
				}

				if(target != object && manager.isCollision(object, target, local, buffer)) {
					target.onCollision(object, local);
					object.onCollision(target, local);
				}
			}

		} finally {
			objects.readUnlock();
		}
	}

	/**
	 * Обновить столкновение объектов.
	 * 
	 * @param object обновляемый объект.
	 * @param local контейнер локальных объектов.
	 */
	public void updateCollision(final SpaceObject object, final VectorBuffer buffer, final LocalObjects local) {
		updateCollision(object, local, buffer, CollisionManager.getInstance());
	}

	/**
	 * Обновляет статус активности близлежащих регионов.
	 */
	private void updateStatus() {

		final SpaceSector[] regions = getNeighbors();

		for(int i = 0, length = regions.length; i < length; i++) {
			regions[i].updateActive();
		}
	}
}

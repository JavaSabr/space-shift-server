package com.ss.server.model.impl;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.network.game.model.GameClient;

/**
 * Реализация аккаунта игрока.
 * 
 * @author Ronn
 */
public final class Account implements Foldable {

	private static final FoldablePool<Account> POOL = PoolFactory.newAtomicFoldablePool(Account.class);

	/**
	 * Создание нового аккаунта по указанным данным.
	 * 
	 * @param name имя аккаунета.
	 * @param password пароль аккаунта.
	 * @param client ссылка на клиент аккаунта.
	 * @return новый аккаунт.
	 */
	public static Account newInstance(final String name, final String password, final GameClient client) {

		Account account = POOL.take();

		if(account == null) {
			account = new Account();
		}

		account.client = client;
		account.name = name;
		account.password = password;

		return account;
	}

	/** имя аккаунта */
	private String name;
	/** пароль аккаунта */
	private String password;
	/** ссылка на клиент аккаунта */
	private GameClient client;

	@Override
	public void finalyze() {
		name = null;
		client = null;
		password = null;
	}

	/**
	 * Складировать в пул.
	 */
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return ссылка на клиент аккаунта.
	 */
	public final GameClient getClient() {
		return client;
	}

	/**
	 * @return название аккаунта.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return пароль аккаунта.
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * @param client ссылка на клиент аккаунта.
	 */
	public final void setClient(final GameClient client) {
		this.client = client;
	}

	/**
	 * @param name название аккаунта.
	 */
	public final void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param password пароль аккаунта.
	 */
	public final void setPassword(final String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Account [name=" + name + ", password=" + password + "]";
	}
}

package com.ss.server.model.condition;

import com.ss.server.model.impl.Env;

/**
 * Модель условия описанного в хмл.
 * 
 * @author Ronn
 */
public interface Condition {

	/**
	 * @return сообщение в чат при невыполнении.
	 */
	public boolean getMessage();

	/**
	 * @return выполнены ли условия.
	 */
	public boolean test(Env env);
}

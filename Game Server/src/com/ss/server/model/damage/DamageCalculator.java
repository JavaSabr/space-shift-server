package com.ss.server.model.damage;

/**
 * Интерфейс для реализации калькулятора урона.
 * 
 * @author Ronn
 */
public interface DamageCalculator {

	/**
	 * Получение итогового урона по корпусу корабля.
	 * 
	 * @param power изначальная мощность.
	 * @param elementType тип использумой стихии.
	 * @return итоговый урон для корпуса.
	 */
	public int getForBody(int power, ElementType elementType);

	/**
	 * Получение итогового значения урона для щита.
	 * 
	 * @param power изначальная мощность.
	 * @param elementType тип используемой стехии.
	 * @param shieldType тип отакуемого щита.
	 * @return итоговый урон для этого щита.
	 */
	public int getForShield(int power, ElementType elementType, ShieldType shieldType);
}

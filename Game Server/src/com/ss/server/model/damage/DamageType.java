package com.ss.server.model.damage;

import rlib.util.ClassUtils;

import com.ss.server.model.damage.impl.DefaultDamageCalculator;
import com.ss.server.model.damage.impl.ExplosinoDamageCalculator;
import com.ss.server.model.damage.impl.LaserDamageCalculator;
import com.ss.server.model.damage.impl.NeutronDamageCalculator;
import com.ss.server.model.damage.impl.PhotonDamageCalculator;
import com.ss.server.model.damage.impl.PlasmaDamageCalculator;
import com.ss.server.model.damage.impl.PulseDamageCalculator;
import com.ss.server.model.damage.impl.TachyonDamageCalculator;

/**
 * Перечисление видов урона.
 * 
 * @author Ronn
 */
public enum DamageType {

	/** тахионный урон */
	TACHYON(TachyonDamageCalculator.class, "@damage-type:tachyon@"),
	/** лазерное */
	LASER(LaserDamageCalculator.class, "@damage-type:laser@"),
	/** импульсный урон */
	PULSE(PulseDamageCalculator.class, "@damage-type:pulse@"),
	/** пламотический урон */
	PLASMA(PlasmaDamageCalculator.class, "@damage-type:plasma@"),
	/** фотонный урон */
	PHOTON(PhotonDamageCalculator.class, "@damage-type:photon@"),
	/** урон от взрыва */
	EXPLOSION(ExplosinoDamageCalculator.class, "@damage-type:explosion@"),
	/** нейтронный урон */
	NEUTRON(NeutronDamageCalculator.class, "@damage-type:neutron@"),
	/** безтиповый урон */
	NONE;

	/** калькулятор урона для текущго типа */
	private final DamageCalculator calculator;

	/** локализованное название типа урона */
	private final String langName;

	private DamageType() {
		this.calculator = new DefaultDamageCalculator();
		this.langName = "none";
	}

	private DamageType(final Class<? extends DamageCalculator> type, String langName) {
		this.calculator = ClassUtils.newInstance(type);
		this.langName = langName;
	}

	/**
	 * @return калькулятор урона для текущго типа.
	 */
	public DamageCalculator getCalculator() {
		return calculator;
	}

	/**
	 * @return локализованное название типа урона.
	 */
	public String getLangName() {
		return langName;
	}
}

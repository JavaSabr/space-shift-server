package com.ss.server.model.damage;

/**
 * Перечисление типов щитов.
 * 
 * @author Ronn
 */
public enum ShieldType {

	/** щит невидимости */
	STEALTH,
	/** черепаший щит */
	TURTLE,
	/** рефракционный щит */
	REFRECTIVE,
	/** тардионный щит */
	TARDIONNY,
	/** безтиповый щит */
	NONE,
}

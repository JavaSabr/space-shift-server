package com.ss.server.model.damage.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.model.damage.DamageCalculator;
import com.ss.server.model.damage.ElementType;
import com.ss.server.model.damage.ShieldType;

/**
 * Базовая реализациюя калькулятора урона.
 * 
 * @author Ronn
 */
public abstract class AbstractDamageCalculator implements DamageCalculator {

	protected static final Logger LOGGER = LoggerManager.getLogger(DamageCalculator.class);

	/** таблица процентов эффективности */
	protected final float[] shieldPercents;

	public AbstractDamageCalculator() {
		this.shieldPercents = new float[ShieldType.values().length];
		fillPercents();
	}

	protected void fillPercents() {
		for(int i = 0, length = shieldPercents.length; i < length; i++) {
			shieldPercents[i] = 1F;
		}
	}

	@Override
	public int getForBody(final int power, final ElementType elementType) {
		return power;
	}

	@Override
	public int getForShield(final int power, final ElementType elementType, final ShieldType shieldType) {
		return (int) (power * shieldPercents[shieldType.ordinal()]);
	}
}

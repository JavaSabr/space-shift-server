package com.ss.server.model.damage.impl;

import static com.ss.server.model.damage.ShieldType.REFRECTIVE;
import static com.ss.server.model.damage.ShieldType.STEALTH;
import static com.ss.server.model.damage.ShieldType.TARDIONNY;
import static com.ss.server.model.damage.ShieldType.TURTLE;

/**
 * Реализация калькулятора взырвного урона.
 * 
 * @author Ronn
 */
public class ExplosinoDamageCalculator extends AbstractDamageCalculator {

	@Override
	protected void fillPercents() {
		shieldPercents[STEALTH.ordinal()] = 1F;
		shieldPercents[TURTLE.ordinal()] = 0.5F;
		shieldPercents[REFRECTIVE.ordinal()] = 1F;
		shieldPercents[TARDIONNY.ordinal()] = 0.5F;
	}
}

package com.ss.server.model.damage.impl;

import static com.ss.server.model.damage.ShieldType.REFRECTIVE;
import static com.ss.server.model.damage.ShieldType.STEALTH;
import static com.ss.server.model.damage.ShieldType.TARDIONNY;
import static com.ss.server.model.damage.ShieldType.TURTLE;

/**
 * Реализация калькулятора импульсного урона.
 * 
 * @author Ronn
 */
public class PulseDamageCalculator extends AbstractDamageCalculator {

	@Override
	protected void fillPercents() {
		shieldPercents[STEALTH.ordinal()] = 0.8F;
		shieldPercents[TURTLE.ordinal()] = 0.5F;
		shieldPercents[REFRECTIVE.ordinal()] = 0.3F;
		shieldPercents[TARDIONNY.ordinal()] = 0.3F;
	}
}

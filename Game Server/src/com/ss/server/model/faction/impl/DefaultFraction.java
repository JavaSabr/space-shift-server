package com.ss.server.model.faction.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;

/**
 * Стандартная реализация фракции.
 * 
 * @author Ronn
 */
public class DefaultFraction extends AbstractFraction {

	public DefaultFraction(final VarTable vars, final Node node) {
		super(vars, node);
	}
}

package com.ss.server.model.faction.impl;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.w3c.dom.Node;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.ArrayUtils;
import rlib.util.VarTable;
import rlib.util.random.Random;
import rlib.util.random.RandomFactory;

import com.ss.server.Config;
import com.ss.server.IdFactory;
import com.ss.server.LocalObjects;
import com.ss.server.database.ModuleDBManager;
import com.ss.server.manager.QuestManager;
import com.ss.server.model.faction.Fraction;
import com.ss.server.model.faction.ItemInfo;
import com.ss.server.model.faction.PositionInfo;
import com.ss.server.model.impl.Space;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSlot;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.storage.Storage;
import com.ss.server.table.ItemTable;
import com.ss.server.table.ModuleTable;
import com.ss.server.table.ShipTable;
import com.ss.server.template.ModuleTemplate;
import com.ss.server.template.item.ItemTemplate;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Базовая реализация фракции.
 * 
 * @author Ronn
 */
public abstract class AbstractFraction implements Fraction {

	protected static final Logger LOGGER = LoggerManager.getLogger(Fraction.class);

	public static final String START_ITEMS_AUTHOR = "fraction_start_items";

	public static final String NODE_MODULE = "module";
	public static final String NODE_QUEST = "quest";
	public static final String NODE_POSITION = "position";
	public static final String NODE_PROPERTY = "property";
	public static final String NODE_TEMPLATE = "template";
	public static final String NODE_QUESTS = "quests";
	public static final String NODE_POSITIONS = "positions";
	public static final String NODE_PROPERTIES = "properties";
	public static final String NODE_SKILL = "skill";
	public static final String NODE_ITEM = "item";
	public static final String NODE_SKILL_PANEL = "skillPanel";
	public static final String NODE_ITEMS = "items";

	public static final String ATTR_RADIUS = "radius";
	public static final String ATTR_FRACTION_MAIN_ICON = "mainIcon";
	public static final String ATTR_STATION_ID = "stationId";
	public static final String ATTR_PROPERTY_NAME = "name";
	public static final String ATTR_PROPERTY_VALUE = "value";
	public static final String ATTR_SHIP_ID = "id";
	public static final String ATTR_QUEST_ID = ATTR_SHIP_ID;
	public static final String ATTR_MODULE_ID = "id";
	public static final String ATTR_FRACTION_NAME = "name";
	public static final String ATTR_FRACTION_ID = "id";
	public static final String ATTR_POSITION_LOCATION = "location";
	public static final String ATTR_POSITION_ROTATION = "rotation";
	public static final String ATTR_SKILL_ORDER = "order";
	public static final String ATTR_ITEM_COUNT = "count";
	public static final String ATTR_ITEM_ID = "id";
	public static final String ATTR_HISTORY = "history";

	/** локальные рандоминайзеры */
	private static final ThreadLocal<Random> LOCAL_RANDOM = new ThreadLocal<Random>() {

		@Override
		protected Random initialValue() {
			return RandomFactory.newFastRandom();
		}
	};

	/** название фракции */
	private final String name;

	/** ид фракции */
	private final int id;

	/** набор модулей дял стартового корабля */
	private ModuleTemplate[] modules;
	/** список стартовых позиций */
	private PositionInfo[] positions;
	/** позиции умений на панели */
	private Integer[] skillPositions;
	/** список выдаваемых предметов */
	private ItemInfo[] items;
	/** набор стартовых заданий */
	private Quest[] quests;

	/** шаблон стартового корабля */
	private PlayerShipTemplate template;

	/** главная эмблема фракции */
	private String mainIcon;
	/** история фракции */
	private String history;

	/** готова ли фракция к использованию */
	private boolean ready;

	public AbstractFraction(final VarTable vars, final Node node) {
		this.id = vars.getInteger(ATTR_FRACTION_ID);
		this.name = vars.getString(ATTR_FRACTION_NAME);
		this.modules = new ModuleTemplate[0];
		this.positions = new PositionInfo[0];
		this.items = new ItemInfo[0];
		this.skillPositions = new Integer[0];
		this.ready = true;

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(NODE_PROPERTIES.equals(child.getNodeName())) {
				parseProperties(child);
			} else if(NODE_POSITIONS.equals(child.getNodeName())) {
				parsePositions(child);
			} else if(NODE_QUESTS.equals(child.getNodeName())) {
				parseQuests(child);
			} else if(NODE_TEMPLATE.equals(child.getNodeName())) {
				parseTemplate(child);
			} else if(NODE_ITEMS.equals(child.getNodeName())) {
				parseItems(child);
			} else if(NODE_SKILL_PANEL.equals(child.getNodeName())) {
				parseSkillPanel(child);
			}
		}
	}

	@Override
	public void addItemsTo(final PlayerShip playerShip, final LocalObjects local) {

		final ItemInfo[] items = getItems();

		if(items.length < 1) {
			return;
		}

		final Storage storage = playerShip.getStorage();
		storage.lock();
		try {

			for(final ItemInfo info : items) {

				final ItemTemplate template = info.getTemplate();

				if(template == null) {
					continue;
				}

				final long count = template.isStackable() ? info.getCount() : 1;

				storage.addItem(template.getId(), count, START_ITEMS_AUTHOR);
			}

		} finally {
			storage.unlock();
		}
	}

	@Override
	public void addModulesTo(final PlayerShip playerShip, final LocalObjects local) {

		final ModuleDBManager dbManager = ModuleDBManager.getInstance();
		final IdFactory idFactory = IdFactory.getInstance();
		final ModuleSystem system = playerShip.getModuleSystem();

		for(final ModuleTemplate template : getModules()) {

			if(template == null) {
				continue;
			}

			final Module module = template.takeInstance(idFactory.getNextModuleId()).getModule();
			dbManager.addNewModule(module);

			system.addModule(module, local);
		}
	}

	@Override
	public void addQuestsTo(final PlayerShip playerShip, final LocalObjects local) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getHistory() {
		return history;
	}

	@Override
	public int getId() {
		return id;
	}

	/**
	 * @return список выдаваемых предметов.
	 */
	public ItemInfo[] getItems() {
		return items;
	}

	@Override
	public String getMainIcon() {
		return mainIcon;
	}

	/**
	 * @return набор модулей дял стартового корабля.
	 */
	public ModuleTemplate[] getModules() {
		return modules;
	}

	@Override
	public String getName() {
		return name;
	}

	/**
	 * @return список стартовых позиций.
	 */
	public PositionInfo[] getPositions() {
		return positions;
	}

	/**
	 * @return набор стартовых заданий.
	 */
	public Quest[] getQuests() {
		return quests;
	}

	@Override
	public PlayerShipTemplate getShipTemplate() {
		return template;
	}

	/**
	 * @return позиции умений на панели.
	 */
	public Integer[] getSkillPositions() {
		return skillPositions;
	}

	/**
	 * @return шаблон стартового корабля.
	 */
	public PlayerShipTemplate getTemplate() {
		return template;
	}

	@Override
	public boolean isReady() {

		if(!ready) {
			return false;
		}

		final PlayerShipTemplate shipTemplate = getTemplate();

		if(shipTemplate == null) {
			return false;
		}

		final PositionInfo[] positions = getPositions();

		if(positions.length < 1) {
			return false;
		}

		return true;
	}

	@Override
	public void moveSkillsToPanel(final PlayerShip playerShip, final LocalObjects local) {

		final Integer[] positions = getSkillPositions();

		final ModuleSystem moduleSystem = playerShip.getModuleSystem();

		int order = 0;

		for(final ModuleSlot slot : moduleSystem.getSlots()) {

			final Module module = slot.getModule();

			if(module == null) {
				continue;
			}

			final Skill[] skills = module.getSkills();

			for(final Skill skill : skills) {

				if(order >= positions.length) {
					break;
				}

				final int position = positions[order++].intValue();

				if(position == -1) {
					continue;
				}

				playerShip.updateSkillPanel(module.getObjectId(), skill.getId(), -1, position);
			}
		}
	}

	@Override
	public void moveToStart(final PlayerShip playerShip, final LocalObjects local) {

		final PositionInfo[] positions = getPositions();
		PositionInfo position = null;

		if(positions.length == 1) {
			position = positions[0];
		} else {
			final Random random = LOCAL_RANDOM.get();
			position = positions[random.nextInt(0, positions.length - 1)];
		}

		final Rotation rotation = playerShip.getRotation();
		rotation.set(position.getRotation());

		final Vector location = playerShip.getLocation();
		location.set(position.getPosition());

		playerShip.setLocationId(position.getLocationId());

		final int stationId = position.getStationId();

		if(stationId > 0) {

			final Space space = Space.getInstance();
			final SpaceStation station = space.getContainer(stationId, Config.SERVER_STATION_CLASS_ID);

			if(station == null) {
				LOGGER.error(this, "not found station from id " + stationId);
			} else {

				final AtomicReference<SpaceStation> reference = playerShip.getStationReference();

				synchronized(reference) {
					reference.set(station);
				}

				final AtomicBoolean onStation = playerShip.getOnStation();

				synchronized(onStation) {
					onStation.set(true);
				}
			}
		}
	}

	/**
	 * Парсинг стартовых предметов.
	 */
	protected void parseItems(final Node node) {

		final ItemTable itemTable = ItemTable.getInstance();

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_ITEM.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final int templateId = vars.getInteger(ATTR_ITEM_ID);

			final ItemTemplate template = itemTable.getTemplate(templateId);

			if(template == null) {
				LOGGER.warning("not found item for id " + templateId);
				continue;
			}

			final long count = vars.getLong(ATTR_ITEM_COUNT);

			if(count < 1 || !template.isStackable() && count > 1) {
				LOGGER.warning("incorrect count " + count + " for item " + template);
				continue;
			}

			items = ArrayUtils.addToArray(items, new ItemInfo(template, count), ItemInfo.class);
		}
	}

	/**
	 * Парсинг стартовых позиций фракции.
	 */
	protected void parsePositions(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_POSITION.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final int locationId = vars.getInteger(ATTR_POSITION_LOCATION);
			final int stationId = vars.getInteger(ATTR_STATION_ID, 0);
			final int radius = vars.getInteger(ATTR_RADIUS, 0);

			float[] array = vars.getFloatArray(NODE_POSITION, ",");

			final Vector position = Vector.newInstance(array);

			array = vars.getFloatArray(ATTR_POSITION_ROTATION, ",");

			final Rotation rotation = Rotation.newInstance();
			rotation.fromAngles(array);

			positions = ArrayUtils.addToArray(positions, new PositionInfo(position, rotation, locationId, stationId, radius), PositionInfo.class);
		}
	}

	/**
	 * Парсинг доп. свойств фракции.
	 */
	protected void parseProperties(final Node node) {

		final VarTable vars = VarTable.newInstance(node, NODE_PROPERTY, ATTR_PROPERTY_NAME, ATTR_PROPERTY_VALUE);

		this.mainIcon = vars.getString(ATTR_FRACTION_MAIN_ICON);
		this.history = vars.getString(ATTR_HISTORY);
	}

	/**
	 * Парсинг стартовых заданий фракции.
	 */
	protected void parseQuests(final Node node) {

		final QuestManager questManager = QuestManager.getInstance();

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_QUEST.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final int questId = vars.getInteger(ATTR_QUEST_ID);

			final Quest quest = questManager.getQuest(questId);

			if(quest == null) {
				LOGGER.warning("not found quest for id " + questId);
				continue;
			}

			quests = ArrayUtils.addToArray(quests, quest, Quest.class);
		}
	}

	/**
	 * Парсинг расположения умений на панели.
	 */
	protected void parseSkillPanel(final Node node) {

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_SKILL.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			skillPositions = ArrayUtils.addToArray(skillPositions, vars.getInteger(ATTR_SKILL_ORDER), Integer.class);
		}
	}

	/**
	 * Парсинг структуры стартового корабля.
	 */
	protected void parseTemplate(final Node node) {

		final ShipTable shipTable = ShipTable.getInstance();
		final ModuleTable moduleTable = ModuleTable.getInstance();

		final VarTable vars = VarTable.newInstance(node);

		int templateId = vars.getInteger(ATTR_SHIP_ID);

		final PlayerShipTemplate template = shipTable.getTemplate(templateId);

		if(template == null) {
			LOGGER.warning("not found player ship template for id " + templateId);
			return;
		}

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE || !NODE_MODULE.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			templateId = vars.getInteger(ATTR_MODULE_ID);

			final ModuleTemplate moduleTemplate = moduleTable.getTemplate(templateId);

			if(moduleTemplate == null) {
				LOGGER.warning("not found module template for id " + templateId);
				ready = false;
				continue;
			}

			modules = ArrayUtils.addToArray(modules, moduleTemplate, ModuleTemplate.class);
		}

		this.template = template;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + name + ", id=" + id + ", modules=" + Arrays.toString(modules) + ", positions=" + Arrays.toString(positions) + ", quests="
				+ Arrays.toString(quests) + ", template=" + template + ", mainIcon=" + mainIcon + "]";
	}
}

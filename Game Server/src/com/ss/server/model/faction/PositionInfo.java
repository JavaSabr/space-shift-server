package com.ss.server.model.faction;

import rlib.geom.Rotation;
import rlib.geom.Vector;

/**
 * Реализация контейнера информации о стартовой позиции.
 * 
 * @author Ronn
 */
public class PositionInfo {

	/** позиция корабля */
	private final Vector position;
	/** разворот корабля */
	private final Rotation rotation;

	/** ид локации */
	private final int locationId;
	/** ид станции, за которой надо закрепить корабль */
	private final int stationId;
	/** радиус в рамках которого идет рассчет позиции от точки */
	private final int radius;

	public PositionInfo(final Vector position, final Rotation rotation, final int locationId, final int stationId, final int radius) {
		this.position = position;
		this.rotation = rotation;
		this.locationId = locationId;
		this.stationId = stationId;
		this.radius = radius;
	}

	/**
	 * @return ид локации.
	 */
	public int getLocationId() {
		return locationId;
	}

	/**
	 * @return позиция корабля.
	 */
	public Vector getPosition() {
		return position;
	}

	/**
	 * @return разворот корабля.
	 */
	public Rotation getRotation() {
		return rotation;
	}

	/**
	 * @return ид станции, за которой надо закрепить корабль.
	 */
	public int getStationId() {
		return stationId;
	}

	/**
	 * @return радиус в рамках которого идет рассчет позиции от точки.
	 */
	public int getRadius() {
		return radius;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [position=" + position + ", rotation=" + rotation + ", locationId=" + locationId + ", stationId=" + stationId + ", radius=" + radius + "]";
	}
}

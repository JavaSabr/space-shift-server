package com.ss.server.model.faction;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.template.ship.PlayerShipTemplate;

/**
 * Интерфейс для реализации фракции игроков.
 * 
 * @author Ronn
 */
public interface Fraction {

	/**
	 * Выдача стартовых предметов кораблю.
	 * 
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void addItemsTo(PlayerShip playerShip, LocalObjects local);

	/**
	 * @param playerShip выдача стартовых модулей кораблю.
	 * @param local контейнер локальных объектов.
	 */
	public void addModulesTo(PlayerShip playerShip, LocalObjects local);

	/**
	 * @param playerShip выдача стартовых заданий кораблю.
	 * @param local контейнер локальных объектов.
	 */
	public void addQuestsTo(PlayerShip playerShip, LocalObjects local);

	/**
	 * @return история фракци.
	 */
	public String getHistory();

	/**
	 * @return ид фракции.
	 */
	public int getId();

	/**
	 * @return основная иконка фракции.
	 */
	public String getMainIcon();

	/**
	 * @return название фракции.
	 */
	public String getName();

	/**
	 * @return шаблон стартового корабля.
	 */
	public PlayerShipTemplate getShipTemplate();

	/**
	 * @return полностью ли готова фракции для работы на сервере.
	 */
	public boolean isReady();

	/**
	 * Размещение возможностей корабля на панели быстрого доступа.
	 * 
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void moveSkillsToPanel(PlayerShip playerShip, LocalObjects local);

	/**
	 * @param playerShip перемещение корабля на стартовую позицию.
	 * @param local контейнер локальных объектов.
	 */
	public void moveToStart(PlayerShip playerShip, LocalObjects local);
}

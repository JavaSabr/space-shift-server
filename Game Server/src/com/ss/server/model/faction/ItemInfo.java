package com.ss.server.model.faction;

import com.ss.server.template.item.ItemTemplate;

/**
 * Информация о выдоваемом предмете.
 * 
 * @author Ronn
 */
public class ItemInfo {

	/** шаблон выдаваемого предмета */
	private final ItemTemplate template;

	/** кол-во выдаваемых предметов */
	private final long count;

	public ItemInfo(final ItemTemplate template, final long count) {
		this.template = template;
		this.count = count;
	}

	/**
	 * @return кол-во выдаваемых предметов.
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @return шаблон выдаваемого предмета.
	 */
	public ItemTemplate getTemplate() {
		return template;
	}

	@Override
	public String toString() {
		return "ItemInfo [template=" + template + ", count=" + count + "]";
	}
}

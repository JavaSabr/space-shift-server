package com.ss.server.model.teleport;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;

/**
 * Интерфейс для реализации обработчика тлепортирования объекта.
 * 
 * @author Ronn
 */
public interface TeleportHandler {

	public default long finish(final SpaceObject object, final LocalObjects local) {
		return 0;
	}

	public default void handle(final SpaceObject object, final LocalObjects local) {
	}

	public default long start(final SpaceObject object, final LocalObjects local) {
		return 0;
	}
}

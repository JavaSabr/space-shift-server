package com.ss.server.model.teleport;

import static java.lang.System.currentTimeMillis;
import rlib.concurrent.atomic.AtomicReference;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.task.UpdateTask;

/**
 * Реализация задачи по телепортированию объекта.
 * 
 * @author Ronn
 */
public class TeleportObjectTask implements Foldable, UpdateTask {

	private static final Logger LOGGER = LoggerManager.getLogger(TeleportObjectTask.class);

	/** ссылка на обработчик телепортации */
	private final AtomicReference<TeleportHandler> handlerReference;

	/** телепортирующийся объект */
	private final SpaceObject object;

	/** время выполнения финиша телепортации */
	private long finishTime;
	/** время выполнения обработки завершения телепортации */
	private long handleTime;

	public TeleportObjectTask(final SpaceObject object) {
		this.object = object;
		this.handlerReference = new AtomicReference<>();
	}

	private void cancel() {

		final AtomicReference<TeleportHandler> ref = getHandlerReference();
		ref.set(null);

		setFinishTime(0);
		setHandleTime(0);
	}

	@Override
	public void finalyze() {
		setHandler(null);
	}

	/**
	 * @return время выполнения финиша телепортации.
	 */
	protected long getFinishTime() {
		return finishTime;
	}

	/**
	 * @return ссылка на обработчик телепортации.
	 */
	protected AtomicReference<TeleportHandler> getHandlerReference() {
		return handlerReference;
	}

	/**
	 * @return время выполнения обработки завершения телепортации.
	 */
	protected long getHandleTime() {
		return handleTime;
	}

	/**
	 * @return телепортирующийся объект.
	 */
	public SpaceObject getObject() {
		return object;
	}

	/**
	 * @param finishTime время выполнения финиша телепортации.
	 */
	protected void setFinishTime(final long finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * @param handler ссылка на обработчик телепортации.
	 */
	protected void setHandler(final TeleportHandler handler) {
		this.handlerReference.set(null);
	}

	/**
	 * @param handleTime время выполнения обработки завершения телепортации.
	 */
	protected void setHandleTime(final long handleTime) {
		this.handleTime = handleTime;
	}

	/**
	 * Запуск телепортации объекта.
	 * 
	 * @param handler обработчик телепортации.
	 * @param local контейнер локальных объектов.
	 */
	public void startTeleport(final TeleportHandler handler, final LocalObjects local) {

		final AtomicReference<TeleportHandler> ref = getHandlerReference();

		synchronized(ref) {

			final TeleportHandler prev = ref.getAndSet(handler);

			if(prev != null) {
				LOGGER.warning("interrupted " + prev);
				return;
			}

			final long result = handler.start(getObject(), local);

			if(result == -1) {
				cancel();
				return;
			}

			setFinishTime(currentTimeMillis() + result);
		}
	}

	@Override
	public void update(final LocalObjects local, final long currentTime) {

		final AtomicReference<TeleportHandler> ref = getHandlerReference();
		final TeleportHandler handler = ref.get();

		if(handler == null) {
			return;
		}

		// время завершения телепортации
		long finishTime = getFinishTime();
		// время выполнение обработки, обработка выполняется после завершения
		// телепортации
		long handleTime = getHandleTime();

		if(finishTime == 0 && handleTime == 0) {
			return;
		}

		// если настало время завершать телепортацию
		if(finishTime > 0 && currentTime > finishTime) {

			synchronized(ref) {

				if(ref.get() != handler) {
					return;
				}

				finishTime = getFinishTime();

				if(finishTime == 0 || finishTime > currentTime) {
					return;
				}

				final long result = handler.finish(getObject(), local);

				if(result == -1) {
					cancel();
					return;
				}

				setFinishTime(0);
				setHandleTime(currentTime + result);
			}

		}
		// если телепортация завершена и настало время обработать результат
		else if(handleTime > 0 && currentTime > handleTime) {

			synchronized(ref) {

				if(ref.get() != handler) {
					return;
				}

				handleTime = getHandleTime();

				if(handleTime == 0 || handleTime > currentTime) {
					return;
				}

				ref.set(null);
				setHandleTime(0);

				handler.handle(getObject(), local);
			}
		}
	}
}

package com.ss.server.model.teleport.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.PacketUtils;

/**
 * Реализация обработчика телепорта корабля на станцию.
 * 
 * @author Ronn
 */
public class ShipToStationTeleportHandler extends AbstractTeleportHandler<PlayerShip> {

	public static final int TELEPORT_PHASE_INTERVAL = 500;

	private static final ShipToStationTeleportHandler INSTANCE = new ShipToStationTeleportHandler();

	public static ShipToStationTeleportHandler getInstance() {
		return INSTANCE;
	}

	@Override
	protected long finishImpl(final PlayerShip object, final LocalObjects local) {

		PacketUtils.finishTeleport(local, object);

		if(object.isDestructed()) {
			return -1;
		}

		return TELEPORT_PHASE_INTERVAL;
	}

	@Override
	protected void handleImpl(final PlayerShip object, final LocalObjects local) {

		if(object.isDestructed()) {
			return;
		}

		object.finishSkills(local);
		object.decayMe(local);

		final AtomicBoolean onStation = object.getOnStation();

		synchronized(onStation) {

			if(onStation.get()) {
				object.sendMessage("Вы уже находитесь на станции.", local);
				LOGGER.warning(this, "playerShip already on station.");
				return;
			}

			onStation.set(true);

			PacketUtils.shipTeleportedToStation(local, object);
		}
	}

	@Override
	protected long startImpl(final PlayerShip object, final LocalObjects local) {
		PacketUtils.startTeleport(local, object);
		return TELEPORT_PHASE_INTERVAL;
	}
}

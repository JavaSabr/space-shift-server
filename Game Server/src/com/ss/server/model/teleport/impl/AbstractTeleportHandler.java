package com.ss.server.model.teleport.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.teleport.TeleportHandler;

/**
 * Базовая реализация обработчика телепортации объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractTeleportHandler<T extends SpaceObject> implements TeleportHandler {

	protected static final Logger LOGGER = LoggerManager.getLogger(TeleportHandler.class);

	@Override
	@SuppressWarnings("unchecked")
	public long finish(final SpaceObject object, final LocalObjects local) {
		return finishImpl((T) object, local);
	}

	protected long finishImpl(final T object, final LocalObjects local) {
		return 0;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void handle(final SpaceObject object, final LocalObjects local) {
		handleImpl((T) object, local);
	}

	protected void handleImpl(final T object, final LocalObjects local) {
	}

	@Override
	@SuppressWarnings("unchecked")
	public long start(final SpaceObject object, final LocalObjects local) {
		return startImpl((T) object, local);
	}

	protected long startImpl(final T object, final LocalObjects local) {
		return 0;
	}
}

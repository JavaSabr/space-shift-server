package com.ss.server.model.teleport.impl;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.item.spawn.ItemSpawn;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.storage.ItemLocation;
import com.ss.server.model.storage.StorageUtils;
import com.ss.server.model.teleport.TeleportHandler;
import com.ss.server.network.PacketUtils;

/**
 * Реализация обработчика телепортации предмета в хранилище.
 * 
 * @author Ronn
 */
public class ItemToStorageTeleportHandler extends AbstractTeleportHandler<SpaceItem> {

	public static TeleportHandler getInstance() {
		return INSTANCE;
	}

	private static final TeleportHandler INSTANCE = new ItemToStorageTeleportHandler();

	@Override
	protected long finishImpl(final SpaceItem item, final LocalObjects local) {

		final SpaceShip locker = item.getLocker();

		if(locker == null) {
			return -1;
		}

		boolean teleported = false;

		synchronized(item) {

			if(!item.isVisible() || item.getItemLocation() != ItemLocation.IN_SPACE) {
				return -1;
			}

			teleported = StorageUtils.finishTeleportTo(locker, item, local);
		}

		if(teleported) {
			PacketUtils.finishTeleport(local, item);
			return 500;
		}

		return -1;
	}

	@Override
	protected void handleImpl(final SpaceItem item, final LocalObjects local) {

		final ItemSpawn spawn = item.getSpawn();

		if(spawn != null) {
			spawn.notifyFinish(item);
			item.setSpawn(null);
		}

		final int ownerId = item.getOwnerId();

		if(ownerId == 0) {
			item.deleteMe(local);
		} else {
			item.decayMe(local);
		}
	}

	@Override
	protected long startImpl(final SpaceItem item, final LocalObjects local) {
		PacketUtils.startTeleport(local, item);
		return 500;
	}

}

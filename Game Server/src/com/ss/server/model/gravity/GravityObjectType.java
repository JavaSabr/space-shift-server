package com.ss.server.model.gravity;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.gravity.impl.Asteroid;
import com.ss.server.model.gravity.impl.Star;

/**
 * Переисление типов гравитационных объектов.
 * 
 * @author Ronn
 */
public enum GravityObjectType implements SpaceObjectType {
	/** модель астеройда */
	ASTEROID(Asteroid.class),
	/** модель планеты */
	PLANET(Star.class),
	/** модель луны */
	MOON(Star.class),
	/** модель звезды */
	STAR(Star.class);

	/** тип гравитационного объекта */
	private Class<? extends GravityObject> instanceClass;

	private GravityObjectType(final Class<? extends GravityObject> instanceClass) {
		this.instanceClass = instanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public int index() {
		return ordinal();
	}
}

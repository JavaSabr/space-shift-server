package com.ss.server.model.gravity.impl;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.impl.AbstractSpaceObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseGravityObjectInfo;
import com.ss.server.network.game.packet.server.ResponseObjectDelete;
import com.ss.server.network.game.packet.server.ResponseUpdateGravityObject;
import com.ss.server.task.impl.GravityFlyTask;
import com.ss.server.template.GravityObjectTemplate;

/**
 * Базовая модель гравитационного объекта.
 * 
 * @author Ronn
 */
public abstract class AbstractGravityObject<T extends GravityObjectTemplate> extends AbstractSpaceObject<T> implements GravityObject {

	/** объекты под управлением гравитации */
	protected final Array<GravityObject> childs;
	/** содержимые объекты */
	protected final Array<SpaceObject> objects;

	/** обработчик полета объекта */
	protected final GravityFlyTask gravityTask;

	/** наклон орбиты */
	protected final Rotation orbitalRotation;

	/** владеющий объект */
	protected GravityObject parent;

	/** стартовое время оборота */
	protected long startTime;
	/** время оборота вокруг орбиты */
	protected long turnAroundTime;

	/** время оборота вокруг своей оси */
	protected int turnTime;
	/** дистанция от центра орбиты */
	protected int distance;

	public AbstractGravityObject(final int objectId, final T template) {
		super(objectId, template);

		this.childs = ArrayFactory.newConcurrentArray(GravityObject.class);
		this.objects = ArrayFactory.newConcurrentArray(SpaceObject.class);
		this.orbitalRotation = Rotation.newInstance();
		this.gravityTask = new GravityFlyTask(this);
	}

	@Override
	public void add(final SpaceObject object) {
		getObjects().add(object);
	}

	@Override
	public void addChild(final GravityObject object) {

		final Array<GravityObject> childs = getChilds();

		childs.writeLock();
		try {
			childs.add(object);
			object.setParent(this);
		} finally {
			childs.writeUnlock();
		}
	}

	@Override
	public void addMe(final PlayerShip playerShip, final SpaceLocation location, final LocalObjects local) {
		playerShip.sendPacket(getInfoPacket(), true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("add me " + this + " to " + playerShip);
		}
	}

	@Override
	public boolean contains(final SpaceObject object) {
		return getObjects().contains(object);
	}

	@Override
	public void deleteMe(final LocalObjects local) {
		super.deleteMe(local);

		final Array<GravityObject> childs = getChilds();

		if(childs.isEmpty()) {
			return;
		}

		childs.readLock();
		try {

			for(final GravityObject object : childs.array()) {

				if(object == null) {
					break;
				}

				object.deleteMe(local);
			}

		} finally {
			childs.readUnlock();
		}
	}

	@Override
	public void flyChild(final Vector changed, final LocalObjects local) {

		final Array<SpaceObject> objects = getObjects();

		if(objects.isEmpty()) {
			return;
		}

		objects.readLock();
		try {

			for(final SpaceObject object : objects.array()) {

				if(object == null) {
					break;
				}

				final Vector location = object.getLocation();

				location.addLocal(changed);

				object.setLocation(location, changed, local);
			}

		} finally {
			objects.readUnlock();
		}
	}

	@Override
	public Array<GravityObject> getChilds() {
		return childs;
	}

	@Override
	public int getClassId() {
		return Config.SERVER_GRAVITY_OBJECT_CLASS_ID;
	}

	@Override
	public int getDistance() {
		return distance;
	}

	@Override
	public double getFlyDone() {
		return gravityTask.getDone();
	}

	/**
	 * @return пакет с информацией об объекте.
	 */
	protected ServerPacket getInfoPacket() {
		return ResponseGravityObjectInfo.getInstance(this);
	}

	@Override
	public Array<SpaceObject> getObjects() {
		return objects;
	}

	@Override
	public Rotation getOrbitalRotation() {
		return orbitalRotation;
	}

	@Override
	public GravityObject getParent() {
		return parent;
	}

	@Override
	public int getRadius() {
		return template.getRadius();
	}

	@Override
	public long getStartTime() {
		return startTime;
	}

	@Override
	public long getTurnAroundTime() {
		return turnAroundTime;
	}

	@Override
	public int getTurnTime() {
		return turnTime;
	}

	@Override
	public void remove(final SpaceObject object) {
		getObjects().fastRemove(object);
	}

	@Override
	public void removeChild(final GravityObject object) {

		final Array<GravityObject> childs = getChilds();

		childs.writeLock();
		try {
			childs.fastRemove(object);
			object.setParent(null);
		} finally {
			childs.writeUnlock();
		}
	}

	@Override
	public void removeMe(final PlayerShip playerShip, final SpaceLocation location, final LocalObjects local) {
		playerShip.sendPacket(ResponseObjectDelete.getInstance(this, local), true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("remove me " + this + " to " + playerShip);
		}

		final Array<GravityObject> childs = getChilds();

		if(childs.isEmpty()) {
			return;
		}

		childs.readLock();
		try {

			for(final GravityObject object : childs.array()) {

				if(object == null) {
					break;
				}

				object.removeMe(playerShip, location, local);
			}

		} finally {
			childs.readUnlock();
		}
	}

	@Override
	public void setDistance(final int distance) {
		this.distance = distance;
	}

	@Override
	public void setLocation(final Vector vector, final Vector changed, final LocalObjects local) {
		super.setLocation(vector, changed, local);
		flyChild(changed, local);
	}

	@Override
	public void setOrbitalRotation(final Rotation orbitalRotation) {
		this.orbitalRotation.set(orbitalRotation);
	}

	@Override
	public void setParent(final GravityObject object) {
		this.parent = object;
	}

	@Override
	public void setStartTime(final long startTime) {
		this.startTime = startTime;
		this.gravityTask.setLastTime(startTime);
	}

	@Override
	public void setTurnAroundTime(final long turnAroundTime) {
		this.turnAroundTime = turnAroundTime;
	}

	@Override
	public void setTurnTime(final int turnTime) {
		this.turnTime = turnTime;
	}

	@Override
	public void spawnMe(final LocalObjects local) {
		super.spawnMe(local);

		SPACE.addGravityObject(this);

		final Array<GravityObject> childs = getChilds();

		if(childs.isEmpty()) {
			return;
		}

		childs.readLock();
		try {

			for(final GravityObject object : childs.array()) {

				if(object == null) {
					break;
				}

				object.spawnMe(local);
			}

		} finally {
			childs.readUnlock();
		}
	}

	@Override
	public void syncFor(final SpaceObject object, final LocalObjects local) {
		super.syncFor(object, local);
		object.sendPacket(ResponseUpdateGravityObject.getInstance(this, gravityTask.getDone()), true);
	}

	@Override
	public String toString() {
		return "type " + getTemplate().getType() + ", id " + getTemplateId();
	}

	@Override
	public void updateGravity(final long currentTime, final LocalObjects local) {
		gravityTask.update(local, currentTime);

		final Array<GravityObject> childs = getChilds();

		if(childs.isEmpty()) {
			return;
		}

		childs.readLock();
		try {

			for(final GravityObject object : childs.array()) {

				if(object == null) {
					break;
				}

				object.updateGravity(currentTime, local);
			}

		} finally {
			childs.readUnlock();
		}
	}
}

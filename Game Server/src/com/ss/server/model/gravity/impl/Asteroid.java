package com.ss.server.model.gravity.impl;

import com.ss.server.template.GravityObjectTemplate;

/**
 * Модель гравитационноо астеройда.
 * 
 * @author Ronn
 */
public class Asteroid extends AbstractGravityObject<GravityObjectTemplate> {

	public Asteroid(final int objectId, final GravityObjectTemplate template) {
		super(objectId, template);
	}
}

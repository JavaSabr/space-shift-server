package com.ss.server.model.gravity.impl;

import com.ss.server.template.GravityObjectTemplate;

/**
 * Модель звезды.
 * 
 * @author Ronn
 */
public class Star extends AbstractGravityObject<GravityObjectTemplate> {

	public Star(final int objectId, final GravityObjectTemplate template) {
		super(objectId, template);
	}
}

package com.ss.server.model.gravity;

import rlib.geom.Rotation;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.ObjectContainer;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Интерфейс для реализации гравитационного объекта.
 * 
 * @author Ronn
 */
public interface GravityObject extends SpaceObject, ObjectContainer {

	/**
	 * @param object добавить объект в гравитационное поле.
	 */
	public void addChild(GravityObject object);

	/**
	 * Добавление объекта для игрока в указанной локации.
	 * 
	 * @param ship кому добавляемся.
	 * @param location локация, в которой находится объект.
	 * @param local контейнер локальных объектов.
	 */
	public void addMe(PlayerShip ship, SpaceLocation location, final LocalObjects local);

	/**
	 * @return объекты в гравитационном поле.
	 */
	public Array<GravityObject> getChilds();

	/**
	 * @return дистанция от центра.
	 */
	public int getDistance();

	/**
	 * @return выполненность полета вокруг центра.
	 */
	public double getFlyDone();

	@Override
	public default GravityObject getGravityObject() {
		return this;
	}

	/**
	 * @return разворот орбиты.
	 */
	public Rotation getOrbitalRotation();

	/**
	 * @return в чьем гравитацинном поле находится.
	 */
	public GravityObject getParent();

	/**
	 * @return радиус объекта.
	 */
	public int getRadius();

	/**
	 * @return стартовое время оборота.
	 */
	public long getStartTime();

	/**
	 * @return время вращения вокруг своей орбиты.
	 */
	public long getTurnAroundTime();

	/**
	 * @return время вращения вокруг своей оси.
	 */
	public int getTurnTime();

	@Override
	public default boolean isGravityObject() {
		return true;
	}

	/**
	 * @param object цудалить объект из гравитационного поля.
	 */
	public void removeChild(GravityObject object);

	/**
	 * Удаление объекта для игрока в указанной локации.
	 * 
	 * @param ship у кого удаляемся.
	 * @param location локация, в которой находится объект.
	 * @param local контейнер локальных объектов.
	 */
	public void removeMe(PlayerShip ship, SpaceLocation location, LocalObjects local);

	/**
	 * @param distance дистанция от центра.
	 */
	public void setDistance(int distance);

	/**
	 * @param orbitalRotation разворот орбиты.
	 */
	public void setOrbitalRotation(Rotation orbitalRotation);

	/**
	 * @param object в чьем гравитацинном поле находится.
	 */
	public void setParent(GravityObject object);

	/**
	 * @param startTime стартовое время оборота.
	 */
	public void setStartTime(long startTime);

	/**
	 * @param turnAroundTime время вращения вокруг своей орбиты.
	 */
	public void setTurnAroundTime(long turnAroundTime);

	/**
	 * @param turnTime время вращения вокруг своей оси.
	 */
	public void setTurnTime(int turnTime);

	/**
	 * Обновить гравитационных объект.
	 * 
	 * @param currentTime текущее время.
	 * @param local контейнер локальных объектов.
	 */
	public void updateGravity(long currentTime, LocalObjects local);;
}

package com.ss.server.model.impact;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageType;

/**
 * Класс с содержанием информации об атаке.
 * 
 * @author Ronn
 */
public final class ImpactInfo {

	/** тип воздействия */
	private ImpactType impactType;
	/** тип урона */
	private DamageType damageType;

	/** воздействующий объект */
	private SpaceObject effector;

	/** значение воздействия */
	private int value;

	/** было ли воздействие в щит */
	private boolean onShield;

	/**
	 * @return очистка контейнера.
	 */
	public ImpactInfo clear() {
		setImpactType(null);
		setEffector(null);
		setValue(0);
		setOnShield(false);
		setDamageType(DamageType.NONE);
		return this;
	}

	/**
	 * @return тип урона.
	 */
	public DamageType getDamageType() {
		return damageType;
	}

	/**
	 * @return воздействующий объект.
	 */
	public SpaceObject getEffector() {
		return effector;
	}

	/**
	 * @return тип воздействия.
	 */
	public ImpactType getImpactType() {
		return impactType;
	}

	/**
	 * @return значение воздействия.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @return было ли воздействие в щит.
	 */
	public boolean isOnShield() {
		return onShield;
	}

	/**
	 * @param damageType тип урона.
	 */
	public void setDamageType(final DamageType damageType) {
		this.damageType = damageType;
	}

	/**
	 * @param effector воздействующий объект.
	 */
	public void setEffector(final SpaceObject effector) {
		this.effector = effector;
	}

	/**
	 * @param impactType тип воздействия.
	 */
	public void setImpactType(final ImpactType impactType) {
		this.impactType = impactType;
	}

	/**
	 * @param onShield было ли воздействие в щит.
	 */
	public void setOnShield(final boolean onShield) {
		this.onShield = onShield;
	}

	/**
	 * @param value значение воздействия.
	 */
	public void setValue(final int value) {
		this.value = value;
	}
}

package com.ss.server.model.impact.handler.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.impact.handler.ImpactHandler;
import com.ss.server.model.skills.Skill;

/**
 * Базовая реализация обработчика воздействия.
 * 
 * @author Ronn
 */
public abstract class AbstractImpactHandler implements ImpactHandler {

	protected static final Logger LOGGER = LoggerManager.getLogger(ImpactHandler.class);

	/**
	 * Проверка входящих аргументов.
	 * 
	 * @param effector корабль, который воздействует на обхектю
	 * @param effected объект, на который воздействует корабль.
	 * @param skill умение, которым было произведено воздействие.
	 * @param impactInfo информация о воздействии.
	 * @return можно ли продожлать обработку.
	 */
	protected boolean checkArguments(final SpaceObject effector, final SpaceObject effected, final Skill skill, final ImpactInfo impactInfo) {
		return effector != null && effected != null && impactInfo != null;
	}
}

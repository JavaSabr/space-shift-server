package com.ss.server.model.impact.handler.impl;

import static com.ss.server.model.rating.local.LocalRatingElementType.SPACE_SHIP_BODY;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ShipEventManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.damage.DamageCalculator;
import com.ss.server.model.damage.DamageType;
import com.ss.server.model.damage.ElementType;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.rating.event.local.kill.KillRatingEvent;
import com.ss.server.model.ship.ForceShield;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;

/**
 * Реализация нанесения урона по кораблю.
 * 
 * @author Ronn
 */
public class ShipDamageImpactHandler extends AbstractImpactHandler {

	public static ShipDamageImpactHandler getInstance() {
		return INSTANCE;
	}

	private static final ShipDamageImpactHandler INSTANCE = new ShipDamageImpactHandler();

	@Override
	protected boolean checkArguments(final SpaceObject effector, final SpaceObject effected, final Skill skill, final ImpactInfo impactInfo) {
		return super.checkArguments(effector, effected, skill, impactInfo) && skill != null && effected.isSpaceShip();
	}

	@Override
	public void handle(final SpaceObject effector, final SpaceObject effected, final Skill skill, final ImpactInfo impactInfo, final LocalObjects local) {

		if(!checkArguments(effector, effected, skill, impactInfo)) {
			LOGGER.warning(new Exception("invalid arguments"));
			return;
		}

		final SpaceShip attacker = effector.getSpaceShip();
		final SpaceShip target = effected.getSpaceShip();

		if(target.isDestructed()) {
			return;
		}

		final ShipEventManager eventManager = ShipEventManager.getInstance();
		eventManager.notifyAttack(attacker, effected.getSpaceShip(), skill, impactInfo, local);

		final Array<ForceShield> forceShields = target.getForceShields();

		if(!forceShields.isEmpty()) {

			final Array<ForceShield> container = local.getNextForceShieldList();

			forceShields.readLock();
			try {
				container.addAll(forceShields);
			} finally {
				forceShields.readUnlock();
			}

			for(final ForceShield forceShield : container.array()) {

				if(forceShield == null) {
					break;
				}

				if(forceShield.processAbsorb(effector, impactInfo, local)) {
					impactInfo.setValue(0);
					impactInfo.setOnShield(true);
				}
			}
		}

		final DamageType damageType = impactInfo.getDamageType();
		final DamageCalculator calculator = damageType.getCalculator();

		final int strength = target.getCurrentStrength();

		// TODO определить место для получения стехии
		final int damage = calculator.getForBody(impactInfo.getValue(), ElementType.NONE);

		if(damage > 0) {

			attacker.sendMessage("нанесено " + damage + " урона цели.", local);

			target.sendMessage("получено " + damage + " урона.", local);
			target.setCurrentStrength(strength - damage, local);

			final RatingSystem ratingSystem = target.getRatingSystem();
			ratingSystem.notify(RatingEvents.getHitSpaceShipBodyRatingEvent(local, SPACE_SHIP_BODY));
		}

		target.updateStatus(local);

		if(target.isDestructed()) {

			target.doDestruct(effector, local);

			final DestroyRatingEvent destroyEvent = RatingEvents.getDestroyRatingEvent(local);
			destroyEvent.setDamageType(damageType);
			destroyEvent.setDestroyed(target);
			destroyEvent.setDestroyer(attacker);

			final KillRatingEvent killEvent = RatingEvents.getKillRatingEvent(local);
			killEvent.setKilled(target);
			killEvent.setKiller(attacker);
			killEvent.setDamageType(damageType);

			RatingSystem ratingSystem = target.getRatingSystem();
			ratingSystem.notify(destroyEvent);

			ratingSystem = attacker.getRatingSystem();
			ratingSystem.notify(killEvent);
		}
	}
}

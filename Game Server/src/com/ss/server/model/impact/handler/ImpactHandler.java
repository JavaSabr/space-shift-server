package com.ss.server.model.impact.handler;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impact.ImpactInfo;
import com.ss.server.model.skills.Skill;

/**
 * Интерфейс для реализации обработчика воздействия корабля на объект.
 * 
 * @author Ronn
 */
public interface ImpactHandler {

	/**
	 * Обработка воздействия корабля на объект.
	 * 
	 * @param effector корабль, который воздействует на обхектю
	 * @param effected объект, на который воздействует корабль.
	 * @param skill умение, которым было произведено воздействие.
	 * @param impactInfo информация о воздействии.
	 * @param local контейнер локальных объектов.
	 */
	public void handle(SpaceObject effector, SpaceObject effected, Skill skill, ImpactInfo impactInfo, LocalObjects local);
}

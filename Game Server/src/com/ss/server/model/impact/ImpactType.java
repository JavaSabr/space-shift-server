package com.ss.server.model.impact;

/**
 * Перечисленте типов воздействия.
 * 
 * @author Ronn
 */
public enum ImpactType {

	DAMAGE, ;

	public static final ImpactType[] VALUES = values();

	public static final int SIZE = VALUES.length;
}

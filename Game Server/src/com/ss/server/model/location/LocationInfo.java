package com.ss.server.model.location;

import java.awt.Color;

import rlib.geom.Vector;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;

import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.location.object.LocationObject;

/**
 * Модель информации о локации.
 * 
 * @author Ronn
 */
public final class LocationInfo {

	protected static final Logger LOGGER = LoggerManager.getLogger(LocationInfo.class);

	public static final String PROP_SECTORS_Z = "sectorsZ";
	public static final String PROP_SECTORS_Y = "sectorsY";
	public static final String PROP_SECTORS_X = "sectorsX";
	public static final String PROP_VECTOR_STAR = "vectorStar";
	public static final String PROP_AMBIENT_COLOR = "ambientColor";
	public static final String PROP_SHADOW_LIGHT_COLOR = "shadowLightColor";
	public static final String PROP_STAR_LIGHT_COLOR = "starLightColor";
	public static final String PROP_BACKGROUND = "background";
	public static final String PROP_LIGHTS = "lights";
	public static final String PROP_GRAVITY = "gravity";
	public static final String PROP_LOCATION_OBJECTS = "objects";
	public static final String PROP_NAME = "name";
	public static final String PROP_ID = "id";

	/** список гравитационных объектов в локации */
	private final GravityObject[] gravity;
	/** список всех локационных объектов */
	private final LocationObject[] objects;

	/** характеристики звезд в локации */
	private final LightInfo[] lights;

	/** позиция звезды в локации */
	private final Vector vectorStar;

	/** описание светового фона локации */
	private final Color starLightColor;
	private final Color shadowLightColor;
	private final Color ambientColor;

	/** информация о космическом фоне */
	private final BackgroundInfo backgroundInfo;

	/** название локации */
	private final String name;

	/** ид локации */
	private final int id;

	/** кол-во секторов в локации по X */
	private final int sectorsX;
	/** кол-во секторов в локации по Y */
	private final int sectorsY;
	/** кол-во секторов в локации по Z */
	private final int sectorsZ;

	public LocationInfo(final VarTable vars) {
		this.id = vars.getInteger(PROP_ID);
		this.name = vars.getString(PROP_NAME);
		this.gravity = vars.get(PROP_GRAVITY, GravityObject[].class);
		this.objects = vars.get(PROP_LOCATION_OBJECTS, LocationObject[].class);
		this.lights = vars.get(PROP_LIGHTS, LightInfo[].class);
		this.vectorStar = vars.get(PROP_VECTOR_STAR, Vector.class);
		this.backgroundInfo = vars.get(PROP_BACKGROUND, BackgroundInfo.class);
		this.starLightColor = vars.get(PROP_STAR_LIGHT_COLOR, Color.class);
		this.shadowLightColor = vars.get(PROP_SHADOW_LIGHT_COLOR, Color.class);
		this.ambientColor = vars.get(PROP_AMBIENT_COLOR, Color.class);
		this.sectorsX = vars.getInteger(PROP_SECTORS_X, SpaceLocation.SECTORS_X);
		this.sectorsY = vars.getInteger(PROP_SECTORS_Y, SpaceLocation.SECTORS_Y);
		this.sectorsZ = vars.getInteger(PROP_SECTORS_Z, SpaceLocation.SECTORS_Z);
	}

	@Override
	public boolean equals(final Object obj) {

		if(this == obj) {
			return true;
		}

		if(obj == null) {
			return false;
		}

		if(getClass() != obj.getClass()) {
			return false;
		}

		final LocationInfo other = (LocationInfo) obj;

		return id == other.id;
	}

	/**
	 * @return цвет фоновоо освещения.
	 */
	public Color getAmbientColor() {
		return ambientColor;
	}

	/**
	 * @return информация о фоне локации.
	 */
	public BackgroundInfo getBackgroundInfo() {
		return backgroundInfo;
	}

	/**
	 * @return список гравитационных объектов.
	 */
	public GravityObject[] getGravity() {
		return gravity;
	}

	/**
	 * @return ид локации.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return набор источников света.
	 */
	public LightInfo[] getLights() {
		return lights;
	}

	/**
	 * @return название локации.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return список всех локационных объектов.
	 */
	public LocationObject[] getObjects() {
		return objects;
	}

	/**
	 * @return кол-во секторов в локации по X.
	 */
	public int getSectorsX() {
		return sectorsX;
	}

	/**
	 * @return кол-во секторов в локации по Y.
	 */
	public int getSectorsY() {
		return sectorsY;
	}

	/**
	 * @return кол-во секторов в локации по Z.
	 */
	public int getSectorsZ() {
		return sectorsZ;
	}

	/**
	 * @return цвет подсветки тени.
	 */
	public Color getShadowLightColor() {
		return shadowLightColor;
	}

	/**
	 * @return цвет подсветки света звезд.
	 */
	public Color getStarLightColor() {
		return starLightColor;
	}

	/**
	 * @return вектор нахождения звезды в локации.
	 */
	public Vector getVectorStar() {
		return vectorStar;
	}
}

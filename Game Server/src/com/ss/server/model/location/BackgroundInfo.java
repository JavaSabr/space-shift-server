package com.ss.server.model.location;

import rlib.util.VarTable;

/**
 * Контейнер информации о текстурах фона локации.
 * 
 * @author Ronn
 */
public final class BackgroundInfo {

	/**
	 * Перечисление сторон фона.
	 * 
	 * @author Ronn
	 */
	public static enum BackgroundSide {

		WEST,
		EAST,
		NORTH,
		SOUTH,
		UP,
		DOWN;

		public static final BackgroundSide[] VALUES = values();
	}

	public static final String KEY = "key";
	public static final String VALUE = "value";

	public static final String TYPE = "type";

	/** набор путей к текстурам фона */
	private final String[] keys;

	public BackgroundInfo(final VarTable vars) {

		keys = new String[BackgroundSide.VALUES.length];

		for(final BackgroundSide side : BackgroundSide.VALUES) {
			keys[side.ordinal()] = vars.getString(side.name());
		}
	}

	/**
	 * @return список ключей к текстуре фона.
	 */
	public String[] getKeys() {
		return keys;
	}
}

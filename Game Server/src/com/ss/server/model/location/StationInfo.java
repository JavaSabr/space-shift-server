package com.ss.server.model.location;

import rlib.geom.Vector;
import rlib.util.VarTable;

/**
 * Модель инфы о размещении станций в локациях.
 * 
 * @author Ronn
 */
public class StationInfo {

	/** координаты станции */
	private final Vector location;

	/** ид темплейта станции */
	private final int templateId;

	public StationInfo(final VarTable vars) {
		this.templateId = vars.getInteger("templateId");
		this.location = vars.getVector("location");
	}

	/**
	 * @return местоположение станции.
	 */
	public final Vector getLocation() {
		return location;
	}

	/**
	 * @return темплейт ид станции.
	 */
	public final int getTemplateId() {
		return templateId;
	}
}

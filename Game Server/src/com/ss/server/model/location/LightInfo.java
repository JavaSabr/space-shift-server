package com.ss.server.model.location;

import rlib.geom.Vector;
import rlib.graphics.color.ColorRGBA;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

/**
 * Контейнер информации, описывающая источник сввета в локации.
 * 
 * @author Ronn
 */
public final class LightInfo {

	public static enum LightType {
		AMBIENT,
		SPOT,
		POINT,
		DIRECTION,
	}

	public static final String MAIN = "main";
	public static final String POSITION = "position";
	public static final String TYPE = "type";
	public static final String COLOR = "color";

	public static final String RADIUS = "radius";

	/** позиция */
	private final Vector position;
	/** цвет света */
	private final ColorRGBA color;
	/** тип света */
	private final LightType lightType;

	/** радиус света */
	private final int radius;

	/** является ли основным источником света */
	private final boolean main;

	public LightInfo(final VarTable vars) {
		this.radius = vars.getInteger(RADIUS, 0);
		this.main = vars.getBoolean(MAIN, false);

		final float[] coords = vars.getFloatArray(POSITION, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.position = Vector.newInstance(coords);
		this.lightType = vars.getEnum(TYPE, LightType.class, LightType.POINT);

		final float[] colors = vars.getFloatArray(COLOR, ",");

		this.color = new ColorRGBA(colors[0], colors[1], colors[2], 1);
	}

	/**
	 * @return цвет освещения.
	 */
	public final ColorRGBA getColor() {
		return color;
	}

	/**
	 * @return тип света.
	 */
	public LightType getLightType() {
		return lightType;
	}

	/**
	 * @return позиция освещения.
	 */
	public final Vector getPosition() {
		return position;
	}

	/**
	 * @return радиус освещения.
	 */
	public final int getRadius() {
		return radius;
	}

	/**
	 * @return является ли основным источником света.
	 */
	public boolean isMain() {
		return main;
	}
}

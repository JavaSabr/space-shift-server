package com.ss.server.model.location.object;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.location.object.impl.Asteroid;

/**
 * Перечисление типов локационных объектов.
 * 
 * @author Ronn
 */
public enum LocationObjectType implements SpaceObjectType {

	ASTEROID(Asteroid.class), ;

	/** класс реализации объекта */
	private final Class<? extends LocationObject> instanceClass;

	private LocationObjectType(final Class<? extends LocationObject> instanceClass) {
		this.instanceClass = instanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public int index() {
		return ordinal();
	}
}

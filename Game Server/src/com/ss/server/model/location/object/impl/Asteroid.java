package com.ss.server.model.location.object.impl;

import com.ss.server.template.LocationObjectTemplate;

/**
 * Реализация астеройда для заполнения локации.
 * 
 * @author Ronn
 */
public class Asteroid extends AbstractLocationObject {

	public Asteroid(final int objectId, final LocationObjectTemplate template) {
		super(objectId, template);
	}
}

package com.ss.server.model.location.object;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Интерфейс для реализации локационных объектов, предназначенных для оформление
 * локаций.
 * 
 * @author Ronn
 */
public interface LocationObject extends SpaceObject {

	@Override
	public default LocationObject getLocationObject() {
		return this;
	}

	@Override
	public default boolean isLocationObject() {
		return true;
	}

	/**
	 * Добавление объекта для игрока в указанной локации.
	 * 
	 * @param ship кому добавляемся.
	 * @param location локация, в которой находится объект.
	 */
	public void addMe(PlayerShip ship, SpaceLocation location, LocalObjects local);

	/**
	 * Удаление объекта для игрока в указанной локации.
	 * 
	 * @param ship у кого удаляемся.
	 * @param location локация, в которой находится объект.
	 * @param local контейнер локальных объектов.
	 */
	public void removeMe(PlayerShip ship, SpaceLocation location, LocalObjects local);
}

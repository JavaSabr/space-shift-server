package com.ss.server.model.location.object.impl;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.model.impl.AbstractSpaceObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.location.object.LocationObject;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.packet.server.ResponseLocationObjectInfo;
import com.ss.server.network.game.packet.server.ResponseObjectDelete;
import com.ss.server.template.LocationObjectTemplate;

/**
 * Базовая реализация объекта локации.
 * 
 * @author Ronn
 */
public abstract class AbstractLocationObject extends AbstractSpaceObject<LocationObjectTemplate> implements LocationObject {

	public AbstractLocationObject(final int objectId, final LocationObjectTemplate template) {
		super(objectId, template);
	}

	@Override
	public int getClassId() {
		return Config.SERVER_LOCATION_OBJECT_CLASS_ID;
	}

	@Override
	public void addMe(PlayerShip ship, SpaceLocation location, LocalObjects local) {
		ship.sendPacket(ResponseLocationObjectInfo.getInstance(this, local), true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("add me " + this + " to " + ship);
		}
	}

	@Override
	public void removeMe(PlayerShip ship, SpaceLocation location, LocalObjects local) {
		ship.sendPacket(ResponseObjectDelete.getInstance(this, local), true);

		if(LOGGER.isEnabledDebug()) {
			LOGGER.debug("remove me " + this + " to " + ship);
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [location=" + location + ", rotation=" + rotation + ", objectId=" + objectId + ", locationId=" + locationId + "]";
	}
}

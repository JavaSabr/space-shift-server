package com.ss.server.model.module.system;

import rlib.util.Synchronized;
import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;

/**
 * Интерфейс для реализации системы модулей корабля.
 * 
 * @author Ronn
 */
public interface ModuleSystem extends Foldable, Synchronized {

	/**
	 * Добавление модуля в систему.
	 * 
	 * @param module новый модуль.
	 * @param local контейнер локальных объектов.
	 * @return успешно ли добавлен модуль.
	 */
	public boolean addModule(Module module, LocalObjects local);

	/**
	 * Получение модуля по уникальному ид.
	 * 
	 * @param objectId ид модуля.
	 * @return искомый содуль.
	 */
	public Module getModuleId(int objectId);

	/**
	 * Получение модуля по индексу.
	 * 
	 * @param index индекс слота.
	 * @return модуль в слоте.
	 */
	public Module getModuleIndex(int index);

	/**
	 * @return кол-во вставленных модулей.
	 */
	public int getModulesCount();

	/**
	 * @return владелец системы.
	 */
	public SpaceShip getOwner();

	/**
	 * @return массив всех слотов.
	 */
	public ModuleSlot[] getSlots();

	/**
	 * @return номер слота, в котором находится модуль.
	 */
	public int indexOf(Module module);

	/**
	 * Инициализация системы по указанному набору слотов.
	 * 
	 * @param types набор слотов.
	 */
	public void init(SlotInfo[] types);

	/**
	 * Удаление модуля по индексу.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param index индекс удаляемого модуля.
	 * @return удаляемый модуль.
	 */
	public Module removeModule(LocalObjects local, int index);

	/**
	 * Установка модуля в указанный слот.
	 * 
	 * @param index индекс слота.
	 * @param module устанавливаемый модуль.
	 * @param local контейнер локальных объектов.
	 */
	public void setModule(int index, Module module, LocalObjects local);

	/**
	 * @param ship владелец системы.
	 */
	public void setOwner(SpaceShip ship);
}

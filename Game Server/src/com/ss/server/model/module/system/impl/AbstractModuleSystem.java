package com.ss.server.model.module.system.impl;

import java.util.Arrays;
import java.util.concurrent.locks.Lock;

import rlib.concurrent.lock.LockFactory;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.system.ModuleSlot;
import com.ss.server.model.module.system.ModuleSystem;
import com.ss.server.model.module.system.SlotInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.ModuleTemplate;

/**
 * Базовая модель системы модулей.
 * 
 * @author Ronn
 */
@SuppressWarnings("unchecked")
public abstract class AbstractModuleSystem<T extends SpaceShip> implements ModuleSystem {

	protected static final Logger LOGGER = LoggerManager.getLogger(AbstractModuleSystem.class);

	/** синхронизатор системы */
	protected final Lock lock;

	/** владелец системы */
	protected T owner;

	/** набор слотов в системе */
	protected ModuleSlot[] slots;

	public AbstractModuleSystem() {
		this.lock = LockFactory.newReentrantAtomicLock();
	}

	@Override
	public boolean addModule(final Module module, final LocalObjects local) {

		final T owner = getOwner();

		if(!module.isAvealablePlatform(owner.getTemplateId())) {
			LOGGER.warning("can't add module " + module + " for " + owner);
			return false;
		}

		lock();
		try {

			for(final ModuleSlot slot : getSlots()) {
				if(slot.getType() == module.getType() && slot.isEmpty()) {

					slot.setModule(module, local);
					module.setOwner(owner);

					final ModuleTemplate template = module.getTemplate();
					template.addFuncs(owner);

					if(owner != null) {
						owner.addSkills(module, module.getSkills(), local);
					}

					return true;
				}
			}

			return false;

		} finally {
			unlock();
		}
	}

	@Override
	public void finalyze() {

		final LocalObjects local = LocalObjects.get();
		final T owner = getOwner();

		for(final ModuleSlot slot : getSlots()) {

			if(slot.isEmpty()) {
				continue;
			}

			final Module module = slot.getModule();

			if(module != null) {

				final ModuleTemplate template = module.getTemplate();
				template.removeFuncs(owner);

				if(module.isActive()) {
					template.removeActiveFuncs(owner);
				}

				if(owner != null) {
					owner.removeSkills(module, module.getSkills(), local);
				}

				module.deleteMe(local);
			}

			slot.setModule(null, local);
		}
	}

	/**
	 * @return синхронизатор системы.
	 */
	protected final Lock getLock() {
		return lock;
	}

	@Override
	public Module getModuleId(final int objectId) {

		final Lock lock = getLock();
		lock.lock();
		try {

			for(final ModuleSlot slot : getSlots()) {

				if(slot.isEmpty()) {
					continue;
				}

				final Module module = slot.getModule();

				if(module.getObjectId() == objectId) {
					return module;
				}
			}

		} finally {
			lock.unlock();
		}

		return null;
	}

	@Override
	public Module getModuleIndex(final int index) {
		return slots[index].getModule();
	}

	@Override
	public int getModulesCount() {

		int counter = 0;

		final Lock lock = getLock();
		lock.lock();
		try {

			for(final ModuleSlot slot : getSlots()) {
				if(!slot.isEmpty()) {
					counter++;
				}
			}

		} finally {
			lock.unlock();
		}

		return counter;
	}

	@Override
	public T getOwner() {
		return owner;
	}

	@Override
	public ModuleSlot[] getSlots() {
		return slots;
	}

	@Override
	public int indexOf(final Module module) {

		if(module == null) {
			return -1;
		}

		final Lock lock = getLock();
		lock.lock();
		try {

			final ModuleSlot[] slots = getSlots();

			for(int i = 0, length = slots.length; i < length; i++) {

				final ModuleSlot slot = slots[i];

				if(slot.getModule() == module) {
					return i;
				}
			}

		} finally {
			lock.unlock();
		}

		return -1;
	}

	@Override
	public void init(final SlotInfo[] infos) {

		slots = new ModuleSlot[infos.length];

		for(int i = 0, length = infos.length; i < length; i++) {
			slots[i] = new ModuleSlot(infos[i].getType(), infos[i].getOffset(), i);
		}
	}

	@Override
	public void lock() {
		lock.lock();
	}

	@Override
	public void reinit() {
	}

	@Override
	public Module removeModule(final LocalObjects local, final int index) {

		final ModuleSlot[] slots = getSlots();

		if(index < 0 || index >= slots.length) {
			LOGGER.warning(getClass(), "can't remove module to index slot " + index);
			return null;
		}

		final T owner = getOwner();

		final Lock lock = getLock();
		lock.lock();
		try {

			final ModuleSlot slot = slots[index];
			final Module module = slot.getModule();

			slot.setModule(null, local);

			module.setOwner(null);
			module.getTemplate().removeFuncs(owner);

			owner.removeSkills(module, module.getSkills(), local);
			return module;

		} finally {
			lock.unlock();
		}
	}

	@Override
	public void setModule(final int index, final Module module, final LocalObjects local) {

		final ModuleSlot[] slots = getSlots();

		if(index < 0 || index >= slots.length) {
			LOGGER.warning(getClass(), "can't set module to index slot " + index);
			return;
		}

		final T owner = getOwner();

		lock();
		try {

			slots[index].setModule(module, local);

			module.setOwner(owner);

			final ModuleTemplate template = module.getTemplate();
			template.addFuncs(owner);

			if(owner != null) {
				owner.addSkills(module, module.getSkills(), local);
			}

		} finally {
			unlock();
		}
	}

	@Override
	public void setOwner(final SpaceShip owner) {
		this.owner = (T) owner;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [owner=" + owner + ", slots=" + Arrays.toString(slots) + "]";
	}

	@Override
	public void unlock() {
		lock.unlock();
	}
}

package com.ss.server.model.module.system.impl;

import com.ss.server.model.ship.nps.Nps;

/**
 * Реализация системы модулей дял NPS.
 * 
 * @author Ronn
 */
public class NpsModuleSystem extends AbstractModuleSystem<Nps> {
}

package com.ss.server.model.module.system;

import rlib.geom.Vector;

import com.ss.server.model.module.ModuleType;

/**
 * контейнер информации о слоте модуля.
 * 
 * @author Ronn
 */
public final class SlotInfo {

	/** тип модуля */
	private final ModuleType type;

	/** позиция модуля относительно корабля */
	private final Vector offset;

	public SlotInfo(final ModuleType type, final Vector offset) {
		this.type = type;
		this.offset = offset;
	}

	/**
	 * @return позиция модуля относительно корабля.
	 */
	public Vector getOffset() {
		return offset;
	}

	/**
	 * @return тип модуля.
	 */
	public ModuleType getType() {
		return type;
	}
}

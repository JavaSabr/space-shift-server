package com.ss.server.model.module.system.impl;

import com.ss.server.LocalObjects;
import com.ss.server.database.ModuleDBManager;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.player.PlayerShip;

/**
 * Модель системы моделй для игровых кораблей.
 * 
 * @author Ronn
 */
public class PlayerModuleSystem extends AbstractModuleSystem<PlayerShip> {

	private static final ModuleDBManager MODULE_DB_MANAGER = ModuleDBManager.getInstance();

	@Override
	public boolean addModule(final Module module, final LocalObjects local) {

		if(super.addModule(module, local)) {
			MODULE_DB_MANAGER.updateModule(module);
			return true;
		}

		return false;
	}

	@Override
	public Module removeModule(final LocalObjects local, final int index) {

		final Module module = super.removeModule(local, index);

		if(module != null) {
			MODULE_DB_MANAGER.updateModule(module);
		}

		return module;
	}
}

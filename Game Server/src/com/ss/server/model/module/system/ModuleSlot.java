package com.ss.server.model.module.system;

import rlib.geom.Vector;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.ModuleType;

/**
 * Модель слота модуля.
 * 
 * @author Ronn
 */
public final class ModuleSlot {

	/** позиция модуля относительно корабля */
	private final Vector offset;
	/** тип слота */
	private final ModuleType type;
	/** номер слота */
	private final int index;

	/** модуль в слоте */
	private Module module;

	public ModuleSlot(final ModuleType type, final Vector offset, final int index) {
		this.offset = offset;
		this.type = type;
		this.index = index;
	}

	/**
	 * @return номер слота.
	 */
	public final int getIndex() {
		return index;
	}

	/**
	 * @return модуль в слоте.
	 */
	public final Module getModule() {
		return module;
	}

	/**
	 * @return тип слота.
	 */
	public final ModuleType getType() {
		return type;
	}

	/**
	 * @return пустой ли слот.
	 */
	public boolean isEmpty() {
		return module == null;
	}

	/**
	 * @param module модуль для слота.
	 */
	public final void setModule(final Module module, final LocalObjects local) {
		this.module = module;

		if(module != null) {
			module.setLocation(offset, offset, local);
			module.setIndex(index);
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [offset=" + offset + ", type=" + type + ", index=" + index + ", module=" + module + "]";
	}
}

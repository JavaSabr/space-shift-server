package com.ss.server.model.module;

import com.ss.server.manager.ObjectTypeManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.module.impl.BlasterModule;
import com.ss.server.model.module.impl.EnergyGeneratorModule;
import com.ss.server.model.module.impl.EngineModule;
import com.ss.server.model.module.impl.ForceShieldModulle;
import com.ss.server.model.module.impl.RocketModule;

/**
 * Перечисление типов модулей.
 * 
 * @author Ronn
 */
public enum ModuleType implements SpaceObjectType {
	MODULE_ROCKET(RocketModule.class, "@module-type:rocket@"),
	MODULE_FORCE_SHIELD(ForceShieldModulle.class, "@module-type:forceShield@"),
	MODULE_ENERGY_GENERATOR(EnergyGeneratorModule.class, "@module-type:energyGenerator@"),
	MODULE_BLASTER(BlasterModule.class, "@module-type:blaster@"),
	MODULE_ENGINE(EngineModule.class, "@module-type:engine@");

	/** тип объектов */
	private Class<? extends Module> instanceClass;

	/** название типа модуля для локализации */
	private final String langName;

	private ModuleType(final Class<? extends Module> instanceClass, final String langName) {
		this.instanceClass = instanceClass;
		this.langName = langName;

		final ObjectTypeManager manager = ObjectTypeManager.getInstance();
		manager.register(this);
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public String getLangName() {
		return langName;
	}

	@Override
	public String getName() {
		return name();
	}

	@Override
	public int index() {
		return ordinal();
	}
}

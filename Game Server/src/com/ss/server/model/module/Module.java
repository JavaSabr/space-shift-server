package com.ss.server.model.module;

import rlib.util.pools.Foldable;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.template.ModuleTemplate;

/**
 * Интерфейс для реализации модуля корабля.
 * 
 * @author Ronn
 */
public interface Module extends SpaceObject, Foldable {

	/**
	 * @return индекс в системе.
	 */
	public int getIndex();

	/**
	 * @return текущий владелец модуля.
	 */
	public SpaceShip getOwner();

	/**
	 * @return ид владельца.
	 */
	public int getOwnerId();

	/**
	 * @return скилы модуля.
	 */
	public Skill[] getSkills();

	@Override
	public ModuleTemplate getTemplate();

	/**
	 * @return тип модуля.
	 */
	public ModuleType getType();

	/**
	 * @return активный ли модуль.
	 */
	public boolean isActive();

	/**
	 * Проверка на совместимость модуля с корпусом.
	 * 
	 * @param platformId ид корпуса.
	 * @return совместим ли модуль.
	 */
	public boolean isAvealablePlatform(int platformId);

	/**
	 * @param index индекс в системе.
	 */
	public void setIndex(int index);

	/**
	 * Установка нового владельца модуля.
	 */
	public void setOwner(SpaceShip owner);

	/**
	 * Обновить скилы модуля.
	 */
	public void updateSkills();
}

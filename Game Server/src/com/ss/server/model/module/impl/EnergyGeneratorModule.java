package com.ss.server.model.module.impl;

import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.ModuleTemplate;

/**
 * Реализация модуля генератора энергии.
 * 
 * @author Ronn
 */
public class EnergyGeneratorModule extends AbstractModule<SpaceShip> {

	public EnergyGeneratorModule(final int objectId, final ModuleTemplate template) {
		super(objectId, template);
	}
}

package com.ss.server.model.module.impl;

import static com.ss.server.table.SkillTable.createSkills;
import rlib.util.Util;

import com.ss.server.Config;
import com.ss.server.model.impl.AbstractSpaceObject;
import com.ss.server.model.module.Module;
import com.ss.server.model.module.ModuleType;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.Skill;
import com.ss.server.template.ModuleTemplate;

/**
 * Базовая реализация модуля корабля.
 * 
 * @author Ronn
 */
public abstract class AbstractModule<O extends SpaceShip> extends AbstractSpaceObject<ModuleTemplate> implements Module {

	/** владелец модуля */
	protected O owner;

	/** скилы модуля */
	protected Skill[] skills;

	/** индекс модуля в системе */
	protected int index;

	public AbstractModule(final int objectId, final ModuleTemplate template) {
		super(objectId, template);

		Util.safeExecute(() -> {
			this.skills = createSkills(template.getSkills());
		});

		updateSkills();
	}

	@Override
	public void finalyze() {
		super.finalyze();
		owner = null;
	}

	@Override
	public int getClassId() {
		return Config.SERVER_MODULE_CLASS_ID;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public Module getModule() {
		return this;
	}

	@Override
	public O getOwner() {
		return owner;
	}

	@Override
	public int getOwnerId() {
		return owner != null ? owner.getObjectId() : 0;
	}

	@Override
	public Skill[] getSkills() {
		return skills;
	}

	@Override
	public ModuleType getType() {
		return template.getType();
	}

	@Override
	public boolean isActive() {
		return false;
	}

	@Override
	public boolean isAvealablePlatform(final int platformId) {
		return template.isAvealablePlatform(platformId);
	}

	@Override
	public boolean isModule() {
		return true;
	}

	@Override
	public void setIndex(final int index) {
		this.index = index;
	}

	@Override
	public void setObjectId(final int objectId) {
		super.setObjectId(objectId);
		updateSkills();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setOwner(final SpaceShip owner) {
		this.owner = (O) owner;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [owner=" + owner + ", index=" + index + ", location=" + location + ", objectId=" + objectId + ", locationId=" + locationId + ", visible=" + visible + "]";
	}

	@Override
	public void updateSkills() {
		for(final Skill skill : getSkills()) {
			skill.setModule(this);
		}
	}
}

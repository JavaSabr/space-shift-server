package com.ss.server.model.module.impl;

import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.ModuleTemplate;

/**
 * Реализация модуля силового щита.
 * 
 * @author Ronn
 */
public class ForceShieldModulle extends AbstractModule<SpaceShip> {

	public ForceShieldModulle(final int objectId, final ModuleTemplate template) {
		super(objectId, template);
	}
}

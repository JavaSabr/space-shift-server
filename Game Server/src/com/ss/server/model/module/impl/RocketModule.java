package com.ss.server.model.module.impl;

import rlib.geom.Vector;

import com.ss.server.model.module.info.RocketShotInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.ModuleTemplate;

/**
 * Реализация ракетного модуля.
 * 
 * @author Ronn
 */
public class RocketModule extends AbstractModule<SpaceShip> {

	/** отступ старта ракеты */
	private final Vector offset;

	public RocketModule(final int objectId, final ModuleTemplate template) {
		super(objectId, template);

		final RocketShotInfo info = template.getRocketShotInfo();

		this.offset = info.getOffset();
	}

	/**
	 * @return отступ старта ракеты.
	 */
	public Vector getOffset() {
		return offset;
	}
}

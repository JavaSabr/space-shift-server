package com.ss.server.model.module.impl;

import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.ModuleTemplate;

/**
 * Модель модуля двигателя.
 * 
 * @author Ronn
 */
public class EngineModule extends AbstractModule<SpaceShip> {

	/** флаг активированности двигателя */
	private boolean activate;

	public EngineModule(final int objectId, final ModuleTemplate template) {
		super(objectId, template);
	}

	@Override
	public void finalyze() {
		setActive(false);
		super.finalyze();
	}

	@Override
	public boolean isActive() {
		return activate;
	}

	/**
	 * @param activate активирован ли двигатель.
	 */
	public final void setActive(final boolean activate) {
		this.activate = activate;
	}
}

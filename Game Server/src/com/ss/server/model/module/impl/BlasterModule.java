package com.ss.server.model.module.impl;

import rlib.geom.Vector;

import com.ss.server.model.module.info.BlasterRayInfo;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.ModuleTemplate;

/**
 * Модель модуля бластера.
 * 
 * @author Ronn
 */
public class BlasterModule extends AbstractModule<SpaceShip> {

	/** отступы стартов бластеров */
	private final Vector[] offsets;

	/** кол-во выстрелов в залпе */
	private final int shotCount;

	public BlasterModule(final int objectId, final ModuleTemplate template) {
		super(objectId, template);

		final BlasterRayInfo[] rayInfos = template.getBlasterRayInfos();

		this.shotCount = rayInfos.length;
		this.offsets = new Vector[rayInfos.length];

		for(int i = 0, length = rayInfos.length; i < length; i++) {
			this.offsets[i] = rayInfos[i].getOffset();
		}
	}

	/**
	 * @param index индекс выстрела модуля.
	 * @return отступ выстрела
	 */
	public Vector getOffset(final int index) {
		return offsets[index];
	}

	/**
	 * @return кол-во выстрелов модуля.
	 */
	public int getShotCount() {
		return shotCount;
	}
}

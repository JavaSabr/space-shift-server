package com.ss.server.model.module;

/**
 * Интерфей для энергетически регулируемых модулей.
 * 
 * @author Ronn
 */
public interface EnergyModule {

	public static final float ENERGY_POWER_100 = 1F;
	public static final float ENERGY_POWER_90 = 0.9F;
	public static final float ENERGY_POWER_80 = 0.8F;
	public static final float ENERGY_POWER_70 = 0.7F;
	public static final float ENERGY_POWER_60 = 0.6F;
	public static final float ENERGY_POWER_50 = 0.5F;
	public static final float ENERGY_POWER_40 = 0.4F;
	public static final float ENERGY_POWER_30 = 0.3F;
	public static final float ENERGY_POWER_20 = 0.2F;
	public static final float ENERGY_POWER_10 = 0.1F;
	public static final float ENERGY_POWER_0 = 0F;
}

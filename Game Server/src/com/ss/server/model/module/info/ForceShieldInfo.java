package com.ss.server.model.module.info;

import java.awt.Color;

import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

/**
 * Информация о силовом щите.
 * 
 * @author Ronn
 */
public class ForceShieldInfo {

	public static final String PROP_ELECTRIC_SCALE = "electricScale";
	public static final String PROP_HIT_SCALE = "hitScale";
	public static final String PROP_ELECTRIC_SPEED = "electricSpeed";
	public static final String PROP_ELECTRIC_FOG = "electricFog";
	public static final String PROP_ELECTRIC_FALL_OF = "electricFallOf";
	public static final String PROP_ELECTRIC_LINES = "electricLines";
	public static final String PROP_ELECTRIC_WIDTH = "electricWidth";
	public static final String PROP_ELECTRIC_NOSE_AMOUNT = "electricNoseAmount";
	public static final String PROP_ELECTRIC_COLOR = "electricColor";
	public static final String PROP_HIT_EFFECT_COLOR = "hitEffectColor";
	public static final String PROP_HIT_TEXTURE = "hitTexture";
	public static final String PROP_HIT_EFFECT_SIZE = "hitEffectSize";
	public static final String PROP_MODEL = "model";

	/** цвет эффекта щита */
	private final Color hitEffectColor;
	/** цвет электричества щита */
	private final Color electricColor;

	/** настройка маштабирования модели попаданий */
	private final Vector hitScale;
	/** настройка маштобирования модели поля */
	private final Vector electricScale;

	/** ключ для модели щита */
	private final String modelKey;
	/** адресс текстуры щита попаданий */
	private final String hitTexture;

	/** размер эффекта попадания в щит */
	private final float hitEffectSize;
	/** ?? */
	private final float electricNoseAmount;
	/** скорость анимации электричества */
	private final float electricSpeed;
	/** ширина линий электричества */
	private final float electricWidth;
	/** плотность линий */
	private final float electricLines;
	/** область прозрачности */
	private final float electricFallOf;
	/** дальность видимости шейдера */
	private final float electricFog;

	public ForceShieldInfo(final VarTable vars) {
		this.modelKey = vars.getString(PROP_MODEL, "models/modules/force_shields/simple_sphere.j3o");
		this.hitEffectSize = vars.getFloat(PROP_HIT_EFFECT_SIZE, 1F);
		this.hitTexture = vars.getString(PROP_HIT_TEXTURE, "textures/modules/force_shield/simple_force_shield.png");

		float[] values = vars.getFloatArray(PROP_HIT_EFFECT_COLOR, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.hitEffectColor = new Color(values[0], values[1], values[2]);

		values = vars.getFloatArray(PROP_ELECTRIC_COLOR, ",", values);

		this.electricColor = new Color(values[0], values[1], values[2]);

		this.electricNoseAmount = vars.getFloat(PROP_ELECTRIC_NOSE_AMOUNT, 0.07F);
		this.electricSpeed = vars.getFloat(PROP_ELECTRIC_SPEED, 0.11F);
		this.electricWidth = vars.getFloat(PROP_ELECTRIC_WIDTH, 2F);
		this.electricLines = vars.getFloat(PROP_ELECTRIC_LINES, 0.2F);
		this.electricFallOf = vars.getFloat(PROP_ELECTRIC_FALL_OF, 6F);
		this.electricFog = vars.getFloat(PROP_ELECTRIC_FOG, 60F);

		values = vars.getFloatArray(PROP_HIT_SCALE, ",", ArrayFactory.toFloatArray(1.1F, 1.1F, 1.1F));

		this.hitScale = Vector.newInstance(values);

		values = vars.getFloatArray(PROP_ELECTRIC_SCALE, ",", ArrayFactory.toFloatArray(0.84F, 0.65F, 0.82F));

		this.electricScale = Vector.newInstance(values);
	}

	/**
	 * @return цвет электричества щита.
	 */
	public Color getElectricColor() {
		return electricColor;
	}

	/**
	 * @return область прозрачности.
	 */
	public float getElectricFallOf() {
		return electricFallOf;
	}

	/**
	 * @return дальность видимости шейдера.
	 */
	public float getElectricFog() {
		return electricFog;
	}

	/**
	 * @return плотность линий.
	 */
	public float getElectricLines() {
		return electricLines;
	}

	/**
	 * @return
	 */
	public float getElectricNoseAmount() {
		return electricNoseAmount;
	}

	/**
	 * @return настройка маштобирования модели поля.
	 */
	public Vector getElectricScale() {
		return electricScale;
	}

	/**
	 * @return скорость анимации электричества.
	 */
	public float getElectricSpeed() {
		return electricSpeed;
	}

	/**
	 * @return ширина линий электричества.
	 */
	public float getElectricWidth() {
		return electricWidth;
	}

	/**
	 * @return цвет эффекта щита.
	 */
	public Color getHitEffectColor() {
		return hitEffectColor;
	}

	/**
	 * @return размер эффекта попадания в щит.
	 */
	public float getHitEffectSize() {
		return hitEffectSize;
	}

	/**
	 * @return настройка маштабирования модели попаданий.
	 */
	public Vector getHitScale() {
		return hitScale;
	}

	/**
	 * @return адресс текстуры щита попаданий.
	 */
	public String getHitTexture() {
		return hitTexture;
	}

	/**
	 * @return ключ для модели щита.
	 */
	public String getModelKey() {
		return modelKey;
	}
}

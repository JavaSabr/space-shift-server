package com.ss.server.model.module.info;

import static rlib.geom.util.AngleUtils.degreeToRadians;

import java.awt.Color;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

/**
 * Модель информации, описывающего огонь двигателя.
 * 
 * @author Ronn
 */
public final class EngineFireInfo {

	public static final String PROP_TRAIL_SERGMENT_LIFE = "trailSergmentLife";
	public static final String PROP_TRAIL_SEGMENT_LENGTH = "trailSegmentLength";
	public static final String PROP_TRAIL_END_SIZE = "trailEndSize";
	public static final String PROP_TRAIL_START_SIZE = "trailStartSize";
	public static final String PROP_TRAIL_OFFSET = "trailOffset";
	public static final String PROP_TRAIL_COLOR = "trailColor";
	public static final String PROP_TRAIL_MATERIAL = "trailMaterial";
	public static final String PROP_FIRE_ROTATE = "fireRotate";
	public static final String PROP_FIRE_SCALE = "fireScale";
	public static final String PROP_FIRE_OFFSET = "fireOffset";
	public static final String PROP_FIRE_MATERIAL = "fireMaterial";
	public static final String PROP_FIRE_MODEL = "fireModel";

	/** адресс модели огня двигателя */
	private final String fireModel;
	/** адресс материала огня двигателя */
	private final String fireMaterial;
	/** адресс материала следа двигателя */
	private final String trailMaterial;

	/** цвет следа от двигателя */
	private final Color trailColor;

	/** относительное положение от позиции модуля */
	private final Vector fireOffset;
	/** отступ старта следа двигателя */
	private final Vector trailOffset;
	/** маштаб пламени двигателя */
	private final Vector fireScale;

	/** наклон струи */
	private final Rotation fireRotation;

	/** ширина старта следа двигателя */
	private final float trailStartSize;
	/** ширина конца следа двигателя */
	private final float trailEndSize;
	/** длинна сегмента следа двигателя */
	private final float trailSegmentLength;
	/** время жизни сегмента двигателя */
	private final float trailSergmentLife;

	public EngineFireInfo(final VarTable vars) {
		this.fireModel = vars.getString(PROP_FIRE_MODEL);
		this.fireMaterial = vars.getString(PROP_FIRE_MATERIAL);

		float[] vals = vars.getFloatArray(PROP_FIRE_OFFSET, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.fireOffset = Vector.newInstance(vals[0], vals[1], vals[2]);

		vals = vars.getFloatArray(PROP_FIRE_SCALE, ",", ArrayFactory.toFloatArray(1, 1, 1));

		this.fireScale = Vector.newInstance(vals[0], vals[1], vals[2]);

		vals = vars.getFloatArray(PROP_FIRE_ROTATE, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.fireRotation = Rotation.newInstance(degreeToRadians(vals[0]), degreeToRadians(vals[1]), degreeToRadians(vals[2]));

		this.trailMaterial = vars.getString(PROP_TRAIL_MATERIAL, StringUtils.EMPTY);

		vals = vars.getFloatArray(PROP_TRAIL_COLOR, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.trailColor = new Color(vals[0], vals[1], vals[2]);

		vals = vars.getFloatArray(PROP_TRAIL_OFFSET, ",", vars.getFloatArray(PROP_FIRE_OFFSET, ","));

		this.trailOffset = Vector.newInstance(vals);
		this.trailStartSize = vars.getFloat(PROP_TRAIL_START_SIZE, 3F);
		this.trailEndSize = vars.getFloat(PROP_TRAIL_END_SIZE, 4F);
		this.trailSegmentLength = vars.getFloat(PROP_TRAIL_SEGMENT_LENGTH, 0.1F);
		this.trailSergmentLife = vars.getFloat(PROP_TRAIL_SERGMENT_LIFE, 1F);
	}

	/**
	 * @return адресс материала огня двигателя.
	 */
	public String getFireMaterial() {
		return fireMaterial;
	}

	/**
	 * @return адресс модели огня двигателя.
	 */
	public String getFireModel() {
		return fireModel;
	}

	/**
	 * @return относительное положение от позиции модуля.
	 */
	public Vector getFireOffset() {
		return fireOffset;
	}

	/**
	 * @return наклон струи.
	 */
	public Rotation getFireRotation() {
		return fireRotation;
	}

	/**
	 * @return маштаб пламени двигателя.
	 */
	public Vector getFireScale() {
		return fireScale;
	}

	/**
	 * @return цвет следа от двигателя.
	 */
	public Color getTrailColor() {
		return trailColor;
	}

	/**
	 * @return ширина конца следа двигателя.
	 */
	public float getTrailEndSize() {
		return trailEndSize;
	}

	/**
	 * @return адресс материала следа двигателя.
	 */
	public String getTrailMaterial() {
		return trailMaterial;
	}

	/**
	 * @return отступ старта следа двигателя.
	 */
	public Vector getTrailOffset() {
		return trailOffset;
	}

	/**
	 * @return длинна сегмента следа двигателя.
	 */
	public float getTrailSegmentLength() {
		return trailSegmentLength;
	}

	/**
	 * @return время жизни сегмента двигателя.
	 */
	public float getTrailSergmentLife() {
		return trailSergmentLife;
	}

	/**
	 * @return ширина старта следа двигателя.
	 */
	public float getTrailStartSize() {
		return trailStartSize;
	}
}
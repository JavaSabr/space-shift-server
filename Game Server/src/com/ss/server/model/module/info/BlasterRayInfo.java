package com.ss.server.model.module.info;

import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.table.GraficEffectTable;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;

/**
 * Модель информации, описывающего луч бластера.
 * 
 * @author Ronn
 */
public final class BlasterRayInfo {

	public static final String PROP_OFFSET = "offset";
	public static final String PROP_EXPLOSION = "explosion";
	public static final String PROP_SCALE = "scale";
	public static final String PROP_MATERIAL = "material";
	public static final String PROP_MODEL = "model";

	/** адресс модели */
	private final String model;
	/** адресс материала */
	private final String material;

	/** маштаб луча бластера */
	private final Vector scale;
	/** отступ от модуля */
	private final Vector offset;

	/** шаблон эффекта взрыва от бластера */
	private final GraphicEffectTemplate explosion;

	public BlasterRayInfo(final VarTable vars) {
		this.model = vars.getString(PROP_MODEL);
		this.material = vars.getString(PROP_MATERIAL);

		float[] vals = vars.getFloatArray(PROP_SCALE, ",", ArrayFactory.toFloatArray(1, 1, 1));

		this.scale = Vector.newInstance(vals[0], vals[1], vals[2]);

		vals = vars.getFloatArray(PROP_OFFSET, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.offset = Vector.newInstance(vals[0], vals[1], vals[2]);

		final GraficEffectTable effectTable = GraficEffectTable.getInstance();

		this.explosion = effectTable.getTemplate(vars.getInteger(PROP_EXPLOSION, 0));
	}

	/**
	 * @return шаблон эффекта взрыва от бластера.
	 */
	public GraphicEffectTemplate getExplosion() {
		return explosion;
	}

	/**
	 * @return адресс материала.
	 */
	public String getMaterial() {
		return material;
	}

	/**
	 * @return адресс модели.
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @return отступ от модуля.
	 */
	public Vector getOffset() {
		return offset;
	}

	/**
	 * @return маштаб.
	 */
	public Vector getScale() {
		return scale;
	}
}
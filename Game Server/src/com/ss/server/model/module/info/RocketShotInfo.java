package com.ss.server.model.module.info;

import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.table.GraficEffectTable;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;

/**
 * Модель информации, описывающего ракету.
 * 
 * @author Ronn
 */
public final class RocketShotInfo {

	public static final String PROP_OFFSET = "offset";
	public static final String PROP_ENGINE_INFO = "engineInfo";
	public static final String PROP_EXPLOSION = "explosion";
	public static final String PROP_SCALE = "scale";
	public static final String PROP_MODEL = "model";

	/** путь к модели ракеты */
	private final String model;

	/** маштаб модели ракеты */
	private final Vector scale;
	/** отступ от модуля */
	private final Vector offset;

	/** шаблон эффекта взрыва от ракеты */
	private final GraphicEffectTemplate explosion;

	/** описание двигателя ракеты */
	private final EngineFireInfo engineFireInfo;

	public RocketShotInfo(final VarTable vars) {
		this.model = vars.getString(PROP_MODEL);
		this.engineFireInfo = vars.get(PROP_ENGINE_INFO, EngineFireInfo.class);

		float[] vals = vars.getFloatArray(PROP_SCALE, ",", ArrayFactory.toFloatArray(1, 1, 1));

		this.scale = Vector.newInstance(vals[0], vals[1], vals[2]);

		vals = vars.getFloatArray(PROP_OFFSET, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.offset = Vector.newInstance(vals[0], vals[1], vals[2]);

		final GraficEffectTable effectTable = GraficEffectTable.getInstance();

		this.explosion = effectTable.getTemplate(vars.getInteger(PROP_EXPLOSION, 0));
	}

	/**
	 * @return описание двигателя ракеты.
	 */
	public EngineFireInfo getEngineFireInfo() {
		return engineFireInfo;
	}

	/**
	 * @return шаблон эффекта взрыва от бластера.
	 */
	public GraphicEffectTemplate getExplosion() {
		return explosion;
	}

	/**
	 * @return адресс модели.
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @return отступ от модуля.
	 */
	public Vector getOffset() {
		return offset;
	}

	/**
	 * @return маштаб модели ракеты.
	 */
	public Vector getScale() {
		return scale;
	}
}
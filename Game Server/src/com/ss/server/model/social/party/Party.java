package com.ss.server.model.social.party;

import java.util.Comparator;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.MessageType;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.ship.nps.Nps;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.storage.StorageUtils;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseObjectFriendStatus;
import com.ss.server.network.game.packet.server.ResponsePartyDisband;
import com.ss.server.network.game.packet.server.ResponsePartyInfo;
import com.ss.server.network.game.packet.server.ResponsePartyMemberExclude;
import com.ss.server.network.game.packet.server.ResponsePartyMemberStatus;
import com.ss.server.network.game.packet.server.ResponseSayMessage;

/**
 * Реализация группы игроков.
 * 
 * @author Ronn
 */
public class Party implements Foldable {

	public static final int MAX_LEVEL_DIFF = 5;
	public static final int MAX_DISTANCE = 2000;
	public static final int PARTY_SIZE_LIMIT = 6;

	/** сортировщик членов группы */
	private final Comparator<PlayerShip> comparator = (first, second) -> {

		if(first == getOwner()) {
			return 1;
		}

		return -1;
	};

	/** список членов группы */
	private final Array<PlayerShip> members;

	/** владелец группы */
	private PlayerShip owner;

	public Party() {
		this.members = ArrayFactory.newConcurrentArray(PlayerShip.class);
	}

	/**
	 * Выдача опыта группе.
	 * 
	 * @param topDamager топовый демагер уничтоженного НПС.
	 * @param nps уничтоженный НПС.
	 * @param exp полученный опыт с НПС.
	 * @param local контейнер локальных объектов.
	 */
	public void addExp(final PlayerShip topDamager, final Nps nps, long exp, final LocalObjects local) {

		final Array<PlayerShip> targets = local.getNextPlayerList();

		final Array<PlayerShip> members = getMembers();
		members.readLock();
		try {

			for(final PlayerShip member : members.array()) {

				if(member == null) {
					break;
				}

				if(!(topDamager == member || topDamager.isInDistance(member, MAX_DISTANCE))) {
					continue;
				}

				if(topDamager != member) {

					final int diff = Math.abs(topDamager.getLevel() - member.getLevel());

					if(diff > MAX_LEVEL_DIFF) {
						continue;
					}
				}

				targets.add(member);
			}

			if(targets.isEmpty()) {
				return;
			}

			exp /= targets.size();

			if(exp < 1) {
				return;
			}

			for(final PlayerShip member : targets.array()) {

				if(member == null) {
					break;
				}

				member.addExp(exp, nps, local);
			}

		} finally {
			members.readUnlock();
		}
	}

	/**
	 * Добавление нового члена в группу.
	 * 
	 * @param playerShip запрашивающий добавление игрока.
	 * @param member новый член группы.
	 * @param local контейнер локальных объектов.
	 */
	public void addPlayer(final PlayerShip playerShip, final PlayerShip member, final LocalObjects local) {

		if(!isOwner(playerShip)) {
			return;
		}

		final Array<PlayerShip> members = getMembers();
		members.writeLock();
		try {

			if(isFull()) {
				return;
			}

			members.add(member);

		} finally {
			members.writeUnlock();
		}

		member.setParty(this);

		updatePartyInfo(local);
		updateMembers(true, true, local);
	}

	/**
	 * Смена владельца.
	 * 
	 * @param playerShip текущий владелец.
	 * @param objectId уникальный ид нового владельца.
	 */
	public void changeOwner(final PlayerShip playerShip, final int objectId, final LocalObjects local) {

		final PlayerShip target = findMember(objectId);

		if(target == null) {
			return;
		}

		final Array<PlayerShip> members = getMembers();
		members.writeLock();
		try {

			if(!isOwner(playerShip)) {
				return;
			}

			setOwner(target);
			members.sort(getComparator());

		} finally {
			members.writeUnlock();
		}

		updatePartyInfo(local);
		updateMembers(false, true, local);
	}

	/**
	 * Роспуск группы.
	 * 
	 * @param playerShip игрок, запрашивающий роспуск.
	 * @param local контейнер локальных объектов.
	 */
	public void disband(final PlayerShip playerShip, final LocalObjects local) {

		if(!isOwner(playerShip)) {
			return;
		}

		final Array<PlayerShip> members = getMembers();
		members.readLock();
		try {

			for(final PlayerShip member : members.array()) {

				if(member == null) {
					break;
				}

				member.setParty(null);
			}

		} finally {
			members.readUnlock();
		}

		sendPacket(null, ResponsePartyDisband.getInstance());
		updateMembers(true, false, local);

		PartyFactory.saveParty(this);
	}

	/**
	 * Исключение игрока из состава группы.
	 * 
	 * @param playerShip запрашивающий исключение игрок.
	 * @param objectId уникальный ид исключающего игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void excludePlayer(final PlayerShip playerShip, final int objectId, final LocalObjects local) {

		if(!isOwner(playerShip)) {
			return;
		}

		removePlayer(objectId, local);
	}

	@Override
	public void finalyze() {
		getMembers().clear();
		setOwner(null);
	}

	/**
	 * Поиск члена группы по уникальному ид.
	 * 
	 * @param objectId уникальный ид члена группы.
	 * @return найденный член группы.
	 */
	public PlayerShip findMember(final int objectId) {

		final Array<PlayerShip> members = getMembers();
		members.readLock();
		try {

			for(final PlayerShip member : members.array()) {

				if(member == null) {
					break;
				}

				if(member.getObjectId() == objectId) {
					return member;
				}
			}

		} finally {
			members.readUnlock();
		}

		return null;
	}

	/**
	 * @return сортировщик членов группы.
	 */
	public Comparator<PlayerShip> getComparator() {
		return comparator;
	}

	/**
	 * @return список членов группы.
	 */
	public Array<PlayerShip> getMembers() {
		return members;
	}

	/**
	 * @return владелец группы.
	 */
	public PlayerShip getOwner() {
		return owner;
	}

	/**
	 * Инициализация группы.
	 * 
	 * @param owner владелец группы.
	 * @param member первый член группы.
	 * @param local контейнер локальных объектов.
	 */
	public void init(final PlayerShip owner, final PlayerShip member, final LocalObjects local) {
		setOwner(owner);

		owner.setParty(this);
		member.setParty(this);

		final Array<PlayerShip> members = getMembers();
		members.add(owner);
		members.add(member);

		updatePartyInfo(local);
		updateMembers(true, true, local);
	}

	/**
	 * @return заполнена ли группа.
	 */
	public boolean isFull() {
		return getMembers().size() >= PARTY_SIZE_LIMIT;
	}

	/**
	 * Является ли владельцем группы указанный игрок.
	 * 
	 * @param playerShip проверяемый игрок.
	 * @return является ли этот игрок владельцем.
	 */
	public boolean isOwner(final PlayerShip playerShip) {
		return getOwner() != null && getOwner() == playerShip;
	}

	@Override
	public void reinit() {
	}

	/**
	 * Удаление игрока из членов группы.
	 * 
	 * @param objectId уникальный ид удаляемого игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void removePlayer(final int objectId, final LocalObjects local) {
		removePlayer(findMember(objectId), local);
	}

	/**
	 * Удаление игрока из членов группы.
	 * 
	 * @param playerShip удаляемый игрок.
	 * @param local контейнер локальных объектов.
	 */
	public void removePlayer(final PlayerShip playerShip, final LocalObjects local) {

		if(playerShip == null) {
			return;
		}

		final Array<PlayerShip> members = getMembers();
		members.writeLock();
		try {

			if(!members.contains(playerShip)) {
				return;
			}

			if(members.size() < 3) {
				disband(getOwner(), local);
			} else {

				playerShip.setParty(null);

				updateMembers(true, false, local);
				sendPacket(null, ResponsePartyMemberExclude.getInstance(playerShip));

				members.slowRemove(playerShip);

				if(isOwner(playerShip)) {
					setOwner(members.first());
				}
			}

		} finally {
			members.writeUnlock();
		}

		updatePartyInfo(local);
		updateMembers(false, true, local);
	}

	/**
	 * Отправка сообщения от имени игрока всем сопартийцам.
	 * 
	 * @param playerShip корабль, отправляющий сообщение.
	 * @param message сообщение игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void sendMessage(final PlayerShip playerShip, final String message, final LocalObjects local) {
		sendPacket(null, ResponseSayMessage.getInstance(MessageType.PARTY, playerShip.getName(), message, local));
	}

	/**
	 * Отправка пакета сопартийцам игрока.
	 * 
	 * @param playerShip корабль игрока.
	 * @param packet отправляемый пакет.
	 */
	public void sendPacket(final PlayerShip playerShip, final ServerPacket packet) {

		final Array<PlayerShip> members = getMembers();
		members.readLock();
		try {

			packet.increaseSends(members.size());

			for(final PlayerShip member : members.array()) {

				if(member == null) {
					break;
				}

				if(member == playerShip) {
					continue;
				}

				member.sendPacket(packet, false);
			}

		} finally {
			members.readUnlock();
		}
	}

	/**
	 * @param owner владелец группы.
	 */
	public void setOwner(final PlayerShip owner) {
		this.owner = owner;
	}

	/**
	 * Обработка телепортирования предмета членом группы.
	 * 
	 * @param item телепортирующийся предмет.
	 * @param playerShip игрок, который телепортировал предмет.
	 */
	public void teleportItem(final SpaceItem item, final PlayerShip playerShip, final LocalObjects local) {
		StorageUtils.startTeleportTo(playerShip, item, local);
	}

	/**
	 * Обновление статуса члена группыд ля остальных членов.
	 * 
	 * @param playerShip обновляемый член группы.
	 * @param updateFriendly обновлять ли статус дружелюбности.
	 * @param updateStatus обновлять ли статус члена группы.
	 */
	public void updateMember(final PlayerShip playerShip, final boolean updateFriendly, final boolean updateStatus, final LocalObjects local) {

		ServerPacket statusPacket = null;

		if(updateStatus) {
			statusPacket = ResponsePartyMemberStatus.getInstance(playerShip, local);
			statusPacket.increaseSends();
		}

		final Array<PlayerShip> members = getMembers();
		members.readLock();
		try {

			for(final PlayerShip member : members.array()) {

				if(member == null) {
					break;
				}

				if(member == playerShip) {
					continue;
				}

				if(updateFriendly) {
					member.sendPacket(ResponseObjectFriendStatus.getInstance(member, playerShip, local), true);
					playerShip.sendPacket(ResponseObjectFriendStatus.getInstance(playerShip, member, local), true);
				}

				if(updateStatus) {
					member.sendPacket(statusPacket, true);
				}
			}

		} finally {
			members.readUnlock();
		}

		if(statusPacket != null) {
			statusPacket.complete();
		}
	}

	/**
	 * Взаимное обновление членов группы.
	 * 
	 * @param updateFriendly обновлять ли статус дружелюбности.
	 * @param updateStatus обновлять ли статусы членов группы.
	 * @param local контейнер локальных объектов.
	 */
	public void updateMembers(final boolean updateFriendly, final boolean updateStatus, final LocalObjects local) {

		final Array<PlayerShip> members = getMembers();
		members.readLock();
		try {

			for(final PlayerShip member : members.array()) {

				if(member == null) {
					break;
				}

				updateMember(member, updateFriendly, updateStatus, local);
			}

		} finally {
			members.readUnlock();
		}
	}

	/**
	 * Обновлением всем участникам состав группы.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void updatePartyInfo(final LocalObjects local) {
		sendPacket(null, ResponsePartyInfo.getInstance(this, local));
	}
}

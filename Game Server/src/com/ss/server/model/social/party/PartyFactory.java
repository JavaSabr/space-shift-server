package com.ss.server.model.social.party;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

/**
 * Фабрика групп игроков.
 * 
 * @author Ronn
 */
public class PartyFactory {

	/**
	 * Сохранить уже использованную группу.
	 * 
	 * @param party использованная группа.
	 */
	public static final void saveParty(final Party party) {
		POOL.put(party);
	}

	/**
	 * @return новая группа.
	 */
	public static final Party takeParty() {

		Party party = POOL.take();

		if(party == null) {
			party = new Party();
		}

		return party;
	}

	private static final FoldablePool<Party> POOL = PoolFactory.newAtomicFoldablePool(Party.class);
}

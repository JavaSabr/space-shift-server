package com.ss.server.model.shot.impl;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.pools.FoldablePool;

import com.ss.server.LocalObjects;
import com.ss.server.executor.GameExecutor;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.Space;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.shot.Shot;
import com.ss.server.model.skills.ShotSkill;
import com.ss.server.network.game.packet.server.ResponseShotEnd;
import com.ss.server.task.impl.UpdateShotTask;

/**
 * Базовая реализация выстрела.
 * 
 * @author Ronn
 */
public abstract class AbstractShot implements Shot {

	protected static final Space SPACE = Space.getInstance();

	/** контейнер объектов */
	private final Array<SpaceObject> container;

	/** реализация задачи по обновлению выстрела */
	private final UpdateShotTask updateTask;

	/** стартовая позиция выстрела */
	private final Vector startPosition;
	/** позиция выстрела */
	private final Vector position;
	/** разворот выстела */
	private final Rotation rotation;

	/** исполнитель работы выстрела */
	protected GameExecutor<UpdateShotTask> executor;

	/** пул, в который сложится выстрел после выполнения задачи */
	private FoldablePool<? super Shot> pool;

	/** стреляющий корабль */
	private SpaceShip shooter;
	/** модуль, из которого был произведен пуск */
	private Module module;
	/** стреляющий скил */
	private ShotSkill skill;

	/** пролетевшее расстояние */
	private float done;
	/** текущая скорость выстрела */
	private float speed;

	/** время последнего обновления рассчета */
	private long lastTime;
	/** время запуска выстрела */
	private long startTime;

	/** уникаьный ид выстрела */
	private int objectId;
	/** максимальная дальность полета выстрела */
	private int maxDistance;
	/** индекс ствола, из котороо был пуск */
	private int index;

	/** нажно ли самоуничтожится */
	private boolean needDestruct;
	/** надо ли завершать */
	private boolean needEnd;

	public AbstractShot() {
		this.startPosition = Vector.newInstance();
		this.position = Vector.newInstance();
		this.rotation = Rotation.newInstance();
		this.container = ArrayFactory.newArray(SpaceObject.class);
		this.updateTask = new UpdateShotTask(this);
		this.needEnd = true;
	}

	@Override
	public void bind(final FoldablePool<? super Shot> pool) {
		this.pool = pool;
	}

	@Override
	public void doFinish(final LocalObjects local) {

		final SpaceShip shooter = getShooter();

		if(shooter != null && shooter.isNeedSendPacket() && isNeedEnd()) {
			shooter.broadcastPacket(ResponseShotEnd.getInstance(this, isNeedDestruct(), local));
		}

		if(pool != null) {
			pool.put(this);
		}
	}

	@Override
	public void finalyze() {
		container.clear();
		shooter = null;
		module = null;
		pool = null;
		skill = null;
	}

	/**
	 * @return контейнер объектов.
	 */
	protected Array<SpaceObject> getContainer() {
		return container;
	}

	@Override
	public float getDone() {
		return done;
	}

	/**
	 * @return исполнитель работы выстрела.
	 */
	public GameExecutor<UpdateShotTask> getExecutor() {
		return executor;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public long getLastTime() {
		return lastTime;
	}

	@Override
	public int getMaxDistance() {
		return maxDistance;
	}

	@Override
	public Module getModule() {
		return module;
	}

	@Override
	public int getObjectId() {
		return objectId;
	}

	@Override
	public Vector getPosition() {
		return position;
	}

	@Override
	public Rotation getRotation() {
		return rotation;
	}

	@Override
	public SpaceShip getShooter() {
		return shooter;
	}

	@Override
	public ShotSkill getSkill() {
		return skill;
	}

	@Override
	public float getSpeed() {
		return speed;
	}

	@Override
	public Vector getStartLoc() {
		return startPosition;
	}

	@Override
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @return реализация задачи по обновлению выстрела.
	 */
	public UpdateShotTask getUpdateTask() {
		return updateTask;
	}

	/**
	 * @return нажно ли самоуничтожится.
	 */
	protected boolean isNeedDestruct() {
		return needDestruct;
	}

	/**
	 * @return надо ли завершать.
	 */
	protected boolean isNeedEnd() {
		return needEnd;
	}

	@Override
	public void reinit() {
		setDone(0);
		setNeedEnd(true);
	}

	/**
	 * @param done выполненность полета.
	 */
	protected void setDone(final float done) {
		this.done = done;
	}

	@Override
	public void setIndex(final int index) {
		this.index = index;
	}

	/**
	 * @param lastTime время последнего обновления.
	 */
	protected void setLastTime(final long lastTime) {
		this.lastTime = lastTime;
	}

	@Override
	public void setMaxDistance(final int maxDistance) {
		this.maxDistance = maxDistance;
	}

	@Override
	public void setModule(final Module module) {
		this.module = module;
	}

	/**
	 * @param needDestruct нажно ли самоуничтожится.
	 */
	protected void setNeedDestruct(final boolean needDestruct) {
		this.needDestruct = needDestruct;
	}

	/**
	 * @param needEnd надо ли завершать.
	 */
	protected void setNeedEnd(final boolean needEnd) {
		this.needEnd = needEnd;
	}

	@Override
	public void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	/**
	 * @param position позиция выстрела.
	 */
	protected void setPosition(final Vector position) {
		this.position.set(position);
	}

	@Override
	public void setRotation(final Rotation rotation) {
		this.rotation.set(rotation);
	}

	@Override
	public void setShooter(final SpaceShip shooter) {
		this.shooter = shooter;
	}

	@Override
	public void setSkill(final ShotSkill skill) {
		this.skill = skill;
	}

	@Override
	public void setSpeed(final float speed) {
		this.speed = speed;
	}

	@Override
	public void setStartLoc(final Vector startLoc) {
		this.startPosition.set(startLoc);
	}

	public void setStartTime(final long startTime) {
		this.startTime = startTime;
	}

	@Override
	public void start(final LocalObjects local) {
		setLastTime(System.currentTimeMillis());
		setPosition(getStartLoc());
		setNeedDestruct(false);
	}

	@Override
	public void stop(final LocalObjects local) {
		throw new RuntimeException("not implemented.");
	}
}
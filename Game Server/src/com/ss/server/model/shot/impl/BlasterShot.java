package com.ss.server.model.shot.impl;

import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.packet.server.ResponseShotHitInfo;

/**
 * Реализация модели выстрела бластера.
 * 
 * @author Ronn
 */
public final class BlasterShot extends AbstractShot {

	/** целевая точка попадания */
	private final Vector targetLoc;
	/** направление полета выстрела */
	private final Vector direction;

	public BlasterShot() {
		this.targetLoc = Vector.newInstance();
		this.direction = Vector.newInstance();
	}

	@Override
	public boolean doUpdate(final long currentTime, final LocalObjects local) {

		float done = getDone();

		try {

			final float speed = getSpeed();

			final long diff = currentTime - getLastTime();

			done += diff * speed / 1000F;

			final float percent = done / getMaxDistance();

			final Vector startLoc = getStartLoc();
			final Vector direction = getDirection();

			updateLocation(percent, startLoc);

			return processFalling(local, done, percent, startLoc, direction);

		} finally {
			setLastTime(currentTime);
			setDone(done);
		}
	}

	/**
	 * @return направление полета выстрела.
	 */
	public Vector getDirection() {
		return direction;
	}

	/**
	 * @return целевая точка попадания.
	 */
	public Vector getTargetLoc() {
		return targetLoc;
	}

	/**
	 * Процесс вычисления попадания бластера в объекты.
	 * 
	 * @return завершен ли полет бластера.
	 */
	protected boolean processFalling(final LocalObjects local, final float done, final float percent, final Vector startLoc, final Vector direction) {

		// TODO оптимизировать работу этого метода
		final Array<SpaceObject> container = getContainer();

		if(!container.isEmpty()) {

			final SpaceObject[] array = container.array();
			final SpaceShip shooter = getShooter();

			for(int i = 0, length = container.size(); i < length; i++) {

				final SpaceObject object = array[i];

				if(object.isLocationObject() || object.isGravityObject()) {

					if(!object.isInDistance(startLoc, (int) (done + object.getMaxSize()))) {
						continue;
					}

					final Bounding bounding = object.getBounding();

					if(bounding.intersects(startLoc, direction, local)) {

						if(shooter.isNeedSendPacket()) {
							shooter.broadcastPacket(ResponseShotHitInfo.getInstance(this, object, false, local));
						}

						return true;
					}

				} else if(shooter.canAttack(object)) {

					if(!object.isInDistance(startLoc, (int) (done + object.getMaxSize()))) {
						continue;
					}

					final Bounding bounding = object.getBounding();

					if(bounding.intersects(startLoc, direction, local.getNextVectorBuffer())) {
						getSkill().applySkill(this, shooter, object, local);
						setNeedEnd(false);
						return true;
					}

				}

				container.fastRemove(i);
				length--;
				i--;
			}
		}

		return percent >= 1F;
	}

	/**
	 * @param targetLoc целевая точка попадания.
	 */
	public void setTargetLoc(final Vector targetLoc) {
		this.targetLoc.set(targetLoc);
	}

	@Override
	public void start(final LocalObjects local) {

		final SpaceShip shooter = getShooter();

		if(shooter == null || getSkill() == null) {
			throw new RuntimeException("not found shooter or skill.");
		}

		final Vector startLoc = getStartLoc();
		final Vector currentLoc = getPosition();
		final Vector targetLoc = getTargetLoc();
		final Vector direction = getDirection();

		currentLoc.set(startLoc);
		direction.set(targetLoc).subtractLocal(startLoc).normalizeLocal();

		super.start(local);

		final SpaceLocation location = SPACE.getLocation(shooter.getLocationId());

		if(location == null) {
			throw new RuntimeException("not found location from id " + shooter.getLocationId());
		}

		final Array<SpaceObject> container = getContainer();

		SPACE.addAround(shooter, container);

		if(!container.isEmpty()) {

			final VectorBuffer buffer = local.getNextVectorBuffer();
			final SpaceObject[] array = container.array();

			final int maxDistance = getMaxDistance();
			final int doubleDistance = maxDistance * 2;

			for(int i = 0, length = container.size(); i < length; i++) {

				final SpaceObject object = array[i];

				if(object.isLocationObject() || object.isGravityObject()) {

					final Bounding bounding = object.getBounding();

					if(!object.isInDistance(startLoc, maxDistance + object.getMaxSize())) {
						container.fastRemove(i);
						length--;
						i--;
					} else if(!bounding.intersects(startLoc, direction, buffer)) {
						container.fastRemove(i);
						length--;
						i--;
					}

				} else if(object.isSpaceShip()) {

					if(object.isInDistance(startLoc, doubleDistance)) {
						continue;
					}

					container.fastRemove(i);
					length--;
					i--;

				} else if(object.isSpaceItem()) {
					container.fastRemove(i);
					length--;
					i--;
				}
			}
		}

		executor = location.getShotExecutor();
		executor.addTask(getUpdateTask());
	}

	/**
	 * Обновление текущей позиции бластера.
	 */
	protected void updateLocation(final float percent, final Vector startLoc) {

		final Vector targetLoc = getTargetLoc();
		final Vector currentLoc = getPosition();

		// рассчитываем новую точку текущей позиции
		final float newX = startLoc.getX() + (targetLoc.getX() - startLoc.getX()) * percent;
		final float newY = startLoc.getY() + (targetLoc.getY() - startLoc.getY()) * percent;
		final float newZ = startLoc.getZ() + (targetLoc.getZ() - startLoc.getZ()) * percent;

		currentLoc.setXYZ(newX, newY, newZ);
	}
}

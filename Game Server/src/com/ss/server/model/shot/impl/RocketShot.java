package com.ss.server.model.shot.impl;

import rlib.geom.DirectionType;
import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.ship.SpaceShip;

/**
 * Реалиация выстрела ракетой.
 * 
 * @author Ronn
 */
public class RocketShot extends AbstractShot {

	public static final int GUIDANCE_INTERVAL = 500;

	/** целевой разворот ракеты */
	private final Rotation targetRotation;
	/** стартовый разворот ракеты */
	private final Rotation startRotation;
	/** итоговый текущий разворот ракеты */
	private final Rotation resultRotation;
	/** буферный разворот для вычислений */
	private final Rotation bufferRotation;

	/** текущее направление ракеты */
	private final Vector direction;

	/** цель ракеты */
	private SpaceObject target;

	/** время последнего обновления наведения */
	private long lastGuidanceUpdate;

	/** скорость разворота ракеты */
	private float rotationSpeed;
	/** выполненость разворота ракеты */
	private float rotatinDone;
	/** шаг разворота */
	private float rotationStep;

	/** максимальная скорость ракеты */
	private int maxSpeed;
	/** ускорение ракеты */
	private int accel;

	public RocketShot() {
		this.targetRotation = Rotation.newInstance();
		this.startRotation = Rotation.newInstance();
		this.resultRotation = Rotation.newInstance();
		this.bufferRotation = Rotation.newInstance();
		this.direction = Vector.newInstance();
	}

	@Override
	public boolean doUpdate(final long currentTime, final LocalObjects local) {

		final float done = getDone();

		try {

			if(done > getMaxDistance()) {
				return true;
			}

			final SpaceObject target = getTarget();

			if(target == null || target.isDestructed()) {
				return true;
			}

			processGuidance(target, currentTime, local);

			final long diff = currentTime - getLastTime();

			processRotation(currentTime, diff, local);
			processFly(currentTime, diff, local);

			return processFalling(getPosition(), getDirection(), local);

		} finally {
			setLastTime(currentTime);
		}
	}

	/**
	 * @return ускорение ракеты.
	 */
	public int getAccel() {
		return accel;
	}

	/**
	 * @return буферный разворот для вычислений.
	 */
	private Rotation getBufferRotation() {
		return bufferRotation;
	}

	/**
	 * @return текущее направление ракеты.
	 */
	private Vector getDirection() {
		return direction;
	}

	/**
	 * @return время последнего обновления наведения.
	 */
	private long getLastGuidanceUpdate() {
		return lastGuidanceUpdate;
	}

	/**
	 * @return максимальная скорость ракеты.
	 */
	public int getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * @return итоговый текущий разворот ракеты.
	 */
	private Rotation getResultRotation() {
		return resultRotation;
	}

	/**
	 * @return выполненость разворота ракеты.
	 */
	private float getRotatinDone() {
		return rotatinDone;
	}

	/**
	 * @return скорость разворота ракеты.
	 */
	public float getRotationSpeed() {
		return rotationSpeed;
	}

	/**
	 * @return шаг разворота.
	 */
	private float getRotationStep() {
		return rotationStep;
	}

	/**
	 * @return стартовый разворот ракеты.
	 */
	private Rotation getStartRotation() {
		return startRotation;
	}

	/**
	 * @return цель ракеты.
	 */
	public SpaceObject getTarget() {
		return target;
	}

	/**
	 * @return целевой разворот ракеты.
	 */
	private Rotation getTargetRotation() {
		return targetRotation;
	}

	/**
	 * Обработка попадания ракеты в цель.
	 * 
	 * @param location текущая позиция ракеты.
	 * @param direction направление полета.
	 * @param local контейнер локальных объектов.
	 * @return было ли попадание ракеты.
	 */
	private boolean processFalling(final Vector location, final Vector direction, final LocalObjects local) {

		// TODO оптимизировать этот метод
		final Array<SpaceObject> container = getContainer();

		final VectorBuffer buffer = local.getNextVectorBuffer();
		final SpaceShip shoter = getShooter();

		final Vector check = local.getNextVector();
		check.set(direction).negateLocal();

		final int checkDistance = (int) (getSpeed() * 1000 / GUIDANCE_INTERVAL);

		final SpaceObject[] array = container.array();

		for(int i = 0, length = container.size(); i < length; i++) {

			final SpaceObject target = array[i];

			if(target.isLocationObject() || target.isGravityObject()) {

				if(!target.isInDistance(location, target.getMaxSize())) {
					continue;
				}

				final Bounding bounding = target.getBounding();

				if(bounding.contains(location, buffer)) {
					return true;
				}

			} else if(shoter.canAttack(target)) {

				if(!target.isInDistance(location, target.getMaxSize() + checkDistance)) {
					continue;
				}

				if(!target.isInDistance(location, checkDistance)) {
					continue;
				}

				final Bounding bounding = target.getBounding();

				if(bounding.intersects(location, check, buffer)) {
					getSkill().applySkill(this, shoter, target, local);
					setNeedDestruct(false);
					setNeedEnd(false);
					return true;
				}

			} else {
				container.fastRemove(i);
				length--;
				i--;
			}
		}

		return false;
	}

	/**
	 * Обработка полета ракеты.
	 * 
	 * @param currentTime текущее время.
	 * @param diff разница времени с последним обновлением.
	 * @param local контейнер локальных объектов.
	 */
	private void processFly(final long currentTime, final long diff, final LocalObjects local) {

		float done = getDone();
		float speed = getSpeed();
		final float accel = getAccel();

		try {

			// считаем ускорение
			if(accel < 1 && speed < 1) {
				return;
			}

			speed = Math.min(speed + diff / 1000F * accel, getMaxSpeed());

			final float dist = diff * speed / 1000F;
			final Vector direction = local.getNextVector();

			done += dist;

			direction.set(getDirection());
			direction.multLocal(dist);
			direction.addLocal(getPosition());

			setPosition(direction);

		} finally {
			setDone(done);
			setSpeed(speed);
		}
	}

	/**
	 * Процесс наведения ракеты на цель.
	 * 
	 * @param target цель ракеты.
	 * @param local контейнер локальных объектов.
	 */
	private void processGuidance(final SpaceObject target, final long currentTime, final LocalObjects local) {

		final long diff = currentTime - getLastGuidanceUpdate();

		if(diff > GUIDANCE_INTERVAL) {

			final Rotation targetRotation = getTargetRotation();
			final Rotation current = getRotation();

			if(target.isSpaceShip()) {

				final SpaceShip ship = target.getSpaceShip();

				final float flyDistance = ship.getCurrentSpeed() * (1000 / GUIDANCE_INTERVAL) / 3;

				Vector direction = ship.getDirection();
				final Vector up = current.getVectorDirection(DirectionType.UP, local.getNextVector());
				final Vector targetPoint = local.getNextVector();
				targetPoint.set(direction).multLocal(flyDistance);
				targetPoint.addLocal(ship.getLocation());

				direction = local.getNextVector();
				direction.set(targetPoint).subtractLocal(getPosition());
				direction.normalizeLocal();

				targetRotation.lookAt(direction, up, local);
				updateRotation(targetRotation, local);

			} else {

				final Vector location = getPosition();
				final Vector direction = local.getNextVector();

				final float distance = target.distanceTo(location);

				targetRotation.getVectorDirection(DirectionType.DIRECTION, direction);

				direction.multLocal(distance);
				direction.addLocal(location);

				final Bounding bounding = target.getBounding();

				if(!bounding.contains(direction, local)) {

					final Vector up = current.getVectorDirection(DirectionType.UP, local.getNextVector());
					direction.set(target.getLocation());

					direction.subtractLocal(getPosition());
					direction.normalizeLocal();

					targetRotation.lookAt(direction, up, local);
					updateRotation(targetRotation, local);
				}
			}

			setLastGuidanceUpdate(currentTime);
		}
	}

	/**
	 * Обработка разворота ракеты.
	 * 
	 * @param currentTime текущее время.
	 * @param diff разница времени с последним обновлением.
	 * @param local контейнер локальных объектов.
	 */
	private void processRotation(final long currentTime, final long diff, final LocalObjects local) {

		float done = getRotatinDone();

		try {

			if(done >= 1F) {
				return;
			}

			done += getRotationStep() * diff / 1000F;

			final Rotation start = getStartRotation();
			final Rotation target = getTargetRotation();
			Rotation result = getResultRotation();

			if(done >= 1F) {
				result = target;
			}

			if(result != target) {

				final Rotation buffer = getBufferRotation();
				buffer.set(target);

				result = result.slerp(start, buffer, done, true);
			}

			result.getVectorDirection(DirectionType.DIRECTION, getDirection());
			setRotation(result);

		} finally {
			setRotatinDone(done);
		}
	}

	/**
	 * @param accel ускорение ракеты.
	 */
	public void setAccel(final int accel) {
		this.accel = accel;
	}

	/**
	 * @param lastGuidanceUpdate время последнего обновления наведения.
	 */
	private void setLastGuidanceUpdate(final long lastGuidanceUpdate) {
		this.lastGuidanceUpdate = lastGuidanceUpdate;
	}

	/**
	 * @param maxSpeed максимальная скорость ракеты.
	 */
	public void setMaxSpeed(final int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	/**
	 * @param rotatinDone выполненость разворота ракеты.
	 */
	private void setRotatinDone(final float rotatinDone) {
		this.rotatinDone = rotatinDone;
	}

	/**
	 * @param rotationSpeed скорость разворота ракеты.
	 */
	public void setRotationSpeed(final float rotationSpeed) {
		this.rotationSpeed = rotationSpeed;
	}

	/**
	 * @param rotationStep шаг разворота.
	 */
	private void setRotationStep(final float rotationStep) {
		this.rotationStep = rotationStep;
	}

	/**
	 * @param rotation стартовый разворот ракеты.
	 */
	private void setStartRotation(final Rotation rotation) {
		this.startRotation.set(rotation);
	}

	/**
	 * @param target цель ракеты.
	 */
	public void setTarget(final SpaceObject target) {
		this.target = target;
	}

	/**
	 * @param rotation целевой разворот ракеты.
	 */
	private void setTargetRotation(final Rotation rotation) {
		this.targetRotation.set(targetRotation);
	}

	@Override
	public void start(final LocalObjects local) {

		final SpaceShip shooter = getShooter();

		if(shooter == null || getSkill() == null) {
			throw new RuntimeException("not found shooter or skill.");
		}

		final Rotation rotation = getRotation();
		rotation.getVectorDirection(DirectionType.DIRECTION, getDirection());

		setStartRotation(rotation);
		setTargetRotation(rotation);
		setRotatinDone(1);
		setLastGuidanceUpdate(System.currentTimeMillis() + GUIDANCE_INTERVAL);

		super.start(local);

		setNeedDestruct(true);

		final SpaceLocation location = SPACE.getLocation(shooter.getLocationId());

		if(location == null) {
			throw new RuntimeException("not found location from id " + shooter.getLocationId());
		}

		final Array<SpaceObject> container = getContainer();

		SPACE.addAround(shooter, container);

		if(!container.isEmpty()) {

			final SpaceObject[] array = container.array();
			final Vector startLoc = getStartLoc();

			final int checkDistance = getMaxDistance() * 2;

			for(int i = 0, length = container.size(); i < length; i++) {

				final SpaceObject object = array[i];

				if(object.isSpaceShip()) {

					if(object.isInDistance(startLoc, checkDistance)) {
						continue;
					}

					container.fastRemove(i);
					length--;
					i--;

				} else if(object.isSpaceItem()) {
					container.fastRemove(i);
					length--;
					i--;
				}
			}
		}

		executor = location.getShotExecutor();
		executor.addTask(getUpdateTask());
	}

	/**
	 * Обновление целевого направления.
	 * 
	 * @param target новое конечное направление.
	 * @param local контейнер локальных объектов.
	 */
	public void updateRotation(final Rotation target, final LocalObjects local) {

		final float speed = getRotationSpeed();

		if(speed <= 0) {
			return;
		}

		final Rotation current = getRotation();

		final float dot = Math.abs(current.dot(target));

		if(dot > 0.99999F) {
			setDone(1F);
			return;
		}

		final float radians = (float) Math.acos(dot);

		final float count = radians / speed;
		final float step = 1 / count;

		setStartRotation(current);
		setTargetRotation(target);
		setRotationStep(step);
		setRotatinDone(0);
	}
}

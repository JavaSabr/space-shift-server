package com.ss.server.model.shot;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;

import com.ss.server.LocalObjects;
import com.ss.server.model.module.Module;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.ShotSkill;

/**
 * Интерфейс для реализации модели выстрела бластером/ракетой.
 * 
 * @author Ronn
 */
public interface Shot extends Foldable {

	/**
	 * Бинд пула, в который выстрел сложится после выполнения задачи.
	 * 
	 * @param pool пул выстрелов.
	 */
	public void bind(FoldablePool<? super Shot> pool);

	/**
	 * Обработка заверщения выстрела.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void doFinish(LocalObjects local);

	/**
	 * Обновить состояние выстрела.
	 * 
	 * @param currentTime текущее время.
	 * @param local локальные объекты.
	 * @param завершен ли выстрел.
	 */
	public boolean doUpdate(long currentTime, LocalObjects local);

	/**
	 * @return % пройденного расстояния от максимального.
	 */
	public float getDone();

	/**
	 * @return индекс ствола, из котороо был пуск.
	 */
	public int getIndex();

	/**
	 * @return время последнего обновления.
	 */
	public long getLastTime();

	/**
	 * @return максимальная дальность полета выстрела.
	 */
	public int getMaxDistance();

	/**
	 * @return модуль, из которого был пуск.
	 */
	public Module getModule();

	/**
	 * @return уникальный ид выстрела.
	 */
	public int getObjectId();

	/**
	 * @return позиция снаряда.
	 */
	public Vector getPosition();

	/**
	 * @return разворот выстрела.
	 */
	public Rotation getRotation();

	/**
	 * @return выстреливший корабль.
	 */
	public SpaceShip getShooter();

	/**
	 * @return скил, с помощью которого был произведен выстрел.
	 */
	public ShotSkill getSkill();

	/**
	 * @return скорость выстрела.
	 */
	public float getSpeed();

	/**
	 * @return стартовая точка выстрела.
	 */
	public Vector getStartLoc();

	/**
	 * @return время старта выстрела.
	 */
	public long getStartTime();

	/**
	 * @param index индекс ствола, из котороо был пуск.
	 */
	public void setIndex(int index);

	/**
	 * @param maxDistance максимальная дистанция выстрела.
	 */
	public void setMaxDistance(int maxDistance);

	/**
	 * @param module модуль, из которого был пуск.
	 */
	public void setModule(Module module);

	/**
	 * @param objectId уникальный ид выстрела.
	 */
	public void setObjectId(int objectId);

	/**
	 * @param rotation разворот выстрела.
	 */
	public void setRotation(Rotation rotation);

	/**
	 * @param shooter стреляющий корабль.
	 */
	public void setShooter(SpaceShip shooter);

	/**
	 * @param skill скил, с помощью которого был произведен выстрел.
	 */
	public void setSkill(ShotSkill skill);

	/**
	 * @param speed текущая скорость выстрела.
	 */
	public void setSpeed(float speed);

	/**
	 * @param startLoc точна старта выстрела.
	 */
	public void setStartLoc(Vector startLoc);

	/**
	 * Запуск выстрела.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void start(LocalObjects local);

	/**
	 * Остановка выстрела.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void stop(LocalObjects local);
}

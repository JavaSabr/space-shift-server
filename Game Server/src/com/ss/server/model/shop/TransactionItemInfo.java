package com.ss.server.model.shop;

import rlib.util.pools.Foldable;

/**
 * Контейнер информации о транзакции предмета.
 * 
 * @author Ronn
 */
public class TransactionItemInfo implements Foldable {

	/** уникальный ид предмета */
	private int objectId;
	/** ид шаблона предмета */
	private int templateId;
	/** кол-во предметов */
	private int count;

	/**
	 * @return кол-во предметов.
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @return уникальный ид предмета.
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return ид шаблона предмета.
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param count кол-во предметов.
	 */
	public void setCount(final int count) {
		this.count = count;
	}

	/**
	 * @param objectId уникальный ид предмета.
	 */
	public void setObjectId(final int objectId) {
		this.objectId = objectId;
	}

	/**
	 * @param templateId ид шаблона предмета.
	 */
	public void setTemplateId(final int templateId) {
		this.templateId = templateId;
	}
}

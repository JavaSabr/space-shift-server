package com.ss.server.model.shop;

import com.ss.server.template.item.ItemTemplate;

/**
 * Информация о стоимости предмета.
 * 
 * @author Ronn
 */
public class ItemPriceInfo {

	/** шаблон предмета */
	private final ItemTemplate template;

	/** значение стоимости продажи игроку */
	private final int buy;
	/** значение стоимости продажи игроком */
	private final int sell;

	public ItemPriceInfo(final ItemTemplate template, final int buy, final int sell) {
		this.template = template;
		this.buy = buy;
		this.sell = sell;
	}

	@Override
	public boolean equals(final Object obj) {

		if(this == obj) {
			return true;
		}

		if(obj == null) {
			return false;
		}

		if(getClass() != obj.getClass()) {
			return false;
		}

		final ItemPriceInfo other = (ItemPriceInfo) obj;

		if(template == null) {

			if(other.template != null) {
				return false;
			}

		} else if(!template.equals(other.template)) {
			return false;
		}

		return true;
	}

	/**
	 * @return значение стоимости продажи игроку.
	 */
	public int getBuy() {
		return buy;
	}

	/**
	 * @return значение стоимости продажи игроком.
	 */
	public int getSell() {
		return sell;
	}

	/**
	 * @return шаблон предмета.
	 */
	public ItemTemplate getTemplate() {
		return template;
	}

	/**
	 * @return ид шаблона.
	 */
	public int getTemplateId() {
		return template.getId();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (template == null ? 0 : template.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "ItemPriceInfo [template=" + template + ", buy=" + buy + ", sell=" + sell + "]";
	}
}

package com.ss.server.model.shop;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Реализация списка цен на предметы.
 * 
 * @author Ronn
 */
public class ItemPriceList {

	private static final Logger LOGGER = LoggerManager.getLogger(ItemPriceList.class);

	/** список цен на предметы */
	private final Array<ItemPriceInfo> items;

	/** ид списка цен */
	private final int id;

	public ItemPriceList(final int id) {
		this.items = ArrayFactory.newArray(ItemPriceInfo.class);
		this.id = id;
	}

	/**
	 * @param info цена на предмет.
	 */
	public void addItem(final ItemPriceInfo info) {

		final Array<ItemPriceInfo> items = getItems();

		if(items.contains(info)) {
			LOGGER.warning("found duplicate price info for " + info);
		}

		items.add(info);
	}

	/**
	 * @return ид списка цен.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return список цен на предметы.
	 */
	public Array<ItemPriceInfo> getItems() {
		return items;
	}
}

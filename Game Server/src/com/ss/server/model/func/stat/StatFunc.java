package com.ss.server.model.func.stat;

import com.ss.server.model.func.Func;
import com.ss.server.model.impl.Env;

/**
 * Интерфейс для реализации функции рассчета стата.
 * 
 * @author Ronn
 */
public interface StatFunc extends Func, Comparable<StatFunc> {

	public static final int ORDER_10 = 0x10;
	public static final int ORDER_20 = 0x20;
	public static final int ORDER_30 = 0x30;
	public static final int ORDER_40 = 0x40;
	public static final int ORDER_50 = 0x50;
	public static final int ORDER_60 = 0x60;
	public static final int ORDER_70 = 0x70;

	/**
	 * Рассчет функции.
	 * 
	 * @param result результат.
	 */
	public void calc(Env env);

	/**
	 * @return порядок функции.
	 */
	public int getOrder();

	/**
	 * @return тип рассчитываемого стата.
	 */
	public StatType getType();
}

package com.ss.server.model.func.stat.impl;

import java.util.function.Consumer;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.condition.Condition;
import com.ss.server.model.func.impl.AbstractFunc;
import com.ss.server.model.func.stat.StatFunc;
import com.ss.server.model.func.stat.StatType;
import com.ss.server.model.impl.Env;
import com.ss.server.model.ship.SpaceShip;

/**
 * Базовая модель функции статов.
 * 
 * @author Ronn
 */
public class MathStatFunc extends AbstractFunc implements StatFunc {

	/** тип стата */
	protected final StatType type;
	/** изминение стата */
	protected final Consumer<Env> function;

	/** порядок приминения */
	protected final int order;

	public MathStatFunc(final StatType type, final Condition condition, final Consumer<Env> function, final int order) {
		super(condition);
		this.type = type;
		this.function = function;
		this.order = order;
	}

	@Override
	public void addMe(final SpaceObject object) {

		if(object == null) {
			return;
		}

		final SpaceShip ship = object.getSpaceShip();

		if(ship == null) {
			LOGGER.warning(this, new Exception("incorrect instance " + object));
			return;
		}

		ship.addStatFunc(this);
	}

	@Override
	public void calc(final Env env) {

		final Condition condition = getCondition();

		if(condition == null || condition.test(env)) {
			function.accept(env);
		}
	}

	@Override
	public int compareTo(final StatFunc func) {
		return order - func.getOrder();
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Override
	public StatType getType() {
		return type;
	}

	@Override
	public void removeMe(final SpaceObject object) {

		if(object == null) {
			return;
		}

		final SpaceShip ship = object.getSpaceShip();

		if(ship == null) {
			LOGGER.warning(this, new Exception("incorrect instance " + object));
			return;
		}

		ship.removeStatFunc(this);
	}

	@Override
	public String toString() {
		return "MathStatFunc [type=" + type + ", cond=" + condition + ", function=" + function + ", order=" + order + "]";
	}
}

package com.ss.server.model.func.stat;

/**
 * Перечисление статов кораблей.
 * 
 * @author Ronn
 */
public enum StatType {

	/** ----------- характеристики полета ----------------- */
	/** максимальная скорость перемещения корабля */
	MAX_FLY_SPEED("@stat-type:max-fly-speed@"),
	/** максимальная скорость разгона */
	MAX_CURRENT_FLY_SPEED("@stat-type:max-current-fly-speed@"),
	/** ускорение корабля */
	FLY_ACCEL("@stat-type:fly-accel@"),
	/** скорость разворота корабля */
	ROTATE_SPEED("@stat-type:rotate-speed@"),

	/** -------------- характеристики прочности корабля -------------- */
	/** максимальная прочность корабля */
	MAX_STRENGTH("@stat-type:max-strength@"),

	/** --------------- энергетические характеристики ------------- */
	/** максимальный запас энергии */
	MAX_ENERGY("@stat-type:max-energy@"),
	/** скорость регенерации энергии */
	REGEN_ENERGY("@stat-type:regen-energy@"),
	/** постоянное потребление энергии двигателем */
	ENGINE_CONSUME_ENERGY("@stat-type:consume-energy@"),

	/** кол-во силовых полей на корабле */
	FORCE_SHIELD_COUNT("@stat-type:force-shield-count@"),

	/** -------------- усилители от генератора ---------------------- */
	/** усиление энергетического оружия */
	ENERGY_WEAPON_POWER("@stat-type:energy-weapon-power@"),
	/** усиление мощности силового поля */
	FORCE_SHIELD_POWER("@stat-type:force-shield-power@"),
	/** усиление скорости двигателей */
	ENGINE_SPEED_POWER("@stat-type:engine-speed-power@"), ;

	public static final int LENGTH = values().length;

	private String langName;

	private StatType(final String langName) {
		this.langName = langName;
	}

	/**
	 * @return название стата для локализации.
	 */
	public String getLangName() {
		return langName;
	}
}

package com.ss.server.model.func.stat;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.impl.Env;

/**
 * Модель калькулятора статов.
 * 
 * @author Ronn
 */
public final class Calculator {

	public static Calculator newInstance() {
		return new Calculator();
	}

	/** список функций в калькуляторе */
	private final Array<StatFunc> funcs;

	public Calculator() {
		this.funcs = ArrayFactory.newSortedArray(StatFunc.class);
	}

	/**
	 * Добавить новую функцию.
	 */
	public void addStatFunc(final StatFunc func) {
		funcs.add(func);
	}

	/**
	 * Рассчет стата.
	 */
	public void calc(final Env env) {
		for(final StatFunc func : getFuncs().array()) {

			if(func == null) {
				break;
			}

			func.calc(env);
		}
	}

	/**
	 * @return список функций в калькуляторе.
	 */
	private final Array<StatFunc> getFuncs() {
		return funcs;
	}

	/**
	 * Удалить старую функцию.
	 */
	public void removeStatFunc(final StatFunc func) {
		funcs.slowRemove(func);
	}
}

package com.ss.server.model.func.impl;

import java.util.function.Consumer;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.condition.Condition;

/**
 * Реализация функции с помощью consumer.
 * 
 * @author Ronn
 */
public class ConsumerStateFunc extends AbstractFunc {

	/** функция для приминения действий при добавлении */
	private final Consumer<SpaceObject> addFunction;
	/** функция для приминения действий при удалении */
	private final Consumer<SpaceObject> removeAction;

	public ConsumerStateFunc(final Condition condition, final Consumer<SpaceObject> addFunction, final Consumer<SpaceObject> removeFunction) {
		super(condition);

		this.addFunction = addFunction;
		this.removeAction = removeFunction;
	}

	@Override
	public void addMe(final SpaceObject object) {
		addFunction.accept(object);
	}

	@Override
	public void removeMe(final SpaceObject object) {
		removeAction.accept(object);
	}
}

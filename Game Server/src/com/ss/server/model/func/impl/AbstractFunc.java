package com.ss.server.model.func.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.model.condition.Condition;
import com.ss.server.model.func.Func;

/**
 * Базовая реализация функции.
 * 
 * @author Ronn
 */
public abstract class AbstractFunc implements Func {

	protected static final Logger LOGGER = LoggerManager.getLogger(Func.class);

	/** условие приминения функции */
	protected final Condition condition;

	public AbstractFunc(final Condition condition) {
		this.condition = condition;
	}

	/**
	 * @return условие приминения функции.
	 */
	public Condition getCondition() {
		return condition;
	}
}

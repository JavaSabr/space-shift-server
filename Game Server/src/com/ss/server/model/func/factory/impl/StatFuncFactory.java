package com.ss.server.model.func.factory.impl;

import java.util.function.Consumer;

import org.w3c.dom.Node;

import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.manager.ConditionManager;
import com.ss.server.model.condition.Condition;
import com.ss.server.model.func.Func;
import com.ss.server.model.func.factory.FuncFactory;
import com.ss.server.model.func.stat.StatType;
import com.ss.server.model.func.stat.impl.MathStatFunc;
import com.ss.server.model.impl.Env;

/**
 * Модель фабрики функций статов кораблей.
 * 
 * @author Ronn
 */
public final class StatFuncFactory implements FuncFactory {

	public static final String FUNC_SET = "set";
	public static final String FUNC_DIV = "div";
	public static final String FUNC_SUB = "sub";
	public static final String FUNC_MUL = "mul";
	public static final String FUNC_ADD = "add";

	public static final String PROP_STAT = "stat";
	public static final String PROP_VAL = "val";
	public static final String PROP_ORDER = "order";

	/** список создаваемых функций */
	private final String[] funcNames;

	public StatFuncFactory() {
		this.funcNames = ArrayFactory.toGenericArray(FUNC_ADD, FUNC_MUL, FUNC_SUB, FUNC_DIV, FUNC_SET);
	}

	@Override
	public Func create(final Node node, final Object... objects) {

		final VarTable vars = VarTable.newInstance(node);

		final Condition cond = ConditionManager.parse(node, objects);
		Consumer<Env> function = null;

		final int order = Integer.decode(vars.getString(PROP_ORDER)).intValue();
		final float value = vars.getFloat(PROP_VAL);

		switch(node.getNodeName()) {
			case FUNC_SET: {
				function = env -> env.value = value;
				break;
			}
			case FUNC_ADD: {
				function = env -> env.value += value;
				break;
			}
			case FUNC_SUB: {
				function = env -> env.value -= value;
				break;
			}
			case FUNC_MUL: {
				function = env -> env.value *= value;
				break;
			}
			case FUNC_DIV: {
				function = env -> env.value /= value;
				break;
			}
		}

		if(function == null) {
			return null;
		}

		return new MathStatFunc(vars.getEnum(PROP_STAT, StatType.class), cond, function, order);
	}

	@Override
	public String[] getFuncNames() {
		return funcNames;
	}
}

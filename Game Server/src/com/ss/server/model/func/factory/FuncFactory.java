package com.ss.server.model.func.factory;

import org.w3c.dom.Node;

import com.ss.server.model.func.Func;

/**
 * Интерфейс для реализации фабрики функций.
 * 
 * @author Ronn
 */
public interface FuncFactory {

	/**
	 * Создание функции.
	 * 
	 * @param node узел хмл.
	 * @param objects дополнительные объекты.
	 * @return новая функция.
	 */
	public Func create(Node node, Object... objects);

	/**
	 * @return имена функций, с которыми работает данная фабрика.
	 */
	public String[] getFuncNames();
}

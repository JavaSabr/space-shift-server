package com.ss.server.model.func.factory.impl;

import java.util.function.Consumer;

import org.w3c.dom.Node;

import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.func.Func;
import com.ss.server.model.func.factory.FuncFactory;
import com.ss.server.model.func.impl.ConsumerStateFunc;
import com.ss.server.model.ship.GeneratorMode;
import com.ss.server.model.ship.SpaceShip;

/**
 * Модель фабрики функций изменений состояний кораблей.
 * 
 * @author Ronn
 */
public final class GenerateModeFuncFactory implements FuncFactory {

	public static final String NAME_ENGINE_GENERATOR = "EngineGenerator";

	public static final String PROP_MODE = "mode";
	public static final String PROP_NAME = "name";

	public static final String FUNC_FUNC = "func";

	/** список создаваемых функций */
	private final String[] funcNames;

	public GenerateModeFuncFactory() {
		this.funcNames = ArrayFactory.toGenericArray(FUNC_FUNC);
	}

	@Override
	public Func create(final Node node, final Object... objects) {

		final VarTable vars = VarTable.newInstance(node);

		final String name = vars.getString(PROP_NAME);

		if(!NAME_ENGINE_GENERATOR.equals(name)) {
			return null;
		}

		final GeneratorMode mode = vars.getEnum(PROP_MODE, GeneratorMode.class);

		final Consumer<SpaceObject> addFunction = object -> {
			final SpaceShip ship = object.getSpaceShip();
			ship.addActiveGenerator(mode);
		};

		final Consumer<SpaceObject> removeFunction = object -> {
			final SpaceShip ship = object.getSpaceShip();
			ship.removeActiveGenerator(mode);
		};

		return new ConsumerStateFunc(null, addFunction, removeFunction);
	}

	@Override
	public String[] getFuncNames() {
		return funcNames;
	}
}

package com.ss.server.model.func;

import com.ss.server.model.SpaceObject;

/**
 * Модель функции космического объекта.
 * 
 * @author Ronn
 */
public interface Func {

	/**
	 * Добавить функцию объекту.
	 */
	public void addMe(SpaceObject object);

	/**
	 * Удалить функцию у объекта.
	 */
	public void removeMe(SpaceObject object);
}

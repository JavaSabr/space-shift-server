package com.ss.server.model.storage;

import com.ss.server.LocalObjects;
import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.SystemMessageType;
import com.ss.server.model.SystemMessageVars;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.teleport.impl.ItemToStorageTeleportHandler;
import com.ss.server.network.game.packet.server.ResponseSystemMessage;

/**
 * Набор утильных методов по работе с хранилищем корабля.
 * 
 * @author Ronn
 */
public class StorageUtils {

	/**
	 * Запуск телепортирования предмета на корабль.
	 * 
	 * @param ship корабль, который хочет телепортировать предмет.
	 * @param item предмет, который нужно тедепортировать.
	 */
	public static void addMoneyTo(final SpaceShip ship, final long count, final String author) {

		final Storage storage = checkStorage(ship);

		if(storage == null) {
			return;
		}

		storage.addItem(SpaceItem.ITEM_ID_MONEY, count, author);
	}

	/**
	 * Проверка на наличие хранилища.
	 * 
	 * @param spaceShip проверяемый корабль.
	 * @return хранилище корабля.
	 */
	private static Storage checkStorage(final SpaceShip spaceShip) {

		final Storage storage = spaceShip.getStorage();

		if(storage == null) {
			// TODO сообщение о проблемах с хранилищем
		}

		return storage;
	}

	/**
	 * Телепорт предмета из космоса в хранилище корабля.
	 * 
	 * @param spaceShip корабль, на который перемещаем предмет.
	 * @param item предмет, который будет перемещен.
	 * @param local контейнер локальных объектов.
	 */
	public static boolean finishTeleportTo(final SpaceShip spaceShip, final SpaceItem item, final LocalObjects local) {

		final Storage storage = checkStorage(spaceShip);

		if(storage == null) {
			return false;
		}

		final long itemCount = item.getItemCount();

		if(storage.addItem(item)) {

			if(spaceShip.isPlayerShip()) {

				final ResponseSystemMessage responseSystemMessage = ResponseSystemMessage.getInstance(SystemMessageType.TELEPORTED_ITEM, local);
				responseSystemMessage.addVar(SystemMessageVars.VAR_ITEM_NAME, item.getName());
				responseSystemMessage.addVar(SystemMessageVars.VAR_ITEM_COUNT, String.valueOf(itemCount));

				spaceShip.sendPacket(responseSystemMessage, true);
			}

			final ObjectEventManager eventManager = ObjectEventManager.getInstance();
			eventManager.notifyItemTeleported(spaceShip, item, local);

			return true;
		}

		return false;
	}

	/**
	 * Перемещение предмета из одной ячейки в другую в хранидище корабля.
	 * 
	 * @param ship корабль, в котором перемещаем предметы.
	 * @param startIndex изначальный индекс ячейки.
	 * @param endIndex целевой индекс ячейки.
	 * @param local контейнер локальных объектов.
	 */
	public static void moveItem(final SpaceShip spaceShip, final int startIndex, final int endIndex, final LocalObjects local) {

		final Storage storage = checkStorage(spaceShip);

		if(storage == null) {
			return;
		}

		storage.moveItem(startIndex, endIndex, local);
	}

	/**
	 * Запуск телепортирования предмета на корабль.
	 * 
	 * @param ship корабль, который хочет телепортировать предмет.
	 * @param item предмет, который нужно тедепортировать.
	 * @param local контейнер локальных объектов.
	 */
	public static void startTeleportTo(final SpaceShip ship, final SpaceItem item, final LocalObjects local) {

		if(!ship.isInDistance(item, 200)) {
			ship.sendSystemMessage(SystemMessageType.YOU_TOO_FAR_FROM_TARGET, local);
			return;
		}

		final Storage storage = checkStorage(ship);

		if(storage == null) {
			return;
		}

		synchronized(item) {

			if(!item.isVisible() || item.getItemLocation() != ItemLocation.IN_SPACE) {
				return;
			}

			if(item.getLocker() != null) {
				return;
			}

			item.setLocker(ship);
		}

		item.startTeleport(ItemToStorageTeleportHandler.getInstance(), local);
	}
}

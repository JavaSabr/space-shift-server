package com.ss.server.model.storage;

/**
 * Перечисление нахождения предмета в космосе.
 * 
 * @author Ronn
 */
public enum ItemLocation {
	/** удален */
	REMOVED,
	/** предмет находится в космосе */
	IN_SPACE,
	/** предмет находится на корабле */
	IN_SHIP_STORAGE,
}

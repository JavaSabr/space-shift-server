package com.ss.server.model.storage;

import com.ss.server.model.item.SpaceItem;

/**
 * Модель ячейки хранилища.
 * 
 * @author Ronn
 */
public final class Cell {

	/** предмет в ячейке */
	private SpaceItem item;

	/** расположение ячейки */
	private ItemLocation location;

	/** индекс ячейки */
	private int index;

	/**
	 * @return предмет, размещенный в ячейке.
	 */
	public SpaceItem getItem() {
		return item;
	}

	public void init(final ItemLocation location, final int index) {
		this.location = location;
		this.index = index;
	}

	/**
	 * @return пустая ли ячейка.
	 */
	public boolean isEmpty() {
		return item == null;
	}

	/**
	 * @param item предмет, размещенный в ячейке.
	 * @param storage хранилище, в котором лежит ячейка.
	 */
	public void setItem(final SpaceItem item, final Storage storage) {

		final SpaceItem currentItem = getItem();

		if(item != null && currentItem == item) {
			throw new RuntimeException("double set item.");
		}

		if(currentItem != null) {
			currentItem.setItemLocation(ItemLocation.REMOVED);
			currentItem.setIndex(0);
			currentItem.setOwnerId(0);
		}

		this.item = item;

		if(item != null) {
			item.setItemLocation(location);
			item.setIndex(index);
			item.setOwnerId(storage.getOwnerId());
		}
	}
}

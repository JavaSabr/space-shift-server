package com.ss.server.model.storage.impl;

import com.ss.server.LocalObjects;
import com.ss.server.database.ItemDBManager;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.storage.Cell;
import com.ss.server.network.game.packet.server.ResponseStorageInfo;
import com.ss.server.network.game.packet.server.ResponseUpdateStorageCell;

/**
 * Реализация хранилища корабля игрока.
 * 
 * @author Ronn
 */
public class PlayerShipStorage extends ShipStorage<PlayerShip> {

	public static PlayerShipStorage newInstance(final PlayerShip owner) {
		return newInstance(owner, DEFAULT_SIZE);
	}

	public static PlayerShipStorage newInstance(final PlayerShip owner, final int size) {

		final PlayerShipStorage storage = new PlayerShipStorage();
		storage.setSize(size);
		storage.setOwner(owner);
		storage.init();

		return storage;
	}

	public static final int DEFAULT_SIZE = 48;

	@Override
	protected void notifyEmptyCell(final int index, final LocalObjects local) {
		super.notifyEmptyCell(index, local);

		final PlayerShip owner = getOwner();
		owner.sendPacket(ResponseUpdateStorageCell.getInstance(index, local), true);
	}

	@Override
	protected void notifyItemAdded(final Cell cell, final SpaceItem item) {
		super.notifyItemAdded(cell, item);

		final PlayerShip owner = getOwner();
		owner.sendPacket(ResponseUpdateStorageCell.getInstance(item), true);

		final ItemDBManager manager = ItemDBManager.getInstance();
		manager.updateItemLocation(item);
	}

	@Override
	protected void notifyItemRemoved(final Cell cell, final SpaceItem item) {
		super.notifyItemRemoved(cell, item);

		final PlayerShip owner = getOwner();
		owner.sendPacket(ResponseStorageInfo.getInstance(this), true);

		final ItemDBManager manager = ItemDBManager.getInstance();
		manager.updateItem(item);
	}

	@Override
	protected void notifyItemUpdate(final Cell cell, final SpaceItem item) {
		super.notifyItemUpdate(cell, item);

		final PlayerShip owner = getOwner();
		owner.sendPacket(ResponseUpdateStorageCell.getInstance(item), true);

		final ItemDBManager manager = ItemDBManager.getInstance();
		manager.updateItem(item);
	}
}

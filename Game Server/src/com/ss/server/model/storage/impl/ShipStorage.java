package com.ss.server.model.storage.impl;

import com.ss.server.manager.ObjectEventManager;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.storage.Cell;
import com.ss.server.model.storage.ItemLocation;

/**
 * Модель хранилища для игрока.
 * 
 * @author Ronn
 */
public class ShipStorage<T extends SpaceShip> extends AbstractStorage<T> {

	public ShipStorage() {
		super(ItemLocation.IN_SHIP_STORAGE);
	}

	@Override
	public int getOwnerId() {
		return getOwner().getObjectId();
	}

	@Override
	protected void notifyItemAdded(final Cell cell, final SpaceItem item) {
		super.notifyItemAdded(cell, item);

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.notifyItemAdded(getOwner(), item);
	}

	@Override
	protected void notifyItemUpdate(final Cell cell, final SpaceItem item) {
		super.notifyItemUpdate(cell, item);

		final ObjectEventManager eventManager = ObjectEventManager.getInstance();
		eventManager.notifyItemUpdate(getOwner(), item);
	}
}

package com.ss.server.model.storage.impl;

import java.util.concurrent.locks.Lock;

import rlib.concurrent.lock.LockFactory;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.ItemUtils;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.storage.Cell;
import com.ss.server.model.storage.ItemLocation;
import com.ss.server.model.storage.Storage;
import com.ss.server.table.ItemTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Базовая реализация хранилища.
 * 
 * @author Ronn
 */
public abstract class AbstractStorage<T> implements Storage {

	/**
	 * Сохранить в пул список ячеяк.
	 * 
	 * @param cells использованный список ячеяк.
	 */
	protected static void foldCells(final Cell[] cells) {

		Array<Cell[]> pool = POOL_TABLE.get(cells.length);

		if(pool == null) {
			synchronized(POOL_TABLE) {

				pool = POOL_TABLE.get(cells.length);

				if(pool == null) {
					pool = ArrayFactory.newConcurrentArray(Cell[].class);
					POOL_TABLE.put(cells.length, pool);
				}
			}
		}

		pool.add(cells);
	}

	/**
	 * Получить из пула набор ячеяк.
	 * 
	 * @param location тип размещения хранилища.
	 * @param size размер хранилища.
	 * @return список ячеяк для хранилища.
	 */
	protected static Cell[] getCells(final ItemLocation location, final int size) {

		Array<Cell[]> pool = POOL_TABLE.get(size);

		if(pool == null) {
			synchronized(POOL_TABLE) {

				pool = POOL_TABLE.get(size);

				if(pool == null) {
					pool = ArrayFactory.newConcurrentArray(Cell[].class);
					POOL_TABLE.put(size, pool);
				}
			}
		}

		Cell[] cells = pool.pop();

		if(cells != null) {

			for(int i = 0, length = cells.length; i < length; i++) {
				cells[i].init(location, i);
			}

		} else {

			cells = new Cell[size];

			for(int i = 0, length = cells.length; i < length; i++) {
				final Cell cell = new Cell();
				cell.init(location, i);
				cells[i] = cell;
			}
		}

		return cells;
	}

	private static final Table<IntKey, Array<Cell[]>> POOL_TABLE = TableFactory.newIntegerTable();

	/** синхронизатор */
	private final Lock sync;

	/** тип хранилища */
	private final ItemLocation location;

	/** список ячеяк хранилища */
	private Cell[] cells;

	/** владелец хранилища */
	private T owner;

	/** размер хранилища */
	private int size;

	public AbstractStorage(final ItemLocation location) {
		this.location = location;
		this.sync = LockFactory.newReentrantAtomicLock();
	}

	@Override
	public boolean addItem(final int id, long count, final String author) {

		if(count < 1L) {
			return false;
		}

		final ItemTable itemTable = ItemTable.getInstance();
		final ItemTemplate template = itemTable.getTemplate(id);

		if(template == null) {
			return false;
		}

		if(!template.isStackable()) {
			count = 1L;
		}

		final Cell[] cells = getCells();

		Cell empty = null;
		Cell sametype = null;

		// поиск свободной ячейки и ячейки с однотипным предметом
		for(final Cell cell : cells) {

			if(cell.isEmpty()) {

				if(empty == null) {
					empty = cell;
				}

				continue;
			}

			final SpaceItem current = cell.getItem();

			if(current.getTemplateId() == id) {
				sametype = cell;
			}
		}

		// если нужно положить в уже существующую стопку
		if(template.isStackable() && sametype != null) {

			final SpaceItem current = sametype.getItem();
			current.addItemCount(count);

			notifyItemUpdate(sametype, current);
			return true;
		}

		// если есть куда вставить итем
		if(empty != null) {
			final SpaceItem item = ItemUtils.createItem(count, author, template);
			empty.setItem(item, this);
			notifyItemAdded(empty, item);
			return true;
		}

		return false;
	}

	@Override
	public boolean addItem(final SpaceItem item) {

		if(item == null) {
			throw new RuntimeException("item is null.");
		}

		final Cell[] cells = getCells();

		if(contains(item, cells)) {
			throw new RuntimeException("added item is contains.");
		}

		Cell empty = null;
		Cell sametype = null;

		// поиск свободной ячейки и ячейки с однотипным предметом
		for(final Cell cell : cells) {

			if(cell.isEmpty()) {

				if(empty == null) {
					empty = cell;
				}

				continue;
			}

			final SpaceItem current = cell.getItem();

			if(current.getTemplate() == item.getTemplate()) {
				sametype = cell;
			}
		}

		// если нужно положить в уже существующую стопку
		if(item.isStackable() && sametype != null) {

			final SpaceItem current = sametype.getItem();
			current.addItemCount(item.getItemCount());

			notifyItemUpdate(sametype, current);

			item.setOwnerId(0);
			item.setItemLocation(ItemLocation.IN_SPACE);

			return true;
		}

		// если есть куда вставить итем
		if(empty != null) {
			empty.setItem(item, this);
			notifyItemAdded(empty, item);
			return true;
		}

		return false;
	}

	@Override
	public boolean consume(final int itemId, final long itemCount) {

		final Cell[] cells = getCells();

		for(final Cell cell : cells) {

			final SpaceItem item = cell.getItem();

			if(item == null) {
				continue;
			}

			if(item.getTemplateId() == itemId) {

				if(item.getItemCount() < itemCount) {
					return false;
				}

				item.setItemCount(item.getItemCount() - itemCount);

				if(item.getItemCount() > 0) {
					notifyItemUpdate(cell, item);
				} else {
					cell.setItem(null, this);
					notifyItemRemoved(cell, item);
				}

				return true;
			}
		}

		return false;
	}

	@Override
	public boolean contains(final int itemId, final long itemCount) {

		final Cell[] cells = getCells();

		for(final Cell cell : cells) {

			final SpaceItem item = cell.getItem();

			if(item == null) {
				continue;
			}

			if(item.getTemplateId() == itemId) {
				return item.getItemCount() >= itemCount;
			}
		}

		return false;
	}

	/**
	 * Проверка наличия указанного предмета в хранилище.
	 * 
	 * @param item искомы предмет.
	 * @param cells список ячеяк хранилища.
	 * @return есть ли такой предмет в хранилище.
	 */
	protected boolean contains(final SpaceItem item, final Cell[] cells) {

		for(final Cell cell : cells) {
			if(cell.getItem() == item) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void finalyze() {

		final Cell[] cells = getCells();

		for(final Cell cell : cells) {
			cell.setItem(null, this);
		}

		setOwner(null);
		setCells(null);
		setSize(0);
	}

	@Override
	public SpaceItem findItem(final int templateId) {

		final Cell[] cells = getCells();

		for(final Cell cell : cells) {

			final SpaceItem item = cell.getItem();

			if(item == null) {
				continue;
			}

			if(item.getTemplateId() == templateId) {
				return item;
			}
		}

		return null;
	}

	@Override
	public Cell[] getCells() {
		return cells;
	}

	@Override
	public ItemLocation getLocation() {
		return location;
	}

	@Override
	public T getOwner() {
		return owner;
	}

	@Override
	public int getSize() {
		return size;
	}

	/**
	 * Инициализация хранилища.
	 */
	protected void init() {

		if(getOwner() == null) {
			throw new RuntimeException("owner is null.");
		}

		this.cells = getCells(getLocation(), getSize());
	}

	@Override
	public final void lock() {
		sync.lock();
	}

	@Override
	public void moveItem(final int startIndex, final int endIndex, final LocalObjects local) {

		final Cell[] cells = getCells();

		if(startIndex < 0 || endIndex < 0 || startIndex >= cells.length || endIndex >= cells.length) {
			return;
		}

		final Cell source = cells[startIndex];
		final Cell destination = cells[endIndex];

		final SpaceItem moved = source.getItem();
		final SpaceItem old = destination.getItem();

		source.setItem(null, this);
		destination.setItem(null, this);

		source.setItem(old, this);
		destination.setItem(moved, this);

		if(moved != null) {
			notifyItemUpdate(destination, moved);
		} else {
			notifyEmptyCell(endIndex, local);
		}

		if(old != null) {
			notifyItemUpdate(source, old);
		} else {
			notifyEmptyCell(startIndex, local);
		}
	}

	protected void notifyEmptyCell(final int index, final LocalObjects local) {

	}

	/**
	 * Уведомление реализаций о добавлении предмета в хранилище.
	 * 
	 * @param cell обновленная ячейка.
	 * @param item добавленный предмет.
	 */
	protected void notifyItemAdded(final Cell cell, final SpaceItem item) {

	}

	protected void notifyItemRemoved(final Cell cell, final SpaceItem item) {

	}

	/**
	 * Уведомление реализаций о обновлении предмета в хранилище.
	 * 
	 * @param cell обновленная ячейка.
	 * @param item обновленный предмет.
	 */
	protected void notifyItemUpdate(final Cell cell, final SpaceItem item) {

	}

	@Override
	public boolean removeItem(final int objectId, final int count) {

		if(count < 1) {
			return false;
		}

		final Cell[] cells = getCells();

		for(final Cell cell : cells) {

			final SpaceItem item = cell.getItem();

			if(item == null || item.getObjectId() != objectId) {
				continue;
			}

			final long current = item.getItemCount();

			if(current < count) {
				return false;
			}

			item.setItemCount(current - count);

			if(item.getItemCount() > 0) {
				notifyItemUpdate(cell, item);
			} else {
				cell.setItem(null, this);
				notifyItemRemoved(cell, item);
			}

			return true;
		}

		return false;
	}

	/**
	 * @param cells список ячеяк хранилища.
	 */
	private void setCells(final Cell[] cells) {
		this.cells = cells;
	}

	@Override
	public void setItem(final SpaceItem item, final int index) {
		final Cell cell = getCells()[index];
		cell.setItem(item, this);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setOwner(final Object owner) {
		this.owner = (T) owner;
	}

	@Override
	public void setSize(final int size) {
		this.size = size;
	}

	@Override
	public final void unlock() {
		sync.unlock();
	}
}

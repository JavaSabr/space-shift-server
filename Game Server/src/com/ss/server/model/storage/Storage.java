package com.ss.server.model.storage;

import rlib.util.Synchronized;
import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.SpaceItem;

/**
 * Интерфейс для реализации храналища предметов.
 * 
 * @author Ronn
 */
public interface Storage extends Foldable, Synchronized {

	/**
	 * Добавление предметов в хранилище.
	 * 
	 * @param id ид шаблона предмета.
	 * @param count кол-во предметов.
	 * @param author автор предметов.
	 * @return были ли успешно добавлены предметы.
	 */
	public boolean addItem(int id, long count, String author);

	/**
	 * Добавить предмет в хранилище.
	 * 
	 * @param item добавляемый предмет.
	 * @return был ли добалвен предмет.
	 */
	public boolean addItem(SpaceItem item);

	/**
	 * Потребление предметов.
	 * 
	 * @param itemId ид потребляемого предмета.
	 * @param itemCount кол-во потребляемых предметов.
	 * @return успешно ли были потреблены
	 */
	public boolean consume(int itemId, long itemCount);

	/**
	 * Проверка на содержание нужного кол-ва предметов в хранилище.
	 * 
	 * @param itemId ид предмета.
	 * @param itemCount кол-во предметов.
	 * @return содержатся ли эти предметы.
	 */
	public boolean contains(int itemId, long itemCount);

	/**
	 * Поиск предмета по указанному ид шаблону.
	 * 
	 * @param templateId ид шаблона предмета.
	 * @return найденный предмет.
	 */
	public SpaceItem findItem(int templateId);

	/**
	 * @return список ячеяк хранилища.
	 */
	public Cell[] getCells();

	/**
	 * @return тип хранилища.
	 */
	public ItemLocation getLocation();

	/**
	 * @return владелец хранилища.
	 */
	public Object getOwner();

	/**
	 * @return ид владельца хранилища.
	 */
	public int getOwnerId();

	/**
	 * @return размер хранилища.
	 */
	public int getSize();

	/**
	 * Перемещение предмета из одной ячейки в другую.
	 * 
	 * @param startIndex индекс изначальной ячейки.
	 * @param endIndex индекс целевой ячейки.
	 * @param local контейнер локальных объектов.
	 */
	public void moveItem(int startIndex, int endIndex, LocalObjects local);

	/**
	 * Удаление предметов из хранилища.
	 * 
	 * @param objectId уникальный ид удаляемого предмета.
	 * @param count кол-во удаляемых предметов.
	 * @return было ли удалено все как надо было.
	 */
	public boolean removeItem(int objectId, int count);

	/**
	 * Установка предмета в ячейку с указанным индексом.
	 * 
	 * @param item устанавливаемый предмет.
	 * @param index устанавливаемый индекс.
	 */
	public void setItem(SpaceItem item, int index);

	/**
	 * @param object владелец хранилища.
	 */
	public void setOwner(Object owner);

	/**
	 * @param size размер хранилища.
	 */
	public void setSize(int size);
}

package com.ss.server.model.station;

import com.ss.server.model.SpaceObject;
import com.ss.server.model.SpaceObjectType;
import com.ss.server.model.station.impl.BaseStation;

/**
 * Перечисление типов космических станций.
 * 
 * @author Ronn
 */
public enum SpaceStationType implements SpaceObjectType {
	BASE_STATION(BaseStation.class);

	private Class<? extends SpaceStation> instanceClass;

	private SpaceStationType(final Class<? extends SpaceStation> instanceClass) {
		this.instanceClass = instanceClass;
	}

	@Override
	public Class<? extends SpaceObject> getInstanceClass() {
		return instanceClass;
	}

	@Override
	public int index() {
		return ordinal();
	}
}

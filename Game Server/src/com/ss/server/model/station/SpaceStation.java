package com.ss.server.model.station;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.nps.StationDefenderNps;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.module.StationModule;
import com.ss.server.model.station.module.external.ExternalStationModule;

/**
 * Интерфейс для реализации космической станции.
 * 
 * @author Ronn
 */
public interface SpaceStation extends GravityObject {

	/**
	 * Выдача доступных заданий для указанного игрока.
	 * 
	 * @param container контейнер заданий.
	 * @param event событие с игроком.
	 * @param local контейнер локальных объектов.
	 */
	public void addAvailableQuests(Array<Quest> container, QuestEvent event, LocalObjects local);

	/**
	 * Радиус, в котором {@link StationDefenderNps} уничтожают нарушителей
	 * близлежащей зоны {@link SpaceStation}.
	 * 
	 * @return радиус для уничтожения.
	 */
	public int getDestroyRadius();

	/**
	 * @return список ангаров.
	 */
	public Hangar[] getHangars();

	/**
	 * @return название станции.
	 */
	public String getName();

	/**
	 * @return список связанных квестов.
	 */
	public Quest[] getQuests();

	/**
	 * Радиус действия телепорта станции, в котором он может телепортировать
	 * какой-нибудь корабль на борт.
	 * 
	 * @return радиус телепортации станции.
	 */
	public int getTeleportRadius();

	/**
	 * Радиус при вхождения в которой, {@link SpaceStation} высылает
	 * предупреждение о запретном сближении.
	 * 
	 * @return радиус для предупреждения.
	 */
	public int getWarningRadius();

	/**
	 * Проверка наличия доступного модуля у станции.
	 * 
	 * @param playerShip корабль игрока.
	 * @param moduleClass класс модуля.
	 * @param local контейнер локальных объектов.
	 * @return есть ли у станции доступный модуль для корабля игрока.
	 */
	public boolean isAvailableModule(PlayerShip playerShip, Class<? extends ExternalStationModule> moduleClass, LocalObjects local);

	/**
	 * Отобразить предупреждение о приближении.
	 * 
	 * @param playerShip корабль, который вошел в опасную зону
	 * {@link SpaceStation}.
	 * @param local контейнер локальных объектов.
	 */
	public void showWarning(PlayerShip playerShip, LocalObjects local);

	/**
	 * @return список модулей станции.
	 */
	public StationModule[] getModules();

	/**
	 * Поиск интересуемого модуля на станции.
	 * 
	 * @param moduleClass класс модуля.
	 * @return искомый модулт либо <code>null</code>.
	 */
	public <T extends StationModule> T findModule(Class<T> moduleClass);
}

package com.ss.server.model.station.impl;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.template.StationTemplate;

/**
 * Модель базовой космической станции.
 * 
 * @author Ronn
 */
public class BaseStation extends AbstractSpaceStation {

	public BaseStation(final int objectId, final StationTemplate template) {
		super(objectId, template);
	}

	@Override
	public void addAvailableQuests(final Array<Quest> container, final QuestEvent event, final LocalObjects local) {

		final Quest[] quests = getTemplate().getQuests();

		if(quests == null || quests.length < 1) {
			return;
		}

		final Quest current = event.getQuest();

		for(final Quest quest : quests) {

			event.setQuest(quest);

			if(quest.isInteresting(event)) {
				container.add(quest);
			}
		}

		event.setQuest(current);
	}
}

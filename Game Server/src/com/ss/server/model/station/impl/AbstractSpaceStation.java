package com.ss.server.model.station.impl;

import rlib.geom.Rotation;
import rlib.util.array.Array;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.model.gravity.impl.AbstractGravityObject;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.module.StationModule;
import com.ss.server.model.station.module.external.ExternalStationModule;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseSpaceStationInfo;
import com.ss.server.template.HangarTemplate;
import com.ss.server.template.StationTemplate;

/**
 * Модель космической станции.
 * 
 * @author Ronn
 */
public abstract class AbstractSpaceStation extends AbstractGravityObject<StationTemplate> implements SpaceStation {

	/** список модулей станции */
	private final StationModule[] modules;

	/** массив ангаров станции */
	private Hangar[] hangars;

	public AbstractSpaceStation(final int objectId, final StationTemplate template) {
		super(objectId, template);

		final HangarTemplate[] temps = template.getHangars();
		final Hangar[] hangars = new Hangar[temps.length];

		for(int i = 0, length = hangars.length; i < length; i++) {
			hangars[i] = temps[i].newInstance(this);
		}

		final Array<StationModule> modules = template.getModules();

		this.modules = new StationModule[modules.size()];

		for(int i = 0, length = modules.size(); i < length; i++) {
			this.modules[i] = modules.get(i).copy();
		}

		setHangars(hangars);
		setRotation(template.getRotation(), LocalObjects.get());
	}

	@Override
	public StationModule[] getModules() {
		return modules;
	}

	@Override
	public <T extends StationModule> T findModule(final Class<T> moduleClass) {

		final StationModule[] modules = getModules();

		if(modules.length < 1) {
			return null;
		}

		// сначало ищем по точному совпадению класса
		for(final StationModule module : modules) {
			if(moduleClass == module.getClass()) {
				return moduleClass.cast(module);
			}
		}

		// если не нашли по точному, ищем по наследованию
		for(final StationModule module : modules) {
			if(moduleClass.isInstance(module)) {
				return moduleClass.cast(module);
			}
		}

		return null;
	}

	@Override
	public boolean isAvailableModule(final PlayerShip playerShip, final Class<? extends ExternalStationModule> moduleClass, final LocalObjects local) {

		final StationModule[] modules = getModules();

		if(modules.length < 1) {
			return false;
		}

		final ExternalStationModule module = findModule(moduleClass);

		if(module == null) {
			return false;
		}

		return module.condition(this, playerShip, local);
	}

	@Override
	public void addQuestButtons(final Array<QuestButton> container, final PlayerShip playerShip, final LocalObjects local) {

		final QuestEvent event = local.getNextQuestEvent();

		event.setPlayerShip(playerShip);
		event.setStation(this);

		for(final Quest quest : getQuests()) {
			event.setQuest(quest);
			quest.addButtons(container, event);
		}
	}

	@Override
	public int getClassId() {
		return Config.SERVER_STATION_CLASS_ID;
	}

	@Override
	public int getDestroyRadius() {
		return template.getDestroyRadius();
	}

	@Override
	public final Hangar[] getHangars() {
		return hangars;
	}

	@Override
	protected ServerPacket getInfoPacket() {
		return ResponseSpaceStationInfo.getInstance(this);
	}

	@Override
	public String getName() {
		return template.getName();
	}

	@Override
	public Quest[] getQuests() {
		return template.getQuests();
	}

	@Override
	public SpaceStation getSpaceStation() {
		return this;
	}

	@Override
	public int getTeleportRadius() {
		return template.getTeleportRadius();
	}

	@Override
	public int getWarningRadius() {
		return template.getWarningRadius();
	}

	@Override
	public boolean isSpaceStation() {
		return true;
	}

	protected final void setHangars(final Hangar[] hangars) {
		this.hangars = hangars;
	}

	@Override
	public void setRotation(final Rotation newRotation, final LocalObjects local) {
		super.setRotation(newRotation, local);

		final Hangar[] hangars = getHangars();

		for(int i = 0, length = hangars.length; i < length; i++) {
			hangars[i].rotate(newRotation, local);
		}
	}

	@Override
	public void showWarning(final PlayerShip playerShip, final LocalObjects local) {
		// TODO реализовать в блоее нормальном виде
		playerShip.sendMessage("ВЫ СЛИШКОМ БЛИЗНО ПОДЛЕТАЕТЕ К СТАНЦИИ, ВЫ МОЖЕТЕ БЫТЬ УНИЧТОЖЕНЫ!!!!", local);
	}

	@Override
	public void spawnMe(final LocalObjects local) {
		super.spawnMe(local);
		SPACE.addContainer(this);
	}
}

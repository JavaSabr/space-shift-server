package com.ss.server.model.station.hangar.function;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.manager.ClassManager;

/**
 * Фабрика функций в ангаре.
 * 
 * @author Ronn
 */
public class HangarFunctionFactory {

	/**
	 * Создание функции ангара.
	 * 
	 * @param node узел хмл.
	 * @param vars таблица атрибутов.
	 * @return новая функция ангара.
	 */
	public static final HangarFunction create(final Node node, final VarTable vars) {

		final String name = vars.getString(PROP_NAME);
		final HangarFunctionType type = TABLE.get(name);

		if(type == null) {
			return null;
		}

		return type.create(node, vars);
	}

	private static final void register(final Class<HangarFunction> type) {
		final String name = type.getSimpleName().replaceFirst(REGEX, StringUtils.EMPTY);
		TABLE.put(name, new HangarFunctionType(name, type));
	}

	private static final Logger LOGGER = LoggerManager.getLogger(HangarFunctionFactory.class);

	public static final String PROP_NAME = "name";

	private static final String REGEX = HangarFunction.class.getSimpleName();

	/** таблица всех видов функций ангара */
	private static final Table<String, HangarFunctionType> TABLE = TableFactory.newObjectTable();

	static {

		final Array<Class<HangarFunction>> container = ArrayFactory.newArray(Class.class);

		final ClassManager manager = ClassManager.getInstance();
		manager.findImplements(container, HangarFunction.class);

		for(final Class<HangarFunction> cs : container) {
			register(cs);
		}

		LOGGER.info("register " + container.size() + " hangar functions.");
	}
}

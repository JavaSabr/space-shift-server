package com.ss.server.model.station.hangar.window.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.model.station.hangar.window.HangarWindowType;

/**
 * Реализация окна с мастерской.
 * 
 * @author Ronn
 */
public class WorkShopHangarWindow extends AbstractHangarWindow {

	public static final int INDEX = 2;

	public WorkShopHangarWindow(final HangarWindowType type, final Node node, final VarTable vars) {
		super(type, node, vars);
	}

	@Override
	public HangarWindow copy() {
		final WorkShopHangarWindow window = new WorkShopHangarWindow(getType(), null, null);
		return window;
	}

	@Override
	public int getIndex() {
		return INDEX;
	}

	@Override
	public String getUIName() {
		return "@interface:hangarWindowWorkShop@";
	}
}

package com.ss.server.model.station.hangar.function;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;

/**
 * Интерфейс для реализации функции ангара.
 * 
 * @author Ronn
 */
public interface HangarFunction {

	/**
	 * Приминение функции ангара на игрока.
	 * 
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void apply(Hangar hangar, SpaceStation station, PlayerShip playerShip, LocalObjects local);

	/**
	 * Проверка условий приминения функции.
	 * 
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip игрок, который запрашивает функцию.
	 * @param local контейнер локальных объектов.
	 * @return можно ли приминить функцию.
	 */
	public boolean condition(Hangar hangar, SpaceStation station, PlayerShip playerShip, LocalObjects local);

	/**
	 * @return новая копия этой функции.
	 */
	public HangarFunction copy();

	/**
	 * @return тип функции.
	 */
	public HangarFunctionType getType();

	/**
	 * @return название функции для отображения.
	 */
	default public String getUIName() {
		return "<unknown>";
	}

	/**
	 * @return виден ли для окна ангара.
	 */
	default public boolean isVisible() {
		return true;
	}
}

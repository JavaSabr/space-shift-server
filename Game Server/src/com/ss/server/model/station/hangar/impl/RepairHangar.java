package com.ss.server.model.station.hangar.impl;

import rlib.geom.Vector;
import rlib.geom.bounding.Bounding;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.template.HangarTemplate;

/**
 * Реализация ремонтного ангара.
 * 
 * @author Ronn
 */
public class RepairHangar extends HangarBox {

	public RepairHangar(final HangarTemplate template) {
		super(template);
	}

	/**
	 * Получение глобальной позиции для восстановления корабля.
	 * 
	 * @param spaceShip космический корабль.
	 * @param local контейнер локальных объектов.
	 * @param store контейнер позиции.
	 * @return позиция для восстановления.
	 */
	public Vector getRestorePosition(final SpaceShip spaceShip, final LocalObjects local, final Vector store) {
		final Bounding bounding = getBounding();
		final Vector center = bounding.getResultCenter(local);
		return center;
	}
}

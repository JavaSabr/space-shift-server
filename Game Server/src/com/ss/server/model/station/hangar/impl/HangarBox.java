package com.ss.server.model.station.hangar.impl;

import rlib.geom.Vector;
import rlib.geom.bounding.BoundingFactory;
import rlib.util.VarTable;

import com.ss.server.template.HangarTemplate;

/**
 * Модель ангара с пространством в виде коробки.
 * 
 * @author Ronn
 */
public class HangarBox extends StationHangar {

	public static final String PROP_EXTENT_Z = "extentZ";
	public static final String PROP_EXTENT_Y = "extentY";
	public static final String PROP_EXTENT_X = "extentX";
	public static final String PROP_OFFSET_Z = "offsetZ";
	public static final String PROP_OFFSET_Y = "offsetY";
	public static final String PROP_OFFSET_X = "offsetX";

	public HangarBox(final HangarTemplate template) {
		super(template);

		final VarTable vars = template.getVars();

		final float offsetX = vars.getFloat(PROP_OFFSET_X);
		final float offsetY = vars.getFloat(PROP_OFFSET_Y);
		final float offsetZ = vars.getFloat(PROP_OFFSET_Z);

		final Vector baseOffset = getBaseOffset();
		baseOffset.setXYZ(offsetX, offsetY, offsetZ);

		final float extentX = vars.getFloat(PROP_EXTENT_X);
		final float extentY = vars.getFloat(PROP_EXTENT_Y);
		final float extentZ = vars.getFloat(PROP_EXTENT_Z);

		this.bounding = BoundingFactory.newBoundingBox(Vector.ZERO, getOffset(), extentX, extentY, extentZ);
	}
}

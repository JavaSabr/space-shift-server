package com.ss.server.model.station.hangar.window.impl;

import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.quest.Quest;
import com.ss.server.model.quest.QuestButton;
import com.ss.server.model.quest.event.QuestEvent;
import com.ss.server.model.quest.reward.QuestReward;
import com.ss.server.model.quest.reward.QuestRewardClass;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.model.station.hangar.window.HangarWindowType;
import com.ss.server.network.game.ServerPacket;

/**
 * Реализация окна со списком заданий.
 * 
 * @author Ronn
 */
public class QuestListHangarWindow extends AbstractHangarWindow {

	public static final int INDEX = 0;

	private static final ThreadLocal<Array<QuestReward>> REWARD_CONTAINER_LOC = new ThreadLocal<Array<QuestReward>>() {

		@Override
		protected Array<QuestReward> initialValue() {
			return ArrayFactory.newArray(QuestReward.class);
		}
	};

	public QuestListHangarWindow(final HangarWindowType type, final Node node, final VarTable vars) {
		super(type, node, vars);
	}

	@Override
	public HangarWindow copy() {
		final QuestListHangarWindow window = new QuestListHangarWindow(getType(), null, null);
		return window;
	}

	@Override
	public int getIndex() {
		return INDEX;
	}

	@Override
	public String getUIName() {
		return "@interface:hangarWindowQuestList@";
	}

	@Override
	public void writeTo(final ServerPacket packet, final ByteBuffer buffer, final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		super.writeTo(packet, buffer, hangar, station, playerShip, local);

		final QuestEvent event = local.getNextQuestEvent();
		event.setPlayerShip(playerShip);
		event.setStation(station);

		final Array<Quest> quests = local.getNextQuestList();
		station.addAvailableQuests(quests, event, local);

		packet.writeInt(buffer, quests.size());

		if(!quests.isEmpty()) {

			final Array<QuestButton> buttons = local.getNextQuestButtonList();
			final Array<QuestReward> rewards = REWARD_CONTAINER_LOC.get();

			for(final Quest quest : quests.array()) {

				if(quest == null) {
					break;
				}

				event.setQuest(quest);

				quest.addButtons(buttons.clear(), event);

				if(buttons.size() != 1) {
					LOGGER.warning("incorrect button count for quest " + quest + " and event " + event);
				}

				final QuestButton first = buttons.first();

				packet.writeInt(buffer, quest.getId());

				String name = quest.getName();
				final String description = first.getDescription();

				packet.writeByte(buffer, name.length());
				packet.writeString(buffer, name);
				packet.writeByte(buffer, description.length());
				packet.writeString(buffer, description);

				name = first.getName();

				packet.writeByte(buffer, first.getId());
				packet.writeByte(buffer, name.length());
				packet.writeString(buffer, name);

				quest.addRewardsTo(rewards.clear(), event, local);

				packet.writeByte(buffer, rewards.size());

				if(!rewards.isEmpty()) {
					for(final QuestReward reward : rewards.array()) {

						if(reward == null) {
							break;
						}

						final QuestRewardClass rewardClass = reward.getRewardClass();
						packet.writeByte(buffer, rewardClass.ordinal());
						rewardClass.writeTo(packet, buffer, reward, event);
					}
				}
			}
		}
	}
}
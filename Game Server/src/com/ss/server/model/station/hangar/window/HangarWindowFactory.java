package com.ss.server.model.station.hangar.window;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.StringUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.manager.ClassManager;

/**
 * Фабрика окон ангара.
 * 
 * @author Ronn
 */
public class HangarWindowFactory {

	/**
	 * Создание окна ангара.
	 * 
	 * @param node узел хмл.
	 * @param vars таблица атрибутов.
	 * @return новое окно ангара.
	 */
	public static final HangarWindow create(final Node node, final VarTable vars) {

		final String name = vars.getString(PROP_NAME);
		final HangarWindowType type = TABLE.get(name);

		if(type == null) {
			return null;
		}

		return type.create(node, vars);
	}

	private static final void register(final Class<HangarWindow> type) {
		final String name = type.getSimpleName().replaceFirst(REGEX, StringUtils.EMPTY);
		TABLE.put(name, new HangarWindowType(name, type));
	}

	private static final Logger LOGGER = LoggerManager.getLogger(HangarWindowFactory.class);

	public static final String PROP_NAME = "name";

	private static final String REGEX = HangarWindow.class.getSimpleName();

	private static final Table<String, HangarWindowType> TABLE = TableFactory.newObjectTable();

	static {

		final Array<Class<HangarWindow>> container = ArrayFactory.newArray(Class.class);

		final ClassManager manager = ClassManager.getInstance();
		manager.findImplements(container, HangarWindow.class);

		for(final Class<HangarWindow> cs : container) {
			register(cs);
		}

		LOGGER.info("register " + container.size() + " hangar windows.");
	}
}

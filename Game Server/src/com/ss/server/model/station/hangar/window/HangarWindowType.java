package com.ss.server.model.station.hangar.window;

import java.lang.reflect.Constructor;

import org.w3c.dom.Node;

import rlib.util.ClassUtils;
import rlib.util.VarTable;

/**
 * Реализация типа окна взаимодействия с ангаром.
 * 
 * @author Ronn
 */
public class HangarWindowType {

	/** конструктор окна */
	private final Constructor<? extends HangarWindow> constructor;
	/** название типа окна */
	private final String name;

	public HangarWindowType(final String name, final Class<? extends HangarWindow> type) {
		this.name = name;
		this.constructor = ClassUtils.getConstructor(type, HangarWindowType.class, Node.class, VarTable.class);
	}

	/**
	 * Создание окна ангара.
	 * 
	 * @param node узел окна.
	 * @param vars набор параметров окна.
	 * @return новая окно.
	 */
	public HangarWindow create(final Node node, final VarTable vars) {
		return ClassUtils.newInstance(constructor, this, node, vars);
	}

	/**
	 * @return название типа окна.
	 */
	public String getName() {
		return name;
	}
}

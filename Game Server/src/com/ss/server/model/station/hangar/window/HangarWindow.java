package com.ss.server.model.station.hangar.window;

import java.nio.ByteBuffer;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.network.game.ServerPacket;

/**
 * Интерфейс для реализации окна взаимодействия с ангаром станции.
 * 
 * @author Ronn
 */
public interface HangarWindow {

	/**
	 * Проверка условий отображения окна.
	 * 
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip игрок, который запрашивает окно.
	 * @param local контейнер локальных объектов.
	 * @return можно ли отобразить окно.
	 */
	public boolean condition(Hangar hangar, SpaceStation station, PlayerShip playerShip, LocalObjects local);

	/**
	 * @return новая копия этого окна.
	 */
	public HangarWindow copy();

	/**
	 * @return ангар, которому принадлежит окно.
	 */
	public Hangar getHangar();

	/**
	 * @return номер типа окна.
	 */
	public int getIndex();

	/**
	 * @return тип окна.
	 */
	public HangarWindowType getType();

	/**
	 * @return название окна для отображения.
	 */
	default public String getUIName() {
		return "<unknown>";
	}

	/**
	 * @param hangar ангар, которому принадлежит окно.
	 */
	public void setHangar(Hangar hangar);

	/**
	 * Обновить окно для игрока.
	 * 
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void updateTo(Hangar hangar, SpaceStation station, PlayerShip playerShip, LocalObjects local);

	/**
	 * Запись данных окна в пакет.
	 * 
	 * @param packet сам пакет для записи.
	 * @param buffer буффер данных для записи.
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	public void writeTo(ServerPacket packet, ByteBuffer buffer, Hangar hangar, SpaceStation station, PlayerShip playerShip, LocalObjects local);
}

package com.ss.server.model.station.hangar.function.impl;

import static com.ss.server.model.rating.local.LocalRatingElementType.SPACE_SHIP_BODY;

import org.w3c.dom.Node;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.function.HangarFunction;
import com.ss.server.model.station.hangar.function.HangarFunctionType;

/**
 * Функция восстановления корабля фракции.
 * 
 * @author Ronn
 */
public class FractionRepairHangarFunction extends AbstractHangarFunction {

	public static final String PROP_FRACTION = "fraction";
	private static final String PROP_LOCATION = "location";
	private static final String PROP_ROTATION = "rotation";

	/** разворот при восстановлении */
	private Rotation rotation;
	/** смещение при восстановлении */
	private Vector location;

	/** целевая фракция */
	private int fractionId;

	public FractionRepairHangarFunction(final HangarFunctionType type, final Node node, final VarTable vars) {
		super(type, node, vars);
	}

	@Override
	protected void applyImpl(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {

		final Vector position = local.getNextVector();
		position.set(station.getLocation());
		position.addLocal(getLocation());

		final Rotation rotation = local.getNextRotation();
		rotation.set(getRotation());

		playerShip.setCurrentStrength(Integer.MAX_VALUE, local);
		playerShip.teleportTo(position, rotation, LocalObjects.get(), playerShip.getLocationId());
		playerShip.updateInfo(local);

		final RatingSystem ratingSystem = playerShip.getRatingSystem();
		ratingSystem.notify(RatingEvents.getRepairSpaceShipBodyRatingEvent(local, SPACE_SHIP_BODY));
	}

	@Override
	protected boolean conditionImpl(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {

		final int fractionId = getFractionId();

		if(fractionId != -1 && playerShip.getFractionId() != fractionId) {
			return false;
		}

		return super.conditionImpl(hangar, station, playerShip, local);
	}

	@Override
	public HangarFunction copy() {

		final FractionRepairHangarFunction function = new FractionRepairHangarFunction(getType(), null, null);
		function.location = getLocation();
		function.rotation = getRotation();
		function.fractionId = getFractionId();

		return function;
	}

	/**
	 * @return целевая фракция.
	 */
	public int getFractionId() {
		return fractionId;
	}

	/**
	 * @return смещение при восстановлении.
	 */
	public Vector getLocation() {
		return location;
	}

	/**
	 * @return разворот при восстановлении.
	 */
	public Rotation getRotation() {
		return rotation;
	}

	@Override
	public boolean isVisible() {
		return false;
	}

	@Override
	protected void parse(final Node node, final VarTable vars) {

		float[] vals = vars.getFloatArray(PROP_LOCATION, ",", ArrayFactory.toFloatArray(0, 0, 0));

		this.location = Vector.newInstance(vals);

		vals = vars.getFloatArray(PROP_ROTATION, ",", ArrayFactory.toFloatArray(0, 0, 0, 0));

		this.rotation = Rotation.newInstance(vals);
		this.fractionId = vars.getInteger(PROP_FRACTION, -1);
	}
}

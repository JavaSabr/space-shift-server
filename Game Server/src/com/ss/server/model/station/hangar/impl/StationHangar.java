package com.ss.server.model.station.hangar.impl;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.geom.bounding.Bounding;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.ObjectContainer;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.function.HangarFunction;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.network.game.packet.server.ResponseHangarWindowInfo;
import com.ss.server.network.game.packet.server.ResponseHangarWindowUpdated;
import com.ss.server.template.HangarTemplate;

/**
 * Реализация ангара станции.
 * 
 * @author Ronn
 */
public class StationHangar implements Hangar {

	protected static final Logger LOGGER = LoggerManager.getLogger(ObjectContainer.class);

	/** список объектов внутри ангара */
	protected final Array<SpaceObject> objects;
	/** список функций ангара */
	protected final HangarFunction[] functions;
	/** список окон ангара */
	protected final HangarWindow[] windows;

	/** объект, в котором находится ангар */
	protected SpaceStation parent;

	/** базовый оступ */
	protected Vector baseOffset;
	/** конечный отступ */
	protected Vector offset;

	/** пространство ангара */
	protected Bounding bounding;

	/** темплейт ангара */
	protected HangarTemplate template;

	public StationHangar(final HangarTemplate template) {
		this.objects = ArrayFactory.newConcurrentArray(SpaceObject.class);
		this.baseOffset = Vector.newInstance();
		this.offset = Vector.newInstance();
		this.template = template;

		// копирование функций ангара
		{
			final Array<HangarFunction> array = template.getFunctions();

			this.functions = new HangarFunction[array.size()];

			for(int i = 0, length = array.size(); i < length; i++) {
				functions[i] = array.get(i).copy();
			}
		}

		// копирование окон ангара
		{
			final Array<HangarWindow> array = template.getWindows();

			this.windows = new HangarWindow[array.size()];

			for(int i = 0, length = array.size(); i < length; i++) {
				windows[i] = array.get(i).copy();
				windows[i].setHangar(this);
			}
		}
	}

	@Override
	public boolean contains(final SpaceObject object) {
		return objects.contains(object);
	}

	@Override
	public boolean contains(final Vector loc, final VectorBuffer buffer) {
		return bounding.contains(loc, buffer);
	}

	@Override
	public void finalyze() {
		setParent(null);
	}

	/**
	 * @return базовый отступ.
	 */
	protected Vector getBaseOffset() {
		return baseOffset;
	}

	/**
	 * @return форма ангара.
	 */
	protected Bounding getBounding() {
		return bounding;
	}

	@Override
	public HangarFunction getFunction(final PlayerShip playerShip, final Class<? extends HangarFunction> functionClass, final LocalObjects local) {

		for(final HangarFunction function : getFunctions()) {

			if(functionClass.isAssignableFrom(function.getClass()) && function.condition(this, getParent(), playerShip, local)) {
				return function;
			}
		}

		return null;
	}

	/**
	 * @return список функций ангара.
	 */
	public HangarFunction[] getFunctions() {
		return functions;
	}

	@Override
	public Array<SpaceObject> getObjects() {
		return objects;
	}

	/**
	 * @return отступ.
	 */
	protected Vector getOffset() {
		return offset;
	}

	@Override
	public SpaceStation getParent() {
		return parent;
	}

	@Override
	public HangarTemplate getTemplate() {
		return template;
	}

	/**
	 * @return список окон ангара.
	 */
	public HangarWindow[] getWindows() {
		return windows;
	}

	@Override
	public void onEnter(final SpaceObject object, final LocalObjects local) {

	}

	@Override
	public void onExit(final SpaceObject object, final LocalObjects local) {

	}

	@Override
	public void openWindow(final PlayerShip playerShip, final LocalObjects local) {
		playerShip.sendPacket(ResponseHangarWindowInfo.getInstance(this, playerShip, local), true);
	}

	@Override
	public void rotate(final Rotation rotation, final LocalObjects local) {
		offset.set(baseOffset);
		rotation.multLocal(offset);
		bounding.update(rotation, local);
	}

	@Override
	public void setParent(final SpaceStation parent) {

		if(parent == null) {
			bounding.setCenter(Vector.ZERO);
		} else {
			bounding.setCenter(parent.getLocation());
		}

		this.parent = parent;
	}

	@Override
	public void update(final VectorBuffer buffer, final LocalObjects local) {

		final Array<SpaceObject> objects = getObjects();

		objects.writeLock();
		try {

			final SpaceObject[] array = objects.array();

			for(int i = 0, length = objects.size(); i < length; i++) {

				final SpaceObject object = array[i];

				if(!contains(object.getLocation(), buffer)) {
					onExit(object, local);
					length--;
				}
			}

		} finally {
			objects.writeUnlock();
		}
	}

	@Override
	public void updateWindow(final PlayerShip playerShip, final LocalObjects local) {

		final SpaceStation station = getParent();

		final Array<HangarWindow> windows = playerShip.getOpeHangarWindows();
		windows.readLock();
		try {

			for(final HangarWindow window : windows.array()) {

				if(window == null) {
					break;
				}

				window.updateTo(this, station, playerShip, local);
			}

		} finally {
			windows.readUnlock();
		}

		playerShip.sendPacket(ResponseHangarWindowUpdated.getInstance(), true);
	}
}

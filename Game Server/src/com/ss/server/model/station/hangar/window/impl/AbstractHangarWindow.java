package com.ss.server.model.station.hangar.window.impl;

import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.model.station.hangar.window.HangarWindowType;
import com.ss.server.network.game.ServerPacket;

/**
 * Базовая реализация окна ангара.
 * 
 * @author Ronn
 */
public abstract class AbstractHangarWindow implements HangarWindow {

	protected static final Logger LOGGER = LoggerManager.getLogger(HangarWindow.class);

	/** тип окна ангара */
	private final HangarWindowType type;

	/** ангар, которому принадлежит окно */
	private Hangar hangar;

	public AbstractHangarWindow(final HangarWindowType type, final Node node, final VarTable vars) {
		this.type = type;

		if(node != null && vars != null) {
			parse(node, vars);
		}
	}

	@Override
	public boolean condition(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		return hangar != null && station != null && playerShip != null && conditionImpl(hangar, station, playerShip, local);
	}

	/**
	 * Реализация проверки условий отображения окна.
	 * 
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip игрок, который запрашивает окно.
	 * @param local контейнер локальных объектов.
	 * @return можно ли отобразить окно.
	 */
	protected boolean conditionImpl(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		return true;
	}

	@Override
	public Hangar getHangar() {
		return hangar;
	}

	@Override
	public HangarWindowType getType() {
		return type;
	}

	/**
	 * Парс параметров окна ангара.
	 * 
	 * @param node узел описания в хмл.
	 * @param vars таблица атрибутов окна.
	 */
	protected void parse(final Node node, final VarTable vars) {
	}

	@Override
	public void setHangar(final Hangar hangar) {
		this.hangar = hangar;
	}

	@Override
	public void updateTo(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
	}

	@Override
	public void writeTo(final ServerPacket packet, final ByteBuffer buffer, final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {

		final String uiName = getUIName();

		packet.writeInt(buffer, getIndex());
		packet.writeByte(buffer, uiName.length());
		packet.writeString(buffer, uiName);
	}
}

package com.ss.server.model.station.hangar.function;

import java.lang.reflect.Constructor;

import org.w3c.dom.Node;

import rlib.util.ClassUtils;
import rlib.util.VarTable;

/**
 * Реализация типа функции.
 * 
 * @author Ronn
 */
public class HangarFunctionType {

	/** конструктор функции */
	private final Constructor<? extends HangarFunction> constructor;
	/** название типа функции */
	private final String name;

	public HangarFunctionType(final String name, final Class<? extends HangarFunction> type) {
		this.name = name;
		this.constructor = ClassUtils.getConstructor(type, HangarFunctionType.class, Node.class, VarTable.class);
	}

	/**
	 * Создание функции ангара.
	 * 
	 * @param node узел функции.
	 * @param vars набор параметров функции.
	 * @return новая функция.
	 */
	public HangarFunction create(final Node node, final VarTable vars) {
		return ClassUtils.newInstance(constructor, this, node, vars);
	}

	/**
	 * @return название типа функции.
	 */
	public String getName() {
		return name;
	}
}

package com.ss.server.model.station.hangar;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.geom.VectorBuffer;
import rlib.util.array.Array;
import rlib.util.pools.Foldable;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.function.HangarFunction;
import com.ss.server.template.HangarTemplate;

/**
 * Интерфейс для реализации ангара.
 * 
 * @author Ronn
 */
public interface Hangar extends Foldable {

	/**
	 * Находится ли объект в ангаре.
	 * 
	 * @param object проверяемый объект.
	 * @return содержится ли.
	 */
	public boolean contains(SpaceObject object);

	/**
	 * Находится ли точка в ангаре.
	 * 
	 * @param location проверяемая точка.
	 * @param buffer веткорный буффер.
	 * @return содержится ли.
	 */
	public boolean contains(Vector location, VectorBuffer buffer);

	/**
	 * Получение функции анара для приминения.
	 * 
	 * @param playerShip корабль игрока.
	 * @param functionClass класс функции.
	 * @param local контейнер локальны объектов.
	 * @return сама функция.
	 */
	public HangarFunction getFunction(PlayerShip playerShip, Class<? extends HangarFunction> functionClass, LocalObjects local);

	/**
	 * @return внутренние объекты.
	 */
	public Array<SpaceObject> getObjects();

	/**
	 * @return объект, в которой находится ангар.
	 */
	public SpaceStation getParent();

	/**
	 * @return шаблон ангара.
	 */
	public HangarTemplate getTemplate();

	/**
	 * Обработка входа объекта в ангар.
	 * 
	 * @param object вошедший объект.
	 * @param local контейнер локальных объектов.
	 */
	public void onEnter(SpaceObject object, LocalObjects local);

	/**
	 * Обработка выхода объекта из ангара.
	 * 
	 * @param object вышедший объект.
	 * @param local контейнер локальных объектов.
	 */
	public void onExit(SpaceObject object, LocalObjects local);

	/**
	 * Открытия диалога взаимодействия с ангаром.
	 * 
	 * @param playerShip запрашивающий корабль.
	 * @param local контейнер локальных объектов.
	 */
	public void openWindow(PlayerShip playerShip, LocalObjects local);

	/**
	 * @param rotation новый разворот.
	 * @param local контейнер локальных объектов.
	 */
	public void rotate(Rotation rotation, LocalObjects local);

	/**
	 * Установка центра ангара.
	 * 
	 * @param parent объект, в котором ангар находится.
	 */
	public void setParent(SpaceStation parent);

	/**
	 * Обновить положение самого ангара.
	 * 
	 * @param buffer векторный буффер.
	 * @param local контейнер локальны объектов.
	 */
	public void update(VectorBuffer buffer, LocalObjects local);

	/**
	 * обновление диалога взаимодействия с ангаром.
	 * 
	 * @param playerShip запрашивающий корабль.
	 * @param local контейнер локальных объектов.
	 */
	public void updateWindow(PlayerShip playerShip, LocalObjects local);
}

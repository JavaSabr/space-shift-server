package com.ss.server.model.station.hangar.window.impl;

import java.nio.ByteBuffer;

import org.w3c.dom.Node;

import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.item.SpaceItem;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.shop.ItemPriceInfo;
import com.ss.server.model.shop.ItemPriceList;
import com.ss.server.model.shop.TransactionItemInfo;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.window.HangarWindow;
import com.ss.server.model.station.hangar.window.HangarWindowType;
import com.ss.server.model.storage.Storage;
import com.ss.server.network.game.ServerPacket;
import com.ss.server.network.game.packet.server.ResponseUpdateItemShopHangarWindow;
import com.ss.server.table.ItemShopPriceTable;
import com.ss.server.template.item.ItemTemplate;

/**
 * Реализация окна с магазином предметов.
 * 
 * @author Ronn
 */
public class ItemShopHangarWindow extends AbstractHangarWindow {

	public static final String ATTR_PRICE = "price";

	public static final int INDEX = 1;

	/** таблица данных о продаваемых предметах */
	private final Table<IntKey, ItemPriceInfo> priceInfoTable;

	/** прайс литс этого магазина */
	private ItemPriceList priceList;

	public ItemShopHangarWindow(final HangarWindowType type, final Node node, final VarTable vars) {
		super(type, node, vars);

		this.priceInfoTable = TableFactory.newIntegerTable();

		if(vars == null) {
			return;
		}

		final ItemShopPriceTable priceTable = ItemShopPriceTable.getInstance();

		this.priceList = priceTable.getPriceList(vars.getInteger(ATTR_PRICE));

		if(priceList == null) {
			LOGGER.warning("not found price list for " + vars.getInteger(ATTR_PRICE));
			throw new RuntimeException();
		}

		final Array<ItemPriceInfo> items = priceList.getItems();

		for(final ItemPriceInfo priceItem : items) {
			priceInfoTable.put(priceItem.getTemplateId(), priceItem);
		}
	}

	@Override
	public HangarWindow copy() {

		final ItemShopHangarWindow window = new ItemShopHangarWindow(getType(), null, null);
		window.priceList = getPriceList();
		window.priceInfoTable.put(getPriceInfoTable());

		return window;
	}

	@Override
	public int getIndex() {
		return INDEX;
	}

	/**
	 * @return таблица данных о продаваемых предметах.
	 */
	public Table<IntKey, ItemPriceInfo> getPriceInfoTable() {
		return priceInfoTable;
	}

	/**
	 * @return прайс литс этого магазина.
	 */
	public ItemPriceList getPriceList() {
		return priceList;
	}

	@Override
	public String getUIName() {
		return "@interface:hangarWindowItemShop@";
	}

	/**
	 * Выполнение транзакции по покупке и продажиbuyItems предметов.
	 * 
	 * @param playerShip корабль игрока.
	 * @param buyItems список покупаемых предметов.
	 * @param sellItems список продаваемых предметов.
	 */
	public void processTransaction(final PlayerShip playerShip, final Array<TransactionItemInfo> buyItems, final Array<TransactionItemInfo> sellItems, final LocalObjects local) {

	}

	@Override
	public void updateTo(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		super.updateTo(hangar, station, playerShip, local);

		final Storage storage = playerShip.getStorage();
		final SpaceItem credits = storage.findItem(SpaceItem.ITEM_ID_MONEY);

		playerShip.sendPacket(ResponseUpdateItemShopHangarWindow.getInstance(credits == null ? 0 : credits.getItemCount(), local), true);
	}

	@Override
	public void writeTo(final ServerPacket packet, final ByteBuffer buffer, final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		super.writeTo(packet, buffer, hangar, station, playerShip, local);

		final ItemPriceList priceList = getPriceList();
		final Array<ItemPriceInfo> items = priceList.getItems();

		packet.writeInt(buffer, items.size());

		for(final ItemPriceInfo item : items.array()) {

			if(item == null) {
				break;
			}

			final ItemTemplate template = item.getTemplate();

			packet.writeInt(buffer, template.getId());
			packet.writeInt(buffer, item.getBuy());
			packet.writeInt(buffer, item.getSell());
		}

		final Storage storage = playerShip.getStorage();
		final SpaceItem item = storage.findItem(SpaceItem.ITEM_ID_MONEY);

		packet.writeLong(buffer, item == null ? 0 : item.getItemCount());
	}
}

package com.ss.server.model.station.hangar.function.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.hangar.Hangar;
import com.ss.server.model.station.hangar.function.HangarFunction;
import com.ss.server.model.station.hangar.function.HangarFunctionType;

/**
 * Базовая реализация функции ангара.
 * 
 * @author Ronn
 */
public abstract class AbstractHangarFunction implements HangarFunction {

	/** тип функции */
	private final HangarFunctionType type;

	public AbstractHangarFunction(final HangarFunctionType type, final Node node, final VarTable vars) {
		this.type = type;

		if(node != null && vars != null) {
			parse(node, vars);
		}
	}

	@Override
	public void apply(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		applyImpl(hangar, station, playerShip, local);
	}

	/**
	 * Реализация приминения функции.
	 * 
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip корабль игрока.
	 * @param local контейнер локальных объектов.
	 */
	protected abstract void applyImpl(Hangar hangar, SpaceStation station, PlayerShip playerShip, LocalObjects local);

	@Override
	public boolean condition(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		return hangar != null && station != null && playerShip != null && conditionImpl(hangar, station, playerShip, local);
	}

	/**
	 * Реализация проверки условий.
	 * 
	 * @param hangar ангар, в котором находится игрок.
	 * @param station станция, в которой находится ангар.
	 * @param playerShip игрок, который запрашивает функцию.
	 * @param local контейнер локальных объектов.
	 * @return можно ли приминить функцию.
	 */
	protected boolean conditionImpl(final Hangar hangar, final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		return true;
	}

	@Override
	public HangarFunctionType getType() {
		return type;
	}

	/**
	 * Парс данных из хмл и таблицы атрибутов.
	 * 
	 * @param node узел в хмл функции.
	 * @param vars таблица атрибутов.
	 */
	protected void parse(final Node node, final VarTable vars) {
	}
}

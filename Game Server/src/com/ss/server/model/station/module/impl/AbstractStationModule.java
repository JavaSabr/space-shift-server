package com.ss.server.model.station.module.impl;

import org.w3c.dom.Node;

import rlib.geom.Vector;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.VarTable;

import com.ss.server.model.station.module.StationModule;
import com.ss.server.template.StationTemplate;

/**
 * Базовая реализация модуля станции.
 * 
 * @author Ronn
 */
public abstract class AbstractStationModule implements StationModule {

	protected static final Logger LOGGER = LoggerManager.getLogger(StationModule.class);

	public static final String PROP_POSITION = "position";
	public static final String PROP_ROTATION = "rotation";

	/** позиция модуля относительно станции */
	protected final Vector position;

	public AbstractStationModule() {
		this.position = Vector.newInstance();
	}

	public AbstractStationModule(final StationTemplate template, final Node node) {
		this();

		final VarTable vars = VarTable.newInstance(node);

		final Vector position = getPosition();
		position.set(vars.getVector(PROP_POSITION));
	}

	@Override
	public Vector getPosition() {
		return position;
	}

	@Override
	public StationModule copy() {

		final StationModule module = create();

		copyImpl(module);

		return module;
	}

	/**
	 * Реализация копирования данных в указанный модуль.
	 */
	protected void copyImpl(final StationModule module) {

		final Vector position = module.getPosition();
		position.set(getPosition());
	}

	/**
	 * @return новый экземпляр этого модуля.
	 */
	protected abstract StationModule create();

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [position=" + position + "]";
	}
}

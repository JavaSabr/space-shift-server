package com.ss.server.model.station.module.external.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import org.w3c.dom.Node;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.module.external.ExternalStationModule;
import com.ss.server.model.station.module.impl.AbstractStationModule;
import com.ss.server.template.StationTemplate;

/**
 * Базовая реализация внешнего модуля станции.
 * 
 * @author Ronn
 */
public abstract class AbstractExternalStationModule extends AbstractStationModule implements ExternalStationModule {

	public AbstractExternalStationModule() {
		super();
	}

	public AbstractExternalStationModule(final StationTemplate template, final Node node) {
		super(template, node);
	}

	@Override
	public void execute(final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {

		if(station == null || playerShip == null) {
			return;
		}

		final AtomicBoolean onStation = playerShip.getOnStation();

		if(onStation.get()) {
			return;
		}

		synchronized(onStation) {

			if(onStation.get()) {
				return;
			}

			playerShip.deleteLock();
			try {

				if(playerShip.isDeleted() || !conditionImpl(station, playerShip, local)) {
					return;
				}

				executeImpl(station, playerShip, local);

			} finally {
				playerShip.deleteUnlock();
			}
		}
	}

	@Override
	public boolean condition(final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {

		if(station == null || playerShip == null) {
			return false;
		}

		final AtomicBoolean onStation = playerShip.getOnStation();

		if(onStation.get()) {
			return false;
		}

		synchronized(onStation) {

			if(onStation.get()) {
				return false;
			}

			playerShip.deleteLock();
			try {

				if(playerShip.isDeleted()) {
					return false;
				}

				return conditionImpl(station, playerShip, local);

			} finally {
				playerShip.deleteUnlock();
			}
		}
	}

	/**
	 * Выполнение работы модуля на кораблей игрока.
	 * 
	 * @param station станция, в которой находится модуль.
	 * @param playerShip игрок, который хочет воспользоваться модулем.
	 * @param local контейнер локальных объектов.
	 */
	protected abstract void executeImpl(final SpaceStation station, final PlayerShip playerShip, final LocalObjects local);

	/**
	 * Проверка условий доступности модуля для корабля игрока.
	 * 
	 * @param station станция, в которой находится модуль.
	 * @param playerShip игрок, который хочет воспользоваться модулем.
	 * @param local контейнер локальных объектов.
	 * @return можно ли кораблю игрока воспользоваться модулем.
	 */
	protected boolean conditionImpl(final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {
		return true;
	}
}

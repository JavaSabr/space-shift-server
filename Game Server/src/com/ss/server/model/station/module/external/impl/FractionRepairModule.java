package com.ss.server.model.station.module.external.impl;

import org.w3c.dom.Node;

import rlib.util.VarTable;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.module.StationModule;
import com.ss.server.template.StationTemplate;

/**
 * Реализация модуля восстановления кораблей с учетом фракции корабля.
 * 
 * @author Ronn
 */
public class FractionRepairModule extends RepairModule {

	public static final String PROP_FRACTION_ID = "fractionId";

	/** ид фракции */
	private int fractionId;

	public FractionRepairModule() {
		super();
	}

	public FractionRepairModule(final StationTemplate template, final Node node) {
		super(template, node);
	}

	@Override
	protected void copyImpl(final StationModule module) {
		super.copyImpl(module);

		final FractionRepairModule repairModule = (FractionRepairModule) module;
		repairModule.setFractionId(getFractionId());
	}

	@Override
	protected void initImpl(final VarTable vars) {
		super.initImpl(vars);

		setFractionId(vars.getInteger(PROP_FRACTION_ID));
	}

	@Override
	protected StationModule create() {
		return new FractionRepairModule();
	}

	/**
	 * @return ид фракции.
	 */
	public int getFractionId() {
		return fractionId;
	}

	/**
	 * @param fractionId ид фракции.
	 */
	public void setFractionId(final int fractionId) {
		this.fractionId = fractionId;
	}

	@Override
	protected boolean conditionImpl(final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {

		if(!super.conditionImpl(station, playerShip, local)) {
			return false;
		}

		final int fractionId = getFractionId();

		if(fractionId != -1 && playerShip.getFractionId() != fractionId) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [fractionId=" + fractionId + ", repairLocation=" + repairLocation + ", repairRotation=" + repairRotation + ", repairRadius=" + repairRadius
				+ ", randomRotation=" + randomRotation + ", position=" + position + "]";
	}
}

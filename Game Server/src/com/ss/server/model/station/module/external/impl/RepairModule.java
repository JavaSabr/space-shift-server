package com.ss.server.model.station.module.external.impl;

import static com.ss.server.model.rating.local.LocalRatingElementType.SPACE_SHIP_BODY;
import static rlib.geom.DirectionType.DIRECTION;
import static rlib.util.array.ArrayFactory.toFloatArray;

import org.w3c.dom.Node;

import rlib.geom.Rotation;
import rlib.geom.Vector;
import rlib.util.VarTable;
import rlib.util.random.Random;

import com.ss.server.LocalObjects;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.module.StationModule;
import com.ss.server.template.StationTemplate;

/**
 * Реализация модуля восстановления кораблей.
 * 
 * @author Ronn
 */
public class RepairModule extends AbstractExternalStationModule {

	public static final String PROP_RANDOM_ROTATION = "randomRotation";
	public static final String PROP_REPAIR_RADIUS = "repairRadius";
	public static final String PROP_REPAIR_LOCATION = "repairLocation";
	public static final String PROP_REPAIR_ROTATION = "repairRotation";

	/** позиция восстановления относительно модуля */
	protected final Vector repairLocation;
	/** разворот корабля после восстановления */
	protected final Rotation repairRotation;

	/** радиус размещение восстановленного корабля от точки восстановления */
	protected int repairRadius;

	/** рандомный ли разворот после восстановления */
	protected boolean randomRotation;

	public RepairModule() {
		super();

		this.repairLocation = Vector.newInstance();
		this.repairRotation = Rotation.newInstance();
	}

	public RepairModule(final StationTemplate template, final Node node) {
		super(template, node);

		this.repairLocation = Vector.newInstance();
		this.repairRotation = Rotation.newInstance();

		final VarTable vars = VarTable.newInstance(node, "set", "name", "value");

		initImpl(vars);
	}

	protected void initImpl(final VarTable vars) {

		final Rotation repairRotation = getRepairRotation();
		repairRotation.fromAngles(vars.getFloatArray(PROP_REPAIR_ROTATION, ",", toFloatArray(0, 0, 0)));

		final Vector repairLocation = getRepairLocation();
		repairLocation.set(vars.getVector(PROP_REPAIR_LOCATION));

		setRepairRadius(vars.getInteger(PROP_REPAIR_RADIUS, 0));
		setRandomRotation(vars.getBoolean(PROP_RANDOM_ROTATION, false));
	}

	@Override
	protected StationModule create() {
		return new RepairModule();
	}

	@Override
	protected void copyImpl(final StationModule module) {
		super.copyImpl(module);

		final RepairModule repairModule = (RepairModule) module;
		repairModule.setRepairRadius(getRepairRadius());
		repairModule.setRandomRotation(isRandomRotation());

		final Vector repairLocation = repairModule.getRepairLocation();
		repairLocation.set(getRepairLocation());

		final Rotation repairRotation = repairModule.getRepairRotation();
		repairRotation.set(getRepairRotation());
	}

	/**
	 * @return позиция восстановления относительно модуля.
	 */
	public Vector getRepairLocation() {
		return repairLocation;
	}

	/**
	 * @return радиус размещение восстановленного корабля от точки
	 * восстановления.
	 */
	public int getRepairRadius() {
		return repairRadius;
	}

	/**
	 * @return разворот корабля после восстановления.
	 */
	public Rotation getRepairRotation() {
		return repairRotation;
	}

	/**
	 * @return рандомный ли разворот после восстановления.
	 */
	public boolean isRandomRotation() {
		return randomRotation;
	}

	/**
	 * @param repairRadius радиус размещение восстановленного корабля от точки
	 * восстановления.
	 */
	public void setRepairRadius(final int repairRadius) {
		this.repairRadius = repairRadius;
	}

	/**
	 * @param randomRotation рандомный ли разворот после восстановления.
	 */
	public void setRandomRotation(final boolean randomRotation) {
		this.randomRotation = randomRotation;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [repairLocation=" + repairLocation + ", repairRotation=" + repairRotation + ", repairRadius=" + repairRadius + ", randomRotation=" + randomRotation
				+ ", position=" + position + "]";
	}

	@Override
	protected void executeImpl(final SpaceStation station, final PlayerShip playerShip, final LocalObjects local) {

		final Random random = local.getRandom();

		final Rotation rotation = local.getNextRotation();
		rotation.set(station.getRotation());

		final Vector position = local.getNextVector();
		position.set(getPosition());
		position.addLocal(getRepairLocation());
		rotation.multLocal(position);
		position.addLocal(station.getLocation());

		if(isRandomRotation()) {
			rotation.random(random);
		} else {
			rotation.set(getRepairRotation());
		}

		final int repairRadius = getRepairRadius();

		if(repairRadius > 0) {

			final Rotation temp = local.getNextRotation();
			temp.random();

			final Vector offset = temp.getVectorDirection(DIRECTION, local.getNextVector());
			offset.multLocal(random.nextInt(repairRadius));

			position.addLocal(offset);
		}

		playerShip.setCurrentStrength(Integer.MAX_VALUE, local);
		playerShip.teleportTo(position, rotation, local, playerShip.getLocationId());
		playerShip.updateInfo(local);

		final RatingSystem ratingSystem = playerShip.getRatingSystem();
		ratingSystem.notify(RatingEvents.getRepairSpaceShipBodyRatingEvent(local, SPACE_SHIP_BODY));
	}
}

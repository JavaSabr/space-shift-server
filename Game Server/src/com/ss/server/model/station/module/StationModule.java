package com.ss.server.model.station.module;

import rlib.geom.Vector;

/**
 * Интерфейс для реализации модуля станци.
 * 
 * @author Ronn
 */
public interface StationModule {

	/**
	 * @return позиция модуля относительно станции.
	 */
	public Vector getPosition();

	/**
	 * Копирование модуля.
	 */
	public StationModule copy();
}

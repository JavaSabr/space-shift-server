package com.ss.server.model.station.module.external;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.model.station.SpaceStation;
import com.ss.server.model.station.module.StationModule;

/**
 * Реализация внешнего модуля станции, внешние модули используются для
 * реализации какого-то функционала станции доступного вне нахождения в самой
 * станции.
 * 
 * @author Ronn
 */
public interface ExternalStationModule extends StationModule {

	/**
	 * Выполнение работы модуля на кораблей игрока.
	 * 
	 * @param station станция, в которой находится модуль.
	 * @param playerShip игрок, который хочет воспользоваться модулем.
	 * @param local контейнер локальных объектов.
	 */
	public void execute(SpaceStation station, PlayerShip playerShip, LocalObjects local);

	/**
	 * Проверка условий доступности модуля для корабля игрока.
	 * 
	 * @param station станция, в которой находится модуль.
	 * @param playerShip игрок, который хочет воспользоваться модулем.
	 * @param local контейнер локальных объектов.
	 * @return можно ли кораблю игрока воспользоваться модулем.
	 */
	public boolean condition(SpaceStation station, PlayerShip playerShip, LocalObjects local);
}

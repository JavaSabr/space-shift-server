package com.ss.server.model;

/**
 * Интерфейс с перечислением видов переменных системных сообщений.
 * 
 * @author Ronn
 */
public interface SystemMessageVars {

	public static final String VAR_ADD_EXP = "%exp%";
	public static final String VAR_ITEM_NAME = "%item_name%";
	public static final String VAR_ITEM_COUNT = "%item_count%";
	public static final String VAR_OBJECT_NAME = "%object_name%";
	public static final String VAR_NPS_DESTROYED_COUNT = "%nps_destroyed_count%";
	public static final String VAR_SKILL_NAME = "%skill_name%";
}

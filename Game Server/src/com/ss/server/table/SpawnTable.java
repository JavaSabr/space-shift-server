package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.PathBuilder;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.ServerThread;
import com.ss.server.document.DocumentItemSpawn;
import com.ss.server.document.DocumentNpsSpawn;
import com.ss.server.model.item.spawn.ItemSpawn;
import com.ss.server.model.ship.nps.spawn.NpsSpawn;
import com.ss.server.model.spawn.Spawn;

/**
 * Таблица различных спавнов.
 * 
 * @author Ronn
 */
public class SpawnTable {

	private static final Logger LOGGER = LoggerManager.getLogger(SpawnTable.class);

	public static final String ITEM_FOLDER_NAME = "item";
	public static final String NPS_FOLDER_NAME = "nps";

	public static final String PVP_SPAWN_FOLDER_NAME = "pvp-spawns";
	public static final String PVE_SPAWN_FOLDER_NAME = "pve-spawns";

	private static SpawnTable instance;

	public static SpawnTable getInstance() {

		if(instance == null) {
			instance = new SpawnTable();
		}

		return instance;
	}

	/** список всех спавнов */
	private final Array<Spawn> spawns;
	/** список спавнов NPS */
	private final Array<NpsSpawn> npsSpawns;
	/** список спавннов предметов */
	private final Array<ItemSpawn> itemSpawns;

	private SpawnTable() {
		InitializeManager.valid(getClass());

		spawns = ArrayFactory.newArray(Spawn.class);
		npsSpawns = ArrayFactory.newArray(NpsSpawn.class);
		itemSpawns = ArrayFactory.newArray(ItemSpawn.class);

		File[] files = FileUtils.getFiles(getNpsSpawnFolder(), "xml");

		for(final File file : files) {
			npsSpawns.addAll(new DocumentNpsSpawn(file).parse());
		}

		files = FileUtils.getFiles(getItemSpawnFolder(), "xml");

		for(final File file : files) {
			itemSpawns.addAll(new DocumentItemSpawn(file).parse());
		}

		spawns.addAll(itemSpawns);
		spawns.addAll(npsSpawns);

		LOGGER.info("load " + npsSpawns.size() + " nps spawns.");
		LOGGER.info("load " + npsSpawns.size() + " item spawns.");
		LOGGER.info("result load " + spawns.size() + " spawns.");

		final ServerThread thread = new ServerThread(() -> start());
		thread.setName("Spawn Thread");
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();
	}

	private final File getItemSpawnFolder() {

		final String folderName = Config.SERVER_ONLY_PVP ? PVP_SPAWN_FOLDER_NAME : PVE_SPAWN_FOLDER_NAME;

		final PathBuilder builder = new PathBuilder(Config.FOLDER_DATA_PATH);
		builder.append(folderName);
		builder.append(ITEM_FOLDER_NAME);

		return new File(builder.getPath());
	}

	private final File getNpsSpawnFolder() {

		final String folderName = Config.SERVER_ONLY_PVP ? PVP_SPAWN_FOLDER_NAME : PVE_SPAWN_FOLDER_NAME;

		final PathBuilder builder = new PathBuilder(Config.FOLDER_DATA_PATH);
		builder.append(folderName);
		builder.append(NPS_FOLDER_NAME);

		return new File(builder.getPath());
	}

	/**
	 * Запуск всех спавнов.
	 */
	public void start() {

		LOGGER.info("starting spawn objects...");

		final LocalObjects local = LocalObjects.get();

		for(final Spawn spawn : spawns) {
			spawn.start(local);
		}

		LOGGER.info("spawned " + spawns.size() + " objects.");
	}
}

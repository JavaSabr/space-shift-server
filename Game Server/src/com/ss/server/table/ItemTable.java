package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentItem;
import com.ss.server.template.item.ItemTemplate;

/**
 * Таблица шаблонов космических предместов.
 * 
 * @author Ronn
 */
public final class ItemTable {

	private static final Logger LOGGER = LoggerManager.getLogger(ItemTable.class);

	private static ItemTable instance;

	public static ItemTable getInstance() {

		if(instance == null) {
			instance = new ItemTable();
		}

		return instance;
	}

	/** таблица шаблонов */
	private final Table<IntKey, ItemTemplate> table;

	private ItemTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/items"), "xml");

		for(final File file : files) {

			final Array<ItemTemplate> result = new DocumentItem(file).parse();

			for(final ItemTemplate template : result) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate for id " + template.getId());
				}

				table.put(template.getId(), template);
			}
		}

		LOGGER.info("loaded " + table.size() + " items.");
	}

	/**
	 * @param id ид шаблона предмета.
	 * @return искомый шаблон.
	 */
	@SuppressWarnings("unchecked")
	public <T extends ItemTemplate> T getTemplate(final int id) {
		return (T) table.get(id);
	}
}

package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentModule;
import com.ss.server.template.ModuleTemplate;

/**
 * Таблица темплейтов кораблей.
 * 
 * @author Ronn
 */
public final class ModuleTable {

	private static final Logger LOGGER = LoggerManager.getLogger(ModuleTable.class);

	private static ModuleTable instance;

	public static ModuleTable getInstance() {

		if(instance == null) {
			instance = new ModuleTable();
		}

		return instance;
	}

	/** таблица темплейтов */
	private final Table<IntKey, ModuleTemplate> table;

	private ModuleTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/modules"), "xml");

		for(final File file : files) {

			final Array<ModuleTemplate> result = new DocumentModule(file).parse();

			for(final ModuleTemplate template : result) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate for id " + template.getId());
				}

				table.put(template.getId(), template);
			}
		}

		LOGGER.info("loaded " + table.size() + " modules.");
	}

	/**
	 * @param id ид темплейта.
	 * @return темплейт.
	 */
	public <T extends ModuleTemplate> T getTemplate(final Class<T> type, final int id) {
		return type.cast(table.get(id));
	}

	/**
	 * @param id ид темплейта.
	 * @return темплейт.
	 */
	public ModuleTemplate getTemplate(final int id) {
		return table.get(id);
	}
}

package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentGravityObject;
import com.ss.server.template.GravityObjectTemplate;

/**
 * Таблица шаблонов гравитационных объектов.
 * 
 * @author Ronn
 */
public final class GravityObjectTable {

	private static final Logger LOGGER = LoggerManager.getLogger(GravityObjectTable.class);

	private static GravityObjectTable instance;

	public static GravityObjectTable getInstance() {

		if(instance == null) {
			instance = new GravityObjectTable();
		}

		return instance;
	}

	/** таблица шаблонов */
	private final Table<IntKey, GravityObjectTemplate> table;

	private GravityObjectTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/gravity"), "xml");

		for(final File file : files) {

			final Array<GravityObjectTemplate> result = new DocumentGravityObject(file).parse();

			for(final GravityObjectTemplate template : result) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate for id " + template.getId());
				}

				table.put(template.getId(), template);
			}
		}

		LOGGER.info("loaded " + table.size() + " gravity templates.");
	}

	/**
	 * Получение шаблона гравитационного объекта по его ид.
	 * 
	 * @param id ид шаблона.
	 * @return шаблон объекта.
	 */
	public GravityObjectTemplate getTemplate(final int id) {
		return table.get(id);
	}
}

package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentDrop;
import com.ss.server.model.drop.DropList;
import com.ss.server.template.ship.ShipTemplate;

/**
 * Таблица дропа всех NPS.
 * 
 * @author Ronn
 */
public final class DropTable {

	private static final Logger LOGGER = LoggerManager.getLogger(DropTable.class);

	private static DropTable instance;

	public static DropTable getInstance() {

		if(instance == null) {
			instance = new DropTable();
		}

		return instance;
	}

	/** таблица дропа */
	private final Table<IntKey, DropList> table;

	private DropTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/drop"), "xml");

		for(final File file : files) {

			final Table<Integer, DropList> result = new DocumentDrop(file).parse();

		}

		LOGGER.info("loaded " + table.size() + " drop lists.");
	}

	/**
	 * @param id ид шаблона корабля.
	 * @return шаблон корабля.
	 */
	public <T extends ShipTemplate> T getTemplate(final Class<T> type, final int id) {
		return type.cast(table.get(id));
	}

	/**
	 * @param id ид шаблона корабля.
	 * @return шаблон корабля.
	 */
	@SuppressWarnings("unchecked")
	public <T extends ShipTemplate> T getTemplate(final int id) {
		return (T) table.get(id);
	}
}

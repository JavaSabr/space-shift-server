package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentConfigAi;
import com.ss.server.model.ai.nps.ConfigAi;

/**
 * Таблица конфигов Аi Nps.
 * 
 * @author Ronn
 */
public final class ConfigAiTable {

	private static final Logger LOGGER = LoggerManager.getLogger(ConfigAiTable.class);

	public static final String DEFAULT_CONFIG_NAME = "default";

	private static ConfigAiTable instance;

	public static ConfigAiTable getInstance() {

		if(instance == null) {
			instance = new ConfigAiTable();
		}

		return instance;
	}

	/** таблица конфигов */
	private final Table<String, ConfigAi> table;

	private ConfigAiTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newObjectTable();

		for(final File file : FileUtils.getFiles(new File(Config.FOLDER_DATA_PATH, "config_ai"), "xml")) {

			final Array<ConfigAi> result = new DocumentConfigAi(file).parse();

			for(final ConfigAi config : result) {

				if(table.containsKey(config.getName())) {
					LOGGER.warning(new Exception("found duplicate config " + config.getName()));
					continue;
				}

				table.put(config.getName(), config);
			}
		}

		LOGGER.info("loaded " + table.size() + " npc ai configs.");
	}

	/**
	 * Получение конфига Ai Nps по его имени.
	 * 
	 * @param name название конфига.
	 * @return конфиг.
	 */
	public ConfigAi getConfig(final String name) {
		return table.get(name);
	}
}

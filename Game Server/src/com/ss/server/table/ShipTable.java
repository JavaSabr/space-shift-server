package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentNps;
import com.ss.server.document.DocumentPlayerShip;
import com.ss.server.template.ship.ShipTemplate;

/**
 * Таблица темплейтов кораблей.
 * 
 * @author Ronn
 */
public final class ShipTable {

	private static final Logger LOGGER = LoggerManager.getLogger(ShipTable.class);

	private static ShipTable instance;

	public static ShipTable getInstance() {

		if(instance == null) {
			instance = new ShipTable();
		}

		return instance;
	}

	/** таблица темплейтов */
	private final Table<IntKey, ShipTemplate> table;

	private ShipTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/ships/player"), "xml");

		for(final File file : files) {

			final Array<ShipTemplate> result = new DocumentPlayerShip(file).parse();

			for(final ShipTemplate template : result) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate for id " + template.getId());
				}

				table.put(template.getId(), template);
			}
		}

		files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/ships/nps"), "xml");

		for(final File file : files) {

			final Array<ShipTemplate> result = new DocumentNps(file).parse();

			for(final ShipTemplate template : result) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate for id " + template.getId());
				}

				table.put(template.getId(), template);
			}
		}

		LOGGER.info("loaded " + table.size() + " templates.");
	}

	/**
	 * @param id ид шаблона корабля.
	 * @return шаблон корабля.
	 */
	public <T extends ShipTemplate> T getTemplate(final Class<T> type, final int id) {
		return type.cast(table.get(id));
	}

	/**
	 * @param id ид шаблона корабля.
	 * @return шаблон корабля.
	 */
	@SuppressWarnings("unchecked")
	public <T extends ShipTemplate> T getTemplate(final int id) {
		return (T) table.get(id);
	}
}

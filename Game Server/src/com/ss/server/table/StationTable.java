package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentStation;
import com.ss.server.template.StationTemplate;

/**
 * Таблица шаблонов космических станций.
 * 
 * @author Ronn
 */
public final class StationTable {

	private static final Logger LOGGER = LoggerManager.getLogger(StationTable.class);

	private static StationTable instance;

	public static StationTable getInstance() {

		if(instance == null) {
			instance = new StationTable();
		}

		return instance;
	}

	/** таблица шаблонов станций */
	private final Table<IntKey, StationTemplate> table;

	private StationTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_DATA_PATH, "stations"), "xml");

		for(final File file : files) {

			final Array<StationTemplate> result = new DocumentStation(file).parse();

			for(final StationTemplate template : result) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate for id " + template.getId());
				}

				table.put(template.getId(), template);
			}
		}

		LOGGER.info("loaded " + table.size() + " templates.");
	}

	/**
	 * @param id ид темплейта.
	 * @return темплейт.
	 */
	public StationTemplate getTemplate(final int id) {
		return table.get(id);
	}
}

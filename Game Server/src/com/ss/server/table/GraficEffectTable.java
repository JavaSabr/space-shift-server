package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentGraficTemplate;
import com.ss.server.template.graphic.effect.GraphicEffectTemplate;

/**
 * Таблица графических эффектов.
 * 
 * @author Ronn
 */
public class GraficEffectTable {

	private static final Logger LOGGER = LoggerManager.getLogger(GraphicEffectTemplate.class);

	private static GraficEffectTable instance;

	public static GraficEffectTable getInstance() {

		if(instance == null) {
			instance = new GraficEffectTable();
		}

		return instance;
	}

	/** таблица шаблонов графических эффектов */
	private final Table<IntKey, GraphicEffectTemplate> table;

	public GraficEffectTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/effects"), "xml");

		for(final File file : files) {

			final Array<GraphicEffectTemplate> templates = new DocumentGraficTemplate(file).parse();

			for(final GraphicEffectTemplate template : templates) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate template for " + template.getId() + " in file " + file);
				}

				table.put(template.getId(), template);
			}
		}

		LOGGER.info("load " + table.size() + " effect templates.");
	}

	/**
	 * Получение шаблона эффекта.
	 * 
	 * @param id ид шаблона.
	 * @return шаблон эффекта.
	 */
	public GraphicEffectTemplate getTemplate(final int id) {
		return table.get(id);
	}
}

package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.Util;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentItemShopPriceList;
import com.ss.server.model.shop.ItemPriceList;

/**
 * Таблица листов цен на предметы.
 * 
 * @author Ronn
 */
public final class ItemShopPriceTable {

	private static final Logger LOGGER = LoggerManager.getLogger(ItemShopPriceTable.class);

	private static ItemShopPriceTable instance;

	public static ItemShopPriceTable getInstance() {

		if(instance == null) {
			instance = new ItemShopPriceTable();
		}

		return instance;
	}

	/** таблица шаблонов */
	private final Table<IntKey, ItemPriceList> table;

	private ItemShopPriceTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		Util.safeExecute(() -> {

			final File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/item_shop_price"), "xml");

			for(final File file : files) {
				for(final ItemPriceList price : new DocumentItemShopPriceList(file).parse()) {

					if(table.containsKey(price.getId())) {
						LOGGER.warning("found duplicate for id " + price.getId());
					}

					table.put(price.getId(), price);
				}
			}
		});

		LOGGER.info("loaded " + table.size() + " price lists.");
	}

	/**
	 * Получение по ид списка цен на предметы.
	 * 
	 * @param id ид списка.
	 * @return список цен на предметы.
	 */
	public ItemPriceList getPriceList(final int id) {
		return table.get(id);
	}
}

package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentLocationObject;
import com.ss.server.template.LocationObjectTemplate;

/**
 * Таблица шаблонов локационных объектов.
 * 
 * @author Ronn
 */
public final class LocationObjectTable {

	private static final Logger LOGGER = LoggerManager.getLogger(LocationObjectTable.class);

	public static final String LOCATION_OBJECTS_FOLDER = "location_objects";

	private static LocationObjectTable instance;

	public static LocationObjectTable getInstance() {

		if(instance == null) {
			instance = new LocationObjectTable();
		}

		return instance;
	}

	/** таблица шаблонов */
	private final Table<IntKey, LocationObjectTemplate> table;

	private LocationObjectTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		final File[] files = FileUtils.getFiles(new File(Config.FOLDER_DATA_PATH, LOCATION_OBJECTS_FOLDER), "xml");

		for(final File file : files) {

			final Array<LocationObjectTemplate> result = new DocumentLocationObject(file).parse();

			for(final LocationObjectTemplate template : result) {

				if(table.containsKey(template.getId())) {
					LOGGER.warning("found duplicate for id " + template.getId());
				}

				table.put(template.getId(), template);
			}
		}

		LOGGER.info("loaded " + table.size() + " location object templates.");
	}

	/**
	 * Получение шаблона локационного объекта по его ид.
	 * 
	 * @param id ид шаблона.
	 * @return шаблон объекта.
	 */
	public LocationObjectTemplate getTemplate(final int id) {
		return table.get(id);
	}
}

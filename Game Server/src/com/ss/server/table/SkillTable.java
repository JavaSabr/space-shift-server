package com.ss.server.table;

import java.io.File;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.Util;
import rlib.util.array.Array;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.server.Config;
import com.ss.server.document.DocumentSkill;
import com.ss.server.model.skills.Skill;
import com.ss.server.template.SkillTemplate;

/**
 * Таблица скилов модулей кораблей.
 * 
 * @author Ronn
 */
public final class SkillTable {

	private static final Logger LOGGER = LoggerManager.getLogger(SkillTable.class);

	private static SkillTable instance;

	/**
	 * Создание массива скилов.
	 * 
	 * @param temps массив темплейтов скилов.
	 * @return массив скилов.
	 */
	public static final Skill[] createSkills(final SkillTemplate[] temps) {

		final Skill[] skills = new Skill[temps.length];

		for(int i = 0, length = temps.length; i < length; i++) {
			skills[i] = temps[i].newInstance();
		}

		return skills;
	}

	public static SkillTable getInstance() {

		if(instance == null) {
			instance = new SkillTable();
		}

		return instance;
	}

	/** таблица темплейтов */
	private final Table<IntKey, SkillTemplate> table;

	private SkillTable() {
		InitializeManager.valid(getClass());

		table = TableFactory.newIntegerTable();

		Util.safeExecute(() -> {

			final File[] files = FileUtils.getFiles(new File(Config.FOLDER_PROJECT_PATH + "/data/skills"), "xml");

			for(final File file : files) {

				final Array<SkillTemplate> result = new DocumentSkill(file).parse();

				for(final SkillTemplate template : result) {

					if(table.containsKey(template.getId())) {
						LOGGER.warning("found duplicate for id " + template.getId());
					}

					table.put(template.getId(), template);
				}
			}

		});

		LOGGER.info("loaded " + table.size() + " skills.");
	}

	/**
	 * @param templateId ид темплейта скила.
	 * @return темплейт скила.
	 */
	public final SkillTemplate getTemplate(final int templateId) {
		return table.get(templateId);
	}
}

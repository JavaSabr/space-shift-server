package com.ss.server.executor;

import rlib.concurrent.executor.PeriodicTaskExecutor;
import rlib.concurrent.task.PeriodicTask;

import com.ss.server.LocalObjects;

/**
 * Интерфейс для реализации исполнителя игровых задач.
 * 
 * @author Ronn
 */
public interface GameExecutor<T extends PeriodicTask<LocalObjects>> extends PeriodicTaskExecutor<T, LocalObjects> {
}

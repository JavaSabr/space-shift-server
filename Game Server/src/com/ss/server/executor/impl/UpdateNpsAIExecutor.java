package com.ss.server.executor.impl;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.task.impl.UpdateNpsAITask;

/**
 * Реализация исполнителя обновлений состояний AI NPS.
 * 
 * @author Ronn
 */
public class UpdateNpsAIExecutor extends AbstractGameExecutor<UpdateNpsAITask> {

	public static final String EXECUTOR_NAME = UpdateNpsAIExecutor.class.getSimpleName();

	public UpdateNpsAIExecutor(final String postfix) {
		super(UpdateNpsAITask.class, Thread.MIN_PRIORITY, 100, EXECUTOR_NAME + "_" + postfix);
	}

	@Override
	protected void executeImpl(final Array<UpdateNpsAITask> executeTasks, final Array<UpdateNpsAITask> finishedTasks, final LocalObjects local, final long startExecuteTime) {

		long currentTime = startExecuteTime;

		int updated = 0;

		for(final UpdateNpsAITask task : executeTasks.array()) {

			if(task == null) {
				break;
			}

			if(updated > 50) {
				currentTime = System.currentTimeMillis();
				updated = 0;
			}

			task.call(local, currentTime);

			updated++;
		}
	}
}

package com.ss.server.executor.impl;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.task.impl.UpdateSpaceObjectTask;

/**
 * Реализация исполнителя обновлений космических объектов.
 * 
 * @author Ronn
 */
public class UpdateObjectExecutor extends AbstractGameExecutor<UpdateSpaceObjectTask> {

	public static final String EXECUTOR_NAME = UpdateObjectExecutor.class.getSimpleName();

	public UpdateObjectExecutor(final String postfix) {
		super(UpdateSpaceObjectTask.class, Thread.NORM_PRIORITY - 3, 100, EXECUTOR_NAME + "_" + postfix);
	}

	@Override
	protected void executeImpl(final Array<UpdateSpaceObjectTask> executeTasks, final Array<UpdateSpaceObjectTask> finishedTasks, final LocalObjects local, final long startExecuteTime) {

		long currentTime = startExecuteTime;

		int updated = 0;

		for(final UpdateSpaceObjectTask task : executeTasks.array()) {

			if(task == null) {
				break;
			}

			if(updated > 50) {
				currentTime = System.currentTimeMillis();
				updated = 0;
			}

			task.call(local, currentTime);

			updated++;
		}
	}
}

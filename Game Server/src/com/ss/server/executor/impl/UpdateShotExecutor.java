package com.ss.server.executor.impl;

import rlib.util.array.Array;

import com.ss.server.LocalObjects;
import com.ss.server.task.impl.UpdateShotTask;

/**
 * Реализация исполнителя обновления выстрелов.
 * 
 * @author Ronn
 */
public class UpdateShotExecutor extends AbstractGameExecutor<UpdateShotTask> {

	public static final String EXECUTOR_NAME = UpdateShotExecutor.class.getSimpleName();

	public UpdateShotExecutor(final String postfix) {
		super(UpdateShotTask.class, Thread.NORM_PRIORITY - 2, 100, EXECUTOR_NAME + "_" + postfix);
	}

	@Override
	protected void executeImpl(final Array<UpdateShotTask> executeTasks, final Array<UpdateShotTask> finishedTasks, final LocalObjects local, final long startExecuteTime) {

		long currentTime = startExecuteTime;

		int updated = 0;

		for(final UpdateShotTask task : executeTasks.array()) {

			if(task == null) {
				break;
			}

			if(updated > 50) {
				currentTime = System.currentTimeMillis();
				updated = 0;
			}

			if(task.call(local, currentTime) == Boolean.TRUE) {
				finishedTasks.add(task);
			}

			updated++;
		}
	}
}

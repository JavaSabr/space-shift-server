package com.ss.server.task.ship;

import com.ss.server.task.UpdateTask;

/**
 * Интерфейс для реализации задач связанных с космическими кораблями.
 * 
 * @author Ronn
 */
public interface SpaceShipTask extends UpdateTask {

}

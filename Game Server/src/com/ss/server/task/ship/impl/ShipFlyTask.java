package com.ss.server.task.ship.impl;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;

import rlib.concurrent.lock.LockFactory;
import rlib.geom.Vector;
import rlib.util.Synchronized;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.manager.CollisionManager;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.func.stat.StatType;
import com.ss.server.model.impl.Env;
import com.ss.server.model.impl.SpaceLocation;
import com.ss.server.model.impl.SpaceSector;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.packet.server.ResponseDebugObjectPosition;
import com.ss.server.network.game.packet.server.ResponseShipFly;
import com.ss.server.task.ship.SpaceShipTask;

/**
 * Модель полета корабля.
 * 
 * @author Ronn
 */
public class ShipFlyTask implements SpaceShipTask, Synchronized {

	public static final float MINIMUM_ACCEL = 0.01F;

	public static final int SHIP_SYNC_INTERVAL = 3000;
	public static final int COLLISION_INTERVAL = 5000;

	private static final CollisionManager COLLISION_MANAGER = CollisionManager.getInstance();

	private static final int DEFAULT_ACCEL = -20;

	/** флаг активности */
	private final AtomicBoolean running;
	/** корабль */
	private final SpaceShip spaceShip;
	/** вектор изменения положения */
	private final Vector changed;
	/** блокировщик */
	private final Lock lock;

	/** последняя скорость */
	private volatile float lastSpeed;
	/** последнее ускорение */
	private volatile float lastAccel;

	/** время последнего обновления */
	private volatile long lastUpdate;
	/** время последней общей синхронизации */
	private volatile long lastSync;
	/** время последнего обновления колизии */
	private volatile long lastCollisionUpdate;

	public ShipFlyTask(final SpaceShip spaceShip) {
		this.spaceShip = spaceShip;
		this.changed = Vector.newInstance();
		this.running = new AtomicBoolean();
		this.lock = LockFactory.newPrimitiveAtomicLock();
	}

	/**
	 * @return вектор изменения положения.
	 */
	public Vector getChanged() {
		return changed;
	}

	/**
	 * @return последнее ускорение.
	 */
	public final float getLastAccel() {
		return lastAccel;
	}

	/**
	 * @return время последнего обновления колизии.
	 */
	public long getLastCollisionUpdate() {
		return lastCollisionUpdate;
	}

	/**
	 * @return последняя рассчетная скорость.
	 */
	public final float getLastSpeed() {
		return lastSpeed;
	}

	/**
	 * @return время последней общей синхронизации.
	 */
	public long getLastSync() {
		return lastSync;
	}

	/**
	 * @return время последнего обновления.
	 */
	public final long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @return летающий корабль.
	 */
	public SpaceShip getSpaceShip() {
		return spaceShip;
	}

	/**
	 * Прервать текущий полет,
	 */
	public void interrupt() {
		setLastAccel(0);
		setLastSpeed(0);
	}

	/**
	 * @return запущена ли задача полета.
	 */
	public boolean isRunning() {
		return running.get();
	}

	@Override
	public void lock() {
		lock.lock();
	}

	/**
	 * Обработка колизиии корабля с объектами.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 * @param ship синхронизирующий корабль.
	 */
	protected void processCollision(final LocalObjects local, final long currentTime, final SpaceShip ship) {

		final SpaceSector sector = ship.getCurrentSector();

		if(sector != null && currentTime - getLastCollisionUpdate() > COLLISION_INTERVAL) {

			final SpaceLocation location = sector.getLocation();
			location.updateCollision(ship, local, COLLISION_MANAGER);

			setLastCollisionUpdate(currentTime);
		}
	}

	/**
	 * обработка синхронизации объекта.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 * @param ship синхронизирующий корабль.
	 * @param needSync надо ли синхронизироваться.
	 * @param accel ускорение корабля.
	 */
	protected void processSync(final LocalObjects local, final long currentTime, final SpaceShip ship, final boolean needSync, final float accel) {

		if(needSync || accel > MINIMUM_ACCEL && currentTime - getLastSync() > SHIP_SYNC_INTERVAL) {

			// синхронизируем
			ship.sync(local, currentTime);

			// рандоминизируем следующую синхронизацию
			final ThreadLocalRandom current = ThreadLocalRandom.current();
			setLastSync(currentTime + current.nextInt(SHIP_SYNC_INTERVAL));
		}
	}

	/**
	 * Реинициализация.
	 */
	public void reinit() {

		final long currentTime = System.currentTimeMillis();

		setRunning(true);
		setLastUpdate(currentTime);
		setLastSync(currentTime);
	}

	/**
	 * Отправка пакета.
	 */
	public void sendPacket(final float accel, final float speed, final float maxSpeed, final float maxCurrentSpeed, final boolean active, final LocalObjects local) {

		final SpaceShip ship = getSpaceShip();

		if(ship.isNeedSendPacket()) {
			ship.broadcastPacket(ResponseShipFly.getInstance(ship, accel, speed, maxSpeed, maxCurrentSpeed, active, false, local));
		}
	}

	/**
	 * @param lastAccel последнее ускорение.
	 */
	public final void setLastAccel(final float lastAccel) {
		this.lastAccel = lastAccel;
	}

	/**
	 * @param lastCollisionUpdate время последнего обновления колизии.
	 */
	public void setLastCollisionUpdate(final long lastCollisionUpdate) {
		this.lastCollisionUpdate = lastCollisionUpdate;
	}

	/**
	 * @param lastSpeed последняя рассчетная скорость.
	 */
	public final void setLastSpeed(final float lastSpeed) {
		this.lastSpeed = Math.max(lastSpeed, 0);
	}

	/**
	 * @param lastSync время последней общей синхронизации.
	 */
	public void setLastSync(final long lastSync) {
		this.lastSync = lastSync;
	}

	/**
	 * @param lastUpdate время последнего обновления.
	 */
	public final void setLastUpdate(final long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @param running запущена ли задача полета.
	 */
	public void setRunning(final boolean running) {
		this.running.getAndSet(running);
	}

	/**
	 * Остановить полет.
	 */
	public void stop() {
		lock();
		try {
			setRunning(false);
			setLastAccel(0);
			setLastSpeed(0);
		} finally {
			unlock();
		}
	}

	/**
	 * Отправка пакета для синхронизации положения корабля.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param currentTime текущее время.
	 */
	public void sync(final LocalObjects local, final long currentTime) {

		final SpaceShip ship = getSpaceShip();

		final Env env = local.getNextEnv();
		env.setOwner(ship);

		final float accel = ship.calcStat(StatType.FLY_ACCEL, DEFAULT_ACCEL, env);
		final float maxSpeed = ship.calcStat(StatType.MAX_FLY_SPEED, 0, env);
		final float maxCurrentSpeed = ship.calcStat(StatType.MAX_CURRENT_FLY_SPEED, 0, env);

		setLastSync(currentTime);
		sendPacket(accel, getLastSpeed(), maxSpeed, maxCurrentSpeed, accel > 0, local);
	}

	/**
	 * Отправка сведений о полете корабля для указанного объекта.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param object объект, запрашивающий сведения.
	 */
	public void syncFor(final LocalObjects local, final SpaceObject object) {

		final SpaceShip ship = getSpaceShip();
		final Env env = local.getNextEnv();

		final float accel = ship.calcStat(StatType.FLY_ACCEL, DEFAULT_ACCEL, env);
		final float maxSpeed = ship.calcStat(StatType.MAX_FLY_SPEED, 0, env);
		final float maxCurrentSpeed = ship.calcStat(StatType.MAX_CURRENT_FLY_SPEED, 0, env);

		final ResponseShipFly packet = ResponseShipFly.getInstance(ship, accel, getLastSpeed(), maxSpeed, maxCurrentSpeed, accel > 0, true, local);
		object.sendPacket(packet, true);
	}

	@Override
	public void unlock() {
		lock.unlock();
	}

	@Override
	public void update(final LocalObjects local, final long currentTime) {

		final SpaceShip ship = getSpaceShip();

		boolean needSync = false;

		float lastSpeed = getLastSpeed();
		float accel = 0;

		try {

			if(!isRunning()) {
				return;
			}

			final Env env = local.getNextEnv();

			accel = ship.calcStat(StatType.FLY_ACCEL, DEFAULT_ACCEL, env);

			if(accel < 0.01F && lastSpeed < 1) {
				needSync = getLastAccel() != accel;
				setLastAccel(accel);
				setLastSync(currentTime);
				return;
			}

			needSync = lastSpeed < 1;

			final float maxSpeed = ship.calcStat(StatType.MAX_FLY_SPEED, 0, env);
			final float maxCurrentSpeed = ship.calcStat(StatType.MAX_CURRENT_FLY_SPEED, 0, env);

			if(getLastAccel() != accel) {
				setLastAccel(accel);
				needSync = true;
			}

			if(maxCurrentSpeed < lastSpeed) {
				accel = DEFAULT_ACCEL;
			}

			final long lastUpdate = getLastUpdate();
			final long diff = (int) (currentTime - lastUpdate);

			final float oldSpeed = lastSpeed;
			final float newSpeed = Math.min(lastSpeed + diff / 1000F * accel, maxSpeed);

			if(oldSpeed <= maxCurrentSpeed && newSpeed > maxCurrentSpeed) {
				lastSpeed = maxCurrentSpeed;
			} else {
				lastSpeed = newSpeed;
			}

			final float dist = diff * lastSpeed / 1000F;
			final Vector direction = local.getNextVector();

			direction.set(ship.getDirection());
			direction.multLocal(dist);
			direction.addLocal(ship.getLocation());

			final Vector changed = getChanged();
			changed.set(direction).subtractLocal(ship.getLocation());

			if(!isRunning()) {
				return;
			}

			lock();
			try {
				ship.setLocation(direction, changed, local);
			} finally {
				unlock();
			}

			setLastCollisionUpdate(0);

			if(Config.DEVELOPER_DEBUG_SYNCHRONIZE && ship.isNeedSendPacket()) {
				ship.broadcastPacket(ResponseDebugObjectPosition.getInstance(ship, ship.getLocation()));
			}

			needSync = needSync || lastSpeed < 1;

		} finally {

			setLastUpdate(currentTime);
			setLastSpeed(lastSpeed);

			processSync(local, currentTime, ship, needSync, accel);
			processCollision(local, currentTime, ship);
		}
	}
}

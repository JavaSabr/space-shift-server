package com.ss.server.task.ship.impl;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.task.ship.SpaceShipTask;

/**
 * Реализация задачи по процессу регенерации корабля.
 * 
 * @author Ronn
 */
public class ShipRegenTask implements SpaceShipTask {

	public static final int REGENERATION_INTERVAL = 1000;

	/** регенирирующий корабль */
	private final SpaceShip ship;

	/** время последнего обновления регенерации */
	private long lastUpdate;

	public ShipRegenTask(final SpaceShip ship) {
		this.ship = ship;
	}

	/**
	 * @return время последнего обновления регенерации.
	 */
	public long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @return регенирирующий корабль.
	 */
	public SpaceShip getShip() {
		return ship;
	}

	/**
	 * Перезапуск задачи.
	 */
	public void reinit() {
		this.lastUpdate = System.currentTimeMillis();
	}

	/**
	 * @param lastUpdate время последнего обновления регенерации.
	 */
	public void setLastUpdate(final long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public void update(final LocalObjects local, final long currentTime) {

		if(currentTime - getLastUpdate() < REGENERATION_INTERVAL) {
			return;
		}

		final SpaceShip ship = getShip();
		ship.doRegen(local, currentTime);

		setLastUpdate(currentTime);
	}
}

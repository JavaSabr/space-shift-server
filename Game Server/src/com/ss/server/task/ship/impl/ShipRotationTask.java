package com.ss.server.task.ship.impl;

import static com.ss.server.Config.DEVELOPER_DEBUG_SYNCHRONIZE;
import static com.ss.server.model.func.stat.StatType.ROTATE_SPEED;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.System.currentTimeMillis;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;

import rlib.concurrent.lock.LockFactory;
import rlib.geom.Rotation;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.Synchronized;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.network.game.packet.server.ResponseDebugObjectRotation;
import com.ss.server.network.game.packet.server.ResponseShipRotate;
import com.ss.server.task.ship.SpaceShipTask;

/**
 * Реализация задачи по развороту корабля.
 * 
 * @author Ronn
 */
public class ShipRotationTask implements SpaceShipTask, Synchronized {

	private static final Logger LOGGER = LoggerManager.getLogger(ShipRotationTask.class);

	/** флаг активности */
	private final AtomicBoolean running;
	/** поворачиаемый корабль */
	private final SpaceShip spaceShip;

	/** конечный разворот */
	private final Rotation endRotation;
	/** стартовый зраворот */
	private final Rotation startRotation;
	/** итоговый текущий разворот */
	private final Rotation resultRotation;
	/** буфферный разворот */
	private final Rotation bufferRotation;

	/** блокировщик */
	private final Lock lock;

	/** время последнего рассчета */
	private volatile long lastUpdate;

	/** процент выполненности разворота */
	private volatile float done;
	/** размер шага разворота при обновлении */
	private volatile float step;
	/** скорость разворота */
	private volatile float speed;

	/** бесконечный ли разворот */
	private volatile boolean infinity;

	public ShipRotationTask(final SpaceShip spaceShip) {
		this.spaceShip = spaceShip;
		this.endRotation = Rotation.newInstance();
		this.startRotation = Rotation.newInstance();
		this.resultRotation = Rotation.newInstance();
		this.bufferRotation = Rotation.newInstance();
		this.running = new AtomicBoolean();
		this.lock = LockFactory.newPrimitiveAtomicLock();
	}

	/**
	 * @return буферный разворот для рассчета.
	 */
	public Rotation getBufferRotation() {
		return bufferRotation;
	}

	/**
	 * @return процент выполненности разворота.
	 */
	public final float getDone() {
		return done;
	}

	/**
	 * @return конечный разворот.
	 */
	public Rotation getEndRotation() {
		return endRotation;
	}

	/**
	 * @return время последнего обновления.
	 */
	public final long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @return итоговый текущий разворот.
	 */
	public Rotation getResultRotation() {
		return resultRotation;
	}

	/**
	 * @return поворачиаемый корабль.
	 */
	public SpaceShip getSpaceShip() {
		return spaceShip;
	}

	/**
	 * @return скорость разворота.
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * @return стартовый разворот.
	 */
	public Rotation getStartRotation() {
		return startRotation;
	}

	/**
	 * @return step шаг выполнения.
	 */
	public final float getStep() {
		return step;
	}

	/**
	 * @return бесконечный ли разворот.
	 */
	public boolean isInfinity() {
		return infinity;
	}

	/**
	 * @return запущена ли задача разворота.
	 */
	public boolean isRunning() {
		return running.get();
	}

	@Override
	public void lock() {
		lock.lock();
	}

	/**
	 * Реинициализация.
	 */
	public void reinit() {

		final SpaceShip ship = getSpaceShip();
		final Rotation rotation = ship.getRotation();

		setRunning(true);
		setEndRotation(rotation);
		setStartRotation(rotation);
		setLastUpdate(System.currentTimeMillis());
		setDone(1F);
	}

	/**
	 * @param done процент выполнения разворота.
	 */
	public final void setDone(final float done) {
		this.done = done;
	}

	/**
	 * @param rotation конечный разворот.
	 */
	public void setEndRotation(final Rotation rotation) {
		this.endRotation.set(rotation);
	}

	/**
	 * @param infinity бесконечный ли разворот.
	 */
	public void setInfinity(final boolean infinity) {
		this.infinity = infinity;
	}

	/**
	 * @param lastUpdate время последнего выполнения.
	 */
	public final void setLastUpdate(final long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @param running запущена ли задача разворота.
	 */
	public void setRunning(final boolean running) {
		this.running.set(running);
	}

	/**
	 * @param speed скорость разворота.
	 */
	public void setSpeed(final float speed) {
		this.speed = speed;
	}

	/**
	 * @param rotation стартовый разворот.
	 */
	public void setStartRotation(final Rotation rotation) {
		this.startRotation.set(rotation);
	}

	/**
	 * @param step шаг выполнения.
	 */
	public final void setStep(final float step) {
		this.step = step;
	}

	/**
	 * Остановка.
	 */
	public void stop() {

		final SpaceShip ship = getSpaceShip();
		final Rotation rotation = ship.getRotation();

		lock();
		try {

			setRunning(false);
			setEndRotation(rotation);
			setStartRotation(rotation);
			setDone(1F);

		} finally {
			unlock();
		}
	}

	/**
	 * Отправка пакета для синхронизации положения корабля.
	 * 
	 * @param local контейнер локальных объектов.
	 */
	public void sync(final LocalObjects local) {

		final SpaceShip ship = getSpaceShip();

		if(ship.isNeedSendPacket()) {
			final ResponseShipRotate packet = ResponseShipRotate.getInstance(ship, getStartRotation(), getEndRotation(), getStep(), getDone(), isInfinity(), false, local);
			ship.broadcastPacket(packet);
		}
	}

	/**
	 * Отправка пакета для синхронизации положения корабля.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param object объект, которому отправить.
	 */
	public void syncFor(final LocalObjects local, final SpaceObject object) {
		final ResponseShipRotate packet = ResponseShipRotate.getInstance(getSpaceShip(), getStartRotation(), getEndRotation(), getStep(), getDone(), isInfinity(), true, local);
		object.sendPacket(packet, true);
	}

	@Override
	public void unlock() {
		lock.unlock();
	}

	@Override
	public void update(final LocalObjects local, final long currentTime) {

		float done = getDone();

		try {

			if(done >= 1F) {
				return;
			}

			if(!isRunning()) {
				return;
			}

			final SpaceShip ship = getSpaceShip();

			final long diff = currentTime - getLastUpdate();

			final float step = getStep();
			final float added = step * diff / 1000F;

			done += added;

			final Rotation start = getStartRotation();
			final Rotation end = getEndRotation();

			Rotation result = getResultRotation();

			if(done >= 1F) {

				if(!isInfinity()) {
					result = end;
				} else {

					final Rotation buffer = getBufferRotation();
					buffer.set(end);
					buffer.subtractLocal(start);

					start.addLocal(buffer);
					start.normalizeLocal();

					end.addLocal(buffer);
					end.normalizeLocal();

					done = Math.min(0.99F, done -= 1F);

					if(Float.isNaN(done)) {
						LOGGER.error(this, new Exception("detect incorrect donce " + done));
					}

					if(Float.isNaN(end.getX())) {
						LOGGER.error(this, "incorrect end rotation from cacl next end: " + end + ", start " + start + ", buffer " + buffer);
					}
				}
			}

			if(result != end) {

				final Rotation buffer = getBufferRotation();
				buffer.set(end);

				result = result.slerp(start, buffer, done);

				if(Float.isNaN(result.getX())) {
					LOGGER.error(this, "incorrect result rotation from slerp: " + result + ", start " + start + ", end " + end + ", done = " + done);
				}
			}

			lock();
			try {

				if(!isRunning()) {
					return;
				}

				ship.setRotation(result, local);

			} finally {
				unlock();
			}

			if(DEVELOPER_DEBUG_SYNCHRONIZE && ship.isNeedSendPacket()) {
				ship.broadcastPacket(ResponseDebugObjectRotation.getInstance(ship, ship.getRotation()));
			}

		} finally {
			setLastUpdate(currentTime);
			setDone(done);
		}
	}

	/**
	 * Обновление целевого направления.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param start стартовый разворот.
	 * @param target новое конечное направление.
	 * @param infinity бесконечный ли разворот.
	 */
	public void update(final LocalObjects local, final Rotation start, final Rotation target, final boolean infinity) {
		update(local, start, target, infinity, 1F);
	}

	/**
	 * Обновление целевого направления.
	 * 
	 * @param local контейнер локальных объектов.
	 * @param start стартовый разворот.
	 * @param target новое конечное направление.
	 * @param infinity бесконечный ли разворот.
	 */
	public void update(final LocalObjects local, final Rotation start, final Rotation target, final boolean infinity, final float modiff) {

		final SpaceShip ship = getSpaceShip();

		float speed = ship.calcStat(ROTATE_SPEED, 0, local.getNextEnv());
		speed *= max(min(modiff, 1F), 0F);

		if(speed <= 0) {
			return;
		}

		lock();
		try {

			final float dot = Math.abs(start.dot(target));

			if(ship.isPlayerShip() && dot > 0.999F) {
				setDone(1F);
				setInfinity(false);
				return;
			} else if(ship.isNps() && dot > 1F) {
				setDone(1F);
				setInfinity(false);
				return;
			}

			final float radians = (float) Math.acos(dot);

			final float count = radians / speed;
			final float step = 1 / count;

			if(Float.isInfinite(step) || Float.isNaN(step)) {
				setDone(1F);
				setInfinity(false);
				return;
			}

			final long currentTime = currentTimeMillis();

			setStartRotation(start);
			setEndRotation(target);
			setSpeed(speed);
			setDone(0);
			setStep(step);
			setInfinity(infinity);
			setLastUpdate(currentTime);
			sync(local);

		} finally {
			unlock();
		}
	}
}

package com.ss.server.task;

import com.ss.server.LocalObjects;

/**
 * Интерфейс для реализации задач обновления объектов.
 * 
 * @author Ronn
 */
public interface UpdateTask {

	public void update(LocalObjects local, long currentTime);
}

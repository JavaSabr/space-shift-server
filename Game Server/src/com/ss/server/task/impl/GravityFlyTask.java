package com.ss.server.task.impl;

import rlib.geom.Rotation;
import rlib.geom.Vector;

import com.ss.server.Config;
import com.ss.server.LocalObjects;
import com.ss.server.model.gravity.GravityObject;
import com.ss.server.network.game.packet.server.ResponseDebugObjectPosition;
import com.ss.server.task.UpdateTask;

/**
 * Модель обработки гравитации объекта.
 * 
 * @author Ronn
 */
public final class GravityFlyTask implements UpdateTask {

	private static final int UPDATE_INTERVAL = 30000;

	/** гравитационный объект */
	private final GravityObject object;

	/** наклон орбиты */
	private final Rotation rotation;

	/** вектор разворота */
	private final Vector rotateVector;
	/** вектор изменения положения */
	private final Vector changed;

	/** время последнего действия */
	private long lastTime;

	/** выполненый градус оборота */
	private double done;

	public GravityFlyTask(final GravityObject object) {
		this.object = object;
		this.rotation = object.getOrbitalRotation();
		this.rotateVector = Vector.newInstance();
		this.changed = Vector.newInstance();
	}

	/**
	 * @return вектор изменения положения.
	 */
	public Vector getChanged() {
		return changed;
	}

	/**
	 * @return выполнено.
	 */
	public double getDone() {
		return done;
	}

	/**
	 * @param lastTime время последней обработки.
	 */
	public long getLastTime() {
		return lastTime;
	}

	/**
	 * @return гравитационных объект.
	 */
	public GravityObject getObject() {
		return object;
	}

	/**
	 * @return вектор наклона обриты.
	 */
	public Vector getRotateVector() {
		return rotateVector;
	}

	/**
	 * @param done степень выполненности.
	 */
	public void setDone(final double done) {
		this.done = done;
	}

	/**
	 * @param lastTime время последней обработки.
	 */
	public void setLastTime(final long lastTime) {
		this.lastTime = lastTime;
	}

	@Override
	public void update(final LocalObjects local, final long currentTime) {
		double done = getDone();

		try {

			final GravityObject object = getObject();

			// если нет времени оборота, не обрабатываем
			if(object.getTurnAroundTime() < 1) {
				return;
			}

			final GravityObject parent = object.getParent();

			if(parent == null) {
				return;
			}

			final long lastTime = getLastTime();
			final long turnAroundTime = object.getTurnAroundTime();
			final long diff = currentTime - lastTime;

			done += 360D * diff / turnAroundTime;

			final double radians = done * Math.PI / 180;

			final Vector center = parent.getLocation();

			final int distance = object.getDistance();

			final float newX = (float) (0 + distance * Math.cos(radians));
			final float newZ = (float) (0 + distance * Math.sin(radians));

			final Vector rotateVector = getRotateVector();
			rotateVector.setXYZ(newX, 0, newZ);

			rotation.multLocal(rotateVector);

			rotateVector.addLocal(center);

			final Vector changed = getChanged();
			changed.set(rotateVector).subtractLocal(object.getLocation());

			object.setLocation(rotateVector, changed, local);

			if(Config.DEVELOPER_DEBUG_SYNCHRONIZE && object.isNeedSendPacket()) {
				object.broadcastPacket(ResponseDebugObjectPosition.getInstance(object, rotateVector));
			}

		} finally {
			setLastTime(currentTime);
			setDone(done);
		}
	}
}

package com.ss.server.task.impl;

import java.util.concurrent.Callable;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.server.LocalObjects;
import com.ss.server.model.ship.SpaceShip;
import com.ss.server.model.skills.TimeActiveSkill;

/**
 * Реализация задачи по авто завершению временно активного умения.
 * 
 * @author Ronn
 */
public class FinishActiveSkillTask implements Foldable, Callable<Void> {

	public static FinishActiveSkillTask newTask(final SpaceShip ship, final TimeActiveSkill skill) {

		FinishActiveSkillTask task = POOL.take();

		if(task == null) {
			task = new FinishActiveSkillTask();
		}

		task.skill = skill;
		task.ship = ship;

		return task;
	}

	private static final FoldablePool<FinishActiveSkillTask> POOL = PoolFactory.newAtomicFoldablePool(FinishActiveSkillTask.class);

	/** активировавший корабль */
	private SpaceShip ship;
	/** деактивируемое умение */
	private TimeActiveSkill skill;

	@Override
	public Void call() throws Exception {

		final TimeActiveSkill skill = getSkill();

		if(skill != null) {
			skill.timeFinish(LocalObjects.get());
		}

		POOL.put(this);
		return null;
	}

	@Override
	public void finalyze() {
		this.skill = null;
	}

	/**
	 * @return активировавший корабль.
	 */
	public SpaceShip getShip() {
		return ship;
	}

	/**
	 * @return деактивируемое умение.
	 */
	public TimeActiveSkill getSkill() {
		return skill;
	}
}

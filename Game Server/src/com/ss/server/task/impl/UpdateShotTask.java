package com.ss.server.task.impl;

import rlib.concurrent.task.PeriodicTask;

import com.ss.server.LocalObjects;
import com.ss.server.model.shot.Shot;

/**
 * Реализация задачи по обновлению Shot.
 * 
 * @author Ronn
 */
public class UpdateShotTask implements PeriodicTask<LocalObjects> {

	/** обновляемый выстрел */
	private final Shot shot;

	public UpdateShotTask(final Shot shot) {
		this.shot = shot;
	}

	@Override
	public void onFinish(final LocalObjects local) {
		shot.doFinish(local);
	}

	@Override
	public boolean update(final LocalObjects local, final long currentTime) {
		return shot.doUpdate(currentTime, local);
	}
}

package com.ss.server.task.impl;

import rlib.concurrent.task.PeriodicTask;

import com.ss.server.LocalObjects;
import com.ss.server.model.SpaceObject;

/**
 * Реализация задачи по обновлению SpaceObject.
 * 
 * @author Ronn
 */
public class UpdateSpaceObjectTask implements PeriodicTask<LocalObjects> {

	/** обновляемый объект */
	private final SpaceObject object;

	public UpdateSpaceObjectTask(final SpaceObject object) {
		this.object = object;
	}

	@Override
	public boolean update(final LocalObjects local, final long currentTime) {
		object.doUpdate(local, currentTime);
		return false;
	}
}

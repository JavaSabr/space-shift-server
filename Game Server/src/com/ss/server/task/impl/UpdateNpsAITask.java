package com.ss.server.task.impl;

import rlib.concurrent.task.PeriodicTask;

import com.ss.server.LocalObjects;
import com.ss.server.model.ai.nps.NpsAI;

/**
 * Реализация задачи по обновлению AI NPS.
 * 
 * @author Ronn
 */
public class UpdateNpsAITask implements PeriodicTask<LocalObjects> {

	/** обновляемый AI */
	private final NpsAI ai;

	/** время следующего обновления */
	private long next;

	/** интервал обновления */
	private int interval;

	public UpdateNpsAITask(final NpsAI ai) {
		this.ai = ai;
	}

	/**
	 * @return обновляемый AI.
	 */
	public NpsAI getAi() {
		return ai;
	}

	/**
	 * @return интервал обновления.
	 */
	public int getInterval() {
		return interval;
	}

	/**
	 * @return время следующего обновления.
	 */
	public long getNext() {
		return next;
	}

	/**
	 * @param interval интервал обновления.
	 */
	public void setInterval(final int interval) {
		this.next = 0;
		this.interval = interval;
	}

	/**
	 * @param next время следующего обновления.
	 */
	public void setNext(final long next) {
		this.next = next;
	}

	@Override
	public boolean update(final LocalObjects local, final long currentTime) {

		final NpsAI ai = getAi();

		if(false || !ai.isEnabled()) {
			return false;
		}

		if(getNext() > currentTime) {
			return false;
		}

		try {
			ai.doUpdate(currentTime, local);
		} finally {
			setNext(currentTime + getInterval());
		}

		return false;
	}
}

package com.ss.server.client.command.impl;

import com.ss.server.LocalObjects;
import com.ss.server.model.rating.RatingSystem;
import com.ss.server.model.rating.event.RatingEvents;
import com.ss.server.model.rating.event.local.destroy.DestroyRatingEvent;
import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.model.GameClient;

/**
 * Реализация обработчика команды на уничтожения корабля из-за коллизии.
 * 
 * @author Ronn
 */
public class CollissionDestroyCommandHandler extends AbstractClientCommandHandler {

	@Override
	public void execute(GameClient client, PlayerShip playerShip) {

		if(playerShip == null || playerShip.isDestructed()) {
			return;
		}

		playerShip.deleteLock();
		try {

			if(playerShip.isDeleted()) {
				return;
			}

			final LocalObjects local = LocalObjects.get();
			playerShip.setCurrentStrength(0, local);
			playerShip.doDestruct(playerShip, local);
			playerShip.interruptFly();
			playerShip.sync(local, System.currentTimeMillis());

			final DestroyRatingEvent event = RatingEvents.getDestroyRatingEvent(local);
			event.setDamageType(null);
			event.setDestroyed(playerShip);
			event.setDestroyer(playerShip);

			final RatingSystem ratingSystem = playerShip.getRatingSystem();
			ratingSystem.notify(event);

		} finally {
			playerShip.deleteUnlock();
		}
	}
}

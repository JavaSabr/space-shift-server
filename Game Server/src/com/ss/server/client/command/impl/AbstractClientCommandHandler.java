package com.ss.server.client.command.impl;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import com.ss.server.client.command.ClientCommandHandler;

/**
 * Базовая реализация обработчика клиентской команды.
 * 
 * @author Ronn
 */
public abstract class AbstractClientCommandHandler implements ClientCommandHandler {

	protected static final Logger LOGGER = LoggerManager.getLogger(ClientCommandHandler.class);
}

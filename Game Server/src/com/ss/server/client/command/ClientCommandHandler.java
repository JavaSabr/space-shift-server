package com.ss.server.client.command;

import com.ss.server.model.ship.player.PlayerShip;
import com.ss.server.network.game.model.GameClient;

/**
 * Интерфейс для реализации обработчика клиентской команды.
 * 
 * @author Ronn
 */
public interface ClientCommandHandler {

	/**
	 * Выполненине команды для указанного клиента и его корабля.
	 * 
	 * @param client запрашиваемый клиент.
	 * @param playerShip корабль клиент.
	 */
	public void execute(GameClient client, PlayerShip playerShip);
}

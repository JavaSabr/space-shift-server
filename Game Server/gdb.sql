SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `gdb` ;
CREATE SCHEMA IF NOT EXISTS `gdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `gdb` ;

-- -----------------------------------------------------
-- Table `gdb`.`player_ships`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ships` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ships` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля.',
  `account_name` VARCHAR(45) NOT NULL COMMENT 'Название аккаунта, за которым закреплен корабль.',
  `pilot_name` VARCHAR(45) NOT NULL COMMENT 'Имя пилота корабля.',
  `template_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Ид шаблона корабля.',
  `create_time` BIGINT UNSIGNED NULL DEFAULT 0 COMMENT 'Дата создания корабля.',
  `online_time` BIGINT UNSIGNED NULL DEFAULT 0 COMMENT 'Время в онлайне корабля.',
  `delete_time` BIGINT UNSIGNED NULL DEFAULT 0 COMMENT 'Дата постановки корабля на удаление.',
  `fraction_id` TINYINT UNSIGNED NULL DEFAULT 0 COMMENT 'Ид фракции корабля.',
  `location_id` SMALLINT UNSIGNED NULL COMMENT 'Ид локации, в которой находится корабль.',
  `loc_x` FLOAT NULL DEFAULT 0 COMMENT 'Координата положения.',
  `loc_y` FLOAT NULL DEFAULT 0,
  `loc_z` FLOAT NULL DEFAULT 0 COMMENT 'Координата положения.',
  `q_x` FLOAT NULL DEFAULT 0 COMMENT 'Направление разворота корабля.',
  `q_y` FLOAT NULL DEFAULT 0 COMMENT 'Направление разворота корабля.',
  `q_z` FLOAT NULL DEFAULT 0 COMMENT 'Направление разворота корабля.',
  `q_w` FLOAT NULL DEFAULT 1 COMMENT 'Направление разворота корабля.',
  `level` SMALLINT UNSIGNED NULL DEFAULT 0 COMMENT 'Уровень ранга корабля.',
  `exp` BIGINT NULL DEFAULT 0 COMMENT 'Текущее кол-во опыта корабля игрока.',
  `access_level` TINYINT NULL DEFAULT 0 COMMENT 'Уровень доступа.',
  `engine_energy` FLOAT NULL DEFAULT 1 COMMENT 'Процент распределения энергии на двигатель.',
  `station_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Ид станции в радиусе которой находится сейчас корабль игрока.',
  `on_station` TINYINT NOT NULL DEFAULT 0 COMMENT 'Находится ли корабль сейчас на космической станции.',
  `current_strength` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Состояние корпуса корабля.',
  `max_strength` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Максимальная прочность корабля.',
  `current_shield` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Состояние щитов корабля.',
  `max_shield` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Максимальная прочность щитов корабля.',
  PRIMARY KEY (`object_id`),
  INDEX `account_name` (`account_name` ASC),
  UNIQUE INDEX `ship_name` (`pilot_name` ASC),
  INDEX `restore_index` (`account_name` ASC, `object_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица кораблей игроков.';


-- -----------------------------------------------------
-- Table `gdb`.`player_ship_modules`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ship_modules` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ship_modules` (
  `object_id` INT NOT NULL COMMENT 'Уникальный ид модуля.',
  `template_id` INT NULL COMMENT 'Ид шаблона модуля.',
  `owner_id` INT UNSIGNED NOT NULL,
  `index` TINYINT NULL COMMENT 'Номер сокета, в который вставлен.',
  PRIMARY KEY (`object_id`),
  INDEX `select_key` (`owner_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица вставленных модулей в корабли ироков.';


-- -----------------------------------------------------
-- Table `gdb`.`player_ship_skill_panel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ship_skill_panel` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ship_skill_panel` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид игрока.',
  `module_id` INT NOT NULL,
  `skill_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Ид шаблона умения.',
  `order` TINYINT UNSIGNED NOT NULL COMMENT 'Позиция на панели.',
  PRIMARY KEY (`object_id`, `module_id`, `skill_id`, `order`),
  INDEX `ship_key` (`object_id` ASC),
  INDEX `module_key` (`module_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица расположения умений на панели быстрого доступа игрок /* comment truncated */ /*ов.*/';


-- -----------------------------------------------------
-- Table `gdb`.`player_ship_interface`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ship_interface` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ship_interface` (
  `object_id` INT UNSIGNED NOT NULL,
  `element_type` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Тип элемента интерфейса.',
  `x` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Координата положения элемента на экране.',
  `y` INT UNSIGNED NOT NULL DEFAULT 0,
  `minimized` TINYINT(1) NOT NULL DEFAULT false,
  PRIMARY KEY (`element_type`, `object_id`),
  INDEX `select_key` (`object_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица расположения элементов интерфейса игрока.';


-- -----------------------------------------------------
-- Table `gdb`.`player_ship_quests`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ship_quests` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ship_quests` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид игрока.',
  `quest_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Ид задания.',
  `state` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Стадия выполнения задания.',
  `finish_date` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Дата завершения задания.',
  `finish_status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Результат завершения задания.',
  `favorite` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Находится ли задание в изрбранном пользователя.',
  PRIMARY KEY (`quest_id`, `object_id`),
  INDEX `select_key` (`object_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица квестов кораблей игроков.';


-- -----------------------------------------------------
-- Table `gdb`.`player_ship_quest_vars`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ship_quest_vars` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ship_quest_vars` (
  `object_id` INT UNSIGNED NOT NULL,
  `quest_id` INT UNSIGNED NOT NULL,
  `var_name` VARCHAR(45) NOT NULL,
  `var_value` VARCHAR(45) NOT NULL,
  INDEX `clear_key` (`quest_id` ASC, `object_id` ASC),
  PRIMARY KEY (`object_id`, `quest_id`, `var_name`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица квестовых переменных игроков.';


-- -----------------------------------------------------
-- Table `gdb`.`player_ship_storage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ship_storage` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ship_storage` (
  `owner_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Уникальный ид корабля, чье это хранилище.',
  `size` SMALLINT UNSIGNED NULL DEFAULT 0 COMMENT 'Размер хранилища.',
  INDEX `owner_index` (`owner_id` ASC),
  PRIMARY KEY (`owner_id`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица хранилищ кораблей игрока.';


-- -----------------------------------------------------
-- Table `gdb`.`items`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`items` ;

CREATE TABLE IF NOT EXISTS `gdb`.`items` (
  `object_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Уникальный ид предмета.',
  `owner_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Уникальный ид владельца.',
  `location` INT UNSIGNED NULL DEFAULT 0 COMMENT 'Тип места размещения предмета.',
  `template_id` INT UNSIGNED NULL DEFAULT 0 COMMENT 'Ид шаблона предмета',
  `item_count` BIGINT UNSIGNED NULL DEFAULT 0 COMMENT 'Кол-во предметов.',
  `index` SMALLINT UNSIGNED NULL DEFAULT 0 COMMENT 'Индекс ячейки, в которой размещен предмет.',
  `create_date` BIGINT UNSIGNED NULL DEFAULT 0 COMMENT 'Дата создания предмета.',
  `creator` VARCHAR(45) NULL COMMENT 'Имя создателя предмета.',
  PRIMARY KEY (`object_id`, `owner_id`),
  INDEX `select_index` (`owner_id` ASC, `location` ASC),
  INDEX `update_index` (`object_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица игровых предметов.';


-- -----------------------------------------------------
-- Table `gdb`.`player_ship_item_panel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_ship_item_panel` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_ship_item_panel` (
  `object_id` INT UNSIGNED NOT NULL,
  `item_id` INT UNSIGNED NOT NULL,
  `order` TINYINT UNSIGNED NOT NULL COMMENT 'Позиция на панели.',
  PRIMARY KEY (`object_id`, `item_id`, `order`),
  INDEX `ship_key` (`object_id` ASC),
  INDEX `item_id_key` (`item_id` ASC),
  INDEX `select_key` (`object_id` ASC, `item_id` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Таблица расположения умений на панели быстрого доступа игрок /* comment truncated */ /*ов.*/';


-- -----------------------------------------------------
-- Table `gdb`.`player_achievement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`player_achievement` ;

CREATE TABLE IF NOT EXISTS `gdb`.`player_achievement` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.\n',
  `achievement_type` INT NOT NULL DEFAULT 0 COMMENT 'Тип достижения.',
  `achievement_value` BIGINT NOT NULL DEFAULT 0 COMMENT 'Текущее хначение достижения.',
  INDEX `fk_player_achievement_player_ships1_idx` (`object_id` ASC),
  PRIMARY KEY (`object_id`, `achievement_type`))
ENGINE = MyISAM
COMMENT = 'Таблица с достижениями игроков.';


-- -----------------------------------------------------
-- Table `gdb`.`weapon_rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`weapon_rating` ;

CREATE TABLE IF NOT EXISTS `gdb`.`weapon_rating` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.',
  `weapon_type` TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Тип модуля оружия.',
  `shot_count` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Кол-во произведенных выстрелов этим оружием.',
  `hit_count` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Кол-во произведенных попаданий этим оружием.',
  `depletion_count` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Кол-во истощений оружия.',
  PRIMARY KEY (`object_id`, `weapon_type`),
  INDEX `select_object_id` (`object_id` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `gdb`.`local_rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`local_rating` ;

CREATE TABLE IF NOT EXISTS `gdb`.`local_rating` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.',
  `element_type` SMALLINT(3) NOT NULL COMMENT 'Тип элемента локального рейтинга.',
  `hit_count` BIGINT NOT NULL DEFAULT 0 COMMENT 'Кол-во попаданий',
  `repair_count` BIGINT NOT NULL DEFAULT 0 COMMENT 'Кол-во полных восстановлений.',
  PRIMARY KEY (`object_id`, `element_type`),
  INDEX `select_object_id` (`object_id` ASC))
ENGINE = MyISAM
COMMENT = 'Таблица с данными локального рейтинга игроков.';


-- -----------------------------------------------------
-- Table `gdb`.`shield_rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`shield_rating` ;

CREATE TABLE IF NOT EXISTS `gdb`.`shield_rating` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.',
  `shield_type` TINYINT(2) NOT NULL COMMENT 'Тип модуля щита.',
  `hit_count` BIGINT NOT NULL DEFAULT 0 COMMENT 'Кол-во попаданий в щит.',
  `reload_count` BIGINT NOT NULL DEFAULT 0 COMMENT 'Кол-во восстановлений щитом.',
  PRIMARY KEY (`object_id`, `shield_type`),
  INDEX `select_object_id` (`object_id` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `gdb`.`destroy_rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`destroy_rating` ;

CREATE TABLE IF NOT EXISTS `gdb`.`destroy_rating` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.',
  `destroy_type` SMALLINT(3) NOT NULL COMMENT 'Тип элемента рейтинга уничтожений..',
  `destroy_count` BIGINT NOT NULL DEFAULT 0 COMMENT 'Кол-во уничтожений.',
  PRIMARY KEY (`object_id`, `destroy_type`),
  INDEX `select_object_id` (`object_id` ASC))
ENGINE = MyISAM
COMMENT = 'Таблица с данными разрушений корабля игроков.';


-- -----------------------------------------------------
-- Table `gdb`.`kill_rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`kill_rating` ;

CREATE TABLE IF NOT EXISTS `gdb`.`kill_rating` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.',
  `kill_type` SMALLINT(3) UNSIGNED NOT NULL COMMENT 'Тип элемента рейтинга убийства..',
  `rocket_launcher_count` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Кол-во убийств.',
  `tachyon_count` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `laser_count` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `pulse_count` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `neutron_count` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `photon_count` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `plasma_count` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`object_id`, `kill_type`),
  INDEX `select_object_id` (`object_id` ASC))
ENGINE = MyISAM
COMMENT = 'Таблица с данными рейтинга убийства целей игроком.';


-- -----------------------------------------------------
-- Table `gdb`.`skill_states`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`skill_states` ;

CREATE TABLE IF NOT EXISTS `gdb`.`skill_states` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.',
  `module_id` INT NOT NULL COMMENT 'Уникальный ид модуля, в котором находится умение.',
  `skill_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Уникальный ид умения в рамках модуля.',
  `charge` INT NOT NULL DEFAULT 0 COMMENT 'Текущий заряд умения.',
  `max_charge` INT NOT NULL COMMENT 'Максимальный заряд умения.',
  `reload_time` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Время перезарядки умения.',
  `reload_finish_time` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Дата завершения перезарядки умения.',
  INDEX `select_all_key` (`object_id` ASC),
  PRIMARY KEY (`object_id`, `module_id`, `skill_id`),
  INDEX `update_key` (`object_id` ASC, `module_id` ASC, `skill_id` ASC),
  INDEX `select_module_id` (`module_id` ASC))
ENGINE = MyISAM
COMMENT = 'Таблица состояний умений корабля.';


-- -----------------------------------------------------
-- Table `gdb`.`friend_pilots`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gdb`.`friend_pilots` ;

CREATE TABLE IF NOT EXISTS `gdb`.`friend_pilots` (
  `object_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид корабля игрока.',
  `friend_id` INT UNSIGNED NOT NULL COMMENT 'Уникальный ид дружественного корабля игрока.',
  INDEX `select_index` (`object_id` ASC),
  INDEX `friend_index` (`friend_id` ASC),
  PRIMARY KEY (`object_id`, `friend_id`))
ENGINE = MyISAM
COMMENT = 'Таблица со списками дружественных пилотов игроков.';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

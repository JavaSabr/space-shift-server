package com.ss.login.model;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

/**
 * Контейнер инфы о сервере.
 * 
 * @author Ronn
 */
public final class GameServer implements Foldable {

	public static GameServer newInstance(final String name, final String type, final String host, final int port, final float loading, final int online) {

		GameServer info = POOL.take();

		if(info == null) {
			info = new GameServer();
		}

		info.name = name;
		info.type = type;
		info.host = host;
		info.port = port;
		info.loading = loading;
		info.online = online;
		info.string = "[" + name + "] [" + type + "] [" + loading + "] [ " + online + "]";

		return info;
	}

	private static final FoldablePool<GameServer> POOL = PoolFactory.newAtomicFoldablePool(GameServer.class);

	/** название сервера */
	private String name;
	/** тип сервера */
	private String type;
	/** адресс сервера */
	private String host;
	/** текстовое предстовление контейнера */
	private String string;

	/** загруженность сервера */
	private float loading;
	/** порт сервера */
	private int port;
	/** онлаин сервера */
	private int online;

	@Override
	public void finalyze() {
		host = null;
		loading = 0F;
		name = null;
		online = 0;
		type = null;
	}

	/**
	 * Сложить в пул.
	 */
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return адресс сервера.
	 */
	public final String getHost() {
		return host;
	}

	/**
	 * @return загруженность сервера.
	 */
	public final float getLoading() {
		return loading;
	}

	/**
	 * @return название сервера.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return онлаин сервера.
	 */
	public final int getOnline() {
		return online;
	}

	/**
	 * @return порт сервера.
	 */
	public final int getPort() {
		return port;
	}

	/**
	 * @return тип сервера.
	 */
	public final String getType() {
		return type;
	}

	/**
	 * @param loading загрузка сервера.
	 */
	public final void setLoading(final float loading) {
		this.loading = loading;
	}

	/**
	 * @param online онлаин сервера.
	 */
	public final void setOnline(final int online) {
		this.online = online;
	}

	@Override
	public String toString() {
		return string;
	}
}

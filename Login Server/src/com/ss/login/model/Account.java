package com.ss.login.model;

import rlib.util.pools.Foldable;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

import com.ss.login.network.model.Client;

/**
 * Модель аккаунта пользоватля.
 * 
 * @author Ronn
 */
public class Account implements Foldable {

	public static Account newInstance(final String name, final String password, final String email, final int accessLevel, final long endPay, final long endBlock, final String lastIP,
			final String allowIPs, final String comments) {

		Account account = POOL.take();

		if(account == null) {
			account = new Account();
		}

		account.name = name;
		account.password = password;
		account.email = email;
		account.accessLevel = accessLevel;
		account.endPay = endPay;
		account.endBlock = endBlock;
		account.lastIP = lastIP;
		account.allowIPs = allowIPs;
		account.comments = comments;

		return account;
	}

	private static final FoldablePool<Account> POOL = PoolFactory.newAtomicFoldablePool(Account.class);

	/** пользователь */
	private Client user;

	/** имя аккаунта */
	private String name;
	/** пароль аккаунта */
	private String password;
	/** почта аккаунта */
	private String email;
	/** последний ип аккаунта */
	private String lastIP;
	/** доступные ип для аккаунта */
	private String allowIPs;
	/** коментарии к аккаунту */
	private String comments;

	/** время окончания проплаты */
	private long endPay;
	/** время окончания бана */
	private long endBlock;

	/** уровень прав */
	private int accessLevel;

	@Override
	public void finalyze() {
		name = null;
		allowIPs = null;
		comments = null;
		email = null;
		lastIP = null;
		password = null;
		user = null;
	}

	/**
	 * Сложить в пул.
	 */
	public void fold() {
		POOL.put(this);
	}

	/**
	 * @return уровень прав.
	 */
	public final int getAccessLevel() {
		return accessLevel;
	}

	/**
	 * @return разрешенные ИП.
	 */
	public final String getAllowIPs() {
		return allowIPs;
	}

	/**
	 * @return коментарий к аккаунту.
	 */
	public final String getComments() {
		return comments;
	}

	/**
	 * @return имеил.
	 */
	public final String getEmail() {
		return email;
	}

	/**
	 * @return окончание бана.
	 */
	public final long getEndBlock() {
		return endBlock;
	}

	/**
	 * @return окончание проплаты.
	 */
	public final long getEndPay() {
		return endPay;
	}

	/**
	 * @return последний ип.
	 */
	public final String getLastIP() {
		return lastIP;
	}

	/**
	 * @return имя аккаунта.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return пароль аккаунта.
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * @return пользователь.
	 */
	public final Client getUser() {
		return user;
	}

	/**
	 * @param user пользователь.
	 */
	public final void setUser(final Client user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Account [name = " + name + ", email = " + email + ", lastIP = " + lastIP + "]";
	}
}

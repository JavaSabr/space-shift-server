package com.ss.login.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import rlib.database.ConnectFactories;
import rlib.database.ConnectFactory;
import rlib.database.DBUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.login.Config;
import com.ss.login.model.Account;

/**
 * Менеджер работы с Базой Данных.
 * 
 * @author Ronn
 */
public final class DataBaseManager {

	public static DataBaseManager getInstance() {

		if(instance == null) {
			instance = new DataBaseManager();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(DataBaseManager.class);
	private static final String CREATE_ACCOUNT = "INSERT INTO `accounts` (login, password, email) VALUES (?, ?, ?)";

	private static final String RESTORE_ACCOUNT = "SELECT * FROM `accounts` WHERE `login` = ? LIMIT 1";

	private static DataBaseManager instance;

	/** фабрика подключений к БД */
	private final ConnectFactory connectFactory;

	private DataBaseManager() {
		InitializeManager.valid(getClass());
		this.connectFactory = ConnectFactories.newBoneCPConnectFactory(Config.DATA_BASE_CONFIG, Config.DATA_BASE_DRIVER);
	}

	/**
	 * Создание нового аккаунта.
	 * 
	 * @param login логин аккаунта.
	 * @param password пароль аккаунта.
	 * @param email почта аккаунта.
	 * @return создался ли аккаунт.
	 */
	public boolean createAccount(final String login, final String password, final String email) {

		PreparedStatement statement = null;
		Connection con = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(CREATE_ACCOUNT);
			statement.setString(1, login);
			statement.setString(2, password);
			statement.setString(3, email);
			statement.execute();

			return true;
		} catch(final SQLException e) {
			return false;
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}
	}

	/**
	 * Загрузка из бд аккаунта.
	 * 
	 * @param login имя аккаунта.
	 * @return аккаунт.
	 */
	public Account restoreAccount(final String login) {

		PreparedStatement statement = null;
		Connection con = null;
		ResultSet rset = null;

		try {

			con = connectFactory.getConnection();

			statement = con.prepareStatement(RESTORE_ACCOUNT);
			statement.setString(1, login);

			rset = statement.executeQuery();

			if(rset.next()) {

				final String password = rset.getString("password");
				final String email = rset.getString("email");
				final String lastIp = rset.getString("last_ip");
				final String allowIps = rset.getString("allow_ips");
				final String comments = rset.getString("comments");

				final int access_level = rset.getInt("access_level");

				final long endPay = rset.getLong("end_pay");
				final long endBlock = rset.getLong("end_block");

				return Account.newInstance(login, password, email, access_level, endPay, endBlock, lastIp, allowIps, comments);
			}

		} catch(final SQLException e) {
			LOGGER.warning(e);
		} finally {
			DBUtils.closeDatabaseCS(con, statement);
		}

		return null;
	}
}
package com.ss.login.manager;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.login.model.GameServer;

/**
 * Менеджер списка серверов.
 * 
 * @author Ronn
 */
public final class GameServerManager {

	public static GameServerManager getInstance() {

		if(instance == null) {
			instance = new GameServerManager();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(GameServerManager.class);

	private static GameServerManager instance;

	/** список серверов */
	private final Array<GameServer> serverList;

	private GameServerManager() {
		InitializeManager.valid(getClass());
		this.serverList = ArrayFactory.newConcurrentArray(GameServer.class);
	}

	/**
	 * Добавление игрового сервера в список серверов.
	 * 
	 * @param server игровой сервер.
	 */
	public void addServer(final GameServer server) {

		final Array<GameServer> serverList = getServerList();
		serverList.writeLock();
		try {
			serverList.add(server);
		} finally {
			serverList.writeUnlock();
		}

		LOGGER.info("add server " + server);
	}

	/**
	 * @return список серверов.
	 */
	public Array<GameServer> getServerList() {
		return serverList;
	}

	/**
	 * Удаление из списка игрового сервера.
	 * 
	 * @param server игровой сервер.
	 */
	public void removeServer(final GameServer server) {

		final Array<GameServer> serverList = getServerList();
		serverList.writeLock();
		try {
			serverList.slowRemove(server);
		} finally {
			serverList.writeUnlock();
		}

		LOGGER.info("remove server " + server);
	}
}

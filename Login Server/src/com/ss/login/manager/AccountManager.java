package com.ss.login.manager;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.util.StringUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.ss.login.Config;
import com.ss.login.model.Account;
import com.ss.login.network.model.Client;
import com.ss.login.network.packet.client.RequestUserRegister.RegisterResultType;
import com.ss.login.network.packet.server.UserAuthResult;
import com.ss.login.network.packet.server.UserAuthResult.AuthResultType;
import com.ss.login.network.packet.server.UserRegisterResult;

/**
 * Менеджер для работы с аккаунтами.
 * 
 * @author Ronn
 */
public final class AccountManager {

	public static AccountManager getInstance() {

		if(instance == null) {
			instance = new AccountManager();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(AccountManager.class);

	public static final String REMOVED_ACCOUNT_NAME = "removed";

	private static AccountManager instance;

	/** список аккаунтов */
	private final Array<Account> accounts;

	private AccountManager() {
		InitializeManager.valid(getClass());
		this.accounts = ArrayFactory.newConcurrentAtomicArray(Account.class);
	}

	/**
	 * Авторизация пользователя.
	 * 
	 * @param client клиент пользователя.
	 * @param name имя аккаунта.
	 * @param password пароль аккаунта.
	 */
	public void auth(final Client client, final String name, final String password) {

		final DataBaseManager manager = DataBaseManager.getInstance();

		Account account = getAccount(name);

		if(account == null) {
			account = manager.restoreAccount(name);
		}

		if(account == null) {

			LOGGER.warning("incorrect login " + name);

			if(!Config.AUTO_ACCOUNT_REGISTER || REMOVED_ACCOUNT_NAME.equalsIgnoreCase(name) || name.length() < 4) {
				client.sendPacket(UserAuthResult.getInstance(AuthResultType.INCORRECT_LOGIN), true);
				return;
			}

			LOGGER.warning("try auto create account for login " + name);

			manager.createAccount(name, password, "autoregistration");
			account = manager.restoreAccount(name);
		}

		if(account == null) {
			client.sendPacket(UserAuthResult.getInstance(AuthResultType.INCORRECT_LOGIN), true);
			return;
		}

		if(!StringUtils.equals(password, account.getPassword())) {
			client.sendPacket(UserAuthResult.getInstance(AuthResultType.INCORRECT_PASSWORD), true);
			return;
		}

		final Client prev = account.getUser();

		if(prev != null) {

			if(prev != client) {

				client.setAccount(account);
				account.setUser(client);

				prev.close();
			}

		} else {

			client.setAccount(account);
			account.setUser(client);

			final Array<Account> accounts = getAccounts();
			accounts.writeLock();
			try {
				accounts.add(account);
			} finally {
				accounts.writeUnlock();
			}
		}

		client.sendPacket(UserAuthResult.getInstance(AuthResultType.SUCCESSFUL), true);

		LOGGER.info("auth user \"" + account.getName() + "\"");
	}

	/**
	 * Получение аккаунта из кэшеированных.
	 * 
	 * @param name имя аккаунта.
	 * @return аккаунт.
	 */
	public Account getAccount(final String name) {

		final Array<Account> accounts = getAccounts();

		accounts.readLock();
		try {

			for(final Account account : accounts.array()) {

				if(account == null) {
					break;
				}

				if(name.equalsIgnoreCase(account.getName())) {
					return account;
				}
			}

			return null;

		} finally {
			accounts.readUnlock();
		}
	}

	/**
	 * Получение аккаунта из активных аккаунтов, по имени и паролю.
	 * 
	 * @param name имя аккаунта.
	 * @param password пароль аккаунта.
	 * @return искомый аккаунт.
	 */
	public Account getAccount(final String name, final String password) {

		final Account account = getAccount(name);

		if(account == null || !account.getPassword().equals(password)) {
			return null;
		}

		return account;
	}

	/**
	 * @return закешеированные аккаунты.
	 */
	private Array<Account> getAccounts() {
		return accounts;
	}

	/**
	 * Обработка регистрации нового аккаунта.
	 * 
	 * @param client регистрируемый клиент.
	 * @param login регистрируемый логин.
	 * @param password регистрируемый пароль.
	 * @param email регистрируемая почта.
	 */
	public void register(final Client client, final String login, final String password, final String email) {

		if(!StringUtils.checkEmail(email)) {
			client.sendPacket(UserRegisterResult.getInstance(RegisterResultType.INCORRECT_EMAIL), true);
			return;
		}

		final DataBaseManager manager = DataBaseManager.getInstance();

		if(REMOVED_ACCOUNT_NAME.equalsIgnoreCase(login) || login.length() < 4 || !manager.createAccount(login, password, email)) {
			client.sendPacket(UserRegisterResult.getInstance(RegisterResultType.INCORRECT_LOGIN), true);
		} else {
			client.sendPacket(UserRegisterResult.getInstance(RegisterResultType.SUCCESSFUL), true);
			LOGGER.info("register new account \"" + login + "\".");
		}
	}

	/**
	 * Удаление аккаунта из кэша.
	 * 
	 * @param account удаляемый аккаунт.
	 */
	public void removeAccount(final Account account) {

		if(account == null) {
			return;
		}

		final Array<Account> accounts = getAccounts();
		accounts.writeLock();
		try {

			if(accounts.fastRemove(account)) {
				account.fold();
			}

		} finally {
			accounts.writeUnlock();
		}
	}
}

package com.ss.login.manager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import rlib.concurrent.GroupThreadFactory;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;

import com.ss.login.ServerThread;

/**
 * Менеджер пула потоков сервера.
 * 
 * @author Ronn
 */
public final class ExecutorManager {

	public static ExecutorManager getInstance() {

		if(instance == null) {
			instance = new ExecutorManager();
		}

		return instance;
	}

	/** логгер для менеджера */
	private static final Logger LOGGER = LoggerManager.getLogger(ExecutorManager.class);

	private static ExecutorManager instance;

	/** пул потоков для основных тасков */
	private final ScheduledExecutorService generalPool;

	/** обработчик клиентских пакетов */
	private final ExecutorService asynchPacketPool;
	/** обработчик параллельных дейтсвий */
	private final ExecutorService executor;

	private ExecutorManager() {

		InitializeManager.valid(getClass());

		generalPool = Executors.newScheduledThreadPool(3, new GroupThreadFactory("GeneralThreadPool", ServerThread.class, Thread.NORM_PRIORITY + 1));
		asynchPacketPool = Executors.newFixedThreadPool(3, new GroupThreadFactory("AsynchPacketPool", ServerThread.class, Thread.NORM_PRIORITY));
		executor = Executors.newFixedThreadPool(2, new GroupThreadFactory("AsynThreadPool", ServerThread.class, Thread.MAX_PRIORITY));

		LOGGER.info("initialized.");
	}

	/**
	 * Выполнение задания в параллельном потоке.
	 * 
	 * @param runnable задание, которое надо выполнить.
	 */
	public void execute(final Runnable runnable) {
		executor.execute(runnable);
	}

	/**
	 * Отправка на асинхронное выполнение пакет.
	 * 
	 * @param packet пакет, который нужно отправить.
	 */
	public void runAsynPacket(final Runnable packet) {
		try {
			asynchPacketPool.execute(packet);
		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}

	/**
	 * Создание отложенного общего таска.
	 * 
	 * @param runnable содержание таска.
	 * @param delay задержка перед выполнением.
	 * @return ссылка на таск.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneral(final T runnable, long delay) {
		try {

			if(delay < 0) {
				delay = 0;
			}

			return (ScheduledFuture<T>) generalPool.schedule(runnable, delay, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * Создание периодического отложенного общего таска.
	 * 
	 * @param runnable содержание таска.
	 * @param delay задержка перед первым запуском.
	 * @param interval интервал между выполнением таска.
	 * @return ссылка на таск.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneralAtFixedRate(final T runnable, final long delay, long interval) {
		try {

			if(interval <= 0) {
				interval = 1;
			}

			return (ScheduledFuture<T>) generalPool.scheduleAtFixedRate(runnable, delay, interval, TimeUnit.MILLISECONDS);
		} catch(final RejectedExecutionException e) {
			LOGGER.warning(e);
		}

		return null;
	}
}
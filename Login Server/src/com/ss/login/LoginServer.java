package com.ss.login;

import java.io.File;

import rlib.Monitoring;
import rlib.logging.Logger;
import rlib.logging.LoggerLevel;
import rlib.logging.LoggerManager;
import rlib.logging.impl.FolderFileListener;
import rlib.manager.InitializeManager;
import rlib.util.Util;

import com.ss.login.manager.AccountManager;
import com.ss.login.manager.DataBaseManager;
import com.ss.login.manager.ExecutorManager;
import com.ss.login.manager.GameServerManager;
import com.ss.login.network.ClientPacketType;
import com.ss.login.network.Network;

/**
 * Логин сервер проекта MMO Space Shift
 * 
 * @author Ronn
 */
public class LoginServer {

	private static void configureLogging() {

		LoggerLevel.DEBUG.setEnabled(true);
		LoggerLevel.ERROR.setEnabled(true);
		LoggerLevel.WARNING.setEnabled(true);
		LoggerLevel.INFO.setEnabled(true);

		LoggerManager.addListener(new FolderFileListener(new File(Config.FOLDER_PROJECT_PATH, "log")));
	}

	public static LoginServer getInstance() {

		if(instance == null) {
			instance = new LoginServer();
		}

		return instance;
	}

	public static void main(final String[] args) {

		Monitoring.init();
		Config.init();

		configureLogging();

		Util.checkFreePort("*", Config.GAME_PORT);
		Util.checkFreePort("*", Config.USER_PORT);

		ClientPacketType.init();

		getInstance();

		System.gc();
	}

	private static final Logger LOGGER = LoggerManager.getLogger(LoginServer.class);

	/** экземпляр логин сервера */
	private static LoginServer instance;

	private LoginServer() {

		InitializeManager.register(ExecutorManager.class);
		InitializeManager.register(DataBaseManager.class);
		InitializeManager.register(AccountManager.class);
		InitializeManager.register(GameServerManager.class);
		InitializeManager.register(Network.class);
		InitializeManager.initialize();

		LOGGER.info("initialized.");
	}
}

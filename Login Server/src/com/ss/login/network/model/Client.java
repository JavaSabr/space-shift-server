package com.ss.login.network.model;

import rlib.network.server.client.impl.AbstractClient;

import com.ss.login.manager.AccountManager;
import com.ss.login.manager.ExecutorManager;
import com.ss.login.manager.GameServerManager;
import com.ss.login.model.Account;
import com.ss.login.model.GameServer;
import com.ss.login.network.ClientPacket;
import com.ss.login.network.NetCrypt;
import com.ss.login.network.ServerPacket;

/**
 * Модель клиента пользователей и серверов.
 * 
 * @author Ronn
 */
public class Client extends AbstractClient<Object, Void, NetConnection, NetCrypt, ClientPacket, ServerPacket> {

	/** менеджер серверов */
	private static final GameServerManager SERVER_MANAGER = GameServerManager.getInstance();
	/** менеджер аккаунтов */
	private static final AccountManager ACCOUNT_MANAGER = AccountManager.getInstance();
	/** исполняющий менеджер */
	private static final ExecutorManager EXECUTOR_MANAGER = ExecutorManager.getInstance();

	public Client(final NetConnection connection, final NetCrypt crypt) {
		super(connection, crypt);
	}

	@Override
	public void close() {

		final Object account = getAccount();

		LOGGER.info("close " + account);

		if(account != null && account instanceof GameServer) {
			SERVER_MANAGER.removeServer((GameServer) account);
		} else if(account != null && account instanceof Account) {
			ACCOUNT_MANAGER.removeAccount((Account) account);
		}

		super.close();
	}

	@Override
	protected void execute(final ClientPacket packet) {
		EXECUTOR_MANAGER.runAsynPacket(packet);
	}

	@Override
	public String getHostAddress() {
		return "unknown";
	}

	/**
	 * Отправка пакета с возможность увеличения счетчика отправок.
	 * 
	 * @param packet пакет дял отправки.
	 * @param increaseSends увеличивать ли счетчик отправок.
	 */
	public void sendPacket(final ServerPacket packet, final boolean increaseSends) {

		if(increaseSends) {
			packet.increaseSends();
		}

		sendPacket(packet);
	}
}

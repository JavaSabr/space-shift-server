package com.ss.login.network.model;

import java.nio.channels.AsynchronousSocketChannel;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.network.server.AcceptHandler;

import com.ss.login.network.NetCrypt;
import com.ss.login.network.Network;
import com.ss.login.network.ServerPacket;
import com.ss.login.network.packet.server.RequestServerRegister;

/**
 * Модель обработчика подключений серверов.
 * 
 * @author Ronn
 */
public final class ServerAcceptHandler extends AcceptHandler {

	public static ServerAcceptHandler getInstance() {

		if(instance == null) {
			instance = new ServerAcceptHandler();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(ServerAcceptHandler.class);

	private static ServerAcceptHandler instance;

	private ServerAcceptHandler() {
		super();
	}

	@Override
	protected void onAccept(final AsynchronousSocketChannel channel) {

		final Network network = Network.getInstance();

		final NetConnection connection = new NetConnection(network.getServerNetwork(), channel, ServerPacket.class);
		final Client client = new Client(connection, new NetCrypt());

		connection.setClient(client);
		connection.startRead();

		client.sendPacket(RequestServerRegister.getInstance(), true);
	}

	@Override
	protected void onFailed(final Throwable exc) {
		LOGGER.warning(this, new Exception(exc));
	}
}

package com.ss.login.network.model;

import java.nio.channels.AsynchronousSocketChannel;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.network.server.AcceptHandler;

import com.ss.login.network.NetCrypt;
import com.ss.login.network.Network;
import com.ss.login.network.ServerPacket;
import com.ss.login.network.packet.server.TestUserConnect;

/**
 * Модель обработчика подключений пользователей.
 * 
 * @author Ronn
 */
public final class UserAcceptHandler extends AcceptHandler {

	public static UserAcceptHandler getInstance() {

		if(instance == null) {
			instance = new UserAcceptHandler();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(UserAcceptHandler.class);

	private static UserAcceptHandler instance;

	private UserAcceptHandler() {
		super();
	}

	@Override
	protected void onAccept(final AsynchronousSocketChannel channel) {

		final Network network = Network.getInstance();

		final NetConnection connection = new NetConnection(network.getUserNetwork(), channel, ServerPacket.class);
		final Client client = new Client(connection, new NetCrypt());

		connection.setClient(client);
		connection.startRead();

		client.sendPacket(TestUserConnect.getInstance(), true);
	}

	@Override
	protected void onFailed(final Throwable exc) {
		LOGGER.warning(this, new Exception(exc));
	}
}

package com.ss.login.network.model;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

import rlib.network.server.ServerNetwork;
import rlib.network.server.client.impl.AbstractClientConnection;
import rlib.util.Util;

import com.ss.login.Config;
import com.ss.login.network.ClientPacket;
import com.ss.login.network.ClientPacketType;
import com.ss.login.network.ServerPacket;

/**
 * Модель подключения сервера к клиенту.
 * 
 * @author Ronn
 */
public final class NetConnection extends AbstractClientConnection<Client, ClientPacket, ServerPacket> {

	public NetConnection(final ServerNetwork network, final AsynchronousSocketChannel channel, final Class<ServerPacket> sendableType) {
		super(network, channel, sendableType);
	}

	/**
	 * Получение пакета из буфера.
	 */
	private ClientPacket getPacket(final ByteBuffer buffer, final Client client) {

		int opcode = -1;

		if(buffer.remaining() < 2) {
			return null;
		}

		opcode = buffer.getShort() & 0xFFFF;

		return ClientPacketType.getPacketForOpcode(opcode);
	}

	@Override
	protected ByteBuffer movePacketToBuffer(final ServerPacket packet, final ByteBuffer buffer) {

		final Client client = getClient();

		buffer.clear();

		packet.writePosition(buffer);
		packet.write(buffer);

		buffer.flip();

		packet.writeHeader(buffer, buffer.limit());

		if(Config.DEV_DEBUG_SERVER_PACKETS) {
			System.out.println("Server packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):");
			System.out.println(Util.hexdump(buffer.array(), buffer.limit()));
		}

		client.encrypt(buffer, 2, buffer.limit() - 2);

		if(Config.DEV_DEBUG_SERVER_PACKETS) {
			System.out.println("Server crypt packet " + packet.getName() + ", dump(size: " + buffer.limit() + "):");
			System.out.println(Util.hexdump(buffer.array(), buffer.limit()));
		}

		return buffer;
	}

	@Override
	protected void readPacket(final ByteBuffer buffer) {

		final Client client = getClient();

		if(Config.DEV_DEBUG_CLIENT_PACKETS) {
			System.out.println("Client crypt dump(size: " + buffer.limit() + "):\n" + Util.hexdump(buffer.array(), buffer.limit()));
		}

		int length = buffer.getShort() & 0xFFFF;

		client.decrypt(buffer, buffer.position(), length - 2);

		if(length >= buffer.remaining()) {
			client.readPacket(getPacket(buffer, client), buffer);
		} else {

			client.readPacket(getPacket(buffer, client), buffer);

			buffer.position(length);

			for(int i = 0; buffer.remaining() > 2 && i < 10; i++) {

				length += buffer.getShort() & 0xFFFF;

				client.decrypt(buffer, buffer.position(), length - 2);
				client.readPacket(getPacket(buffer, client), buffer);

				if(length < 4 || length > buffer.limit()) {
					break;
				}

				buffer.position(length);
			}
		}

		if(Config.DEV_DEBUG_CLIENT_PACKETS) {
			System.out.println("Client dump(size: " + buffer.limit() + "):\n" + Util.hexdump(buffer.array(), buffer.limit()));
		}
	}
}

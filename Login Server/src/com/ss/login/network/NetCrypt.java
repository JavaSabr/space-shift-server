package com.ss.login.network;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.network.GameCrypt;
import rlib.util.crypt.SymmetryCrypt;

/**
 * Модель игрового криптора пакетов.
 * 
 * @author Ronn
 */
public final class NetCrypt implements GameCrypt {

	private static final Logger LOGGER = LoggerManager.getLogger(NetCrypt.class);

	/** сам криптор */
	private SymmetryCrypt crypter;

	public NetCrypt() {
		try {
			crypter = new SymmetryCrypt(":qYx5GE?");
		} catch(InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException e) {
			LOGGER.warning(e);
			System.exit(0);
		}
	}

	@Override
	public void decrypt(final byte[] data, final int offset, final int length) {
		try {
			crypter.decrypt(data, offset, length, data);
		} catch(ShortBufferException | IllegalBlockSizeException | BadPaddingException e) {
			LOGGER.warning(e);
		}
	}

	@Override
	public void encrypt(final byte[] data, final int offset, final int length) {
		try {
			crypter.encrypt(data, offset, length, data);
		} catch(ShortBufferException | IllegalBlockSizeException | BadPaddingException e) {
			LOGGER.warning(e);
		}
	}
}

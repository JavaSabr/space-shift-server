package com.ss.login.network;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.StringUtils;
import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;
import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.ss.login.network.packet.client.RequestServerCheckAccount;
import com.ss.login.network.packet.client.RequestUserAuth;
import com.ss.login.network.packet.client.RequestUserRegister;
import com.ss.login.network.packet.client.RequestUserServerList;
import com.ss.login.network.packet.client.ResponseServerRegister;

/**
 * Перечисление типов клиентских пакетов.
 * 
 * @author Ronn
 */
public enum ClientPacketType {

	REQUEST_USER_REGISTER(0x1000, new RequestUserRegister()),
	REQUEST_USER_AUTH(0x1001, new RequestUserAuth()),
	REQUEST_USER_SERVER_LIST(0x1002, new RequestUserServerList()),

	RESPONSE_SERVER_REGISTER(0x2000, new ResponseServerRegister()),
	RESPONSE_SERVER_CHECK_ACCOUNT(0x2001, new RequestServerCheckAccount()), ;

	/**
	 * Возвращает новый экземпляр пакета в соответствии с опкодом
	 * 
	 * @param opcode опкод пакета.
	 * @return экземпляр нужного пакета.
	 */
	public static ClientPacket getPacketForOpcode(final int opcode) {
		final ClientPacket packet = packets[opcode];
		return packet == null ? null : packet.newInstance();
	}

	/**
	 * Инициализация клиентских пакетов.
	 */
	public static void init() {

		final Logger logger = LoggerManager.getDefaultLogger();
		final Table<IntKey, Object> set = TableFactory.newIntegerTable();

		for(final ClientPacketType packet : values()) {

			final int index = packet.getOpcode();

			if(set.containsKey(index)) {
				logger.warning(ClientPacketType.class, "found duplicate opcode " + index + " or " + Integer.toHexString(packet.getOpcode()) + "!");
			}

			set.put(index, StringUtils.EMPTY);

			size = Math.max(size, index);
		}

		set.clear();

		packets = new ClientPacket[65536];

		for(final ClientPacketType packet : values()) {
			packets[packet.getOpcode()] = packet.packet;
		}

		logger.info(ClientPacketType.class, "created an array of package size " + size + " elements for " + values().length + " packets.");
	}

	/** массив пакетов */
	private static ClientPacket[] packets;

	/** кол-во клиентских пакетов */
	private static int size = 0;

	/** пул клиентских пакетов */
	private FoldablePool<ClientPacket> pool;

	/** экземпляр пакета */
	private ClientPacket packet;

	/** опкод пакета */
	private int opcode;

	/**
	 * @param opcode опкод пакета.
	 * @param packet экземпляр пакета.
	 */
	private ClientPacketType(final int opcode, final ClientPacket packet) {
		this.pool = PoolFactory.newAtomicFoldablePool(ClientPacket.class);
		this.opcode = opcode;
		this.packet = packet;

		packet.setPacketType(this);
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * @return пул соответствующего типа пакетов.
	 */
	public FoldablePool<ClientPacket> getPool() {
		return pool;
	}

	/**
	 * @param opcode опкод пакета.
	 */
	public void setOpcode(final int opcode) {
		this.opcode = opcode;
	}
}
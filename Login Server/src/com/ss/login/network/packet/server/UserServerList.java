package com.ss.login.network.packet.server;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import rlib.util.array.Array;

import com.ss.login.manager.GameServerManager;
import com.ss.login.model.GameServer;
import com.ss.login.network.ServerPacket;
import com.ss.login.network.ServerPacketType;

/**
 * Пакет со списком доступных серверов.
 * 
 * @author Ronn
 */
public class UserServerList extends ServerPacket {

	public static UserServerList getInstance() {

		final UserServerList packet = instance.newInstance();

		final ByteBuffer buffer = packet.getPrepare();

		final Array<GameServer> serverList = SERVER_MANAGER.getServerList();
		serverList.readLock();
		try {

			packet.writeByte(buffer, serverList.size());

			final GameServer[] array = serverList.array();

			String name = null;
			String type = null;
			String host = null;

			for(int i = 0, length = serverList.size(); i < length; i++) {

				final GameServer server = array[i];

				if(server == null) {
					continue;
				}

				name = server.getName();

				packet.writeByte(buffer, name.length());
				packet.writeString(buffer, name);

				type = server.getType();

				packet.writeByte(buffer, type.length());
				packet.writeString(buffer, type);

				host = server.getHost();

				packet.writeByte(buffer, host.length());
				packet.writeString(buffer, host);

				packet.writeInt(buffer, server.getPort());
				packet.writeFloat(buffer, server.getLoading());
				packet.writeInt(buffer, server.getOnline());
			}

		} finally {
			serverList.readUnlock();
		}

		buffer.flip();

		return packet;
	}

	private static final GameServerManager SERVER_MANAGER = GameServerManager.getInstance();

	private static final UserServerList instance = new UserServerList();

	/** подготовленные данные к отправке */
	private final ByteBuffer prepare;

	public UserServerList() {
		prepare = ByteBuffer.allocate(4092).order(ByteOrder.LITTLE_ENDIAN);
	}

	@Override
	public void finalyze() {
		prepare.clear();
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.USER_SERVER_LIST;
	}

	private ByteBuffer getPrepare() {
		return prepare;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);

		final ByteBuffer prepare = getPrepare();
		buffer.put(prepare.array(), 0, prepare.limit());
	}
}

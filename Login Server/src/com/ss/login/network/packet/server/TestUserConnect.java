package com.ss.login.network.packet.server;

import java.nio.ByteBuffer;

import com.ss.login.network.ServerPacket;
import com.ss.login.network.ServerPacketType;

/**
 * Пакет для проверки конекта логин сервера с пользователем.
 * 
 * @author Ronn
 */
public class TestUserConnect extends ServerPacket {

	public static ServerPacket getInstance() {
		return instance.newInstance();
	}

	private static final TestUserConnect instance = new TestUserConnect();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.USER_TEST_PACKET;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, 0);
	}
}

package com.ss.login.network.packet.server;

import java.nio.ByteBuffer;

import com.ss.login.network.ServerPacket;
import com.ss.login.network.ServerPacketType;

/**
 * Пакет с результатом попытки авторизации пользователя.
 * 
 * @author Ronn
 */
public class UserAuthResult extends ServerPacket {

	public static enum AuthResultType {
		SUCCESSFUL,
		INCORRECT_LOGIN,
		INCORRECT_PASSWORD,
	}

	public static UserAuthResult getInstance(final AuthResultType type) {

		final UserAuthResult packet = instance.newInstance();
		packet.type = type;

		return packet;
	}

	private static final UserAuthResult instance = new UserAuthResult();

	/** результат авторизации */
	private AuthResultType type;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.USER_AUTH_RESULT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, type.ordinal());
	}
}

package com.ss.login.network.packet.server;

import java.nio.ByteBuffer;

import com.ss.login.network.ServerPacket;
import com.ss.login.network.ServerPacketType;

/**
 * Пакет с запросом на авторизациюю гейм сервера.
 * 
 * @author Ronn
 */
public class RequestServerRegister extends ServerPacket {

	public static ServerPacket getInstance() {
		return instance.newInstance();
	}

	private static final RequestServerRegister instance = new RequestServerRegister();

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.REQUEST_SERVER_REGISTER;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
	}
}

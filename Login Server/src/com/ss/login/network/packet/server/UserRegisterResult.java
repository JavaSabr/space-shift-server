package com.ss.login.network.packet.server;

import java.nio.ByteBuffer;

import com.ss.login.network.ServerPacket;
import com.ss.login.network.ServerPacketType;
import com.ss.login.network.packet.client.RequestUserRegister.RegisterResultType;

/**
 * Пакет с результатом попытки регистрации нового аккаунта.
 * 
 * @author Ronn
 */
public class UserRegisterResult extends ServerPacket {

	public static UserRegisterResult getInstance(final RegisterResultType type) {

		final UserRegisterResult packet = instance.newInstance();
		packet.type = type;

		return packet;
	}

	private static final UserRegisterResult instance = new UserRegisterResult();

	/** результат попытки регистрации пользователя */
	private RegisterResultType type;

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.USER_REGISTER_RESULT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeByte(buffer, type.ordinal());
	}
}

package com.ss.login.network.packet.server;

import java.nio.ByteBuffer;

import com.ss.login.network.ServerPacket;
import com.ss.login.network.ServerPacketType;

/**
 * Пакет с результатом проверки аккаунта игрока.
 * 
 * @author Ronn
 */
public class ResponseCheckAccount extends ServerPacket {

	public static ResponseCheckAccount getInstance(final String name, final String password, final int waitId, final boolean correctly) {

		final ResponseCheckAccount packet = instance.newInstance();
		packet.name = name;
		packet.password = password;
		packet.waitId = waitId;
		packet.correctly = correctly;

		return packet;
	}

	private static final ResponseCheckAccount instance = new ResponseCheckAccount();

	/** имя аккаунта */
	private String name;
	/** парол аккаунта */
	private String password;

	/** ид ожидания клиента */
	private int waitId;

	/** корректный ли аккаунт */
	private boolean correctly;

	@Override
	public void finalyze() {
		name = null;
		password = null;
	}

	@Override
	public ServerPacketType getPacketType() {
		return ServerPacketType.RESPONSE_SERVER_CHECK_ACCOUNT;
	}

	@Override
	protected void writeImpl(final ByteBuffer buffer) {
		writeOpcode(buffer);
		writeInt(buffer, waitId);
		writeByte(buffer, name.length());
		writeString(buffer, name);
		writeByte(buffer, password.length());
		writeString(buffer, password);
		writeByte(buffer, correctly ? 1 : 0);
	}
}

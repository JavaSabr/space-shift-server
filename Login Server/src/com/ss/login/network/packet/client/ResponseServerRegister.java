package com.ss.login.network.packet.client;

import com.ss.login.manager.GameServerManager;
import com.ss.login.model.GameServer;
import com.ss.login.network.ClientPacket;
import com.ss.login.network.model.Client;

/**
 * Ответ на запрос о регистрации сервера.
 * 
 * @author Ronn
 */
public class ResponseServerRegister extends ClientPacket {

	/** клиент сервера */
	private Client client;

	/** информация о сервере */
	private GameServer gameServer;

	@Override
	public void finalyze() {
		client = null;
		gameServer = null;
	}

	@Override
	protected void readImpl() {

		client = getOwner();

		final String name = readString(readByte());
		final String type = readString(readByte());
		final String host = readString(readByte());

		final int port = readInt();

		final float loaded = readFloat();

		final int online = readInt();

		gameServer = GameServer.newInstance(name, type, host, port, loaded, online);
	}

	@Override
	protected void runImpl() {

		client.setAccount(gameServer);

		final GameServerManager manager = GameServerManager.getInstance();
		manager.addServer(gameServer);
	}
}

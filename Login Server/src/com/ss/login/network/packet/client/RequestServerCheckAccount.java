package com.ss.login.network.packet.client;

import com.ss.login.manager.AccountManager;
import com.ss.login.network.ClientPacket;
import com.ss.login.network.model.Client;
import com.ss.login.network.packet.server.ResponseCheckAccount;

/**
 * Ответ на запрос о регистрации сервера.
 * 
 * @author Ronn
 */
public class RequestServerCheckAccount extends ClientPacket {

	/** клиент сервера */
	private Client client;

	/** имя аккаунта */
	private String name;
	/** парол аккаунта */
	private String password;

	/** ид ожидания клиента */
	private int waitId;

	@Override
	public void finalyze() {
		client = null;
		name = null;
		password = null;
	}

	@Override
	protected void readImpl() {
		client = getOwner();
		waitId = readInt();
		name = readString(readByte());
		password = readString(readByte());
	}

	@Override
	protected void runImpl() {
		final AccountManager manager = AccountManager.getInstance();
		client.sendPacket(ResponseCheckAccount.getInstance(name, password, waitId, manager.getAccount(name, password) != null), true);
	}
}

package com.ss.login.network.packet.client;

import com.ss.login.manager.AccountManager;
import com.ss.login.network.ClientPacket;
import com.ss.login.network.model.Client;

/**
 * Запрос на авторизацию пользователя.
 * 
 * @author Ronn
 */
public class RequestUserAuth extends ClientPacket {

	/** клиент пользователя */
	private Client client;

	/** логин аккаунта */
	private String login;
	/** пароль аккаунта */
	private String password;

	@Override
	public void finalyze() {
		login = null;
		password = null;
		client = null;
	}

	@Override
	protected void readImpl() {
		client = getOwner();
		login = readString(readByte());
		password = readString(readByte());
	}

	@Override
	protected void runImpl() {

		if(client == null) {
			return;
		}

		final AccountManager manager = AccountManager.getInstance();
		manager.auth(client, login, password);
	}

	@Override
	public String toString() {
		return "RequestUserAuth [client=" + client + ", login=" + login + ", password=" + password + "]";
	}
}

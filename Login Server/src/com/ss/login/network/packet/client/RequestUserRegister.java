package com.ss.login.network.packet.client;

import com.ss.login.manager.AccountManager;
import com.ss.login.network.ClientPacket;
import com.ss.login.network.model.Client;

/**
 * Запрос на регистрацию нового аккаунта.
 * 
 * @author Ronn
 */
public class RequestUserRegister extends ClientPacket {

	public static enum RegisterResultType {
		SUCCESSFUL,
		INCORRECT_LOGIN,
		INCORRECT_EMAIL,
	}

	/** клиент пользователя */
	private Client client;

	/** логин аккаунта */
	private String login;
	/** пароль аккаунта */
	private String password;
	/** почта аккаунта */
	private String email;

	@Override
	public void finalyze() {
		email = null;
		login = null;
		password = null;
		client = null;
	}

	@Override
	protected void readImpl() {
		client = getOwner();

		login = readString(readByte());
		password = readString(readByte());
		email = readString(readByte());
	}

	@Override
	protected void runImpl() {

		if(client == null) {
			return;
		}

		final AccountManager manager = AccountManager.getInstance();
		manager.register(client, login, password, email);
	}

	@Override
	public String toString() {
		return "RequestUserRegister [client=" + client + ", login=" + login + ", password=" + password + ", email=" + email + "]";
	}
}

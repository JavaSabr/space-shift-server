package com.ss.login.network.packet.client;

import com.ss.login.network.ClientPacket;
import com.ss.login.network.model.Client;
import com.ss.login.network.packet.server.UserServerList;

/**
 * Запрос на авторизацию пользователя.
 * 
 * @author Ronn
 */
public class RequestUserServerList extends ClientPacket {

	/** клиент пользователя */
	private Client client;

	@Override
	public void finalyze() {
		client = null;
	}

	@Override
	protected void readImpl() {
		client = getOwner();
	}

	@Override
	protected void runImpl() {

		if(client == null) {
			return;
		}

		client.sendPacket(UserServerList.getInstance(), true);
	}

	@Override
	public String toString() {
		return "RequestUserServerList [client=" + client + "]";
	}
}

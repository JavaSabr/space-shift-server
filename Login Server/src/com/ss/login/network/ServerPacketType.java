package com.ss.login.network;

import rlib.util.pools.FoldablePool;
import rlib.util.pools.PoolFactory;

/**
 * Перечисление типов серверных пакетов.
 * 
 * @author Ronn
 */
public enum ServerPacketType {

	USER_TEST_PACKET(0x1000),
	USER_REGISTER_RESULT(0x1001),
	USER_AUTH_RESULT(0x1002),
	USER_SERVER_LIST(0x1003),

	REQUEST_SERVER_REGISTER(0x2000),
	RESPONSE_SERVER_CHECK_ACCOUNT(0x2001), ;

	/** пул серверных пакетов */
	private FoldablePool<ServerPacket> pool;

	/** опкод пакета */
	private int opcode;

	/**
	 * @param opcode опкод пакета.
	 */
	private ServerPacketType(final int opcode) {
		this.opcode = opcode;
		this.pool = PoolFactory.newAtomicFoldablePool(ServerPacket.class);
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * @return пул соответствующего типа пакетов.
	 */
	public FoldablePool<ServerPacket> getPool() {
		return pool;
	}

	/**
	 * @param opcode опкод пакета.
	 */
	public void setOpcode(final int opcode) {
		this.opcode = opcode;
	}
}

package com.ss.login.network;

import java.io.IOException;
import java.net.InetSocketAddress;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.manager.InitializeManager;
import rlib.network.NetworkFactory;
import rlib.network.server.ServerNetwork;

import com.ss.login.Config;
import com.ss.login.network.model.ServerAcceptHandler;
import com.ss.login.network.model.UserAcceptHandler;

/**
 * Сеть логин сервера.
 * 
 * @author Ronn
 */
public final class Network {

	public static Network getInstance() {

		if(instance == null) {
			instance = new Network();
		}

		return instance;
	}

	private static final Logger LOGGER = LoggerManager.getLogger(Network.class);

	private static Network instance;

	/** сеть для подключения пользователей */
	private ServerNetwork userNetwork;
	/** сеть для подключения серверов */
	private ServerNetwork serverNetwork;

	private Network() {

		InitializeManager.valid(getClass());

		try {

			userNetwork = NetworkFactory.newDefaultAsynchronousServerNetwork(Config.NETWORK_CONFIG, UserAcceptHandler.getInstance());
			serverNetwork = NetworkFactory.newDefaultAsynchronousServerNetwork(Config.NETWORK_CONFIG, ServerAcceptHandler.getInstance());

			userNetwork.bind(new InetSocketAddress(Config.USER_PORT));
			serverNetwork.bind(new InetSocketAddress(Config.GAME_PORT));

			LOGGER.info("initialized.");
		} catch(final IOException e) {
			LOGGER.warning(e);
		}
	}

	/**
	 * @return сеть для подключения серверов.
	 */
	public ServerNetwork getServerNetwork() {
		return serverNetwork;
	}

	/**
	 * @return сеть для подключения пользователей.
	 */
	public ServerNetwork getUserNetwork() {
		return userNetwork;
	}
}

package com.ss.login.network;

import rlib.network.packet.impl.AbstractRunnableReadeablePacket;
import rlib.util.ClassUtils;
import rlib.util.pools.FoldablePool;

import com.ss.login.network.model.Client;

/**
 * Базовая модель клиентского пакета.
 * 
 * @author Ronn
 */
public abstract class ClientPacket extends AbstractRunnableReadeablePacket<Client> {

	/** типа пакета */
	private ClientPacketType type;

	@Override
	protected final FoldablePool<ClientPacket> getPool() {
		return type.getPool();
	}

	public final ClientPacket newInstance() {

		ClientPacket packet = getPool().take();

		if(packet == null) {
			packet = ClassUtils.newInstance(getClass());
			packet.type = type;
		}

		return packet;
	}

	/**
	 * @param type тип пакета.
	 */
	public final void setPacketType(final ClientPacketType type) {
		this.type = type;
	}
}

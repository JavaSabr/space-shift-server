package com.ss.login.network;

import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;

import rlib.network.packet.impl.AbstractReusableSendablePacket;
import rlib.util.ClassUtils;
import rlib.util.pools.FoldablePool;

import com.ss.login.network.model.Client;

/**
 * Базовая модель отправляемых пакетов.
 * 
 * @author Ronn
 */
public abstract class ServerPacket extends AbstractReusableSendablePacket<Client> {

	/** тип серверного пакета */
	private final ServerPacketType type;

	/** получаем конструктор пакета */
	private final Constructor<? extends ServerPacket> constructor;

	public ServerPacket() {
		this.type = getPacketType();
		this.constructor = ClassUtils.getConstructor(getClass());
	}

	@Override
	protected void completeImpl() {
		getPool().put(this);
	}

	/**
	 * @return тип пакета.
	 */
	public abstract ServerPacketType getPacketType();

	/**
	 * @return получение пула для соотвествующего пакета.
	 */
	protected FoldablePool<ServerPacket> getPool() {
		return type.getPool();
	}

	public boolean isSynchronized() {
		return false;
	}

	/**
	 * @return новый экземпляр пакета.
	 */
	@SuppressWarnings("unchecked")
	public final <T extends ServerPacket> T newInstance() {

		ServerPacket packet = getPool().take();

		if(packet == null) {
			packet = ClassUtils.newInstance(constructor);
		}

		return (T) packet;
	}

	@Override
	public final void writeHeader(final ByteBuffer buffer, final int length) {
		buffer.putShort(0, (short) length);
	}

	/**
	 * Запись опкода пакета.
	 */
	protected final void writeOpcode(final ByteBuffer buffer) {
		writeShort(buffer, type.getOpcode());
	}

	@Override
	public final void writePosition(final ByteBuffer buffer) {
		buffer.position(2);
	}
}

package com.ss.login;

import java.io.File;
import java.util.Properties;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.network.NetworkConfig;
import rlib.util.Util;
import rlib.util.VarTable;

import com.jolbox.bonecp.BoneCPConfig;
import com.ss.login.document.DocumentConfig;

/**
 * Конфиг логин сервера
 * 
 * @author Ronn
 */
public abstract class Config {

	/**
	 * инициализация конфига
	 */
	public static void init() {

		initFolders();

		final VarTable vars = VarTable.newInstance();

		final File file = new File(FOLDER_PROJECT_PATH, "config.xml");

		if(!file.exists()) {
			LOGGER.warning("config not available, the configuration is not loaded!");
			System.exit(0);
		}

		vars.set(new DocumentConfig(file).parse());

		GAME_PORT = vars.getInteger("GAME_PORT");
		USER_PORT = vars.getInteger("USER_PORT");
		AUTO_ACCOUNT_REGISTER = vars.getBoolean("AUTO_ACCOUNT_REGISTER", false);

		DATA_BASE_MAX_CONNECTIONS = vars.getInteger("DATA_BASE_MAX_CONNECTIONS");
		DATA_BASE_MAX_STATEMENTS = vars.getInteger("DATA_BASE_MAX_STATEMENTS");

		DATA_BASE_DRIVER = vars.getString("DATA_BASE_DRIVER");
		DATA_BASE_URL = vars.getString("DATA_BASE_URL");
		DATA_BASE_LOGIN = vars.getString("DATA_BASE_LOGIN");
		DATA_BASE_PASSWORD = vars.getString("DATA_BASE_PASSWORD");

		NETWORK_READ_BUFFER_SIZE = vars.getInteger("NETWORK_READ_BUFFER_SIZE");
		NETWORK_WRITE_BUFFER_SIZE = vars.getInteger("NETWORK_WRITE_BUFFER_SIZE");
		NETWORK_GROUP_SIZE = vars.getInteger("NETWORK_GROUP_SIZE");
		NETWORK_THREAD_PRIORITY = vars.getInteger("NETWORK_THREAD_PRIORITY");

		DEV_DEBUG_SERVER_PACKETS = vars.getBoolean("DEV_DEBUG_SERVER_PACKETS", false);
		DEV_DEBUG_CLIENT_PACKETS = vars.getBoolean("DEV_DEBUG_CLIENT_PACKETS", false);

		DATA_BASE_CONFIG.setJdbcUrl(DATA_BASE_URL);
		DATA_BASE_CONFIG.setUsername(DATA_BASE_LOGIN);
		DATA_BASE_CONFIG.setPassword(DATA_BASE_PASSWORD);
		DATA_BASE_CONFIG.setAcquireRetryAttempts(0);
		DATA_BASE_CONFIG.setAcquireIncrement(5);
		DATA_BASE_CONFIG.setReleaseHelperThreads(0);
		DATA_BASE_CONFIG.setMinConnectionsPerPartition(2);
		DATA_BASE_CONFIG.setMaxConnectionsPerPartition(DATA_BASE_MAX_CONNECTIONS);
		DATA_BASE_CONFIG.setStatementsCacheSize(DATA_BASE_MAX_STATEMENTS);

		final Properties properties = new Properties();

		Util.addUTFToSQLConnectionProperties(properties);

		DATA_BASE_CONFIG.setDriverProperties(properties);
	}

	/**
	 * Инициализация путей к основным папкам сервера.
	 */
	private static void initFolders() {
		FOLDER_PROJECT_PATH = Util.getRootFolderFromClass(Config.class).getAbsolutePath();
	}

	private static final Logger LOGGER = LoggerManager.getLogger(Config.class);

	/** пути к основным папкам сервера */
	public static String FOLDER_PROJECT_PATH;

	public static final NetworkConfig NETWORK_CONFIG = new NetworkConfig() {

		@Override
		public String getGroupName() {
			return "Network";
		}

		@Override
		public int getGroupSize() {
			return NETWORK_GROUP_SIZE;
		}

		@Override
		public int getReadBufferSize() {
			return NETWORK_READ_BUFFER_SIZE;
		}

		@Override
		public Class<? extends Thread> getThreadClass() {
			return ServerThread.class;
		}

		@Override
		public int getThreadPriority() {
			return NETWORK_THREAD_PRIORITY;
		}

		@Override
		public int getWriteBufferSize() {
			return NETWORK_WRITE_BUFFER_SIZE;
		}

		@Override
		public boolean isVesibleReadException() {
			return false;
		}

		@Override
		public boolean isVesibleWriteException() {
			return false;
		}
	};

	/** экземпляр конфига для БД */
	public static final BoneCPConfig DATA_BASE_CONFIG = new BoneCPConfig();

	/** порт для подключения гейм сервера */
	public static int GAME_PORT;
	/** порт для подключения пользователя */
	public static int USER_PORT;

	/** автоматическая регистрация аккаунтов */
	public static boolean AUTO_ACCOUNT_REGISTER;

	/** класс драйвера базы данных */
	public static String DATA_BASE_DRIVER;
	/** адресс базы данных */
	public static String DATA_BASE_URL;

	/** логин для доступа к базе данных */
	public static String DATA_BASE_LOGIN;
	/** пароль для доступа к базе данных */
	public static String DATA_BASE_PASSWORD;

	/** максимальное кол-во коннектов к базе в пуле */
	public static int DATA_BASE_MAX_CONNECTIONS;
	/** максимальное кол-во создаваемых statements */
	public static int DATA_BASE_MAX_STATEMENTS;

	/** размер читаемого буфера */
	public static int NETWORK_READ_BUFFER_SIZE;
	/** размер записываемого буфера */
	public static int NETWORK_WRITE_BUFFER_SIZE;

	/** размер группы сетевых потоков */
	public static int NETWORK_GROUP_SIZE;
	/** приоритет сетевых потоков */
	public static int NETWORK_THREAD_PRIORITY;

	/** отображать дебаг серверных пакетов */
	public static boolean DEV_DEBUG_SERVER_PACKETS;
	/** отображать дебаг клиентских пакетов */
	public static boolean DEV_DEBUG_CLIENT_PACKETS;
}

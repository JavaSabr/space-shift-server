package com.ss.login.document;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.VarTable;

/**
 * Парсер конфига с xml файла.
 * 
 * @author Ronn
 */
public final class DocumentConfig extends AbstractFileDocument<VarTable> {

	public static final String NODE_LIST = "list";

	public DocumentConfig(final File file) {
		super(file);
	}

	@Override
	protected VarTable create() {
		return VarTable.newInstance();
	}

	@Override
	protected void parse(final Document document) {
		for(Node node = document.getFirstChild(); node != null; node = node.getNextSibling()) {
			if(NODE_LIST.equals(node.getNodeName())) {
				result.parse(node, "set", "name", "value");
			}
		}
	}
}

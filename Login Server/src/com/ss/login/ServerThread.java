package com.ss.login;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

/**
 * Модель потока сервера.
 * 
 * @author Ronn
 */
public class ServerThread extends Thread {

	private static final Logger LOGGER = LoggerManager.getLogger(ServerThread.class);

	public ServerThread(final Runnable target) {
		super(target);
	}

	public ServerThread(final ThreadGroup group, final Runnable target, final String name) {
		super(group, target, name);
	}

	@Override
	public void run() {
		try {
			super.run();
		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}
}

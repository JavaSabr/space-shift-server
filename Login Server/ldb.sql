SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `ldb`.`accounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ldb`.`accounts` (
  `login` INT NOT NULL COMMENT 'Уникальное имя аккаунта.',
  `password` VARCHAR(45) NOT NULL COMMENT 'Хеш пароля от аккаунта.',
  `email` VARCHAR(255) NULL COMMENT 'Электронная почта аккаунта.',
  `access_level` INT(11) NOT NULL COMMENT 'Уровень прав у аккаунта.',
  `end_pay` BIGINT NOT NULL COMMENT 'Время завершения оплаты аккаунта.',
  `end_block` BIGINT NOT NULL COMMENT 'Время завершения блокировки аккаунта.',
  `last_ip` VARCHAR(45) NULL COMMENT 'ИП с которого была последняя авторизация аккаунта.',
  `allow_ips` VARCHAR(255) NULL COMMENT 'Доступные ИП для авторизации.',
  `comments` VARCHAR(255) NULL COMMENT 'Комментарий к аккаунту.',
  PRIMARY KEY (`login`, `password`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC))
ENGINE = InnoDB
COMMENT = 'Таблица для хранение аккаунтов серверов.';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
